import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from "../../../service/service";;
import swal from 'sweetalert2';
declare var $: any;
import * as Feather from 'feather-icons';
import { HCoreXAddress } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';
declare var google: any;

@Component({
    selector: 'app-addbranch',
    templateUrl: './addbranch.component.html',

})



export class AddbranchComponent implements OnInit {


    public _Address: HCoreXAddress = {};
    // public _AddressStore: HCoreXAddress = {};
    AddressChange(Address) {
        this._Address = Address;
        // this._AddressStore = Address;
    }
    public isActive: boolean = false;
    public _ShowMap = false;
    subscription: any;
    public _Roles = [
        {
            'id': 0,
            'text': 'Select Roles',
            'apival': 'Select Roles'
        },
        {
            'id': 6,
            'text': 'Manager',
            'apival': 'manager'
        },
        {
            'id': 7,
            'text': 'Administrator/Sub Account',
            'apival': 'admin'
        },
        {
            'id': 8,
            'text': 'RM',
            'apival': 'rm'
        }
    ];
    _CurrentAddress: any = {};
    _ShowInput: boolean = false;
    _SubRegions: any[] = [];
    select2: boolean = true;

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {
    }


    ngOnInit() {
        Feather.replace();
        this.GetStateCategories();
        this._ShowMap = true;
        // this._HelperService._InitMap();
        this.subscription = this._HelperService.navchange
            .subscribe(item => {
                this._ChangeDetectorRef.detectChanges();
            });

        $('#wizard1').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            onFinished: (event, currentIndex) => {
                if (this.isActive) {
                    if (this.MForm_AddUser.valid) {
                        this.Form_AddUser_Process(this.Form_AddUser.value);

                    } else {
                        this._HelperService.NotifyError('Please Enter All Fields Value');
                    }
                } else {
                    if (this.ExistingManager.ReferenceKey) {
                        this.Form_AddUser_Process(this.Form_AddUser.value);

                    } else {

                        this._HelperService.NotifyError('Please Enter All Fields Value');
                    }

                }
            },
            onStepChanging: (event, currentIndex, newIndex) => {
                if (currentIndex < newIndex) {
                    // Step 1 form validation
                    if (currentIndex === 0) {

                        var isStage1Valid: boolean = this.Form_AddUser.controls['Name'].valid
                            && this.Form_AddUser.controls['DisplayName'].valid
                            && this.Form_AddUser.controls['PhoneNumber'].valid
                            && this.Form_AddUser.controls['BranchCode'].valid
                            && this.Form_AddUser.controls['EmailAddress'].valid;

                        if (isStage1Valid) {
                            return true;
                        } else {
                            this._HelperService.NotifyError('Please Enter All Fields Value');
                        }
                    }
                    // 
                    // Step 2 form validation
                    // 
                    if (currentIndex === 1) {
                        var isStage2Valid: boolean = this.Form_AddUser.controls['CityName'].valid
                            && this.Form_AddUser.controls['StateName'].valid
                            // && this.Form_AddUser.controls['RegionName'].valid
                            && this.Form_AddUser.controls['Address'].valid
                        if (isStage2Valid) {

                            return true;
                        } else {
                            this._HelperService.NotifyError('Please Enter All Fields Value');
                        }
                    }

                    // Always allow step back to the previous step even if the current step is not valid.
                } else { return true; }
            }

        });

        this.Form_AddUser_Load();
        this.GetRoles_List()
        this.GetMangers_List();
        this.AssignManger_List();
        this.S2_Region_Load();
        this.MForm_AddUser_Load();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];

        });
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }

    MForm_AddUser: FormGroup;
    ExistingManager: any = {};

    MForm_AddUser_Load() {
        this.MForm_AddUser = this._FormBuilder.group({
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256),Validators.pattern('^[a-zA-Z \-\']+')])],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$'), Validators.minLength(2)])],
            // RoleKey: [null],
            // RoleId: [null],
            OwnerKey: [null, Validators.required],
            OwnerId: [null, Validators.required]

        });
    }
    MForm_AddUser_Clear() {
        this.MForm_AddUser.reset();
        this.MForm_AddUser_Load();
    }

    Form_AddUser: FormGroup;
    Form_AddUser_Address: string = null;
    Form_AddUser_Latitude: number = 0;
    Form_AddUser_Longitude: number = 0;
    @ViewChild('places') places: GooglePlaceDirective;
    Form_AddUser_PlaceMarkerClick(event) {
        this.Form_AddUser_Latitude = event.coords.lat;
        this.Form_AddUser_Longitude = event.coords.lng;
    }


    public Form_AddUser_AddressChange(address: Address) {
        this.Form_AddUser_Latitude = address.geometry.location.lat();
        this.Form_AddUser_Longitude = address.geometry.location.lng();
        this.Form_AddUser_Address = address.formatted_address;

        this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);

        if (this._CurrentAddress.country != this._HelperService.UserCountrycode) {
            this._HelperService.NotifyError('Currently we’re not serving in this area, please add locality within ' + this._HelperService.UserCountrycode);
            this.reset1();

        }
        else {
            this.Form_AddUser.controls['Address'].setValue(address.formatted_address);
            this.Form_AddUser.controls['MapAddress'].setValue(address.formatted_address);
        }

        this.select2 = false;
        this._ChangeDetectorRef.detectChanges();

        if (this._CurrentAddress.sublocality_level_1) {
            this._SubRegions.push({
                text: this._CurrentAddress.sublocality_level_1,
                id: 0
            });

            if (this._CurrentAddress.sublocality_level_2) {
                this._SubRegions.push({
                    text: this._CurrentAddress.sublocality_level_2,
                    id: 1
                });

                if (this._CurrentAddress.sublocality_level_3) {
                    this._SubRegions.push({
                        text: this._CurrentAddress.sublocality_level_3,
                        id: 2
                    });

                    if (this._CurrentAddress.sublocality_level_4) {
                        this._SubRegions.push({
                            text: this._CurrentAddress.sublocality_level_4,
                            id: 3
                        });
                    }
                }
            }
        }

        this.select2 = true;
        this._ChangeDetectorRef.detectChanges();

    }

    reset1() {
        this.Form_AddUser.controls['Address'].reset()
        this.Form_AddUser.controls['CityName'].reset()
        this.Form_AddUser.controls['StateName'].reset()
        this.Form_AddUser.controls['CountryName'].reset()

    }


    toogleShowInput(): void {
        this._ShowInput = !this._ShowInput;
    }

    Form_AddUser_Show() {
        // this._HelperService.OpenModal('Form_AddUser_Content');
    }
    Form_AddUser_Close() {

        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.ReferredMerchants]);
        // this._HelperService.OpenModal('Form_AddUser_Content');
    }
    Form_AddUser_Load() {

        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_AddUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SaveBranch,
            AccountId: this._HelperService.AppConfig.ActiveReferenceId,
            AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            PhoneNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$'), Validators.minLength(2)])],
            RegionName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
            BranchCode: [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(14)])],

            Address: [null],
            StateName: [null],
            CityName: [null],
            Latitude: 0,
            Longitude: 0,

            CityId: [null],
            CityCode: [null],
            StateId: [null],
            StateCode: [null],
            CountryName: this._HelperService.UserCountrycode,
            StatusCode: this._HelperService.AppConfig.Status.Active,
            Manager: [null]
        });
    }
    Form_AddUser_Clear() {
        this.Form_AddUser_Latitude = 0;
        this.Form_AddUser_Longitude = 0;
        this.Form_AddUser.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    public MerchantSaveRequest: any;

    Form_AddUser_Process(_FormValue: any) {

        if (!this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "") {
            this._HelperService.NotifyError('Please select business location');
        }
        else {
            swal({
                position: 'top',
                title: 'Create branch account ?',
                text: 'Please verify information and click on continue button to create branch',
                animation: false,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                showCancelButton: true,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            }).then((result) => {
                if (result.value) {


                    

                    _FormValue = this.ReFormat_RequestBody();
                    // this.MerchantSaveRequest.Address = this._Address.Address;
                    _FormValue.Manager = (this.isActive) ? this.MForm_AddUser.value : this.ExistingManager;
                    _FormValue.Address = this._Address,
                        _FormValue.AddressComponent = this._Address,
                        this._HelperService.IsFormProcessing = true;
                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Branch, _FormValue);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess(_Response.Message);
                                this.Form_AddUser_Clear();
                                this.Form_AddUser_Load();
                                this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AcquirerPanel.Accounts.Branch, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId]);


                                if (_FormValue.OperationType == 'edit') {
                                    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.MerchantManager, _Response.Result.ReferenceKey]);
                                    // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.Merchant.Dashboard, _Response.Result.ReferenceKey]);
                                }
                                else if (_FormValue.OperationType == 'close') {
                                    this.Form_AddUser_Close();
                                }
                            }
                            else {
                                this._HelperService.NotifyError(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        });
                }
            });
        }


    }

    //City DropDown
    public GetRoles_Option: Select2Options;
    public GetRoles_Transport: any;
    GetRoles_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreParameters,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'Name',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
        this.GetRoles_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetRoles_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetRoles_Transport,
            multiple: false,
        };
    }

    GetRoles_ListChange(event: any) {

        this.Form_AddUser.controls['OwnerId'].setValue(null);
        this.Form_AddUser.controls['OwnerKey'].setValue(null);

        this.MForm_AddUser.patchValue(
            {
                RoleId: event.value,
                RoleKey: event.data[0].apival
            }
        );

        this._ShowRole = false;
        this._ChangeDetectorRef.detectChanges();

        if (event.value == 8) { //RM
            this._ShowReportingM = false;
            this._ChangeDetectorRef.detectChanges();
            this._MangerTypeId = 6; //Manager

            this.AssignManger_List();
            this._ShowReportingM = true;
            this._ChangeDetectorRef.detectChanges();
        } else if (event.value == 7) { //SubAccount

            this._ShowReportingM = false;

            this.Form_AddUser.controls['OwnerId'].setValue(this._HelperService.UserAccount.AccountId);
            this.Form_AddUser.controls['OwnerKey'].setValue(this._HelperService.UserAccount.AccountKey);

            this._MangerTypeId = undefined;
        }
        else if (event.value == 6) { //Manager
            this._ShowReportingM = false;
            this._ChangeDetectorRef.detectChanges();
            this._MangerTypeId = 7; //Subaccount
            this.AssignManger_List();
            this._ShowReportingM = true;
            this._ChangeDetectorRef.detectChanges();

        } else {
            this._MangerTypeId = undefined;
            this._ShowReportingM = true;
            this.AssignManger_List();
        }

        this._ShowRole = true;
        this._ChangeDetectorRef.detectChanges();

        this.AssignManger_List();
    }

    public _MangerTypeId: number;
    public _ShowRole: boolean = true;
    public _ShowReportingM: boolean = true;



    public AssignManger_Option: Select2Options;
    public AssignManger_Transport: any;
    AssignManger_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect;

        _Select =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetSubAccounts,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.SubAccounts,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
            ]
        }




        this.AssignManger_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.AssignManger_Option = {
            placeholder: PlaceHolder,
            ajax: this.AssignManger_Transport,
            multiple: false,
        };
    }
    AssignManger_ListChange(event: any) {
        this.MForm_AddUser.patchValue(
            {
                OwnerId: event.data[0].ReferenceId,
                OwnerKey: event.data[0].ReferenceKey

            }
        );

    }

    // Manager DropDown
    public GetMangers_Option: Select2Options;
    public GetMangers_Transport: any;
    GetMangers_List() {
        var PlaceHolder = "Select Manager";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetManagers,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },

            ]
        }

        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'RoleId', this._HelperService.AppConfig.DataType.Text, '6', '=');
        this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetMangers_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetMangers_Transport,
            multiple: false,
        };
    }
    GetMangers_ListChange(event: any) {
       
        // alert(event);
        this.ExistingManager.Name=event.data[0].OwnerDisplayName;
        this.ExistingManager.MobileNumber=event.data[0].MobileNumber;
        this.ExistingManager.EmailAddress=this._HelperService.lowerCaseConvert(event.data[0].EmailAddress);
        this.ExistingManager.ReferenceKey = event.data[0].ReferenceKey;
        this.ExistingManager.ReferenceId = event.data[0].ReferenceId;

        // this.ExistingManager.EmailAddress=event.data[0].EmailAddress;
        // this.ExistingManager.Name=event.data[0].Name;
        // this.ExistingManager.MobileNumber=event.data[0].MobileNumber;

    }


    //Region Drop Down
    public S2_Region_Option: Select2Options
    S2_Region_Load() {

        this.S2_Region_Option = {
            placeholder: 'Select Region',
            multiple: false,
            allowClear: false,
        };
    }

    S2_Region_Change(event: any) {

        this.Form_AddUser.patchValue(
            {
                RegionName: event.data[0].text
            }

        );

    }

    toogleIsActive(): void {
        this.isActive = !this.isActive;

        if (this.isActive) {
            this.ExistingManager = {};
        }


    }


    //state
    public StateCategories = [];
    public S2StateCategories = [];
    public ShowstateSelector: boolean = true;
    public ShowcitySelector: boolean = true;
    GetStateCategories() {

        this._HelperService.ToggleField = true;
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Core.getstates,
            ReferenceKey: this._HelperService.UserCountrykey,
            ReferenceId: this._HelperService.UserCountryId,
            //SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
            // SortExpression: 'Name asc',
            Offset: 0,
            Limit: 1000,
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.State, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        this.StateCategories = _Response.Result.Data;

                        this.ShowstateSelector = false;
                        this._ChangeDetectorRef.detectChanges();
                        this.S2StateCategories.push(
                            {
                                id: 0,
                                key: "0",
                                text: "Select State"
                            }
                        );


                        for (let index = 0; index < this.StateCategories.length; index++) {

                            const element = this.StateCategories[index];
                            this.S2StateCategories.push(
                                {
                                    id: element.ReferenceId,
                                    key: element.ReferenceKey,
                                    text: element.Name
                                }
                            );
                        }
                        this.ShowstateSelector = true;
                        this._ChangeDetectorRef.detectChanges();

                        this._HelperService.ToggleField = false;

                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                this._HelperService.ToggleField = false;

            });
    }
    public SelectedStateCategories = [];
    public selectedstate: boolean = false;
    statekey: any; stateid: any; statename: any;
    StateSelected(Items) {
        if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
            this.SelectedStateCategories = Items.value;
            this.statekey = Items.data[0].key;
            this.stateid = Items.data[0].id;
            this.statename = Items.data[0].text;
            this.selectedstate = true;
            this.GetCityCategories()
            this.Form_AddUser.controls['StateCode'].patchValue(this.statekey);
            this.Form_AddUser.controls['StateId'].patchValue(this.stateid);
            this.Form_AddUser.controls['StateName'].patchValue(this.statename);
        }
        else {
            this.SelectedStateCategories = [];
        }
    }


    //City
    public CityCategories = [];
    public S2CityCategories = [];
    //public ShowcitySelector: boolean = true;
    GetCityCategories() {
        this._HelperService.ToggleField = true;
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Core.getcities,
            ReferenceKey: this.statekey,
            ReferenceId: this.stateid,
            //SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
            // SortExpression: 'Name asc',
            Offset: 0,
            Limit: 1000,
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.City, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        this.CityCategories = _Response.Result.Data;
                        this.ShowcitySelector = false;
                        this._ChangeDetectorRef.detectChanges();
                        this.S2CityCategories = [];
                        for (let index = 0; index < this.CityCategories.length; index++) {
                            const element = this.CityCategories[index];
                            this.S2CityCategories.push(
                                {
                                    id: element.ReferenceId,
                                    key: element.ReferenceKey,
                                    text: element.Name
                                }
                            );
                        }
                        this.ShowcitySelector = true;
                        this._ChangeDetectorRef.detectChanges();

                        this._HelperService.ToggleField = false;

                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                this._HelperService.ToggleField = false;

            });



    }
    public SelectedCityCategories = [];
    public citykey: any; public cityid: any; cityname: any;
    CitySelected(Items) {
        if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
            this.SelectedCityCategories = Items.value;
            this.citykey = Items.data[0].key;
            this.cityid = Items.data[0].id
            this.cityname = Items.data[0].text
            this.Form_AddUser.controls['CityCode'].patchValue(this.citykey);
            this.Form_AddUser.controls['CityId'].patchValue(this.cityid);
            this.Form_AddUser.controls['CityName'].patchValue(this.cityname);
        }
        else {
            this.SelectedCityCategories = [];
        }
    }


    ReFormat_RequestBody(): void {
        var formValue: any = this.Form_AddUser.value;
        var MformValue: any = this.MForm_AddUser.value;
        var formRequest: any = {

            OperationType: 'new',
            Task: formValue.Task,
            AccountId: formValue.AccountId,
            AccountKey: formValue.AccountKey,
            BranchId: formValue.BranchId,
            BranchKey: formValue.BranchKey,
            RmId: formValue.RmId,
            RmKey: formValue.RmKey,

            BranchCode: formValue.BranchCode,

            DisplayName: formValue.DisplayName,
            Name: formValue.Name,
            FirstName: formValue.FirstName,
            BusinessOwnerName: formValue.BusinessOwnerName,
            ReferralCode: formValue.ReferralCode,

            // MobileNumber: formValue.MobileNumber,
            PhoneNumber: formValue.PhoneNumber,
            ContactNumber: formValue.ContactNumber,
            EmailAddress: this._HelperService.lowerCaseConvert(formValue.EmailAddress),
            RewardPercentage: formValue.RewardPercentage,
            Address: this._Address.Address,
            AddressComponent: this._Address,
            // 

            // Manager: {

            //     Name: MformValue.Name,
            //     MobileNumber: MformValue.MobileNumber,
            //     EmailAddress: this._HelperService.lowerCaseConvert(MformValue.EmailAddress)
            // },


            // ContactPerson: {
            //     FirstName: formValue.FirstName,
            //     LastName: formValue.LastName,
            //     MobileNumber: formValue.MobileNumber,
            //     EmailAddress: formValue.EmailAddress
            // },
            // Stores: [],
            StatusCode: formValue.StatusCode

        };
        // if(formValue.EmailAddress != null){
        //     formRequest.EmailAddress = this._HelperService.lowerCaseConvert(formValue.EmailAddress);
        //  }
        //  if(MformValue.EmailAddress != null){
        //     formRequest.Manager.EmailAddress = this._HelperService.lowerCaseConvert(MformValue.EmailAddress);
        //  }
        return formRequest;

    }


}

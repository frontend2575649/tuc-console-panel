import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
declare var moment: any;
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from "../../../service/service";;
import swal from 'sweetalert2';
declare var $: any;
import * as Feather from 'feather-icons';
import { InputFile, InputFileComponent } from 'ngx-input-file';
//import { HCoreXAddress } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';
declare var google: any;

@Component({
    selector: 'app-tuaddreferal',
    templateUrl: './tuaddreferal.component.html',

})
export class AddReferalComponent implements OnInit {
    public SaveAccountRequest: any;
    CurrentImagesCount: number = 0;

    _DealConfig =
        {

            Images: [],
            StartDateConfig: {
            },
            EndDateConfig: {
            },
            DefaultStartDate: null,
            DefaultEndDate: null,
            DealImages: [],
            StartDate: null,
            EndDate: null,
        }

    //  @ViewChild("inputfile")
    //  private InputFileComponent: InputFileComponent;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {
    }

    ngOnInit() {
        Feather.replace();
        this._DataHelperService.LocManager_OpenUpdateManager_Clear();
        this.Form_AddUser_Load();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
        });
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
        this.GetConditionCategories();

        // this.InitImagePicker(this.InputFileComponent);


        //deal date
        this._DealConfig.DefaultStartDate = moment();
        this._DealConfig.DefaultEndDate = moment().add(1, 'days').endOf("day");
        this._DealConfig.StartDate = this._DealConfig.DefaultStartDate;
        this._DealConfig.EndDate = this._DealConfig.DefaultEndDate;
        this._DealConfig.StartDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: this._HelperService.DateInUTC(moment()),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: this._HelperService.DateInUTC(moment()),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        //end deal date
    }


    private InitImagePicker(InputFileComponent: InputFileComponent) {
        if (InputFileComponent != undefined) {
            this.CurrentImagesCount = 0;
            this._HelperService._InputFileComponent = InputFileComponent;
            InputFileComponent.onChange = (files: Array<InputFile>): void => {
                if (files.length >= this.CurrentImagesCount) {
                    this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
                }
                this.CurrentImagesCount = files.length;
            };
        }
    }
    removeImage(): void {
        this.CurrentImagesCount = 0;
        this._HelperService.Icon_Crop_Clear();
    }
    _CurrentAddress: any = {};
    Form_AddUser: FormGroup;
    Form_AddUser_Close() {
        this._Router.navigate(['console' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AccountSettings.Referal]);
    }
    Form_AddUser_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_AddUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.savepromocode,

            CountryId: this._HelperService.UserCountryId,
            CountryKey: this._HelperService.UserCountrycode,
            AccountTypeId: 109,
            AccountTypeCode: 'appuser',
            ConditionId: 1,
            ConditionKey: 1,
            TypeId: "1",
            TypeCode: 'default.inactive',
            Title: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            Description: [null, Validators.compose([Validators.minLength(2), Validators.maxLength(256)])],
            Budget: 0,
            PromoCode: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(12)])],
            PromoCodeValue: [null, Validators.compose([Validators.required, Validators.minLength(1)])],
            MaximumLimit: [null, Validators.compose([Validators.required, Validators.minLength(1)])],
           // MaximumLimitPerUser: [null, Validators.compose([Validators.minLength(1)])],
           MaximumLimitPerUser: 1,
            StatusCode: this._HelperService.AppConfig.Status.Active,
            CodeUsageValidity: 1,
            StartDate: moment().format('DD-MM-YYYY hh:mm a'),
            EndDate: moment().add(1, 'days').endOf("day").format('DD-MM-YYYY hh:mm a'),
        });
    }
    Form_AddUser_Clear() {
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 300);
        this.Form_AddUser.reset();
        this._HelperService.Icon_Crop_Clear();
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_AddUser_Process(_FormValue: any) {
        if (this._HelperService._Icon_Cropper_Data.Content != null) {
            _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
        }


        else {
            this._HelperService.IsFormProcessing = true;
            this.SaveAccountRequest = this.ReFormat_RequestBody();
           
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promocode, this.SaveAccountRequest);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess('Referral code campaign successfully created.');
                        this._HelperService.RandomPassword = null;
                        this.Form_AddUser_Clear();
                        if (_FormValue.OperationType == 'edit') {
                        }
                        else if (_FormValue.OperationType == 'close') {
                            this.Form_AddUser_Close();
                        }
                        this.Form_AddUser_Close();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }


    }

    ReFormat_RequestBody(): void {
        var formValue: any = this.Form_AddUser.value;
        if (formValue.AccountTypeId == '108') {
            formValue.AccountTypeCode = 'thankumerchant'
        }
        else {
            if (formValue.AccountTypeId == '109') {
                formValue.AccountTypeCode = 'appuser'
            }
        }


        if (formValue.ConditionKey == '1') {
            formValue.ConditionId = '1'
        }
        else {
            if (formValue.ConditionKey == '2') {
                formValue.ConditionId = '2'
            }
        }
        var formRequest: any = {
            OperationType: 'new',
            Task: formValue.Task,


            CountryId: this._HelperService.UserCountryId,
            CountryKey: this._HelperService.UserCountrycode,
            AccountTypeId: formValue.AccountTypeId,
            AccountTypeCode: formValue.AccountTypeCode,
            ConditionId: formValue.ConditionId,
            ConditionKey: formValue.ConditionKey,
            TypeId: 2,
            TypeCode: 'Promotional',
            Title: formValue.Title,
            Description: formValue.Description,
            Budget: this.budgetAmount,
            StartDate: moment(this._DealConfig.StartDate).format('YYYY-MM-DD HH:mm'),
            EndDate: moment(this._DealConfig.EndDate).format('YYYY-MM-DD HH:mm'),
            PromoCode: formValue.PromoCode,
            PromoCodeValue: formValue.PromoCodeValue,
            MaximumLimit: formValue.MaximumLimit,
            MaximumLimitPerUser: formValue.MaximumLimitPerUser,
            StatusCode: "default.active"
        };
        return formRequest;

    }

    ScheduleStartDateRangeChange(value) {
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: value.start,
            minDate: value.start,
        };
        this._DealConfig.StartDate = value.start;
        this._DealConfig.EndDate = value.start;
        // this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                StartDate: value.start.format('DD-MM-YYYY hh:mm a'),
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),

            }
        );
    }
    ScheduleEndDateRangeChange(value) {

        this._DealConfig.EndDate = value.start;

        this.Form_AddUser.patchValue(
            {
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),

            }
        );
    }

    //Get condition
    //  public GetCondition_Option: Select2Options;
    public GetApps_Transport: any;
    getApps_list() {
        this.GetMerchants_Option = {
            placeholder: 'Condition',
            ajax: this.GetApps_Transport,
            multiple: false,
        };
    }

    TargetData: any;
    GetApp_ListChange(event: any) {
        this.TargetData = event.data[0].Name
        this.Form_AddUser.patchValue(
            {
                ConditionKey: event.data[0].ReferenceKey,
                ConditionId: event.data[0].ReferenceId
                // TypeId: event.value

            }


        );
    }
    budgetAmount: any = 0
    ProcessAmounts() {
        this.budgetAmount = (this.Form_AddUser.controls['MaximumLimit'].value) * (this.Form_AddUser.controls['PromoCodeValue'].value)
    }


    public ConditionCategories = [];
    public S2ConditionCategories = [];
    public stateerrro: boolean = false;
   


    public GetMerchants_Option: Select2Options;
    public GetMerchants_Transport: any;
    public SelectedMerchant: any = {};
    GetConditionCategories() {
        var PlaceHolder = "Select Target Audience";
        var _Select: OSelect =
        {


            Task: this._HelperService.AppConfig.Api.Core.getpromocodeconditions,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Promocode,

            ReferenceKey: null,
            ReferenceId: 0,


            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                
            ]
        }
        this.GetMerchants_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetMerchants_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetMerchants_Transport,
            multiple: false,
        };

    }


    

    CodeUsageValidity = 1;
    validity: boolean = false;
    CodeUsage(code) {
        this.CodeUsageValidity = code;
        if (this.CodeUsageValidity == 2) {
            this.validity = true;
        }
        else {
            this.validity = false;
        }
    }




}

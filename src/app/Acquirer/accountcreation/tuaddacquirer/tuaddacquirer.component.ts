import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from "../../../service/service";;
import swal from 'sweetalert2';
declare var $: any;
import * as Feather from 'feather-icons';
import { InputFile, InputFileComponent } from 'ngx-input-file';
import { HCoreXAddress } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';
declare var google: any;

@Component({
    selector: 'app-tuaddacquirer',
    templateUrl: './tuaddacquirer.component.html',

})
export class AddAcquirerComponent implements OnInit {
    public SaveAccountRequest: any;
    CurrentImagesCount: number = 0;


    @ViewChild("inputfile")
    private InputFileComponent: InputFileComponent;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {
    }

    ngOnInit() {
        Feather.replace();
        this._HelperService._Icon_Cropper_Image = '';
        this._DataHelperService.LocManager_OpenUpdateManager_Clear();
        this.Form_AddUser_Load();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
        });
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
        this.InitImagePicker(this.InputFileComponent);
    }

    public _Address: HCoreXAddress = {};
    AddressChange(Address) {
        this._Address = Address;
    }

    private InitImagePicker(InputFileComponent: InputFileComponent) {
        if (InputFileComponent != undefined) {
            this.CurrentImagesCount = 0;
            this._HelperService._InputFileComponent = InputFileComponent;
            InputFileComponent.onChange = (files: Array<InputFile>): void => {
                if (files.length >= this.CurrentImagesCount) {
                    this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
                }
                this.CurrentImagesCount = files.length;
            };
        }
    }
    removeImage(): void {
        this.CurrentImagesCount = 0;
        this._HelperService.Icon_Crop_Clear();
    }
    resetImage(){
        this.CurrentImagesCount = 0;
    this._HelperService.Icon_Crop_Clear();
    this.InputFileComponent.files.pop();
}
    
    // _CurrentAddress: any = {};
    Form_AddUser: FormGroup;
    Form_AddUser_Close() {
        this._Router.navigate(['console' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AccountSettings.Acquirer]);
    }
    Form_AddUser_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_AddUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SaveAcquirer,
            UserName: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(256)])],
            Password: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.Password)])],
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256),Validators.pattern('^[a-zA-Z \-\']+')])],
            FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            SecondaryEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            WebsiteUrl: [null, Validators.compose([Validators.minLength(2), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.WebsiteUrl)])],
            Description: null,
            ReferralCode: null,
            StatusCode: this._HelperService.AppConfig.Status.Inactive,
            IconContent: this._HelperService._Icon_Cropper_Data,
        });
    }
    Form_AddUser_Clear() {
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 300);
        this.Form_AddUser.reset();
        this._HelperService.Icon_Crop_Clear();
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_AddUser_Process(_FormValue: any) {
        
        if (this._HelperService._Icon_Cropper_Data.Content != null) {
            _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
        }

        if (!this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "") {
            this._HelperService.NotifyError('Please select business location');
        }
        else {
            this._HelperService.IsFormProcessing = true;
            this.SaveAccountRequest = this.ReFormat_RequestBody();
            //Location Manager - Start
            this.SaveAccountRequest.Address = this._Address.Address;
            //Location Manager - End
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, this.SaveAccountRequest);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess('Acquirer account created successfully.');
                        this._HelperService.RandomPassword = null;
                        this.Form_AddUser_Clear();
                        if (_FormValue.OperationType == 'edit') {
                        }
                        else if (_FormValue.OperationType == 'close') {
                            this.Form_AddUser_Close();
                        }
                        this.Form_AddUser_Close();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }


    }

    ReFormat_RequestBody(): void {
        var formValue: any = this.Form_AddUser.value;
        var formRequest: any = {
            OperationType: 'new',
            Task: formValue.Task,
            DisplayName: formValue.DisplayName,
            Name: formValue.Name,
            ContactNumber: formValue.ContactNumber,
            EmailAddress: formValue.EmailAddress,
            RewardPercentage: formValue.RewardPercentage,
            WebsiteUrl: formValue.WebsiteUrl,
            ReferralCode: formValue.ReferralCode,
            Description: formValue.Description,
            UserName: formValue.UserName,
            Password: formValue.Password,
            StatusCode: formValue.StatusCode,
            // 
            AddressComponent: this._Address,
            // 
            ContactPerson: {
                FirstName: formValue.FirstName,
                LastName: formValue.LastName,
                MobileNumber: formValue.MobileNumber,
                EmailAddress: formValue.EmailAddress
            },
            IconContent: formValue.IconContent
        };
        return formRequest;

    }

}

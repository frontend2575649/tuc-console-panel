import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon } from '../../../../service/service';
import swal from 'sweetalert2';

@Component({
    selector: 'tu-redeem',
    templateUrl: './turedeem.component.html',
})
export class TURedeemComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {
    }
    ngOnInit() {
        this.TURedeem_Filter_Merchants_Load();
        this.TURedeem_Filter_Stores_Load();
        this.TURedeem_Filter_Banks_Load();
        this.TURedeem_Filter_Providers_Load();
        this.TURedeem_Filter_Issuers_Load();
        this.TURedeem_Filter_CardBrands_Load();
        this.TURedeem_Filter_TransactionTypes_Load();
        this.TURedeem_Filter_CardBanks_Load();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
                this.TURedeem_Setup();
            }
            else {
                this._HelperService.Get_UserAccountDetails(true);
                this.TURedeem_Setup();
            }
        });
    }

    public TURedeem_Config: OList;
    TURedeem_Setup() {
        this.TURedeem_Config =
            { Id: null, Sort: null,
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetRedeemTransactions,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore,
                Title: 'Redeem History',
                StatusType: 'transaction',
                Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
                StatusName:"Success",
                Type: this._HelperService.AppConfig.ListType.All,
                DefaultSortExpression: 'TransactionDate desc',
                TableFields: [
                    {
                        DisplayName: 'User',
                        SystemName: 'UserAccountDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                    },
                    {
                        DisplayName: 'Mobile Number',
                        SystemName: 'UserAccountMobileNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                    },
                    {
                        DisplayName: 'Type',
                        SystemName: 'TypeName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Card Type',
                        SystemName: 'CardBrandName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Bank',
                        SystemName: 'CardBankName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: false,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Card',
                        SystemName: 'CardBinNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: false,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Invoice Amt',
                        SystemName: 'InvoiceAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                    },
                    {
                        DisplayName: 'Reward Amt',
                        SystemName: 'RewardAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: 'text-grey',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Date',
                        SystemName: 'TransactionDate',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: 'td-date',
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                        IsDateSearchField: true,
                    },
                    {
                        DisplayName: 'Store',
                        SystemName: 'SubParentDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,


                    },
                    {
                        DisplayName: 'Done By',
                        SystemName: 'CreatedByDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,


                    },
                    {
                        DisplayName: 'Ref',
                        SystemName: 'ReferenceNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: false,
                        Search: true,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Acquirer',
                        SystemName: 'AcquirerName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: false,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Provider',
                        SystemName: 'ProviderDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: false,
                        Search: false,
                        Sort: false,
                    },
                ]
            }
        this.TURedeem_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TURedeem_Config.SearchBaseCondition, 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');
        this.TURedeem_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TURedeem_Config.SearchBaseCondition, 'ParentId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '=');

        this.TURedeem_Config = this._DataHelperService.List_Initialize(this.TURedeem_Config);
        this.TURedeem_GetData();
    }
    TURedeem_ToggleOption_Date(event: any, Type: any) {
        this.TURedeem_ToggleOption(event, Type);
    }
    TURedeem_ToggleOption(event: any, Type: any) {
        this.TURedeem_Config = this._DataHelperService.List_Operations(this.TURedeem_Config, event, Type);
        if (this.TURedeem_Config.RefreshData == true) {
            this.TURedeem_GetData();
        }
    }
    TURedeem_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.TURedeem_Config);
        this.TURedeem_Config = TConfig;
    }
    TURedeem_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    }

    public TURedeem_Filter_Merchant_Option: Select2Options;
    public TURedeem_Filter_Merchant_Selected = 0;
    TURedeem_Filter_Merchants_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Merchant
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TURedeem_Filter_Merchant_Option = {
            placeholder: 'Filter by Merchant',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TURedeem_Filter_Merchants_Change(event: any) {
        if (event.value == this.TURedeem_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_Merchant_Selected, '=');
            this.TURedeem_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TURedeem_Config.SearchBaseConditions);
            this.TURedeem_Filter_Merchant_Selected = 0;
        }
        else if (event.value != this.TURedeem_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_Merchant_Selected, '=');
            this.TURedeem_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TURedeem_Config.SearchBaseConditions);
            this.TURedeem_Filter_Merchant_Selected = event.value;
            this.TURedeem_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_Merchant_Selected, '='));
        }
        this.TURedeem_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TURedeem_Filter_Store_Option: Select2Options;
    public TURedeem_Filter_Store_Selected = 0;
    TURedeem_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Store
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TURedeem_Filter_Store_Option = {
            placeholder: 'Filter by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TURedeem_Filter_Stores_Change(event: any) {
        if (event.value == this.TURedeem_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_Store_Selected, '=');
            this.TURedeem_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TURedeem_Config.SearchBaseConditions);
            this.TURedeem_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TURedeem_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_Store_Selected, '=');
            this.TURedeem_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TURedeem_Config.SearchBaseConditions);
            this.TURedeem_Filter_Store_Selected = event.value;
            this.TURedeem_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_Store_Selected, '='));
        }
        this.TURedeem_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }
    public TURedeem_Filter_Bank_Option: Select2Options;
    public TURedeem_Filter_Bank_Selected = 0;
    TURedeem_Filter_Banks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TURedeem_Filter_Bank_Option = {
            placeholder: 'Sort by Bank',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TURedeem_Filter_Banks_Change(event: any) {
        if (event.value == this.TURedeem_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_Bank_Selected, '=');
            this.TURedeem_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TURedeem_Config.SearchBaseConditions);
            this.TURedeem_Filter_Bank_Selected = 0;
        }
        else if (event.value != this.TURedeem_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_Bank_Selected, '=');
            this.TURedeem_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TURedeem_Config.SearchBaseConditions);
            this.TURedeem_Filter_Bank_Selected = event.value;
            this.TURedeem_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_Bank_Selected, '='));
        }
        this.TURedeem_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TURedeem_Filter_Provider_Option: Select2Options;
    public TURedeem_Filter_Provider_Selected = 0;
    TURedeem_Filter_Providers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TURedeem_Filter_Provider_Option = {
            placeholder: 'Filter by PTSP',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TURedeem_Filter_Providers_Change(event: any) {
        if (event.value == this.TURedeem_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_Provider_Selected, '=');
            this.TURedeem_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TURedeem_Config.SearchBaseConditions);
            this.TURedeem_Filter_Provider_Selected = 0;
        }
        else if (event.value != this.TURedeem_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_Provider_Selected, '=');
            this.TURedeem_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TURedeem_Config.SearchBaseConditions);
            this.TURedeem_Filter_Provider_Selected = event.value;
            this.TURedeem_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_Provider_Selected, '='));
        }
        this.TURedeem_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TURedeem_Filter_Issuer_Option: Select2Options;
    public TURedeem_Filter_Issuer_Selected = 0;
    TURedeem_Filter_Issuers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
            ]
        };
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
            [
                this._HelperService.AppConfig.AccountType.PosTerminal,
                this._HelperService.AppConfig.AccountType.PGAccount,
                this._HelperService.AppConfig.AccountType.Cashier,

            ], '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TURedeem_Filter_Issuer_Option = {
            placeholder: 'Sort by Issuer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TURedeem_Filter_Issuers_Change(event: any) {
        if (event.value == this.TURedeem_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_Issuer_Selected, '=');
            this.TURedeem_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TURedeem_Config.SearchBaseConditions);
            this.TURedeem_Filter_Issuer_Selected = 0;
        }
        else if (event.value != this.TURedeem_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_Issuer_Selected, '=');
            this.TURedeem_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TURedeem_Config.SearchBaseConditions);
            this.TURedeem_Filter_Issuer_Selected = event.value;
            this.TURedeem_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_Issuer_Selected, '='));
        }
        this.TURedeem_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TURedeem_Filter_TransactionType_Option: Select2Options;
    public TURedeem_Filter_TransactionType_Selected = 0;
    TURedeem_Filter_TransactionTypes_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionType
                },
                {
                    SystemName: "SubParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionTypeRedeem
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TURedeem_Filter_TransactionType_Option = {
            placeholder: 'Sort by Transaction Type',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TURedeem_Filter_TransactionTypes_Change(event: any) {
        if (event.value == this.TURedeem_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_TransactionType_Selected, '=');
            this.TURedeem_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TURedeem_Config.SearchBaseConditions);
            this.TURedeem_Filter_TransactionType_Selected = 0;
        }
        else if (event.value != this.TURedeem_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_TransactionType_Selected, '=');
            this.TURedeem_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TURedeem_Config.SearchBaseConditions);
            this.TURedeem_Filter_TransactionType_Selected = event.value;
            this.TURedeem_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_TransactionType_Selected, '='));
        }
        this.TURedeem_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TURedeem_Filter_CardBrand_Option: Select2Options;
    public TURedeem_Filter_CardBrand_Selected = 0;
    TURedeem_Filter_CardBrands_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "TypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.CardBrand
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TURedeem_Filter_CardBrand_Option = {
            placeholder: 'Sort by Card Brand',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TURedeem_Filter_CardBrands_Change(event: any) {
        if (event.value == this.TURedeem_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_CardBrand_Selected, '=');
            this.TURedeem_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TURedeem_Config.SearchBaseConditions);
            this.TURedeem_Filter_CardBrand_Selected = 0;
        }
        else if (event.value != this.TURedeem_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_CardBrand_Selected, '=');
            this.TURedeem_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TURedeem_Config.SearchBaseConditions);
            this.TURedeem_Filter_CardBrand_Selected = event.value;
            this.TURedeem_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_CardBrand_Selected, '='));
        }
        this.TURedeem_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TURedeem_Filter_CardBank_Option: Select2Options;
    public TURedeem_Filter_CardBank_Selected = 0;
    TURedeem_Filter_CardBanks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "TypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.CardBank
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TURedeem_Filter_CardBank_Option = {
            placeholder: 'Sort by Card Bank',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TURedeem_Filter_CardBanks_Change(event: any) {
        if (event.value == this.TURedeem_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_CardBank_Selected, '=');
            this.TURedeem_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TURedeem_Config.SearchBaseConditions);
            this.TURedeem_Filter_CardBank_Selected = 0;
        }
        else if (event.value != this.TURedeem_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_CardBank_Selected, '=');
            this.TURedeem_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TURedeem_Config.SearchBaseConditions);
            this.TURedeem_Filter_CardBank_Selected = event.value;
            this.TURedeem_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TURedeem_Filter_CardBank_Selected, '='));
        }
        this.TURedeem_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

}
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon } from '../../../../service/service';
import swal from 'sweetalert2';
import { Options, LabelType, ChangeContext } from 'ng5-slider';

@Component({
    selector: 'tu-rewardclaim',
    templateUrl: './turewardclaim.component.html',
})
export class TURewardsClaimComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {
    }
    ngOnInit() {

        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
            } else {
                this._HelperService.Get_UserAccountDetails(true);
                this.TUTr_Setup();
                this.TUTr_Filter_UserAccounts_Load();
                this.TUTr_Filter_TransactionTypes_Load();
            }
        });



    }
    TUTr_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
    TUTr_InvoiceRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
    TUTr_InvoiceRange_OnChange(changeContext: ChangeContext): void {
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'InvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        this.TUTr_InvoiceRangeMinAmount = changeContext.value;
        this.TUTr_InvoiceRangeMaxAmount = changeContext.highValue;
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'InvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    TUTr_RewardClaimRangeMinAmount: number = this._HelperService.AppConfig.RangeRewardClaimAmountMinimumLimit;
    TUTr_RewardClaimRangeMaxAmount: number = this._HelperService.AppConfig.RangeRewardClaimAmountMaximumLimit;
    TUTr_RewardClaimRange_OnChange(changeContext: ChangeContext): void {
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalAmount', this.TUTr_RewardClaimRangeMinAmount, this.TUTr_RewardClaimRangeMaxAmount);
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        this.TUTr_RewardClaimRangeMinAmount = changeContext.value;
        this.TUTr_RewardClaimRangeMaxAmount = changeContext.highValue;
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalAmount', this.TUTr_RewardClaimRangeMinAmount, this.TUTr_RewardClaimRangeMaxAmount);
        if (this.TUTr_RewardClaimRangeMinAmount == this._HelperService.AppConfig.RangeRewardClaimAmountMinimumLimit && this.TUTr_RewardClaimRangeMaxAmount == this._HelperService.AppConfig.RangeRewardClaimAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }
    public TUTr_Config: OList;
    TUTr_Setup() {
        this.TUTr_Config =
            { Id: null, Sort: null,
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetRewardClaimTransctions,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore,
                Title: 'Reward Claim History',
                StatusType: 'transaction',
                Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
                StatusName:"Success",
                Type: this._HelperService.AppConfig.ListType.All,
                SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '='),
                DefaultSortExpression: 'TransactionDate desc',
                TableFields: [
                    {
                        DisplayName: '#',
                        SystemName: 'ReferenceId',
                        DataType: this._HelperService.AppConfig.DataType.Number,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Date',
                        SystemName: 'TransactionDate',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Show: true,
                        Search: false,
                        Sort: true,
                        IsDateSearchField: true,
                    },
                    {
                        DisplayName: 'Transaction Status',
                        SystemName: 'StatusName',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'User',
                        SystemName: 'UserDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'User Mobile Number',
                        SystemName: 'UserMobileNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Type',
                        SystemName: 'TypeName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Invoice Amount',
                        SystemName: 'InvoiceAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Show: true,
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'Claim Amount',
                        SystemName: 'TotalAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: 'text-grey',
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'TUC Card Number',
                        SystemName: 'TUCCardNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Merchant',
                        SystemName: 'ParentDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Store',
                        SystemName: 'SubParentDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Acquirer / Bank',
                        SystemName: 'AcquirerDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Terminal Provider',
                        SystemName: 'ProviderDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Transaction Issuer (Done by)',
                        SystemName: 'CreatedByDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Transaction Reference',
                        SystemName: 'ReferenceNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },


                ]
            }
        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this.TUTr_GetData();
    }
    TUTr_ToggleOption_Date(event: any, Type: any) {
        this.TUTr_ToggleOption(event, Type);
    }
    TUTr_ToggleOption(event: any, Type: any) {
        this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
        if (this.TUTr_Config.RefreshData == true) {
            this.TUTr_GetData();
        }
    }
    TUTr_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    }
    TUTr_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    }

    public TUTr_Filter_UserAccount_Option: Select2Options;
    public TUTr_Filter_UserAccount_Selected = 0;
    TUTr_Filter_UserAccounts_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "MobileNumber",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.AppUser
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_UserAccount_Option = {
            placeholder: 'Search Customer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_UserAccounts_Change(event: any) {
        if (event.value == this.TUTr_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_UserAccount_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_UserAccount_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TUTr_Filter_TransactionType_Option: Select2Options;
    public TUTr_Filter_TransactionType_Selected = 0;
    TUTr_Filter_TransactionTypes_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionType
                },
                {
                    SystemName: "SubParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionTypeReward
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_TransactionType_Option = {
            placeholder: 'Search By Tran. Type',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_TransactionTypes_Change(event: any) {
        if (event.value == this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

}
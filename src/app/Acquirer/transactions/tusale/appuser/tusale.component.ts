import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon } from '../../../../service/service';
import swal from 'sweetalert2';

@Component({
    selector: 'tu-sale',
    templateUrl: './tusale.component.html',
})
export class TUSaleComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {
    }
    ngOnInit() {
        this.TUSale_Filter_Merchants_Load();
        this.TUSale_Filter_Stores_Load();
        this.TUSale_Filter_Banks_Load();
        this.TUSale_Filter_Providers_Load();
        this.TUSale_Filter_Issuers_Load();
        this.TUSale_Filter_CardBrands_Load();
        this.TUSale_Filter_TransactionTypes_Load();
        this.TUSale_Filter_CardBanks_Load();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
                this.TUSale_Setup();
            }
            else {
                this._HelperService.Get_UserAccountDetails(true);
                this.TUSale_Setup();
            }
        });
    }

    public TUSale_Config: OList;
    TUSale_Setup() {
        this.TUSale_Config =
            {
                Id:null,
            Sort:null,
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetSaleTransactions,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore,
                Title: 'Sales History',
                StatusType: 'transaction',
                Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
                StatusName:"Success",
                Type: this._HelperService.AppConfig.ListType.All,
                DefaultSortExpression: 'TransactionDate desc',
                TableFields: [
                    {
                        DisplayName: 'User',
                        SystemName: 'UserAccountDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                    },
                    {
                        DisplayName: 'Mobile Number',
                        SystemName: 'UserAccountMobileNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                    },
                    {
                        DisplayName: 'Type',
                        SystemName: 'TypeName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },
                    {
                        DisplayName: 'Card Type',
                        SystemName: 'CardBrandName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },
                    {
                        DisplayName: 'Bank',
                        SystemName: 'CardBankName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: false,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Card',
                        SystemName: 'CardBinNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: false,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Invoice Amt',
                        SystemName: 'InvoiceAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                    },
                    {
                        DisplayName: 'Reward Amt',
                        SystemName: 'RewardAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: 'text-grey',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Date',
                        SystemName: 'TransactionDate',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: 'td-date',
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                        IsDateSearchField: true,
                    },
                    {
                        DisplayName: 'Store',
                        SystemName: 'SubParentDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Done By',
                        SystemName: 'CreatedByDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Ref',
                        SystemName: 'ReferenceNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: false,
                        Search: true,
                        Sort: false,
                        ResourceId: null,
                    },
                    {
                        DisplayName: 'Acquirer',
                        SystemName: 'AcquirerName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: false,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },
                    {
                        DisplayName: 'Provider',
                        SystemName: 'ProviderDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: false,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },
                ]
            }
        this.TUSale_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUSale_Config.SearchBaseCondition, 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');
        this.TUSale_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUSale_Config.SearchBaseCondition, 'ParentId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '=');

        this.TUSale_Config = this._DataHelperService.List_Initialize(this.TUSale_Config);
        this.TUSale_GetData();
    }
    TUSale_ToggleOption_Date(event: any, Type: any) {
        this.TUSale_ToggleOption(event, Type);
    }
    TUSale_ToggleOption(event: any, Type: any) {
        this.TUSale_Config = this._DataHelperService.List_Operations(this.TUSale_Config, event, Type);
        if (this.TUSale_Config.RefreshData == true) {
            this.TUSale_GetData();
        }
    }
    TUSale_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.TUSale_Config);
        this.TUSale_Config = TConfig;
    }
    TUSale_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    }

    public TUSale_Filter_Merchant_Option: Select2Options;
    public TUSale_Filter_Merchant_Selected = 0;
    TUSale_Filter_Merchants_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Merchant
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUSale_Filter_Merchant_Option = {
            placeholder: 'Filter by Merchant',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUSale_Filter_Merchants_Change(event: any) {
        if (event.value == this.TUSale_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_Merchant_Selected, '=');
            this.TUSale_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUSale_Config.SearchBaseConditions);
            this.TUSale_Filter_Merchant_Selected = 0;
        }
        else if (event.value != this.TUSale_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_Merchant_Selected, '=');
            this.TUSale_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUSale_Config.SearchBaseConditions);
            this.TUSale_Filter_Merchant_Selected = event.value;
            this.TUSale_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_Merchant_Selected, '='));
        }
        this.TUSale_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TUSale_Filter_Store_Option: Select2Options;
    public TUSale_Filter_Store_Selected = 0;
    TUSale_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Store
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUSale_Filter_Store_Option = {
            placeholder: 'Filter by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUSale_Filter_Stores_Change(event: any) {
        if (event.value == this.TUSale_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_Store_Selected, '=');
            this.TUSale_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUSale_Config.SearchBaseConditions);
            this.TUSale_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TUSale_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_Store_Selected, '=');
            this.TUSale_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUSale_Config.SearchBaseConditions);
            this.TUSale_Filter_Store_Selected = event.value;
            this.TUSale_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_Store_Selected, '='));
        }
        this.TUSale_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }
    public TUSale_Filter_Bank_Option: Select2Options;
    public TUSale_Filter_Bank_Selected = 0;
    TUSale_Filter_Banks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUSale_Filter_Bank_Option = {
            placeholder: 'Sort by Bank',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUSale_Filter_Banks_Change(event: any) {
        if (event.value == this.TUSale_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_Bank_Selected, '=');
            this.TUSale_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUSale_Config.SearchBaseConditions);
            this.TUSale_Filter_Bank_Selected = 0;
        }
        else if (event.value != this.TUSale_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_Bank_Selected, '=');
            this.TUSale_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUSale_Config.SearchBaseConditions);
            this.TUSale_Filter_Bank_Selected = event.value;
            this.TUSale_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_Bank_Selected, '='));
        }
        this.TUSale_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TUSale_Filter_Provider_Option: Select2Options;
    public TUSale_Filter_Provider_Selected = 0;
    TUSale_Filter_Providers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUSale_Filter_Provider_Option = {
            placeholder: 'Filter by PTSP',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUSale_Filter_Providers_Change(event: any) {
        if (event.value == this.TUSale_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_Provider_Selected, '=');
            this.TUSale_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUSale_Config.SearchBaseConditions);
            this.TUSale_Filter_Provider_Selected = 0;
        }
        else if (event.value != this.TUSale_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_Provider_Selected, '=');
            this.TUSale_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUSale_Config.SearchBaseConditions);
            this.TUSale_Filter_Provider_Selected = event.value;
            this.TUSale_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_Provider_Selected, '='));
        }
        this.TUSale_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TUSale_Filter_Issuer_Option: Select2Options;
    public TUSale_Filter_Issuer_Selected = 0;
    TUSale_Filter_Issuers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
            ]
        };
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
            [
                this._HelperService.AppConfig.AccountType.PosTerminal,
                this._HelperService.AppConfig.AccountType.PGAccount,
                this._HelperService.AppConfig.AccountType.Cashier,

            ], '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUSale_Filter_Issuer_Option = {
            placeholder: 'Sort by Issuer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUSale_Filter_Issuers_Change(event: any) {
        if (event.value == this.TUSale_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_Issuer_Selected, '=');
            this.TUSale_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUSale_Config.SearchBaseConditions);
            this.TUSale_Filter_Issuer_Selected = 0;
        }
        else if (event.value != this.TUSale_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_Issuer_Selected, '=');
            this.TUSale_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUSale_Config.SearchBaseConditions);
            this.TUSale_Filter_Issuer_Selected = event.value;
            this.TUSale_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_Issuer_Selected, '='));
        }
        this.TUSale_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TUSale_Filter_TransactionType_Option: Select2Options;
    public TUSale_Filter_TransactionType_Selected = 0;
    TUSale_Filter_TransactionTypes_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionType
                },

            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUSale_Filter_TransactionType_Option = {
            placeholder: 'Sort by Transaction Type',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUSale_Filter_TransactionTypes_Change(event: any) {
        if (event.value == this.TUSale_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_TransactionType_Selected, '=');
            this.TUSale_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUSale_Config.SearchBaseConditions);
            this.TUSale_Filter_TransactionType_Selected = 0;
        }
        else if (event.value != this.TUSale_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_TransactionType_Selected, '=');
            this.TUSale_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUSale_Config.SearchBaseConditions);
            this.TUSale_Filter_TransactionType_Selected = event.value;
            this.TUSale_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_TransactionType_Selected, '='));
        }
        this.TUSale_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TUSale_Filter_CardBrand_Option: Select2Options;
    public TUSale_Filter_CardBrand_Selected = 0;
    TUSale_Filter_CardBrands_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "TypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.CardBrand
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUSale_Filter_CardBrand_Option = {
            placeholder: 'Sort by Card Brand',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUSale_Filter_CardBrands_Change(event: any) {
        if (event.value == this.TUSale_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_CardBrand_Selected, '=');
            this.TUSale_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUSale_Config.SearchBaseConditions);
            this.TUSale_Filter_CardBrand_Selected = 0;
        }
        else if (event.value != this.TUSale_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_CardBrand_Selected, '=');
            this.TUSale_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUSale_Config.SearchBaseConditions);
            this.TUSale_Filter_CardBrand_Selected = event.value;
            this.TUSale_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_CardBrand_Selected, '='));
        }
        this.TUSale_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TUSale_Filter_CardBank_Option: Select2Options;
    public TUSale_Filter_CardBank_Selected = 0;
    TUSale_Filter_CardBanks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "TypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.CardBank
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUSale_Filter_CardBank_Option = {
            placeholder: 'Sort by Card Bank',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUSale_Filter_CardBanks_Change(event: any) {
        if (event.value == this.TUSale_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_CardBank_Selected, '=');
            this.TUSale_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUSale_Config.SearchBaseConditions);
            this.TUSale_Filter_CardBank_Selected = 0;
        }
        else if (event.value != this.TUSale_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_CardBank_Selected, '=');
            this.TUSale_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUSale_Config.SearchBaseConditions);
            this.TUSale_Filter_CardBank_Selected = event.value;
            this.TUSale_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUSale_Filter_CardBank_Selected, '='));
        }
        this.TUSale_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

}
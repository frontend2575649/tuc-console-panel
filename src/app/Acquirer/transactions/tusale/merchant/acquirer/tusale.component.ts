import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { DataHelperService, HelperService, OList, OSelect, OResponse, FilterHelperService } from '../../../../../service/service';
import { Observable } from 'rxjs';
declare var moment: any;
declare var $: any;
import swal from 'sweetalert2';

@Component({
    selector: 'tu-sale',
    templateUrl: './tusale.component.html',
})
export class TUSaleComponent implements OnInit {

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _FilterHelperService: FilterHelperService,

    ) {
        this._HelperService.ShowDateRange = false;
    }
    ngOnInit() {

        Feather.replace();
        this._HelperService.StopClickPropogation();

        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
            } else {
                this._HelperService.Get_UserAccountDetails(true);

                this.TodayStartTime = new Date(2017, 0, 1, 0, 0, 0, 0), moment();
                this.TodayEndTime = moment();
                //#region FilterInit 

                this.TUTr_Filter_UserAccounts_Load();
                this.TUTr_Filter_Stores_Load();
                this.TUTr_Filter_Banks_Load();
                this.TUTr_Filter_Providers_Load();
                this.TUTr_Filter_Issuers_Load();
                this.TUTr_Filter_CardBrands_Load();
                this.TUTr_Filter_TransactionTypes_Load();
                this.TUTr_Filter_CardBanks_Load();

                //#endregion
                this.TUTr_Setup();
                // this._HelperService.GetSubAccountOverviewLite(moment(this.TodayStartTime), moment(this.TodayEndTime));

            }
        });
    }

    //#region Transactions 

    public TUTr_Config: OList;
    TUTr_Setup() {
        this.TUTr_Config =
        {
            Id: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetSaleTransactions,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore,
            Title: 'Sales History',
            StatusType: 'transaction',
            Status: this._HelperService.AppConfig.StatusList.transactiondefaultitemCode,
            StatusName:"Success",
            Type: this._HelperService.AppConfig.ListType.All,
            Sort:
            {
                SortDefaultName: null,
                SortDefaultColumn: 'TransactionDate',
                SortName: null,
                SortColumn: null,
                SortOrder: 'desc',
                SortOptions: [],
            },
            TableFields: [
                {
                    DisplayName: '#',
                    SystemName: 'ReferenceId',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Date',
                    SystemName: 'TransactionDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Show: true,
                    Search: false,
                    Sort: true,
                    IsDateSearchField: true,
                },
                {
                    DisplayName: 'Transaction Status',
                    SystemName: 'StatusName',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Customer',
                    SystemName: 'UserDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: true,
                },
                {
                    DisplayName: 'User Mobile Number',
                    SystemName: 'UserMobileNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Type',
                    SystemName: 'TypeName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Amount',
                    SystemName: 'InvoiceAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Show: true,
                    Search: false,
                    Sort: true,
                },
                {
                    DisplayName: 'Reward Amount',
                    SystemName: 'RewardAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Class: 'text-grey',
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'User Reward Amount',
                    SystemName: 'UserAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Commission Amount',
                    SystemName: 'CommissionAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Card Number',
                    SystemName: 'AccountNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Card Type',
                    SystemName: 'CardBrandName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Card Bank Provider',
                    SystemName: 'CardBankName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'TUC Card Number',
                    SystemName: 'TUCCardNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Merchant',
                    SystemName: 'ParentDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Store',
                    SystemName: 'SubParentDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Acquirer / Bank',
                    SystemName: 'AcquirerDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Terminal Provider',
                    SystemName: 'ProviderDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Transaction Issuer (Done by)',
                    SystemName: 'CreatedByDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Transaction Reference',
                    SystemName: 'ReferenceNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: false,
                    ResourceId: null,
                },


            ]
        };
            this.TUTr_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUTr_Config.SearchBaseCondition, 'ParentId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '='),
            this.TUTr_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUTr_Config.SearchBaseCondition, 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '='),

            this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
            this._HelperService.Active_FilterInit(
                this._HelperService.AppConfig.FilterTypeOption.MerchantSales,
                this.TUTr_Config
            );
        this.TUTr_GetData();
    }
    TUTr_ToggleOption_Date(event: any, Type: any) {
        this.TUTr_ToggleOption(event, Type);
    }
    TUTr_ToggleOption(event: any, Type: any) {
        this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

        this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
        // this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

        this.TodayStartTime = this.TUTr_Config.StartTime;
        this.TodayEndTime = this.TUTr_Config.EndTime;
        if (event != null) {
            for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                const element = this.TUTr_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {

                    element.SystemActive = true;



                }

                else {
                    element.SystemActive = false;

                }

            }
        }


        if (
            (this.TUTr_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.TUTr_GetData();
        }
    }

    timeout = null;
    TUTr_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {

            if (event != null) {
                for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                    const element = this.TUTr_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {
    
                        element.SystemActive = true;
                    }
                    else {
                        element.SystemActive = false;
                    }
                }
            }
    
            this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
            if (this.TUTr_Config.RefreshData == true) {
                this.TUTr_GetData();
            }
            this._HelperService.StopClickPropogation();
        }, this._HelperService.AppConfig.SearchInputDelay);

    }

    TUTr_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getsaletransactionsoverview);
        this.TUTr_Config = TConfig;
    }
    TUTr_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    }
    TodayStartTime = null;
    TodayEndTime = null;

    public _AccountOverview: OAccountOverview =
        {
            ActiveMerchants: 0,
            ActiveMerchantsDiff: 0,
            ActiveTerminals: 0,
            Idle: 0,
            Dead: 0,
            Active: 0,
            Inactive: 0,
            ActiveTerminalsDiff: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardPurchaseAmountDiff: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardPurchaseAmountDiff: 0,
            Merchants: 0,
            PurchaseAmount: 0,
            PurchaseAmountDiff: 0,
            Terminals: 0,
            Transactions: 0,
            TransactionsDiff: 0,
            UnusedTerminals: 0,
            IdleTerminals: 0,
            TerminalStatus: 0,
            DeadTerminals: 0,
            Total: 0,
            InvoiceAmount : 0
        }
    // GetAccountOverviewLite() {
    //     this._HelperService.IsFormProcessing = true;
    //     var Data = {
    //         Task: 'getaccountoverview',
    //         StartTime: this.TodayStartTime,
    //         EndDate: this.TodayEndTime,
    //         AccountKey: this._HelperService.UserAccount.AccountKey,
    //         AccountId: this._HelperService.UserAccount.AccountId,
    //         SubAccountId: this._HelperService.AppConfig.ActiveReferenceId,
    //         SubAccountKey: this._HelperService.AppConfig.ActiveReferenceKey,

    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
    //     _OResponse.subscribe(
    //         _Response => {
    //             this._HelperService.IsFormProcessing = false;
    //             if (_Response.Status == this._HelperService.StatusSuccess) {
    //                 this._AccountOverview = _Response.Result as OAccountOverview;

    //                 //#region calculatetype 

    //                 this._AccountOverview['CardTransactionsPerc'] = (this._HelperService.DivideTwoNumbers(this._AccountOverview['CardTransactions'], this._AccountOverview['TotalTransactions'])) * 100;
    //                 this._AccountOverview['CashTransactionsPerc'] = (this._HelperService.DivideTwoNumbers(this._AccountOverview['CashTransactions'], this._AccountOverview['TotalTransactions'])) * 100;


    //                 //#endregion

    //                 //#region calculatetermperc 

    //                 if (this._AccountOverview.TerminalStatus['Total'] != 0) {
    //                     var _TempVal = this._HelperService.DivideTwoNumbers(100, this._AccountOverview.TerminalStatus['Total']);
    //                     this._AccountOverview["IdleTerminalsPerc"] = this._AccountOverview.TerminalStatus['Idle'] * _TempVal;
    //                     this._AccountOverview["ActiveTerminalsPerc"] = this._AccountOverview.TerminalStatus['Active'] * _TempVal;
    //                     this._AccountOverview["DeadTerminalsPerc"] = this._AccountOverview.TerminalStatus['Dead'] * _TempVal;
    //                     this._AccountOverview["UnusedTerminalsPerc"] = this._AccountOverview.TerminalStatus['Inactive'] * _TempVal;
    //                 } else {
    //                     this._AccountOverview["IdleTerminalsPerc"] = 0;
    //                     this._AccountOverview["ActiveTerminalsPerc"] = 0;
    //                     this._AccountOverview["DeadTerminalsPerc"] = 0;
    //                     this._AccountOverview["UnusedTerminalsPerc"] = 0;
    //                 }


    //             }
    //             else {
    //                 this._HelperService.NotifyError(_Response.Message);
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.IsFormProcessing = false;
    //             this._HelperService.HandleException(_Error);
    //         });
    // }
    //#endregion

    SetOtherFilters(): void {
        this.TUTr_Config.SearchBaseConditions = [];
       // this.TUTr_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '==');
       this.TUTr_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '==');
       this.TUTr_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUTr_Config.SearchBaseCondition, 'ParentId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '=');

        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_Merchant_Selected = 0;
            this.MerchantEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Provider));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_Provider_Selected = 0;
            this.ProvidersEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Store));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_Store_Selected = 0;
            this.StoresEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.TransactionType));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_TransactionType_Selected = 0;
            this.TransTypeEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBank));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_CardBank_Selected = 0;
            this.CardBankEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBrand));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_CardBrand_Selected = 0;
            this.CardBrandEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }

    //#region dropdowns 

    //#region merchant 

    public TUTr_Filter_Merchant_Option: Select2Options;
    public TUTr_Filter_Merchant_Selected = 0;
    TUTr_Filter_Merchants_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Account,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: "AccountTypeCode",
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: "=",
                //     SearchValue: this._HelperService.AppConfig.AccountType.Merchant
                // }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Merchant_Option = {
            placeholder: 'Search By Merchant',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_Merchants_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant
        );
        this.MerchantEventProcessing(event);
    }

    MerchantEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Merchant_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Merchant_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Store_Toggle = true;

        this.TUTr_Filter_Stores_Load();
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
            this.TUTr_Filter_Store_Toggle = false;
        }, 500);
    }

    //#endregion

    //#region stores 

    public TUTr_Filter_Store_Option: Select2Options;
    public TUTr_Filter_Store_Toggle = false;
    public TUTr_Filter_Store_Selected = 0;
    TUTr_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
              {
                SystemName: "ReferenceId",
                Type: this._HelperService.AppConfig.DataType.Number,
                Id: true,
                Text: false,
              },
              {
                SystemName: "DisplayName",
                Type: this._HelperService.AppConfig.DataType.Text,
                Id: false,
                Text: true
              },
              {
                SystemName: "AccountTypeCode",
                Type: this._HelperService.AppConfig.DataType.Text,
                SearchCondition: "=",
                SearchValue: this._HelperService.AppConfig.AccountType.Store
              }
            ]
          };
          _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '=');
      

        if (this.TUTr_Filter_Merchant_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
        }
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Store_Option = {
            placeholder: 'Search by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Stores_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Store
        );
        this.StoresEventProcessing(event);
    }

    StoresEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion

    //#region useraccount 

    public TUTr_Filter_UserAccount_Option: Select2Options;
    public TUTr_Filter_UserAccount_Selected = 0;
    TUTr_Filter_UserAccounts_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "MobileNumber",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.AppUser
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_UserAccount_Option = {
            placeholder: 'Search Customer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_UserAccounts_Change(event: any) {
        if (event.value == this.TUTr_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_UserAccount_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_UserAccount_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region bank 

    public TUTr_Filter_Bank_Option: Select2Options;
    public TUTr_Filter_Bank_Selected = 0;
    TUTr_Filter_Banks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
                }]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Bank_Option = {
            placeholder: 'Search By Bank / Acquirer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Banks_Change(event: any) {
        if (event.value == this.TUTr_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Bank_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Bank_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion

    //#region provider 

    public TUTr_Filter_Provider_Option: Select2Options;
    public TUTr_Filter_Provider_Selected = 0;
    TUTr_Filter_Providers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Provider_Option = {
            placeholder: 'Search By Provider',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Providers_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Provider
        );

        this.ProvidersEventProcessing(event);
    }

    ProvidersEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Provider_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Provider_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion

    //#region issuers 

    public TUTr_Filter_Issuer_Option: Select2Options;
    public TUTr_Filter_Issuer_Selected = 0;
    TUTr_Filter_Issuers_Load() {
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }]
        };


        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
            [
                this._HelperService.AppConfig.AccountType.PosTerminal,
                this._HelperService.AppConfig.AccountType.PGAccount,
                this._HelperService.AppConfig.AccountType.Cashier,
            ], "=");

        if (this.TUTr_Filter_Provider_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
        }
        if (this.TUTr_Filter_Bank_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'BankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'SubOwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictOr(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Issuer_Option = {
            placeholder: 'Search By Issuer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Issuers_Change(event: any) {
        if (event.value == this.TUTr_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Issuer_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Issuer_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region transactionType 

    public TUTr_Filter_TransactionType_Option: Select2Options;
    public TUTr_Filter_TransactionType_Selected = 0;
    TUTr_Filter_TransactionTypes_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionType
                },
                {
                    SystemName: "SubParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionTypeReward
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_TransactionType_Option = {
            placeholder: 'Search By Tran. Type',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_TransactionTypes_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.TransactionType
        );

        this.TransTypeEventProcessing(event);
    }

    TransTypeEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region cardbrands 

    public TUTr_Filter_CardBrand_Option: Select2Options;
    public TUTr_Filter_CardBrand_Selected = 0;
    TUTr_Filter_CardBrands_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "TypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.CardBrand
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_CardBrand_Option = {
            placeholder: 'Search By Card Brand',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_CardBrands_Change(event: any) {

        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBrand
        );
        this.CardBrandEventProcessing(event);
    }

    CardBrandEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBrand_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBrand_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region cardbank 

    public TUTr_Filter_CardBank_Option: Select2Options;
    public TUTr_Filter_CardBank_Selected = 0;
    TUTr_Filter_CardBanks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields:
                [
                    {
                        SystemName: "ReferenceId",
                        Type: this._HelperService.AppConfig.DataType.Number,
                        Id: true,
                        Text: false,
                    },
                    {
                        SystemName: "Name",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        Id: false,
                        Text: true
                    },
                    {
                        SystemName: "TypeCode",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        SearchCondition: "=",
                        SearchValue: this._HelperService.AppConfig.HelperTypes.CardBank
                    }
                ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_CardBank_Option = {
            placeholder: 'Search By Card Bank',
            ajax: _Transport,
            multiple: false,
            allowClear: true,

        };
    }

    TUTr_Filter_CardBanks_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBank
        );

        this.CardBankEventProcessing(event);
    }

    CardBankEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBank_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBank_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#endregion
    public ResetFilterControls: boolean = true;
    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();

        this.TUTr_Filter_Merchants_Load();
        this.TUTr_Filter_UserAccounts_Load();
        this.TUTr_Filter_Stores_Load();
        this.TUTr_Filter_Providers_Load();
        this.TUTr_Filter_Issuers_Load();
        this.TUTr_Filter_CardBrands_Load();
        this.TUTr_Filter_TransactionTypes_Load();
        this.TUTr_Filter_CardBanks_Load();

        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();

    }
    //#region filterOperations

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetMerchantSalesConfig(this.TUTr_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion

        this.TUTr_GetData();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_MerchantSales(Type, index);
        this._FilterHelperService.SetMerchantSalesConfig(this.TUTr_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion
        this.TUTr_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            input: "text",
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            inputAttributes: {
                autocapitalize: "off",
                autocorrect: "off",
                //maxLength: "4",
                minLength: "4",
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_MerchantSales(result.value);
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.MerchantSales
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._HelperService.Delete_Filter(
                    this._HelperService.AppConfig.FilterTypeOption.MerchantSales
                );
                this._FilterHelperService.SetMerchantSalesConfig(this.TUTr_Config);
                this.TUTr_GetData();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        this._HelperService.MakeFilterSnapPermanent();
        this.TUTr_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();

        if (ButtonType == 'Sort') {
            $("#TUTr_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#TUTr_fdropdown").dropdown('toggle');
        }
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetMerchantSalesConfig(this.TUTr_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion
        this.TUTr_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    GetOverviews(ListOptions: any, Task: string): any {
        this._HelperService.IsFormProcessing = true;

        ListOptions.SearchCondition = '';
        ListOptions = this._DataHelperService.List_GetData(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }


        var pData = {
            Task: Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveReferenceId,
            AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._AccountOverview = _Response.Result as any;

                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);

            });


    }

    //#endregion
}

export class OAccountOverview {

    public DeadTerminals?: number;
    public IdleTerminals?: number;
    public TerminalStatus?: number;
    public Total?: number;
    public Idle?: number;
    public Active?: number;
    public Dead?: number;
    public Inactive?: number;
    public UnusedTerminals?: number;
    public Merchants: number;
    public ActiveMerchants: number;
    public ActiveMerchantsDiff: number;
    public Terminals: number;
    public ActiveTerminals: number;
    public ActiveTerminalsDiff: number;
    public Transactions: number;
    public TransactionsDiff: number;
    public PurchaseAmount: number;
    public PurchaseAmountDiff: number;
    public CashRewardPurchaseAmount: number;
    public CashRewardPurchaseAmountDiff: number;
    public CardRewardPurchaseAmount: number;
    public CardRewardPurchaseAmountDiff: number;
    public InvoiceAmount? :number;
}
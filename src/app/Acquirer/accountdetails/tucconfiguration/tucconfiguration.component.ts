import { ChangeDetectorRef, Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BaseChartDirective, Label } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OList, OResponse, OSelect } from '../../../service/service';
declare var moment: any;
declare var $: any;
import * as pluginEmptyOverlay from "chartjs-plugin-empty-overlay";
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import swal from 'sweetalert2';

@Component({
  selector: 'tucconfiguration',
  templateUrl: './tucconfiguration.component.html',
  styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUCAcqConfigurationComponent implements OnInit {

  public TodayDate: any;

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
  }

  ngOnInit() {

    this._HelperService.FullContainer = false;
    this._HelperService.FullContainer = false;
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];

      if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
        this._Router.navigate([
          this._HelperService.AppConfig.Pages.System.NotFound
        ]);
      } else {
        this._HelperService.ResetDateRange();
        this.GetAccountDetails();
        this.Form_UpdateUser_Load();
      }
    });



  }






  public _UserAccount: any =
    {
      MerchantDisplayName: null,
      SecondaryEmailAddress: null,
      BankDisplayName: null,
      BankKey: null,
      OwnerName: null,
      SubOwnerAddress: null,
      SubOwnerLatitude: null,
      SubOwnerDisplayName: null,
      SubOwnerKey: null,
      SubOwnerLongitude: null,
      AccessPin: null,
      LastLoginDateS: null,
      AppKey: null,
      AppName: null,
      AppVersionKey: null,
      CreateDate: null,
      CreateDateS: null,
      CreatedByDisplayName: null,
      CreatedByIconUrl: null,
      CreatedByKey: null,
      Description: null,
      IconUrl: null,
      ModifyByDisplayName: null,
      ModifyByIconUrl: null,
      ModifyByKey: null,
      ModifyDate: null,
      ModifyDateS: null,
      PosterUrl: null,
      ReferenceKey: null,
      StatusCode: null,
      StatusI: null,
      StatusId: null,
      StatusName: null,
      AccountCode: null,
      AccountOperationTypeCode: null,
      AccountOperationTypeName: null,
      AccountTypeCode: null,
      AccountTypeName: null,
      Address: null,
      AppVersionName: null,
      ApplicationStatusCode: null,
      ApplicationStatusName: null,
      AverageValue: null,
      CityAreaKey: null,
      CityAreaName: null,
      CityKey: null,
      CityName: null,
      ContactNumber: null,
      CountValue: null,
      CountryKey: null,
      CountryName: null,
      DateOfBirth: null,
      DisplayName: null,
      EmailAddress: null,
      EmailVerificationStatus: null,
      EmailVerificationStatusDate: null,
      FirstName: null,
      GenderCode: null,
      GenderName: null,
      LastLoginDate: null,
      LastName: null,
      Latitude: null,
      Longitude: null,
      MobileNumber: null,
      Name: null,
      NumberVerificationStatus: null,
      NumberVerificationStatusDate: null,
      OwnerDisplayName: null,
      OwnerKey: null,
      Password: null,
      Reference: null,
      ReferralCode: null,
      ReferralUrl: null,
      RegionAreaKey: null,
      RegionAreaName: null,
      RegionKey: null,
      RegionName: null,
      RegistrationSourceCode: null,
      RegistrationSourceName: null,
      RequestKey: null,
      RoleKey: null,
      RoleName: null,
      SecondaryPassword: null,
      SystemPassword: null,
      UserName: null,
      WebsiteUrl: null,

      StateKey: null,
      StateName: null,


      custimportonboardallowsms: null,
      custimportonboardmessage: null,

      infobip_url: null,
      infobip_key: null,
      infobip_senderid: null,


    }



  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
    //    this._ChangeDetectorRef.detectChanges();
  }
  public _Address: any = {};
  public _ContactPerson: any = {};
  public _Overview: any = {};

  GetAccountDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: "getaccountconfigurations",
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,
      Accountkey: this._HelperService.AppConfig.ActiveReferenceKey,
      Offset: 0,
      Limit: 200,
      // Reference: this._HelperService.GetSearchConditionStrict(
      //   "",
      //   "ReferenceKey",
      //   this._HelperService.AppConfig.DataType.Text,
      //   this._HelperService.AppConfig.ActiveReferenceKey,
      //   "="
      // ),
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data.length > 0) {
            var DataItem = _Response.Result.Data as any[];
            if (DataItem.find(x => x.SystemName == 'custimportonboardallowsms') != undefined) {
              this._UserAccount.custimportonboardallowsms = DataItem.find(x => x.SystemName == 'custimportonboardallowsms').Value;
            }
            if (DataItem.find(x => x.SystemName == 'custimportonboardmessage') != undefined) {
              this._UserAccount.custimportonboardmessage = DataItem.find(x => x.SystemName == 'custimportonboardmessage').Value;
            }
            if (DataItem.find(x => x.SystemName == 'infobip_url') != undefined) {
              this._UserAccount.infobip_url = DataItem.find(x => x.SystemName == 'infobip_url').Value;
            }
            if (DataItem.find(x => x.SystemName == 'infobip_key') != undefined) {
              this._UserAccount.infobip_key = DataItem.find(x => x.SystemName == 'infobip_key').Value;
            }
            if (DataItem.find(x => x.SystemName == 'infobip_senderid') != undefined) {
              this._UserAccount.infobip_senderid = DataItem.find(x => x.SystemName == 'infobip_senderid').Value;
            }
           
          }
          this.toogleIsFormProcessing(false);
         

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


  Form_UpdateUser: FormGroup;
  Form_UpdateUser_Address: string = null;
  Form_UpdateUser_Latitude: number = 0;
  Form_UpdateUser_Longitude: number = 0;
  @ViewChild('places') places: GooglePlaceDirective;
  Form_UpdateUser_PlaceMarkerClick(event) {
    this.Form_UpdateUser_Latitude = event.coords.lat;
    this.Form_UpdateUser_Longitude = event.coords.lng;
  }
  public Form_UpdateUser_AddressChange(address: Address) {
    this.Form_UpdateUser_Latitude = address.geometry.location.lat();
    this.Form_UpdateUser_Longitude = address.geometry.location.lng();
    this.Form_UpdateUser_Address = address.formatted_address;
  }
  Form_UpdateUser_Show() {
  }
  Form_UpdateUser_Close() {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AccountSettings.Acquirer]);
  }
  Form_UpdateUser_Load() {
    this._HelperService._Icon_Cropper_Data.Width = 128;
    this._HelperService._Icon_Cropper_Data.Height = 128;
    this.Form_UpdateUser = this._FormBuilder.group({
      Task: 'updateaccountconfigurationvalues',
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,
      Accountkey: this._HelperService.AppConfig.ActiveReferenceKey,
      infobip_senderid: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
      infobip_key: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      infobip_url: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(1024)])],
      custimportonboardmessage: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(100)])],

    });
  }
  Form_UpdateUser_Clear() {
    this._HelperService.ToggleField = true;
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 300);
    this.Form_UpdateUser_Latitude = 0;
    this.Form_UpdateUser_Longitude = 0;
    this.Form_UpdateUser.reset();
    this._HelperService.Icon_Crop_Clear();
    this.Form_UpdateUser_Load();
  }
  Form_UpdateUser_Process(_FormValue: any) {
    swal({
      position: 'top',
      title: "Update configuration ?",
      text: "Enter your pin to update configuration",
      input: 'password',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
      inputValidator: function (value) {
        if (value === '' || value.length < 4) {
          return 'Enter your 4 digit pin!'
        }
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {

      if (result.value) {
        var Conf = [
          {
            ConfigurationKey: 'custimportonboardmessage',
            Value: this._UserAccount.custimportonboardmessage,
          },
          {
            ConfigurationKey: 'infobip_url',
            Value: this._UserAccount.infobip_url,
          },
          {
            ConfigurationKey: 'infobip_key',
            Value: this._UserAccount.infobip_key,
          },
          {
            ConfigurationKey: 'infobip_senderid',
            Value: this._UserAccount.infobip_senderid,
          },
        ];
        _FormValue.AccountId = this._HelperService.AppConfig.ActiveReferenceId;
        _FormValue.Accountkey = this._HelperService.AppConfig.ActiveReferenceKey;
        _FormValue.Configurations = Conf;
        _FormValue.AuthPin = result.value;
        _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveReferenceKey;
        _FormValue.StatusCode = this._UserAccount.StatusCode;
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, _FormValue);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess(_Response.Message);
              this._HelperService.Get_UserAccountDetails(true);
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          });
      }
    });



  }



}


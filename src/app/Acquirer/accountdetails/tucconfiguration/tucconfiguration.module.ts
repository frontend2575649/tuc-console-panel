import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AgmOverlays } from 'agm-overlays';
import { ChartsModule } from 'ng2-charts';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { TUCAcqConfigurationComponent } from './tucconfiguration.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { DynamicRoutesguardGuard } from 'src/app/service/guard/dynamicroutes.guard';


const routes: Routes = [
    { path: '',canActivateChild:[DynamicRoutesguardGuard],data:{accessName:['acqconfiguration']}, component: TUCAcqConfigurationComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUDashboardRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUDashboardRoutingModule,
        AgmOverlays,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        ChartsModule,
        ImageCropperModule,

    ],
    declarations: [TUCAcqConfigurationComponent]
})
export class TUCAcqConfigurationModuleModule {

} 

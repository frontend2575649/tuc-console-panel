import { ChangeDetectorRef, Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
import { BaseChartDirective } from 'ng2-charts';
declare var moment: any;
declare var $: any;


@Component({
    selector: 'tu-tuanalyticsreferal',
    templateUrl: './tuanalyticsreferal.component.html',
})
export class TUAnalyticsReferalComponent implements OnInit {
    showDailyChart: boolean = true;
    Piechartlable = ['Unused Codes', 'Used Codes', 'Total Code Qty'];
    public TodayDate: any;
    Piedata = ['0', '0', '0']
    public dougnut: any = {
        type: 'doughnut',
        data: {
            labels: ["Red", "Orange", "Green"],
            datasets: [{
                label: '# of Votes',
                data: [33, 33, 33],
                backgroundColor: [
                    'rgba(231, 76, 60, 1)',
                    'rgba(255, 164, 46, 1)',
                    'rgba(46, 204, 113, 1)'
                ],
                borderColor: [
                    'rgba(255, 255, 255 ,1)',
                    'rgba(255, 255, 255 ,1)',
                    'rgba(255, 255, 255 ,1)'
                ],
                borderWidth: 5
            }]

        },
        options: {
            rotation: 1 * Math.PI,
            circumference: 1 * Math.PI,
            legend: {
                display: false
            },
            tooltip: {
                enabled: false
            },
            cutoutPercentage: 95
        }
    }
    @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {
    }

    ngOnInit() {
        this.TodayStartTime = this._HelperService.AppConfig.DefaultStartTimeAll;
        this.TodayEndTime = this._HelperService.AppConfig.DefaultEndTimeToday;
        this._HelperService.FullContainer = false;
        this._HelperService.FullContainer = false;

        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this.pData.ReferenceKey = params["referencekey"];

            this.pDealPurchaseData.ReferenceKey = params["referencekey"];

            this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
            this.pData.ReferenceId = params["referenceid"];

            this.pDealPurchaseData.ReferenceId = params["referenceid"];


            this._HelperService.AppConfig.ActiveAccountKey = params["accountkey"];
            this.pData.AccountKey = params["accountkey"];

            this.pDealPurchaseData.AccountKey = params["accountkey"];


            this._HelperService.AppConfig.ActiveAccountId = params["accountid"];
            this.pData.AccountId = params["accountid"];

            this.pDealPurchaseData.AccountId = params["accountid"];

            if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
                this._Router.navigate([
                    this._HelperService.AppConfig.Pages.System.NotFound
                ]);
            } else {
                //this._HelperService.ResetDateRange();
                // this.GetAccountDetails();
                // this.PurchaseHistory_Setup();
                // this.GetSalesOverview();
                this.GetPromocodeDetails()
                this.ReviewHistory_Setup();
            }
        });
    }


    public _UserAccount: any =
        {
            MerchantDisplayName: null,
            SecondaryEmailAddress: null,
            BankDisplayName: null,
            BankKey: null,
            OwnerName: null,
            SubOwnerAddress: null,
            SubOwnerLatitude: null,
            SubOwnerDisplayName: null,
            SubOwnerKey: null,
            SubOwnerLongitude: null,
            AccessPin: null,
            LastLoginDateS: null,
            AppKey: null,
            AppName: null,
            AppVersionKey: null,
            CreateDate: null,
            CreateDateS: null,
            CreatedByDisplayName: null,
            CreatedByIconUrl: null,
            CreatedByKey: null,
            Description: null,
            IconUrl: null,
            ModifyByDisplayName: null,
            ModifyByIconUrl: null,
            ModifyByKey: null,
            ModifyDate: null,
            ModifyDateS: null,
            PosterUrl: null,
            ReferenceKey: null,
            StatusCode: null,
            StatusI: null,
            StatusId: null,
            StatusName: null,
            AccountCode: null,
            AccountOperationTypeCode: null,
            AccountOperationTypeName: null,
            AccountTypeCode: null,
            AccountTypeName: null,
            Address: null,
            AppVersionName: null,
            ApplicationStatusCode: null,
            ApplicationStatusName: null,
            AverageValue: null,
            CityAreaKey: null,
            CityAreaName: null,
            CityKey: null,
            CityName: null,
            ContactNumber: null,
            CountValue: null,
            CountryKey: null,
            CountryName: null,
            DateOfBirth: null,
            DisplayName: null,
            EmailAddress: null,
            EmailVerificationStatus: null,
            EmailVerificationStatusDate: null,
            FirstName: null,
            GenderCode: null,
            GenderName: null,
            LastLoginDate: null,
            LastName: null,
            Latitude: null,
            Longitude: null,
            MobileNumber: null,
            Name: null,
            NumberVerificationStatus: null,
            NumberVerificationStatusDate: null,
            OwnerDisplayName: null,
            OwnerKey: null,
            Password: null,
            Reference: null,
            ReferralCode: null,
            ReferralUrl: null,
            RegionAreaKey: null,
            RegionAreaName: null,
            RegionKey: null,
            RegionName: null,
            RegistrationSourceCode: null,
            RegistrationSourceName: null,
            RequestKey: null,
            RoleKey: null,
            RoleName: null,
            SecondaryPassword: null,
            SystemPassword: null,
            UserName: null,
            WebsiteUrl: null,

            StateKey: null,
            StateName: null

        }
    toogleIsFormProcessing(value: boolean): void {
        this._HelperService.IsFormProcessing = value;
        //    this._ChangeDetectorRef.detectChanges();
    }
    public _Address: any = {};
    public _ContactPerson: any = {};

    GetAccountDetails() {
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetDeal,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveAccountId,
            AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
            // Reference: this._HelperService.GetSearchConditionStrict(
            //   "",
            //   "ReferenceKey",
            //   this._HelperService.AppConfig.DataType.Text,
            //   this._HelperService.AppConfig.ActiveReferenceKey,
            //   "="
            // ),
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.toogleIsFormProcessing(false);
                    this._UserAccount = _Response.Result;
                    this._Address = this._UserAccount.Address;
                    this._ContactPerson = this._UserAccount.ContactPerson;

                    //#region RelocateMarker 

                    if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
                        this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
                        this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
                    } else {
                        this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
                        this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
                    }
                    // this.FormC_EditUser_Latitude = this._UserAccount.Latitude;
                    // this.FormC_EditUser_Longitude = this._UserAccount.Longitude;

                    // this._HelperService._ReLocate();

                    //#endregion

                    //#region DatesAndStatusInit 
                    this._UserAccount.CodeValidityStartDate = this._HelperService.GetDateS(
                        this._UserAccount.CodeValidityStartDate
                    );
                    this._UserAccount.StartDateS = this._HelperService.GetDateS(
                        this._UserAccount.StartDate
                    );
                    this._UserAccount.EndDateS = this._HelperService.GetDateS(
                        this._UserAccount.EndDate
                    );
                    this._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(
                        this._UserAccount.CreateDate
                    );
                    this._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(
                        this._UserAccount.ModifyDate
                    );
                    this._UserAccount.StatusI = this._HelperService.GetStatusIcon(
                        this._UserAccount.StatusCode
                    );
                    this._UserAccount.StatusB = this._HelperService.GetStatusBadge(
                        this._UserAccount.StatusCode
                    );
                    this._UserAccount.StatusC = this._HelperService.GetStatusColor(
                        this._UserAccount.StatusCode
                    );


                    //#endregion

                    this._ChangeDetectorRef.detectChanges();
                }
                else {
                    this.toogleIsFormProcessing(false);
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }



    public PurchaseHistory_Config: OList;
    PurchaseHistory_Setup() {
        this.PurchaseHistory_Config = {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetPurchaseHistory,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals,
            Title: "Purchase History",
            StatusType: "default",
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveAccountId,
            AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
            // Type: this._HelperService.AppConfig.ListType.SubOwner,
            DefaultSortExpression: "CreateDate desc",
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'dealcode.unused', '=='),
            TableFields: [
                {
                    DisplayName: " Name",
                    SystemName: "DisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Contact No",
                    SystemName: "ContactNumber",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "text-center",
                    Show: true,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                    DefaultValue: "ThankUCash",
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Email Address",
                    SystemName: "EmailAddress",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },

                {
                    DisplayName: "Terminals",
                    SystemName: "Terminals",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "text-right",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Cashiers",
                    SystemName: "Cashiers",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "text-right",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                    // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
                    //   .PanelAcquirer.Merchant.Dashboard,
                }, {
                    DisplayName: "Manager",
                    SystemName: "ManagerName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "text-right",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: "CreateDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date text-right",
                    Show: true,
                    IsDateSearchField: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
            ]
        };
        this.PurchaseHistory_Config = this._DataHelperService.List_Initialize(
            this.PurchaseHistory_Config
        );

        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.Stores,
            this.PurchaseHistory_Config
        );

        this.PurchaseHistory_GetData();
    }
    PurchaseHistory_ToggleOption(event: any, Type: any) {

        if (event != null) {
            for (let index = 0; index < this.PurchaseHistory_Config.Sort.SortOptions.length; index++) {
                const element = this.PurchaseHistory_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }
            }
        }

        this._HelperService.Update_CurrentFilterSnap(
            event,
            Type,
            this.PurchaseHistory_Config
        );

        this.PurchaseHistory_Config = this._DataHelperService.List_Operations(
            this.PurchaseHistory_Config,
            event,
            Type
        );

        if (
            (this.PurchaseHistory_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.PurchaseHistory_GetData();
        }

    }
    timeout = null;
    PurchaseHistory_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {
            if (event != null) {
                for (let index = 0; index < this.PurchaseHistory_Config.Sort.SortOptions.length; index++) {
                    const element = this.PurchaseHistory_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {
                        element.SystemActive = true;
                    }
                    else {
                        element.SystemActive = false;
                    }
                }
            }

            this._HelperService.Update_CurrentFilterSnap(
                event,
                Type,
                this.PurchaseHistory_Config
            );

            this.PurchaseHistory_Config = this._DataHelperService.List_Operations(
                this.PurchaseHistory_Config,
                event,
                Type
            );

            if (
                (this.PurchaseHistory_Config.RefreshData == true)
                && this._HelperService.DataReloadEligibility(Type)
            ) {
                this.PurchaseHistory_GetData();
            }
        }, this._HelperService.AppConfig.SearchInputDelay);
    }
    PurchaseHistory_GetData() {
        var TConfig = this._DataHelperService.List_GetData(
            this.PurchaseHistory_Config
        );
        this.PurchaseHistory_Config = TConfig;
    }
    PurchaseHistory_RowSelected(ReferenceData) {
        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveDeal,
            {
                ReferenceKey: ReferenceData.ReferenceKey,
                ReferenceId: ReferenceData.ReferenceId,
                DisplayName: ReferenceData.DisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
            }
        );

        this._HelperService.AppConfig.ActiveReferenceKey =
            ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
        this._HelperService.AppConfig.ActiveAccountKey =
            ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveAccountId = ReferenceData.ReferenceId;
        // this._Router.navigate([
        //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Store
        //     .Dashboard,
        //   ReferenceData.ReferenceKey,
        //   ReferenceData.ReferenceId,
        // ]);

        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deal,
            ReferenceData.ReferenceKey,
            ReferenceData.ReferenceId,
            ReferenceData.AccountId,
            ReferenceData.AccountKey,

        ]);


    }


    //#region Redeem History 

    public RedeemHistory_Config: OList;
    RedeemHistory_Setup() {
        this.RedeemHistory_Config = {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetPurchaseHistory,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals,
            Title: "Redeem History",
            StatusType: "default",
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveAccountId,
            AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
            // Type: this._HelperService.AppConfig.ListType.SubOwner,
            DefaultSortExpression: "CreateDate desc",
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'dealcode.used', '=='),
            TableFields: [
                {
                    DisplayName: " Name",
                    SystemName: "DisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Contact No",
                    SystemName: "ContactNumber",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "text-center",
                    Show: true,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                    DefaultValue: "ThankUCash",
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Email Address",
                    SystemName: "EmailAddress",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },

                {
                    DisplayName: "Terminals",
                    SystemName: "Terminals",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "text-right",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Cashiers",
                    SystemName: "Cashiers",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "text-right",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                    // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
                    //   .PanelAcquirer.Merchant.Dashboard,
                }, {
                    DisplayName: "Manager",
                    SystemName: "ManagerName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "text-right",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: "CreateDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date text-right",
                    Show: true,
                    IsDateSearchField: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
            ]
        };
        this.RedeemHistory_Config = this._DataHelperService.List_Initialize(
            this.RedeemHistory_Config
        );

        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.Stores,
            this.RedeemHistory_Config
        );

        this.RedeemHistory_GetData();
    }
    RedeemHistory_ToggleOption(event: any, Type: any) {

        if (event != null) {
            for (let index = 0; index < this.RedeemHistory_Config.Sort.SortOptions.length; index++) {
                const element = this.RedeemHistory_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }
            }
        }

        this._HelperService.Update_CurrentFilterSnap(
            event,
            Type,
            this.RedeemHistory_Config
        );

        this.RedeemHistory_Config = this._DataHelperService.List_Operations(
            this.RedeemHistory_Config,
            event,
            Type
        );

        if (
            (this.RedeemHistory_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.RedeemHistory_GetData();
        }

    }

    RedeemHistory_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {
            if (event != null) {
                for (let index = 0; index < this.RedeemHistory_Config.Sort.SortOptions.length; index++) {
                    const element = this.RedeemHistory_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {
                        element.SystemActive = true;
                    }
                    else {
                        element.SystemActive = false;
                    }
                }
            }

            this._HelperService.Update_CurrentFilterSnap(
                event,
                Type,
                this.RedeemHistory_Config
            );

            this.RedeemHistory_Config = this._DataHelperService.List_Operations(
                this.RedeemHistory_Config,
                event,
                Type
            );

            if (
                (this.RedeemHistory_Config.RefreshData == true)
                && this._HelperService.DataReloadEligibility(Type)
            ) {
                this.RedeemHistory_GetData();
            }
        }, this._HelperService.AppConfig.SearchInputDelay);
    }
    RedeemHistory_GetData() {
        var TConfig = this._DataHelperService.List_GetData(
            this.RedeemHistory_Config
        );
        this.RedeemHistory_Config = TConfig;
    }
    RedeemHistory_RowSelected(ReferenceData) {
        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveDeal,
            {
                ReferenceKey: ReferenceData.ReferenceKey,
                ReferenceId: ReferenceData.ReferenceId,
                DisplayName: ReferenceData.DisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
            }
        );

        this._HelperService.AppConfig.ActiveReferenceKey =
            ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
        this._HelperService.AppConfig.ActiveAccountKey =
            ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveAccountId = ReferenceData.ReferenceId;
        // this._Router.navigate([
        //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Store
        //     .Dashboard,
        //   ReferenceData.ReferenceKey,
        //   ReferenceData.ReferenceId,
        // ]);

        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deal,
            ReferenceData.ReferenceKey,
            ReferenceData.ReferenceId,
            ReferenceData.AccountId,
            ReferenceData.AccountKey,

        ]);


    }

    //#endregion

    public _GetoverviewSummary: any = {};
    public TodayStartTime = null;
    public TodayEndTime = null;
    private pData = {
        Task: 'getpromocodeusageoverview',
        StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
        EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
        ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
        ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
        AccountId: this._HelperService.AppConfig.ActiveAccountId,
        AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
        StoreReferenceId: 0,
        StoreReferenceKey: null,
    };
    GetSalesOverview() {


        this.showDailyChart = false;
        this._HelperService.IsFormProcessing = true;

        this.pData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
        this.pData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promocode, this.pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._GetoverviewSummary = _Response.Result as any;
                    // console.log("this._GetoverviewSummary",this._GetoverviewSummary,this._UserAccount.MaximumLimit)
                    // this.Piedata[1] = this._UserAccount.MaximumLimit;
                    // this.Piedata[2] = this._GetoverviewSummary.Used;
                    // this.TotalCountDeal = this._UserAccount.MaximumLimit - this._GetoverviewSummary.Used 
                    // this.Piedata[0] = this.TotalCountDeal;


                    this.Piedata[1] = this._GetoverviewSummary.Used;

                    this.TotalCountDeal = this._UserAccount.MaximumLimit - this._GetoverviewSummary.Used
                    this.Piedata[2] = this.TotalCountDeal;
                    this.Piedata[0] = this._UserAccount.MaximumLimit;

                    //console.log("this.Piedata",this.Piedata)


                    // this.Piedata[0] = this._GetoverviewSummary.Total;
                    this.showDailyChart = true;
                    this._ChangeDetectorRef.detectChanges();
                    // this.GetDealPurchaseOverview();

                    return;

                }

                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }


    public _GetPurchaseoverviewSummary: any = {};
    TotalCountDeal: any = null;

    private pDealPurchaseData = {
        Task: 'getpromocodeusageoverview',
        StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
        EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
        ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
        ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
        AccountId: this._HelperService.AppConfig.ActiveAccountId,
        AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
        StoreReferenceId: 0,
        StoreReferenceKey: null,
    };
    GetDealPurchaseOverview() {
        this.showDailyChart = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.IsFormProcessing = true;

        this.pData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
        this.pData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promocode, this.pDealPurchaseData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._GetPurchaseoverviewSummary = _Response.Result as any;
                    this.Piedata[1] = this._GetPurchaseoverviewSummary.Total;
                    this.Piedata[2] = this._GetPurchaseoverviewSummary.Used;
                    this.TotalCountDeal = this._GetPurchaseoverviewSummary.MaxLimit - this._GetPurchaseoverviewSummary.Total
                    this.Piedata[0] = this.TotalCountDeal;
                    this.showDailyChart = true;
                    this._ChangeDetectorRef.detectChanges();

                    return;

                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }


    public ReviewHistory_Config: OList;
    ReviewHistory_Setup() {
        this.ReviewHistory_Config = {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.getdealreviews,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals,
            Title: "Purchase History",
            Type: this._HelperService.AppConfig.ListType.All,
            StatusType: "default",
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveAccountId,
            AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
            // Type: this._HelperService.AppConfig.ListType.SubOwner,
            DefaultSortExpression: "CreateDate desc",
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
            TableFields: [
                {
                    DisplayName: " Name",
                    SystemName: "AccountDisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Contact No",
                    SystemName: "AccountMobileNumber",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "text-center",
                    Show: true,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                    DefaultValue: "ThankUCash",
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Rating",
                    SystemName: "Rating",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },

                {
                    DisplayName: "Review",
                    SystemName: "Review",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "text-right",
                    Show: true,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "IsWorking",
                    SystemName: "IsWorking",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "text-right",
                    Show: true,
                    Search: false,
                    Sort: false,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                    // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
                    //   .PanelAcquirer.Merchant.Dashboard,
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: "CreateDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date text-right",
                    Show: true,
                    IsDateSearchField: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
            ]
        };
        this.ReviewHistory_Config = this._DataHelperService.List_Initialize(
            this.ReviewHistory_Config
        );

        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.Stores,
            this.ReviewHistory_Config
        );

        //this.ReviewHistory_GetData();
    }
    ReviewHistory_ToggleOption(event: any, Type: any) {

        if (event != null) {
            for (let index = 0; index < this.ReviewHistory_Config.Sort.SortOptions.length; index++) {
                const element = this.ReviewHistory_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }
            }
        }

        this._HelperService.Update_CurrentFilterSnap(
            event,
            Type,
            this.ReviewHistory_Config
        );

        this.ReviewHistory_Config = this._DataHelperService.List_Operations(
            this.ReviewHistory_Config,
            event,
            Type
        );

        if (
            (this.ReviewHistory_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            //this.ReviewHistory_GetData();
        }

    }
    ReviewHistory_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {
            if (event != null) {
                for (let index = 0; index < this.ReviewHistory_Config.Sort.SortOptions.length; index++) {
                    const element = this.ReviewHistory_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {
                        element.SystemActive = true;
                    }
                    else {
                        element.SystemActive = false;
                    }
                }
            }

            this._HelperService.Update_CurrentFilterSnap(
                event,
                Type,
                this.ReviewHistory_Config
            );

            this.ReviewHistory_Config = this._DataHelperService.List_Operations(
                this.ReviewHistory_Config,
                event,
                Type
            );

            if (
                (this.ReviewHistory_Config.RefreshData == true)
                && this._HelperService.DataReloadEligibility(Type)
            ) {
                //this.ReviewHistory_GetData();
            }
        }, this._HelperService.AppConfig.SearchInputDelay);
    }
    ReviewHistory_GetData() {
        var TConfig = this._DataHelperService.List_GetData(
            this.ReviewHistory_Config
        );
        this.ReviewHistory_Config = TConfig;
    }
    ReviewHistory_RowSelected(ReferenceData) {
        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveDeal,
            {
                ReferenceKey: ReferenceData.ReferenceKey,
                ReferenceId: ReferenceData.ReferenceId,
                DisplayName: ReferenceData.DisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
            }
        );

        this._HelperService.AppConfig.ActiveReferenceKey =
            ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
        this._HelperService.AppConfig.ActiveAccountKey =
            ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveAccountId = ReferenceData.ReferenceId;


        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deal,
            ReferenceData.ReferenceKey,
            ReferenceData.ReferenceId,
            ReferenceData.AccountId,
            ReferenceData.AccountKey,

        ]);


    }



    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        this._HelperService.MakeFilterSnapPermanent();
        //this.ReviewHistory_GetData();

        if (ButtonType == 'Sort') {
            $("#DealsList_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#DealsList_fdropdown").dropdown('toggle');
        }
        this._HelperService.StopClickPropogation();
        // this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }


    tabSwitched(arg1: any): void { }
    ResetFilters(arg1: any, arg2: any): void { }
    OpenEditColModal(): void { }


    GetPromocodeDetails() {
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.getpromocode,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            // Reference: this._HelperService.GetSearchConditionStrict(
            //   "",
            //   "ReferenceKey",
            //   this._HelperService.AppConfig.DataType.Text,
            //   this._HelperService.AppConfig.ActiveReferenceKey,
            //   "="
            // ),
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promocode, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    // console.log(_Response);
                    this.toogleIsFormProcessing(false);
                    this._UserAccount = _Response.Result;
                    this.GetSalesOverview();
                    // console.log(this._UserAccount)

                    //  this.MerchantPlaceHolder = this._UserAccount.ConditionName;

                    var tStartDate = moment(this._UserAccount.StartDate);
                    var tEndDate = moment(this._UserAccount.EndDate);

                    this._UserAccount.StartDateS = this._HelperService.GetDateS(
                        this._UserAccount.StartDate
                    );
                    this._UserAccount.EndDateS = this._HelperService.GetDateS(
                        this._UserAccount.EndDate
                    );
                    this._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(
                        this._UserAccount.CreateDate
                    );
                    this._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(
                        this._UserAccount.ModifyDate
                    );
                    this._UserAccount.StatusI = this._HelperService.GetStatusIcon(
                        this._UserAccount.StatusCode
                    );
                    this._UserAccount.StatusB = this._HelperService.GetStatusBadge(
                        this._UserAccount.StatusCode
                    );
                    this._UserAccount.StatusC = this._HelperService.GetStatusColor(
                        this._UserAccount.StatusCode
                    );


                    //#endregion

                    this._ChangeDetectorRef.detectChanges();
                }
                else {
                    this.toogleIsFormProcessing(false);
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
}

export class OAccountOverview {
    public TotalTransactions?: number;
    public TotalSale?: number;

    public DeadTerminals?: number;
    public IdleTerminals?: number;
    public TerminalStatus?: number;
    public Total?: number;
    public Idle?: number;
    public Active?: number;
    public Dead?: number;
    public Inactive?: number;
    public UnusedTerminals?: number;
    public Merchants: number;
    public ActiveMerchants: number;
    public ActiveMerchantsDiff: number;
    public Terminals: number;
    public ActiveTerminals: number;
    public ActiveTerminalsDiff: number;
    public Transactions: number;
    public TransactionsDiff: number;
    public PurchaseAmount: number;
    public PurchaseAmountDiff: number;
    public CashRewardPurchaseAmount: number;
    public CashRewardPurchaseAmountDiff: number;
    public CardRewardPurchaseAmount: number;
    public CardRewardPurchaseAmountDiff: number;
}
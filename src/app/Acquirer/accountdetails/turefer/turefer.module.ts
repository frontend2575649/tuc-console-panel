import { AgmCoreModule } from '@agm/core';
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Select2Module } from "ng2-select2";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgxPaginationModule } from "ngx-pagination";
import { DynamicRoutesguardGuard } from 'src/app/service/guard/dynamicroutes.guard';
import { MainPipe } from '../../../service/main-pipe.module';
import { TUReferComponent } from "./turefer.component"; 
const routes: Routes = [
    {
        path: "",
        component: TUReferComponent,
        canActivate:[DynamicRoutesguardGuard],
        data:{accessName:['referalcampaignview']},
        children: [
            //{ path: "", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../Acquirer/accountdetails/turefer/tucodeusage/tucodeusage.module#TUCodeUsageModule" },
        //    { path: "", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../dashboards/tucodeusage/tucodeusage.module#TUCodeUsageModule" },
            // { path: ":referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../dashboards/tudetailacquirer/dashboard.module#TUDashboardModule" },
            // //{ path: "acquireraccount/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../dashboards/tudetailacquirer/dashboard.module#TUDashboardModule" },
             { path: "", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../dashboards/tudetailreferal/dashboard.module#TUDashboardModule" },
             { path: "referalaccount/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../dashboards/tudetailreferal/dashboard.module#TUDashboardModule" },
            // { path: "analyticsreferal/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../Acquirer/accountdetails/turefer/tuanalyticsreferal/tuanalyticsreferal.module#TUAnalyticsReferalModule" },
           
           
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUReferRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUReferRoutingModule,
        GooglePlaceModule,
        MainPipe,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),

    ],
    declarations: [TUReferComponent]
})
export class TUReferModule { }

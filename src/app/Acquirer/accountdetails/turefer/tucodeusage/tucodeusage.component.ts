import { ChangeDetectorRef, Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BaseChartDirective, Label } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
declare var moment: any;
declare var $: any;
import * as pluginEmptyOverlay from "chartjs-plugin-empty-overlay";
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import swal from 'sweetalert2';
import * as Feather from 'feather-icons';
import { HCoreXAddress } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';

@Component({
  selector: 'tucodeusage',
  templateUrl: './tucodeusage.component.html',
  styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUCodeUsageComponent implements OnInit {
  public ResetFilterControls: boolean = true;
  public TodayDate: any;
  public SaveAccountRequest: any;
  _DealConfig =
  {

      Images: [],
      StartDateConfig: {
      },
      EndDateConfig: {
      },
      DefaultStartDate: null,
      DefaultEndDate: null,
      DealImages: [],
      StartDate: null,
      EndDate: null,
  }
  Form_AddUser: FormGroup;
  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
  }

  ngOnInit() {
    this._HelperService.ValidateData();
    Feather.replace();

    this._HelperService.FullContainer = false;
    this._HelperService.FullContainer = false;
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];

      if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
        this._Router.navigate([
          this._HelperService.AppConfig.Pages.System.NotFound
        ]);
      } else {
        this._HelperService.ResetDateRange();
       
       // this.GetPromoCodeAccounts();
          this.RequestHistory_Setup();
          this.Form_AddUser_Load();
          this.GetConditionCategories();
          this.GetAccountDetails();
        

        //deal date
        this._DealConfig.DefaultStartDate = moment();
        this._DealConfig.DefaultEndDate = moment().add(1, 'days').endOf("day");
        this._DealConfig.StartDate = this._DealConfig.DefaultStartDate;
        this._DealConfig.EndDate = this._DealConfig.DefaultEndDate;
        this._DealConfig.StartDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: this._HelperService.DateInUTC(moment()),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: this._HelperService.DateInUTC(moment()),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        //end deal date
      }
    });
  }

  public _UserAccount: any =
    {
      MerchantDisplayName: null,
      SecondaryEmailAddress: null,
      BankDisplayName: null,
      BankKey: null,
      OwnerName: null,
      SubOwnerAddress: null,
      SubOwnerLatitude: null,
      SubOwnerDisplayName: null,
      SubOwnerKey: null,
      SubOwnerLongitude: null,
      AccessPin: null,
      LastLoginDateS: null,
      AppKey: null,
      AppName: null,
      AppVersionKey: null,
      CreateDate: null,
      CreateDateS: null,
      CreatedByDisplayName: null,
      CreatedByIconUrl: null,
      CreatedByKey: null,
      Description: null,
      IconUrl: null,
      ModifyByDisplayName: null,
      ModifyByIconUrl: null,
      ModifyByKey: null,
      ModifyDate: null,
      ModifyDateS: null,
      PosterUrl: null,
      ReferenceKey: null,
      StatusCode: null,
      StatusI: null,
      StatusId: null,
      StatusName: null,
      AccountCode: null,
      AccountOperationTypeCode: null,
      AccountOperationTypeName: null,
      AccountTypeCode: null,
      AccountTypeName: null,
      Address: null,
      AppVersionName: null,
      ApplicationStatusCode: null,
      ApplicationStatusName: null,
      AverageValue: null,
      CityAreaKey: null,
      CityAreaName: null,
      CityKey: null,
      CityName: null,
      ContactNumber: null,
      CountValue: null,
      CountryKey: null,
      CountryName: null,
      DateOfBirth: null,
      DisplayName: null,
      EmailAddress: null,
      EmailVerificationStatus: null,
      EmailVerificationStatusDate: null,
      FirstName: null,
      GenderCode: null,
      GenderName: null,
      LastLoginDate: null,
      LastName: null,
      Latitude: null,
      Longitude: null,
      MobileNumber: null,
      Name: null,
      NumberVerificationStatus: null,
      NumberVerificationStatusDate: null,
      OwnerDisplayName: null,
      OwnerKey: null,
      Password: null,
      Reference: null,
      ReferralCode: null,
      ReferralUrl: null,
      RegionAreaKey: null,
      RegionAreaName: null,
      RegionKey: null,
      RegionName: null,
      RegistrationSourceCode: null,
      RegistrationSourceName: null,
      RequestKey: null,
      RoleKey: null,
      RoleName: null,
      SecondaryPassword: null,
      SystemPassword: null,
      UserName: null,
      WebsiteUrl: null,

      StateKey: null,
      StateName: null

    }
  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
    //    this._ChangeDetectorRef.detectChanges();
  }
  public _Address: HCoreXAddress = {};
  AddressChange(Address) {
    this._Address = Address;
  }

  public _ContactPerson: any = {};
  public _Overview: any = {};
  public updateNumber: any;
  targetAudience:any;
  GetAccountDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.getpromocode,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      // Reference: this._HelperService.GetSearchConditionStrict(
      //   "",
      //   "ReferenceKey",
      //   this._HelperService.AppConfig.DataType.Text,
      //   this._HelperService.AppConfig.ActiveReferenceKey,
      //   "="
      // ),
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promocode, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
         // console.log(_Response);
          this.toogleIsFormProcessing(false);
          this._UserAccount = _Response.Result;
         // console.log(this._UserAccount)
          this.Form_AddUser.controls['StartDate'].setValue(moment(this._UserAccount.StartDate).format('DD-MM-YYYY hh:mm a'));
          this.Form_AddUser.controls['EndDate'].setValue(moment(this._UserAccount.EndDate).format('DD-MM-YYYY hh:mm a'));
        //  this.MerchantPlaceHolder = this._UserAccount.ConditionName;
          this.targetAudience=this._UserAccount.ConditionName
          var tStartDate = moment(this._UserAccount.StartDate);
          var tEndDate = moment(this._UserAccount.EndDate);
          this.CouponCount=this._UserAccount.MaximumLimit
          

          this.budgetAmount=this._UserAccount.Budget
          this._DealConfig.StartDate = tStartDate;
          this._DealConfig.EndDate = tEndDate;


         
            if(this._UserAccount.MaximumLimitPerUser != undefined && this._UserAccount.MaximumLimitPerUser > 1 )
            {
              this.IsSingleCode = false;
              this.IsMultipleCode = true;
            }
           
          


          this._UserAccount.StartDateS = this._HelperService.GetDateS(
            this._UserAccount.StartDate
          );
          this._UserAccount.EndDateS = this._HelperService.GetDateS(
            this._UserAccount.EndDate
          );
          this._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(
            this._UserAccount.CreateDate
          );
          this._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(
            this._UserAccount.ModifyDate
          );
          this._UserAccount.StatusI = this._HelperService.GetStatusIcon(
            this._UserAccount.StatusCode
          );
          this._UserAccount.StatusB = this._HelperService.GetStatusBadge(
            this._UserAccount.StatusCode
          );
          this._UserAccount.StatusC = this._HelperService.GetStatusColor(
            this._UserAccount.StatusCode
          );


          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


  GetPromoCodeAccounts() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.getpromocodeaccounts,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      // Reference: this._HelperService.GetSearchConditionStrict(
      //   "",
      //   "ReferenceKey",
      //   this._HelperService.AppConfig.DataType.Text,
      //   this._HelperService.AppConfig.ActiveReferenceKey,
      //   "="
      // ),
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promocode, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
        //  console.log(_Response);
          this.toogleIsFormProcessing(false);
          this._UserAccount = _Response.Result;
        //  console.log(this._UserAccount)
         

          this._UserAccount.StartDateS = this._HelperService.GetDateS(
            this._UserAccount.StartDate
          );
          this._UserAccount.EndDateS = this._HelperService.GetDateS(
            this._UserAccount.EndDate
          );
          this._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(
            this._UserAccount.CreateDate
          );
          this._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(
            this._UserAccount.ModifyDate
          );
          this._UserAccount.StatusI = this._HelperService.GetStatusIcon(
            this._UserAccount.StatusCode
          );
          this._UserAccount.StatusB = this._HelperService.GetStatusBadge(
            this._UserAccount.StatusCode
          );
          this._UserAccount.StatusC = this._HelperService.GetStatusColor(
            this._UserAccount.StatusCode
          );


          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


  StoreRoute() {

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AcquirerPanel.Stores,
      this._HelperService.AppConfig.ActiveReferenceKey,
      this._HelperService.AppConfig.ActiveReferenceId,

    ]);

  }

  MarchantRoute() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AcquirerPanel.Merchants,
      this._HelperService.AppConfig.ActiveReferenceKey,
      this._HelperService.AppConfig.ActiveReferenceId

    ])

  }

  POSRoute() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AcquirerPanel.Terminals,
      this._HelperService.AppConfig.ActiveReferenceKey,
      this._HelperService.AppConfig.ActiveReferenceId

    ])

  }


  BlockAccount() {
    swal({
      title: this._HelperService.AppConfig.CommonResource.BlockAcquirerTitle,
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
        '<input type="text"  placeholder="Enter Comment"  id="swal-input1" class="swal2-input">' +
        '<input style="-webkit-text-security: disc;" type="text" placeholder="Enter Pin"  id="swal-input2" class="swal2-input">',


      focusConfirm: false,
      preConfirm: () => {
        return [
          document.getElementById('swal-input1')['value'],
          document.getElementById('swal-input2')['value']
        ]
      },
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
      inputValidator: function (value) {
        if (value === '' || value.length < 4) {
          return 'Enter your 4 digit pin!'
        }
      },
    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updateaccountstatus",
          AccountId: this._UserAccount.ReferenceId,
          AccountKey: this._UserAccount.ReferenceKey,
          StatusCode: "default.blocked",
          AuthPin: result.value[1],
          Comment: result.value[0],
          AccountTypeCode: this._HelperService.AppConfig.AccountType.Acquirer

        };

        if (PostData.AuthPin) {
          let _OResponse: Observable<OResponse>;
          _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, PostData);
          _OResponse.subscribe(
            _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.NotifySuccess("Acquirer Blocked Successfully");
                this.GetAccountDetails();
              } else {
                this._HelperService.NotifySuccess(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
            }
          );
        }
        else {
          this._HelperService.NotifyError('Enter Your 4 Digit Pin')
        }



      }
    });


  }
  UnblockBlockAccount() {
    swal({
      title: this._HelperService.AppConfig.CommonResource.UnBlockAcquirerTitle,
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
        '<input type="text" placeholder="Enter Comment"  id="swal-input1" class="swal2-input">' +
        '<input style="-webkit-text-security: disc;" type="text" placeholder="Enter Pin" id="swal-input2" class="swal2-input">',


      focusConfirm: false,
      preConfirm: () => {
        return [
          document.getElementById('swal-input1')['value'],
          document.getElementById('swal-input2')['value']
        ]
      },
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updateaccountstatus",
          AccountId: this._UserAccount.ReferenceId,
          AccountKey: this._UserAccount.ReferenceKey,
          StatusCode: "default.active",
          AuthPin: result.value[1],
          Comment: result.value[0],
          AccountTypeCode: this._HelperService.AppConfig.AccountType.Acquirer

        };

        if (PostData.AuthPin) {
          let _OResponse: Observable<OResponse>;
          _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, PostData);
          _OResponse.subscribe(
            _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.NotifySuccess("Acquirer UnBlocked Successfully");
                this.GetAccountDetails();
              } else {
                this._HelperService.NotifySuccess(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
            }
          );
        }
        else {
          this._HelperService.NotifyError('Enter Your 4 Digit Pin')

        }


      }
    });


  }

  Form_UpdateUser: FormGroup;
  Form_UpdateUser_Address: string = null;
  Form_UpdateUser_Latitude: number = 0;
  Form_UpdateUser_Longitude: number = 0;
  @ViewChild('places') places: GooglePlaceDirective;
  Form_UpdateUser_PlaceMarkerClick(event) {
    this.Form_UpdateUser_Latitude = event.coords.lat;
    this.Form_UpdateUser_Longitude = event.coords.lng;
  }
  public Form_UpdateUser_AddressChange(address: Address) {
    this.Form_UpdateUser_Latitude = address.geometry.location.lat();
    this.Form_UpdateUser_Longitude = address.geometry.location.lng();
    this.Form_UpdateUser_Address = address.formatted_address;
  }



  //#region RequestHistory
  public CurrentRequest_Key: string;
  public CurrentRequest_Id : number;
  public RequestHistory_Config: OList;
  RequestHistory_Setup() {
    this.RequestHistory_Config = {
      Id: null,
      Type: null,
     // Sort:null,
      Sort:
      {
          SortDefaultName: null,
          SortDefaultColumn: 'StartDate',
          SortName: null,
          SortColumn: null,
          SortOrder: 'desc',
          SortOptions: [],
      },
      Task: this._HelperService.AppConfig.Api.Core.getpromocodeaccounts,
      // ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      // ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,

      ReferenceId: 0,
      ReferenceKey:null,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Promocode,
      Title: 'Core Usage',
      StatusType: 'transaction',
     SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "PromoCodeId", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveReferenceId, "="),
      // DefaultSortExpression: 'RequestTime desc',
      // Sort:
      // {
      //   SortDefaultName: null,
      //   SortDefaultColumn: 'RequestTime',
      //   SortName: null,
      //   SortColumn: null,
      //   SortOrder: 'desc',
      //   SortOptions: [],
      // },
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        // {
        //     DisplayName: '#',
        //     SystemName: 'ReferenceId',
        //     DataType: this._HelperService.AppConfig.DataType.Number,
        //     Class: '',
        //     Show: true,
        //     Search: true,
        //     Sort: true,
        //     ResourceId: null,
        // },
        {
            DisplayName: 'Promocode',
            SystemName: 'PromoCode',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: true,
            Search: true,
            Sort: false,
            ResourceId: null,
        },
        {
          DisplayName: 'Name',
          SystemName: 'AccountName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
      },
      
        {
            DisplayName: 'Mobile No',
            SystemName: 'AccountMobileNumber',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: true,
            Search: true,
            Sort: true,
            ResourceId: null,
        },
        {
            DisplayName: 'Email Address',
            SystemName: 'AccoountEmailAddress',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: true,
            Sort: false,
            ResourceId: null,
        },
        {
          DisplayName: "Start Date",
          SystemName: 'StartDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
        },
        {
            DisplayName: 'End Date',
            SystemName: 'EndDate',
            DataType: this._HelperService.AppConfig.DataType.Number,
            Show: true,
            Search: true,
            Sort: true,
            ResourceId: null,
        },
    ]

    };
    this.RequestHistory_Config = this._DataHelperService.List_InitializeRewPerc(
      this.RequestHistory_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Cashier,
      this.RequestHistory_Config
    );

    this.RequestHistory_GetData();
  }
  RequestHistory_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.RequestHistory_Config.Sort.SortOptions.length; index++) {
        const element = this.RequestHistory_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.RequestHistory_Config


    );

    this.RequestHistory_Config = this._DataHelperService.List_Operations(
      this.RequestHistory_Config,
      event,
      Type
    );

    if (
      (this.RequestHistory_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.RequestHistory_GetData();
    }

  }
  RequestHistory_GetData() {

    var TConfig = this._DataHelperService.List_GetData(
      this.RequestHistory_Config
    );
    this.RequestHistory_Config = TConfig;
  }
  RequestHistory_RowSelected(ReferenceData) {
    this.CurrentRequest_Key = ReferenceData.ReferenceKey;
    this.CurrentRequest_Id= ReferenceData.ReferenceId;

    this.ListAppUsage_GetDetails();

  }

  //#endregion

  ListAppUsage_GetDetails() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetRequestHistoryDetails,
      ReferenceKey:this.CurrentRequest_Key,
      ReferenceId: this.CurrentRequest_Id,


      // Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this.CurrentRequest_Key, '='),
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          // this._CoreUsage = _Response.Result as OCoreUsage;
          // this._CoreUsage.RequestTime = this._HelperService.GetDateTimeS(this._CoreUsage.RequestTime);
          // this._CoreUsage.ResponseTime = this._HelperService.GetDateTimeS(this._CoreUsage.ResponseTime);
          // this.clicked()
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      });
  }


  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.RequestHistory_GetData();

    if (ButtonType == 'Sort') {
      $("#RequestHistory_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#RequestHistory_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);
    this.SetOtherFilters();

   // this.RequestHistory_GetData();
   this.RequestHistory_Setup();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  SetOtherFilters(): void {
    this.RequestHistory_Config.SearchBaseConditions = [];
    this.RequestHistory_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.RequestHistory_Filter_Stores_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

 


  OwnerEventProcessing(event: any): void {
    if (event.value == this.RequestHistory_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.RequestHistory_Filter_Stores_Selected,
        "="
      );
      this.RequestHistory_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.RequestHistory_Config.SearchBaseConditions
      );
      this.RequestHistory_Filter_Stores_Selected = null;
    } else if (event.value != this.RequestHistory_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.RequestHistory_Filter_Stores_Selected,
        "="
      );
      this.RequestHistory_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.RequestHistory_Config.SearchBaseConditions
      );
      this.RequestHistory_Filter_Stores_Selected = event.data[0].ReferenceKey;
      this.RequestHistory_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "StoreReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.RequestHistory_Filter_Stores_Selected,
          "="
        )
      );
    }

    this.RequestHistory_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }


  public RequestHistory_Filter_Stores_Option: Select2Options;
  public RequestHistory_Filter_Stores_Selected = null;
  RequestHistory_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      // AccountKey: this._HelperService.UserAccount.AccountKey,
      // AccountId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.RequestHistory_Filter_Stores_Option = {
      placeholder: "Select Store",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  RequestHistory_Filter_Stores_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.RequestHistory_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.RequestHistory_GetData();
  }

  // RemoveFilterComponent(Type: string, index?: number): void {
  //   this._FilterHelperService._RemoveFilter_RequestHistory(Type, index);
  //   this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);

  //   this.SetOtherFilters();

  //   this.RequestHistory_GetData();
  // }

  RemoveFilterComponent(Type: string, index?: number): void {
  //  console.log("Type",Type)
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.RequestHistory_Config);

    this.SetOtherFilters();

    this.RequestHistory_Setup();
  }

  Customers_RowSelected(ReferenceData) {
  //  console.log(ReferenceData)

    //#region Save Current Merchant To Storage 

    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveCustomer,
      {
        ReferenceKey: ReferenceData.AccountKey,
        ReferenceId: ReferenceData.AccountId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Customer,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.AccountKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.AccountId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.CustomerPanel.Accounts.WalletInfo,
      ReferenceData.AccountKey,
      ReferenceData.AccountId,
    ]);

    //#endregion


  }

  Editcampaign()
  {
    this._HelperService.OpenModal('Editcampaign')
  }

  //Edit code--
  
    Form_AddUser_Close() {
      this._HelperService.CloseModal('Editcampaign');
      this._HelperService.CloseAllModal();
      this.GetAccountDetails();

      this._Router.navigate([
        this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AcquirerPanel.ReferalAccount,
        this._HelperService.AppConfig.ActiveReferenceKey,
        this._HelperService.AppConfig.ActiveReferenceId,
      ]);
       // this._Router.navigate(['console' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AccountSettings.Referal]);
    }
    Form_AddUser_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_AddUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.updatepromocode,
       
            CountryId: this._HelperService.UserCountryId,
            CountryKey: this._HelperService.UserCountrycode,
            AccountTypeId: 109,
            AccountTypeCode: 'appuser',
            ConditionId: 1,
            ConditionKey: 1,
            TypeId : "1",
            TypeCode : 'default.inactive',
            Title : [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            Description : [null, Validators.compose([ Validators.minLength(2), Validators.maxLength(256)])],
            Budget :0,
            PromoCode: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(12)])],
            PromoCodeValue:[null, Validators.compose([ Validators.required,Validators.minLength(1)])],
            MaximumLimit: [null, Validators.compose([ Validators.required,Validators.minLength(1)])],
            MaximumLimitPerUser: [null, Validators.compose([Validators.minLength(1)])],
            StatusCode: this._HelperService.AppConfig.Status.Active,
            CodeUsageValidity:1,
            StartDate: moment().format('DD-MM-YYYY hh:mm a'),
            EndDate: moment().add(1, 'days').endOf("day").format('DD-MM-YYYY hh:mm a'),
        });
    }
    Form_AddUser_Clear() {
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 300);
        this.Form_AddUser.reset();
        this._HelperService.Icon_Crop_Clear();
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_AddUser_Process(_FormValue: any) {
        if (this._HelperService._Icon_Cropper_Data.Content != null) {
            _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
        }

      
        else {
            this._HelperService.IsFormProcessing = true;
            this.SaveAccountRequest = this.ReFormat_RequestBody();
            //Location Manager - Start
           // this.SaveAccountRequest.Address = this._Address;
            //Location Manager - End
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promocode, this.SaveAccountRequest);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess('promocode Updated successfully.');
                        this._HelperService.RandomPassword = null;
                        this.Form_AddUser_Clear();
                        if (_FormValue.OperationType == 'edit') {
                        }
                        else if (_FormValue.OperationType == 'close') {
                            this.Form_AddUser_Close();
                        }
                        this.Form_AddUser_Close();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }


    }

    ReFormat_RequestBody(): void {
        var formValue: any = this.Form_AddUser.value;
      //  console.log('formvalue',formValue);

        if(formValue.MaximumLimitPerUser==null)
        {
            formValue.MaximumLimitPerUser='1'
        }

       if(formValue.AccountTypeId=='108')
       {
        formValue.AccountTypeCode='thankumerchant'
       }
       else{
        if(formValue.AccountTypeId=='109')
        {
        formValue.AccountTypeCode='appuser'
        }  
       }
        

       if(formValue.ConditionKey=='1')
       {
        formValue.ConditionId='1'
       }
       else{
        if(formValue.ConditionKey=='2')
        {
        formValue.ConditionId='2'
        }  
       }
        var formRequest: any = {
            OperationType: 'new',
            Task: formValue.Task,
          
            ReferenceId: this._UserAccount.ReferenceId,
            ReferenceKey: this._UserAccount.ReferenceKey,
            CountryId: this._HelperService.UserCountryId,
            CountryKey: this._HelperService.UserCountrycode,
            AccountTypeId: formValue.AccountTypeId,
            AccountTypeCode: formValue.AccountTypeCode,
            ConditionId: formValue.ConditionId,
            ConditionKey: formValue.ConditionKey,
            TypeId : 2,
            TypeCode :'Promotional',
            Title : formValue.Title,
            Description : formValue.Description,
            Budget : this.budgetAmount,
            StartDate: moment(this._DealConfig.StartDate).format('YYYY-MM-DD HH:mm'),
            EndDate: moment(this._DealConfig.EndDate).format('YYYY-MM-DD HH:mm'),
            PromoCode: formValue.PromoCode,
            PromoCodeValue:formValue.PromoCodeValue,
            MaximumLimit:formValue.MaximumLimit,
            MaximumLimitPerUser: formValue.MaximumLimitPerUser,
            StatusCode: "default.active"
        };
        return formRequest;

    }

    ScheduleStartDateRangeChange(value) {
       // console.log(value);
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: value.start,
            minDate: value.start,
        };
        this._DealConfig.StartDate = value.start;
        this._DealConfig.EndDate = value.start;
       // this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                StartDate: value.start.format('DD-MM-YYYY hh:mm a'),
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
               
            }
        );
    }
    ScheduleEndDateRangeChange(value) {
       
        this._DealConfig.EndDate = value.start;
       
        this.Form_AddUser.patchValue(
            {
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
               
            }
        );
    }

    budgetAmount:any=0
      ProcessAmounts() {
     this.budgetAmount=  (this.Form_AddUser.controls['MaximumLimit'].value)*(this.Form_AddUser.controls['PromoCodeValue'].value)
       // console.log("tdata",this.budgetAmount);
      }
  // end edit code--

  IsSingleCode =true ;
  IsMultipleCode = false ;

  // CodeUsageValidity=1;
// validity:boolean=false;
CodeUsage(code)
{
  if(code ==  1)
  {
    this.IsSingleCode = true;
    this.IsMultipleCode = false;
    this._UserAccount.MaximumLimitPerUser = 1; 
    this.Form_AddUser.patchValue(
      {
        MaximumLimitPerUser: 1,
      }
    );
  }
  else if(code ==  2)
  {
    this.IsSingleCode = false ;
    this.IsMultipleCode = true;
  }
    // this.CodeUsageValidity=code;
    // if(this.CodeUsageValidity==2)
    // {
    //    this.validity=true; 
    // }
    // else{
    //     this.validity=false; 
    // }
}


public GetMerchants_Option: Select2Options;
public GetMerchants_Transport: any;
public SelectedMerchant: any = {};
GetConditionCategories() {
    var PlaceHolder = "Select Target Audience";
    var _Select: OSelect =
    {


        Task: this._HelperService.AppConfig.Api.Core.getpromocodeconditions,
        Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Promocode,

        ReferenceKey: null,
        ReferenceId: 0,


        SortCondition: [],
        Fields: [
            {
                SystemName: "ReferenceId",
                Type: this._HelperService.AppConfig.DataType.Number,
                Id: true,
                Text: false,
            },

            {
                SystemName: "Name",
                Type: this._HelperService.AppConfig.DataType.Text,
                Id: false,
                Text: true
            },
            // {
            //     SystemName: 'StatusCode',
            //     Type: this._HelperService.AppConfig.DataType.Text,
            //     SearchCondition: '=',
            //     SearchValue: this._HelperService.AppConfig.Status.Active,
            // }
        ]
    }
    this.GetMerchants_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetMerchants_Option = {
        placeholder: PlaceHolder,
        ajax: this.GetMerchants_Transport,
        multiple: false,
        disabled:true
    };

}

    public GetApps_Transport: any;
    getApps_list(){
     this.GetMerchants_Option = {
         placeholder: 'Condition',
         ajax: this.GetApps_Transport,
         multiple: false,
     };
    }
 
    TargetData:any;
    GetApp_ListChange(event: any) {
      
        // console.log(event.data[0].ReferenceKey)
     this.TargetData=event.data[0].Name
         this.Form_AddUser.patchValue(
           {
             ConditionKey: event.data[0].ReferenceKey,
             ConditionId:event.data[0].ReferenceId
            // TypeId: event.value
     
           }
     
     
         );
      
        // console.log("key",this.Form_Promote.NavigationType)
       }

    //    Form_AddUser_Close() {
    //     this._HelperService.CloseModal("ManagePromoteDeal");
    //     this._HelperService.CloseAllModal();

    // }


    RefKey:any;RefId:any;CampaignStartDate:any;CStartDate:any;
    UpdateDate(CampaignData)
    {
      this._HelperService.OpenModal('EditSchedule');
      this.RefKey=CampaignData.ReferenceKey;
      this.RefId=CampaignData.ReferenceId;
      this.CampaignStartDate=CampaignData.StartDate.format('DD-MM-YYYY hh:mm a');
     this.CStartDate=CampaignData.StartDate
    
    }

    UpdatePromoteSchedule()
     {
     // console.log()
      var Pdata={
        'OperationType': 'new',
        'Task': 'updatepromocodeshedule',
        ReferenceId: this.RefId,
        ReferenceKey: this.RefKey,
        StartDate: this.CStartDate,
        EndDate: this._DealConfig.EndDate,
      }
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promocode, Pdata);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess('Promocode Schedule updated successfully.');
  
           
            this._HelperService.CloseModal('EditSchedule');
            this.GetAccountDetails();
          }
          else {
            //this.SelectedDeal.Schedule = [];
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
         // this.SelectedDeal.Schedule = [];
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }

    UpdateEndDateRangeChange(value) {
      //this._DealConfig.StartDate = tStartDate;
     // this._DealConfig.EndDate = tEndDate;
//console.log("data",value.start)
      this._DealConfig.EndDate = value.start;

      this.Form_AddUser.patchValue(
        {
            EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
           
        }
    );
     
    }


    RefKey1:any;RefId1:any;Codedata:any;
    CouponCount: number = null;
    UpdateCode(CampaignData)
    {
      this._HelperService.OpenModal('AddCoupons');
      this.RefKey=CampaignData.ReferenceKey;
      this.RefId=CampaignData.ReferenceId;
      this.Codedata=CampaignData.MaximumLimit;
    //   this.CampaignStartDate=CampaignData.StartDate.format('DD-MM-YYYY hh:mm a');
    //  this.CStartDate=CampaignData.StartDate
    
    }

    UpdatePromoteCodes()
     {
     // console.log()
      var Pdata={
        'OperationType': 'new',
        'Task': 'updatepromocodelimit',
        ReferenceId: this.RefId,
        ReferenceKey: this.RefKey,
        MaximumLimit:this.CouponCount,
      }
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promocode, Pdata);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess('Promocode updated successfully.');
  
           
            this._HelperService.CloseModal('AddCoupons');
            this.GetAccountDetails();
          }
          else {
            //this.SelectedDeal.Schedule = [];
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
         // this.SelectedDeal.Schedule = [];
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }
}


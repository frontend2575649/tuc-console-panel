import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { HCoreXAddress, HCXAddressConfig, locationType } from '../../../component/hcxaddressmanager/hcxaddressmanager.component';
import { DataHelperService } from '../../../service/datahelper.service';
import { HelperService } from '../../../service/helper.service';
import { OUserDetails, OResponse } from '../../../service/object.service';
import * as Feather from 'feather-icons';
declare let $: any;
declare var moment: any;

@Component({
  selector: 'app-tubankloyalty',
  templateUrl: './tubankloyalty.component.html',
})
export class TubankloyaltyComponent implements OnInit {

  public _isAddressLoaded = false;
  public programDetails: any = {};
  public _Address: HCoreXAddress = {};
  reward: any = 0;
  comission: any = 0;
  public minInvoiceAmt: any = 0;
  public MaxComission: any = 0;
  public MaxRewards: any = 0;
  public IncentiveMaxAmount: any = 0;
  public IncentivePercentage: any = 0;

  AddressChange(Address) {
    this._Address = Address;
  }
  public _AddressConfig: HCXAddressConfig =
    {
      locationType: locationType.form,
    }



  defaultToDarkFilter = ["grayscale: 100%", "invert: 100%"];
  isLoaded: boolean = true;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this._HelperService.ResetDateRange();
  }

  //#region mapCorrection 

  ExpandedView: any = false;

  ToogleExpandedView(): void {
    this.ExpandedView = !this.ExpandedView;
    if (this.ExpandedView) {
      // this._HelperService._MapCorrection();
    }
  }

  //#endregion

  //#region DetailShowHIde 

  HideStoreDetail() {
    var element = document.getElementById("StoresDetails");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-ShowStoreDetail");
  }

  ShowStoreDetail() {
    var element = document.getElementById("StoresDetails");
    element.classList.add("Hm-ShowStoreDetail");
    element.classList.remove("Hm-HideDiv");
  }

  //#endregion

  InitBackDropClickEvent(): void {
    // var backdrop: HTMLElement = document.getElementById("backdrop");

    // backdrop.onclick = () => {
    //   $(this.divView.nativeElement).removeClass('show');
    //   backdrop.classList.remove("show");
    // };
  }
  currentProgramDetails: any = {};
  acquireDetails: any = {};

  ngOnInit() {

    //#region GetStorageData 

    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveMerchant);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveMerchantReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }

    //#endregion
    //#region UIInitialize 

    Feather.replace();
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.ContainerHeight = window.innerHeight;

    // this._HelperService._InitMap();
    this.InitBackDropClickEvent();

    this.HideStoreDetail();
    this.acquireDetails = this._HelperService.GetStorage('acquirerdetails');
    if (this.acquireDetails.ProgramDetails && this.acquireDetails.ProgramDetails.length > 0) {
      this.currentProgramDetails = this.acquireDetails.ProgramDetails[0];
      this.GetProgramDetails();
    }
    if (this._HelperService.acquirerProgramDetails && this._HelperService.acquirerProgramDetails.length > 0) {
      this.currentProgramDetails = this._HelperService.acquirerProgramDetails[0];
      this.GetProgramDetails();

    }
    //#endregion

    //#region InitEditForm 

    // this.FormA_EditUser_Load();
    // this.FormB_EditUser_Load();
    // this.FormC_EditUser_Load();
    // this.GetStateCategories();

    //#endregion

    // this.GetMerchantDetails();
    // this.disableOuterClick();

  }
  
  closeModal(data){
    if(data=='EditComission'){
      this.comission=this.programDetails.Comission;
    }
    else if(data=='EditReward'){
      this.reward=this.programDetails.RewardCap;
    }
    else if(data=='EditMinInvoiceAmt'){
      this.minInvoiceAmt = this.programDetails.MinInvoiceAmount;

    }
    else if(data=='EditMaxComission'){
      this.MaxComission=this.programDetails.MaxComission;
    }
    else if(data=='EditMaxReward'){
      this.MaxRewards =this.programDetails.MaxRewards;
    }
    else if (data=='EditCashierIncentive'){
      this.IncentivePercentage=this.programDetails.CashierCommission;
      this.IncentiveMaxAmount=this.programDetails.CashierMaxReward;
    }

  }
  openComissionDialogue() {
    this.comission = this.programDetails.comission || 0;
    this.disableButton=true;
    this._HelperService.OpenModal('EditComission');
    this.comission = this.programDetails.Comission;
    $('#EditComission').modal({backdrop: 'static', keyboard: false});  
  }

  openRewardDialogue() {
    this.reward = this.programDetails.RewardCap || 0;
    this.disableButton=true;
    this._HelperService.OpenModal('EditReward')
    $('#EditReward').modal({backdrop: 'static', keyboard: false}); 
  }

  openInvoiceAmtDialogue() {
    this.minInvoiceAmt = this.programDetails.MinInvoiceAmount || 0;
    this.disableButton=true;
    this._HelperService.OpenModal('EditMinInvoiceAmt');
    $('#EditMinInvoiceAmt').modal({backdrop: 'static', keyboard: false}); 
  }

  openMaxComission() {
    this.MaxComission = this.programDetails.MaxComission || 0;
    this.disableButton=true;
    this._HelperService.OpenModal('EditMaxComission');
    $('#EditMaxComission').modal({backdrop: 'static', keyboard: false});
  }

  openMaxReward() {
    this.MaxRewards = this.programDetails.MaxRewards || 0;
    this.disableButton=true;
    this._HelperService.OpenModal('EditMaxReward');
    $('#EditMaxReward').modal({backdrop: 'static', keyboard: false}); 
  }

  openIncentiveDialogue() {
    this.IncentiveMaxAmount = this.programDetails.CashierMaxReward || 0;
    this.IncentivePercentage = this.programDetails.CashierCommission || 0;
    this.disableButton=true;
    this._HelperService.OpenModal('EditCashierIncentive');
    $('#EditCashierIncentive').modal({backdrop: 'static', keyboard: false});  
  }
  disableButton:boolean=true;
  validateinput(event){
    if(event.target.value=="" || event.target.value < 0){
      this.disableButton=false;
    }
    else{
      this.disableButton=true;
    }

  }
  typeMessage:any;
  updateProgram(type) {
    if(type){
      if(type=='reward'){
        this.typeMessage='Reward'
      }
      if(type=='comission'){
        this.typeMessage='Commission'
      }
      
      if(type=='minInvoiceAmt'){
        this.typeMessage='Invoice Amount'
      }
      if(type=='MaxComission'){
        this.typeMessage='Max Commission'
      }
      if(type=='MaxRewards'){
        this.typeMessage='Max Rewards'
      }
      if(type=='CashierIncentive'){
        this.typeMessage='Cashier Incentive'
      }
      
    }
    if (type === 'MaxComission') {
      if (this.MaxComission < 0 || this.MaxComission > 2000) {
        this._HelperService.NotifyError("Max Commission should be between 0 to 2000");
        return;
      }
    }

    if (type === 'MaxRewards') {
      if (this.MaxRewards < 0 || this.MaxRewards > 5000) {
        this._HelperService.NotifyError("Max Reward should be between 0 to 5000");
        return;
      }
    }

    if (type === 'comission') {
      if (this.comission < 0 || this.comission > 100) {
        this._HelperService.NotifyError("Commission percentage should be between 0 to 100");
        return;
      }
    } else {
      if (this.reward < 0 || this.reward > 100) {
        this._HelperService.NotifyError("Reward Percentage should be between 0 to 100");
        return;
      }
    }

    if (type === 'minInvoiceAmt') {
      if (this.minInvoiceAmt < 5000) {
        this._HelperService.NotifyError("Min invoice amount should be greater than or equal to 5000");
        return;
      }
    }

    if (type === 'CashierIncentive') {
      if (this.IncentiveMaxAmount < 0) {
        this._HelperService.NotifyError("Incentive maximum amount should be greater than 0");
        return;
      }

      if (this.IncentivePercentage < 0 || this.IncentivePercentage > 100) {
        this._HelperService.NotifyError("Incentive commission Percentage should be between 0 to 100");
        return;
      }
    }



    const pdata = {
      Task: "updateprogram",
      ReferenceId: this.programDetails.ReferenceId,
      ReferenceKey: this.programDetails.ReferenceKey,
      ProgramTypeCode: "programtype.grouployalty",
      SubscriptionId: this.programDetails.SubscriptionId,
      ComissionTypeCode: "commissiontype.zero",
      Comission: this.comission,
      RewardCap: this.reward,
      MinInvoiceAmount: this.minInvoiceAmt,
      MaxComission: this.MaxComission,
      MaxRewards: this.MaxRewards,
      CashierCommission: this.IncentivePercentage,
      CashierMaxReward: this.IncentiveMaxAmount
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Program, pdata);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.toogleIsFormProcessing(false);
          this.isLoaded = true;
          this._HelperService.NotifySuccess(this.typeMessage+" "+"updated successfully!");
          this.disableButton=true;
          this.GetProgramDetails();
          if (type === 'comission') {
            this._HelperService.CloseModal('EditComission')
          } else if (type === 'minInvoiceAmt') {
            this._HelperService.CloseModal('EditMinInvoiceAmt')
          } else if (type === 'MaxComission') {
            this._HelperService.CloseModal('EditMaxComission')
          }
          else if (type === 'MaxRewards') {
            this._HelperService.CloseModal('EditMaxReward')
          }
          else if (type === 'CashierIncentive') {
            this._HelperService.CloseModal('EditCashierIncentive')
          }
          else if (type === 'reward') {
            this._HelperService.CloseModal('EditReward')
          }
          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  saveReward() {

  }

  //#region MerchantDetails 

  public _UserAccount: OUserDetails = {
    ContactNumber: null,
    SecondaryEmailAddress: null,
    ReferenceId: null,
    BankDisplayName: null,
    BankKey: null,
    SubOwnerAddress: null,
    SubOwnerLatitude: null,
    SubOwnerDisplayName: null,
    SubOwnerKey: null,
    SubOwnerLongitude: null,
    AccessPin: null,
    LastLoginDateS: null,
    AppKey: null,
    AppName: null,
    AppVersionKey: null,
    CreateDate: null,
    CreateDateS: null,
    CreatedByDisplayName: null,
    CreatedByIconUrl: null,
    CreatedByKey: null,
    Description: null,
    IconUrl: null,
    ModifyByDisplayName: null,
    ModifyByIconUrl: null,
    ModifyByKey: null,
    ModifyDate: null,
    ModifyDateS: null,
    PosterUrl: null,
    ReferenceKey: null,
    StatusCode: null,
    StatusI: null,
    StatusId: null,
    StatusName: null,
    AccountCode: null,
    AccountOperationTypeCode: null,
    AccountOperationTypeName: null,
    AccountTypeCode: null,
    AccountTypeName: null,
    Address: null,
    AppVersionName: null,
    ApplicationStatusCode: null,
    ApplicationStatusName: null,
    AverageValue: null,
    CityAreaKey: null,
    CityAreaName: null,
    CityKey: null,
    CityName: null,
    CountValue: null,
    CountryKey: null,
    CountryName: null,
    DateOfBirth: null,
    DisplayName: null,
    EmailAddress: null,
    EmailVerificationStatus: null,
    EmailVerificationStatusDate: null,
    FirstName: null,
    GenderCode: null,
    GenderName: null,
    LastLoginDate: null,
    LastName: null,
    Latitude: null,
    Longitude: null,
    MobileNumber: null,
    Name: null,
    NumberVerificationStatus: null,
    NumberVerificationStatusDate: null,
    OwnerDisplayName: null,
    OwnerKey: null,
    Password: null,
    Reference: null,
    ReferralCode: null,
    ReferralUrl: null,
    RegionAreaKey: null,
    RegionAreaName: null,
    RegionKey: null,
    RegionName: null,
    RegistrationSourceCode: null,
    RegistrationSourceName: null,
    RequestKey: null,
    RoleKey: null,
    RoleName: null,
    SecondaryPassword: null,
    SystemPassword: null,
    UserName: null,
    WebsiteUrl: null
  };

  public _MerchantDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      Latitude: null,
      Longitude: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
      Address: null,
      AddressComponent:
      {
        Latitude: 0,
        Longitude: 0,
      },

    }

  GetMerchantDetails() {
    this._HelperService.IsFormProcessing = true;
    this.isLoaded = false;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchant,
      ReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.toogleIsFormProcessing(false);
          this._MerchantDetails = _Response.Result;
          this.isLoaded = true;
          this._Address = this._MerchantDetails.AddressComponent;
          this._isAddressLoaded = true;
          //#region RelocateMarker 

          setTimeout(() => {
            this._isAddressLoaded = true;
          }, 300);

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          // this.FormC_EditUser_Latitude = this._MerchantDetails.Latitude;
          // this.FormC_EditUser_Longitude = this._MerchantDetails.Longitude;
          // this.FormC_EditUser_Latitude = _Response.Result.Latitude;
          // this.FormC_EditUser_Longitude = _Response.Result.Longitude;
          if (this._MerchantDetails != undefined && this._MerchantDetails.MobileNumber != undefined && this._MerchantDetails.MobileNumber != null) {
            if (this._MerchantDetails.MobileNumber.startsWith("234") || this._MerchantDetails.MobileNumber.startsWith("233") || this._MerchantDetails.MobileNumber.startsWith("254")) {
              this._MerchantDetails.MobileNumber = this._MerchantDetails.MobileNumber.substring(3, this._MerchantDetails.length);
            }
          }

          if (this._MerchantDetails.ContactPerson != undefined && this._MerchantDetails.ContactPerson.MobileNumber != undefined && this._MerchantDetails.ContactPerson.MobileNumber != null) {
            if (this._MerchantDetails.ContactPerson.MobileNumber.startsWith("234") || this._MerchantDetails.ContactPerson.MobileNumber.startsWith("233") || this._MerchantDetails.ContactPerson.MobileNumber.startsWith("254")) {
              this._MerchantDetails.ContactPerson.MobileNumber = this._MerchantDetails.ContactPerson.MobileNumber.substring(3, this._MerchantDetails.ContactPerson.length);
            }
          }
          // this._HelperService._ReLocate();

          //#endregion

          //#region DatesAndStatusInit 

          this._MerchantDetails.StartDateS = this._HelperService.GetDateS(
            this._MerchantDetails.StartDate
          );
          this._MerchantDetails.EndDateS = this._HelperService.GetDateS(
            this._MerchantDetails.EndDate
          );
          this._MerchantDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._MerchantDetails.CreateDate
          );
          this._MerchantDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._MerchantDetails.ModifyDate
          );
          this._MerchantDetails.StatusI = this._HelperService.GetStatusIcon(
            this._MerchantDetails.StatusCode
          );
          this._MerchantDetails.StatusB = this._HelperService.GetStatusBadge(
            this._MerchantDetails.StatusCode
          );
          this._MerchantDetails.StatusC = this._HelperService.GetStatusColor(
            this._MerchantDetails.StatusCode
          );


          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  GetProgramDetails() {
    this._HelperService.IsFormProcessing = true;
    this.isLoaded = false;
    var pData = {
      Task: "getprogram",
      ReferenceKey: this.currentProgramDetails.ProgramReferenceKey,
      ReferenceId: this.currentProgramDetails.ProgramId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Program, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.toogleIsFormProcessing(false);
          this.programDetails = _Response.Result;
          this.reward = this.programDetails.RewardCap;
          this.comission = this.programDetails.Comission;
          this.minInvoiceAmt = this.programDetails.MinInvoiceAmount;
          this.MaxRewards = this.programDetails.MaxRewards;
          this.MaxComission = this.programDetails.MaxComission;
          this.IncentiveMaxAmount = this.programDetails.CashierMaxReward;
          this.IncentivePercentage = this.programDetails.CashierCommission;
          this.isLoaded = true;
          this.programDetails.StartDateS = this._HelperService.GetDateS(
            this.programDetails.CreateDate
          );

          this.programDetails.StatusI = this._HelperService.GetStatusIcon(
            this.programDetails.StatusCode
          );
          this.programDetails.StatusB = this._HelperService.GetStatusBadge(
            this.programDetails.StatusCode
          );
          this.programDetails.StatusC = this._HelperService.GetStatusColor(
            this.programDetails.StatusCode
          );
          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


  //#endregion

  //#region EditUser

  //#region EditUserFormA 

  // FormA_EditUser: FormGroup;

  // FormA_EditUser_Show() {
  //   this._HelperService.OpenModal("FormA_EditUser_Content");
  // }
  // FormA_EditUser_Close() {
  //   var backdrop: HTMLElement = document.getElementById("backdrop");
  //   $(this.divView.nativeElement).removeClass('show');
  //   backdrop.classList.remove("show");
  // }
  // FormA_EditUser_Load() {
  //   this._HelperService._FileSelect_Icon_Data.Width = 128;
  //   this._HelperService._FileSelect_Icon_Data.Height = 128;

  //   this._HelperService._FileSelect_Poster_Data.Width = 800;
  //   this._HelperService._FileSelect_Poster_Data.Height = 400;

  //   this.FormA_EditUser = this._FormBuilder.group({
  //     OperationType: 'new',
  //     ReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
  //     ReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
  //     Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
  //     AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
  //     AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
  //     RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
  //     OwnerKey: this._HelperService.AppConfig.ActiveReferenceKey,
  //     OwnerName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
  //     DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
  //     Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
  //     ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
  //     EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
  //     AccountPercentage: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])]

  //     // RewardPercentage: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])]
  //   });
  // }

  // FormA_EditUser_Process(_FormValue: any) {
  //   this._HelperService.IsFormProcessing = true;
  //   let _OResponse: Observable<OResponse>;
  //   _OResponse = this._HelperService.PostData(
  //     this._HelperService.AppConfig.NetworkLocation.V2.System,
  //     _FormValue
  //   );
  //   _OResponse.subscribe(
  //     _Response => {
  //       this.toogleIsFormProcessing(false);
  //       if (_Response.Status == this._HelperService.StatusSuccess) {
  //         this._HelperService.NotifySuccess("Account updated successfully");
  //         this.Forms_EditUser_Close();
  //       } else {
  //         this._HelperService.NotifyError(_Response.Message);
  //       }
  //     },
  //     _Error => {
  //       this.toogleIsFormProcessing(false);
  //       this._HelperService.HandleException(_Error);
  //     }
  //   );
  // }

  //#endregion

  //#region EditUserFormB 

  // FormB_EditUser: FormGroup;
  // FormB_EditUser_Address: string = null;
  // FormB_EditUser_Latitude: number = 0;
  // FormB_EditUser_Longitude: number = 0;


  // FormB_EditUser_Show() {
  //   this._HelperService.OpenModal("FormB_EditUser_Content");
  // }

  // FormB_EditUser_Close() {
  //   var backdrop: HTMLElement = document.getElementById("backdrop");
  //   $(this.divView.nativeElement).removeClass('show');
  //   backdrop.classList.remove("show");
  // }
  // FormB_EditUser_Load() {
  //   this._HelperService._FileSelect_Icon_Data.Width = 128;
  //   this._HelperService._FileSelect_Icon_Data.Height = 128;

  //   this._HelperService._FileSelect_Poster_Data.Width = 800;
  //   this._HelperService._FileSelect_Poster_Data.Height = 400;

  //   this.FormB_EditUser = this._FormBuilder.group({
  //     OperationType: 'new',
  //     ReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
  //     ReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
  //     Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
  //     AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
  //     AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
  //     RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
  //     OwnerKey: this._HelperService.AppConfig.ActiveReferenceKey,

  //     MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
  //     FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2)])],
  //     // LastName: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
  //     SecondaryEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
  //   });
  // }

  // FormB_EditUser_Process(_FormValue: any) {
  //   // _FormValue.DisplayName = _FormValue.FirstName;
  //   // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
  //   this.toogleIsFormProcessing(true);
  //   let _OResponse: Observable<OResponse>;
  //   _OResponse = this._HelperService.PostData(
  //     this._HelperService.AppConfig.NetworkLocation.V2.System,
  //     _FormValue
  //   );
  //   _OResponse.subscribe(
  //     _Response => {
  //       this.toogleIsFormProcessing(false);
  //       if (_Response.Status == this._HelperService.StatusSuccess) {
  //         this._HelperService.NotifySuccess("Account updated successfully");
  //         if (_FormValue.OperationType == "edit") {
  //           this.Forms_EditUser_Close();
  //         }
  //       } else {
  //         this._HelperService.NotifyError(_Response.Message);
  //       }
  //     },
  //     _Error => {
  //       this.toogleIsFormProcessing(false);
  //       this._HelperService.HandleException(_Error);
  //     }
  //   );
  // }

  // //#endregion

  // //#region EditUserFormC 

  // FormC_EditUser: FormGroup;
  // FormC_EditUser_Address: string = null;
  // FormC_EditUser_Latitude: number = 0;
  // FormC_EditUser_Longitude: number = 0;


  // @ViewChild('placesStore') placesStore: GooglePlaceDirective;
  // _CurrentAddress: any = {};
  // FormC_EditUser_PlaceMarkerClick(event) {
  //   this.FormC_EditUser_Latitude = event.coords.lat;
  //   this.FormC_EditUser_Longitude = event.coords.lng;

  // }
  // public FormC_EditUser_AddressChange(address: Address) {
  //   this.FormC_EditUser_Latitude = address.geometry.location.lat();
  //   this.FormC_EditUser_Longitude = address.geometry.location.lng();
  //   this.FormC_EditUser_Address = address.formatted_address;
  //   // this.FormC_EditUser.controls['Address'].setValue(address.formatted_address);
  //   this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
  //   if (this._CurrentAddress.country != this._HelperService.UserCountrycode) {
  //     this._HelperService.NotifyError('Currently we’re not serving in this area, please add locality within ' + this._HelperService.UserCountrycode);
  //     this.reset();

  //   }
  //   else {
  //     this.FormC_EditUser.controls['Address'].setValue(address.formatted_address);
  //     this.FormC_EditUser.controls['MapAddress'].setValue(address.formatted_address);
  //   }
  // }

  // FormC_EditUser_Show() {
  //   this._HelperService.OpenModal("FormC_EditUser_Content");
  // }

  // FormC_EditUser_Load() {
  //   this._HelperService._FileSelect_Icon_Data.Width = 128;
  //   this._HelperService._FileSelect_Icon_Data.Height = 128;

  //   this._HelperService._FileSelect_Poster_Data.Width = 800;
  //   this._HelperService._FileSelect_Poster_Data.Height = 400;

  //   this.FormC_EditUser = this._FormBuilder.group({
  //     OperationType: 'new',
  //     ReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
  //     ReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
  //     Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
  //     AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
  //     AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
  //     RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
  //     OwnerKey: this._HelperService.AppConfig.ActiveReferenceKey,



  //     CountryName: this._HelperService.UserCountrycode,
  //     CityId: null,
  //     CityCode: null,
  //     CityName: null,
  //     // CityAreaId: null,
  //     // CityAreaCode: null,
  //     // CityAreaName: null,
  //     StateId: null,
  //     StateCode: null,
  //     StateName: null,


  //     // Address: [null, Validators.compose([Validators.required, Validators.maxLength(256), Validators.minLength(2)])],
  //     Address: [null],
  //     Latitude: 0,
  //     Longitude: 0,
  //     Configuration: [],
  //   });
  // }

  // FormC_EditUser_Process(_FormValue: any) {
  //   // _FormValue.DisplayName = _FormValue.FirstName;
  //   // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
  //   // _FormValue.Longitude = this.FormC_EditUser_Longitude;
  //   // _FormValue.Latitude = this.FormC_EditUser_Latitude;

  //   _FormValue.Address = this._Address.Address;
  //   _FormValue.AddressComponent = this._Address;

  //   if (this._Address.CityId < 1) {
  //     this._HelperService.NotifyError("Please enter address");
  //   }
  //   else {
  //     _FormValue.Address = this._Address.Address;
  //     this.toogleIsFormProcessing(true);
  //     let _OResponse: Observable<OResponse>;
  //     _OResponse = this._HelperService.PostData(
  //       this._HelperService.AppConfig.NetworkLocation.V2.System,
  //       _FormValue
  //     );
  //     _OResponse.subscribe(
  //       _Response => {
  //         this.toogleIsFormProcessing(false);
  //         if (_Response.Status == this._HelperService.StatusSuccess) {
  //           this._HelperService.NotifySuccess("Account updated successfully");
  //           if (_FormValue.OperationType == "edit") {
  //             this.Forms_EditUser_Close();
  //           }
  //         } else {
  //           this._HelperService.NotifyError(_Response.Message);
  //         }
  //       },
  //       _Error => {
  //         this.toogleIsFormProcessing(false);
  //         this._HelperService.HandleException(_Error);
  //       }
  //     );
  //   }

  // }

  // //#endregion
  // Forms_EditUser_Close() {
  //   this.GetMerchantDetails();
  //   var backdrop: HTMLElement = document.getElementById("backdrop");
  //   $(this.divView.nativeElement).removeClass('show');
  //   backdrop.classList.remove("show");
  // }
  // //#endregion

  // //#region BackdropDismiss 

  // @ViewChild("offCanvas") divView: ElementRef;

  // clicked() {
  //   $(this.divView.nativeElement).addClass('show');
  //   var backdrop: HTMLElement = document.getElementById("backdrop");
  //   backdrop.classList.add("show");
  // }
  // unclick() {
  //   $(this.divView.nativeElement).removeClass('show');
  //   var backdrop: HTMLElement = document.getElementById("backdrop");
  //   backdrop.classList.remove("show");
  // }

  //#endregion



  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
    //    this._ChangeDetectorRef.detectChanges();
  }

  // FormA_EditUser_Block() {

  // }
  // FormC_EditUser_Block() {

  // }

  // FormB_EditUser_Block() {

  // }

  // BlockAccount() {
  //   swal({
  //     title: this._HelperService.AppConfig.CommonResource.BlockMerchantTitle,
  //     text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
  //     position: this._HelperService.AppConfig.Alert_Position,
  //     animation: this._HelperService.AppConfig.Alert_AllowAnimation,
  //     customClass: this._HelperService.AppConfig.Alert_Animation,
  //     allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
  //     allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
  //     confirmButtonColor: this._HelperService.AppConfig.Color_Red,
  //     cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
  //     confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
  //     cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
  //     showCancelButton: true,
  //     html:
  //       ' <label> Do you really want to block this merchant?  </label>' +
  //       '<input type="text"  placeholder="Enter Comment"  maxLength= "128" id="swal-input1" class="swal2-input">' +
  //       '<input type="password" placeholder="Enter Pin"     maxLength= "4" id="swal-input2" class="swal2-input">',
  //     focusConfirm: false,
  //     preConfirm: () => {
  //       return [
  //         document.getElementById('swal-input1')['value'],
  //         document.getElementById('swal-input2')['value']
  //       ]
  //     },
  //     inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
  //     // inputAttributes: {
  //     //   autocapitalize: 'off',
  //     //   autocorrect: 'off',
  //     //   maxLength: "4",
  //     //   minLength: "4"
  //     // },

  //   }).then((result) => {
  //     if (result.value) {
  //       if (result.value[1].length < 4) {
  //         this._HelperService.NotifyError('Enter your 4 digit pin');
  //         return;
  //       }

  //       this._HelperService.IsFormProcessing = true;
  //       var PostData = {
  //         Task: "updateaccountstatus",
  //         AccountId: this._MerchantDetails.ReferenceId,
  //         AccountKey: this._MerchantDetails.ReferenceKey,
  //         StatusCode: "default.blocked",
  //         AuthPin: result.value[1],
  //         Comment: result.value[0],
  //         AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant

  //       };

  //       let _OResponse: Observable<OResponse>;

  //       _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, PostData);
  //       _OResponse.subscribe(
  //         _Response => {
  //           this._HelperService.IsFormProcessing = false;
  //           if (_Response.Status == this._HelperService.StatusSuccess) {
  //             this._HelperService.NotifySuccess("Merchant Blocked Successfully");
  //             this.GetMerchantDetails();
  //           } else {
  //             this._HelperService.NotifySuccess(_Response.Message);
  //           }
  //         },
  //         _Error => {
  //           this._HelperService.IsFormProcessing = false;
  //           this._HelperService.HandleException(_Error);
  //         }
  //       );
  //     }
  //   });


  // }
  // UnblockBlockAccount() {
  //   swal({
  //     title: this._HelperService.AppConfig.CommonResource.UnBlockMerchantTitle,
  //     text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
  //     position: this._HelperService.AppConfig.Alert_Position,
  //     animation: this._HelperService.AppConfig.Alert_AllowAnimation,
  //     customClass: this._HelperService.AppConfig.Alert_Animation,
  //     allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
  //     allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
  //     confirmButtonColor: this._HelperService.AppConfig.Color_Red,
  //     cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
  //     confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
  //     cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
  //     showCancelButton: true,
  //     html:
  //       ' <label> Do you really want to unblock this merchant?  </label>' +
  //       '<input type="text" placeholder="Enter Comment" maxLength = "128"  id="swal-input1" class="swal2-input">' +
  //       '<input type="password" placeholder="Enter Pin" maxLength = "4" id="swal-input2" class="swal2-input">',


  //     focusConfirm: false,
  //     preConfirm: () => {
  //       return [
  //         document.getElementById('swal-input1')['value'],
  //         document.getElementById('swal-input2')['value']
  //       ]
  //     },
  //     inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
  //     //   inputAttributes: {
  //     //     autocapitalize: 'off',
  //     //     autocorrect: 'off',
  //     //     maxLength: "4",
  //     //     minLength: "4"
  //     //   },
  //     //   inputValidator:function(value) {
  //     //     if(value === '' || value.length < 4) { 
  //     //       return 'Enter your 4 digit pin!'
  //     //     }
  //     // },
  //   }).then((result) => {
  //     if (result.value) {
  //       if (result.value[1].length < 4) {
  //         this._HelperService.NotifyError('Enter your 4 digit pin');
  //         return;
  //       }
  //       this._HelperService.IsFormProcessing = true;
  //       var PostData = {
  //         Task: "updateaccountstatus",
  //         AccountId: this._MerchantDetails.ReferenceId,
  //         AccountKey: this._MerchantDetails.ReferenceKey,
  //         StatusCode: "default.active",
  //         AuthPin: result.value[1],
  //         Comment: result.value[0],
  //         AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant

  //       };

  //       let _OResponse: Observable<OResponse>;

  //       _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, PostData);
  //       _OResponse.subscribe(
  //         _Response => {
  //           this._HelperService.IsFormProcessing = false;
  //           if (_Response.Status == this._HelperService.StatusSuccess) {
  //             this._HelperService.NotifySuccess("Merchant UnBlocked Successfully");
  //             this.GetMerchantDetails();
  //           } else {
  //             this._HelperService.NotifySuccess(_Response.Message);
  //           }
  //         },
  //         _Error => {
  //           this._HelperService.IsFormProcessing = false;
  //           this._HelperService.HandleException(_Error);
  //         }
  //       );
  //     }
  //   });


  // }



  // public ShowstateSelector: boolean = true;
  // public ShowcitySelector: boolean = true;
  // //state
  // public StateCategories = [];
  // public S2StateCategories = [];

  // GetStateCategories() {

  //   this._HelperService.ToggleField = true;
  //   var PData =
  //   {
  //     Task: this._HelperService.AppConfig.Api.Core.getstates,
  //     ReferenceKey: this._HelperService.UserCountrykey,
  //     ReferenceId: this._HelperService.UserCountryId,
  //     //SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
  //     // SortExpression: 'Name asc',
  //     Offset: 0,
  //     Limit: 1000,
  //   }
  //   // PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
  //   //     PData.SearchCondition,
  //   //     "TypeCode",
  //   //     this._HelperService.AppConfig.DataType.Text,
  //   //     this._HelperService.AppConfig.HelperTypes.MerchantCategories,
  //   //     "="
  //   // );
  //   let _OResponse: Observable<OResponse>;
  //   _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.State, PData);
  //   _OResponse.subscribe(
  //     _Response => {
  //       if (_Response.Status == this._HelperService.StatusSuccess) {
  //         if (_Response.Result.Data != undefined) {
  //           this.StateCategories = _Response.Result.Data;

  //           this.ShowstateSelector = false;
  //           this._ChangeDetectorRef.detectChanges();
  //           this.S2StateCategories.push(
  //             {
  //               id: 0,
  //               key: "0",
  //               text: "Select State"
  //             }
  //           );
  //           for (let index = 0; index < this.StateCategories.length; index++) {
  //             const element = this.StateCategories[index];
  //             this.S2StateCategories.push(
  //               {
  //                 id: element.ReferenceId,
  //                 key: element.ReferenceKey,
  //                 text: element.Name
  //               }
  //             );
  //           }
  //           this.ShowstateSelector = true;
  //           this._ChangeDetectorRef.detectChanges();

  //           this._HelperService.ToggleField = false;

  //         }
  //       }
  //       else {
  //         this._HelperService.NotifyError(_Response.Message);
  //       }
  //     },
  //     _Error => {
  //       this._HelperService.IsFormProcessing = false;
  //       this._HelperService.HandleException(_Error);
  //       this._HelperService.ToggleField = false;

  //     });
  // }
  // public SelectedStateCategories = [];
  // public selectedstate: any;
  // statekey: any; stateid: any; statename: any;
  // StateSelected(Items) {
  //   if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
  //     this.SelectedStateCategories = Items.value;
  //     this.statekey = Items.data[0].key;
  //     this.stateid = Items.data[0].id;
  //     this.statename = Items.data[0].text;
  //     this.selectedstate = true;
  //     this.GetCityCategories()

  //     this.FormC_EditUser.controls['StateCode'].patchValue(this.statekey);
  //     this.FormC_EditUser.controls['StateId'].patchValue(this.stateid);
  //     this.FormC_EditUser.controls['StateName'].patchValue(this.statename);
  //   }
  //   else {
  //     this.SelectedStateCategories = [];
  //   }
  // }


  //City
  // public CityCategories = [];
  // public S2CityCategories = [];

  // GetCityCategories() {


  //   this._HelperService.ToggleField = true;
  //   var PData =
  //   {
  //     Task: this._HelperService.AppConfig.Api.Core.getcities,
  //     ReferenceKey: this.statekey,
  //     ReferenceId: this.stateid,
  //     //SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
  //     // SortExpression: 'Name asc',
  //     Offset: 0,
  //     Limit: 1000,
  //   }
  //   // PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
  //   //     PData.SearchCondition,
  //   //     "TypeCode",
  //   //     this._HelperService.AppConfig.DataType.Text,
  //   //     this._HelperService.AppConfig.HelperTypes.MerchantCategories,
  //   //     "="
  //   // );
  //   let _OResponse: Observable<OResponse>;
  //   _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.City, PData);
  //   _OResponse.subscribe(
  //     _Response => {
  //       if (_Response.Status == this._HelperService.StatusSuccess) {
  //         if (_Response.Result.Data != undefined) {
  //           this.CityCategories = _Response.Result.Data;
  //           this.ShowcitySelector = false;
  //           this._ChangeDetectorRef.detectChanges();
  //           this.S2CityCategories = [];
  //           for (let index = 0; index < this.CityCategories.length; index++) {
  //             const element = this.CityCategories[index];
  //             this.S2CityCategories.push(
  //               {
  //                 id: element.ReferenceId,
  //                 key: element.ReferenceKey,
  //                 text: element.Name
  //               }
  //             );
  //           }
  //           this.ShowcitySelector = true;
  //           this._ChangeDetectorRef.detectChanges();

  //           this._HelperService.ToggleField = false;

  //         }
  //       }
  //       else {
  //         this._HelperService.NotifyError(_Response.Message);
  //       }
  //     },
  //     _Error => {
  //       this._HelperService.IsFormProcessing = false;
  //       this._HelperService.HandleException(_Error);
  //       this._HelperService.ToggleField = false;

  //     });


  // }
  // public SelectedCityCategories = [];
  // public citykey: any; public cityid: any; cityname: any;
  // CitySelected(Items) {
  //   if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
  //     this.SelectedCityCategories = Items.value;
  //     this.citykey = Items.data[0].key;
  //     this.cityid = Items.data[0].id
  //     this.cityname = Items.data[0].text
  //     this.FormC_EditUser.controls['CityCode'].patchValue(this.citykey);
  //     this.FormC_EditUser.controls['CityId'].patchValue(this.cityid);
  //     this.FormC_EditUser.controls['CityName'].patchValue(this.cityname);
  //   }
  //   else {
  //     this.SelectedCityCategories = [];
  //   }
  // }

  // removeCategory1(): void {
  //   this._MerchantDetails.UpdateCategories.splice(1);
  //   this.reset()
  // }
  // reset() {
  //   this.FormC_EditUser.controls['Address'].reset()
  //   this.FormC_EditUser.controls['MapAddress'].reset()
  //   this.FormC_EditUser.controls['CityName'].reset()
  //   this.FormC_EditUser.controls['StateName'].reset()
  //   this.FormC_EditUser.controls['CountryName'].reset()

  // }
}
export class OAccountOverview {
  public Merchants: number;
  public Stores: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
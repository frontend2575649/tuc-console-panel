import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TubankloyaltyComponent } from './tubankloyalty.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
// import { LeafletModule } from '@asymmetrik/ngx-leaflet';
// import { LeafletMarkerClusterModule } from '@asymmetrik/ngx-leaflet-markercluster';
import { MainPipe } from '../../../service/main-pipe.module'
import { HCXAddressManagerModule } from '../../../component/hcxaddressmanager/hcxaddressmanager.component';

const routes: Routes = [
  {
    path: "",
    component: TubankloyaltyComponent,
    children: [
      // { path: "/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../../Acquirer/dashboards/merchant/acquirer/dashboard.module#TUDashboardModule" },
      { path: "dashboard/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../loyaltybankoverview/loyaltybankoverview.module#LoyaltybankoverviewModule" },
      { path: 'merchant/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Merchant' }, loadChildren: '../bankloyaltymerchant/bankloyaltymerchant.module#BankloyaltymerchantModule' },
      { path: 'transactions/:referencekey/:referenceid', data: { 'permission': 'terminals', PageName: 'System.Menu.Merchant' }, loadChildren: '../bankloyaltytransaction/bankloyaltytransaction.module#BankloyaltytransactionModule' },
      // { path: 'stores/:referencekey/:referenceid', data: { 'permission': 'stores', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../Acquirer/account/tustores/merchant/tustores.module#TUStoresModule' }
      { path: 'rewardtransactions/:referencekey/:referenceid', data: { 'permission': 'terminals', PageName: 'System.Menu.Merchant' }, loadChildren: '../bankloyaltyrewardtransaction/bankloyaltyrewardtransaction.module#BankloyaltyrewardtransactionModule' },
      { path: 'redeemtransactions/:referencekey/:referenceid', data: { 'permission': 'terminals', PageName: 'System.Menu.Merchant' }, loadChildren: '../bankloyaltyredeemtransaction/bankloyaltyredeemtransaction.module#BankloyaltyredeemtransactionModule' },]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUBankLoyaltyRoutingModule { }


@NgModule({
  declarations: [TubankloyaltyComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    TUBankLoyaltyRoutingModule,
    GooglePlaceModule,
    MainPipe,
    HCXAddressManagerModule,
    AgmCoreModule.forRoot({
        apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
    }),  ]
})
export class TubankloyaltyModule { }

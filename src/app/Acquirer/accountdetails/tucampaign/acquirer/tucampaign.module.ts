import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { AgmCoreModule } from '@agm/core';
import { TuCampaignComponent } from "./tucampaign.component";
import { MainPipe } from 'src/app/service/main-pipe.module';
import { DynamicRoutesguardGuard } from "src/app/service/guard/dynamicroutes.guard";
const routes: Routes = [
    {
        path: "",
        component: TuCampaignComponent,
        canActivateChild:[DynamicRoutesguardGuard],
        data:{accessName:['acqviewcampaign']},
        children: [
            // { path: "/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../dashboards/campaign/acquirer/dashboard.module#TUDashboardModule" },

            // { path: "dashboard/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../dashboards/campaign/acquirer/dashboard.module#TUDashboardModule" },

            { path: 'sales/saleshistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.SaleHistory' }, loadChildren: '../../../../Acquirer/campaigns/sales/tusale.module#TUSaleModule' },

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUCampaignRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUCampaignRoutingModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        MainPipe
    ],
    declarations: [TuCampaignComponent]
})
export class TUCampaignModule { }

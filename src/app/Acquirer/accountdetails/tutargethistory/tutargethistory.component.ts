import { ChangeDetectorRef, Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { Subscription, Observable } from "rxjs";
import { DataHelperService, HelperService, OList, OSelect, OResponse, FilterHelperService } from "../../../service/service";
declare var moment: any;
declare var $: any;
import swal from "sweetalert2";


@Component({
  selector: "tu-targethistory",
  templateUrl: "./tutargethistory.component.html"
})
export class TUTargetHistoryComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    this.ReloadSubscription.unsubscribe();
  }

  Type: number
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService

  ) {
    this._HelperService.ShowDateRange = false;
  }

  ReloadSubscription: Subscription;

  ngOnInit() {
   
    Feather.replace();
    this._HelperService.StopClickPropogation();

    this._ActivatedRoute.params.subscribe((params: Params) => {
      this.Type = params["type"]
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"]; //RM Key
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid']; //RM Id
      if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
        this.TargetHistoryList_Filter_Owners_Load();
        this.Form_UpdateTarget_Load();
        this.TargetHistoryList_Setup();
      } else {
        // this._HelperService.Get_UserAccountDetails(true);
        this.TargetHistoryList_Filter_Owners_Load();
        this.Form_UpdateTarget_Load();
        this.TargetHistoryList_Setup();
      }
    });
    this.ReloadSubscription = this._HelperService.ReloadEventEmit.subscribe((number) => {
      this.TargetHistoryList_GetData();
    });

    this._HelperService.StopClickPropogationTarget();
  }




  //#region Targets List 

  public TargetHistoryList_Config: OList;
  TargetHistoryList_Setup() {
    var SearchCondition = undefined;
    if (this._HelperService.AppConfig.ActiveReferenceId != 0 && this._HelperService.AppConfig.ActiveReferenceKey != undefined && this._HelperService.AppConfig.ActiveReferenceKey != null && this._HelperService.AppConfig.ActiveReferenceKey != "") {
      SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');
      SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'OwnerKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveReferenceKey, '=');
    } else {
      SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, "0", '>');
    }
    if (SearchCondition != undefined) {
      
      this.TargetHistoryList_Config = {
        Id: null,
        Sort: null,
        Task: this._HelperService.AppConfig.Api.ThankUCash.GetrmTargets,
        Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
        // SubReferenceId: this._HelperService.UserAccount.AccountId,
        // SubReferenceKey: this._HelperService.UserAccount.AccountKey,
        // ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
        // ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
        Title: "Available TargetHistory",
        StatusType: "default",
        DefaultSortExpression: 'CreateDate desc',
        SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', null, this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '='),
       
        TableFields: [
          {
            DisplayName: 'Rm Name',
            SystemName: 'RmDisplayName',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: true,
            Search: true,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Contact No',
            SystemName: 'RmMobileNumber',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: false,
            Search: true,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Target',
            SystemName: 'TotalTarget',
            DataType: this._HelperService.AppConfig.DataType.Decimal,
            Class: '',
            Show: false,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'Achievement',
            SystemName: 'AchievedTarget',
            DataType: this._HelperService.AppConfig.DataType.Decimal,
            Class: '',
            Show: false,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'Difference',
            SystemName: 'RemainingTarget',
            DataType: this._HelperService.AppConfig.DataType.Decimal,
            Class: '',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'Set On',
            SystemName: 'StartDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Target Finish',
            SystemName: 'EndDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Set By',
            SystemName: 'CreatedByDisplayName',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: false,
            Search: true,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'Set On',
            SystemName: 'CreateDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
        ]
      };
      if (this.Type == 6) {
        this.TargetHistoryList_Config.Type = this._HelperService.AppConfig.ListType.Manager
      }
      else {
        this.TargetHistoryList_Config.Type = this._HelperService.AppConfig.ListType.RM

      }
      // this.TargetHistoryList_Config.Type = 

    }
    else {
      this.TargetHistoryList_Config = {
        Id: null,
        Sort: null,
        Task: this._HelperService.AppConfig.Api.ThankUCash.GetrmTargets,
        Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
        SubReferenceId: this._HelperService.UserAccount.AccountId,
        SubReferenceKey: this._HelperService.UserAccount.AccountKey,
        ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
        ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
        Title: "Available TargetHistory",
        StatusType: "default",
        Type: this._HelperService.AppConfig.ListType.All,
        DefaultSortExpression: 'CreateDate desc',
        TableFields: [
          {
            DisplayName: 'Name',
            SystemName: 'RmDisplayName',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: true,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Contact No',
            SystemName: 'RmMobileNumber',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Total Target',
            SystemName: 'TotalTarget',
            DataType: this._HelperService.AppConfig.DataType.Number,
            Class: '',
            Show: false,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'Achieved Target',
            SystemName: 'AchievedTarget',
            DataType: this._HelperService.AppConfig.DataType.Number,
            Class: '',
            Show: false,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'Remaining Target',
            SystemName: 'RemainingTarget',
            DataType: this._HelperService.AppConfig.DataType.Number,
            Class: '',
            Show: false,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'Start Date',
            SystemName: 'StartDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'End Date',
            SystemName: 'EndDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },

          {
            DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
            SystemName: 'CreateDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
        ]

      };
    }

    this.TargetHistoryList_Config = this._DataHelperService.List_Initialize(
      this.TargetHistoryList_Config
    );
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Merchant,
      this.TargetHistoryList_Config
    );
    this.TargetHistoryList_GetData();
  }
  TargetHistoryList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.TargetHistoryList_Config.Sort.SortOptions.length; index++) {
        const element = this.TargetHistoryList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.TargetHistoryList_Config


    );

    this.TargetHistoryList_Config = this._DataHelperService.List_Operations(
      this.TargetHistoryList_Config,
      event,
      Type
    );

    if (
      (this.TargetHistoryList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TargetHistoryList_GetData();
    }

  }
  TargetHistoryList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.TargetHistoryList_Config
    );
    this.TargetHistoryList_Config = TConfig;
  }
  TargetHistoryList_RowSelected(ReferenceData) {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Store.Dashboard, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
    // this._HelperService.SaveStorage(
    //   this._HelperService.AppConfig.Storage.ActiveStore,
    //   {
    //     ReferenceKey: ReferenceData.ReferenceKey,
    //     ReferenceId: ReferenceData.ReferenceId,
    //     DisplayName: ReferenceData.DisplayName,
    //     AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
    //   }
    // );

    // this._HelperService.AppConfig.ActiveReferenceKey =
    //   ReferenceData.ReferenceKey;
    // this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Store
    //     .Dashboard,
    //   ReferenceData.ReferenceKey,
    //   ReferenceData.ReferenceId,
    // ]);


  }

  //#endregion

  public TargetHistoryList_Filter_Owners_Option: Select2Options;
  public TargetHistoryList_Filter_Owners_Selected = 0;
  TargetHistoryList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ]
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TargetHistoryList_Filter_Owners_Option = {
      placeholder: 'Sort by Owner',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TargetHistoryList_Filter_Owners_Change(event: any) {
    if (event.value == this.TargetHistoryList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TargetHistoryList_Filter_Owners_Selected, '=');
      this.TargetHistoryList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TargetHistoryList_Config.SearchBaseConditions);
      this.TargetHistoryList_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.TargetHistoryList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TargetHistoryList_Filter_Owners_Selected, '=');
      this.TargetHistoryList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TargetHistoryList_Config.SearchBaseConditions);
      this.TargetHistoryList_Filter_Owners_Selected = event.value;
      this.TargetHistoryList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TargetHistoryList_Filter_Owners_Selected, '='));
    }

    this.TargetHistoryList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  public ResetFilterControls: boolean = true;
  ShowEditTarget(Target: any): void {
    this.CurrentTarget = Target;
    this.Form_UpdateTarget.controls['ReferenceId'].setValue(Target.ReferenceId);
    this.Form_UpdateTarget.controls['ReferenceKey'].setValue(Target.ReferenceKey);
    this.Form_UpdateTarget.controls['TotalTarget'].setValue(Target.TotalTarget);

    this.Form_UpdateTarget_Show();
  }


  //#region Update target 


  public CurrentTarget: any = {};

  Form_UpdateTarget: FormGroup;
  Form_UpdateTarget_Show() {
    this._HelperService.OpenModal("UpdateTarget");
  }
  Form_UpdateTarget_Close() {
    this._HelperService.CloseModal("UpdateTarget");
  }
  Form_UpdateTarget_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_UpdateTarget = this._FormBuilder.group({

      OperationType: "update",
      Task: 'updatermtarget',

      ReferenceId: [null, Validators.compose([Validators.required])],
      ReferenceKey: [null, Validators.compose([Validators.required])],

      TotalTarget: [null, Validators.compose([
        Validators.required,
        Validators.min(1)
      ])
      ]

    });
  }
  Form_UpdateTarget_Clear() {
    this.Form_UpdateTarget.reset();
    this.Form_UpdateTarget_Load();
  }

  Form_UpdateTarget_Process(_FormValue: any) {

    _FormValue.TStartDate = undefined;

    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.Branch,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Target Updated Successfully");
          this.TargetHistoryList_GetData();
          this.Form_UpdateTarget_Clear();
          this.Form_UpdateTarget_Close();

          if (_FormValue.OperationType == "close") {
            this.Form_UpdateTarget_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }



  //#endregion


  SetOtherFilters(): void {
    this.TargetHistoryList_Config.SearchBaseConditions = [];
    this.TargetHistoryList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      //   this.MerchantsList_Filter_Owners_Selected = null;
      // this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.TargetHistoryList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.TargetHistoryList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.TargetHistoryList_Config);

    this.SetOtherFilters();

    this.TargetHistoryList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Merchant
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Merchant
        );
        this._FilterHelperService.SetMerchantConfig(this.TargetHistoryList_Config);
        this.TargetHistoryList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.TargetHistoryList_GetData();

    if (ButtonType == 'Sort') {
      $("#MerchantsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#MerchantsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.TargetHistoryList_Config);
    this.SetOtherFilters();

    this.TargetHistoryList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    // this.MerchantsList_Filter_Owners_Load();
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

}

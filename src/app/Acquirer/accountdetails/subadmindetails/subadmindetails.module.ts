import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { ImageCropperModule } from 'ngx-image-cropper';
import { AgmCoreModule } from '@agm/core';
import { SubadmindetailsComponent } from './subadmindetails.component';
import { DynamicRoutesguardGuard } from 'src/app/service/guard/dynamicroutes.guard';
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
const routes: Routes = [
  { path: '',canActivateChild:[DynamicRoutesguardGuard],data:{accessName:['viewacqsubaccount']}, component: SubadmindetailsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubAdmindetailsRoutingModule { }
const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 1,
};

@NgModule({
  declarations: [SubadmindetailsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    SubAdmindetailsRoutingModule,
    ImageCropperModule,
    InputFileModule.forRoot(config),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
  }),
  AgmCoreModule
  ]
})
export class SubadmindetailsModule { }

import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, OResponse } from "../../../service/service";
import { InputFile, InputFileComponent } from 'ngx-input-file';
import * as cloneDeep from 'lodash/cloneDeep';
@Component({
  selector: 'app-subadmindetails',
  templateUrl: './subadmindetails.component.html',
  
})
export class SubadmindetailsComponent implements OnInit {
    slideOpen: any = false;
    _ShowMap = false;
    _DateDiff: any = {};
    public _SubAccountDetails:any =
      {
        ReferenceId: null,
        ReferenceKey: null,
        TypeCode: null,
        TypeName: null,
        RoleName:null,
        SubTypeCode: null,
        SubTypeName: null,
        UserAccountKey: null,
        UserAccountDisplayName: null,
        Name: null,
        Description: null,
        StartDate: null,
        StartDateS: null,
        EndDate: null,
        EndDateS: null,
        SubTypeValue: null,
        MinimumInvoiceAmount: null,
        MaximumInvoiceAmount: null,
        MinimumRewardAmount: null,
        MaximumRewardAmount: null,
        ManagerKey: null,
        ManagerDisplayName: null,
        SmsText: null,
        Comment: null,
        CreateDate: null,
        CreatedByKey: null,
        CreatedByDisplayName: null,
        ModifyDate: null,
        ModifyByKey: null,
        ModifyByDisplayName: null,
        StatusId: null,
        StatusCode: null,
        StatusName: null,
  
        CreateDateS: null,
        ModifyDateS: null,
        StatusI: null,
        StatusB: null,
        StatusC: null,
      }
    constructor(
      public _Router: Router,
      public _ActivatedRoute: ActivatedRoute,
      public _FormBuilder: FormBuilder,
      public _HelperService: HelperService,
      public _DataHelperService: DataHelperService
    ) { }
  
    changeSlide(): void {
      this.slideOpen = !this.slideOpen;
    }
  
    myFunction() {
      var element = document.getElementById("StoresHide");
      element.classList.add("Hm-HideDiv");
      element.classList.remove("Hm-DisplayDiv");
  
    }
  
    DisplayDiv() {
      var element = document.getElementById("StoresHide");
      element.classList.add("Hm-DisplayDiv");
      element.classList.remove("Hm-HideDiv");
    }
  
    ngOnInit() {
      Feather.replace();
      this._HelperService.ValidateData();
      this.InitImagePicker(this.InputFileComponent);
      this._ActivatedRoute.params.subscribe((params: Params) => {
          this._HelperService.AppConfig.ActiveSubAccountReferenceKey = params["referencekey"];
          this._HelperService.AppConfig.ActiveSubAccountReferenceId = params["referenceid"];
          if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
              this._Router.navigate([
                  this._HelperService.AppConfig.Pages.System.NotFound
              ]);
          } else {
            this._ShowMap = true;
            // Feather.replace();
            this._HelperService.AppConfig.ShowHeader = true;
            this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = this._HelperService.AppConfig.AccountType.Acquirer;
            this._HelperService.ContainerHeight = window.innerHeight;
            this.GetSubAccountDetails();
            this.Form_EditUser_Load();
          }
      });
  
   
    }
    
    EditBranch() {
      this._HelperService.AppConfig.ActiveSubAccountReferenceKey = this._SubAccountDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveSubAccountReferenceId = this._SubAccountDetails.ReferenceId;
      this.Form_EditUser_Show();
    }
  

    public updateNumber:any;
    GetSubAccountDetails() {
      this._HelperService.IsFormProcessing = true;
      var pData = {
        Task: this._HelperService.AppConfig.Api.Core.GetSubAccount,
        ReferenceKey: this._HelperService.AppConfig.ActiveSubAccountReferenceKey,
        ReferenceId: this._HelperService.AppConfig.ActiveSubAccountReferenceId
      }
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.SubAccounts, pData);
      _OResponse.subscribe(
        _Response => {
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.IsFormProcessing = false;
            this._SubAccountDetails = _Response.Result;
            console.log(_Response)
            if (this._SubAccountDetails != undefined && this._SubAccountDetails.MobileNumber != undefined && this._SubAccountDetails.MobileNumber != null) {
              if (this._SubAccountDetails.MobileNumber.startsWith("234") || this._SubAccountDetails.MobileNumber.startsWith("233") || this._SubAccountDetails.MobileNumber.startsWith("254")) {
                  this._SubAccountDetails.MobileNumber = this._SubAccountDetails.MobileNumber.substring(3, this._SubAccountDetails.length);
              }
          }
  
            if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
              this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
              this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
            } else {
              this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
              this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
            }
  
            this._DateDiff = this._HelperService._DateRangeDifference(this._SubAccountDetails.StartDate, this._SubAccountDetails.EndDate);
            this._SubAccountDetails.StartDateS = this._HelperService.GetDateS(
              this._SubAccountDetails.StartDate
            );
            this._SubAccountDetails.EndDateS = this._HelperService.GetDateS(
              this._SubAccountDetails.EndDate
            );
            this._SubAccountDetails.CreateDateS = this._HelperService.GetDateTimeS(
              this._SubAccountDetails.CreateDate
            );
            this._SubAccountDetails.ModifyDateS = this._HelperService.GetDateTimeS(
              this._SubAccountDetails.ModifyDate
            );
            this._SubAccountDetails.StatusI = this._HelperService.GetStatusIcon(
              this._SubAccountDetails.StatusCode
            );
            this._SubAccountDetails.StatusB = this._HelperService.GetStatusBadge(
              this._SubAccountDetails.StatusCode
            );
            this._SubAccountDetails.StatusC = this._HelperService.GetStatusColor(
              this._SubAccountDetails.StatusCode
            );
  
  
  
          }
          else {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.HandleException(_Error);
        }
      );
    }

    Form_EditUser: FormGroup;
    Form_EditUser_Show() {
        this._HelperService.OpenModal("Form_EditUser_Content");
    }
    Form_EditUser_Close() {
        // this._Router.navigate([
        //     this._HelperService.AppConfig.Pages.System.AdminUsers
        // ]);
        this._HelperService.CloseModal("Form_EditUser_Content");
    }
    Form_EditUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_EditUser = this._FormBuilder.group({
            OperationType: "new",
            Task: this._HelperService.AppConfig.Api.Core.UpdateSubAccount,
            ReferenceId:this._HelperService.AppConfig.ActiveSubAccountReferenceId,
            ReferenceKey:this._HelperService.AppConfig.ActiveSubAccountReferenceKey,
            AccountId:this._HelperService.AppConfig.ActiveReferenceId,
            AccountKey:this._HelperService.AppConfig.ActiveReferenceKey,
            RoleId:[7, Validators.required],
            RoleKey:['admin', Validators.required],
            ImageContent:null,

            Name: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(128)
                ])
            ],
            MobileNumber: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(8),
                    Validators.maxLength(14)
                ])
            ],
            EmailAddress: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.email,
                    Validators.minLength(2)
                ])
            ],
            StatusCode: this._HelperService.AppConfig.Status.Active,

        });
    }
    Form_EditUser_Clear() {
        this.Form_EditUser.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_EditUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    ReFormat_RequestBody(): void {
      var formValue: any = cloneDeep(this.Form_EditUser.value);
      var formRequest: any = {
  
        OperationType: "new",
        Task: this._HelperService.AppConfig.Api.Core.UpdateSubAccount,
        ReferenceId:this._HelperService.AppConfig.ActiveSubAccountReferenceId,
        ReferenceKey:this._HelperService.AppConfig.ActiveSubAccountReferenceKey,
        AccountId: this._HelperService.AppConfig.ActiveReferenceId,
        AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
        StatusCode: formValue.StatusCode,
        ImageContent: this._imageConfig.profileImages[0],
         RoleId: formValue.RoleId,
         RoleKey: formValue.RoleKey,
        Name: formValue.Name,
        MobileNumber: formValue.MobileNumber,
        EmailAddress: formValue.EmailAddress,
          
      };
      return formRequest;
  }
    Form_EditUser_Process(_FormValue: any) {
        // _FormValue.DisplayName = _FormValue.FirstName;
        // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
        this._HelperService.IsFormProcessing = true;
        let requestBody = this.ReFormat_RequestBody();
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.SubAccounts,
            requestBody
        );
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess("Account Updated successfully");
                    // this.Form_EditUser_Clear();
                    this._HelperService.CloseModal('Form_EditUser_Content');
                    this.GetSubAccountDetails()
                    if (_FormValue.OperationType == "close") {
                        this.Form_EditUser_Close();
                    }
                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            }
        );
    }
     
    _imageConfig =
    {
  
      Images: [],
      profileImages: [],
            StartDateConfig: {
            },
            EndDateConfig: {
            },
            DefaultStartDate: null,
            DefaultEndDate: null,
            StartDate: null,
            EndDate: null,
    }
  
  private InputFileComponent_Term: InputFileComponent;
  _ImageManager =
    {
      TCroppedImage: null,
      ActiveImage: null,
      ActiveImageName: null,
      ActiveImageSize: null,
      Option: {
        MaintainAspectRatio: "true",
        MinimumWidth: 800,
        MinimumHeight: 400,
        MaximumWidth: 800,
        MaximumHeight: 400,
        ResizeToWidth: 800,
        ResizeToHeight: 400,
        Format: "jpg",
      }
    }
    @ViewChild("inputfile")
    private InputFileComponent: InputFileComponent;
    CurrentImagesCount: number = 0;
private InitImagePicker(InputFileComponent: InputFileComponent) {
  if (InputFileComponent != undefined) {
    this.CurrentImagesCount = 0;
    this._HelperService._InputFileComponent = InputFileComponent;
    InputFileComponent.onChange = (files: Array<InputFile>): void => {
      if (files.length >= this.CurrentImagesCount) {
        this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
      }
      this.CurrentImagesCount = files.length;
    };
  }
}
Form_ManagePromote: FormGroup;

onImageAccept(value) {
  setTimeout(() => {
      this.Form_EditUser.patchValue(
          {
              ImageContent: null,
          }
      );
  }, 300);
  this._ImageManager.ActiveImage = value;
  this._ImageManager.ActiveImageName = value.file.name;
  this._ImageManager.ActiveImageName = value.file.size;
}

onImageAccept1(value) {
  this._ImageManager.ActiveImage = value;
  this._ImageManager.ActiveImageName = value.file.name;
  this._ImageManager.ActiveImageName = value.file.size;
}
Icon_B64Cropped(base64: string) {
  this._ImageManager.TCroppedImage = base64;
}
Icon_B64CroppedDone() {
  var ImageDetails = this._HelperService.GetImageDetails(this._ImageManager.TCroppedImage);
  var ImageContent =
  {
    //OriginalContent: this._ImageManager.TCroppedImage,
    Name: this._ImageManager.ActiveImageName,
    Size: this._ImageManager.ActiveImageSize,
    Extension: ImageDetails.Extension,
    Content: ImageDetails.Content
  };
 
  if (this._imageConfig.profileImages.length == 0) {
    this._imageConfig.profileImages.push(
      {
        //ImageContent: ImageItem,
        Name: this._ImageManager.ActiveImageName,
        Size: this._ImageManager.ActiveImageSize,
        Extension: ImageDetails.Extension,
        Content: ImageDetails.Content,
        IsDefault: 1,
      }
    );
  }
  else {
    this._imageConfig.profileImages.push(
      {
        //  ImageContent: ImageItem,
        Name: this._ImageManager.ActiveImageName,
        Size: this._ImageManager.ActiveImageSize,
        Extension: ImageDetails.Extension,
        Content: ImageDetails.Content,
        IsDefault: 0,
      }
    );
  }
  this._ImageManager.TCroppedImage = null;
  this._ImageManager.ActiveImage = null;
  this._ImageManager.ActiveImageName = null;
  this._ImageManager.ActiveImageSize = null;
}
Icon_Crop_Clear() {
  this._ImageManager.TCroppedImage = null;
  this._ImageManager.ActiveImage = null;
  this._ImageManager.ActiveImageName = null;
  this._ImageManager.ActiveImageSize = null;
  this._HelperService.CloseModal('_Icon_Cropper_Modal');
}
RemoveImage(Item) {
  this._imageConfig.profileImages = this._imageConfig.profileImages.filter(x => x != Item);
}

removeImage(): void {
  //this.CurrentImagesCount = 0;
  this._imageConfig.profileImages = []
}
  }

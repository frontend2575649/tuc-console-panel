import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { AgmCoreModule } from '@agm/core';
// import { LeafletModule } from '@asymmetrik/ngx-leaflet';



import { TUManagerComponent } from "./tumanager.component";
import { DynamicRoutesguardGuard } from "src/app/service/guard/dynamicroutes.guard";
import { MerchantguardGuard } from "src/app/service/guard/merchantguard.guard";
const routes: Routes = [
    {
        path: "",
        component: TUManagerComponent,
        // canActivateChild:[DynamicRoutesguardGuard],
        canActivateChild:[MerchantguardGuard],
        data:{accessName:['viewmanager']},
        children: [
            // { path: "/:referencekey/:referenceid/:type", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../../Acquirer/dashboards/manager/acquirer/dashboard.module#TUDashboardModule" },
            { path: "/:referencekey/:referenceid/:type", data: { permission: "getmerchant", PageName: 'System.Menu.Manager',menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../account/tuterminals/manager/tuterminals.module#TUTerminalsModule" },

            { path: "dashboard/:referencekey/:referenceid/:type", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../../Acquirer/dashboards/manager/acquirer/dashboard.module#TUDashboardModule" },

            { path: "terminals/:referencekey/:referenceid/:type", data: {accessName:"viewmanager", permission: "getmerchant", PageName: 'System.Menu.Manager',menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../account/tuterminals/manager/tuterminals.module#TUTerminalsModule" },

            { path: 'stores/:referencekey/:referenceid/:type', data: { 'permission': 'salehistory', PageName: 'System.Menu.Manager' }, loadChildren: '../../../../Acquirer/account/tustores/manager/tustores.module#TUStoresModule' },

            { path: 'targets/:referencekey/:referenceid/:type', data: { 'permission': 'salehistory', PageName: 'System.Menu.Manager' }, loadChildren: '../../../../Acquirer/accountdetails/tutargethistory/tutargethistory.module#TUTargetHistoryModule' },

            { path: 'teams/:referencekey/:referenceid/:type', data: { 'permission': 'salehistory', PageName: 'System.Menu.Manager' }, loadChildren: '../../../../Acquirer/account/tuteams/manager/tuteams.module#TUTeamsModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUManagerRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUManagerRoutingModule,
        // LeafletModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    declarations: [TUManagerComponent]
})
export class TUManagerModule { }

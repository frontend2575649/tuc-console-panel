import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { Observable, Subscription } from "rxjs";
import { DataHelperService, FilterHelperService, HelperService, OResponse, OSelect } from "../../../../service/service";
declare var moment: any;
declare var $: any;

@Component({
  selector: "tu-manager",
  templateUrl: "./tumanager.component.html",
})
export class TUManagerComponent implements OnInit {
  public _ShowMap = false;
  public _ObjectSubscription: Subscription = null;
public managerPlaceholder:any;
  Type: string
_RolesArray= [
    {
      'id': 0,
      'text': "Select Role",
      'apival': "Select Role",
    },
    {
      'id': 6,
      'text': 'Manager',
      'apival': 'manager'
    },
    {
      'id': 7,
      'text': 'Administrator/Sub Account',
      'apival': 'admin'
    },
    {
      'id': 8,
      'text': 'RM',
      'apival': 'rm'
    }
  ];
  @ViewChild("offCanvas") divView: ElementRef;
  _DateDiff: any = {};
  slideOpen: any = false;
  public _ManagerDetails: any =
    {
      RoleId: null,
      BranchKey: null,
      BranchId: null,
      BranchName: null,
      RoleKey: null,
      RoleName: null,
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService



  ) { }
  changeSlide(): void {
    this.slideOpen = !this.slideOpen;
    if (this.slideOpen) {
      // this._HelperService._MapCorrection();
    }
  }


  ngOnInit() {
    this._ShowMap = true;
    Feather.replace();
    this._HelperService.ValidateData();
    this.initializeDatePicker('monthlydate', this._HelperService.AppConfig.DatePickerTypes.month, 'MonthSalesTrend_dropdown');

    this.Form_EditUser_Load();
    this.GetRoles();
    this.GetRMs_List();
    this.Form_AssignTarget_Load();

    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };

    this.Form_EditUser_Load();
    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveManager);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveManagerReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveManagerReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }

    this.GetBranches_List();
    this.GetMangers_List();
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.ContainerHeight = window.innerHeight;
    this._HelperService.FullContainer = false;
    this.GetManagerDetails();


  }

  initializeDatePicker(pickerId: string, type: string, dropdownId: string): void {
    var i = '#' + pickerId;

    if (type == this._HelperService.AppConfig.DatePickerTypes.month) {
      $(i).datepicker(
        {
          setDate: new Date(),
          viewMode: "months",
          minViewMode: "months",
          format: "dd/mm/yyyy",
          startDate: '-0m'

        }
      ).on('changeDate', function (ev) {
        $('#sDate1').text($('#datepicker').data('date'));
        $('#datepicker').datepicker('hide');
      });;
    } else {
      $(i).datepicker();
    }

    $(i).on('changeDate', () => {
      this.ScheduleDateRangeChange({
        start: moment($(i).datepicker("getDate")),
        end: moment($(i).datepicker("getDate"))
      });
      $(i).datepicker('hide');
    });

  }

  Form_EditUser: FormGroup;
  Form_EditUser_Show() {
    this._HelperService.OpenModal("Form_EditUser_Content");
  }
  Form_EditUser_Close() {
    // this._Router.navigate([
    //     this._HelperService.AppConfig.Pages.System.AdminUsers
    // ]);
    this._HelperService.OpenModal("Form_EditUser_Content");
  }
  Form_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;
    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_EditUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.Core.UpdateManager,
      ReferenceKey: this._ManagerDetails.ReferenceKey,
      ReferenceId: this._ManagerDetails.ReferenceId,
      OwnerKey: [this._ManagerDetails['OwnerKey'], Validators.required],
      OwnerId: [this._ManagerDetails['OwnerId'], Validators.required],
      RoleKey: [null, Validators.required],
      RoleId: [null, Validators.required],
      BranchKey: [this._ManagerDetails['BranchKey'], Validators.required],
      BranchId: [this._ManagerDetails['BranchId'], Validators.required],
      Name: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(128)
        ])
      ],
      MobileNumber: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(14)
        ])
      ],
      EmailAddress: [
        null,
        Validators.compose([
          Validators.required,
          Validators.email,
          Validators.minLength(2)
        ])
      ],
      StatusCode: this._HelperService.AppConfig.Status.Active,


    });
  }
  Form_EditUser_Clear() {
    this.Form_EditUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_EditUser_Process(_FormValue: any) {
    
    // _FormValue.DisplayName = _FormValue.FirstName;
    // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
    if((_FormValue.OwnerId && _FormValue.OwnerKey)==null){
      _FormValue.OwnerId = this._ManagerDetails.OwnerId;
      _FormValue.OwnerKey = this._ManagerDetails.OwnerKey;
    }
   
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.Branch,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          this._HelperService.CloseModal('off-canvas')
          this.Form_EditUser_Clear();
          this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
            this.GetManagerDetails();
          });
          $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("hide");
          if (_FormValue.OperationType == "close") {
            this.Form_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );

  }


  public GetRoles_Option: Select2Options;
  public GetRoles_Transport: any;
  GetRoles() {
    //   $(".check").select2({
    //     placeholder: "Select a state",
    //     allowClear: true
    // });
  }

  GetRoles_ListChange(event: any) {
    if(event.data){
      this.roleNameFetch=event.data[0].text
    }

    this.Form_EditUser.controls['OwnerId'].setValue(null);
    this.Form_EditUser.controls['OwnerKey'].setValue(null);

    this.Form_EditUser.patchValue(
      {
        RoleId: event.value,
        RoleKey: event.data[0].apival
      }
    );
    this._ShowRole = false;
    this._ChangeDetectorRef.detectChanges();

    if (event.value == 8) { //RM
      this._ShowReportingM = false;
      this._ChangeDetectorRef.detectChanges();
      this._MangerTypeId = 6; //Manager

      this.GetMangers_List();
      this._ShowReportingM = true;
      this._ChangeDetectorRef.detectChanges();
    } else if (event.value == 7) { //SubAccount

      this._ShowReportingM = false;
      this.Form_EditUser.controls['OwnerId'].setValue(this._HelperService.UserAccount.AccountId);
      this.Form_EditUser.controls['OwnerKey'].setValue(this._HelperService.UserAccount.AccountKey);

      this._MangerTypeId = undefined;
    }
    else if (event.value == 6) { //Manager
      this._ShowReportingM = false;
      this._ChangeDetectorRef.detectChanges();
      this._MangerTypeId = 7; //Subaccount
      this.GetMangers_List();
      this._ShowReportingM = true;
      this._ChangeDetectorRef.detectChanges();

    } else {
      this._MangerTypeId = undefined;
      this._ShowReportingM = true;
      this.GetMangers_List();
    }

    this._ShowRole = true;
    this._ChangeDetectorRef.detectChanges();

    this.GetMangers_List();
  }

  public _MangerTypeId: number;
  public _ShowRole: boolean = true;
  public _ShowReportingM: boolean = true;

  public GetMangers_Option: Select2Options;
  public GetMangers_Transport: any;
  GetMangers_List() {
    var PlaceHolder = "Select Manager";
    var _Select: OSelect

    if (this._MangerTypeId == 7) {
      _Select =
      {
        Task: this._HelperService.AppConfig.Api.Core.GetSubAccounts,
        Location: this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.SubAccounts,
        ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
        ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
        SortCondition: [],
        Fields: [
          {
            SystemName: "ReferenceId",
            Type: this._HelperService.AppConfig.DataType.Number,
            Id: true,
            Text: false,
          },
          {
            SystemName: "Name",
            Type: this._HelperService.AppConfig.DataType.Text,
            Id: false,
            Text: true
          },
        ]
      }
    } else {
      _Select =
      {
        Task: this._HelperService.AppConfig.Api.Core.GetManagers,
        Location: this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Branch,
        ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
        ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
        SortCondition: [],
        Fields: [
          {
            SystemName: "ReferenceId",
            Type: this._HelperService.AppConfig.DataType.Number,
            Id: true,
            Text: false,
          },
          {
            SystemName: "Name",
            Type: this._HelperService.AppConfig.DataType.Text,
            Id: false,
            Text: true
          },
        ]
      }

    }


    if (this._MangerTypeId != undefined && this._MangerTypeId != 7) {
      _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'RoleId', this._HelperService.AppConfig.DataType.Number, this._MangerTypeId, '=')
    }

    this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetMangers_Option = {
      // placeholder: this.managerPlaceholder,
      placeholder: "Select Manager",
      ajax: this.GetMangers_Transport,
      multiple: false,
    };
    
  }
  GetMangers_ListChange(event: any) {
    // alert(event);
    if(event.data){
      this.reportingManagerFetch=event.data[0].Name;
    }

    this.Form_EditUser.patchValue(
      {
        OwnerId: event.data[0].ReferenceId,
        OwnerKey: event.data[0].ReferenceKey,
      }


    );

  }

  public GetBranches_Option: Select2Options;
  public GetBranches_Transport: any;
  GetBranches_List() {
    var PlaceHolder = "Select Branch";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetBranches,
      Location: this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Branch,
      // SearchCondition: "",
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },

        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: 'StatusCode',
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: '=',
        //     SearchValue: this._HelperService.AppConfig.Status.Active,
        // }
      ]
    }



    this.GetBranches_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetBranches_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetBranches_Transport,
      multiple: false,
    };
  }
  GetBranches_ListChange(event: any) {
    if(event.data){
      this.branchNameFetch=event.data[0].DisplayName
    }
    // alert(event);\
    this.Form_EditUser.patchValue(
      {
        BranchKey: event.data[0].ReferenceKey,
        BranchId: event.data[0].ReferenceId

      }
    );

  }

  GoTo_Overview(): void {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.ManagerDetail.Overview, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId, this._ManagerDetails['RoleId']]);
  }

  GoTo_Terminals(): void {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.ManagerDetail.Terminals, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId, this._ManagerDetails['RoleId']]);
  }

  GoTo_Stores(): void {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.ManagerDetail.Stores, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId, this._ManagerDetails['RoleId']]);
  }

  GoTo_Teams(): void {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.ManagerDetail.Teams, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId, this._ManagerDetails['RoleId']]);
  }
  reportingManagerFetch:any;
  roleNameFetch:any;
  branchNameFetch:any;
  public updateNumber: any;
  obj:any;
  GetManagerDetails() {
    
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetManger,
      ReferenceKey: this._HelperService.AppConfig.ActiveManagerReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveManagerReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Branch, pData);
    _OResponse.subscribe(

      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this._HelperService.IsFormProcessing = false;
          this._ManagerDetails = _Response.Result;
          if (this._ManagerDetails != undefined && this._ManagerDetails.MobileNumber != undefined && this._ManagerDetails.MobileNumber != null) {
            if (this._ManagerDetails.MobileNumber.startsWith("234") || this._ManagerDetails.MobileNumber.startsWith("233") || this._ManagerDetails.MobileNumber.startsWith("254")) {
              this._ManagerDetails.MobileNumber = this._ManagerDetails.MobileNumber.substring(3, this._ManagerDetails.length);
            }
          }

          this.GetBranches_Option.placeholder = this._ManagerDetails.BranchName;
          this.branchNameFetch=this._ManagerDetails.BranchName;
          this.managerPlaceholder = this._ManagerDetails.OwnerDisplayName;
          this.roleNameFetch=this._ManagerDetails.RoleName;
          this.reportingManagerFetch=this._ManagerDetails.OwnerDisplayName;
          this.Form_EditUser_Load();
          this.Form_AssignTarget_Load();
          // this.GetMangers_Option.placeholder = this._ManagerDetails.Name;
          // this.GetRoles_Option.placeholder = this._ManagerDetails.Name;
          this._HelperService.ManagerBranchKey = this._ManagerDetails.BranchKey;
          this._HelperService.ManagerBranchId = this._ManagerDetails.BranchId;

          // for(let i =1 ;i<= this._Roles.length;i++){
          //   if(this._ManagerDetails.RoleName == this._Roles[i].text){
          //        this._Roles[i].selected = true;
          //   }
          // }

         var x= this._ManagerDetails.RoleId;
          this.obj = this._RolesArray.find(o => o.id === x );
          

          this._DateDiff = this._HelperService._DateRangeDifference(this._ManagerDetails.StartDate, this._ManagerDetails.EndDate);
          this._ManagerDetails.StartDateS = this._HelperService.GetDateS(
            this._ManagerDetails.StartDate
          );
          this._ManagerDetails.EndDateS = this._HelperService.GetDateS(
            this._ManagerDetails.EndDate
          );
          this._ManagerDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._ManagerDetails.CreateDate
          );
          this._ManagerDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._ManagerDetails.ModifyDate
          );
          this._ManagerDetails.StatusI = this._HelperService.GetStatusIcon(
            this._ManagerDetails.StatusCode
          );
          this._ManagerDetails.StatusB = this._HelperService.GetStatusBadge(
            this._ManagerDetails.StatusCode
          );
          this._ManagerDetails.StatusC = this._HelperService.GetStatusColor(
            this._ManagerDetails.StatusCode
          );
          this.RoleName()
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  placeholder: any;
  _Roles:any;
  clicked() {
    console.log("this.obj",this.obj);
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");

    this._Roles = [
      {
        'id': this.obj.id,
        'text': this.obj.text,
        'apival': this.obj.apival,
      },
      
      {
        'id': 6,
        'text': 'Manager',
        'apival': 'manager'
      },
      {
        'id': 7,
        'text': 'Administrator/Sub Account',
        'apival': 'admin'
      },
      {
        'id': 8,
        'text': 'RM',
        'apival': 'rm'
      }
    ];
    this.Form_EditUser.controls.RoleId.setValue(this.obj.id);
    this.Form_EditUser.controls.RoleKey.setValue(this.obj.apival);
    this.GetBranches_Option.placeholder = this._ManagerDetails.BranchName;
    this.managerPlaceholder = this._ManagerDetails.OwnerDisplayName;
    // this.Form_EditUser.patchValue(
    //   {
    //     RoleId: this.obj.id,
    //     RoleKey: this.obj.apival
    //   }
    // );
    this.Form_EditUser.controls.OwnerId.setValue(this._ManagerDetails.OwnerId);
    this.Form_EditUser.controls.OwnerKey.setValue(this._ManagerDetails.OwnerKey);
  

  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

  RoleName() {
    if (this._ManagerDetails.RoleId == 6) {
      this._ManagerDetails['RoleName'] = "Manager";
      // this.GetRoles_Option.placeholder = 'Manager'
      // this._Roles[0].text = "Manager"


    }
    else if (this._ManagerDetails.RoleId == 7) {
      this._ManagerDetails['RoleName'] = "Sub-Admin"
      // this.GetRoles_Option.placeholder = 'Sub-Admin'

      // this._Roles[0].text = "Sub-Admin"


    }
    else {
      this._ManagerDetails['RoleName'] = "RM";
      // this.GetRoles_Option.placeholder = 'RM'

      // this._Roles[0].text = "RM"


    }


  }

  Form_EditUser_Block() {

  }

  //#region Target Assignments 

  ShowAssignTargetModal() {
    this._HelperService.OpenModal('AssignTarget');
  }

  Form_AssignTarget: FormGroup;
  Form_AssignTarget_Show() {
    this._HelperService.OpenModal("AssignTarget");
  }
  Form_AssignTarget_Close() {
    this._HelperService.CloseModal("AssignTarget");
  }
  Form_AssignTarget_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AssignTarget = this._FormBuilder.group({

      ManagerKey: [this._ManagerDetails['OwnerKey'], Validators.required],
      ManagerId: [this._ManagerDetails['OwnerId'], Validators.required],

      // RoleKey: ['rm', Validators.required],
      // RoleId: [8, Validators.required],

      BranchKey: [this._ManagerDetails['BranchKey'], Validators.required],
      BranchId: [this._ManagerDetails['BranchId'], Validators.required],

      RmKey: [this._ManagerDetails['ReferenceKey'], Validators.required],
      RmId: [this._ManagerDetails['ReferenceId'], Validators.required],

      StartDate: [null, Validators.compose([Validators.required])],
      EndDate: [null, Validators.compose([Validators.required])],


      TStartDate: [null],

      TotalTarget: [null, Validators.compose([
        Validators.required,
        Validators.min(1)
      ])
      ]

    });
  }
  Form_AssignTarget_Clear() {

    this.GetRM_ResetSelector();

    this.Form_AssignTarget.reset();
    this.Form_AssignTarget_Load();
  }

  Form_AssignTarget_Process(_FormValue: any) {

    // _FormValue.RmId = this.RMs[0].RmId;
    // _FormValue.RmKey = this.RMs[0].RmKey;
    _FormValue.TStartDate = undefined;

    var pData = {
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.SaveRmTarget,
      Targets: []
    };

    pData.Targets.push(_FormValue);

    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Branch,
      pData
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Target Assigned successfully to selected users");
          this.Form_AssignTarget_Clear();
          this.Form_AssignTarget_Close();
          this.Form_AssignTarget_Load();

          this._HelperService.ReloadEventEmit.emit(0);

          if (_FormValue.OperationType == "close") {
            this.Form_AssignTarget_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  ScheduleDateRangeChange(value): void {

    this.Form_AssignTarget.patchValue(
      {
        TStartDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
      }
    );

    this.Form_AssignTarget.patchValue(
      {
        StartDate: this._HelperService.DateInUTC(moment(value.start).startOf('month').startOf('day')),
        EndDate: this._HelperService.DateInUTC(moment(value.end).endOf('month').endOf('day')),
      }
    );
  }


  //#region RM selection 

  _ShowRMSelector: boolean = true;
  RMs = [];
  public GetRMs_Option: Select2Options;
  public GetRMs_Transport: any;
  GetRMs_List() {
    var PlaceHolder = "Select Manager";
    var _Select: OSelect

    if (this._MangerTypeId == 7) {
      _Select =
      {
        Task: this._HelperService.AppConfig.Api.Core.GetSubAccounts,
        Location: this._HelperService.AppConfig.NetworkLocation.V3.SubAccounts,
        ReferenceKey: this._HelperService.UserAccount.AccountKey,
        ReferenceId: this._HelperService.UserAccount.AccountId,
        SortCondition: [],
        Fields: [
          {
            SystemName: "ReferenceId",
            Type: this._HelperService.AppConfig.DataType.Number,
            Id: true,
            Text: false,
          },
          {
            SystemName: "Name",
            Type: this._HelperService.AppConfig.DataType.Text,
            Id: false,
            Text: true
          },
        ]
      }
    } else {
      _Select =
      {
        Task: this._HelperService.AppConfig.Api.Core.GetManagers,
        Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
        ReferenceKey: this._HelperService.UserAccount.AccountKey,
        ReferenceId: this._HelperService.UserAccount.AccountId,
        SortCondition: [],
        Fields: [
          {
            SystemName: "ReferenceId",
            Type: this._HelperService.AppConfig.DataType.Number,
            Id: true,
            Text: false,
          },
          {
            SystemName: "Name",
            Type: this._HelperService.AppConfig.DataType.Text,
            Id: false,
            Text: true
          },
        ]
      }

    }


    if (this._MangerTypeId != undefined && this._MangerTypeId != 7) {
      _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'RoleId', this._HelperService.AppConfig.DataType.Number, this._MangerTypeId, '=')
    }

    this.GetRMs_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetRMs_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetRMs_Transport,
      multiple: false,
    };
  }
  GetRM_ListChange(event: any) {
    if (event.data[0]) {
      this.RMs[0] = {
        RmId: event.data[0].ReferenceId,
        RmKey: event.data[0].ReferenceKey
      };
    } else {
      this.RMs[0] = undefined;
    }
  }
  GetRM_ResetSelector(): void {
    this._ShowRMSelector = false;
    this._ChangeDetectorRef.detectChanges();
    setTimeout(() => {
      this._ShowRMSelector = true;
      this._ChangeDetectorRef.detectChanges();
    }, 100);
  }

  //#endregion

  //#endregion

}

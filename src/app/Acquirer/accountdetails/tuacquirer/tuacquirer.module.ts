import { AgmCoreModule } from '@agm/core';
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Select2Module } from "ng2-select2";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgxPaginationModule } from "ngx-pagination";
import { DynamicRoutesguardGuard } from 'src/app/service/guard/dynamicroutes.guard';
import { MainPipe } from '../../../service/main-pipe.module';
import { TUAcquirerComponent } from "./tuacquirer.component";

import {} from '../../../Acquirer/accountdetails/tubankloyalty/tubankloyalty.module'

const routes: Routes = [
    {
        path: "",
        component: TUAcquirerComponent,
        canActivate:[DynamicRoutesguardGuard],
        data:{accessName:['viewacquirer']},
        children: [
            { path: ":referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../dashboards/tudetailacquirer/dashboard.module#TUDashboardModule" },
            { path: "acquireraccount/:referencekey/:referenceid", data: { permission: "getmerchant", PageName: 'System.Menu.AcquirerDetails', menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../dashboards/tudetailacquirer/dashboard.module#TUDashboardModule" },
            { path: "configuration/:referencekey/:referenceid", data: { permission: "getmerchant",PageName: 'System.Menu.Configuration', menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../Acquirer/accountdetails/tucconfiguration/tucconfiguration.module#TUCAcqConfigurationModuleModule" },

            //TUCAcqConfigurationModuleModule
            { path: "dashboard/:referencekey/:referenceid", data: { permission: "getmerchant", PageName: 'System.Menu.Overview', menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../dashboards/root/dashboard.module#TUDashboardModule" },
            { path: 'terminals/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.Terminals' }, loadChildren: '../../../Acquirer/account/tuterminals/acquirer/tuterminals.module#TUTerminalsModule' },
            { path: 'merchants/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.Merchants' }, loadChildren: '../../../Acquirer/account/tumerchants/tumerchants.module#TUMerchantsModule' },
            { path: 'stores/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.Stores' }, loadChildren: '../../../Acquirer/account/tustores/acquirer/tustores.module#TUStoresModule' },
            { path: 'branchs/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.Branches' }, loadChildren: '../../../Acquirer/account/branch/branch.module#BranchModule' },
            { path: 'managers/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.Managers' }, loadChildren: '../../../Acquirer/account/tumanagers/tusubaccounts.module#TUSubAccountsModule' },
            { path: 'activity/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.Activity' }, loadChildren: '../../../Acquirer/dashboards/tuactivity/acquirer/tuactivity.module#TUActivityModule' },
            { path: 'saleshistory/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.Transactions' }, loadChildren: '../../../Acquirer/transactions/tusale/acquirer/tusale.module#TUSaleModule' },
            { path: 'merchant', data: { 'permission': 'acquirer', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../Acquirer/accountdetails/tumerchant/acquirer/tumerchant.module#TUMerchantModule' },
            { path: 'terminal', data: { 'permission': 'acquirer', PageName: 'System.Menu.Terminal' }, loadChildren: '../../../Acquirer/accountdetails/tuterminal/acquirer/tuterminal.module#TUTerminalModule' },
            { path: 'store', data: { 'permission': 'acquirer', PageName: 'System.Menu.Terminal' }, loadChildren: '../../../Acquirer/accountdetails/tustore/acquirer/tustore.module#TUStoreModule' },
            { path: 'branchdetails', data: { 'permission': 'acquirer', PageName: 'System.Menu.Branch' }, loadChildren: '../../../Acquirer/accountdetails/BranchDetails/branchdetails.module#BranchDetailsModule' },
            { path: 'manager', data: { 'permission': 'acquirer', PageName: 'System.Menu.Branch' }, loadChildren: '../../../Acquirer/accountdetails/tumanager/acquirer/tumanager.module#TUManagerModule' },
            { path: 'campaigns/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.Campaigns' }, loadChildren: '../../../Acquirer/campaigns/campaigns/campaigns.module#TUCampaignsModule' },
            { path: 'addcampaigns/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.Campaigns' }, loadChildren: '../../../Acquirer/campaigns/addcampaign/addcampaign.module#TUAddCampaignModule' },
            { path: 'campaign', data: { 'permission': 'acquirer', PageName: 'System.Menu.Campaigns' }, loadChildren: '../../../Acquirer/accountdetails/tucampaign/acquirer/tucampaign.module#TUCampaignModule' },
            { path: 'addbranch/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.AddBranch' }, loadChildren: '../../../Acquirer/accountcreation/addbranch/addbranch.module#AddbranchModule' },
            { path: 'merchantonboarding/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.AddMerchant' }, loadChildren: '../../../Acquirer/accountcreation/merchantonboarding/merchantonboarding.module#TUMerchantOnboardingModule' },
            { path: 'subadmins/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.SubAdmins' }, loadChildren: '../../../Acquirer/account/subadmins/subadmins.module#SubadminsModule' },
            { path: 'subadmindetails/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.SubAdmin' }, loadChildren: '../../../Acquirer/accountdetails/subadmindetails/subadmindetails.module#SubadmindetailsModule' },
            { path: 'bankloyalty/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.BankLoyalty' }, loadChildren: '../../../Acquirer/account/bankloyalty/bankloyalty.module#BankloyaltyModule' },
            { path: 'bankloyalty', data: { 'permission': 'acquirer', PageName: 'System.Menu.BankLoyalty' }, loadChildren: '../../../Acquirer/accountdetails/tubankloyalty/tubankloyalty.module#TubankloyaltyModule' },

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUAcquirerRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUAcquirerRoutingModule,
        GooglePlaceModule,
        MainPipe,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),

    ],
    declarations: [TUAcquirerComponent]
})
export class TUAcquirerModule { }

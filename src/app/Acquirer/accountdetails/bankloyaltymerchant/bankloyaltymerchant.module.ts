import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Ng5SliderModule } from 'ng5-slider';
import { MainPipe } from '../../../service/main-pipe.module'

import { BankloyaltymerchantComponent } from './bankloyaltymerchant.component';
import { DynamicRoutesguardGuard } from "src/app/service/guard/dynamicroutes.guard";

const routes: Routes = [{ path: "",canActivateChild:[DynamicRoutesguardGuard],data:{accessName:['acqmerchant']}, component: BankloyaltymerchantComponent }];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BankloyaltymerchantRoutingModule { }

@NgModule({
  declarations: [BankloyaltymerchantComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    BankloyaltymerchantRoutingModule,
    Select2Module,
    Daterangepicker,    
    Ng2FileInputModule,
    Ng5SliderModule,
    NgxPaginationModule,
    MainPipe
  ]
})
export class BankloyaltymerchantModule { }

import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { TUMerchantComponent } from "./tumerchant.component";
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
// import { LeafletModule } from '@asymmetrik/ngx-leaflet';
// import { LeafletMarkerClusterModule } from '@asymmetrik/ngx-leaflet-markercluster';
import { MainPipe } from '../../../../service/main-pipe.module'
import { HCXAddressManagerModule } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';



const routes: Routes = [
    {
        path: "",
        component: TUMerchantComponent,
        children: [
            { path: "/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../../Acquirer/dashboards/merchant/acquirer/dashboard.module#TUDashboardModule" },
            { path: "dashboard/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../../Acquirer/dashboards/merchant/acquirer/dashboard.module#TUDashboardModule" },
            { path: 'sales/saleshistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../Acquirer/transactions/tusale/merchant/acquirer/tusale.module#TUSaleModule' },
            { path: 'terminals/:referencekey/:referenceid', data: { 'permission': 'terminals', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../Acquirer/account/tuterminals/merchant/tuterminals.module#TUTerminalsModule' },
            { path: 'stores/:referencekey/:referenceid', data: { 'permission': 'stores', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../Acquirer/account/tustores/merchant/tustores.module#TUStoresModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUMerchantRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUMerchantRoutingModule,
        GooglePlaceModule,
        MainPipe,
        HCXAddressManagerModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),

        // LeafletModule,
        // LeafletMarkerClusterModule

    ],
    declarations: [TUMerchantComponent]
})
export class TUMerchantModule { }

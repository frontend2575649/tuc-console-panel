import { ChangeDetectorRef, Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ChartDataSets } from 'chart.js';
// import moment from 'moment';
import { BaseChartDirective, Color } from 'ng2-charts';
import { Subscription, Observable } from 'rxjs';
import { DataHelperService } from 'src/app/service/datahelper.service';
import { HelperService } from 'src/app/service/helper.service';
import { OLoyalityHistory, OLoyalityHistoryData, OResponse, OSalesTrend } from 'src/app/service/object.service';
declare var moment: any;
import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-loyaltybankoverview',
  templateUrl: './loyaltybankoverview.component.html',
})
export class LoyaltybankoverviewComponent implements OnInit {

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];
  public MonthlybarChartLabels = [];
  public _OLoyalityHistory: OLoyalityHistory = {
    NewCustomerInvoiceAmount_Sum: 0,
    NewCustomer_Sum: 0,
    RepeatingCustomerSaleAmount_Sum: 0,
    RepeatingCustomer_Sum: 0,
    TotalCustomer_Sum: 0,
    TotalInvoiceAmount_Sum: 0,
    RepeatingCustomerInvoiceAmount: 0
  };
  public _DateSubscription: Subscription = null;
  public acquireDetails: any = {};
  public programId;

  //#region Visits Chart Data 
  public VisitsChartData: ChartDataSets[] = [

    { data: [], type: 'bar', label: 'Returning Visits', hidden: false },
    { data: [], type: 'bar', label: 'New Visits', hidden: false }

  ];
  public VisitsChartColors: Color[] = [
    {
      backgroundColor: 'rgba(0, 204, 204, 1)',
    },
    {
      backgroundColor: 'rgba(0, 204, 204, .10)',
    },
  ];
  public VisitsChartLabels = [];
  //#endregion

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef

  ) {
    this._HelperService.ShowDateRange = true;
  }



  //#endregion



  ngOnInit() {
    this.acquireDetails = this._HelperService.GetStorage('acquirerdetails');
    if (this.acquireDetails.ProgramDetails && this.acquireDetails.ProgramDetails.length > 0) {
      this.programId = this.acquireDetails.ProgramDetails[0].ProgramId;
    }

    this._ActivatedRoute.params.subscribe((params: Params) => {
      // this._HelperService.AppConfig.ActiveMerchantReferenceKey = params["referencekey"];
      // this._HelperService.AppConfig.ActiveMerchantReferenceId = params["referenceid"];

      if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
        this._Router.navigate([
          this._HelperService.AppConfig.Pages.System.NotFound
        ]);
      } else {
        this._HelperService.ResetDateRange();
        this.LoadData();
      }
    });
    this.GetAccountDetails();
    this.getAccountBalance();

  }
  showVisitsLoalityChart: boolean = true;

  LoadData() {
    this.getloyaltyoverview();
    this.GetLoyalityReport(moment().startOf('day'), moment().endOf('day'),
      this.LoyalityData);
  }

  Toogle_Dataset_Visibility(dataset_label: string): void {

    for (let index = 0; index < this.VisitsChartData.length; index++) {
      const element = this.VisitsChartData[index];

      if (element.label == dataset_label) {

        //#region Reload_UI 

        this.showVisitsLoalityChart = false;
        this._ChangeDetectorRef.detectChanges();

        element.hidden = !element.hidden;

        this.showVisitsLoalityChart = true;
        this._ChangeDetectorRef.detectChanges();

        //#endregion
        break;
      }
    }

  }

  private pLoyalityData = {
    Task: 'getgrouployaltyvisithistory',
    StartDate: null,
    EndDate: null,
    AccountKey: this.acquireDetails.ReferenceKey,
    AccountId: this.acquireDetails.ReferenceId,
    Type: null,
    ProgramId: null
  };

  TitleIsHour(title: string): boolean {
    return (title.includes('AM') || title.includes('PM'));
  }

  SelectedDateRangeType: string = this._HelperService.AppConfig.GraphTypes.week;
  Intermediate: any[];
  CalculateDateRangeType(start, end): void {

    this.Intermediate = this._HelperService.CalculateIntermediateDate(start, end);

    if (this.Intermediate.length <= 7) {
      this.SelectedDateRangeType = this._HelperService.AppConfig.GraphTypes.week;
    } else if (this.Intermediate.length > 7 && this.Intermediate.length < 32) {
      this.SelectedDateRangeType = this._HelperService.AppConfig.GraphTypes.month;
    } else if (this.Intermediate.length > 32) {
      this.SelectedDateRangeType = this._HelperService.AppConfig.GraphTypes.year;
    }

  }

  GetLoyalityReport(StartDateTime, EndDateTime, Data: OLoyalityHistoryData[]) {

    this._OLoyalityHistory = {
      NewCustomerInvoiceAmount_Sum: 0,
      NewCustomer_Sum: 0,
      RepeatingCustomerSaleAmount_Sum: 0,
      RepeatingCustomer_Sum: 0,
      TotalCustomer_Sum: 0,
      TotalInvoiceAmount_Sum: 0,
      RepeatingCustomerInvoiceAmount: 0
    };

    this._HelperService.IsFormProcessing = true;

    this.pLoyalityData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pLoyalityData.EndDate = this._HelperService.DateInUTC(EndDateTime);
    this.pLoyalityData.ProgramId = this.programId;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.pLoyalityData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OLoyalityHistoryData[];

          var ReturningSales = [];
          var NewSales = [];

          var ReturningVisits = [];
          var NewVisits = [];

          var ChartLabels: any[] = [];



          if (Data.length > 0 && this.TitleIsHour(Data[0].Title)) {

            for (let index = 0; index < 24; index++) {

              //#region current 

              var dd = " AM";
              var h = index;
              if (h >= 12) {
                h = index - 12;
                dd = " PM";
              }
              if (h == 0) {
                h = 12;
              }
              var Hour = h + ":00" + dd;
              ChartLabels.push(Hour);

              //#endregion

              var RData: any = Data.find(x => x.Title == Hour);
              if (RData != undefined && RData != null) {
                const element: any = RData;

                //#region Calculate Totals 

                this._OLoyalityHistory.NewCustomer_Sum = this._OLoyalityHistory.NewCustomer_Sum + element.NewCustomer;
                this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum = this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum + element.NewCustomerInvoiceAmount;
                this._OLoyalityHistory.RepeatingCustomer_Sum = this._OLoyalityHistory.RepeatingCustomer_Sum + element.RepeatingCustomer;
                this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum = this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum + element.RepeatingCustomerInvoiceAmount;
                this._OLoyalityHistory.TotalCustomer_Sum = this._OLoyalityHistory.TotalCustomer_Sum + element.NewCustomer + element.RepeatingCustomer;
                this._OLoyalityHistory.TotalInvoiceAmount_Sum = this._OLoyalityHistory.TotalInvoiceAmount_Sum + element.NewCustomerInvoiceAmount + element.RepeatingCustomerInvoiceAmount;

                //#endregion

                ReturningSales.push(element.RepeatingCustomerInvoiceAmount);
                NewSales.push(element.NewCustomerInvoiceAmount);
                ReturningVisits.push(element.VisitsByRepeatingCustomers);
                NewVisits.push(element.NewCustomer);

              }
              else {
                ReturningSales.push(0.0);
                NewSales.push(0.0);
                ReturningVisits.push(0.0);
                NewVisits.push(0.0);

              }
            }

          } else {
            this.CalculateDateRangeType(cloneDeep(StartDateTime), cloneDeep(EndDateTime));

            if (this.SelectedDateRangeType == this._HelperService.AppConfig.GraphTypes.week) {

              for (let index = 0; index < this.Intermediate.length; index++) {
                const element = this.Intermediate[index];
                ChartLabels.push(element);
                var RData: any = Data.find(x => moment(x.Title, 'DD-MM-YYYY').format('DD MMM') == element);
                if (RData != undefined && RData != null) {
                  const element: any = RData;

                  //#region Calculate Totals 

                  this._OLoyalityHistory.NewCustomer_Sum = this._OLoyalityHistory.NewCustomer_Sum + element.NewCustomer;
                  this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum = this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum + element.NewCustomerInvoiceAmount;
                  this._OLoyalityHistory.RepeatingCustomer_Sum = this._OLoyalityHistory.RepeatingCustomer_Sum + element.RepeatingCustomer;
                  this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum = this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum + element.RepeatingCustomerInvoiceAmount;
                  this._OLoyalityHistory.TotalCustomer_Sum = this._OLoyalityHistory.TotalCustomer_Sum + element.NewCustomer + element.RepeatingCustomer;
                  this._OLoyalityHistory.TotalInvoiceAmount_Sum = this._OLoyalityHistory.TotalInvoiceAmount_Sum + element.NewCustomerInvoiceAmount + element.RepeatingCustomerInvoiceAmount;

                  //#endregion

                  ReturningSales.push(element.RepeatingCustomerInvoiceAmount);
                  NewSales.push(element.NewCustomerInvoiceAmount);
                  ReturningVisits.push(element.VisitsByRepeatingCustomers);
                  NewVisits.push(element.NewCustomer);

                }
                else {
                  ReturningSales.push(0.0);
                  NewSales.push(0.0);
                  ReturningVisits.push(0.0);
                  NewVisits.push(0.0);

                }
              }

            } else if (this.SelectedDateRangeType == this._HelperService.AppConfig.GraphTypes.month) {

              for (let index = 0; index < this.Intermediate.length; index++) {
                const element = this.Intermediate[index];
                ChartLabels.push(element);
                var RData: any = Data.find(x => moment(x.Title, 'DD-MM-YYYY').format('DD MMM') == element);
                if (RData != undefined && RData != null) {
                  const element: any = RData;

                  //#region Calculate Totals 

                  this._OLoyalityHistory.NewCustomer_Sum = this._OLoyalityHistory.NewCustomer_Sum + element.NewCustomer;
                  this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum = this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum + element.NewCustomerInvoiceAmount;
                  this._OLoyalityHistory.RepeatingCustomer_Sum = this._OLoyalityHistory.RepeatingCustomer_Sum + element.RepeatingCustomer;
                  this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum = this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum + element.RepeatingCustomerInvoiceAmount;
                  this._OLoyalityHistory.TotalCustomer_Sum = this._OLoyalityHistory.TotalCustomer_Sum + element.NewCustomer + element.RepeatingCustomer;
                  this._OLoyalityHistory.TotalInvoiceAmount_Sum = this._OLoyalityHistory.TotalInvoiceAmount_Sum + element.NewCustomerInvoiceAmount + element.RepeatingCustomerInvoiceAmount;

                  //#endregion

                  ReturningSales.push(element.RepeatingCustomerInvoiceAmount);
                  NewSales.push(element.NewCustomerInvoiceAmount);
                  ReturningVisits.push(element.VisitsByRepeatingCustomers);
                  NewVisits.push(element.NewCustomer);

                }
                else {
                  ReturningSales.push(0.0);
                  NewSales.push(0.0);
                  ReturningVisits.push(0.0);
                  NewVisits.push(0.0);

                }
              }

            } else if (this.SelectedDateRangeType == this._HelperService.AppConfig.GraphTypes.year) {

              for (let index = 0; index < this.Intermediate.length; index++) {
                const element = this.Intermediate[index];
              }

            }


          }

          this.MonthlybarChartLabels = ChartLabels;

          this.showVisitsLoalityChart = false;
          this._ChangeDetectorRef.detectChanges();


          this.VisitsChartData[0].data = ReturningVisits;
          this.VisitsChartData[1].data = NewVisits;

          this.showVisitsLoalityChart = true;
          this._ChangeDetectorRef.detectChanges();

          this._HelperService.IsFormProcessing = false;

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  //#region AccountOverview 
  public _UserAccount: any =
    {
      MerchantDisplayName: null,
      SecondaryEmailAddress: null,
      BankDisplayName: null,
      BankKey: null,
      OwnerName: null,
      SubOwnerAddress: null,
      SubOwnerLatitude: null,
      SubOwnerDisplayName: null,
      SubOwnerKey: null,
      SubOwnerLongitude: null,
      AccessPin: null,
      LastLoginDateS: null,
      AppKey: null,
      AppName: null,
      AppVersionKey: null,
      CreateDate: null,
      CreateDateS: null,
      CreatedByDisplayName: null,
      CreatedByIconUrl: null,
      CreatedByKey: null,
      Description: null,
      IconUrl: null,
      ModifyByDisplayName: null,
      ModifyByIconUrl: null,
      ModifyByKey: null,
      ModifyDate: null,
      ModifyDateS: null,
      PosterUrl: null,
      ReferenceKey: null,
      StatusCode: null,
      StatusI: null,
      StatusId: null,
      StatusName: null,
      AccountCode: null,
      AccountOperationTypeCode: null,
      AccountOperationTypeName: null,
      AccountTypeCode: null,
      AccountTypeName: null,
      Address: null,
      AppVersionName: null,
      ApplicationStatusCode: null,
      ApplicationStatusName: null,
      AverageValue: null,
      CityAreaKey: null,
      CityAreaName: null,
      CityKey: null,
      CityName: null,
      ContactNumber: null,
      CountValue: null,
      CountryKey: null,
      CountryName: null,
      DateOfBirth: null,
      DisplayName: null,
      EmailAddress: null,
      EmailVerificationStatus: null,
      EmailVerificationStatusDate: null,
      FirstName: null,
      GenderCode: null,
      GenderName: null,
      LastLoginDate: null,
      LastName: null,
      Latitude: null,
      Longitude: null,
      MobileNumber: null,
      Name: null,
      NumberVerificationStatus: null,
      NumberVerificationStatusDate: null,
      OwnerDisplayName: null,
      OwnerKey: null,
      Password: null,
      Reference: null,
      ReferralCode: null,
      ReferralUrl: null,
      RegionAreaKey: null,
      RegionAreaName: null,
      RegionKey: null,
      RegionName: null,
      RegistrationSourceCode: null,
      RegistrationSourceName: null,
      RequestKey: null,
      RoleKey: null,
      RoleName: null,
      SecondaryPassword: null,
      SystemPassword: null,
      UserName: null,
      WebsiteUrl: null,

      StateKey: null,
      StateName: null

    }
  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
    //    this._ChangeDetectorRef.detectChanges();
  }
  GetAccountDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetAcquirer,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,
      Accountkey: this._HelperService.AppConfig.ActiveReferenceKey,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.toogleIsFormProcessing(false);
          this._UserAccount = _Response.Result;
          this._HelperService.isProgramDetails = _Response.Result.ProgramDetails ? true : false
          this._HelperService.acquirerProgramDetails = _Response.Result.ProgramDetails ? _Response.Result.ProgramDetails : null
          this._HelperService.SaveStorage('acquirerdetails', _Response.Result);
          //#region RelocateMarker 

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          // this.FormC_EditUser_Latitude = this._UserAccount.Latitude;
          // this.FormC_EditUser_Longitude = this._UserAccount.Longitude;

          // this._HelperService._ReLocate();

          //#endregion

          //#region DatesAndStatusInit 

          this._UserAccount.StartDateS = this._HelperService.GetDateS(
            this._UserAccount.StartDate
          );
          this._UserAccount.EndDateS = this._HelperService.GetDateS(
            this._UserAccount.EndDate
          );
          this._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(
            this._UserAccount.CreateDate
          );
          this._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(
            this._UserAccount.ModifyDate
          );
          this._UserAccount.StatusI = this._HelperService.GetStatusIcon(
            this._UserAccount.StatusCode
          );
          this._UserAccount.StatusB = this._HelperService.GetStatusBadge(
            this._UserAccount.StatusCode
          );
          this._UserAccount.StatusC = this._HelperService.GetStatusColor(
            this._UserAccount.StatusCode
          );


          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public _AccountOverview: OAccountOverview =
    {
      OwnerCardTransactions: 0,
      CardTypeSale: 0,
      ActiveMerchants: 0,
      ActiveMerchantsDiff: 0,
      ActiveTerminals: 0,
      ActiveTerminalsDiff: 0,
      CardRewardPurchaseAmount: 0,
      CardRewardPurchaseAmountDiff: 0,
      CashRewardPurchaseAmount: 0,
      CashRewardPurchaseAmountDiff: 0,
      Merchants: 0,
      PurchaseAmount: 0,
      PurchaseAmountDiff: 0,
      Terminals: 0,
      Transaction: 0,
      TransactionsDiff: 0,
      TotalTransactions: 0,
      TotalSale: 0,
      AverageTransactions: 0,
      AverageTransactionAmount: 0,
      CashTransactionAmount: 0,
      CashTransPerTerm: 0,
      OwnerCardTotal: 0,
      CardTransactionsAmount: 0,
      OtherCardTotal: 0,
      OtherCards: 0,
      CardTransPerTerm: 0,
      RedeemAmount: 0,
      RewardAmount: 0

    }

  LoyalityData: OLoyalityHistoryData[];

  onChartClick(event: any) { }

  public _Monthly: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }
  CurrentDate: any;
  //#endregion
  DateChanged(event: any, Type: any): void {

    this.CurrentDate = cloneDeep(event);
    //#region Monthly 
    this._Monthly.ActualStartDate = moment(this.CurrentDate.start).startOf('day');
    this._Monthly.ActualEndDate = moment(this.CurrentDate.end).endOf('day');
    // this._Monthly.CompareStartDate = moment(ev.start).subtract(1, 'months').startOf('month').startOf('day');
    // this._Monthly.CompareEndDate = moment(ev.end).subtract(1, 'months').endOf('month').endOf('day');
    //#endregion
    // this.GetLoyaltyOverviewLite();
    // this._MonthlySalesReportGetActualData();
    this.getloyaltyoverview();
    this.GetLoyalityReport(moment(this.CurrentDate.start).startOf('day'), moment(this.CurrentDate.end).endOf('day'), this.LoyalityData);

  }

  getloyaltyoverview() {
    const pData = {
      Task: "getgrouployaltyoverview",
      StartDate: this._HelperService.DateInUTC(this._Monthly.ActualStartDate),
      EndDate: this._HelperService.DateInUTC(this._Monthly.ActualEndDate),
      AccountKey: this.acquireDetails.ReferenceKey,
      AccountId: this.acquireDetails.ReferenceId,
      ProgramId: this.programId
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverview = _Response.Result as OAccountOverview;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });

  }

  AccountBalance: any;

  getAccountBalance() {
    const pData = {
      Task: "getaccountbalance",
      AccountKey: this.acquireDetails.ReferenceKey,
      AccountId: this.acquireDetails.ReferenceId,
      ProgramId: this.programId,
      Source: "transaction.source.acquirer",
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Program, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.AccountBalance = _Response.Result;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  TransactionId: any;

  generateRandomNumber() {
    var TrRef = Math.floor(100000 + Math.random() * (999999 + 1 - 100000));
    return TrRef;
  }

  OpenPaymentOptions() {
    this._AccountBalanceCreditAmount = 0;
    this.PaymentReference = null;
    this.TransactionId = this.generateRandomNumber();
    this._HelperService.OpenModal('Form_TopUp_Content');
  }

  _AccountBalanceCreditAmount: 0;
  public PaymentReference = null;

  creditWallet() {
    if (this._AccountBalanceCreditAmount  <= 0) {
      this._HelperService.NotifyError("Amount should be greater than 0");
      return;
    }
    const pData = {
      Task: "creditwallet",
      AccountKey: this.acquireDetails.ReferenceKey,
      AccountId: this.acquireDetails.ReferenceId,
      ProgramId: this.programId,
      Amount: this._AccountBalanceCreditAmount,
      PaymentReference: this.PaymentReference,
      TransactionId: this.TransactionId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Program, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.AccountBalance = _Response.Result;
          this._HelperService.CloseModal('Form_TopUp_Content');
          this.getAccountBalance();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

}
export class OAccountOverview {
  public TotalTransactions: any;
  public TotalSale: any;
  public AverageTransactionAmount: any;
  public AverageTransactions: any;
  public CashTransactionAmount: any;
  public CashTransPerTerm: any;
  public CardTransactionsAmount: any;
  public CardTransPerTerm: any;
  public OwnerCardTotal: any;
  public OtherCardTotal: any;
  public OtherCards: any;
  public OwnerCardTransactions: any;

  public CardTypeSale: any;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transaction: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
  public RewardAmount: number;
  public RedeemAmount: number;
}

import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, OResponse } from "../../../service/service";

@Component({
  selector: "tu-branchdetails",
  templateUrl: "branchdetails.component.html"
})
export class BranchDetailsComponent implements OnInit {
  _ShowMap = false;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) { }

  ngOnInit() {

    //#region StorageDetail 

    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveBranch);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveBranchReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveBranchReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }

    //#endregion

    this._ShowMap = true;
    // this._HelperService._InitMap();

    //#region UIInit 

    Feather.replace();
    this._HelperService.ValidateData();
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = this._HelperService.AppConfig.AccountType.Acquirer;
    this._HelperService.ContainerHeight = window.innerHeight;

    //#endregion

    this.GetBranchDetails();

  }

  //#region MapCorrection 

  slideOpen: any = false;

  changeSlide(): void {
    this.slideOpen = !this.slideOpen;
    if (this.slideOpen) {
      // this._HelperService._MapCorrection();
    }
  }

  //#endregion

  //#region ToogleStoreDetail 

  _HideStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-DisplayDiv");
  }

  _ShowStoreDetails() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-DisplayDiv");
    element.classList.remove("Hm-HideDiv");
  }

  //#endregion

  //#region BranchDetails 

  public _BranchDetails:any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,

      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }

  EditBranch() {
    this._HelperService.AppConfig.ActiveBranchReferenceKey = this._BranchDetails.ReferenceKey;
    this._HelperService.AppConfig.ActiveMerchantReferenceId = this._BranchDetails.ReferenceId;
    this._Router.navigate([ "console/" + this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Branches.EditBranch, this._BranchDetails.ReferenceKey, this._BranchDetails.ReferenceId]);
  }

  _DateDiff: any = {};
  GetBranchDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetBranch,
      ReferenceKey: this._HelperService.AppConfig.ActiveBranchReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveBranchReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Branch, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._BranchDetails = _Response.Result;

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          // this._HelperService._ReLocate();

          this._DateDiff = this._HelperService._DateRangeDifference(this._BranchDetails.StartDate, this._BranchDetails.EndDate);
          this._BranchDetails.StartDateS = this._HelperService.GetDateS(
            this._BranchDetails.StartDate
          );
          this._BranchDetails.EndDateS = this._HelperService.GetDateS(
            this._BranchDetails.EndDate
          );
          this._BranchDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._BranchDetails.CreateDate
          );
          this._BranchDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._BranchDetails.ModifyDate
          );
          this._BranchDetails.StatusI = this._HelperService.GetStatusIcon(
            this._BranchDetails.StatusCode
          );
          this._BranchDetails.StatusB = this._HelperService.GetStatusBadge(
            this._BranchDetails.StatusCode
          );
          this._BranchDetails.StatusC = this._HelperService.GetStatusColor(
            this._BranchDetails.StatusCode
          );



        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

}
import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AgmOverlays } from 'agm-overlays';
import { ChartsModule } from 'ng2-charts';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { TUDashboardComponent } from './dashboard.component';
import { ImageCropperModule } from 'ngx-image-cropper';

import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { HCXAddressManagerModule } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';
import { DynamicRoutesguardGuard } from 'src/app/service/guard/dynamicroutes.guard';
// const routes: Routes = [
//     { path: '', component: TUDashboardComponent }
// ];

const routes: Routes = [
    {
        path: "",
        component: TUDashboardComponent,
        canActivateChild:[DynamicRoutesguardGuard],
        data:{accessName:['referalcampaignview']},
        children: [
            // { path: "", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../dashboards/tuanalyticsreferal/tuanalyticsreferal.module#TUAnalyticsReferalModule" }]
            { path: "", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../Acquirer/accountdetails/turefer/tucodeusage/tucodeusage.module#TUCodeUsageModule" },
            // { path: "referalaccount/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../dashboards/tudetailreferal/dashboard.module#TUDashboardModule" },
            // { path: "analyticsreferal/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../Acquirer/accountdetails/turefer/tuanalyticsreferal/tuanalyticsreferal.module#TUAnalyticsReferalModule" }],
            //  { path: "tuanalyticsreferal/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../dashboards/tuanalyticsreferal/tuanalyticsreferal.module#TUAnalyticsReferalModule" }]
            // { path: "referalaccount/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../dashboards/tudetailreferal/dashboard.module#TUDashboardModule" },
            { path: "analyticsreferal/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../Acquirer/accountdetails/turefer/tuanalyticsreferal/tuanalyticsreferal.module#TUAnalyticsReferalModule" }]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUDashboardRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUDashboardRoutingModule,
        GooglePlaceModule,
        AgmOverlays,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        ChartsModule,
        ImageCropperModule,
        HCXAddressManagerModule
    ],
    declarations: [TUDashboardComponent]
})
export class TUDashboardModule {

} 

import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of, Subscription } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
declare var moment: any;
import * as pluginAnnotations from 'chartjs-plugin-annotation';

import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon, OOverview } from '../../../../service/service';
import { DaterangePickerComponent } from 'ng2-daterangepicker';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUDashboardComponent implements OnInit, OnDestroy {

  

  public Options: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      }
    }
  }
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
  ) {
    this._HelperService.ShowDateRange = true;
  }

  public barChartLabels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
  public barChartColors = [{ backgroundColor: ['#10b759', '#FF0000', '#FFC20A', '#10b759', '#FF0000', '#FFC20A', '#10b759', '#FF0000', '#FFC20A', '#10b759', '#FF0000', '#FFC20A'] }];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [100, 20, 80, 100, 30, 85, 100, 35, 90, 100, 20, 95] },

  ];

  public TodayStartTime = null;
  public TodayEndTime = null;
  public _DateSubscription: Subscription = null;

  Type = 5;
  StartTime = null;
  StartTimeS = null;
  EndTime = null;
  EndTimeS = null;
  CustomType = 1;

  ngOnDestroy() {
    this._DateSubscription.unsubscribe();
  }

  ngOnInit() {

    this._DateSubscription = this._HelperService.RangeAltered.subscribe(value => {
      //this.GetAccountOverviewLite();
    });

    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
      if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
      } else {
        this._HelperService.FullContainer = false;
        if (this.StartTime == undefined) {
          this.StartTime = moment().startOf('month');
          this.EndTime = moment().endOf('month');
        }

        if (this.TodayStartTime == undefined) {
          this.TodayStartTime = moment().startOf('day');
          this.TodayEndTime = moment().add(1, 'minutes');
        }

        this._HelperService.DateRangeStart = moment().startOf('day');
        this._HelperService.DateRangeEnd = moment().add(1, 'minutes');

        this.LoadData();
      }
    });

 this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
      // if (this._CoreDataHelper.OCoreParameter.ReferenceKey == null) {
      //     this._Router.navigate([this._DataStoreService.PageLinks.Core.NotFound]);
      // }
      // else {
      //     this.Get_Details();
      // } 
    });
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }

  UserAnalytics_DateChange(Type) {
    this.Type = Type;
    var SDate;
    if (Type == 1) { // today
      SDate =
      {
        start: moment().startOf('day'),
        end: moment().endOf('day'),
      }
    }
    else if (Type == 2) { // yesterday
      SDate =
      {
        start: moment().subtract(1, 'days').startOf('day'),
        end: moment().subtract(1, 'days').endOf('day'),
      }
    }
    else if (Type == 3) {  // this week
      SDate =
      {
        start: moment().startOf('isoWeek'),
        end: moment().endOf('isoWeek'),
      }
    }
    else if (Type == 4) { // last week
      SDate =
      {
        start: moment().subtract(1, 'weeks').startOf('isoWeek'),
        end: moment().subtract(1, 'weeks').endOf('isoWeek'),
      }
    }
    else if (Type == 5) { // this month
      SDate =
      {
        start: moment().startOf('month'),
        end: moment().endOf('month'),
      }
    }
    else if (Type == 6) { // last month
      SDate =
      {
        start: moment().startOf('month').subtract(1, 'month'),
        end: moment().startOf('month').subtract(1, 'days'),
      }
    }
    else if (Type == 7) {
      SDate =
      {
        start: new Date(2017, 0, 1, 0, 0, 0, 0),
        end: moment().endOf('day'),
      }
    }
  
    this.StartTime = SDate.start;
    this.EndTime = SDate.end;
    this.LoadData();
  }
  LoadData() {
    // this.StartTime = new Date(2017, 0, 1, 0, 0, 0, 0);
    // this.EndTime = moment().add(2, 'days');
    this.StartTimeS = this._HelperService.GetDateS(this.StartTime);
    this.EndTimeS = this._HelperService.GetDateS(this.EndTime);
    //this.GetAccountOverviewLite();

  }

  GetAccountOverviewLite() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getaccountoverviewlite',
      StartTime: this._HelperService.DateRangeStart, // new Date(2017, 0, 1, 0, 0, 0, 0),
      EndTime: this._HelperService.DateRangeEnd, // moment().add(2, 'days'),
      UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.ThankU, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverview = _Response.Result as OAccountOverview;

          if (this._AccountOverview.Terminals != 0) {
            var _TempVal = 100 / this._AccountOverview.Terminals;
            this._AccountOverview["IdleTerminalsPerc"] = this._AccountOverview.IdleTerminals * _TempVal;
            this._AccountOverview["ActiveTerminalsPerc"] = this._AccountOverview.ActiveTerminals * _TempVal;
            this._AccountOverview["DeadTerminalsPerc"] = this._AccountOverview.DeadTerminals * _TempVal;
            this._AccountOverview["UnusedTerminalsPerc"] = this._AccountOverview.UnusedTerminals * _TempVal;
          } else {
            this._AccountOverview["IdleTerminalsPerc"] = 0;
            this._AccountOverview["ActiveTerminalsPerc"] = 0;
            this._AccountOverview["DeadTerminalsPerc"] = 0;
            this._AccountOverview["UnusedTerminalsPerc"] = 0;
          }



        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  public TUTr_Config: OList;
  TUTr_Setup() {
    this.TUTr_Config =
    {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetSaleTransactions,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore,
      Title: 'Sales History',
      StatusType: 'transaction',
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '='),
      Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
      StatusName:"Success",
      Type: this._HelperService.AppConfig.ListType.All,
      DefaultSortExpression: 'TransactionDate desc',
      RefreshCount: false,
      PageRecordLimit: 6,
      TableFields: [
        {
          DisplayName: '#',
          SystemName: 'ReferenceId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Show: true,
          Search: false,
          Sort: false,
          // NavigateField: 'UserAccountKey',
          // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
        },
        {
          DisplayName: 'Date',
          SystemName: 'TransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Show: true,
          Search: false,
          Sort: true,
          IsDateSearchField: true,
        },
      ]
    }
    this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
    this.TUTr_GetData();
  }
  TUTr_GetData() {
    var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
    this.TUTr_Config = TConfig;
  }
  public _AccountOverview: OAccountOverview =
    {
      ActiveMerchants: 0,
      ActiveMerchantsDiff: 0,
      ActiveTerminals: 0,
      ActiveTerminalsDiff: 0,
      CardRewardPurchaseAmount: 0,
      CardRewardPurchaseAmountDiff: 0,
      CashRewardPurchaseAmount: 0,
      CashRewardPurchaseAmountDiff: 0,
      Merchants: 0,
      PurchaseAmount: 0,
      PurchaseAmountDiff: 0,
      Terminals: 0,
      Transactions: 0,
      TransactionsDiff: 0,
      UnusedTerminals: 0,
      IdleTerminals: 0,
      DeadTerminals: 0,

    }

  public _OSalesHistory =
    {
      Labels: [],
      SaleColors: [],
      SaleDataSet: [],

      SaleCustomersColors: [],
      SaleCustomersDataSet: [],

      TransactionsDataSetColors: [],
      TransactionsDataSet: [],

      TransactionStatusDataSetColors: [],
      TransactionStatusDataSet: [],

      TransactionTypeDataSetColors: [],
      TransactionTypeDataSet: [],

      TransactionTypeCustomersDataSetColors: [],
      TransactionTypeCustomersDataSet: [],
    }
  GetSalesHistory() {
    var SearchCondition = '';
    SearchCondition = this._HelperService.GetDateCondition('', 'Date', this.StartTime, this.EndTime);
    SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'UserAccountId', this._HelperService.AppConfig.DataType.Number, '1', '=');
    var DaysCount = this.EndTime.diff(this.StartTime, 'days');
    var Type = 'days';
    if (DaysCount == 1) {
      Type = 'day';
    }
    else if (DaysCount < 31) {
      Type = 'days';
    }
    else if (DaysCount > 30 && DaysCount < 366) {
      Type = 'month';
    }
    else if (DaysCount > 364) {
      Type = 'year';
    }
    var Data = {
      Task: "getaccountsalessummary",
      TotalRecords: 0,
      Offset: 0,
      Limit: 90,
      RefreshCount: false,
      ReferenceId: 1,
      Type: Type,
      SearchCondition: SearchCondition,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCAnalytics, Data);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          var TStatusDataSet = [];
          var TStatusDataSetColor = [];
          var TransactionsDataSet = [];
          var TransactionsDataSetColor = [];
          this._OSalesHistory.Labels = [];
          var ResponseData = _Response.Result.Data;
          var TSaleColor = [{
            backgroundColor: [],
          }];
          var TSaleDataSet = [{
            label: 'Sale',
            fill: false,
            borderColor: "#907eec",
            borderDash: [10, 0.5],
            pointBorderWidth: 2,
            pointRadius: 4,
            fillColor: "#e9e5ff",
            backgroundColor: ["#907eec"],
            pointBackgroundColor: "#ff9900",
            pointBorderColor: "#907eec",
            pointHoverBackgroundColor: "#907eec",
            pointHoverBorderColor: "#ff9900",
            data: []
          }];
          var TSaleCustomersColor = [{
            backgroundColor: [],
          }];
          var TSaleCustomersDataSet = [{
            label: 'Customer Visits',
            fill: false,
            borderColor: "#907eec",
            borderDash: [10, 0.5],
            pointBorderWidth: 2,
            pointRadius: 4,
            fillColor: "#e9e5ff",
            backgroundColor: ["#907eec"],
            pointBackgroundColor: "#ff9900",
            pointBorderColor: "#907eec",
            pointHoverBackgroundColor: "#907eec",
            pointHoverBorderColor: "#ff9900",
            data: []
          }];
          var TTypeDataSetColor = [];
          var TTypeDataSet = [];
          var TTypeCustomersDataSetColor = [];
          var TTypeCustomersDataSet = [];
          var TTypeDataSetCardItemColor = {
            backgroundColor: [],
          };
          var TTypeDataSetCashItemColor = {
            backgroundColor: [],
          };
          var TTypeCustomersDataSetCardItemColor = {
            backgroundColor: [],
          };
          var TTypeCustomersDataSetCashItemColor = {
            backgroundColor: [],
          };
          var TTypeDataSetCardItem = {
            label: 'Card',
            backgroundColor: ["#61c03c"],
            fill: false,
            borderColor: "#61c03c",
            pointBackgroundColor: "#227900",
            pointBorderColor: "#61c03c",
            borderDash: [10, 0.5],
            pointBorderWidth: 2,
            pointRadius: 4,
            pointHoverBackgroundColor: "#61c03c",
            pointHoverBorderColor: "#227900",
            data: []
          };
          var TTypeDataSetCashItem = {
            label: 'Cash',
            backgroundColor: ["#ff9900"],
            fill: false,
            borderColor: "#ff9900",
            borderDash: [10, 0.5],
            pointBorderWidth: 2,
            pointRadius: 4,
            pointBackgroundColor: "#ff6200",
            pointBorderColor: "#ff9900",
            pointHoverBackgroundColor: "#ff9900",
            pointHoverBorderColor: "#ff6200",
            data: []
          };
          var TTypeCustomersDataSetCardItem = {
            label: 'Card Customers',
            backgroundColor: ["#61c03c"],
            fill: false,
            borderColor: "#61c03c",
            borderDash: [10, 0.5],
            pointBorderWidth: 2,
            pointRadius: 4,
            pointBackgroundColor: "#227900",
            pointBorderColor: "#61c03c",
            pointHoverBackgroundColor: "#61c03c",
            pointHoverBorderColor: "#227900",
            data: []
          };
          var TTypeCustomersDataSetCashItem = {
            label: 'Cash Customers',
            backgroundColor: ["#ff9900"],
            fill: false,
            borderColor: "#ff9900",
            borderDash: [10, 0.5],
            pointBorderWidth: 2,
            pointRadius: 4,
            pointBackgroundColor: "#ff6200",
            pointBorderColor: "#ff9900",
            pointHoverBackgroundColor: "#ff9900",
            pointHoverBorderColor: "#ff6200",
            data: []
          };
          var TransactionsDataSetItemColor = {
            backgroundColor: [],
          };
          var TStatusDataSetSuccessItemColor = {
            backgroundColor: [],
          };
          var TStatusDataSetErrorItemError = {
            backgroundColor: [],
          };
          var TransactionsDataSetItem = {
            label: 'Transactions',
            fill: false,
            borderColor: "#907eec",
            borderDash: [10, 0.5],
            pointBorderWidth: 2,
            pointRadius: 4,
            fillColor: "#e9e5ff",
            backgroundColor: ["#907eec"],
            pointBackgroundColor: "#ff9900",
            pointBorderColor: "#907eec",
            pointHoverBackgroundColor: "#907eec",
            pointHoverBorderColor: "#ff9900",
            data: []
          };
          var TStatusDataSetSuccessItem = {
            label: 'Success',
            backgroundColor: [],
            data: []
          };
          var TStatusDataSetErrorItem = {
            label: 'Error',
            backgroundColor: [],
            data: []
          };
          ResponseData.forEach(element => {
            var Data = element;
            if (Type == 'month') {
              this._OSalesHistory.Labels.push(this._HelperService.GetDateMonthS(element.Date));
            }
            else if (Type == 'year') {
              this._OSalesHistory.Labels.push(this._HelperService.GetDateYearS(element.Date));
            }
            else {
              this._OSalesHistory.Labels.push(this._HelperService.GetDayS(element.Date));
            }
            TSaleDataSet[0].data.push(Math.round(Data.SuccessfulTransactionInvoiceAmount));
            // TSaleColor[0].backgroundColor.push("rgb(144, 126, 236)");
            TSaleCustomersDataSet[0].data.push(Data.SuccessfulTransactionUser);
            // TSaleCustomersColor[0].backgroundColor.push("rgb(144, 126, 236)");

            TTypeDataSetCardItem.data.push(Math.round(Data.CardInvoiceAmount));
            // TTypeDataSetCardItemColor.backgroundColor.push("#61c03c");
            TTypeDataSetCashItem.data.push(Math.round(Data.CashInvoiceAmount));
            // TTypeDataSetCashItemColor.backgroundColor.push("#ff9900");

            TTypeCustomersDataSetCardItem.data.push(Data.CardTransactionUser);
            // TTypeCustomersDataSetCardItemColor.backgroundColor.push("#990099");
            TTypeCustomersDataSetCashItem.data.push(Data.CashTransactionUser);
            // TTypeCustomersDataSetCashItemColor.backgroundColor.push("#00aff0");

            TStatusDataSetSuccessItem.data.push(Data.SuccessfulTransaction);
            TStatusDataSetSuccessItemColor.backgroundColor.push("#00cccc");
            TStatusDataSetErrorItem.data.push(Data.FailedTransaction);
            TStatusDataSetErrorItemError.backgroundColor.push("#f10075");


            TransactionsDataSetItem.data.push(Data.TotalTransaction);
            // TransactionsDataSetItemColor.backgroundColor.push("#61c03c");
          });
          TransactionsDataSet.push(TransactionsDataSetItem);
          TransactionsDataSetColor.push(TransactionsDataSetItemColor);
          TStatusDataSetColor.push(TStatusDataSetSuccessItemColor);
          TStatusDataSetColor.push(TStatusDataSetErrorItemError);
          TStatusDataSet.push(TStatusDataSetSuccessItem);
          TStatusDataSet.push(TStatusDataSetErrorItem);
          TTypeDataSetColor.push(TTypeDataSetCardItemColor);
          TTypeDataSetColor.push(TTypeDataSetCashItemColor);
          TTypeDataSet.push(TTypeDataSetCardItem);
          TTypeDataSet.push(TTypeDataSetCashItem);
          TTypeCustomersDataSetColor.push(TTypeCustomersDataSetCardItemColor);
          TTypeCustomersDataSetColor.push(TTypeCustomersDataSetCashItemColor);
          TTypeCustomersDataSet.push(TTypeCustomersDataSetCardItem);
          TTypeCustomersDataSet.push(TTypeCustomersDataSetCashItem);
          this._OSalesHistory.TransactionsDataSet = TransactionsDataSet;
          this._OSalesHistory.TransactionsDataSetColors = TransactionsDataSetColor;
          this._OSalesHistory.TransactionStatusDataSet = TStatusDataSet;
          this._OSalesHistory.TransactionStatusDataSetColors = TStatusDataSetColor;
          this._OSalesHistory.TransactionTypeDataSet = TTypeDataSet;
          this._OSalesHistory.TransactionTypeDataSetColors = TTypeDataSetColor;
          this._OSalesHistory.TransactionTypeCustomersDataSet = TTypeCustomersDataSet;
          this._OSalesHistory.TransactionTypeCustomersDataSetColors = TTypeCustomersDataSetColor;
          this._OSalesHistory.SaleDataSet = TSaleDataSet;
          this._OSalesHistory.SaleColors = TSaleColor;
          this._OSalesHistory.SaleCustomersDataSet = TSaleCustomersDataSet;
          this._OSalesHistory.SaleCustomersColors = TSaleCustomersColor;
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
}


export class OAccountOverview {

  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
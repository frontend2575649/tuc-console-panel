import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { TUAddCampaignComponent } from "./addcampaign.component";
const routes: Routes = [{ path: "",canActivateChild:[DynamicRoutesguardGuard],data:{accessName:['acqcampaignadd']}, component: TUAddCampaignComponent }];
import { ArchwizardModule } from 'angular-archwizard';
import { DynamicRoutesguardGuard } from "src/app/service/guard/dynamicroutes.guard";
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUAddCampaignRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        TUAddCampaignRoutingModule,
        Select2Module,
        ArchwizardModule,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule
    ],
    declarations: [TUAddCampaignComponent]
})
export class TUAddCampaignModule { }

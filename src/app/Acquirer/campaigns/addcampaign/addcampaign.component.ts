import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import {
    OSelect,
    OList,
    DataHelperService,
    HelperService,
    OResponse,
    OStorageContent,
    OCoreParameter,
    OCoreCommon
} from "../../../service/service";
import swal from "sweetalert2";
declare var moment: any;

@Component({
    selector: "tu-addcampaign",
    templateUrl: "./addcampaign.component.html"
})
export class TUAddCampaignComponent implements OnInit {


    public CampaignKey = null;

    public CampaignType = [
        {
            'TypeCode': 'owncardcustomers',
            'Name': 'Own Bank Card Customers'
        },
        {
            'TypeCode': 'othercardcustomers',
            'Name': 'Other Bank Card Customers'
        }
    ];
    CampaignRewardType_FixedAmount = "fixedamount";
    CampaignRewardType_AmountPercentage = "amountpercentage";
    public CampaignRewardType = [
        {
            'TypeCode': 'fixedamount',
            'Name': 'Fixed Amount'
        },
        {
            'TypeCode': 'amountpercentage',
            'Name': 'Percentage'
        }
    ];

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef

    ) { }
    ngOnInit() {
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
        this.GetAccountDetails();
    }

    public _UserAccount: any =
        {
            MerchantDisplayName: null,
            SecondaryEmailAddress: null,
            BankDisplayName: null,
            BankKey: null,
            OwnerName: null,
            SubOwnerAddress: null,
            SubOwnerLatitude: null,
            SubOwnerDisplayName: null,
            SubOwnerKey: null,
            SubOwnerLongitude: null,
            AccessPin: null,
            LastLoginDateS: null,
            AppKey: null,
            AppName: null,
            AppVersionKey: null,
            CreateDate: null,
            CreateDateS: null,
            CreatedByDisplayName: null,
            CreatedByIconUrl: null,
            CreatedByKey: null,
            Description: null,
            IconUrl: null,
            ModifyByDisplayName: null,
            ModifyByIconUrl: null,
            ModifyByKey: null,
            ModifyDate: null,
            ModifyDateS: null,
            PosterUrl: null,
            ReferenceKey: null,
            StatusCode: null,
            StatusI: null,
            StatusId: null,
            StatusName: null,
            AccountCode: null,
            AccountOperationTypeCode: null,
            AccountOperationTypeName: null,
            AccountTypeCode: null,
            AccountTypeName: null,
            Address: null,
            AppVersionName: null,
            ApplicationStatusCode: null,
            ApplicationStatusName: null,
            AverageValue: null,
            CityAreaKey: null,
            CityAreaName: null,
            CityKey: null,
            CityName: null,
            ContactNumber: null,
            CountValue: null,
            CountryKey: null,
            CountryName: null,
            DateOfBirth: null,
            DisplayName: null,
            EmailAddress: null,
            EmailVerificationStatus: null,
            EmailVerificationStatusDate: null,
            FirstName: null,
            GenderCode: null,
            GenderName: null,
            LastLoginDate: null,
            LastName: null,
            Latitude: null,
            Longitude: null,
            MobileNumber: null,
            Name: null,
            NumberVerificationStatus: null,
            NumberVerificationStatusDate: null,
            OwnerDisplayName: null,
            OwnerKey: null,
            Password: null,
            Reference: null,
            ReferralCode: null,
            ReferralUrl: null,
            RegionAreaKey: null,
            RegionAreaName: null,
            RegionKey: null,
            RegionName: null,
            RegistrationSourceCode: null,
            RegistrationSourceName: null,
            RequestKey: null,
            RoleKey: null,
            RoleName: null,
            SecondaryPassword: null,
            SystemPassword: null,
            UserName: null,
            WebsiteUrl: null,

            StateKey: null,
            StateName: null

        }
    toogleIsFormProcessing(value: boolean): void {
        this._HelperService.IsFormProcessing = value;
        //    this._ChangeDetectorRef.detectChanges();
    }
    GetAccountDetails() {
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccount,
            Reference: this._HelperService.GetSearchConditionStrict(
                "",
                "ReferenceKey",
                this._HelperService.AppConfig.DataType.Text,
                this._HelperService.AppConfig.ActiveReferenceKey,
                "="
            ),
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.toogleIsFormProcessing(false);
                    this._UserAccount = _Response.Result;

                    //#region RelocateMarker 

                    if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
                        this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
                        this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
                    } else {
                        this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
                        this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
                    }
                    // this.FormC_EditUser_Latitude = this._UserAccount.Latitude;
                    // this.FormC_EditUser_Longitude = this._UserAccount.Longitude;

                    // this._HelperService._ReLocate();

                    //#endregion

                    //#region DatesAndStatusInit 

                    this._UserAccount.StartDateS = this._HelperService.GetDateS(
                        this._UserAccount.StartDate
                    );
                    this._UserAccount.EndDateS = this._HelperService.GetDateS(
                        this._UserAccount.EndDate
                    );
                    this._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(
                        this._UserAccount.CreateDate
                    );
                    this._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(
                        this._UserAccount.ModifyDate
                    );
                    this._UserAccount.StatusI = this._HelperService.GetStatusIcon(
                        this._UserAccount.StatusCode
                    );
                    this._UserAccount.StatusB = this._HelperService.GetStatusBadge(
                        this._UserAccount.StatusCode
                    );
                    this._UserAccount.StatusC = this._HelperService.GetStatusColor(
                        this._UserAccount.StatusCode
                    );


                    //#endregion

                    this._ChangeDetectorRef.detectChanges();
                }
                else {
                    this.toogleIsFormProcessing(false);
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    ScheduleDateRangeChange(value) {

        this.Form_AddUser.patchValue(
            {
                TStartDate: this._HelperService.DateInUTC(moment(value.start)).format('DD-MM-YYYY') + ' - ' + this._HelperService.DateInUTC(moment(value.end)).format('DD-MM-YYYY'),
            }
        );
        this.Form_AddUser.patchValue(
            {
                StartDate: value.start,
                EndDate: value.end,
            }
        );
    }

    SetCampaignType(TypeName) {
        this.Form_AddUser.patchValue(
            {
                TypeCode: TypeName,
            }
        );
    }

    Form_AddUser: FormGroup;
    Form_AddUser_Close() {
        this._Router.navigate([
            "console/" + this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns.Campaigns,
            this._HelperService.AppConfig.ActiveReferenceKey,
            this._HelperService.AppConfig.ActiveReferenceId,

        ]);
    }
    Form_AddUser_Load() {
        this.Form_AddUser = this._FormBuilder.group({
            OperationType: "new",
            Task: this._HelperService.AppConfig.Api.ThankUCash.SaveCampaign,
            UserAccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(50)])],
            Description: null,
            TypeCode: [null, Validators.compose([Validators.required])],
            SubTypeCode: [null, Validators.compose([Validators.required])],
            SubTypeValue: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(10)])],
            MinimumInvoiceAmount: 0,
            MaximumInvoiceAmount: 0,
            MinimumRewardAmount: 0,
            MaximumRewardAmount: [0, Validators.compose([Validators.required])],
            TStartDate: [null, Validators.compose([Validators.required])],
            StartDate: [null, Validators.compose([Validators.required])],
            EndDate: [null, Validators.compose([Validators.required])],
            StatusCode: this._HelperService.AppConfig.Status.Campaign.Creating,
        });
    }

    RewardTypeChange() {
        this.Form_AddUser.patchValue(
            {
                SubTypeValue: 0,
                MinimumInvoiceAmount: 0,
                MaximumInvoiceAmount: 0,
                MinimumRewardAmount: 0,
                MaximumRewardAmount: 0,
            }
        );
    }
    Form_AddUser_Clear() {
        this.Form_AddUser.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_AddUser_Process(_FormValue: any) {
        if (_FormValue.StartDate == undefined) {
            this._HelperService.NotifyError("Please select campaign Schedule");
        }
        else if (_FormValue.EndDate == undefined) {
            this._HelperService.NotifyError("Please select campaign Schedule");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_FixedAmount && _FormValue.SubTypeValue == undefined) {
            this._HelperService.NotifyError("Enter reward amount");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_FixedAmount && _FormValue.SubTypeValue < 1) {
            this._HelperService.NotifyError("Reward amount must be greater than 0");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_AmountPercentage && _FormValue.SubTypeValue == undefined) {
            this._HelperService.NotifyError("Enter reward percentage");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_AmountPercentage && _FormValue.SubTypeValue < 1) {
            this._HelperService.NotifyError("Reward percentage must be greater than 0");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_AmountPercentage && _FormValue.SubTypeValue > 100) {
            this._HelperService.NotifyError("Reward percentage must be between than 1 - 100");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_AmountPercentage && _FormValue.MaximumRewardAmount < 0) {
            this._HelperService.NotifyError("Enter valid minimum purchase amount. Set to 0 for no limit");
        }
        else if (_FormValue.SubTypeValue == undefined) {
            this._HelperService.NotifyError("Please select campaign Schedule");
        }
        else if (_FormValue.MinimumInvoiceAmount == undefined) {
            this._HelperService.NotifyError("Enter minimum purchase amount. Set to 0 for no limit");
        }
        else if (_FormValue.MinimumInvoiceAmount < 0) {
            this._HelperService.NotifyError("Enter valid minimum purchase amount. Set to 0 for no limit");
        }
        else {
            swal({
                title: "Create campaign ?",
                text: "Please make sure all data is correct. You can not edit campaign after publish",
                showCancelButton: true,
                position: this._HelperService.AppConfig.Alert_Position,
                animation: this._HelperService.AppConfig.Alert_AllowAnimation,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
                allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
            }).then(result => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    // _FormValue.StartDate = this._HelperService.DateInUTC(this.Form_AddUser.controls['StartDate'].value);
                    // _FormValue.EndDate = this._HelperService.DateInUTC(this.Form_AddUser.controls['EndDate'].value);
                    let _OResponse: Observable<OResponse>;

                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, _FormValue);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Campaign created successfully");
                                this._Router.navigate([
                                    this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AcquirerPanel.Campaigns.Campaigns,
                                    this._HelperService.AppConfig.ActiveReferenceKey,
                                    this._HelperService.AppConfig.ActiveReferenceId
                                ]);
                                this.Form_AddUser_Clear();

                                if (_FormValue.OperationType == "close") {
                                    this.Form_AddUser_Close();
                                }
                                else {
                                    this.UpdateCampaignStatus(_Response.Result);
                                }

                            } else {
                                this._HelperService.NotifyError(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });
        }



    }

    UpdateCampaignStatus(CReferenceKey: string) {

        var pData =
        {
            Task: "updatecampaign",
            ReferenceKey: CReferenceKey,
            StatusCode: this._HelperService.AppConfig.Status.Campaign.Published
        };
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess("Campaign published successfully");
                    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns.Campaigns]);
                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            }
        );

    }



}

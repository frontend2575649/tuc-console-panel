import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
declare var $: any;
import {
  DataHelperService,
  HelperService,
  OList,
  FilterHelperService,
} from "../../../service/service";
import swal from "sweetalert2";

@Component({
  selector: "tu-campaigns",
  templateUrl: "./campaigns.component.html",
})
export class TUCampaignsComponent implements OnInit {
  public ResetFilterControls: boolean = true;
  SaveEditValue: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "Campaign ID ",
      Value: true,
    },
    {
      Name: "Type ",
      Value: true,
    },

    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "Start",
      Value: true,
    },
    {
      Name: "End",
      Value: true,
    },
  ];
  EditColoum: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "Campaign ID ",
      Value: true,
    },
    {
      Name: "Type ",
      Value: true,
    },

    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "Start",
      Value: true,
    },
    {
      Name: "End",
      Value: true,
    },
  ];
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) { }
  ngOnInit() {
    Feather.replace();
    this._HelperService.ValidateData();
    this.UserAccounts_Setup();

    var temp = this._HelperService.GetStorage("BCampaignTable");
    if (temp != undefined && temp != null) {
      this.EditColoum = temp.config;
      this.SaveEditValue = JSON.parse(JSON.stringify(temp.config));
    }
  }

  SetOtherFilters(): void {
    this.UserAccounts_Config.SearchBaseConditions = [];
    this.UserAccounts_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(
      "",
      "UserAccountKey",
      this._HelperService.AppConfig.DataType.Text,
      this._HelperService.AppConfig.ActiveReferenceKey,
      "="
    );
  }

  EditCol() {
    this._HelperService.OpenModal("EditCol");
  }
  SaveEditCol() {
    this.EditColoum = JSON.parse(JSON.stringify(this.SaveEditValue));
    this._HelperService.SaveStorage("BCampaignTable", {
      config: this.EditColoum,
    });
    this._HelperService.CloseModal("EditCol");
  }

  Open_AddCampaign() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AcquirerPanel.Campaigns
        .AddCampaign,this._HelperService.AppConfig.ActiveReferenceKey,this._HelperService.AppConfig.ActiveReferenceId
    ]);
  }
  public UserAccounts_Config: OList;
  UserAccounts_Setup() {
    this.UserAccounts_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetCampaigns,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign,
      Title: "All Campaigns",
      StatusType: "Campaigns",
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict(
        "",
        "UserAccountKey",
        this._HelperService.AppConfig.DataType.Text,
        this._HelperService.AppConfig.ActiveReferenceKey,
        "="
      ),
      Type: this._HelperService.AppConfig.ListType.All,
      DefaultSortExpression: "CreateDate desc",
      TableFields: [
        {
          DisplayName: "Name",
          SystemName: "Name",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: "Type",
          SystemName: "TypeName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: "Reward Type",
          SystemName: "SubTypeName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: "Start",
          SystemName: "StartDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date",
          Show: true,
          Search: false,
          Sort: true,
          IsDateSearchField:true,
          ResourceId: null,
        },
        {
          DisplayName: "End",
          SystemName: "EndDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
      ],
    };

    this.UserAccounts_Config = this._DataHelperService.List_Initialize(
      this.UserAccounts_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Campaign,
      this.UserAccounts_Config
    );
    this.UserAccounts_GetData();
  }
  UserAccounts_ToggleOption(event: any, Type: any) {
    if (event != null) {
      for (let index = 0; index < this.UserAccounts_Config.Sort.SortOptions.length; index++) {
        const element = this.UserAccounts_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {

          element.SystemActive = true;

        }
        else {
          element.SystemActive = false;

        }

      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.UserAccounts_Config
    );
    this.UserAccounts_Config = this._DataHelperService.List_Operations(
      this.UserAccounts_Config,
      event,
      Type
    );
    if (
      (this.UserAccounts_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.UserAccounts_GetData();
    }
  }
  timeout = null;
  UserAccounts_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.UserAccounts_Config.Sort.SortOptions.length; index++) {
          const element = this.UserAccounts_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
  
            element.SystemActive = true;
  
          }
          else {
            element.SystemActive = false;
  
          }
  
        }
      }
  
      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.UserAccounts_Config
      );
      this.UserAccounts_Config = this._DataHelperService.List_Operations(
        this.UserAccounts_Config,
        event,
        Type
      );
      if (
        (this.UserAccounts_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.UserAccounts_GetData();
      } 
    }, this._HelperService.AppConfig.SearchInputDelay);

  }

  UserAccounts_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.UserAccounts_Config
    );
    this.UserAccounts_Config = TConfig;
  }
  UserAccounts_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveCampaign,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Campaign,
      }
    );

    this._HelperService.AppConfig.ActiveCampaignReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveCampaignReferenceId = ReferenceData.ReferenceId;

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AcquirerPanel.Campaigns
        .Sales,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetStoreConfig(this.UserAccounts_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.UserAccounts_GetData();
  }

  // Save_NewFilter() {
  //   swal({
  //     position: "center",
  //     title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
  //     text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
  //     input: "text",
  //     inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
  //     inputAttributes: {
  //       autocapitalize: "off",
  //       autocorrect: "off",
  //       //maxLength: "4",
  //       minLength: "4",
  //     },
  //     animation: false,
  //     customClass: this._HelperService.AppConfig.Alert_Animation,
  //     showCancelButton: true,
  //     confirmButtonColor: this._HelperService.AppConfig.Color_Green,
  //     cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
  //     confirmButtonText: "Save",
  //     cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
  //   }).then((result) => {
  //     if (result.value) {
  //       this._HelperService._RefreshUI = false;
  //       this._ChangeDetectorRef.detectChanges();

  //       this._FilterHelperService._BuildFilterName_Store(result.value);
  //       this._HelperService.Save_NewFilter(
  //         this._HelperService.AppConfig.FilterTypeOption.Campaign
  //       );

  //       this._HelperService._RefreshUI = true;
  //       this._ChangeDetectorRef.detectChanges();
  //     }
  //   });
  // }

  Save_NewFilter() {
    swal({
        position: "center",
        title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
        text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
        // input: "text",
        html:
            '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
            '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
            '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
        focusConfirm: false,
        preConfirm: () => {
            return {
                filter: document.getElementById('swal-input1')['value'],
                private: document.getElementById('swal-input2')['checked']
            }
        },
        // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
        // inputAttributes: {
        //   autocapitalize: "off",
        //   autocorrect: "off",
        //   maxLength: "4",
        //   minLength: "4",
        // },
        animation: false,
        customClass: this._HelperService.AppConfig.Alert_Animation,
        showCancelButton: true,
        confirmButtonColor: this._HelperService.AppConfig.Color_Green,
        cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
        confirmButtonText: "Save",
        cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
        if (result.value) {

            if (result.value.filter.length < 5) {
                this._HelperService.NotifyError('Enter filter name length greater than 4');
                return;
            }

            this._HelperService._RefreshUI = false;
            this._ChangeDetectorRef.detectChanges();

            this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

            var AccessType: number = result.value.private ? 0 : 1;
            this._HelperService.Save_NewFilter(
                this._HelperService.AppConfig.FilterTypeOption.Acquirer,
                AccessType
            );

            this._HelperService._RefreshUI = true;
            this._ChangeDetectorRef.detectChanges();
        }
    });
}

  Delete_Filter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Campaign
        );
        this._FilterHelperService.SetStoreConfig(this.UserAccounts_Config);
        this.UserAccounts_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.UserAccounts_GetData();

    if (ButtonType == 'Sort') {
      $("#UserAccounts_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#UserAccounts_fdropdown").dropdown('toggle');
    }

  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetStoreConfig(this.UserAccounts_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.UserAccounts_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetStoreConfig(this.UserAccounts_Config);

    //#region setOtherFilters

    this.SetOtherFilters();

    //#endregion

    this.UserAccounts_GetData();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();

  }
}

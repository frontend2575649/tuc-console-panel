import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { ImageCropperModule } from 'ngx-image-cropper';
import { SubadminsComponent } from './subadmins.component';
import { MainPipe } from '../../../service/main-pipe.module'
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { DynamicRoutesguardGuard } from 'src/app/service/guard/dynamicroutes.guard';

import { Ng5SliderModule } from 'ng5-slider';
const routes: Routes = [
  { path: '', canActivateChild: [DynamicRoutesguardGuard], data: { accessName: ['viewacqsubaccount', 'acqsubaccount'] }, component: SubadminsComponent }
];
const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 1,
};

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubAdminsRoutingModule { }


@NgModule({
  declarations: [SubadminsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    SubAdminsRoutingModule,
    ImageCropperModule,
    MainPipe,
    InputFileModule.forRoot(config),
    Ng5SliderModule,
  ]
})
export class SubadminsModule { }

import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import {
  OSelect,
  OList,
  DataHelperService,
  HelperService,
  OResponse,
  OStorageContent,
  OCoreParameter,
  OCoreCommon,
  FilterHelperService,
} from "../../../service/service";
import swal from "sweetalert2";

declare var $: any;

@Component({
  selector: "app-branch",
  templateUrl: "./branch.component.html",
})
export class BranchComponent implements OnInit {
  Show: any = {};
  public ResetFilterControls: boolean = true;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this.Show.ListShow = "none";
    this.Show.IconShow = "block";
  }
  ngOnInit() {
    Feather.replace();
    this._HelperService.ValidateData();
    this.InitColConfig();
    this.BranchList_Setup();

    // this.BranchList_Filter_Owners_Load();
  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AcquirerPanel.Branches
        .AddBranch,this._HelperService.AppConfig.ActiveReferenceKey,this._HelperService.AppConfig.ActiveReferenceId,
    ]);
  }

  //#region ColumnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "BranchDetails",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "Manager",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];
  ColoumConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "BranchDetails",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "Manager",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var temp = this._HelperService.GetStorage("Bbranchtable");

    if (temp != undefined && temp != null) {
      this.ColoumConfig = temp.config;
      this.TempColumnConfig = JSON.parse(JSON.stringify(temp.config));
    }
  }
  EditColConfig() {
    this._HelperService.OpenModal("EditCol");
  }
  SaveColConfig() {
    this.ColoumConfig = JSON.parse(JSON.stringify(this.TempColumnConfig));
    this._HelperService.SaveStorage("Bbranchtable", {
      config: this.ColoumConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  SetOtherFilters(): void {
    this.BranchList_Config.SearchBaseConditions = [];
    this.BranchList_Config.SearchBaseCondition = null;
  }

  //#region Branches

  public BranchList_Config: OList;
  BranchList_Setup() {
    this.BranchList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.Core.GetBranches,
      Location: this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Branch,
      Title: "All Branches",
      StatusType: "default",
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      // DefaultSortExpression: 'CreateDate desc',
      TableFields: [
        {
          DisplayName: "Merchant Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard,
        },
        {
          DisplayName: "City Name",
          SystemName: "ManagerName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard,
        },

        {
          DisplayName: "Contact No",
          SystemName: "PhoneNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard,
        },
        {
          DisplayName: "Branch id",
          SystemName: "BranchCode",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard,
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard,
        },
        {
          DisplayName: "Stores",
          SystemName: "Stores",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "text-right",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard,
        },
        {
          DisplayName: "Terminals",
          SystemName: "Terminals",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard,
        },
        {
          DisplayName: "Active Terminals",
          SystemName: "ActiveTerminals",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard,
        },
      ],
    };
    this.BranchList_Config = this._DataHelperService.List_Initialize(
      this.BranchList_Config
    );
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Branch,
      this.BranchList_Config
    );
    this.BranchList_GetData();
  }

  BranchList_ToggleOption(event: any, Type: any) {
    if (event != null) {
      for (let index = 0; index < this.BranchList_Config.Sort.SortOptions.length; index++) {
        const element = this.BranchList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {

          element.SystemActive = true;

        }
        else {
          element.SystemActive = false;

        }

      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.BranchList_Config
    );
    this.BranchList_Config = this._DataHelperService.List_Operations(
      this.BranchList_Config,
      event,
      Type
    );
    if (
      (this.BranchList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.BranchList_GetData();
    }
  }
  BranchList_GetData(Type?: any) {
    if (
      Type != null &&
      Type == this._HelperService.AppConfig.ListToggleOption.Search
    ) {
      var TConfig = this._DataHelperService.List_GetData(
        this.BranchList_Config
      );
    } else {
      var TConfig = this._DataHelperService.List_GetData(
        this.BranchList_Config,
        this.Show
      );
    }

    this.BranchList_Config = TConfig;
  }
  timeout = null;
  BranchList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
     
      if (event != null) {
        for (let index = 0; index < this.BranchList_Config.Sort.SortOptions.length; index++) {
          const element = this.BranchList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
  
            element.SystemActive = true;
  
          }
          else {
            element.SystemActive = false;
  
          }
  
        }
      }
  
      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.BranchList_Config
      );
      this.BranchList_Config = this._DataHelperService.List_Operations(
        this.BranchList_Config,
        event,
        Type
      );
      if (
        (this.BranchList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.BranchList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);

  }

  BranchList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveBranch,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Branch,
      }
    );

    this._HelperService.AppConfig.ActiveBranchReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveBranchReferenceId = ReferenceData.ReferenceId;
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AcquirerPanel.Branches
        .BranchTerminals,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);
  }

  //#endregion

  //#region Owners

  public BranchList_Filter_Owners_Option: Select2Options;
  public BranchList_Filter_Owners_Selected = 0;
  BranchList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray(
      "",
      "AccountTypeCode",
      this._HelperService.AppConfig.DataType.Text,
      [
        this._HelperService.AppConfig.AccountType.Merchant,
        this._HelperService.AppConfig.AccountType.Acquirer,
        this._HelperService.AppConfig.AccountType.PGAccount,
        this._HelperService.AppConfig.AccountType.PosAccount,
      ],
      "="
    );
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.BranchList_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  BranchList_Filter_Owners_Change(event: any) {
    if (event.value == this.BranchList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "OwnerId",
        this._HelperService.AppConfig.DataType.Number,
        this.BranchList_Filter_Owners_Selected,
        "="
      );
      this.BranchList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.BranchList_Config.SearchBaseConditions
      );
      this.BranchList_Filter_Owners_Selected = 0;
    } else if (event.value != this.BranchList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "OwnerId",
        this._HelperService.AppConfig.DataType.Number,
        this.BranchList_Filter_Owners_Selected,
        "="
      );
      this.BranchList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.BranchList_Config.SearchBaseConditions
      );
      this.BranchList_Filter_Owners_Selected = event.value;
      this.BranchList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "OwnerId",
          this._HelperService.AppConfig.DataType.Number,
          this.BranchList_Filter_Owners_Selected,
          "="
        )
      );
    }
    this.BranchList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetStoreConfig(this.BranchList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.BranchList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Store(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Branch
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Branch
        );
        this._FilterHelperService.SetStoreConfig(this.BranchList_Config);
        this.BranchList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.BranchList_GetData();
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();

    if (ButtonType == 'Sort') {
      $("#BranchList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#BranchList_fdropdown").dropdown('toggle');
    }

  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetStoreConfig(this.BranchList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.BranchList_GetData();
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetStoreConfig(this.BranchList_Config);

    //#region setOtherFilters

    this.SetOtherFilters();

    //#endregion

    this.BranchList_GetData();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.BranchList_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();

  }
}

import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { BranchComponent } from './branch.component';
const routes: Routes = [{ path: "",canActivate:[DynamicRoutesguardGuard],data:{accessName:['viewbranches']}, component: BranchComponent }];
import { MainPipe } from '../../../service/main-pipe.module'
import { DynamicRoutesguardGuard } from "src/app/service/guard/dynamicroutes.guard";

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUBranchRoutingModule {}

@NgModule({
  declarations: [BranchComponent],
  imports: [
    CommonModule,
    MainPipe,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TUBranchRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule
  ]
})
export class BranchModule { }

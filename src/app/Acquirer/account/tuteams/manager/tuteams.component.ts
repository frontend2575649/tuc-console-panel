import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from 'feather-icons';
import { Observable } from "rxjs";
import { DataHelperService, FilterHelperService, HelperService, OResponse, OSelect } from "../../../../service/service";
declare var moment: any;
import * as cloneDeep from 'lodash/cloneDeep';
declare var $: any;
import swal from "sweetalert2";

@Component({
    selector: "tu-teams",
    templateUrl: "./tuteams.component.html"
})
export class TUTeamsComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _FilterHelperService: FilterHelperService


    ) { }
    ngOnInit() {
        Feather.replace();
        this._HelperService.StopClickPropogation();

        this.initializeDatePicker('monthlydate', this._HelperService.AppConfig.DatePickerTypes.month, 'MonthSalesTrend_dropdown');

        this._ActivatedRoute.params.subscribe((params: Params) => {
            // this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            // this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
                this.GetRMs_List();
            } else {
                this.GetRMs_List();
            }
        });


        //#region FiltersInit 

        this.GetMangers_List();
        this.GetBranches_List();
        // this.RMsList_Filter_Owners_Load();

        //#endregion
        this.Form_AddUser_Load();
        this.UserAccounts_Filter_Owners_Load();

        this.Form_AssignTarget_Load();
        this.RMsList_Setup();
        this.GetManagerDetails();

    }

    initializeDatePicker(pickerId: string, type: string, dropdownId: string): void {
        var i = '#' + pickerId;

        if (type == this._HelperService.AppConfig.DatePickerTypes.month) {
            $(i).datepicker(
                {
                    setDate: new Date(),
                    viewMode: "months",
                    minViewMode: "months",
                    format: "dd/mm/yyyy",
                    startDate: '-0m'

                }
            ).on('changeDate', function (ev) {
                $('#sDate1').text($('#datepicker').data('date'));
                $('#datepicker').datepicker('hide');
            });;
        } else {
            $(i).datepicker();
        }

        $(i).on('changeDate', () => {
            this.ScheduleDateRangeChange({
                start: moment($(i).datepicker("getDate")),
                end: moment($(i).datepicker("getDate"))
            });
            $(i).datepicker('hide');
        });

    }


    //#region RMs 

    public RMsList_Config: OList = null;
    RMsList_Setup() {
        this.RMsList_Config = {
            Id: null,
            Task: this._HelperService.AppConfig.Api.Core.GetManagers,
            Location: this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Branch,
            Title: "Available Managers",
            StatusType: "default",
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            Filters: [],
            // SubReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            // SubReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            Type: this._HelperService.AppConfig.ListType.RM,
            PageRecordLimit: 50,
            Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
            TableFields: [
                {
                    DisplayName: 'Name',
                    SystemName: 'Name',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                },
                {
                    DisplayName: 'BranchName',
                    SystemName: 'BranchName',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: false,
                },
                {
                    DisplayName: 'Contact No',
                    SystemName: 'MobileNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Email Address',
                    SystemName: 'EmailAddress',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: false,
                    Search: true,
                    Sort: false,
                },
                {
                    DisplayName: 'Stores',
                    SystemName: 'Stores',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                },
                {
                    DisplayName: 'POS',
                    SystemName: 'Terminals',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                },
                {
                    DisplayName: 'Owner',
                    SystemName: 'OwnerDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                    DefaultValue: 'ThankUCash'
                },
                {
                    DisplayName: 'Last Tr',
                    SystemName: 'LastTransactionDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Last Login',
                    SystemName: 'LastLoginDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: false,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: 'CreateDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                },
            ]
        };
        // this.RMsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '==');
        this.RMsList_Config = this._DataHelperService.List_Initialize(this.RMsList_Config);
        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.Merchant,
            this.RMsList_Config
        );
        //this.RMsList_GetData();
    }
    RMsList_ToggleOption(event: any, Type: any) {
        if (event != null) {
            for (let index = 0; index < this.RMsList_Config.Sort.SortOptions.length; index++) {
                const element = this.RMsList_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }
            }
        }

        this._HelperService.Update_CurrentFilterSnap(
            event,
            Type,
            this.RMsList_Config


        );

        this.RMsList_Config = this._DataHelperService.List_Operations(
            this.RMsList_Config,
            event,
            Type
        );

        if (
            (this.RMsList_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.RMsList_GetData();
        }

    }

    timeout = null;
    RMsList_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {
            if (event != null) {
                for (let index = 0; index < this.RMsList_Config.Sort.SortOptions.length; index++) {
                    const element = this.RMsList_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {

                        element.SystemActive = true;

                    }
                    else {
                        element.SystemActive = false;

                    }
                }

            }

            this.RMsList_Config = this._DataHelperService.List_Operations(this.RMsList_Config, event, Type);
            if (this.RMsList_Config.RefreshData == true) {

                this.RMsList_GetData();
            }
            this._HelperService.StopClickPropogation();

        }, this._HelperService.AppConfig.SearchInputDelay);

    }
    RMsList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.RMsList_Config);
        this.RMsList_Config = TConfig;
    }
    RMsList_ListTypeChange(Type) {
        if (Type == 1) {
            this.RMsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '==');
        }
        else {
            this.RMsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 1, '!=');
            this.RMsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '!=');
        }
        this.RMsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }
    RMsList_RowSelected(ReferenceData) {

        // var Details =
        // {
        //     ReferenceId: ReferenceData.ReferenceId,
        //     ReferenceKey: ReferenceData.ReferenceKey,
        //     DisplayName: ReferenceData.DisplayName,
        // };

        // this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.ActiveManager, {
        //     "ReferenceKey": ReferenceData.ReferenceKey,
        //     "ReferenceId": ReferenceData.ReferenceId,
        //     "DisplayName": ReferenceData.DisplayName,
        //     "AccountTypeCode": this._HelperService.AppConfig.AccountType.Manager
        // });

        // this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.ActiveMerchant, Details);
        // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Manager.Terminals, ReferenceData.ReferenceKey, ReferenceData.ReferenceId, ReferenceData.RoleId]);

    }

    //#endregion

    //#region AddUser 

    Form_AddUser: FormGroup;
    Form_AddUser_Show() {
        this._HelperService.OpenModal("Form_AddUser_Content");
    }
    Form_AddUser_Close() {
        // this._Router.navigate([
        //     this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.SubAccounts
        // ]);
        this._HelperService.OpenModal("Form_AddUser_Content");
    }
    Form_AddUser_Load() {


        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_AddUser = this._FormBuilder.group({
            OperationType: "new",
            Task: this._HelperService.AppConfig.Api.Core.SaveManager,
            // OwnerKey: this._HelperService.AppConfig.ActiveReferenceKey,
            // OwnerId: this._HelperService.AppConfig.ActiveReferenceId,

            OwnerKey: this._ManagerDetails.ReferenceKey,
            OwnerId: this._ManagerDetails.ReferenceId,


            RoleKey: 'rm',
            RoleId: '8',
            BranchKey: this._HelperService.ManagerBranchKey,
            BranchId: this._HelperService.ManagerBranchId,
            Name: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(128)
                ])
            ],
            MobileNumber: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(8),
                    Validators.maxLength(14)
                ])
            ],
            EmailAddress: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.email,
                    Validators.minLength(2)
                ])
            ],
            StatusCode: this._HelperService.AppConfig.Status.Active,

        });
    }
    Form_AddUser_Clear() {
        this.Form_AddUser.reset();
        this.Form_AddUser_Load();
    }

    Form_AddUser_Process(_FormValue: any) {
        // _FormValue.DisplayName = _FormValue.FirstName;
        // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            _FormValue
        );
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess("Account created successfully");
                    this.Form_AddUser_Clear();
                    this.RMsList_GetData();
                    if (_FormValue.OperationType == "close") {
                        this.Form_AddUser_Close();
                    }
                    this._HelperService.CloseModal('Form_AddUser_Content');
                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            }
        );
    }

    //#endregion

    public _ShowRole: boolean = true;
    public _ShowReportingM: boolean = true;

    //#region Owners 

    public RMsList_Filter_Owners_Option: Select2Options;
    public RMsList_Filter_Owners_Selected = 0;
    RMsList_Filter_Owners_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
            ]
        };
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
            [
                this._HelperService.AppConfig.AccountType.Merchant,
                this._HelperService.AppConfig.AccountType.Acquirer,
                this._HelperService.AppConfig.AccountType.PGAccount,
                this._HelperService.AppConfig.AccountType.PosAccount
            ]
            , '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.RMsList_Filter_Owners_Option = {
            placeholder: 'Sort by Referrer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    RMsList_Filter_Owners_Change(event: any) {
        if (event.value == this.RMsList_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.RMsList_Filter_Owners_Selected, '=');
            this.RMsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.RMsList_Config.SearchBaseConditions);
            this.RMsList_Filter_Owners_Selected = 0;
        }
        else if (event.value != this.RMsList_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.RMsList_Filter_Owners_Selected, '=');
            this.RMsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.RMsList_Config.SearchBaseConditions);
            this.RMsList_Filter_Owners_Selected = event.value;
            this.RMsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.RMsList_Filter_Owners_Selected, '='));
        }
        this.RMsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region Roles 

    public GetRoles_Option: Select2Options;
    public GetRoles_Transport: any;
    GetRoles_ListChange(event: any) {

        this.Form_AddUser.controls['OwnerId'].setValue(null);
        this.Form_AddUser.controls['OwnerKey'].setValue(null);

        this.Form_AddUser.patchValue(
            {
                RoleId: event.value,
                RoleKey: event.data[0].apival
            }
        );
        this._ShowRole = false;
        this._ChangeDetectorRef.detectChanges();

        if (event.value == 8) { //RM
            this._ShowReportingM = false;
            this._ChangeDetectorRef.detectChanges();
            this._MangerTypeId = 6; //Manager

            this.GetMangers_List();
            this._ShowReportingM = true;
            this._ChangeDetectorRef.detectChanges();
        } else if (event.value == 7) { //SubAccount

            this._ShowReportingM = false;

            this.Form_AddUser.controls['OwnerId'].setValue(this._HelperService.UserAccount.AccountId);
            this.Form_AddUser.controls['OwnerKey'].setValue(this._HelperService.UserAccount.AccountKey);


            this._MangerTypeId = undefined;
        }
        else if (event.value == 6) { //Manager
            this._ShowReportingM = false;
            this._ChangeDetectorRef.detectChanges();
            this._MangerTypeId = 7; //Subaccount
            this.GetMangers_List();
            this._ShowReportingM = true;
            this._ChangeDetectorRef.detectChanges();

        } else {
            this._MangerTypeId = undefined;
            this._ShowReportingM = true;
            this.GetMangers_List();
        }

        this._ShowRole = true;
        this._ChangeDetectorRef.detectChanges();

        this.GetMangers_List();
    }

    //#endregion

    //#region Managers 

    public _MangerTypeId: number;
    public GetMangers_Option: Select2Options;
    public GetMangers_Transport: any;
    GetMangers_List() {
        var PlaceHolder = "Select Manager";
        var _Select: OSelect

        if (this._MangerTypeId == 7) {
            _Select =
            {
                Task: this._HelperService.AppConfig.Api.Core.GetSubAccounts,
                Location: this._HelperService.AppConfig.NetworkLocation.V3.SubAccounts,
                ReferenceKey: this._HelperService.UserAccount.AccountKey,
                ReferenceId: this._HelperService.UserAccount.AccountId,
                SortCondition: [],
                Fields: [
                    {
                        SystemName: "ReferenceId",
                        Type: this._HelperService.AppConfig.DataType.Number,
                        Id: true,
                        Text: false,
                    },
                    {
                        SystemName: "Name",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        Id: false,
                        Text: true
                    },
                ]
            }
        } else {
            _Select =
            {
                Task: this._HelperService.AppConfig.Api.Core.GetManagers,
                Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
                ReferenceKey: this._HelperService.UserAccount.AccountKey,
                ReferenceId: this._HelperService.UserAccount.AccountId,
                SortCondition: [],
                Fields: [
                    {
                        SystemName: "ReferenceId",
                        Type: this._HelperService.AppConfig.DataType.Number,
                        Id: true,
                        Text: false,
                    },
                    {
                        SystemName: "Name",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        Id: false,
                        Text: true
                    },
                ]
            }

        }


        if (this._MangerTypeId != undefined && this._MangerTypeId != 7) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'RoleId', this._HelperService.AppConfig.DataType.Number, this._MangerTypeId, '=')
        }

        this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetMangers_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetMangers_Transport,
            multiple: false,
        };
    }
    GetMangers_ListChange(event: any) {
        // alert(event);

        this.Form_AddUser.patchValue(
            {
                OwnerId: event.data[0].ReferenceId,
                OwnerKey: event.data[0].ReferenceKey,
            }


        );

    }

    //#endregion

    //#region Branches 

    public GetBranches_Option: Select2Options;
    public GetBranches_Transport: any;
    GetBranches_List() {
        var PlaceHolder = "Select Branch";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetBranches,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            // SearchCondition: "",
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: 'StatusCode',
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: '=',
                //     SearchValue: this._HelperService.AppConfig.Status.Active,
                // }
            ]
        }



        this.GetBranches_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetBranches_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetBranches_Transport,
            multiple: false,
        };
    }
    GetBranches_ListChange(event: any) {
        // alert(event);\
        this.Form_AddUser.patchValue(
            {
                BranchKey: event.data[0].ReferenceKey,
                BranchId: event.data[0].ReferenceId
            }
        );

    }

    //#endregion

    public UserAccounts_Filter_Owners_Option: Select2Options;
    public UserAccounts_Filter_Owners_Selected = 0;
    UserAccounts_Filter_Owners_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetBranches,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
            ]
        };
        // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
        //     [
        //         this._HelperService.AppConfig.AccountType.Merchant,
        //         this._HelperService.AppConfig.AccountType.Acquirer,
        //         this._HelperService.AppConfig.AccountType.PGAccount,
        //         this._HelperService.AppConfig.AccountType.PosAccount
        //     ]
        //     , '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.UserAccounts_Filter_Owners_Option = {
            placeholder: 'Sort by Referrer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    UserAccounts_Filter_Owners_Change(event: any) {

        // this._HelperService.Update_CurrentFilterSnap(
        //     event,
        //     this._HelperService.AppConfig.ListToggleOption.Other,
        //     this.RMsList_Config,
        //     this._HelperService.AppConfig.OtherFilters.Manager.Owner
        // );

        this.OwnerEventProcessing(event);
    }

    OwnerEventProcessing(event: any): void {
        if (event.value == this.UserAccounts_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'BranchId', this._HelperService.AppConfig.DataType.Number, this.UserAccounts_Filter_Owners_Selected, '=');
            this.RMsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.RMsList_Config.SearchBaseConditions);
            this.UserAccounts_Filter_Owners_Selected = 0;
        }
        else if (event.value != this.UserAccounts_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'BranchId', this._HelperService.AppConfig.DataType.Number, this.UserAccounts_Filter_Owners_Selected, '=');
            this.RMsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.RMsList_Config.SearchBaseConditions);
            this.UserAccounts_Filter_Owners_Selected = event.value;
            this.RMsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'BranchId', this._HelperService.AppConfig.DataType.Number, this.UserAccounts_Filter_Owners_Selected, '='));
        }

    }

    public ResetFilterControls: boolean = true;

    public _ManagerDetails: any =
        {
            RoleId: null,
            BranchKey: null,
            BranchId: null,
            BranchName: null,
            RoleKey: null,
            RoleName: null,
            ReferenceId: null,
            ReferenceKey: null,
            TypeCode: null,
            TypeName: null,
            SubTypeCode: null,
            SubTypeName: null,
            UserAccountKey: null,
            UserAccountDisplayName: null,
            Name: null,
            Description: null,
            StartDate: null,
            StartDateS: null,
            EndDate: null,
            EndDateS: null,
            SubTypeValue: null,
            MinimumInvoiceAmount: null,
            MaximumInvoiceAmount: null,
            MinimumRewardAmount: null,
            MaximumRewardAmount: null,
            ManagerKey: null,
            ManagerDisplayName: null,
            SmsText: null,
            Comment: null,
            CreateDate: null,
            CreatedByKey: null,
            CreatedByDisplayName: null,
            ModifyDate: null,
            ModifyByKey: null,
            ModifyByDisplayName: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
            CreateDateS: null,
            ModifyDateS: null,
            StatusI: null,
            StatusB: null,
            StatusC: null,
        }
    GetManagerDetails() {
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.GetManger,
            ReferenceKey: this._HelperService.AppConfig.ActiveManagerReferenceKey,
            ReferenceId: this._HelperService.AppConfig.ActiveManagerReferenceId
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Branch, pData);
        _OResponse.subscribe(

            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {

                    this._HelperService.IsFormProcessing = false;
                    this._ManagerDetails = _Response.Result;
                    this.Form_AddUser_Load()
                    this.GetBranches_Option.placeholder = this._ManagerDetails.BranchName;
                    this.GetMangers_Option.placeholder = this._ManagerDetails.OwnerDisplayName;
                    this._HelperService.ManagerBranchKey = this._ManagerDetails.BranchKey;
                    this._HelperService.ManagerBranchId = this._ManagerDetails.BranchId;

                    this._ManagerDetails.StartDateS = this._HelperService.GetDateS(
                        this._ManagerDetails.StartDate
                    );
                    this._ManagerDetails.EndDateS = this._HelperService.GetDateS(
                        this._ManagerDetails.EndDate
                    );
                    this._ManagerDetails.CreateDateS = this._HelperService.GetDateTimeS(
                        this._ManagerDetails.CreateDate
                    );
                    this._ManagerDetails.ModifyDateS = this._HelperService.GetDateTimeS(
                        this._ManagerDetails.ModifyDate
                    );
                    this._ManagerDetails.StatusI = this._HelperService.GetStatusIcon(
                        this._ManagerDetails.StatusCode
                    );
                    this._ManagerDetails.StatusB = this._HelperService.GetStatusBadge(
                        this._ManagerDetails.StatusCode
                    );
                    this._ManagerDetails.StatusC = this._HelperService.GetStatusColor(
                        this._ManagerDetails.StatusCode
                    );


                    //#region Load List After we have OwnerId 

                    this.RMsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._ManagerDetails.AccountId
                        , '==');
                    this.RMsList_GetData();

                    //#endregion

                }
                else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    //#region Target Assignments 

    ShowAssignTargetModal() {
        this._HelperService.OpenModal('AssignTarget');
    }

    Form_AssignTarget: FormGroup;
    Form_AssignTarget_Show() {
        this._HelperService.OpenModal("AssignTarget");
    }
    Form_AssignTarget_Close() {
        this._HelperService.CloseModal("AssignTarget");
    }
    Form_AssignTarget_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_AssignTarget = this._FormBuilder.group({

            //   ManagerKey: [this._ManagerDetails['OwnerKey'], Validators.required],
            //   ManagerId: [this._ManagerDetails['OwnerId'], Validators.required],

            // BranchKey: [this._ManagerDetails['BranchKey'], Validators.required],
            // BranchId: [this._ManagerDetails['BranchId'], Validators.required],

            StartDate: [null, Validators.compose([Validators.required])],
            EndDate: [null, Validators.compose([Validators.required])],


            TStartDate: [null],

            TotalTarget: [null, Validators.compose([
                Validators.required,
                Validators.min(1)
            ])
            ]

        });
    }
    Form_AssignTarget_Clear() {

        this.GetRM_ResetSelector();

        this.Form_AssignTarget.reset();
        this.Form_AssignTarget_Load();
    }

    Form_AssignTarget_Process(_FormValue: any) {

        _FormValue.TStartDate = undefined;

        var pData = {
            OperationType: "new",
            Task: this._HelperService.AppConfig.Api.ThankUCash.SaveRmTarget,
            Targets: []
        };

        for (let index = 0; index < this.RMsList_Config.Data.length; index++) {
            const element = this.RMsList_Config.Data[index];
            if (element.Value) {
                var newRMTarget = cloneDeep(_FormValue);

                newRMTarget.ManagerKey = element.OwnerKey;
                newRMTarget.ManagerId = element.OwnerId;

                newRMTarget.RmId = element.AccountId;
                newRMTarget.RmKey = element.AccountKey;

                newRMTarget.BranchKey = element.BranchKey;
                newRMTarget.BranchId = element.BranchId;

                pData.Targets.push(newRMTarget);
            }
        }

        if (pData.Targets.length == 0) {
            this._HelperService.NotifyError('Please select atleast one RM');
            return;
        }

        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            pData
        );
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess("Target Assigned successfully to selected users");
                    this.Form_AssignTarget_Clear();
                    this.Form_AssignTarget_Close();
                    this.Form_AssignTarget_Load();

                    if (_FormValue.OperationType == "close") {
                        this.Form_AssignTarget_Close();
                    }
                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            }
        );
    }

    ScheduleDateRangeChange(value): void {

        this.Form_AssignTarget.patchValue(
            {
                TStartDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
            }
        );

        this.Form_AssignTarget.patchValue(
            {
                StartDate: value.start,
                EndDate: value.end,
            }
        );
    }


    //#region RM selection 

    _ShowRMSelector: boolean = true;
    RMs = [];
    public GetRMs_Option: Select2Options;
    public GetRMs_Transport: any;
    GetRMs_List() {
        var PlaceHolder = "Select Manager";
        var _Select: OSelect

        if (this._MangerTypeId == 7) {
            _Select =
            {
                Task: this._HelperService.AppConfig.Api.Core.GetSubAccounts,
                Location: this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.SubAccounts,
                ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
                ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
                SortCondition: [],
                SearchCondition: this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveAccountId
                    , '=='),
                Fields: [
                    {
                        SystemName: "ReferenceId",
                        Type: this._HelperService.AppConfig.DataType.Number,
                        Id: true,
                        Text: false,
                    },
                    {
                        SystemName: "Name",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        Id: false,
                        Text: true
                    },
                ]
            }
        } else {
            _Select =
            {
                Task: this._HelperService.AppConfig.Api.Core.GetManagers,
                Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
                ReferenceKey: this._HelperService.UserAccount.AccountKey,
                ReferenceId: this._HelperService.UserAccount.AccountId,
                SortCondition: [],
                SearchCondition: this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveAccountId
                    , '=='),
                Fields: [
                    {
                        SystemName: "ReferenceId",
                        Type: this._HelperService.AppConfig.DataType.Number,
                        Id: true,
                        Text: false,
                    },
                    {
                        SystemName: "Name",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        Id: false,
                        Text: true
                    },
                ]
            }

        }


        if (this._MangerTypeId != undefined && this._MangerTypeId != 7) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'RoleId', this._HelperService.AppConfig.DataType.Number, this._MangerTypeId, '=')
        }

        this.GetRMs_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetRMs_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetRMs_Transport,
            multiple: false,
        };
    }
    GetRM_ListChange(event: any) {
        if (event.data[0]) {
            this.RMs[0] = {
                RmId: event.data[0].ReferenceId,
                RmKey: event.data[0].ReferenceKey
            };
        } else {
            this.RMs[0] = undefined;
        }
    }
    GetRM_ResetSelector(): void {
        this._ShowRMSelector = false;
        this._ChangeDetectorRef.detectChanges();
        setTimeout(() => {
            this._ShowRMSelector = true;
            this._ChangeDetectorRef.detectChanges();
        }, 100);
    }

    //#region Maintain RMs Selected List 

    IsAll: boolean = false;
    _ToogleSelectAll(event): void {
        this.RMsList_Config.Data.forEach(element => {
            element.Value = this.IsAll;
        });
    }

    SelectRMOption(): void {

    }

    //#endregion

    //#endregion

    //#endregion

    SetOtherFilters(): void {
        this.RMsList_Config.SearchBaseConditions = [];
        this.RMsList_Config.SearchBaseCondition = null;

        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
        if (CurrentIndex != -1) {
            //   this.MerchantsList_Filter_Owners_Selected = null;
            this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }

    //#region filterOperations

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetMerchantConfig(this.RMsList_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion

        this.RMsList_GetData();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_Merchant(Type, index);
        this._FilterHelperService.SetMerchantConfig(this.RMsList_Config);

        this.SetOtherFilters();

        this.RMsList_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            input: "text",
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            inputAttributes: {
                autocapitalize: "off",
                autocorrect: "off",
                //maxLength: "4",
                minLength: "4",
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_Merchant(result.value);
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.Merchant
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._HelperService.Delete_Filter(
                    this._HelperService.AppConfig.FilterTypeOption.Merchant
                );
                this._FilterHelperService.SetMerchantConfig(this.RMsList_Config);
                this.RMsList_GetData();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });

    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        this._HelperService.MakeFilterSnapPermanent();
        this.RMsList_GetData();

        if (ButtonType == 'Sort') {
            $("#MerchantsList_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#MerchantsList_fdropdown").dropdown('toggle');
        }

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetMerchantConfig(this.RMsList_Config);
        this.SetOtherFilters();

        this.RMsList_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    //#endregion

    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();
        // this.MerchantsList_Filter_Owners_Load();
        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();
    }



}

export class OList {
    public Id: string;
    public ListType?: number = 0;
    public Task: string;
    public Location: string;
    public Title: string = "List";

    public TableFields: OListField[];
    public VisibleHeaders?: any[];

    public ActivePage?: number = 1;
    public PageRecordLimit?: number = 10;
    public TotalRecords?: number = 0;
    public ShowingStart?: number = 0;
    public ShowingEnd?: number = 0;


    public SearchBaseCondition?: string;
    public SearchBaseConditions?: any[];
    public SearchParameter?: string;
    public SearchCondition?: string;
    public Filters?: OSearchFilter[];

    public Data?: any[];

    public StatusType?: string = "default";
    public Status?: number = 0;
    public StatusOptions?: any[];

    public Sort: OListSort;

    public RefreshData?: boolean = false;
    public IsDownload?: boolean = false;

    public ReferenceId?: number = null;
    public BranchId?: number = null;
    public ReferenceKey?: string = null;

    public SubReferenceId?: number = null;
    public BranchKey?: string = null;
    public Type?: string = null;
    public RefreshCount?: boolean = true;
    public TitleResourceId?: string = null;
    public StartTime?: any = null;
    public EndTime?: any = null;
    public StartDate?: any = null;
    public EndDate?: any = null;
}
export class OSearchFilter {
    public Title: string;
    public FildSystemName: string;
}
export class OListSort {
    public SortDefaultName: string;
    public SortDefaultColumn: string;
    public SortDefaultOrder?: string = "desc";
    public SortName?: string;
    public SortOrder?: string = "desc";
    public SortColumn?: string;
    public SortOptions?: any[];
}
export class OListField {
    public DefaultValue?: string = "--";
    public DisplayName: string;
    public DownloadDisplayName?: string;
    public SystemName: string;
    public Content?: string;
    public DataType: string = "text";
    public Class?: string = "";
    public ResourceId?: string = "";
    public Sort?: boolean = true;
    public Show?: boolean = true;
    public Search?: boolean = true;
    public NavigateLink?: string = "";
    public NavigateField?: string = "";
    public IsDateSearchField?: boolean = false;
}

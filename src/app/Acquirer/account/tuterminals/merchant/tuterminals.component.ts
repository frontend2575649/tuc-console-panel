import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { Observable } from "rxjs";
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from "../../../../service/service";
declare var moment: any;
declare var $: any;
import swal from "sweetalert2";

@Component({
  selector: "tu-tuterminals",
  templateUrl: "./tuterminals.component.html"
})
export class TUTerminalsComponent implements OnInit {

  Terminals = [];

  StoreName: string;
  TerminalId: string;
  SerialNumber: string
  ProviderName: string;
  TIDs: string[] = [];
  TID: string;
  TID2: string;
  TID3: string;
  TID4: string;
  TID5: string;


  CountTerminals: number;
  curentsize = 0;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService,

  ) {

    this._HelperService.ShowDateRange = false;

  }
  ngOnInit() {
    Feather.replace();
    this._HelperService.StopClickPropogation();

    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
      if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
      } else {
        this.TerminalsList_Setup();
        this.GetTerminalCount();

        //#region TerminalDropDowns 

        this.TerminalsList_Filter_Stores_Load();
        this.TerminalsList_Filter_Banks_Load();
        this.TerminalsList_Filter_Providers_Load();

        //#endregion

        //#region  Dropdowns

        this.ProvidersList_Load();
        this.StoresList_Load();
        this.Form_AddUser_Load();
        this.GetBranches_List();
        this.GetMangers_List();

        //#endregion
      }
    });
  }


  AddTID() {
    for (let index = 0; index < this.TIDs.length; index++) {
      const element = this.TIDs[index];
      if (element.trim().length > 1) {
        this.Terminals.push({
          TerminalId: element, StoreName: this.Form_AddUser.controls['StoreName'].value, ProviderName: this.Form_AddUser.controls['ProviderName'].value, ProviderId: this.Form_AddUser.controls['ProviderId'].value, ProviderKey: this.Form_AddUser.controls['ProviderKey'].value, StoreId: this.Form_AddUser.controls['StoreId'].value, StoreKey: this.Form_AddUser.controls['StoreKey'].value, AcquirerId: this._HelperService.AppConfig.ActiveReferenceId,
          AcquirerKey: this._HelperService.AppConfig.ActiveReferenceKey,
          MerchantId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
          MerchantKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
        });
      }
      else {
      }

    }
    this.TIDs = [];
    this.TID = null;
  }
  DeleteRowTerminal(indexOfTID) {
    if (indexOfTID > -1) {
      this.Terminals.splice(indexOfTID, 1);

    }
  }

  RemoveTID(i) {
    if (i > -1) {
      this.TIDs.splice(i, 1);
    }

    // this.TIDs = this.TIDs.splice(i, 1);
  }
  AddMore() {
    // this.Terminals = this.Terminals.concat([{}, {}, {}, {}, {}]);
    for (let index = 0; index < this.CountTerminals; index++) {
      this.Terminals.push({ StoreName: this.Form_AddUser.controls['StoreName'].value, ProviderName: this.Form_AddUser.controls['ProviderName'].value });
    }
    this.CountTerminals = 0;
    // this.Terminals = this.Terminals.concat([{ StoreName: this.Form_AddUser.controls['StoreName'].value, ProviderName: this.Form_AddUser.controls['ProviderName'].value },
    // { StoreName: this.Form_AddUser.controls['StoreName'].value, ProviderName: this.Form_AddUser.controls['ProviderName'].value },
    // { StoreName: this.Form_AddUser.controls['StoreName'].value, ProviderName: this.Form_AddUser.controls['ProviderName'].value },
    // { StoreName: this.Form_AddUser.controls['StoreName'].value, ProviderName: this.Form_AddUser.controls['ProviderName'].value },
    // { StoreName: this.Form_AddUser.controls['StoreName'].value, ProviderName: this.Form_AddUser.controls['ProviderName'].value }]);

  }


  KeyUP(): void {
    this.TIDs = this.TID.split(',');
  }

  CopyToAll() {
    for (let index = 0; index < this.Terminals.length; index++) {
      const element = this.Terminals[index];

      element['StoreName'] = this.Form_AddUser.controls['StoreName'].value
      element['StoreId'] = this.Form_AddUser.controls['StoreId'].value
      element['StoreKey'] = this.Form_AddUser.controls['StoreKey'].value

      element['ProviderName'] = this.Form_AddUser.controls['ProviderName'].value
      element['ProviderId'] = this.Form_AddUser.controls['ProviderId'].value
      element['ProviderKey'] = this.Form_AddUser.controls['ProviderKey'].value

    }
  }

  //#region Terminals 

  public TerminalsList_Config: OList;
  TerminalsList_Setup() {
    this.TerminalsList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminals,
      Location: this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Account,
      Title: "Available Terminals",
      StatusType: "default",
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      SubReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      SubReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '='),
      Type: this._HelperService.AppConfig.ListType.All,
      DefaultSortExpression: 'CreateDate desc',
      TableFields: [
        {
          DisplayName: 'TID',
          SystemName: 'TerminalId',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Provider',
          SystemName: 'ProviderName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Store',
          SystemName: 'StoreName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        // {
        //   DisplayName: 'Trans',
        //   SystemName: 'TotalTransaction',
        //   DataType: this._HelperService.AppConfig.DataType.Number,
        //   Class: '',
        //   Show: true,
        //   Search: false,
        //   Sort: true,
        //   ResourceId: null,
        // },
        {
          DisplayName: 'Last Tr',
          SystemName: 'LastTransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: false,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Status',
          SystemName: 'StatusCode',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: false,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
      ]
    };
    this.TerminalsList_Config = this._DataHelperService.List_Initialize(
      this.TerminalsList_Config
    );
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Terminal,
      this.TerminalsList_Config
    );
    this.TerminalsList_GetData();
  }
  TerminalsList_ToggleOption(event: any, Type: any) {
    if (event != null) {
      for (let index = 0; index < this.TerminalsList_Config.Sort.SortOptions.length; index++) {
        const element = this.TerminalsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;

        }
      }
    }
    this._HelperService.Update_CurrentFilterSnap(event, Type, this.TerminalsList_Config);
    this.TerminalsList_Config = this._DataHelperService.List_Operations(
      this.TerminalsList_Config,
      event,
      Type
    );
    if (
      (this.TerminalsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TerminalsList_GetData();
    }
  }

  timeout = null;
  TerminalsList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {

      if (event != null) {
        for (let index = 0; index < this.TerminalsList_Config.Sort.SortOptions.length; index++) {
          const element = this.TerminalsList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {

            element.SystemActive = true;

          }
          else {
            element.SystemActive = false;

          }
        }
      }

      this.TerminalsList_Config = this._DataHelperService.List_Operations(
        this.TerminalsList_Config,
        event,
        Type
      );

      if (this.TerminalsList_Config.RefreshData == true) {
        this.TerminalsList_GetData();
      }
      this._HelperService.StopClickPropogation();

    }, this._HelperService.AppConfig.SearchInputDelay);

  }

  TerminalsList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.TerminalsList_Config
    );
    this.TerminalsList_Config = TConfig;
  }
  TerminalsList_RowSelected(ReferenceData) {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Terminal.Dashboard, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveTerminal,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
      }
    );

    this._HelperService.AppConfig.ActiveTerminalReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveTerminalReferenceId = ReferenceData.ReferenceId;

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AcquirerPanel.Terminal
        .Dashboard,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);


  }

  //#endregion

  //#region AddUser 

  Form_AddUser: FormGroup;
  Form_AddUser_Show() {
    this._HelperService.OpenModal("Form_AddUser_Content");
  }
  Form_AddUser_Close() {
    // this._Router.navigate([
    //     this._HelperService.AppConfig.Pages.System.AdminUsers
    // ]);
    this._HelperService.CloseModal("Form_AddUser_Content");
  }
  Form_AddUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.Core.SaveTerminal,
      // AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
      // AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Online,
      // RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource
      //   .System,

      StoreId: [null, Validators.required],
      StoreKey: [null, Validators.required],
      StoreName: null,
      // RmId: [null, Validators.required],
      // RmKey: [null, Validators.required],
      AcquirerId: this._HelperService.AppConfig.ActiveReferenceId,
      AcquirerKey: this._HelperService.AppConfig.ActiveReferenceKey,
      // BankKey: [null],
      // BranchId: [null, Validators.required],
      // BranchKey: [null, Validators.required],
      ProviderId: [null, Validators.required],
      ProviderKey: [null, Validators.required],
      ProviderName: null,
      TerminalId: [null],
      MerchantId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      MerchantKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      // FirstName: [
      //   null,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(2),
      //     Validators.maxLength(128)
      //   ])
      // ],
      StatusCode: this._HelperService.AppConfig.Status.Active,

    });
  }
  Form_AddUser_Clear() {
    this.Form_AddUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }

  Form_AddUser_Process(_FormValue: any) {

    var BulkTerminals = {
      Task: this._HelperService.AppConfig.Api.Core.SaveTerminals,
      Terminals: this.Terminals,
    }

    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Account,
      BulkTerminals
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          //  this._HelperService.NotifySuccess("Account created successfully");
          this._HelperService.NotifySuccess("Terminal Added Successfully.");
          this.Form_AddUser_Clear();
          this.Form_AddUser_Close();
          this.TerminalsList_Setup();
          this.Terminals = [];
          this._HelperService.emitDetailsChangeEvent();
          if (_FormValue.OperationType == "close") {
            this.Form_AddUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }



  //#endregion

  //#region Terminal Dropdowns 

  //#region Bank 

  public TerminalsList_Filter_Bank_Option: Select2Options;
  public TerminalsList_Filter_Bank_Selected = 0;
  TerminalsList_Filter_Banks_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
        }
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Bank_Option = {
      placeholder: 'Sort by Bank',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Banks_Change(event: any) {
    if (event.value == this.TerminalsList_Filter_Bank_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Bank_Selected = 0;
    }
    else if (event.value != this.TerminalsList_Filter_Bank_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Bank_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '='));
    }
    this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  //#endregion

  //#region provider 

  public TerminalsList_Filter_Provider_Option: Select2Options;
  public TerminalsList_Filter_Provider_Selected = 0;
  TerminalsList_Filter_Providers_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
        }
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Provider_Option = {
      placeholder: 'Filter by PTSP',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  /*TerminalsList_Filter_Providers_Change(event: any) {
    if (event.value == this.TerminalsList_Filter_Provider_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Provider_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Provider_Selected = 0;
    }
    else if (event.value != this.TerminalsList_Filter_Provider_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Provider_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Provider_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Provider_Selected, '='));
    }
    this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  } */
  TerminalsList_Filter_Providers_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TerminalsList_Config,
      this._HelperService.AppConfig.OtherFilters.MerchantSales.Provider
    );

    this.ProvidersEventProcessing(event);
  }
  ProvidersEventProcessing(event: any): void {
    if (event.value == this.TerminalsList_Filter_Provider_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Provider_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Provider_Selected = 0;
    }
    else if (event.value != this.TerminalsList_Filter_Provider_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Provider_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Provider_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Provider_Selected, '='));
    }
    this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    this._HelperService.ToggleField = true;
    this.TerminalsList_Filter_Providers_Load();
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 500);
  }
  //#endregion

  //#region store 

  //#region Store

  public TerminalsList_Filter_Store_Option: Select2Options;
  public TerminalsList_Filter_Store_Selected = 0;
  TerminalsList_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Store
        }
      ]
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '=');

    if (this.TerminalsList_Filter_Store_Selected != 0) {
      _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Store_Selected, '=');
    }
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Store_Option = {
      placeholder: "Filter by Store",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Stores_Change(event: any) {

    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TerminalsList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Store
    );

    this.StoreEventProcessing(event);

  }

  StoreEventProcessing(event: any): void {
    if (event.value == this.TerminalsList_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Store_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Store_Selected = 0;
    } else if (event.value != this.TerminalsList_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Store_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Store_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "StoreId",
          this._HelperService.AppConfig.DataType.Number,
          this.TerminalsList_Filter_Store_Selected,
          "="
        )
      );
    }
    this.TerminalsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  //#endregion

  //#endregion

  //#region Dropdowns

  //#region store 

  public StoreKey: string = null;

  public StoresList_Option: Select2Options;
  public StoresList_Selected = 0;
  StoresList_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Store
        }
      ]
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '=');

    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.StoresList_Option = {
      placeholder: 'Select Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  StoresList_Change(event: any) {

    this.Form_AddUser.patchValue(
      {
        StoreKey: event.data[0].ReferenceKey,
        StoreId: event.data[0].ReferenceId,
        StoreName: event.data[0].DisplayName,



      }

    );

  }

  //#endregion

  //#region provider 

  public ProvidersList_Option: Select2Options;
  public ProvidersList_Selected = 0;
  ProvidersList_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
        }
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.ProvidersList_Option = {
      placeholder: 'Select PTSP',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  ProvidersList_Change(event: any) {
    this.Form_AddUser.patchValue(
      {
        ProviderKey: event.data[0].ReferenceKey,
        ProviderId: event.data[0].ReferenceId,
        ProviderName: event.data[0].DisplayName,


      }
    );
  }

  //#endregion

  //#region branch 

  public GetBranches_Option: Select2Options;
  public GetBranches_Transport: any;
  GetBranches_List() {
    var PlaceHolder = "Select Branch";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetBranches,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
      // SearchCondition: "",
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },

        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: 'StatusCode',
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: '=',
        //     SearchValue: this._HelperService.AppConfig.Status.Active,
        // }
      ]
    }



    this.GetBranches_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetBranches_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetBranches_Transport,
      multiple: false,
    };
  }
  GetBranches_ListChange(event: any) {
    // alert(event);\
    this.Form_AddUser.patchValue(
      {
        BranchKey: event.data[0].ReferenceKey,
        BranchId: event.data[0].ReferenceId

      }
    );

  }

  //#endregion

  //#region manager 

  public GetMangers_Option: Select2Options;
  public GetMangers_Transport: any;
  GetMangers_List() {
    var PlaceHolder = "Select Manager";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetManagers,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: "AccountTypeCode",
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: "=",
        //     SearchValue: this._HelperService.AppConfig.AccountType.RelationshipManager
        // }
      ]
    }

    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'RoleId', this._HelperService.AppConfig.DataType.Text, '8', '=');
    this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetMangers_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetMangers_Transport,
      multiple: false,
    };
  }
  GetMangers_ListChange(event: any) {
    // alert(event);
    this.Form_AddUser.patchValue(
      {
        RmKey: event.data[0].text,
        RmId: event.data[0].ReferenceId
      }
    );
  }

  //#endregion

  //#endregion

  //#region Terminals Count 

  public _AOverview: any = {
    TerminalStatus: {
      Active: 0,
      Dead: 0,
      Idle: 0,
      Inactive: 0,
      Total: 0
    }
  };

  GetTerminalCount() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getaccountoverview',
      StartTime: new Date(2017, 0, 1, 0, 0, 0, 0),
      EndDate: moment().endOf('day'),
      AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,
      SubAccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      SubAccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Analytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AOverview = _Response.Result;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  //#endregion

  //#region filterOperations
  SetOtherFilters(): void {
    this.TerminalsList_Config.SearchBaseConditions = [];
    this.TerminalsList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Terminal.Merchant));
    if (CurrentIndex != -1) {
      // this.TerminalsList_Filter_Merchant_Selected = null;
      // this.MerchantEventProcess(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }

    CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Terminal.Provider));
    if (CurrentIndex != -1) {
      this.TerminalsList_Filter_Provider_Selected = null;
      // this.ProvderEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }

    CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Terminal.Store));
    if (CurrentIndex != -1) {
      this.TerminalsList_Filter_Store_Selected = null;
      this.StoreEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetPOSConfig(this.TerminalsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.TerminalsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_POS(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Terminal
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Terminal
        );
        this._FilterHelperService.SetPOSConfig(this.TerminalsList_Config);
        this.TerminalsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.TerminalsList_GetData();
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();

    if (ButtonType == 'Sort') {
      $("#TerminalsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#TerminalsList_fdropdown").dropdown('toggle');
    }
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetPOSConfig(this.TerminalsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.TerminalsList_GetData();
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_POS(Type, index);
    this._FilterHelperService.SetPOSConfig(this.TerminalsList_Config);

    //#region setOtherFilters

    this.SetOtherFilters();

    //#endregion

    this.TerminalsList_GetData();
  }


  //#endregion
  public ResetFilterControls: boolean = true;
  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    // this.TerminalsList_Filter_Merchants_Load();
    this.TerminalsList_Filter_Stores_Load();
    // this.TerminalsList_Filter_Providers_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();

  }
}

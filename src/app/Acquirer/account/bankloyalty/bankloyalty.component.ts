import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DataHelperService } from '../../../service/datahelper.service';
import { FilterHelperService } from '../../../service/filterhelper.service';
import { HelperService } from '../../../service/helper.service';
import * as Feather from "feather-icons";
import { OResponse } from '../../../service/object.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-bankloyalty',
  templateUrl: './bankloyalty.component.html'
})
export class BankloyaltyComponent implements OnInit {
  public activeAcquirerDetails: any
  public BankLoyaltyData: any = {};
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) { }
  ngOnInit() {
    Feather.replace();
    this._HelperService.ValidateData();
    this.activeAcquirerDetails = this._HelperService.GetStorage("hcactiveacquirer")
  }
  ActivateBankLoyalty() {
    var pData = {
      Task: "saveprogram",
      AccountId: this.activeAcquirerDetails.ReferenceId,
      Name: this.activeAcquirerDetails.DisplayName + " - Loyalty",
      SubscriptionId: 26,
      Comission: 0.25,
      RewardCap: 1,
      ProgramTypeCode: "programtype.grouployalty",
      ComissionTypeCode: "commissiontype.zero",
      MaxInvoiceAmount : 5000
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Program, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.BankLoyaltyData = _Response.Result as any;
          this._HelperService.acquirerProgramDetails = [{ "ProgramId": _Response.Result.ReferenceId, "ProgramReferenceKey": _Response.Result.ReferenceKey }]
          this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AcquirerPanel.BankLoyaltyOverview, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId])
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      })
  }
}
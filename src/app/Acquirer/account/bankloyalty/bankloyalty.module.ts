import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BankloyaltyComponent } from './bankloyalty.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { MainPipe } from '../../../service/main-pipe.module'

import { DynamicRoutesguardGuard } from '../../../service/guard/dynamicroutes.guard'

const routes: Routes = [
{ path: "",canActivateChild:[DynamicRoutesguardGuard],data:{accessName:['acqmerchant']}, component: BankloyaltyComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BankLoyaltyRoutingModule {}

@NgModule({
  
  declarations: [BankloyaltyComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    BankLoyaltyRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    MainPipe
  ],
})
export class BankloyaltyModule { }

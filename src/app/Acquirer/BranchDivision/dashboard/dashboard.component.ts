import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { DataHelperService, HelperService, OList, OResponse } from "../../../service/service";

;
declare var $: any;
declare var moment: any;


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
})
export class BranchUpdateComponent implements OnInit, OnDestroy {

  public Options: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      }
    }
  }

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
  ) {
    this._HelperService.ShowDateRange = true;
  }

  //#region barchartconfig 

  public barChartLabels = ['day 1 ', 'day 2', 'day 3', 'day 4', 'day 5', "day 6", "day 7"];
  public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [63, 55, 80, 62, 51, 62, 31], label: 'Remote' },
    { data: [65, 59, 50, 60, 50, 62, 51], label: 'Remote' },
    { data: [28, 48, 40, 81, 60, 82, 21], label: 'Visit' },
    { data: [28, 48, 30, 90, 50, 32, 51], label: 'Visit' },
  ];

  //#endregion

  public _DateSubscription: Subscription = null;

  ngOnDestroy() {
    this._DateSubscription.unsubscribe();
  }

  ngOnInit() {

    this._HelperService.FullContainer = false;

    //#region DateInit 

    this.StartTime = moment().startOf('month');
    this.EndTime = moment().endOf('month');

    this._HelperService.DateRangeStart = moment().startOf('day');
    this._HelperService.DateRangeEnd = moment().add(1, 'minutes');

    //#endregion

    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveBranchReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveBranchReferenceId = params["referenceid"];
      if (this._HelperService.AppConfig.ActiveBranchReferenceKey == null) {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
      } else {
        this.LoadData();
      }
    });

    this._DateSubscription = this._HelperService.RangeAltered.subscribe(value => {
      this.GetAccountOverviewLite();
    });

    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }

  LoadData() {
    this.GetAccountOverviewLite();
    this.Alert_Terminal_Setup();
  }

  //#region AccountOverview 

  public _AccountOverview: OAccountOverview =
    {
      ActiveMerchants: 0,
      ActiveMerchantsDiff: 0,
      ActiveTerminals: 0,
      ActiveTerminalsDiff: 0,
      CardRewardPurchaseAmount: 0,
      CardRewardPurchaseAmountDiff: 0,
      CashRewardPurchaseAmount: 0,
      CashRewardPurchaseAmountDiff: 0,
      Merchants: 0,
      PurchaseAmount: 0,
      PurchaseAmountDiff: 0,
      Terminals: 0,
      Transactions: 0,
      TransactionsDiff: 0,
      UnusedTerminals: 0,
      IdleTerminals: 0,
      DeadTerminals: 0,
      ActiveTerminalsPerc:0,
      IdleTerminalsPerc:0,
      DeadTerminalsPerc:0,
      UnusedTerminalsPerc:0,
    }

  GetAccountOverviewLite() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getaccountoverviewlite',
      StartTime: this._HelperService.DateRangeStart, // new Date(2017, 0, 1, 0, 0, 0, 0),
      EndTime: this._HelperService.DateRangeEnd, // moment().add(2, 'days'),
      UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.ThankU, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverview = _Response.Result as OAccountOverview;

          if (this._AccountOverview.Terminals != 0) {
            var _TempVal = 100 / this._AccountOverview.Terminals;
            this._AccountOverview["IdleTerminalsPerc"] = this._AccountOverview.IdleTerminals * _TempVal;
            this._AccountOverview["ActiveTerminalsPerc"] = this._AccountOverview.ActiveTerminals * _TempVal;
            this._AccountOverview["DeadTerminalsPerc"] = this._AccountOverview.DeadTerminals * _TempVal;
            this._AccountOverview["UnusedTerminalsPerc"] = this._AccountOverview.UnusedTerminals * _TempVal;
          } else {
            this._AccountOverview["IdleTerminalsPerc"] = 0;
            this._AccountOverview["ActiveTerminalsPerc"] = 0;
            this._AccountOverview["DeadTerminalsPerc"] = 0;
            this._AccountOverview["UnusedTerminalsPerc"] = 0;
          }



        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  //#endregion

  //#region AlertTerminals 

  public TerminalsList_Config: OList;
  Alert_Terminal_Setup() {
    this.TerminalsList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminals,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      Title: " Terminal List",
      StatusType: "default",
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
      SubReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      SubReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      Type: this._HelperService.AppConfig.ListType.Branch,
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.dead', '='),
      DefaultSortExpression: 'LastTransactionDate  desc',

      TableFields: [
        {
          DisplayName: 'TID',
          SystemName: 'DisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Provider',
          SystemName: 'ProviderDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Merchant',
          SystemName: 'MerchantDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Store',
          SystemName: 'StoreDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Trans',
          SystemName: 'TotalTransaction',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Last Tr',
          SystemName: 'LastTransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: false,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Status',
          SystemName: 'ApplicationStatusId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
      ]
    };
    this.TerminalsList_Config = this._DataHelperService.List_Initialize(
      this.TerminalsList_Config,
    );
    this.TerminalsList_GetData();

  }

  TerminalsList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.TerminalsList_Config
    );
    this.TerminalsList_Config = TConfig;
  }

  //#endregion

  //#region Transactions 

  public TUTr_Config: OList;
  TUTr_Setup() {
    this.TUTr_Config =
    {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetSaleTransactions,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore,
      Title: 'Sales History',
      StatusType: 'transaction',
      StatusName:"Success",
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '='),
      Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
      Type: this._HelperService.AppConfig.ListType.All,
      DefaultSortExpression: 'TransactionDate desc',
      RefreshCount: false,
      PageRecordLimit: 6,
      TableFields: [
        {
          DisplayName: '#',
          SystemName: 'ReferenceId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Show: true,
          Search: false,
          Sort: false,
          // NavigateField: 'UserAccountKey',
          // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
        },
        {
          DisplayName: 'Date',
          SystemName: 'TransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Show: true,
          Search: false,
          Sort: true,
          IsDateSearchField: true,
        },
      ]
    }
    this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
    this.TUTr_GetData();
  }
  TUTr_GetData() {
    var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
    this.TUTr_Config = TConfig;
  }

  //#endregion

  //#region SalesHIstory 

  StartTime = null;
  EndTime = null;

  public _OSalesHistory =
    {
      Labels: [],
      SaleColors: [],
      SaleDataSet: [],

      SaleCustomersColors: [],
      SaleCustomersDataSet: [],

      TransactionsDataSetColors: [],
      TransactionsDataSet: [],

      TransactionStatusDataSetColors: [],
      TransactionStatusDataSet: [],

      TransactionTypeDataSetColors: [],
      TransactionTypeDataSet: [],

      TransactionTypeCustomersDataSetColors: [],
      TransactionTypeCustomersDataSet: [],
    }
  GetSalesHistory() {
    var SearchCondition = '';
    SearchCondition = this._HelperService.GetDateCondition('', 'Date', this.StartTime, this.EndTime);
    SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'UserAccountId', this._HelperService.AppConfig.DataType.Number, '1', '=');
    var DaysCount = this.EndTime.diff(this.StartTime, 'days');
    var Type = 'days';
    if (DaysCount == 1) {
      Type = 'day';
    }
    else if (DaysCount < 31) {
      Type = 'days';
    }
    else if (DaysCount > 30 && DaysCount < 366) {
      Type = 'month';
    }
    else if (DaysCount > 364) {
      Type = 'year';
    }
    var Data = {
      Task: "getaccountsalessummary",
      TotalRecords: 0,
      Offset: 0,
      Limit: 90,
      RefreshCount: false,
      ReferenceId: 1,
      Type: Type,
      SearchCondition: SearchCondition,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCAnalytics, Data);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          var TStatusDataSet = [];
          var TStatusDataSetColor = [];
          var TransactionsDataSet = [];
          var TransactionsDataSetColor = [];
          this._OSalesHistory.Labels = [];
          var ResponseData = _Response.Result.Data;
          var TSaleColor = [{
            backgroundColor: [],
          }];
          var TSaleDataSet = [{
            label: 'Sale',
            fill: false,
            borderColor: "#907eec",
            borderDash: [10, 0.5],
            pointBorderWidth: 2,
            pointRadius: 4,
            fillColor: "#e9e5ff",
            backgroundColor: ["#907eec"],
            pointBackgroundColor: "#ff9900",
            pointBorderColor: "#907eec",
            pointHoverBackgroundColor: "#907eec",
            pointHoverBorderColor: "#ff9900",
            data: []
          }];
          var TSaleCustomersColor = [{
            backgroundColor: [],
          }];
          var TSaleCustomersDataSet = [{
            label: 'Customer Visits',
            fill: false,
            borderColor: "#907eec",
            borderDash: [10, 0.5],
            pointBorderWidth: 2,
            pointRadius: 4,
            fillColor: "#e9e5ff",
            backgroundColor: ["#907eec"],
            pointBackgroundColor: "#ff9900",
            pointBorderColor: "#907eec",
            pointHoverBackgroundColor: "#907eec",
            pointHoverBorderColor: "#ff9900",
            data: []
          }];
          var TTypeDataSetColor = [];
          var TTypeDataSet = [];
          var TTypeCustomersDataSetColor = [];
          var TTypeCustomersDataSet = [];
          var TTypeDataSetCardItemColor = {
            backgroundColor: [],
          };
          var TTypeDataSetCashItemColor = {
            backgroundColor: [],
          };
          var TTypeCustomersDataSetCardItemColor = {
            backgroundColor: [],
          };
          var TTypeCustomersDataSetCashItemColor = {
            backgroundColor: [],
          };
          var TTypeDataSetCardItem = {
            label: 'Card',
            backgroundColor: ["#61c03c"],
            fill: false,
            borderColor: "#61c03c",
            pointBackgroundColor: "#227900",
            pointBorderColor: "#61c03c",
            borderDash: [10, 0.5],
            pointBorderWidth: 2,
            pointRadius: 4,
            pointHoverBackgroundColor: "#61c03c",
            pointHoverBorderColor: "#227900",
            data: []
          };
          var TTypeDataSetCashItem = {
            label: 'Cash',
            backgroundColor: ["#ff9900"],
            fill: false,
            borderColor: "#ff9900",
            borderDash: [10, 0.5],
            pointBorderWidth: 2,
            pointRadius: 4,
            pointBackgroundColor: "#ff6200",
            pointBorderColor: "#ff9900",
            pointHoverBackgroundColor: "#ff9900",
            pointHoverBorderColor: "#ff6200",
            data: []
          };
          var TTypeCustomersDataSetCardItem = {
            label: 'Card Customers',
            backgroundColor: ["#61c03c"],
            fill: false,
            borderColor: "#61c03c",
            borderDash: [10, 0.5],
            pointBorderWidth: 2,
            pointRadius: 4,
            pointBackgroundColor: "#227900",
            pointBorderColor: "#61c03c",
            pointHoverBackgroundColor: "#61c03c",
            pointHoverBorderColor: "#227900",
            data: []
          };
          var TTypeCustomersDataSetCashItem = {
            label: 'Cash Customers',
            backgroundColor: ["#ff9900"],
            fill: false,
            borderColor: "#ff9900",
            borderDash: [10, 0.5],
            pointBorderWidth: 2,
            pointRadius: 4,
            pointBackgroundColor: "#ff6200",
            pointBorderColor: "#ff9900",
            pointHoverBackgroundColor: "#ff9900",
            pointHoverBorderColor: "#ff6200",
            data: []
          };
          var TransactionsDataSetItemColor = {
            backgroundColor: [],
          };
          var TStatusDataSetSuccessItemColor = {
            backgroundColor: [],
          };
          var TStatusDataSetErrorItemError = {
            backgroundColor: [],
          };
          var TransactionsDataSetItem = {
            label: 'Transactions',
            fill: false,
            borderColor: "#907eec",
            borderDash: [10, 0.5],
            pointBorderWidth: 2,
            pointRadius: 4,
            fillColor: "#e9e5ff",
            backgroundColor: ["#907eec"],
            pointBackgroundColor: "#ff9900",
            pointBorderColor: "#907eec",
            pointHoverBackgroundColor: "#907eec",
            pointHoverBorderColor: "#ff9900",
            data: []
          };
          var TStatusDataSetSuccessItem = {
            label: 'Success',
            backgroundColor: [],
            data: []
          };
          var TStatusDataSetErrorItem = {
            label: 'Error',
            backgroundColor: [],
            data: []
          };
          ResponseData.forEach(element => {
            var Data = element;
            if (Type == 'month') {
              this._OSalesHistory.Labels.push(this._HelperService.GetDateMonthS(element.Date));
            }
            else if (Type == 'year') {
              this._OSalesHistory.Labels.push(this._HelperService.GetDateYearS(element.Date));
            }
            else {
              this._OSalesHistory.Labels.push(this._HelperService.GetDayS(element.Date));
            }
            TSaleDataSet[0].data.push(Math.round(Data.SuccessfulTransactionInvoiceAmount));
            // TSaleColor[0].backgroundColor.push("rgb(144, 126, 236)");
            TSaleCustomersDataSet[0].data.push(Data.SuccessfulTransactionUser);
            // TSaleCustomersColor[0].backgroundColor.push("rgb(144, 126, 236)");

            TTypeDataSetCardItem.data.push(Math.round(Data.CardInvoiceAmount));
            // TTypeDataSetCardItemColor.backgroundColor.push("#61c03c");
            TTypeDataSetCashItem.data.push(Math.round(Data.CashInvoiceAmount));
            // TTypeDataSetCashItemColor.backgroundColor.push("#ff9900");

            TTypeCustomersDataSetCardItem.data.push(Data.CardTransactionUser);
            // TTypeCustomersDataSetCardItemColor.backgroundColor.push("#990099");
            TTypeCustomersDataSetCashItem.data.push(Data.CashTransactionUser);
            // TTypeCustomersDataSetCashItemColor.backgroundColor.push("#00aff0");

            TStatusDataSetSuccessItem.data.push(Data.SuccessfulTransaction);
            TStatusDataSetSuccessItemColor.backgroundColor.push("#00cccc");
            TStatusDataSetErrorItem.data.push(Data.FailedTransaction);
            TStatusDataSetErrorItemError.backgroundColor.push("#f10075");


            TransactionsDataSetItem.data.push(Data.TotalTransaction);
            // TransactionsDataSetItemColor.backgroundColor.push("#61c03c");
          });
          TransactionsDataSet.push(TransactionsDataSetItem);
          TransactionsDataSetColor.push(TransactionsDataSetItemColor);
          TStatusDataSetColor.push(TStatusDataSetSuccessItemColor);
          TStatusDataSetColor.push(TStatusDataSetErrorItemError);
          TStatusDataSet.push(TStatusDataSetSuccessItem);
          TStatusDataSet.push(TStatusDataSetErrorItem);
          TTypeDataSetColor.push(TTypeDataSetCardItemColor);
          TTypeDataSetColor.push(TTypeDataSetCashItemColor);
          TTypeDataSet.push(TTypeDataSetCardItem);
          TTypeDataSet.push(TTypeDataSetCashItem);
          TTypeCustomersDataSetColor.push(TTypeCustomersDataSetCardItemColor);
          TTypeCustomersDataSetColor.push(TTypeCustomersDataSetCashItemColor);
          TTypeCustomersDataSet.push(TTypeCustomersDataSetCardItem);
          TTypeCustomersDataSet.push(TTypeCustomersDataSetCashItem);
          this._OSalesHistory.TransactionsDataSet = TransactionsDataSet;
          this._OSalesHistory.TransactionsDataSetColors = TransactionsDataSetColor;
          this._OSalesHistory.TransactionStatusDataSet = TStatusDataSet;
          this._OSalesHistory.TransactionStatusDataSetColors = TStatusDataSetColor;
          this._OSalesHistory.TransactionTypeDataSet = TTypeDataSet;
          this._OSalesHistory.TransactionTypeDataSetColors = TTypeDataSetColor;
          this._OSalesHistory.TransactionTypeCustomersDataSet = TTypeCustomersDataSet;
          this._OSalesHistory.TransactionTypeCustomersDataSetColors = TTypeCustomersDataSetColor;
          this._OSalesHistory.SaleDataSet = TSaleDataSet;
          this._OSalesHistory.SaleColors = TSaleColor;
          this._OSalesHistory.SaleCustomersDataSet = TSaleCustomersDataSet;
          this._OSalesHistory.SaleCustomersColors = TSaleCustomersColor;
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

}

export class OAccountOverview {
  public ActiveTerminalsPerc?:number;
  public IdleTerminalsPerc?:number;
  public DeadTerminalsPerc?:number;
  public UnusedTerminalsPerc?:number;
  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";
declare var $: any;

import { AgmCoreModule } from "@agm/core";
import {
  OSelect,
  OList,
  DataHelperService,
  HelperService,
  OResponse,
  OStorageContent,
  OCoreParameter,
  OCoreCommon,
  FilterHelperService,
} from "../../../service/service";
import swal from "sweetalert2";
import * as Feather from "feather-icons";

@Component({
  selector: "app-manager",
  templateUrl: "./manager.component.html",
})
export class ManagerComponent implements OnInit {
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService

  ) {
    this._HelperService.ShowDateRange = false;
  }
  ngOnInit() {
    Feather.replace();
    this._HelperService.StopClickPropogation();

    this._ActivatedRoute.params.subscribe((params: Params) => {
      // this._HelperService.AppConfig.ActiveBranchReferenceKey = params["referencekey"];
      // this._HelperService.AppConfig.ActiveBranchReferenceId = params["referenceid"];
      if (
        this._HelperService.AppConfig.ActiveBranchReferenceKey == null ||
        this._HelperService.AppConfig.ActiveBranchReferenceId == null
      ) {
        // this.StoresList_Filter_Owners_Load();
        this.UserAccounts_Filter_Owners_Load();
        this.StoresList_Setup();
      } else {
        // this._HelperService.Get_UserAccountDetails(true);
        // this.StoresList_Filter_Owners_Load();
        this.StoresList_Setup();
        this.UserAccounts_Filter_Owners_Load();

      }
    });
  }

  //#region Stores

  public StoresList_Config: OList;
  StoresList_Setup() {
    this.StoresList_Config = {
      Id: 'StoresList',
      Sort: null,
      Task: this._HelperService.AppConfig.Api.Core.GetManagers,
      Location: this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Branch,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      SubReferenceId: this._HelperService.AppConfig.ActiveBranchReferenceId,
      SubReferenceKey: this._HelperService.AppConfig.ActiveBranchReferenceKey,
      Title: "Available Stores",
      StatusType: "default",
      Type: this._HelperService.AppConfig.ListType.All,
      // DefaultSortExpression: 'CreateDate desc',
      TableFields: [
        {
          DisplayName: "Name",
          SystemName: "Name",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: "Owner",
          SystemName: "OwnerDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: "Contact No",
          SystemName: "MobileNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: false,
          Search: true,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: false,
          Search: true,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: "Terminals",
          SystemName: "Terminals",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: "R. %",
          SystemName: "RewardPercentage",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "_600",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: "Last Tr",
          SystemName: "LastTransactionDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: "Last Login",
          SystemName: "LastLoginDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date",
          Show: false,
          Search: false,
          Sort: false,
          ResourceId: null,
        },

        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
      ],
    };

    this.StoresList_Config = this._DataHelperService.List_Initialize_ForStores(
      this.StoresList_Config
    );
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Merchant,
      this.StoresList_Config
    );

    this.StoresList_GetData();
  }
  StoresList_ToggleOption(event: any, Type: any) {
    if (event != null) {
      for (let index = 0; index < this.StoresList_Config.Sort.SortOptions.length; index++) {
        const element = this.StoresList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.StoresList_Config


    );

    this.StoresList_Config = this._DataHelperService.List_Operations(
      this.StoresList_Config,
      event,
      Type
    );

    if (
      (this.StoresList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.StoresList_GetData();
    }

  }

  timeout = null;
  StoresList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.StoresList_Config.Sort.SortOptions.length; index++) {
          const element = this.StoresList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {

            element.SystemActive = true;

          }
          else {
            element.SystemActive = false;

          }

        }
      }
      this.StoresList_Config = this._DataHelperService.List_Operations(
        this.StoresList_Config,
        event,
        Type
      );

      if (this.StoresList_Config.RefreshData == true) {
        this.StoresList_GetData();
      }

      this._HelperService.StopClickPropogation();

    }, this._HelperService.AppConfig.SearchInputDelay);

  }

  StoresList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(this.StoresList_Config);
    this.StoresList_Config = TConfig;
  }
  StoresList_RowSelected(ReferenceData) {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Store.Dashboard, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
    var Details =
    {
      ReferenceId: ReferenceData.ReferenceId,
      ReferenceKey: ReferenceData.ReferenceKey,
      DisplayName: ReferenceData.DisplayName,
    };

    this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.ActiveManager, {
      "ReferenceKey": ReferenceData.ReferenceKey,
      "ReferenceId": ReferenceData.ReferenceId,
      "DisplayName": ReferenceData.DisplayName,
      "AccountTypeCode": this._HelperService.AppConfig.AccountType.Manager
    });

    this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.ActiveMerchant, Details);

    this._Router.navigate(["console/" + this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Manager.Terminals, ReferenceData.ReferenceKey, ReferenceData.ReferenceId, ReferenceData.RoleId]);


  }

  //#endregion

  //#region OwnersFilter

  public UserAccounts_Filter_Owners_Option: Select2Options;
  public UserAccounts_Filter_Owners_Selected = 0;
  UserAccounts_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetBranches,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ]
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //     [
    //         this._HelperService.AppConfig.AccountType.Merchant,
    //         this._HelperService.AppConfig.AccountType.Acquirer,
    //         this._HelperService.AppConfig.AccountType.PGAccount,
    //         this._HelperService.AppConfig.AccountType.PosAccount
    //     ]
    //     , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.UserAccounts_Filter_Owners_Option = {
      placeholder: 'Sort by Referrer',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  UserAccounts_Filter_Owners_Change(event: any) {

    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.StoresList_Config,
      this._HelperService.AppConfig.OtherFilters.Manager.Owner
    );

    this.OwnerEventProcessing(event);
  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.UserAccounts_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'BranchId', this._HelperService.AppConfig.DataType.Number, this.UserAccounts_Filter_Owners_Selected, '=');
      this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
      this.UserAccounts_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.UserAccounts_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'BranchId', this._HelperService.AppConfig.DataType.Number, this.UserAccounts_Filter_Owners_Selected, '=');
      this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
      this.UserAccounts_Filter_Owners_Selected = event.value;
      this.StoresList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'BranchId', this._HelperService.AppConfig.DataType.Number, this.UserAccounts_Filter_Owners_Selected, '='));
    }

  }


  //#endregion


  public ResetFilterControls: boolean = true;
  SetOtherFilters(): void {
    this.StoresList_Config.SearchBaseConditions = [];
    this.StoresList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      //   this.MerchantsList_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.StoresList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);

    this.SetOtherFilters();

    this.StoresList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Merchant
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Merchant
        );
        this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);
        this.StoresList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.StoresList_GetData();

    if (ButtonType == 'Sort') {
      $("#MerchantsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#MerchantsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);
    this.SetOtherFilters();

    this.StoresList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    // this.MerchantsList_Filter_Owners_Load();
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }


}

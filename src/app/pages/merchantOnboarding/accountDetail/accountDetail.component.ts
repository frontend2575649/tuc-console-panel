import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import * as Feather from 'feather-icons';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DataHelperService, HelperService, OResponse, OUserDetails, OSelect } from "../../../service/service";
import { Observable } from 'rxjs/internal/Observable';
import { LocationStrategy } from '@angular/common';
declare var $: any;
@Component({
    selector: 'app-accountDetail',
    templateUrl: './accountDetail.component.html',
    styleUrls: ['./accountDetail.component.css']
})
export class accountDetailComponent implements OnInit, AfterViewInit {
    deals: boolean = false;
    loyalty: boolean = false;
    rewards: boolean = undefined;
    loyaltyPercentage = null;
    dealsRadio: boolean = false;
    percentVal = null
    accountTypes = []
    loyaltyModel
    dealCategory
    accountId
    accountKey;
    businessName:any;
    constructor(
        private router: Router,
        private activeRoute: ActivatedRoute,
        public _HelperService: HelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        private location: LocationStrategy) { }

    queryParams: any;
    isLoaded: boolean = true;
    public isMainCardShow = true;
    public isAccountTypeSelect = false;
    public isViewPricing = false;
    public MerchantAddress: any = {};
    public MerchantContactPerson: any = {};
    public _MerchantDetails: any =
        {
            ReferenceId: null,
            ReferenceKey: null,
            TypeCode: null,
            TypeName: null,
            SubTypeCode: null,
            SubTypeName: null,
            UserAccountKey: null,
            UserAccountDisplayName: null,
            Name: null,
            Description: null,
            StartDate: null,
            StartDateS: null,
            EndDate: null,
            EndDateS: null,
            SubTypeValue: null,
            MinimumInvoiceAmount: null,
            MaximumInvoiceAmount: null,
            MinimumRewardAmount: null,
            MaximumRewardAmount: null,
            ManagerKey: null,
            ManagerDisplayName: null,
            SmsText: null,
            Comment: null,
            CreateDate: null,
            CreatedByKey: null,
            CreatedByDisplayName: null,
            ModifyDate: null,
            ModifyByKey: null,
            ModifyByDisplayName: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
            Latitude: null,
            Longitude: null,
            CreateDateS: null,
            ModifyDateS: null,
            StatusI: null,
            StatusB: null,
            StatusC: null,

        }


    ngOnInit() {
        this.accountId = this._HelperService.GetStorage("activeRegister").ReferenceId;
        this.accountKey = this._HelperService.GetStorage("activeRegister").ReferenceKey;
        this.businessName = this._HelperService.GetStorage("activeRegister").businessName;
        let isMainCardShow=document.getElementById("isMainCardShow");
        isMainCardShow.style.display = 'block';   
        let isAccountTypeSelect=document.getElementById("isAccountTypeSelect");
        isAccountTypeSelect.style.display = 'none';   
        let isViewPricing=document.getElementById("isViewPricing");
        isViewPricing.style.display = 'none'; 
        const box = document.getElementById('modal');
        $("#modal").modal("show");
        if (!this._HelperService.isDocComplete) {
            this._HelperService.isDocComplete = location.href.includes('isComplete=true')
        }
        if (!this._HelperService.isAccTypeComplete) {
            this._HelperService.isAccTypeComplete = location.href.includes('isAccTypeComplete=true')
        }
    }

    ngAfterViewInit() {
        Feather.replace();
    }
    accountType() {
        let isMainCardShow=document.getElementById("isMainCardShow");
        isMainCardShow.style.display = 'none';   
        let isAccountTypeSelect=document.getElementById("isAccountTypeSelect");
        isAccountTypeSelect.style.display = 'block';   
        let isViewPricing=document.getElementById("isViewPricing");
        isViewPricing.style.display = 'none';   
    }
    viewPricing() {
        let isMainCardShow=document.getElementById("isMainCardShow");
        isMainCardShow.style.display = 'none';   
        let isAccountTypeSelect=document.getElementById("isAccountTypeSelect");
        isAccountTypeSelect.style.display = 'none';   
        let isViewPricing=document.getElementById("isViewPricing");
        isViewPricing.style.display = 'block';  
    }

    back() {
        let isMainCardShow=document.getElementById("isMainCardShow");
        isMainCardShow.style.display = 'block';   
        let isAccountTypeSelect=document.getElementById("isAccountTypeSelect");
        isAccountTypeSelect.style.display = 'none';   
        let isViewPricing=document.getElementById("isViewPricing");
        isViewPricing.style.display = 'none';  
    }
    onClick() {
        if (this.deals && this.loyalty) {
            if (this.percentVal == null) {
                this._HelperService.NotifyError("Please add loyalty percentage value")
            }
            else if (this.percentVal > 100 || this.percentVal < 0) {
                this._HelperService.NotifyError("Please add value between 0-100")
            } else {
                this.accountTypes.push(
                    { AccountTypeCodes: "thankucashloyalty", ConfigurationValue: this.loyaltyModel, Value: this.percentVal },
                    { AccountTypeCodes: "thankucashdeals", value: this.dealCategory }
                )
            }
        }
        else if (this.deals) {
            this.accountTypes.push({ AccountTypeCodes: "thankucashdeals", Value: this.dealCategory })
        }
        else if (this.loyalty) {
            if (this.percentVal == null) {
                this._HelperService.NotifyError("Please add loyalty percentage value")
            }
            else if (this.percentVal > 100 || this.percentVal < 0) {
                this._HelperService.NotifyError("Please add value between 0-100")
            } else {
                this.accountTypes.push(
                    { AccountTypeCodes: "thankucashloyalty", ConfigurationValue: this.loyaltyModel, Value: this.percentVal }
                )
            }
        }

        // console.log("loyalty", this.accountTypes);
        var _PostData = {
            Task: "ob_merchant_requestaccountconfiguration",
            ReferenceId: this.accountId,
            ReferenceKey: this.accountKey,
            AccountTypes: this.accountTypes
        }

        console.log("_PostData", _PostData);

        if (this.accountTypes.length > 0) {
            this._HelperService.IsFormProcessing = true;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.MOnBoardV2, _PostData);
            _OResponse.subscribe(
                _Response => {
                    this.accountTypes = []
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess('Account Details added successfully');
                        this._HelperService.isDocComplete = true;
                        this._HelperService.isAccTypeComplete = true;
                        let isMainCardShow=document.getElementById("isMainCardShow");
                        isMainCardShow.style.display = 'block';   
                        let isAccountTypeSelect=document.getElementById("isAccountTypeSelect");
                        isAccountTypeSelect.style.display = 'none';   
                        let isViewPricing=document.getElementById("isViewPricing");
                        isViewPricing.style.display = 'none'; 
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }

    dealsChange(event) {
        if (event.target.id == 'tuDealsRadio1') {
            this.dealCategory = "0"
        } else if (event.target.id == 'tuDealsRadio2') {
            this.dealCategory = "1"

        } else if (event.target.id == 'tuDealsRadio3') {
            this.dealCategory = "2"
        }
        this.dealsRadio = true;
    }
isValidPercentage:boolean=false;
    percentage(percentage) {
        if (percentage > 0 && percentage < 100) {
            this.percentVal = percentage
            this.isValidPercentage=false;
        }
        else{
            this.isValidPercentage=true
        }
     
        
    }

    rewardSelect(event) {
        if (event.target.id == 'tuLoyaltyRadio1') {
            this.percentVal = null;
            this.rewards = true;
            this.loyaltyModel = "openloyaltymodel"
            this.loyaltyPercentage = null;
            this.loyaltyPercentage = null;
            this.isValidPercentage = false
        } else if (event.target.id == 'tuLoyaltyRadio2') {
            this.percentVal = null
            this.rewards = false;
            this.loyaltyModel = "closedloyaltymodel"
            this.loyaltyPercentage = null;
            this.loyaltyPercentage = null;
            this.isValidPercentage = false
        }
    }

    selectSolution(event, data) {
        if (this.loyalty == false && data == 'tuLoyalty') {
            const box = document.getElementById('loyaltyCard');
            if (box != null) {
                box.classList.add('loyaltyCard');
            }
            this.loyalty = true;
        } else if (this.loyalty == true && data == 'tuLoyalty') {
            const box = document.getElementById('loyaltyCard');

            if (box != null) {
                box.classList.remove('loyaltyCard');
            }
            this.rewards = undefined;
            this.loyalty = false;
        } else if (this.deals == false && data == 'tuDeals') {
            const box = document.getElementById('dealsCard');

            if (box != null) {
                box.classList.add('dealsCard');
            }
            this.deals = true;
        } else if (this.deals == true && data == 'tuDeals') {
            const box = document.getElementById('dealsCard');

            if (box != null) {
                box.classList.remove('dealsCard');
            }
            this.dealsRadio = false;
            this.deals = false;
        }
    }
    backBusinessDetailPage(){
        this.router.navigate(['/console/merchantOnboardingBusiness']);
    }
    home(){
        this.router.navigate(['/console/merchants']);
        this._HelperService.isAccTypeComplete = false;
        this._HelperService.isDocComplete = false;
    }
}

enum HostType {
    Live,
    Test,
    Local
}
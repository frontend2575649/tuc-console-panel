import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import * as Feather from 'feather-icons';
declare var $: any;
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Select2OptionData } from 'ng2-select2';
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { Observable } from 'rxjs/internal/Observable';
import { DataHelperService, HelperService, OResponse, OSelect } from '../../../service/service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-business',
  templateUrl: './businessDetail.component.html',
  styleUrls: ['./businessDetail.component.css']
})
export class businessDetailComponent implements OnInit, AfterViewInit {
  documentForm: FormGroup;
  documentationForm: FormGroup;
  // bankList: any[] = [];
  base64textString: String;

  documentType = [
    { id: 'driversLicense', text: 'Drivers License' },
    { id: 'internationalPassport', text: 'International Passport' },
    { id: 'nationalIdNumber', text: 'National Identity Number (NIN)' },
    { id: 'utilityBill', text: 'Utility Bill' },
    { id: 'voterCard', text: 'Voters Card', },
    { id: 'CAC', text: 'CAC' },
    { id: 'companyCertificate', text: 'Company Certificate of Registration' },
  ];

  public _S2Banks_Data: Array<Select2OptionData>;
  public _S2DocumentType_Data: Array<Select2OptionData>;
  public HCXLocManager_S2States_Option: Select2Options;
  sizeRestriction: boolean = false;
  public BCategory_Option: Select2Options;
  public Banks_Option: Select2Options;
  public _S2DocumentType_Option: Select2Options;

  public displayMap = false;
  ShowCity = true;
  ShowState = true;
  ShowCategory = true
  ShowBanks = true
  IsAddressSet = false;
  selectedCategoryId = null;
  selectedCategoryKey = null;
  selectedCategory = null;
  categoryValue = null
  DocValue = null
  BankValue = null
  isDocumentationForm: boolean = false;
  // selectedBank = null;
  docName;
  fileName;
  fileSize;
  fileType;
  accountId
  accountKey
  public modifiedName

  public data =
    {

      Address: null,
      Latitude: null,
      Longitude: null,
      CityAreaId: null,
      CityAreaCode: null,
      CityAreaName: null,
      CityId: null,
      CityCode: null,
      CityName: null,
      StateId: null,
      StateCode: null,
      StateName: null,
      CountryId: null,
      CountryCode: null,
      CountryName: null,
      PostalCode: null,
      MapAddress: null,
      ReferralCode: null,
      draggable: true
    };
  isLoaded: boolean = true;
  public MerchantAddress: any = {};
  public MerchantContactPerson: any = {};
  public _MerchantDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      Latitude: null,
      Longitude: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }

  public _BankDetails = {
    BankId: null,
    BankName: null,
    BankCode: null,
    AccountNumber: null,
    AccountName: null
  }
  public _ContactInfo: boolean;
  public _cpMobileNumber

  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _location: Location) {
    this._ContactInfo = false;
  }

  ngOnInit() {
    let secondForm = document.getElementById("secondPage");
    secondForm.style.display = 'none';
    this._MerchantDetails = this._HelperService.GetStorage("merchantDetails");
    this.accountId = this._HelperService.GetStorage("hca").UserAccount.AccountId
    this.accountKey = this._HelperService.GetStorage("hca").UserAccount.AccountKey
    let businessName;
    let businessEmail;
    this.documentForm = this.formBuilder.group({
      'businessName': [businessName, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
      'bussinessEmail': [businessEmail, Validators.compose([Validators.required, Validators.email, Validators.minLength(2), Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$')])],
      'accountNumber': [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14)])],
      'bank': [null, Validators.compose([Validators.required])],
      'accountName': [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128), Validators.pattern("^[a-zA-Z _-]*$")])],
      'selectCategory': [null, Validators.required],
      'streetAddress': [null, Validators.required],
      'city': [null, Validators.compose([Validators.required, Validators.pattern("^[a-zA-Z _-]*$")])],
      'state': [null, Validators.compose([Validators.required, Validators.pattern("^[a-zA-Z _-]*$")])],
      'detailAddress': [null, Validators.required],
      'lat': [null],
      'lng': [null],
      "CountryIsd": ["234", Validators.required],
      'docType': [null, Validators.required],
      'docFile': [null, Validators.required],
      'MobileNumber': [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])], Source: 'Merchant',
      'cpFirstName': [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128), Validators.pattern("^[a-zA-Z _-]*$")])],
      'cpLastName': [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128), Validators.pattern("^[a-zA-Z _-]*$")])],
      'cpMobileNumber': [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      'cpEmailAddress': [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2), Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$')])],


    });
    this.documentationForm = this.formBuilder.group({
      docType: [null, Validators.compose([Validators.required])],
      docFile: [null, Validators.compose([Validators.required])]
    });

    this._S2DocumentType_Option = {
      placeholder: "Select Document",
      multiple: false,
      allowClear: false,
    };

    this.TUTr_Filter_Banks_Load();
    this.GetBusinessCategories_List();
    this._S2DocumentType_Data = this.documentType
  }
  uploadedDocument: any;
  GetDocType_Selected(event: any) {
    this.documentationForm.patchValue({ "docType": event.data[0].text })
    this.documentForm.patchValue({ "docType": event.data[0].text })
    this.uploadedDocument = event;
  }

  _ToggleContactInfo(val?: boolean) {

    if (val != undefined) {
      this._ContactInfo = val
    }

    if (this._ContactInfo) {
      this.documentForm.controls['cpMobileNumber'].patchValue(this.documentForm.controls['MobileNumber'].value);
      this.documentForm.controls['cpEmailAddress'].patchValue(this.documentForm.controls['bussinessEmail'].value);
      $('#ymobileFormSelector').attr('readonly', 'readonly');
      $('#yemailFormSelector').attr('readonly', 'readonly');

    } else {
      this.documentForm.controls['cpMobileNumber'].patchValue(null);
      this.documentForm.controls['cpEmailAddress'].patchValue(null);
      this.documentForm.controls['cpMobileNumber'].markAsUntouched();
      this.documentForm.controls['cpEmailAddress'].markAsUntouched();

      $('#ymobileFormSelector').attr('readonly', false);
      $('#yemailFormSelector').attr('readonly', false);

    }
  }

  markerDragEnd(e) {

    this.data.Latitude = e.coords.lat;
    this.data.Longitude = e.coords.lng;
    this.documentForm.patchValue({ "lat": this.data.Latitude })
    this.documentForm.patchValue({ "lng": this.data.Longitude })
  }
  ngAfterViewInit() {
    Feather.replace();
  }

  docModalOpen() {
    let element = document.getElementById("firstPage");
    // $('#exampleModalCenter').modal({backdrop: 'static', keyboard: false}) 
    element.style.display = 'none';
    let secondForm = document.getElementById("secondPage");
    secondForm.style.display = 'block';

    // if (this.documentationForm){
    //   this.documentationForm.patchValue({ "docType": this.uploadedDocument.data[0].text })
    //   this.documentForm.patchValue({ "docType": this.uploadedDocument.data[0].text })
    // }

    // if (this.selectedFile) {
    //   let file = this.selectedFile;

    //   this.docName = file.name
    //   this.fileName = file.name;
    //   this.fileSize = file.size;
    //   this.fileType = file.type

    //   const reader = new FileReader();
    //   reader.readAsDataURL(file);
    //   reader.onload = () => {

    //     this.documentForm.patchValue({ "docFile": reader.result })
    //   };
    // }
  }

  NavigateHome() {
    this.router.navigate(['/console/merchants']);
  }

  uploadDoc() {


    if (this.documentationForm.value.docType == null) {
      this._HelperService.NotifyError("Please select document type")
    }
    else if (this.documentationForm.value.docFile == null) {
      this._HelperService.NotifyError("Please select Document")
    }
    else if (this.documentationForm.valid) {
      let element = document.getElementById("firstPage");

      element.style.display = 'block';
      let secondForm = document.getElementById("secondPage");
      secondForm.style.display = 'none';

      // $('#exampleModalCenter').modal("hide");

      Feather.replace();
      this.docModalClose()
    }
  }

  clearName() {
    this.modifiedName = ''
  }

  selectedFile: any;

  onFileChanged(event: { target: { files: any[]; }; }) {
    if (event.target.files && event.target.files.length > 0) {
      this.selectedFile = event.target.files[0];
      let file = event.target.files[0];
      this.sizeRestriction = file.size / 1000000 > 1
      if (this.sizeRestriction == false) {
        this.docName = file.name
        this.fileName = file.name;
        this.fileSize = file.size;
        this.fileType = file.type

        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {

          this.documentForm.patchValue({ "docFile": reader.result })
        }

      }
      else {
        this.docName = null
        this.fileName = null
        this.fileSize = null
        this.fileType = null

        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {

          this.documentForm.patchValue({ "docFile": null })
        }
      };
    }
  }


  docModalClose() {
    // this.isDocumentationForm = false;
    let element = document.getElementById("firstPage");

    element.style.display = 'block';
    let secondForm = document.getElementById("secondPage");
    secondForm.style.display = 'none';
  }

  backHome() {
    this.router.navigate(['/console/merchantOnboardingAccount'], { queryParams: { isComplete: true } });
  }

  public TUTr_Filter_Bank_Option: Array<Select2OptionData>;
  TUTr_Filter_Banks_Load() {
    this.ShowBanks = false;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.getbankcodes,
      Offset: 0,
      Limit: 1000,
      CountryId: this._HelperService.UserCountry.CountryId
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Bank, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result != undefined) {
            var _Data = _Response.Result.Data as any[];
            if (_Data.length > 0) {
              var finalCat = [];
              this.ShowBanks = false;
              _Data.forEach(element => {
                var Item = {
                  id: element.ReferenceId,
                  text: element.Name,
                  ReferenceKey: element.ReferenceKey,
                  ReferenceId: element.ReferenceId,
                  Code: element.Code
                };
                finalCat.push(Item);
              });
              this.Banks_Option = {
                placeholder: "Select Bank",
                multiple: false,
                allowClear: false,
              };
              this.TUTr_Filter_Bank_Option = finalCat;
              this.ShowBanks = true;
            }
            else {
              this.Banks_Option = {
                placeholder: "Select Bank",
                multiple: false,
                allowClear: false,
              };
              setTimeout(() => {
                this.ShowBanks = true;
              }, 500);
            }
          }
          else {
            this.Banks_Option = {
              placeholder: "Select Bank",
              multiple: false,
              allowClear: false,
            };
            setTimeout(() => {
              this.ShowBanks = true;
            }, 500);
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;
      }
    );
  }
  TUTr_Filter_Banks_Change(event: any) {

    if (event.data[0] != undefined && event.data[0] != undefined && event.data.length > 0) {
      this._BankDetails.BankCode = event.data[0].Code;
      this._BankDetails.BankName = event.data[0].text;
      this._BankDetails.BankId = event.data[0].ReferenceId
      this.documentForm.patchValue({ "bank": event.data[0].text })
    }
  }
  Countries = [
    {
      ReferenceId: 1,
      ReferenceKey: "nigeria",
      Name: "Nigeria",
      IconUrl: "../../../../assets/countries/nigeria_icon.png",
      Isd: "234",
      Iso: "ng",
      CurrencyName: "Naira",
      CurrencyNotation: "NGN",
      CurrencySymbol: "₦",
      CurrencyHex: "&#8358;",
      NumberLength: 10
    },
    {
      ReferenceId: 87,
      ReferenceKey: "ghana",
      Name: "Ghana",
      IconUrl: "../../../../assets/countries/ghana_icon.png",
      Isd: "233",
      Iso: "gh",
      CurrencyName: "Cedi",
      CurrencyNotation: "GHS",
      CurrencySymbol: "₵",
      CurrencyHex: "&#8373;",
      NumberLength: 9
    },
    {
      ReferenceId: 118,
      ReferenceKey: "kenya",
      Name: "Kenya",
      IconUrl: "../../../../assets/countries/kenya_icon.png",
      Isd: "254",
      Iso: "key",
      CurrencyName: "Shilling",
      CurrencyNotation: "KHS",
      CurrencySymbol: "KSh",
      CurrencyHex: "KSh",
      NumberLength: 9
    }
  ]
  SelectedCountry = this.Countries[0];
  CountryClick(Item) {
    this.SelectedCountry = Item;
    this.documentForm.patchValue({
      CountryIsd: Item.Isd,
    })
  }
  public _S2Categories_Data: Array<Select2OptionData>;
  GetBusinessCategories_List() {
    this.ShowCategory = false;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Product.getcategories,
      Offset: 0,
      Limit: 1000,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Op, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            var _Data = _Response.Result.Data as any[];
            if (_Data.length > 0) {
              var finalCat = [];
              this.ShowCategory = false;
              _Data.forEach(element => {
                var Item = {
                  id: element.ReferenceId,
                  text: element.Name,
                  ReferenceKey: element.ReferenceKey,
                  ReferenceId: element.ReferenceId
                };
                finalCat.push(Item);
              });
              this.BCategory_Option = {
                placeholder: "Select Category",
                multiple: false,
                allowClear: false,
              };
              this._S2Categories_Data = finalCat;
              this.ShowCategory = true;
            }
            else {
              this.BCategory_Option = {
                placeholder: "Select Category",
                multiple: false,
                allowClear: false,
              };
              setTimeout(() => {
                this.ShowCategory = true;
              }, 500);
            }
          }
          else {
            this.BCategory_Option = {
              placeholder: "Select Category",
              multiple: false,
              allowClear: false,
            };
            setTimeout(() => {
              this.ShowCategory = true;
            }, 500);
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;
      });
  }

  GetBusinessCategories_Selected(Items: any) {
    if (Items != undefined && Items.value != undefined && Items.data.length > 0) {

      this.selectedCategoryId = Items.data[0].ReferenceId
      this.selectedCategoryKey = Items.data[0].ReferenceKey;
      this.selectedCategory = Items.data[0].text
      this.documentForm.patchValue({ "selectCategory": this.selectedCategory })
    }
  }

  HCXLocManager_OpenUpdateManager_Clear() {
    this.IsAddressSet = false;
    this.data =
    {
      Latitude: 0,
      Longitude: 0,

      Address: null,
      MapAddress: null,

      CountryId: this._HelperService.UserCountry.CountryId,
      CountryCode: this._HelperService.UserCountry.CountryKey,
      CountryName: this._HelperService.UserCountry.CountryName,

      StateId: 0,
      StateName: null,
      StateCode: null,

      CityId: 0,
      CityCode: null,
      CityName: null,

      CityAreaId: 0,
      CityAreaName: null,
      CityAreaCode: null,

      PostalCode: null,
      ReferralCode: null,
      draggable: true
    }
  }
  HCXLocManager_AddressChange(address: Address) {

    this.data.Latitude = address.geometry.location.lat();
    this.data.Longitude = address.geometry.location.lng();
    this.data.MapAddress = address.formatted_address;
    this.data.Address = address.formatted_address;
    this.documentForm.patchValue({ "lat": this.data.Latitude })
    this.documentForm.patchValue({ "lng": this.data.Longitude })
    var tAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);


    if (tAddress.postal_code != undefined && tAddress.postal_code != null && tAddress.postal_code != "") {
      this.data.PostalCode = tAddress.postal_code;
    }
    if (tAddress.country != undefined && tAddress.country != null && tAddress.country != "") {
      this.data.CountryName = tAddress.country;
    }
    if (tAddress.administrative_area_level_1 != undefined && tAddress.administrative_area_level_1 != null && tAddress.administrative_area_level_1 != "") {
      this.data.StateName = tAddress.administrative_area_level_1;
    }
    if (tAddress.locality != undefined && tAddress.locality != null && tAddress.locality != "") {
      this.data.CityName = tAddress.locality;
    }
    if (tAddress.administrative_area_level_2 != undefined && tAddress.administrative_area_level_2 != null && tAddress.administrative_area_level_2 != "") {
      this.data.CityAreaName = tAddress.administrative_area_level_2;
    }
    if (tAddress.administrative_area_level_2 != undefined && tAddress.administrative_area_level_2 != null && tAddress.administrative_area_level_2 != "") {
      this.data.CityAreaName = tAddress.administrative_area_level_2;
    }
    if (this.data.CountryName != this._HelperService.UserCountry.CountryName) {
      this._HelperService.NotifyError('Currently we’re not serving in this area, please add locality within ' + this._HelperService.UserCountry.CountryName);
      setTimeout(() => {
        this.HCXLocManager_OpenUpdateManager_Clear();
      }, 1);
    }

    // if (this.data.CountryName != "Nigeria") {
    //   this.displayMap = false
    //   this._HelperService.NotifyError('Currently we’re not serving in this area');

    //   setTimeout(() => {
    //     this.HCXLocManager_OpenUpdateManager_Clear();
    //   }, 1);
    // }
    else {
      this.displayMap = true
      // this.data.CountryId = 1;
      // this.data.CountryCode = "nigeria";
      // this.data.CountryName = "Nigeria";
      this.data.CountryId = this._HelperService.UserCountry.CountryId;
      this.data.CountryCode = this._HelperService.UserCountry.CountryKey;
      this.data.CountryName = this._HelperService.UserCountry.CountryName;
      this.documentForm.patchValue({ "streetAddress": this.data.MapAddress })
      this.HCXLoc_Manager_GetStates();
    }
  }


  public HCXLocManager_S2States_Data: Array<Select2OptionData>;
  HCXLoc_Manager_GetStates() {
    this._HelperService.IsFormProcessing = true;
    this.ShowState = false;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getstates,
      ReferenceKey: this.data.CountryCode,
      ReferenceId: this.data.CountryId,
      Offset: 0,
      Limit: 1000,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.State, PData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            var _Data = _Response.Result.Data as any[];
            if (_Data.length > 0) {
              var finalCat = [];
              _Data.forEach(element => {
                var Item = {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name,
                  additional: element,
                };
                if (this.data.StateName != undefined || this.data.StateName != null || this.data.StateName != '') {
                  if (element.Name == this.data.StateName) {
                    this.data.StateId = Item.id;
                    this.data.StateCode = Item.key;
                    this.data.StateName = Item.text;
                    this.documentForm.patchValue({ "state": this.data.StateName })
                    this.ShowCity = false;
                    this.HCXLoc_Manager_City_Load();
                    setTimeout(() => {
                      this.ShowCity = true;
                    }, 1);
                  }
                }
                finalCat.push(Item);
              }); if (this.data.StateId > 0) {
                this.HCXLocManager_S2States_Option = {
                  placeholder: this.data.StateName,
                  multiple: false,
                  allowClear: false,
                };
                setTimeout(() => {
                  this.ShowState = true;
                }, 500);
              }
              else {
                this.HCXLocManager_S2States_Option = {
                  placeholder: "Select state",
                  multiple: false,
                  allowClear: false,
                };
                setTimeout(() => {
                  this.ShowState = true;
                }, 500);
              }
              this.HCXLocManager_S2States_Data = finalCat;
              this.ShowState = true;
              if (this.data.CityName != undefined && this.data.CityName != null && this.data.CityName != "") {
                this.HCXLoc_Manager_GetStateCity(this.data.CityName);
              }
              // this.HCXLocManager_S2States_Data = finalCat;
              // this.ShowState = true;
              // if (this.data.CityName != undefined && this.data.CityName != null && this.data.CityName != "") {
              //   this.HCXLoc_Manager_GetStateCity(this.data.CityName);
              // }
            }
            else {
              this.HCXLocManager_S2States_Option = {
                placeholder: "Select state",
                multiple: false,
                allowClear: false,
              };
              setTimeout(() => {
                this.ShowState = true;
              }, 500);
            }
          }
          else {
            this.HCXLocManager_S2States_Option = {
              placeholder: "Select state",
              multiple: false,
              allowClear: false,
            };
            setTimeout(() => {
              this.ShowState = true;
            }, 500);
          }
        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });
  }
  // HCXLoc_Manager_StateSelected(Items) {
  //   if (Items != undefined && Items.value != undefined && Items.data.length > 0 && Items.data[0].selected) {
  //     this.data.StateId = Items.data[0].id;
  //     this.data.StateCode = Items.data[0].key;
  //     this.data.StateName = Items.data[0].text;
  //     this.documentForm.patchValue({ "state": this.data.StateName })
  //     this.ShowCity = false;
  //     this.documentForm.patchValue({ "city": this.data.CityName })
  //     this.documentForm.patchValue({ "streetAddress": this.data.MapAddress })
  //     this.HCXLoc_Manager_City_Load();
  //     setTimeout(() => {
  //       this.ShowCity = true;
  //     }, 1);
  //   }
  // }
  HCXLoc_Manager_StateSelected(Items) {
    if (this.data.MapAddress) {
      this.data.MapAddress = null
      //  this.documentForm.patchValue({ "streetAddress": null})
      // this.documentForm.controls['streetAddress'].markAsUntouched()
    }
    if (Items != undefined && Items.value != undefined && Items.data.length > 0 && Items.data[0].selected) {
      this.displayMap = false
      this.data.StateId = Items.data[0].id;
      this.data.StateCode = Items.data[0].key;
      this.data.StateName = Items.data[0].text;
      this.documentForm.patchValue({ "state": this.data.StateName })
      // this.data.CityId = null;
      // this.data.CityCode = null;
      // this.data.CityName = null;
      // this.data.Address = null;
      // this.data.MapAddress = null;
      // this.data.CityAreaName = null
      // this.data.Latitude = 0;
      // this.data.Longitude = 0;
      this.ShowCity = false;
      this.documentForm.patchValue({ "city": this.data.CityName })

      this.HCXLoc_Manager_City_Load();
      setTimeout(() => {
        this.ShowCity = true;
      }, 1);
    }
  }
  HCXLoc_Manager_GetStateCity(CityName) {
    this._HelperService.IsFormProcessing = true;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getcities,
      ReferenceKey: this.data.StateCode,
      ReferenceId: this.data.StateId,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'Name', 'text', CityName, '='),
      Offset: 0,
      Limit: 1,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.City, PData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            var _Result = _Response.Result.Data;
            if (_Result != undefined && _Result != null && _Result.length > 0) {
              var Item = _Result[0];
              this.data.CityId = Item.ReferenceId;
              this.data.CityCode = Item.ReferenceKey;
              this.data.CityName = Item.Name;
              this.documentForm.patchValue({ "city": this.data.CityName })
              // this.ShowCity = false;
              this.HCXLoc_Manager_City_Load();
              setTimeout(() => {
                this.ShowCity = true;
              }, 1);
            }
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });
  }

  public HCXLoc_Manager_City_Option: Select2Options;
  HCXLoc_Manager_City_Load() {
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.getcities,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.City,
      ReferenceId: this.data.StateId,
      ReferenceKey: this.data.StateCode,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    if (this.data.CityName != undefined || this.data.CityName != null && this.data.CityName != '') {
      this.documentForm.patchValue({ "city": this.data.CityName })
      this.HCXLoc_Manager_City_Option = {
        placeholder: this.data.CityName,
        ajax: _Transport,
        multiple: false,
        allowClear: false,
      };
    }
    else {
      this.HCXLoc_Manager_City_Option = {
        placeholder: "Select City",
        ajax: _Transport,
        multiple: false,
        allowClear: false,
      };
    }

  }
  HCXLoc_Manager_City_Change(Items: any) {
    if (Items != undefined && Items.value != undefined && Items.data.length > 0) {
      this.data.CityId = Items.data[0].ReferenceId;
      this.data.CityCode = Items.data[0].ReferenceKey;
      this.data.CityName = Items.data[0].Name;
      this.documentForm.patchValue({ "city": this.data.CityName })
    }
  }
  HCXLoc_Manager_Validator() {
    this._BankDetails.AccountName = this.documentForm.value.accountName
    this._BankDetails.AccountNumber = this.documentForm.value.accountNumber;
    if (this.documentForm.value.bank == null) {
      this._HelperService.NotifyError("Please select bank")
    }
    else if (this.documentForm.value.selectCategory == null || this.documentForm.value.selectCategory == "Select category") {
      this._HelperService.NotifyError("Please select category")
    }
    else if (this.documentForm.value.cpFirstName == null) {
      this._HelperService.NotifyError("Please enter contact person's first name")
    }
    else if (this.documentForm.value.cpLastName == null) {
      this._HelperService.NotifyError("Please enter contact person's last name")
    }
    else if (this.documentForm.value.cpMobileNumber == null) {
      this._HelperService.NotifyError("Please enter contact person's mobile number")
    }
    else if (this.documentForm.value.cpEmailAddress == null) {
      this._HelperService.NotifyError("Please enter contact person's email address")
    }
    else if (this.documentForm.value.streetAddress == null) {
      this._HelperService.NotifyError("Please enter address")
    }
    else if (this.documentForm.value.city == null) {
      this._HelperService.NotifyError("Please select city")
    }
    else if (this.documentForm.value.state == null) {
      this._HelperService.NotifyError("Please select state")
    }
    else if (this.documentForm.value.detailAddress == null) {
      this._HelperService.NotifyError("Please enter Building/Floor number")
    }
    else if (this.documentForm.value.docFile == null) {
      this._HelperService.NotifyError("Please upload identification document")
    }
    else if (this.documentForm.invalid) {
      this._HelperService.NotifyError("Please fill all the details")
    }
    else if (this.documentForm.valid) {
      var _PostData = {
        Task: "ob_merchant_requestupdatebusinessDetails",
        ReferenceId: this.accountId,
        ReferenceKey: this.accountKey,
        BusinessName: this.documentForm.value.businessName,
        BusinessEmail: this.documentForm.value.bussinessEmail,
        MobileNumber: this.documentForm.value.MobileNumber,
        Category: {
          ReferenceId: this.selectedCategoryId,
          ReferenceKey: this.selectedCategoryKey
        },
        Bankdetails: this._BankDetails,
        Address: {
          Latitude: this.documentForm.value.lat,
          Longitude: this.documentForm.value.lng,
          Address: this.documentForm.value.streetAddress,
          MapAddress: this.data.MapAddress,
          CountryId: this.data.CountryId,
          CountryCode: this.data.CountryCode,
          CountryName: this.data.CountryName,
          StateId: this.data.StateId,
          StateName: this.documentForm.value.state,
          StateCode: this.data.StateCode,
          CityId: this.data.CityId,
          CityCode: this.data.CityCode,
          CityName: this.data.CityName,
          CityAreaId: 0,
          CityAreaName: this.data.CityAreaName,
          CityAreaCode: 0,
          PostalCode: this.data.PostalCode
        },
        ImageContent: {
          Name: this.fileName,
          Size: this.fileSize,
          Extension: this.fileType,
          Content: this.documentForm.value.docFile,
          DocumentType: this.documentForm.value.docType,
          IsDefault: 0
        }
      }
      let emailid = this.documentForm.controls['bussinessEmail'].value;
      let email = emailid.toLowerCase();
      let cpEmailid = this.documentForm.controls['cpEmailAddress'].value;
      let cpEmail = cpEmailid.toLowerCase();

      let requestData = {
        Task: "savemerchant",
        BusinessName: this.documentForm.controls['businessName'].value,
        BusinessEmail: email,
        MobileNumber: this.documentForm.controls['MobileNumber'].value,
        countryIsd: this.documentForm.controls['CountryIsd'].value,

        AccountId: 11272,
        Category: {
          ReferenceId: this.selectedCategoryId,
          ReferenceKey: this.selectedCategoryKey
        },
        Bankdetails: this._BankDetails,
        ContactPerson: {
          FirstName: this.documentForm.value.cpFirstName,
          LastName: this.documentForm.value.cpLastName,
          MobileNumber: this.documentForm.value.cpMobileNumber,
          EmailAddress: cpEmail,
          countryIsd: this.documentForm.controls['CountryIsd'].value
        },
        Address: {
          Latitude: this.documentForm.value.lat,
          Longitude: this.documentForm.value.lng,
          Address: this.documentForm.value.streetAddress,
          MapAddress: this.data.MapAddress,
          CountryId: this.data.CountryId,
          CountryCode: this.data.CountryCode,
          CountryName: this.data.CountryName,
          StateId: this.data.StateId,
          StateName: this.documentForm.value.state,
          StateCode: this.data.StateCode,
          CityId: this.data.CityId,
          CityCode: this.data.CityCode,
          CityName: this.data.CityName,
          CityAreaId: 0,
          CityAreaName: this.data.CityAreaName,
          CityAreaCode: 0,
          PostalCode: this.data.PostalCode
        },
        ImageContent: {
          Name: this.fileName,
          Size: this.fileSize,
          Extension: this.fileType,
          Content: this.documentForm.value.docFile.split("base64,")[1],
          DocumentType: this.documentForm.value.docType,
          IsDefault: 0
        }
      }
      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;
      let _MResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(
        this._HelperService.AppConfig.NetworkLocation.Console.V3.MOnBoardV2,
        requestData
      );
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess(_Response.Message);
            this.accountId = _Response.Result.ReferenceId;
            this.accountKey = _Response.Result.ReferenceKey;
            let activeRegister = {
              ReferenceId: _Response.Result.ReferenceId,
              ReferenceKey: _Response.Result.ReferenceKey,
              businessName: this.documentForm.controls['businessName'].value
            }
            this._HelperService.SaveStorage("activeRegister", activeRegister);
            this.backHome()
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );

    }
  }
}

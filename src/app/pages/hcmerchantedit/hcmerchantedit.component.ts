import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { ImageCroppedEvent } from 'ngx-image-cropper';
//import { InputFile, InputFileComponent } from 'ngx-input-file';
import { InputFileConfig, InputFileModule, InputFileComponent, InputFile } from 'ngx-input-file';
import { Observable } from 'rxjs';
import { HCoreXAddress, HCXAddressConfig, locationType } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';
import swal from 'sweetalert2';
import { DataHelperService, HelperService, OResponse, OUserDetails } from '../../service/service';

@Component({
    selector: 'hc-hcmerchantedit',
    templateUrl: './hcmerchantedit.component.html',
})
export class HCMerchantEditComponent implements OnInit {
    public SaveAccountRequest: any;

    // @ViewChild(InputFileComponent)
    // private InputFileComponent: InputFileComponent;
    CurrentImagesCount: number = 0;

    public ShowCategorySelector: boolean = true;

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {
    }
    @ViewChild("addimage")
    private InputFileComponent_Add: InputFileComponent;

    @ViewChild("EditImage")
    private InputFileComponent_Edit: InputFileComponent;

    @ViewChild("inputfile")
    private InputFileComponent: InputFileComponent;

    public _Address: HCoreXAddress = {};
    public _AddressConfig: HCXAddressConfig = {
        locationType: locationType.form
    };
    AddressChange(Address) {
        this._Address = Address;
    }

    private InitImagePicker(previewurl?: string) {
        if (this.InputFileComponent != undefined) {
            this.CurrentImagesCount = 0;
            if (previewurl) {
                this.InputFileComponent.files[0] = {};
                this.InputFileComponent.files[0].preview = previewurl;
            }
            this._HelperService._InputFileComponent = this.InputFileComponent;
            this.InputFileComponent.onChange = (files: Array<InputFile>): void => {
                if (files.length >= this.CurrentImagesCount) {
                    this._HelperService._SetFirstImageOrNone(this.InputFileComponent.files);
                }
                this.CurrentImagesCount = files.length;
            };
        }
    }
    public isAddressLoaded = false;
    ngOnInit() {
        this._HelperService.FullContainer = true;
        // this.checkimg();
        // this._HelperService.Icon_Crop_Clear();
        // this._HelperService._InputFileComponent = this.InputFileComponent;
        // this.InputFileComponent.onChange = (files: Array<InputFile>): void => {
        //     if (files.length >= this.CurrentImagesCount) {
        //         this._HelperService._SetFirstImageOrNone(this.InputFileComponent.files);
        //     }
        //     this.CurrentImagesCount = files.length;
        // }
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveMerchantReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveMerchantReferenceId = params["referenceid"];
            if (
                this._HelperService.AppConfig.ActiveMerchantReferenceKey == null ||
                this._HelperService.AppConfig.ActiveMerchantReferenceId == null
            ) {
                this.Form_UpdateUser_Load();
                this.Get_UserAccountDetails();
                this.Form_EditPassword_Load();
                this.Form_UpdateContactUser_Load();
                this.Form_Edit_Load();
                this.Form_UpdateCredentialUser_Load();
                this.GetBusinessCategories();
                this.GetStateCategories();
                this.GetMerchantDetails();
                this.GetMerchantSecurity();
            } else {
                this.Form_UpdateUser_Load();
                this.Get_UserAccountDetails();
                this.Form_EditPassword_Load();
                this.Form_UpdateContactUser_Load();
                this.Form_Edit_Load();
                this.Form_UpdateCredentialUser_Load();
                this.GetBusinessCategories();
                this.GetStateCategories();
                this.GetMerchantDetails();
                this.GetMerchantSecurity();

            }
        });
    }


    ResetImagePlaceholder: boolean = true
    ResetImage(): void {
        this.ResetImagePlaceholder = false;
        this._ChangeDetectorRef.detectChanges();
        this.ResetImagePlaceholder = true;
        this._ChangeDetectorRef.detectChanges();
    }

    private InitImagePickerAdd(InputFileComponent: InputFileComponent) {
        if (InputFileComponent != undefined) {
            this.CurrentImagesCount = 0;
            this._HelperService._InputFileComponent = InputFileComponent;
            InputFileComponent.onChange = (files: Array<InputFile>): void => {
                if (files.length >= this.CurrentImagesCount) {
                    this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
                }
                this.CurrentImagesCount = files.length;
            };
        }
    }

    //   private InitImagePicker(previewurl?: string) {
    //     if (this.InputFileComponent != undefined) {
    //       this.CurrentImagesCount = 0;
    //       if (previewurl) {
    //         this.InputFileComponent.files[0] = {};
    //         this.InputFileComponent.files[0].preview = previewurl;
    //       }
    //       this._HelperService._InputFileComponent = this.InputFileComponent;
    //       this.InputFileComponent.onChange = (files: Array<InputFile>): void => {
    //         if (files.length >= this.CurrentImagesCount) {
    //           this._HelperService._SetFirstImageOrNone(this.InputFileComponent.files);
    //         }
    //         this.CurrentImagesCount = files.length;
    //       };
    //     }
    //   }



    public ShowPasswordDiv: boolean = false;
    ShowPassword() {
        this.ShowPasswordDiv = !this.ShowPasswordDiv;
    }
    public _MerchantDetails: any =
        {
            // added 
            Address: null,
            // added
            ReferenceId: null,
            ReferenceKey: null,
            TypeCode: null,
            TypeName: null,
            SubTypeCode: null,
            SubTypeName: null,
            UserAccountKey: null,
            UserAccountDisplayName: null,
            Name: null,
            Description: null,
            StartDate: null,
            StartDateS: null,
            EndDate: null,
            EndDateS: null,
            SubTypeValue: null,
            MinimumInvoiceAmount: null,
            MaximumInvoiceAmount: null,
            MinimumRewardAmount: null,
            MaximumRewardAmount: null,
            ManagerKey: null,
            ManagerDisplayName: null,
            SmsText: null,
            Comment: null,
            CreateDate: null,
            CreatedByKey: null,
            CreatedByDisplayName: null,
            ModifyDate: null,
            ModifyByKey: null,
            ModifyByDisplayName: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
            Latitude: null,
            Longitude: null,
            CreateDateS: null,
            ModifyDateS: null,
            StatusI: null,
            StatusB: null,
            StatusC: null,

        }


    public isReadOnly = true;
    substring: any;
    EditPassword() {
        if (this.isReadOnly == true) {
            this.isReadOnly = false;
        }
        else {
            this.isReadOnly = true;
        }
    }
    public Get_UserAccountDetails() {
        this.isAddressLoaded = false;
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccount,
            Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveMerchantReferenceKey, '='),
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    let data = _Response.Result;
                    if (data.ContactNumber) {
                        let contact_no = data.ContactNumber;
                        if (contact_no.startsWith("234") || contact_no.startsWith("233") || contact_no.startsWith("255") || contact_no.startsWith("254")) {
                            this.substring = contact_no.slice(3);
                            data.ContactNumber = this.substring;
                        }

                    }

                    // this._HelperService._UserAccount = _Response.Result as OUserDetails;
                    this._HelperService._UserAccount = data as OUserDetails;
                    this._HelperService._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(this._HelperService._UserAccount.CreateDate);
                    this._HelperService._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(this._HelperService._UserAccount.ModifyDate);
                    this._HelperService._UserAccount.StatusI = this._HelperService.GetStatusIcon(this._HelperService._UserAccount.StatusCode);
                    this._HelperService.IsFormProcessing = false;
                    //#region Patch Profile
                    if (this._HelperService._UserAccount.AddressComponent != undefined) {
                        this._Address = this._HelperService._UserAccount.AddressComponent;
                    }
                    //#endregion
                    this.isAddressLoaded = true;
                }
                else {
                    this._HelperService.IsFormProcessing = false;

                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {

                this._HelperService.HandleException(_Error);
            });
    }

    //#region User
    Form_UpdateUser: FormGroup;
    Form_UpdateUser_Show() {
        this.Form_UpdateUser_Show();
        this._HelperService.Icon_Crop_Clear();
        this._HelperService.OpenModal('_Icon_Cropper_Modal');
        setTimeout(() => {
            this.InitImagePickerAdd(this.InputFileComponent_Add);
        }, 100);
    }
    Form_UpdateUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreUsers]);
    }
    Form_UpdateUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;
        this._HelperService.Icon_Crop_Clear();
        this.InitImagePickerAdd(this.InputFileComponent_Add);
        this.Form_UpdateUser = this._FormBuilder.group({
            Task: this._HelperService.AppConfig.Api.Core.updatemerchantdetails,
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            OwnerName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256), Validators.pattern("^[a-zA-Z _-]*$")])],
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(30)])],
            ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.pattern(this._HelperService.AppConfig.EmailPattern), Validators.minLength(2)])],
            GenderCode: this._HelperService.AppConfig.Gender.Male,
            DateOfBirth: null,
            WebsiteUrl: [null, Validators.compose([Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.WebsiteUrl1)])],
            Description: null,
            Address: null,
            IconContent: this._HelperService._Icon_Cropper_Data
            // ReferralCode: null,
            // ReferralUrl: null,

        });
    }
    Form_UpdateUser_Clear() {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_UpdateUser_Load();
    }

    DoesCatAlreadyExist(Id: number): boolean {
        for (let index = 0; index < this._MerchantDetails.UpdateCategories.length; index++) {
            const element = this._MerchantDetails.UpdateCategories[index];
            if (element.ReferenceId == Id) {
                return true;
            }
        }
        return false;
    }

    removeCategory(cat: any): void {
        this._MerchantDetails.UpdateCategories.splice(cat, 1);
    }

    removeCategory1(): void {
        this._MerchantDetails.UpdateCategories.splice(1);
    }

    Form_UpdateUser_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Primary,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                // _FormValue.AuthPin = result.value;
                _FormValue.Categories = [];
                if (this.SelectedBusinessCategories != undefined && this.SelectedBusinessCategories.length > 0) {
                    this.SelectedBusinessCategories.forEach(element => {
                        if (!this.DoesCatAlreadyExist(element)) {
                            _FormValue.Categories.push(
                                {
                                    ReferenceId: element
                                }
                            )
                        }
                    });
                }
                var IconContent: any = undefined;
                if (this._HelperService._Icon_Cropper_Data.Content != null) {
                    IconContent = this._HelperService._Icon_Cropper_Data;
                }
                _FormValue.IconContent = IconContent;
                var Request = this.CreateRequestJson();
                Request.Categories = Request.Categories.concat(this._MerchantDetails.UpdateCategories);
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, Request);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.FlashSwalSuccess('Changes Updated', 'Done! Changes Updated Successfully');
                            this.GetMerchantDetails();
                            this._HelperService.Icon_Crop_Clear();
                            this.InputFileComponent_Add = null;
                            this.ResetImage();
                            this._HelperService._FileSelect_Icon_Reset();


                        }
                        else {
                            this._HelperService.NotifyError('Unable to update account details');
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }
    public BusinessCategories = [];
    public S2BusinessCategories = [];

    GetBusinessCategories() {

        this._HelperService.ToggleField = true;
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Core.getcategories,
            SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
            SortExpression: 'Name asc',
            Offset: 0,
            Limit: 1000,
        }
        PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
            PData.SearchCondition,
            "TypeCode",
            this._HelperService.AppConfig.DataType.Text,
            this._HelperService.AppConfig.HelperTypes.MerchantCategories,
            "="
        );
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Op, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        this.BusinessCategories = _Response.Result.Data;

                        this.ShowCategorySelector = false;
                        this._ChangeDetectorRef.detectChanges();
                        for (let index = 0; index < this.BusinessCategories.length; index++) {
                            const element = this.BusinessCategories[index];
                            this.S2BusinessCategories.push(
                                {
                                    id: element.ReferenceId,
                                    key: element.ReferenceKey,
                                    text: element.Name
                                }
                            );
                        }
                        this.ShowCategorySelector = true;
                        this._ChangeDetectorRef.detectChanges();

                        this._HelperService.ToggleField = false;

                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                this._HelperService.ToggleField = false;

            });
    }
    public SelectedBusinessCategories = [];
    CategoriesSelected(Items) {
        if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
            this.SelectedBusinessCategories = Items.value;
        }
        else {
            this.SelectedBusinessCategories = [];
        }



    }
    SaveMerchantBusinessCategory(item) {
        if (item != '0') {
            var Setup =
            {
                Task: this._HelperService.AppConfig.Api.Core.SaveUserParameter,
                TypeCode: this._HelperService.AppConfig.HelperTypes.MerchantCategories,
                // UserAccountKey: this._UserAccount.ReferenceKey,
                CommonKey: item,
                StatusCode: 'default.active'
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, Setup);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess('Business category assigned to merchant');
                        // this.BusinessCategories = [];
                        // this.GetMerchantBusinessCategories();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    //#endregion

    //#region Credential
    Form_UpdateCredentialUser: FormGroup;
    Form_UpdateCredentialUser_Show() {
    }
    Form_UpdateCredentialUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreUsers]);
    }
    Form_UpdateCredentialUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_UpdateCredentialUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
            // AccountTypeCode: this._HelperService.AppConfig.AccountType.Admin,
            // AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Online,
            // RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
            OwnerKey: null,
            RoleKey: null,
            UserName: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(256)])],
            Description: null,
            ReferralCode: null,
            ReferralUrl: null,

            CountValue: 0,
            AverageValue: 0,

            ApplicationStatusCode: null,

            EmailVerificationStatus: 0,
            EmailVerificationStatusDate: null,

            NumberVerificationStatus: 0,
            NumberVerificationStatusDate: null,

            CountryKey: null,
            RegionKey: null,
            RegionAreaKey: null,
            CityKey: null,
            CityAreaKey: null,

            IconContent: this._HelperService._FileSelect_Icon_Data,
            PosterContent: this._HelperService._FileSelect_Poster_Data,

            Owners: [],
            Configuration: [],
        });
    }
    Form_UpdateCredentialUser_Clear() {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_UpdateCredentialUser_Load();
    }
    Form_UpdateCredentialUser_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Primary,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                // _FormValue.AuthPin = result.value;
                _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this.GetMerchantDetails();
                            this._HelperService.NotifySuccess('Account details updated');
                        }
                        else {
                            this._HelperService.NotifyError('Unable to udpate account details');
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }
    //#endregion


    removeImage(): void {
        this.CurrentImagesCount = 0;
        this._HelperService.Icon_Crop_Clear();
        var Req = this.Approve_RequestBody();
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Profile, Req);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess("Business Logo Removed Successfully");

                }
                else {

                    this._HelperService.NotifyError("Business Logo Not Found");
                }
            },
            _Error => {

                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }


    Approve_RequestBody(): void {
        var formRequest: any = {
            'OperationType': 'new',
            'Task': 'removeprofileimage',
            //'ReferenceKey': this.SelectedDeal.ReferenceKey,
            // 'ReferenceId': this.SelectedDeal.ReferenceId,
            "ReferenceKey": this._HelperService._UserAccount.ReferenceKey,
            'ReferenceId': this._HelperService._UserAccount.ReferenceId,
            // 'IconUrl':'https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/o/2021/12/51da6698fc8044b1a2d4ed78aee6f2f8.png'


        };
        //#region Set Schedule 

        //#endregion

        return formRequest;

    }

    //#region Contact
    Form_UpdateContactUser: FormGroup;
    Form_UpdateContactUser_Show() {
    }
    Form_UpdateContactUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreUsers]);
    }
    Form_UpdateContactUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_UpdateContactUser = this._FormBuilder.group({
            // Task: this._HelperService.AppConfig.Api.Core.updatemerchantdetails,
            FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128), Validators.pattern("^[a-zA-Z _-]*$")])],
            LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128), Validators.pattern("^[a-zA-Z _-]*$")])],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'), Validators.minLength(2)])],
        });
    }
    Form_UpdateContactUser_Clear() {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_UpdateContactUser_Load();
    }
    Form_UpdateContactUser_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Primary,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                // _FormValue.AuthPin = result.value;

                var Request = this.CreateRequestJson();

                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, Request);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.FlashSwalSuccess('Changes Updated', 'Done! Changes Updated Successfully');
                            this.GetMerchantDetails();
                        }
                        else {
                            this._HelperService.NotifyError('Unable to udpate account details');
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }
    //#endregion 

    //#region Map
    _CurrentAddress: any = {};
    _CurrentAddressStore: any = {};




    //#endregion

    //#region Address



    Form_UpdateAddressUser_Process() {
        if (!this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "") {
            this._HelperService.NotifyError('Please select business location');
        }
        else {
            swal({
                position: 'top',
                title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
                // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
                animation: false,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                showCancelButton: true,
                confirmButtonColor: this._HelperService.AppConfig.Color_Primary,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            }).then((result) => {
                if (result.value) {
                    // _FormValue.AuthPin = result.value;
                    var Request = this.CreateRequestJson();
                    // _FormValue.Location = this._Address;
                    this._HelperService.IsFormProcessing = true;
                    this.SaveAccountRequest = this.ReFormat_RequestBody();
                    //Location Manager - Start
                    this.SaveAccountRequest.Address = this._Address.Address;
                    this.SaveAccountRequest.AddressComponent = this._Address;
                    //Location Manager - End
                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, Request);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.FlashSwalSuccess('Changes Updated', 'Done! Changes Updated Successfully');
                                this.GetMerchantDetails();
                            }
                            else {
                                this._HelperService.NotifyError('Unable to udpate account details');
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        });
                }
            });

        }

    }
    //#endregion

    //#region password
    ShowImagePicker: boolean = true;

    Form_EditPassword: FormGroup;
    Form_EditPassword_Load() {
        this.Form_EditPassword = this._FormBuilder.group({
            OperationType: 'new',
            ReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccountPassword,
            OldPassword: [null, Validators.required],
            Password: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.Password)])],
        });
    }
    Form_EditPassword_Clear() {
        this.Form_EditPassword.reset();
    }
    Form_EditPassword_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: 'Update password',
            // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Primary,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;
                // _FormValue.AuthPin = result.value;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess('Password updated');
                            this.Form_EditPassword_Clear();
                        }
                        else {
                            this._HelperService.NotifyError('Enter valid old password');
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }
    //#endregion


    CreateRequestJson(): any {

        var AccountDetail = this.Form_UpdateUser.value;
        // AccountDetail.EmailAddress = this.Form_UpdateUser['EmailAddress'].value;

        // var AddressDetail = this.Form_UpdateAddressUser.value;
        AccountDetail.Address = this._Address.Address
        AccountDetail.AddressComponent = this._Address;;
        //     AccountDetail.CountryName= this._HelperService.UserCountrycode;
        //     AccountDetail.CityId= this.cityid;
        //     AccountDetail.CityCode= this.citykey;
        //     AccountDetail.CityName= this.cityname;
        //    // CityAreaId: null,
        //    // CityAreaCode: null,
        //    // CityAreaName: null,
        //    AccountDetail.StateId= this.stateid;
        //    AccountDetail.StateCode= this.statekey;
        //    AccountDetail.StateName= this.statename;
        var ContactPersonDetail = this.Form_UpdateContactUser.value;
        AccountDetail.ContactPerson = ContactPersonDetail;
        var IconContent: any = undefined;
        if (this._HelperService._Icon_Cropper_Data.Content != null) {
            IconContent = this._HelperService._Icon_Cropper_Data;
        }
        // AccountDetail.IconContent = IconContent;
        // AccountDetail.IconContent =  this._HelperService._Icon_Cropper_Data;
        // AccountDetail.IconContent.Content=this.croppedImage1;
        AccountDetail.IconContent = IconContent;
        AccountDetail.AccountKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;
        AccountDetail.AccountId = this._HelperService.AppConfig.ActiveMerchantReferenceId;
        return AccountDetail;


    }

    Form_Edit: FormGroup;
    Form_Edit_Load() {
        this.Form_Edit = this._FormBuilder.group({
            OperationType: 'new',
            ReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccountAccessPin,
            OldAccessPin: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)])],
            AccessPin: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)])],
        });
    }
    Form_Edit_Clear() {
        this.Form_Edit.reset();
    }
    Form_Edit_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Primary,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;
                // _FormValue.AuthPin = result.value;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess('Password updated');
                            this.Form_Edit_Clear();
                        }
                        else {
                            this._HelperService.NotifyError('Enter valid old password');
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }

    ShowSuccessModal(): void {
        this._HelperService.OpenModal('');
    }
    toogleIsFormProcessing(value: boolean): void {
        this._HelperService.IsFormProcessing = value;
        //    this._ChangeDetectorRef.detectChanges();
    }
    public ContactPerson: any = {}
    public AddressDetails: any = {}
    public updateNumber: any;
    public imgmerchanturl: any;
    GetMerchantDetails() {
        this.isAddressLoaded = false;
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchant,
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.toogleIsFormProcessing(false);
                    this._MerchantDetails = _Response.Result;
                    this.ContactPerson = this._MerchantDetails.ContactPerson;
                    // this.AddressDetails = this._MerchantDetails.Address;
                    this._Address = this._MerchantDetails.AddressComponent;
                    this.isAddressLoaded = true;
                    if (this.ContactPerson != undefined && this.ContactPerson.MobileNumber != undefined && this.ContactPerson.MobileNumber != null) {
                        if (this.ContactPerson.MobileNumber.startsWith("234") || this.ContactPerson.MobileNumber.startsWith("233") || this.ContactPerson.MobileNumber.startsWith("254")) {
                            this.ContactPerson.MobileNumber = this.ContactPerson.MobileNumber.substring(3, this.ContactPerson.length);
                        }
                    }

                    this.InitImagePicker(this._MerchantDetails.IconUrl);
                    this.imgmerchanturl = this._MerchantDetails.IconUrl
                    this.checkimg();
                    this._MerchantDetails.UpdateCategories = [];
                    for (let index = 0; index < this._MerchantDetails.Categories.length; index++) {
                        const element = this._MerchantDetails.Categories[index];
                        this._MerchantDetails.UpdateCategories.push({
                            Name: element.Name,
                            ReferenceKey: element.ReferenceKey,
                            ReferenceId: element.ReferenceId
                        });

                    }


                    //#region RelocateMarker 

                    if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
                        this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
                        this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
                    } else {
                        this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
                        this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
                    }


                    //#endregion

                    //#region DatesAndStatusInit 

                    this._MerchantDetails.StartDateS = this._HelperService.GetDateS(
                        this._MerchantDetails.StartDate
                    );
                    this._MerchantDetails.EndDateS = this._HelperService.GetDateS(
                        this._MerchantDetails.EndDate
                    );
                    this._MerchantDetails.CreateDateS = this._HelperService.GetDateTimeS(
                        this._MerchantDetails.CreateDate
                    );
                    this._MerchantDetails.ModifyDateS = this._HelperService.GetDateTimeS(
                        this._MerchantDetails.ModifyDate
                    );
                    this._MerchantDetails.StatusI = this._HelperService.GetStatusIcon(
                        this._MerchantDetails.StatusCode
                    );
                    this._MerchantDetails.StatusB = this._HelperService.GetStatusBadge(
                        this._MerchantDetails.StatusCode
                    );
                    this._MerchantDetails.StatusC = this._HelperService.GetStatusColor(
                        this._MerchantDetails.StatusCode
                    );


                    //#endregion

                    this._ChangeDetectorRef.detectChanges();
                }
                else {
                    this.toogleIsFormProcessing(false);
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }


    RouterMerchant() {

        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.RewardPercentage,
            this._MerchantDetails.ReferenceKey,
            this._MerchantDetails.ReferenceId,
        ]);
    }




    public Password = null;
    ResetPassword() {
        swal({
            title: 'Are You Sure You Want To Reset Your Password',
            text: 'Click Continue To Reset Your Password',
            showCancelButton: true,
            position: this._HelperService.AppConfig.Alert_Position,
            animation: this._HelperService.AppConfig.Alert_AllowAnimation,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
            confirmButtonColor: this._HelperService.AppConfig.Color_Primary,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            input: 'text',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputClass: 'swalText',
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var pData = {
                    Task: 'resetmerchantpassword',
                    AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
                    AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
                    AuthPin: result.value
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this.Password = _Response.Result;
                            this.GetMerchantDetails();
                            this._HelperService.NotifySuccess("Password Reset Successfully");
                            this.ShowPasswordDiv = true
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {

                        this._HelperService.HandleException(_Error);
                    });
            }
        });
    }

    MerchantSecurity: any = {};
    GetMerchantSecurity() {
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: 'getconfigurations',
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            "Limit": 100,
            "RefreshCount": true,
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Operations, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.toogleIsFormProcessing(false);
                    this.MerchantSecurity = _Response.Result.Data;
                    if (this.MerchantSecurity) {


                        var maxamount: any = this.MerchantSecurity.find(x => x['ConfigurationSystemName'] == 'securitymaximuminvoiceamountpertransactioncap');
                        if (maxamount) {
                            this.Configuration.MaxAmount = maxamount.Value;

                        }


                        var MaxTransaction: any = this.MerchantSecurity.find(x => x['ConfigurationSystemName'] == 'securitymaximumtransactionperdaycap');
                        if (MaxTransaction) {

                            this.Configuration.MaxTransaction = MaxTransaction.Value;
                        }

                        var maxamountpercustomer: any = this.MerchantSecurity.find(x => x['ConfigurationSystemName'] == 'securitymaximuminvoiceamountperdaycap');
                        if (maxamountpercustomer) {
                            this.Configuration.MaxAmountPerCustomer = maxamountpercustomer.Value;

                        }

                        var NotificationEmail: any = this.MerchantSecurity.find(x => x['ConfigurationSystemName'] == 'securitynotificationemails');
                        if (NotificationEmail) {
                            this.Configuration.EmailAddress = NotificationEmail.Value;

                        }


                    }


                    this._ChangeDetectorRef.detectChanges();
                }
                else {
                    this.toogleIsFormProcessing(false);
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    isValidEmail: boolean = true;
    Emails: string[] = [];
    validEmails = [];
    KeyUP(event): void {
        // this.Emails = this.Configuration.EmailAddress.split(';');
        // if(event.target.value){
        //     if (event.target.value.match(this._HelperService.AppConfig.EmailPattern)) {
        //         this.isValidEmail = true;
        //     }
        //     else{
        //         this.isValidEmail = false;
        //     }
        // }

        // let str = "abc@abc.com;abc@abc.com; abc@a@bc.com ; abc@abc.com ;abc@abc.com;"
        let str = event.target.value
        let emails = str.split(';');
        let invalidEmails = [];

        for (let i = 0; i < emails.length; i++) {
            if (!this.validateEmail(emails[i].trim())) {
                invalidEmails.push(emails[i].trim());

            }
            else if (this.validateEmail(emails[i].trim())) {
                this.validEmails.push(emails[i].trim())
            }
        }
        if (invalidEmails.length >= 1) {
            this.isValidEmail = false;
        }
        else {
            this.isValidEmail = true;
        }

    }


    validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        // let re =this._HelperService.AppConfig.EmailPattern
        return re.test(email);
    }

    Configuration: any = {
        MaxAmount: null,
        MaxTransaction: null,
        MaxAmountPerCustomer: null,
        EmailAddress: null


    }
    Security_Update(ConfigurationKey, value) {
        // console.log(ConfigurationKey);
        var data = []
        data.push({
            ConfigurationKey: ConfigurationKey,
            Value: value
        })
        if ((value == null || value == undefined || value == "") && ConfigurationKey == "securitymaximuminvoiceamountpertransactioncap") {
            this._HelperService.NotifyError(
                "Please Enter Configuration Value"
            );
        }
        else if ((value == null || value == undefined || value == "") && ConfigurationKey == "securitymaximumtransactionperdaycap") {
            this._HelperService.NotifyError(
                "Please Enter Maximum Transaction Value"
            );
        }
        else if ((value == null || value == undefined || value == "") && ConfigurationKey == "securitymaximuminvoiceamountperdaycap") {
            this._HelperService.NotifyError(
                "Please Enter Maximum Invoice Amount"
            );
        }
        else if ((value == null || value == undefined || value == "") && ConfigurationKey == "securitynotificationemails") {
            this._HelperService.NotifyError(
                "Please Enter Valid  Email Address"
            );
        }
        else {
            swal({
                title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
                // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
                position: this._HelperService.AppConfig.Alert_Position,
                animation: this._HelperService.AppConfig.Alert_AllowAnimation,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
                allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
                confirmButtonColor: this._HelperService.AppConfig.Color_Primary,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
                showCancelButton: true,
                // input: 'password',
                // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
                // inputAttributes: {
                //     autocapitalize: 'off',
                //     autocorrect: 'off',
                //     maxLength: "4",
                //     minLength: "4"
                // },
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveconfigurations",
                        AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
                        AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
                        Items: data,
                    };

                    let _OResponse: Observable<OResponse>;

                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Operations, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                value = null;
                                this.Emails = [];
                                this.Configuration.MaxAmount = null
                                this.Configuration.MaxAmountPerCustomer = null
                                this.Configuration.MaxTransaction = null
                                this.Configuration.EmailAddress = null
                                this.GetMerchantSecurity();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }

                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });

        }








    }

    //image code---
    imageChangedEvent: any = '';
    croppedImage: any = '';
    croppedImage1: any = ''

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }
    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage = event.base64;
        this.croppedImage1 = this.croppedImage.replace("data:image/png;base64,", "")
    }
    imageLoaded() {
        // show cropper
    }
    cropperReady() {
        // cropper ready
    }
    loadImageFailed() {
        // show message
    }

    onImgError(event) {
        event.target.src = this._MerchantDetails.IconUrl;
    }

    Icon_Crop_Clear() {
        this.croppedImage1 = '';
        this.croppedImage = '';
        this._MerchantDetails.IconUrl = "https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/defaults/defaulticon.png";
    }
    chekimgcon: boolean = false
    chekimgcon1: boolean = false
    checkimg() {
        if (this._MerchantDetails.IconUrl == "https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/defaults/defaulticon.png") {
            this.chekimgcon = true;
        }
        else {
            this.chekimgcon = false;
        }

    }
    //end code---


    public ShowstateSelector: boolean = true;
    public ShowcitySelector: boolean = true;
    //state
    public StateCategories = [];
    public S2StateCategories = [];

    GetStateCategories() {

        this._HelperService.ToggleField = true;
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Core.getstates,
            ReferenceKey: this._HelperService.UserCountrykey,
            ReferenceId: this._HelperService.UserCountryId,
            Offset: 0,
            Limit: 1000,
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.State, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        this.StateCategories = _Response.Result.Data;

                        this.ShowstateSelector = false;
                        this._ChangeDetectorRef.detectChanges();
                        this.S2StateCategories.push(
                            {
                                id: 0,
                                key: "0",
                                text: "Select State"
                            }
                        );
                        for (let index = 0; index < this.StateCategories.length; index++) {
                            const element = this.StateCategories[index];
                            this.S2StateCategories.push(
                                {
                                    id: element.ReferenceId,
                                    key: element.ReferenceKey,
                                    text: element.Name
                                }
                            );
                        }
                        this.ShowstateSelector = true;
                        this._ChangeDetectorRef.detectChanges();

                        this._HelperService.ToggleField = false;

                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                this._HelperService.ToggleField = false;

            });
    }



    //City
    ReFormat_RequestBody(): void {
        var formValue: any = this.Form_UpdateContactUser.value;
        var formRequest: any = {
            OperationType: 'new',
            Task: formValue.Task,
            DisplayName: formValue.DisplayName,
            Name: formValue.Name,
            ContactNumber: formValue.ContactNumber,
            EmailAddress: formValue.EmailAddress,
            RewardPercentage: formValue.RewardPercentage,
            WebsiteUrl: formValue.WebsiteUrl,
            ReferralCode: formValue.ReferralCode,
            Description: formValue.Description,
            UserName: formValue.UserName,
            Password: formValue.Password,
            StatusCode: formValue.StatusCode,
            Address: this._Address.Address,
            AddressComponent: this._Address,
            ContactPerson: {
                FirstName: formValue.FirstName,
                LastName: formValue.LastName,
                MobileNumber: formValue.MobileNumber,
                EmailAddress: formValue.EmailAddress
            },
            IconContent: formValue.IconContent
        };
        return formRequest;
    }
}
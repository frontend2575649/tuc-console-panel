import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs";
import { DataHelperService, HelperService, OResponse } from "../../../service/service";
declare var moment: any;

@Component({
  selector: "tumerchantterminallive",
  templateUrl: "./tumerchantterminallive.component.html",
  styles: [
    `
      agm-map {
        height: 300px;
      }
    `
  ]
})
export class TUMerchantTerminalLiveComponent implements OnInit {
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) {
    this._HelperService.AppConfig.ShowMenu = false;
    this._HelperService.AppConfig.ShowHeader = false;
    this._HelperService.ContainerHeight = window.innerHeight;

    this.todaysdate = this._HelperService.GetDateS(new Date());
    setInterval(() => {
      this.now = Date.now();
    }, 1);
  }
  public GenderChart = [];

  ngOnInit() {
    this._HelperService.FullContainer = true;
    this.LoadData();
    setInterval(() => {
      this.LoadData();
    }, 60000);
    this.startTimer();
  }
  public todaysdate: string = null;
  public now: number;
  timeLeft: number = 60;
  interval;
  StartTime = null;
  EndTime = null;
  startTimer() {
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.timeLeft = 60;
      }
    }, 1000);
  }

  LoadData() {
    if (this.StartTime == undefined) {
      this.StartTime = moment().startOf("day");
      this.EndTime = moment().endOf("day");
    }
    this.GetPosTerminalOverviewList();
    this.GetPosTerminalsStatusOverview();
    this.GetPosTerminalsByTransactionsAsc();
  }

  public PosTerminalsByTransactions = {
    TotalRecords: 0,
    Data: [],
    Offset: 0,
    Limit: 0
  };
  GetPosTerminalOverviewList() {
    this.PosTerminalsByTransactions = {
      TotalRecords: 0,
      Data: [],
      Offset: 0,
      Limit: 0
    };
    this._HelperService.IsFormProcessing = true;
    var SearchCondition = "";
    if (
      this._HelperService.UserAccount.AccountTypeCode == this._HelperService.AppConfig.AccountType.Merchant ||
      this._HelperService.UserAccount.AccountTypeCode == this._HelperService.AppConfig.AccountType.MerchantSubAccount
    ) {
      SearchCondition = this._HelperService.GetSearchConditionStrict(
        "",
        "MerchantKey",
        "text",
        this._HelperService.AppConfig.ActiveOwnerKey,
        "="
      );
    } else if (
      this._HelperService.UserAccount.AccountTypeCode ==
      this._HelperService.AppConfig.AccountType.Acquirer ||
      this._HelperService.UserAccount.AccountTypeCode ==
      this._HelperService.AppConfig.AccountType.AcquirerSubAccount
    ) {
      SearchCondition = this._HelperService.GetSearchConditionStrict(
        "",
        "AcquirerKey",
        "text",
        this._HelperService.AppConfig.ActiveOwnerKey,
        "="
      );
    } else if (
      this._HelperService.UserAccount.AccountTypeCode ==
      this._HelperService.AppConfig.AccountType.Store ||
      this._HelperService.UserAccount.AccountTypeCode ==
      this._HelperService.AppConfig.AccountType.StoreSubAccount
    ) {
      SearchCondition = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreKey",
        "text",
        this._HelperService.AppConfig.ActiveOwnerKey,
        "="
      );
    } else {
      SearchCondition = this._HelperService.GetSearchConditionStrict(
        "",
        "MerchantKey",
        "text",
        this._HelperService.AppConfig.ActiveOwnerKey,
        "="
      );
    }
    // SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'Transactions', 'text', 0, '=');
    var Data = {
      Task: "getposterminalsoverviewlist",
      StartDate: this.StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
      EndDate: this.EndTime, // moment().add(2, 'days'),
      // StartDate: new Date(2017, 0, 1, 0, 0, 0, 0),
      // EndDate: moment().add(2, 'days'),
      Type: "subowner",
      SortExpression: "LastTransactionDate desc",
      SearchCondition: SearchCondition,
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
      Data
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.PosTerminalsByTransactions = _Response.Result;
          if (this.PosTerminalsByTransactions != undefined) {
            this.PosTerminalsByTransactions.Data.forEach(element => {
              element.LastTransactionDateD = this._HelperService.GetDateS(
                element.LastTransactionDate
              );
              element.LastTransactionDateT = this._HelperService.GetTimeS(
                element.LastTransactionDate
              );
              if (element.LastTransactionDate != "0001-01-01 00:00:00.000000") {
                element.LastTransactionDateDiffS = this._HelperService.GetTimeDifferenceS(
                  element.LastTransactionDate,
                  moment()
                );
                element.LastTransactionDateDiff = this._HelperService.GetTimeDifference(
                  element.LastTransactionDate,
                  moment()
                );
              }
              element.LastTransactionDate = this._HelperService.GetDateTimeS(
                element.LastTransactionDate
              );
            });
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  Overview = null;
  GetPosTerminalsStatusOverview() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: "getposterminalsstatusoverview",
      StartTime: this.StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
      EndTime: this.EndTime, // moment().add(2, 'days'),
      UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
      Data
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverview = _Response.Result as OAccountOverview;
          if (this._AccountOverview.PosOverview != undefined) {
            this._AccountOverview.PosOverview.forEach(element => {
              element.LastTransactionDateD = this._HelperService.GetDateS(
                element.LastTransactionDate
              );
              element.LastTransactionDateT = this._HelperService.GetTimeS(
                element.LastTransactionDate
              );
              element.LastTransactionDate = this._HelperService.GetDateTimeS(
                element.LastTransactionDate
              );
            });
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  public _AccountOverview: OAccountOverview = {
    AppUsers: 0,
    CardRewardPurchaseAmount: 0,
    CashRewardPurchaseAmount: 0,
    IssuerCommissionAmount: 0,
    OtherRewardPurchaseAmount: 0,
    PurchaseAmount: 0,
    RedeemAmount: 0,
    RepeatingAppUsers: 0,
    RewardAmount: 0,
    RewardChargeAmount: 0,
    TUCPlusRewardAmount: 0,
    TUCPlusRewardChargeAmount: 0,
    TUCPlusRewardClaimedAmount: 0,
    Transactions: 0,
    UniqueAppUsers: 0,
    PosOverview: []
  };
  public PosTerminalsByTransactionsAsc = {
    TotalRecords: 0,
    Data: [],
    Offset: 0,
    Limit: 0
  };
  GetPosTerminalsByTransactionsAsc() {
    this.PosTerminalsByTransactions = {
      TotalRecords: 0,
      Data: [],
      Offset: 0,
      Limit: 0
    };
    this._HelperService.IsFormProcessing = true;
    // SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'MerchantKey', 'text', this._HelperService.AppConfig.ActiveOwnerKey, '=');
    var SearchCondition = "";
    if (
      this._HelperService.UserAccount.AccountTypeCode ==
      this._HelperService.AppConfig.AccountType.Merchant ||
      this._HelperService.UserAccount.AccountTypeCode ==
      this._HelperService.AppConfig.AccountType.MerchantSubAccount
    ) {
      SearchCondition = this._HelperService.GetSearchConditionStrict(
        "",
        "MerchantKey",
        "text",
        this._HelperService.AppConfig.ActiveOwnerKey,
        "="
      );
    } else if (
      this._HelperService.UserAccount.AccountTypeCode ==
      this._HelperService.AppConfig.AccountType.Acquirer ||
      this._HelperService.UserAccount.AccountTypeCode ==
      this._HelperService.AppConfig.AccountType.AcquirerSubAccount
    ) {
      SearchCondition = this._HelperService.GetSearchConditionStrict(
        "",
        "AcquirerKey",
        "text",
        this._HelperService.AppConfig.ActiveOwnerKey,
        "="
      );
    } else if (
      this._HelperService.UserAccount.AccountTypeCode ==
      this._HelperService.AppConfig.AccountType.Store ||
      this._HelperService.UserAccount.AccountTypeCode ==
      this._HelperService.AppConfig.AccountType.StoreSubAccount
    ) {
      SearchCondition = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreKey",
        "text",
        this._HelperService.AppConfig.ActiveOwnerKey,
        "="
      );
    } else {
      SearchCondition = this._HelperService.GetSearchConditionStrict(
        "",
        "MerchantKey",
        "text",
        this._HelperService.AppConfig.ActiveOwnerKey,
        "="
      );
    }
    SearchCondition = this._HelperService.GetSearchConditionStrict(
      SearchCondition,
      "Transactions",
      "text",
      "0",
      "="
    );

    var Data = {
      Task: "getposterminalsoverviewlist",
      StartDate: this.StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
      EndDate: this.EndTime, // moment().add(2, 'days'),
      // StartDate: new Date(2017, 0, 1, 0, 0, 0, 0),
      // EndDate: moment().add(2, 'days'),
      Type: "subowner",
      SortExpression: "LastTransactionDate asc",
      SearchCondition: SearchCondition, //this._HelperService.GetSearchConditionStrict('', 'MerchantKey', 'text', this._HelperService.AppConfig.ActiveOwnerKey, '='),
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
      Data
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.PosTerminalsByTransactionsAsc = _Response.Result;
          if (this.PosTerminalsByTransactionsAsc != undefined) {
            this.PosTerminalsByTransactionsAsc.Data.forEach(element => {
              element.LastTransactionDateD = this._HelperService.GetDateS(
                element.LastTransactionDate
              );
              element.LastTransactionDateT = this._HelperService.GetTimeS(
                element.LastTransactionDate
              );
              if (element.LastTransactionDate != "0001-01-01 00:00:00.000000") {
                element.LastTransactionDateDiff = this._HelperService.GetTimeDifferenceS(
                  element.LastTransactionDate,
                  moment()
                );
              }
              element.LastTransactionDate = this._HelperService.GetDateTimeS(
                element.LastTransactionDate
              );
            });
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
}

export class OAccountOverview {
  public AppUsers: number;
  public RepeatingAppUsers: number;
  public UniqueAppUsers: number;
  public RewardChargeAmount: number;
  public TUCPlusRewardChargeAmount: number;
  public Transactions: number;
  public PurchaseAmount: number;
  public IssuerCommissionAmount: number;
  public CashRewardPurchaseAmount: number;
  public CardRewardPurchaseAmount: number;
  public OtherRewardPurchaseAmount: number;
  public RedeemAmount: number;
  public RewardAmount: number;
  public TUCPlusRewardAmount: number;
  public TUCPlusRewardClaimedAmount: number;
  public PosOverview: any[];
}

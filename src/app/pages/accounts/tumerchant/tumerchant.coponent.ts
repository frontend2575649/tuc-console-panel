import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { DataHelperService, HelperService } from "../../../service/service";

@Component({
  selector: "tu-merchant",
  templateUrl: "./tumerchant.component.html"
})
export class TUMerchantComponent implements OnInit {
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) { }
  ngOnInit() {
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = this._HelperService.AppConfig.AccountType.Merchant;
    this._HelperService.ContainerHeight = window.innerHeight;
  }

} 

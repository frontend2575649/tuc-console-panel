import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { TUMerchantComponent } from "./tumerchant.coponent";
const routes: Routes = [
  {
    path: "",
    component: TUMerchantComponent,
    children: [
      {
        path: "/:referencekey",
        data: {
          permission: "getmerchant",
          menuoperations: "ManageMerchant",
          accounttypecode: "merchant"
        },
        loadChildren: "./dashboard/tudashboard.module#TUMerchantDashboardModule"
      },
      {
        path: "dashboard/:referencekey",
        data: {
          permission: "getappuser",
          menuoperations: "ManageAppUser",
          accounttypecode: "merchant"
        },
        loadChildren: "./dashboard/tudashboard.module#TUMerchantDashboardModule"
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUMerchantRoutingModule { }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TUMerchantRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule
  ],
  declarations: [TUMerchantComponent]
})
export class TUMerchantModule { }

import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import * as XLSX from 'xlsx';
import { DataHelperService, HelperService, OResponse } from '../../../service/service';

type AOA = any[][];

@Component({
    selector: 'hc-merchantupload',
    templateUrl: './merchantupload.component.html',
})
export class TUMerchantUploadComponent implements OnInit {
    public SelectedDay = null;
    public SelectedMonth = null;
    public SelectedYear = null;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {

    }
    ngOnInit() {

    }
    IsUploading = false;
    UploadCount = 0;
    TotalCount = 0;
    CustomersList: CustomerImport[] = [];
    data: AOA = [];
    wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
    fileName: string = 'SheetJS.xlsx';
    onFileChange(evt: any) {
        this.UploadCount = 0;
        this.TotalCount = 0;
        this.CustomersList = [];
        this.IsUploading = false;
        const target: DataTransfer = <DataTransfer>(evt.target);
        if (target.files.length !== 1) throw new Error('Cannot use multiple files');
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
            const bstr: string = e.target.result;
            const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
            const wsname: string = wb.SheetNames[0];
            const ws: XLSX.WorkSheet = wb.Sheets[wsname];
            this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
            for (let index = 1; index < this.data.length; index++) {
                const CustomerInfo = this.data[index];
                var DisplayName = CustomerInfo[0];
                var CompanyName = CustomerInfo[1];
                var BusinessContactNumber = CustomerInfo[2];
                var BusinessEmailAddress = CustomerInfo[2];
                var BusinessType = CustomerInfo[2];
                var BusinessAddress = CustomerInfo[2];
                var ContactFirstName = CustomerInfo[2];
                var ContactLastName = CustomerInfo[2];
                var ContactMobileNumber = CustomerInfo[2];
                var RewardPercentage = CustomerInfo[2];
                if (BusinessType == "online") {
                    BusinessType = this._HelperService.AppConfig.AccountOperationType.Online
                }
                else if (BusinessType == "offline") {
                    BusinessType = this._HelperService.AppConfig.AccountOperationType.Offline
                }
                else {
                    BusinessType = this._HelperService.AppConfig.AccountOperationType.OnlineAndOffline
                }
                if (DisplayName != undefined && DisplayName != undefined) {
                    var AppUserInfo: CustomerImport =
                    {
                        BusinessAddress: BusinessAddress,
                        BusinessContactNumber: BusinessContactNumber,
                        BusinessEmailAddress: BusinessEmailAddress,
                        CompanyName: CompanyName,
                        BusinessType: BusinessType,
                        ContactFirstName: ContactFirstName,
                        ContactLastName: ContactLastName,
                        ContactMobileNumber: ContactMobileNumber,
                        DisplayName: DisplayName,
                        RewardPercentage: RewardPercentage,
                        Status: 'pending',
                        Message: '',
                    };
                    this.CustomersList.push(AppUserInfo);
                    this.TotalCount = this.TotalCount + 1;
                }
            }
        };
        reader.readAsBinaryString(target.files[0]);
    }
    export(): void {
        var Cus = this.CustomersList as any;
        const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(Cus);
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'ImportReport');
        XLSX.writeFile(wb, this.fileName);
    }
    onUpload(){
        this._HelperService.OpenModal("ModalShowFile");
    }
    onUploadFinish(){
        this._HelperService.OpenModal("ModalShowFileFinish");
    }
    Modal_Close(){
        this._HelperService.CloseModal("ModalShowFile");
    }
    Modal_Finish_Close(){
        this._HelperService.CloseModal("ModalShowFileFinish");
    }
    StartCustomerUpload() {
        if (this.CustomersList.length > 0) {
            this.IsUploading = true;
            for (let index = 0; index < this.CustomersList.length; index++) {
                const element = this.CustomersList[index];
                this.CustomersList[index].Status = "processing";
                this.CustomersList[index].Message = "sending data";
                var PostItem = {
                    OperationType: 'new',
                    Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,
                    AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
                    AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
                    RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
                    OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
                    DisplayName: element.DisplayName,
                    Name: element.CompanyName,
                    FirstName: element.ContactFirstName,
                    LastName: element.ContactLastName,
                    MobileNumber: element.ContactMobileNumber,
                    ContactNumber: element.BusinessContactNumber,
                    EmailAddress: element.BusinessEmailAddress,
                    Address: element.BusinessAddress,
                    Latitude: 0,
                    Longitude: 0,
                    RegionKey: '',
                    RegionAreaKey: '',
                    CityKey: '',
                    CityAreaKey: '',
                    PostalCodeKey: '',
                    CountValue: 0,
                    AverageValue: 0,
                    StatusCode: this._HelperService.AppConfig.Status.Inactive,
                    Configuration: [{
                        SystemName: 'rewardpercentage',
                        Value: element.RewardPercentage,
                    }],
                };
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, PostItem);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        this.UploadCount = this.UploadCount + 1;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this.CustomersList[index].Status = "success";
                            this.CustomersList[index].Message = "merchant added";
                        }
                        else {
                            this.CustomersList[index].Status = "failed";
                            this.CustomersList[index].Message = "merchant already present";
                        }
                        if (index == (this.CustomersList.length - 1)) {
                            this.IsUploading = false;
                            this._HelperService.NotifySuccess('Merchants import successfull');
                        }


                    },
                    _Error => {
                        this.UploadCount = this.UploadCount + 1;
                        if (index == (this.CustomersList.length - 1)) {
                            this.IsUploading = false;
                            this._HelperService.NotifySuccess('Merchants import successfull');
                        }
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        }
        else {
            this._HelperService.NotifyError('Select file to upload merchants');
        }

    }

}





export class CustomerImport {
    public DisplayName: string | null;
    public CompanyName: string | null;
    public BusinessContactNumber: string | null;
    public BusinessEmailAddress: string | null;
    public BusinessAddress: string | null;
    public BusinessType: string | null;
    public ContactFirstName: string | null;
    public ContactLastName: string | null;
    public ContactMobileNumber: string | null;
    public RewardPercentage: string | null;
    public Status: string | null;
    public Message: string | null;
}

import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { IframeItem } from '@ngx-gallery/core';
import * as Feather from 'feather-icons';
import { Select2OptionData } from 'ng2-select2';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable } from 'rxjs';
import { HCoreXAddress } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';
import swal from 'sweetalert2';
import { DataHelperService, HelperService, OResponse, OSelect, OMerchantOnboard } from '../../../service/service';

// import steps from "../../../../assets/js/jquery.steps.min.js"

declare var $: any;

@Component({
    selector: 'hc-merchantonboarding',
    templateUrl: './merchantonboarding.component.html',
})
export class TUMerchantOnboardingComponent implements OnInit {

    public ShowStoreAddressLocator = false;
    public _Address: HCoreXAddress = {};
    
    public _AddressStore: HCoreXAddress = {};
    resetAddressComponent:boolean=false;
    AddressChange(Address) {
        // debugger;
        this._Address = Address;
        
    }
    onAddresschange(Address) {
        // debugger;
        this._Address = Address;
       
       
        // console.log("this._AddressStore",this._AddressStore);
    }
    onStoreAddresschange(Address) {
        // debugger;
        this._AddressStore = Address;
        // console.log("this._address",this._Address);
        

    }
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {
        this._ContactInfo = true;
        this._StoreInfoCopy = false;
    }
    ngOnInit() {
        this.countrycode = this._HelperService.UserCountrycode
        $('#wizard1').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            enablePagination: false,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            onFinished: (event, currentIndex) => {
            },
            labels: {
                next: "Save",
            }
        });
        Feather.replace();

        this._MerchantDetails.Categories = [];
        this.GetBusinessCategories();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
            this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
            this.TerminalsList_Filter_Providers_Load();
            this.Form_AddUser_Load();
            this.F_AddStore_Load();
            this.GetAcquirersList();
            this.GetBranches_List();
            this.TUTr_Filter_Banks_Load();
            this.GetMangers_List();
        });
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }

    TerminalId: string;
    public _ContactInfo: boolean;
    public _StoreInfoCopy: boolean;
    public GetAcquirersOption: Select2Options;
    public MerchantSaveRequest: any;

    GetAcquirersList() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccounts,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: [],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'DisplayName',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'AccountTypeCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        // S2Data.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Text, Condition, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetAcquirersOption = {
            placeholder: PlaceHolder,
            ajax: _Transport,
            multiple: false,
        };
    }
    GetAcquirersListChange(event: any) {
        this.Form_AddUser.patchValue(
            {
                OwnerKey: event.value
            }
        );
    }

    _SavedMerchant: any = {};
    public Form_AddUser: FormGroup;
    Form_AddUser_Address: string = null;
    Form_AddUser_Latitude: number = 0;
    Form_AddUser_Longitude: number = 0;
    @ViewChild('places') places: GooglePlaceDirective;
    Form_AddUser_PlaceMarkerClick(event) {
        this.Form_AddUser_Latitude = event.coords.lat;
        this.Form_AddUser_Longitude = event.coords.lng;
    }

    countrycode: any;
    public Form_AddUser_AddressChange(address: Address) {
        this.Form_AddUser_Latitude = address.geometry.location.lat();
        this.Form_AddUser_Longitude = address.geometry.location.lng();
        this.Form_AddUser_Address = address.formatted_address;
        this.Form_AddUser.controls['Latitude'].setValue(this.Form_AddUser_Latitude);
        this.Form_AddUser.controls['Longitude'].setValue(this.Form_AddUser_Longitude);
        this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
        if (this._CurrentAddress.country != this._HelperService.UserCountrycode) {
            this._HelperService.NotifyError('Currently we’re not serving in this area, please add locality within ' + this._HelperService.UserCountrycode);
        }
        else {
            this.Form_AddUser.controls['Address'].setValue(address.formatted_address);
            this.Form_AddUser.controls['MapAddress'].setValue(address.formatted_address);
        }
    }
    Form_AddUser_Show() {
    }
    Form_AddUser_Close() {
    }
    Form_AddUser_Load() {

        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;
        this.Form_AddUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SaveMerchant,
            OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
            OwnerId: this._HelperService.AppConfig.ActiveOwnerId,
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25),Validators.pattern('^[a-zA-Z \-\']+')])],
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128),Validators.pattern('^[a-zA-Z \-\']+')])],
            FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            BusinessOwnerName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256),Validators.pattern('^[a-zA-Z \-\']+')])],
            CityName: [null],
            StateName: [null],
            CountryName: [null],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.pattern(this._HelperService.AppConfig.EmailPattern), Validators.minLength(2)])],
            EmailAddressContact: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2),Validators.pattern(this._HelperService.AppConfig.EmailPattern)])],
            RewardPercentage: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])],
            ReferralCode: [null],
            RewardDeductionTypeCode: 'rewarddeductiontype.prepay',
            DealTypeCode: 'false',
            StatusCode: this._HelperService.AppConfig.Status.Active,
        });
    }


    _ToogleContactInfo() {
        this._ContactInfo = !this._ContactInfo;
        if (!this._ContactInfo) {
            this.Form_AddUser.controls['MobileNumber'].patchValue(this.Form_AddUser.controls['ContactNumber'].value);
            this.Form_AddUser.controls['EmailAddressContact'].patchValue(this.Form_AddUser.controls['EmailAddress'].value);
        } else {
            this.Form_AddUser.controls['MobileNumber'].patchValue(null);
            this.Form_AddUser.controls['EmailAddressContact'].patchValue(null);
        }
    }

    Form_AddUser_Clear() {
        this.Form_AddUser_Latitude = 0;
        this.Form_AddUser_Longitude = 0;
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }

    AddressBind: any;

    Form_AddUser_Process(value?: any) {
        var _FormValue = this.Form_AddUser.value;
        if (_FormValue.RewardPercentage == undefined) {
            this._HelperService.NotifyError('Enter reward percentage');
        }
        else if (isNaN(_FormValue.RewardPercentage) == true) {
            this._HelperService.NotifyError('Enter valid reward percentage');
        }
        else if (parseFloat(_FormValue.RewardPercentage) > 100) {
            this._HelperService.NotifyError('Reward percentage must be greater than 0 and less than 101');
        }
        else if (parseFloat(_FormValue.RewardPercentage) < 0) {
            this._HelperService.NotifyError('Reward percentage must be greater than 0 and less than 101');
        }
        else if (this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "") {
            this._HelperService.NotifyError("Please set your registered address");
        }
        else {
           // console.log(this._Address);
            this.AddressBind = this._Address;
            this._AddressStore = this.AddressBind;
            this.ShowStoreAddressLocator = true;
            this.MerchantSaveRequest = {};
            this.MerchantSaveRequest = this.ReFormat_RequestBody();
            this.MerchantSaveRequest.Address = this._Address.Address;
            this.MerchantSaveRequest.AddressComponent = this._Address;
            this._HelperService.IsFormProcessing = true;
            this._HelperService.CloseModal('_PreviewGeneral');
            
            $("#wizard1").steps("next", {});
            this._ToogleStoreCopy();
            this._HelperService.IsFormProcessing = false;
            
        }
    }

    F_AddStore: FormGroup;
    _SavedStore: any = {};
    _CurrentAddress: any = {};
    _CurrentAddressStore: any = {};
    F_AddStore_Show() {
    }
    F_AddStore_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.Stores]);
    }
    F_AddStore_Load() {
        this.F_AddStore = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SaveStore,
            AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            AccountId: this._HelperService.AppConfig.ActiveOwnerId,
            MerchantKey: [null],
            MerchantId: [null],
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            EmailAddress: [null, Validators.compose([Validators.email, Validators.minLength(2),Validators.pattern(this._HelperService.AppConfig.EmailPattern)])],
            FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            ContactMobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            ContactEmailId: [null, Validators.compose([Validators.email, Validators.minLength(2),Validators.pattern(this._HelperService.AppConfig.EmailPattern)])],
            StatusCode: this._HelperService.AppConfig.Status.Active,
        });
    }
    F_AddStore_Clear() {
        this.F_AddStore.reset();
        this.F_AddStore_Load();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    AddressChangeStore(Item) {
        // debugger;
        this._AddressStore = Item;

    }
    F_AddStore_Process() {
        if (this._AddressStore.Address == undefined || this._AddressStore.Address == null || this._AddressStore.Address == "") {
            this._HelperService.NotifyError("Please enter store location")
        }
        else {
            this.F_AddStore.controls['MerchantKey'].setValue(this._SavedMerchant.ReferenceKey);
            this.F_AddStore.controls['MerchantId'].setValue(this._SavedMerchant.ReferenceId);
            var _FormValue = this.F_AddStore.value;
            _FormValue.OwnerKey = this._SavedMerchant.ReferenceKey;
            _FormValue.Name = _FormValue.DisplayName;
            this._HelperService.IsFormProcessing = true;
            this._HelperService.NotifySuccess('Store added');
            this._HelperService.CloseModal('_PreviewStore');
            $("#wizard1").steps("next", {});
            this._HelperService.IsFormProcessing = false;
        }
    }
    _ToogleStoreCopy(): void {
        this._StoreInfoCopy = !this._StoreInfoCopy;
        if (true) {
            this.F_AddStore.controls['ContactNumber'].setValue(this.Form_AddUser.controls['ContactNumber'].value);
            this.F_AddStore.controls['DisplayName'].setValue(this.Form_AddUser.controls['DisplayName'].value);
            this.F_AddStore.controls['EmailAddress'].setValue(this.Form_AddUser.controls['EmailAddress'].value);
            this.F_AddStore.controls['FirstName'].setValue(this.Form_AddUser.controls['FirstName'].value);
            this.F_AddStore.controls['LastName'].setValue(this.Form_AddUser.controls['LastName'].value);
            this.F_AddStore.controls['ContactMobileNumber'].setValue(this.Form_AddUser.controls['MobileNumber'].value);
            this.F_AddStore.controls['ContactEmailId'].setValue(this.Form_AddUser.controls['EmailAddressContact'].value);
        }
    }
    Form_AddTerminal = this._FormBuilder.group({
        TerminalId: [null],
    });

    public TerminalsList_Filter_Provider_Option: Select2Options;
    public TerminalsList_Filter_Provider_Selected = 0;
    TerminalsList_Filter_Providers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TerminalsList_Filter_Provider_Option = {
            placeholder: 'Filter by PTSP',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    //ProviderName:any;
    public ProviderName1: any[] = [];
    TerminalsList_Filter_Providers_Change(event: any) {
        this.ProviderName1=[];
        if (event.value == this.TerminalsList_Filter_Provider_Selected) {

        }
        else if (event.value != this.TerminalsList_Filter_Provider_Selected) {
            this._ProviderId = 0;
            this._ChangeDetectorRef.detectChanges();
            this._ProviderId = event.value;
            this._ProviderKey = event.data[0].ReferenceKey;
            //this.ProviderName=event.data[0].DisplayName;
            this.ProviderName1.push(event.data[0].DisplayName);
            // console.log(this.ProviderName1)
            this.TerminalsLists_Load();
            this._ChangeDetectorRef.detectChanges();
        }
    }

    public _ProviderId: number = 0;
    public _ProviderKey: string = '';

    public _AcquireId: number = 0;
    public _AcquireKey: string = '';

    public _TerminalList: any[] = [];
    public _TerminalList1: any[] = [];
    public _SelectedTerminal: any = null;

    public TerminalsList_Option: Select2Options;
    public TerminalsList_Selected = 0;
    TerminalsLists_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: 'getterminals',
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceKey",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ProviderId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    SearchCondition: "=",
                    SearchValue: this._ProviderId.toString()
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TerminalsList_Option = {
            placeholder: 'Filter by PTSP',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TerminalsLists_Change(event: any) {


        if (event.value == this.TerminalsList_Selected) {

        }
        else if (event.value != this.TerminalsList_Selected) {
            this._SelectedTerminal = event.data[0];
        }
    }
    public TUTr_Filter_Bank_Option: Select2Options;
    TUTr_Filter_Banks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceKey",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
                }]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Bank_Option = {
            placeholder: 'Select Bank (Acquirer)',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    public bankname: any[] = [];
    TUTr_Filter_Banks_Change(event: any) {
        // console.log("bname:", event)
        this.bankname=[];
        this.Form_AddTerminal.patchValue(
            {
                AcquirerReferenceId: event.data[0].ReferenceId,
                AcquirerReferenceKey: event.data[0].ReferenceKey,
                BankName: event.data[0].DisplayName

            });
        this._AcquireId = event.data[0].ReferenceId;
        this._AcquireKey = event.data[0].ReferenceKey;
        //this.bankname = event.data[0].DisplayName;
        this.bankname.push(event.data[0].DisplayName);
        // console.log(this.bankname)
    }



    AddTerminal(): void {




        var terminal: any = {
            OperationType: 'edit',
            // Task: this._HelperService.AppConfig.Api.Core.SaveTerminal,
            TerminalId: this.Form_AddTerminal.controls['TerminalId'].value,
            MerchantId: this._SavedMerchant.ReferenceId,
            MerchantKey: this._SavedMerchant.ReferenceKey,
            StoreId: this._SavedStore.ReferenceId,
            StoreKey: this._SavedStore.ReferenceKey,
            ProviderId: this._ProviderId,
            ProviderKey: this._ProviderKey,
            AcquirerId: this._AcquireId,
            AcquirerKey: this._AcquireKey,

            // BankName:this.bankname,
            // ProviderName:this.ProviderName1,
            StatusCode: "default.active"
        };

        var terminal1: any = {
            OperationType: 'edit',
            // Task: this._HelperService.AppConfig.Api.Core.SaveTerminal,
            TerminalId: this.Form_AddTerminal.controls['TerminalId'].value,
            MerchantId: this._SavedMerchant.ReferenceId,
            MerchantKey: this._SavedMerchant.ReferenceKey,
            StoreId: this._SavedStore.ReferenceId,
            StoreKey: this._SavedStore.ReferenceKey,
            ProviderId: this._ProviderId,
            ProviderKey: this._ProviderKey,
            AcquirerId: this._AcquireId,
            // AcquirerKey: this._HelperService.UserAccount.AccountKey,
            AcquirerKey: this._AcquireKey,

            BankName: this.bankname,
            ProviderName: this.ProviderName1,
            StatusCode: "default.active"
        };

        var ElementIndex: number = null;
        for (let index = 0; index < this._TerminalList.length; index++) {
            if (this._TerminalList[index].TerminalId == terminal.TerminalId) {
                ElementIndex = index;
                break;
            }
        }
        if (ElementIndex == null) {
            this._TerminalList.push(terminal);
            this._TerminalList1.push(terminal1);
        }

        this.Form_AddTerminal.controls['TerminalId'].setValue(null);
        this.bankname = [];
    }
    AddTerminal_Process() {
        // debugger;
        this.MerchantSaveRequest.Task = 'savemerchant';
        this.CreateRequest();
        if (this.SelectedBusinessCategories != undefined && this.SelectedBusinessCategories.length > 0) {
            this.MerchantSaveRequest.Categories = [];
            for (let index = 0; index < this.SelectedBusinessCategories.length; index++) {
                const element = this.SelectedBusinessCategories[index];
                this.MerchantSaveRequest.Categories.push(
                    {
                        ReferenceId: element
                    }
                )
            }
        }
        this._HelperService.terminalsavecount = 0;
        for (let index = 0; index < this._TerminalList.length; index++) {
            this._HelperService.IsFormProcessing = true;
            var _FormValue = this._TerminalList[index];
            _FormValue.OperationType = undefined;
            this.MerchantSaveRequest.Stores[0].Terminals.push(_FormValue);
        }
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
            this.MerchantSaveRequest
        );
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                    this.Form_AddUser_Clear();
                    this.RouteToMerchant();
                   // this.resetAddressComponent=true;
                    this._AddressStore=null;
                    this._HelperService.updateStoreAddress(this._AddressStore);
                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            }
        );
    }
    private CreateRequest() {
        this.MerchantSaveRequest.AccountTypeCode = null;
        this.MerchantSaveRequest.DisplayName = this.Form_AddUser.controls['DisplayName'].value;
        this.MerchantSaveRequest.CompanyName = this.Form_AddUser.controls['Name'].value,
            this.MerchantSaveRequest.ContactNumber = this.Form_AddUser.controls['ContactNumber'].value;
        this.MerchantSaveRequest.MobileNumber = this.Form_AddUser.controls['MobileNumber'].value;
        this.MerchantSaveRequest.EmailAddress = this.Form_AddUser.controls['EmailAddress'].value;
        this.MerchantSaveRequest.WebsiteUrl = '';
        this.MerchantSaveRequest.Description = '';
        this.MerchantSaveRequest.StartDate = '';
        this.MerchantSaveRequest.StatusCode = this.Form_AddUser.controls['StatusCode'].value;
        this.MerchantSaveRequest.RewardPercentage = undefined;
        this.MerchantSaveRequest.IsDealMerchant = this.Form_AddUser.controls['DealTypeCode'].value;
        this.MerchantSaveRequest.Configurations = {
            RewardPercentage: this.Form_AddUser.controls['RewardPercentage'].value,
            RewardDeductionTypeCode: this.Form_AddUser.controls['RewardDeductionTypeCode'].value
        };
        this.MerchantSaveRequest.Address = this._Address.Address;
        this.MerchantSaveRequest.AddressComponent = this._Address;
        this.MerchantSaveRequest.ContactPerson = {
            FirstName: this.Form_AddUser.controls['FirstName'].value,
            LastName: this.Form_AddUser.controls['LastName'].value,
            MobileNumber: this.Form_AddUser.controls['MobileNumber'].value,
            EmailAddress: this.Form_AddUser.controls['EmailAddressContact'].value
        };
        this.MerchantSaveRequest.Stores = [
            {
                AccountOperationTypeCode: '',
                DisplayName: this.F_AddStore.controls['DisplayName'].value,
                Name: this.F_AddStore.controls['DisplayName'].value,
                ContactNumber: this.F_AddStore.controls['ContactNumber'].value,
                EmailAddress: this.F_AddStore.controls['EmailAddress'].value,
                StatusCode: this._HelperService.AppConfig.Status.Active,
                WebsiteUrl: '',
                Description: '',
                StartDate: '',
                Address: this._AddressStore.Address,
                AddressComponent: this._AddressStore,
                ContactPerson: {
                    FirstName: this.F_AddStore.controls['FirstName'].value,
                    LastName: this.F_AddStore.controls['LastName'].value,
                    MobileNumber: this.F_AddStore.controls['ContactMobileNumber'].value,
                    EmailAddress: this.F_AddStore.controls['ContactEmailId'].value
                },
                Terminals: []
            }
        ];
        this.MerchantSaveRequest.Terminals = null;
    }

    terminal_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Merchants]);

    }
    RouteToMerchant() {
        this._Router.navigate(["console/" + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AccountSettings.Merchants]);
    }

    _ShowGeneralPreview() {
        this._AddressStore = this._Address;
       // console.log("storeDetails",this._AddressStore);
        //this._HelperService.StoreAddressTemp=this._AddressStore;
        this._HelperService.updateStoreAddress(this._AddressStore);
        if (this._Address == undefined || this._Address == null || this._Address == "") {
            this._HelperService.NotifyError('Please select address');
        }
        else if (this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "") {
            this._HelperService.NotifyError('Please enter address');
        }
        else {
            this._HelperService.OpenModal('_PreviewGeneral');
        }
    }

    StoreName: any;
    _ShowStorePreview() {
        this.StoreName = this.F_AddStore.controls['DisplayName'].value;
        if (this._AddressStore == undefined || this._AddressStore == null || this._AddressStore == "") {
            this._HelperService.NotifyError('Please select address');
        }
        else {
            this._HelperService.OpenModal('_PreviewStore');
        }
    }

    _ShowTerminalPreview() {
        this._HelperService.OpenModal('_PreviewTerminal');
    }

    RemoveTerminal(ReferenceId: number): void {
        // console.log("inside function");
        var ElementIndex: number = null;
        for (let index = 0; index < this._TerminalList.length; index++) {
            if (this._TerminalList[index].ReferenceId == ReferenceId) {
                ElementIndex = index;
                break;
            }
        }

        if (ElementIndex != null) {
            this._TerminalList.splice(ElementIndex, 1);
        }

    //Remove terminal list1
        for (let index = 0; index < this._TerminalList1.length; index++) {
            if (this._TerminalList1[index].ReferenceId == ReferenceId) {
                ElementIndex = index;
                break;
            }
        }

        if (ElementIndex != null) {
            this._TerminalList1.splice(ElementIndex, 1);
        }

    }
    public GetBranches_Option: Select2Options;
    public GetBranches_Transport: any;
    GetBranches_List() {
        var PlaceHolder = "Select Branch";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetBranches,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            // SearchCondition: "",
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: 'StatusCode',
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: '=',
                //     SearchValue: this._HelperService.AppConfig.Status.Active,
                // }
            ]
        }



        this.GetBranches_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetBranches_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetBranches_Transport,
            multiple: false,
        };
    }
    GetBranches_ListChange(event: any) {
        // alert(event);\
        this.Form_AddUser.patchValue(
            {
                BranchKey: event.data[0].ReferenceKey,
                BranchId: event.data[0].ReferenceId

            }
        );

        this.F_AddStore.patchValue(
            {
                BranchKey: event.data[0].ReferenceKey,
                BranchId: event.data[0].ReferenceId

            }
        );

    }
    public GetMangers_Option: Select2Options;
    public GetMangers_Transport: any;
    GetMangers_List() {
        var PlaceHolder = "Select Manager";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetManagers,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: "AccountTypeCode",
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: "=",
                //     SearchValue: this._HelperService.AppConfig.AccountType.RelationshipManager
                // }
            ]
        }

        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'RoleId', this._HelperService.AppConfig.DataType.Text, '8', '=');
        this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetMangers_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetMangers_Transport,
            multiple: false,
        };
    }
    GetMangers_ListChange(event: any) {
        // alert(event);
        this.Form_AddUser.patchValue(
            {
                RmKey: event.data[0].ReferenceKey,
                RmId: event.data[0].ReferenceId
            }
        );

        this.F_AddStore.patchValue(
            {
                RmKey: event.data[0].ReferenceKey,
                RmId: event.data[0].ReferenceId
            }
        );
    }
    ReFormat_RequestBody(): void {
        var formValue: any = this.Form_AddUser.value;

        var formRequest: any = {

            OperationType: 'new',
            Task: formValue.Task,
            // AccountId: formValue.AccountId,
            // AccountKey: formValue.AccountKey,

            OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
            OwnerId: this._HelperService.AppConfig.ActiveOwnerId,


            BranchId: formValue.BranchId,
            BranchKey: formValue.BranchKey,
            RmId: formValue.RmId,
            RmKey: formValue.RmKey,
            ReferralCode: formValue.ReferralCode,

            DisplayName: formValue.DisplayName,
            Name: formValue.Name,
            FirstName: formValue.FirstName,
            BusinessOwnerName: formValue.BusinessOwnerName,
            MobileNumber: formValue.MobileNumber,
            ContactNumber: formValue.ContactNumber,
            EmailAddress: formValue.EmailAddress,
            RewardPercentage: formValue.RewardPercentage,

            AddressComponent: {
                Latitude: 0,
                Longitude: 0
            },

            Address: {
                StreetAddress: formValue.Address,
                Address: formValue.Address,
                Latitude: formValue.Latitude,
                Longitude: formValue.Longitude,
                CityName: formValue.CityName,
                StateName: formValue.StateName,
                CountryName: formValue.CountryName,
                MapAddress: formValue.MapAddress
            },
            ContactPerson: {
                FirstName: formValue.FirstName,
                LastName: formValue.LastName,
                MobileNumber: formValue.MobileNumber,
                EmailAddress: formValue.EmailAddress
            },
            Stores: [],
            StatusCode: formValue.StatusCode

        };
        return formRequest;

    }
    _MerchantDetails: OMerchantOnboard = {};
    public ShowCategorySelector: boolean = true;
    public ShowstateSelector: boolean = true;
    public ShowcitySelector: boolean = true;
    DoesCatAlreadyExist(Id: string): boolean {
        for (let index = 0; index < this._MerchantDetails.Categories.length; index++) {
            const element = this._MerchantDetails.Categories[index];
            if (element.ReferenceId == Id) {
                return true;
            }
        }
        return false;
    }

    removeCategory(cat: any): void {
        this._MerchantDetails.Categories.splice(cat, 1);
    }

    public BusinessCategories = [];
    public S2BusinessCategories = [];

    GetBusinessCategories() {

        this._HelperService.ToggleField = true;
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Core.getcategories,
            SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
            SortExpression: 'Name asc',
            Offset: 0,
            Limit: 1000,
        }
        PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
            PData.SearchCondition,
            "TypeCode",
            this._HelperService.AppConfig.DataType.Text,
            this._HelperService.AppConfig.HelperTypes.MerchantCategories,
            "="
        );
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Op, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        this.BusinessCategories = _Response.Result.Data;

                        this.ShowCategorySelector = false;
                        this._ChangeDetectorRef.detectChanges();
                        for (let index = 0; index < this.BusinessCategories.length; index++) {
                            const element = this.BusinessCategories[index];
                            this.S2BusinessCategories.push(
                                {
                                    id: element.ReferenceId,
                                    key: element.ReferenceKey,
                                    text: element.Name
                                }
                            );
                        }
                        this.ShowCategorySelector = true;
                        this._ChangeDetectorRef.detectChanges();

                        this._HelperService.ToggleField = false;

                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                this._HelperService.ToggleField = false;

            });
    }
    public SelectedBusinessCategories = [];
    CategoriesSelected(Items) {
        if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
            this.SelectedBusinessCategories = Items.value;
        }
        else {
            this.SelectedBusinessCategories = [];
        }
    }
    SaveMerchantBusinessCategory(item) {
        if (item != '0') {
            var Setup =
            {
                Task: this._HelperService.AppConfig.Api.Core.SaveUserParameter,
                TypeCode: this._HelperService.AppConfig.HelperTypes.MerchantCategories,
                // UserAccountKey: this._UserAccount.ReferenceKey,
                CommonKey: item,
                StatusCode: 'default.active'
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, Setup);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess('Business category assigned to merchant');
                        // this.BusinessCategories = [];
                        // this.GetMerchantBusinessCategories();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }

    checkEmail:boolean=false;
    ValidateEmail(inputText)
    {
   // var mailformat = /^w+([.-]?w+)*@w+([.-]?w+)*(.w{2,3})+$/;
    var mailformat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if(inputText.match(mailformat))
    {
        this.checkEmail=false;
   
    }
    else if(inputText=="")
    {
        this.checkEmail=false;
   
    }
    else if(inputText!="")
    {
        this.checkEmail=true;
    
    }
    }
}




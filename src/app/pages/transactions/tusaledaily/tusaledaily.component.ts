import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataHelperService, HelperService, OList } from '../../../service/service';

@Component({
    selector: 'tu-tusaledaily',
    templateUrl: './tusaledaily.component.html',
})
export class TUSaleComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {

    }
    ngOnInit() {
        this.TUCommissionPtsp_Setup();
    }

    public TUCommissionPtsp_Config: OList;
    TUCommissionPtsp_Setup() {
        this.TUCommissionPtsp_Config =
            {
                Id:null,
            Sort:null,
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetComissionTransactions,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
                Title: 'Commission History',
                StatusType: 'transaction',
                ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
                Type: this._HelperService.AppConfig.ListType.Owner,
                TableFields: [
                    {
                        DisplayName: 'Date',
                        SystemName: 'Date',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        IsDateSearchField: true,
                    },
                    {
                        DisplayName: 'Total Purchase',
                        SystemName: 'PurchaseAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                    },
                    {
                        DisplayName: 'Commission Amount',
                        SystemName: 'Amount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                    }
                ]
            }
        this.TUCommissionPtsp_Config = this._DataHelperService.List_Initialize(this.TUCommissionPtsp_Config);
        this.TUCommissionPtsp_GetData();
    }
    TUCommissionPtsp_ToggleOption(event: any, Type: any) {
        this.TUCommissionPtsp_Config = this._DataHelperService.List_Operations(this.TUCommissionPtsp_Config, event, Type);
        if (this.TUCommissionPtsp_Config.RefreshData == true) {
            this.TUCommissionPtsp_GetData();
        }
    }
    TUCommissionPtsp_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.TUCommissionPtsp_Config);
        this.TUCommissionPtsp_Config = TConfig;
    }
    TUCommissionPtsp_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    }

}
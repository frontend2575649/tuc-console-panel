import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BnploverviewComponent } from './bnploverview.component';

const routes: Routes = [
  {path:"", component:BnploverviewComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BnploverviewRoutingModule { }

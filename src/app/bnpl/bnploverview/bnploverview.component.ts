import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataHelperService, HelperService, OResponse } from '../../service/service';
import { BaseChartDirective, Color, Label } from 'ng2-charts';
import { Observable } from 'rxjs';
import { ChartDataSets, ChartOptions } from 'chart.js';
import * as moment from 'moment';
import * as cloneDeep from 'lodash/cloneDeep';
import * as _ from 'lodash';
import { Router } from '@angular/router';
import { setTheme } from 'ngx-bootstrap/utils';
import * as Feather from "feather-icons";


@Component({
  selector: 'app-bnploverview',
  templateUrl: './bnploverview.component.html',
  styleUrls: ['./bnploverview.component.css']
})
export class BnploverviewComponent implements OnInit {

  public DateSelected = moment().startOf("day");
  public selectedMonth = new Date().getMonth() + 1;
  public DisbursementByCurrentMonth;
  public currentMonth = new Date().getMonth() + 1;
  public DisbursementBylastMonth;
  maxDate = new Date();

  public currentDate = new Date();
  another = new Date(this.currentDate);
  lastMonthDate = new Date(this.another.setMonth(this.another.getMonth() - 1));

  public ActualStartDate;
  public ActualEndDate;

  barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      xAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };

  overviewData: any = {};
  disbursementData: any = {};
  repaymentStatusOverview: any = {};
  loanDisbursementByProviderData: any = {};
  disbursementByMonthData: any = {};
  totalEligibleCustomers: any = {};

  disbursementDistributionChartData = [];
  loanDisbursementChartData = [];
  principalAmountDisbursementData = [];
  intrestAmountData = [];
  totalEligibleCustomerChartData = [];

  lineChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [
        {
          display: true,
          // type: "linear",
          // position: "bottom",
          // ticks: {
          //   // precision: 0,
          //   stepSize: 2
          // }
        }
      ]
    }
  };

  public
  SalelineChartData: any[] = [];
  public SalelineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];
  public SalelineChartLabels = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
  showSaleChart = true;

  colors: Color[] = [
    {
      backgroundColor: "#DC3545",
    },
    {
      backgroundColor: "#10B759",
    },
    {
      backgroundColor: "#00CCCC",
    }
  ];

  loanDisbursementByProviderChartColors = [
    {
      backgroundColor: ["#5B53BA", "#B11A83"],
    },
  ]

  loanDisbursementDistributionChartColors = [
    {
      backgroundColor: ["#B11A83", "#0168FA"],
    },
  ]

  eligibleCustomerChartColor = [
    {
      backgroundColor: ["#5B53BA", "#C0CCDA"],
    },
  ]

  public repaymentStatusData = [];
  SalesLabels: any;
  modelDate = '';

  constructor(
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    private cdr: ChangeDetectorRef,
    private router: Router
  ) {
    setTheme('bs4'); // or 'bs4'
  }

  onOpenCalendar(container) {
    container.monthSelectHandler = (event: any): void => {
      container._store.dispatch(container._actions.select(event.date));
    };
    container.setViewMode('month');
  }

  // DateChangedForDisbursementDistribution(event: any, Type: any): void {
  //   this.DisbursementDistributionDate = event;
  //   this.getDisbursmentDistributionView();
  // }

  // DateChangedForRepaymentStatus(event: any, Type: any): void {
  //   this.repaymentStatusDate = event;
  //   this.getRepaymentStatusOverview();
  // }

  // DateChangedForLoanDisbursement(event: any, Type: any): void {
  //   this.loanDisbursementDate = event;
  //   this.getLoanDisbursementByprovider();
  // }

  DateChangedForDisbursementByMonth(event: any, Type: any): void {
    this.DisbursementBylastMonth = event;
    this.getDisbursementByMonth();
  }



  goToRequestPage() {
    this.router.navigate(["console/loanrequests/loanrequests"]);
  }

  gotoActiveLoanPage() {
    this.router.navigate(["console/loanactivity/loanactive"]);
  }

  // 3***
  gotoMerchantSettlements() {
    this.router.navigate(["console/bnpl/transaction"]);
  }


  // console/bnpl/merchantsettlements
  // 
  //#region DateChangeHandler
  DateChanged(event: any, Type: any): void {

    this._HelperService.DateChanged.next(event);

    var ev: any = cloneDeep(event);
    this.DateSelected = moment(ev.start).startOf("day");
    this.ActualStartDate = moment(ev.start).startOf("day");
    this.ActualEndDate = moment(ev.end).endOf("day");

    this.selectedMonth = new Date(ev.start).getMonth() + 1;
    this.getBNPLOverview();
    this.getDisbursmentDistributionView();
    this.getRepaymentStatusOverview();
    this.getLoanDisbursementByprovider();
  }

  getBNPLOverview() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetBnplOverview,
      ReferenceKey: null,
      ReferenceId: 0,
      StartDate: this._HelperService.DateInUTC(this.ActualStartDate),
      EndDate: this._HelperService.DateInUTC(this.ActualEndDate),
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.loans, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.overviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

  getDisbursmentDistributionView() {

    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetDisbursementDistribution,
      ReferenceKey: null,
      ReferenceId: 0,
      StartDate: this._HelperService.DateInUTC(this.ActualStartDate),
      EndDate: this._HelperService.DateInUTC(this.ActualEndDate),
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.loans, pData);
    _OResponse.subscribe(
      _Response => {
        this.disbursementDistributionChartData = [];
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.disbursementData = _Response.Result as any;
          if (this.disbursementData && this.disbursementData.TUCFees && this.disbursementData.TUCFees > 0) {
            this.disbursementDistributionChartData.push(this.disbursementData.TUCFees);
          }
          if (this.disbursementData && this.disbursementData.TUCFees && this.disbursementData.MerchantSettlement > 0) {
            this.disbursementDistributionChartData.push(this.disbursementData.MerchantSettlement);
          }
          this.cdr.detectChanges();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  getRepaymentStatusOverview() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetRepaymentStatusOverview,
      ReferenceKey: null,
      ReferenceId: 0,
      StartDate: this._HelperService.DateInUTC(this.ActualStartDate),
      EndDate: this._HelperService.DateInUTC(this.ActualEndDate),
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.loans, pData);
    _OResponse.subscribe(
      _Response => {
        this.repaymentStatusData = [];
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.repaymentStatusOverview = _Response.Result as any;

          this.repaymentStatusData.push({ data: [this.repaymentStatusOverview.OverDueCount], label: 'Overdue Repayments' });
          this.repaymentStatusData.push({ data: [this.repaymentStatusOverview.PaidCount], label: 'Paid Repayments' })
          this.repaymentStatusData.push({ data: [this.repaymentStatusOverview.UpComingCount], label: 'Upcoming' });
          this.cdr.detectChanges();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

  getLoanDisbursementByprovider() {

    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetLoanDisbursementByProvider,
      ReferenceKey: null,
      ReferenceId: 0,
      StartDate: this._HelperService.DateInUTC(this.ActualStartDate),
      EndDate: this._HelperService.DateInUTC(this.ActualEndDate),
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.loans, pData);
    _OResponse.subscribe(
      _Response => {
        this.loanDisbursementChartData = [];
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.loanDisbursementByProviderData = _Response.Result as any;
          this.loanDisbursementChartData = [];
          this.principalAmountDisbursementData = [];
          this.intrestAmountData = [];
          this.loanDisbursementByProviderData.TotalInterestAmount = (this.loanDisbursementByProviderData.TotalInterestAmount) ? this.loanDisbursementByProviderData.TotalInterestAmount.toFixed(2) : 0;
          this.loanDisbursementByProviderData.TotalDisbursedAmount = (this.loanDisbursementByProviderData.TotalDisbursedAmount) ? this.loanDisbursementByProviderData.TotalDisbursedAmount.toFixed(2) : 0;


          if (this.loanDisbursementByProviderData && this.loanDisbursementByProviderData.TotalDisbursedLoansByEC && this.loanDisbursementByProviderData.TotalDisbursedLoansByEC > 0) {
            this.loanDisbursementChartData.push(this.loanDisbursementByProviderData.TotalDisbursedLoansByEC);
          }
          if (this.loanDisbursementByProviderData && this.loanDisbursementByProviderData.TotalDisbursedLoansByTUC && this.loanDisbursementByProviderData.TotalDisbursedLoansByTUC > 0) {
            this.loanDisbursementChartData.push(this.loanDisbursementByProviderData.TotalDisbursedLoansByTUC);
          }
          if (this.loanDisbursementByProviderData && this.loanDisbursementByProviderData.TotalDisbursedAmountByEC && this.loanDisbursementByProviderData.TotalDisbursedAmountByEC > 0) {
            this.principalAmountDisbursementData.push(this.loanDisbursementByProviderData.TotalDisbursedAmountByEC);
          }
          if (this.loanDisbursementByProviderData && this.loanDisbursementByProviderData.TotalDisbursedAmountByTUC && this.loanDisbursementByProviderData.TotalDisbursedAmountByTUC > 0) {
            this.principalAmountDisbursementData.push(this.loanDisbursementByProviderData.TotalDisbursedAmountByTUC);
          }
          if (this.loanDisbursementByProviderData && this.loanDisbursementByProviderData.TotalInterestByEC && this.loanDisbursementByProviderData.TotalInterestByEC > 0) {
            this.intrestAmountData.push(this.loanDisbursementByProviderData.TotalInterestByEC);
          }
          if (this.loanDisbursementByProviderData && this.loanDisbursementByProviderData.TotalInterestByTUC && this.loanDisbursementByProviderData.TotalInterestByTUC > 0) {
            this.intrestAmountData.push(this.loanDisbursementByProviderData.TotalInterestByTUC);
          }
          this.cdr.detectChanges();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });

  }




  getDisbursementByMonth() {

    let startDate;

    if (!this.DisbursementBylastMonth) {
      let today = new Date();
      let makeDate = new Date(today);
      startDate = new Date(makeDate.setMonth(makeDate.getMonth() - 1));

    } else {
      startDate = this.DisbursementBylastMonth;
    }

    this.lastMonthDate = startDate;

    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetDisbursementByMonth,
      ReferenceKey: null,
      ReferenceId: 0,
      StartDate: this._HelperService.DateInUTC(startDate),
      EndDate: this._HelperService.DateInUTC(this.DisbursementByCurrentMonth)
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.loans, pData);
    _OResponse.subscribe(
      _Response => {
        this.SalelineChartData = [];
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.disbursementByMonthData = _Response.Result as any;

          if (this.disbursementByMonthData && this.disbursementByMonthData.DataByDate && this.disbursementByMonthData.DataByDate.length > 0) {

            const grouped = this.disbursementByMonthData.DataByDate.reduce((result, currentValue) => {
              // If an array already present for key, push it to the array. Else create an array and push the object
              (result[currentValue['MonthNo']] = result[currentValue['MonthNo']] || []).push(
                currentValue
              );
              // Return the current iteration `result` value, this will be taken as next iteration `result` value and accumulate
              return result;
            }, {});
            const currentMonth = new Date().getMonth() + 1;
            const lastMonth = new Date(startDate).getMonth() + 1;
            if (grouped[currentMonth]) {
              this.SalelineChartData.push({ data: [], label: grouped[currentMonth][0].Month });

              for (let i = 1; i <= 31; i++) {
                this.SalelineChartData[0].data.push({ x: Number(i), y: 0 });
              }

              for (let item of grouped[currentMonth]) {
                const index = this.SalelineChartData[0].data.findIndex(el => el.x == item.Title);
                if (index > -1) {
                  this.SalelineChartData[0].data[index].y = item.TotalLoanAmount
                }
                // this.SalelineChartData[0].data.push({ x: Number(item.Title), y: item.TotalLoanAmount });
              }

            }
            const index = this.SalelineChartData.length > 0 ? 1 : 0
            this.SalelineChartData.push({ data: [], borderDash: [10, 5] });

            for (let i = 1; i <= 31; i++) {
              this.SalelineChartData[index].data.push({ x: Number(i), y: 0 });
            }

            if (grouped[lastMonth] && grouped[lastMonth].length > 0) {

              // for (let item of grouped[lastMonth]) {
              //   this.SalelineChartData[index].data.push({ x: Number(item.Title), y: item.TotalLoanAmount });
              // }

              this.SalelineChartData[0].label = grouped[lastMonth][0].Month;


              for (let item of grouped[currentMonth]) {
                const index1 = this.SalelineChartData[index].data.findIndex(el => el.x == item.Title);
                if (index1 > -1) {
                  this.SalelineChartData[index].data[index1].y = item.TotalLoanAmount
                }
                // this.SalelineChartData[0].data.push({ x: Number(item.Title), y: item.TotalLoanAmount });
              }

            }
          }
          this.cdr.detectChanges();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

  getTotalEligibleCustomerTillToday() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetTotalEligibleCustomerTillToday
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.eligibleCustomers, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.totalEligibleCustomers = _Response.Result as any;
          this.totalEligibleCustomerChartData.push(this.totalEligibleCustomers.UsedCredit);
          this.totalEligibleCustomerChartData.push(this.totalEligibleCustomers.UnUsedCredit);

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }
  options;
  ngOnInit() {
    Feather.replace();
    window.addEventListener('scroll', this.scroll, true);
    this.getBNPLOverview();
    this.getDisbursmentDistributionView();
    this.getRepaymentStatusOverview();
    this.getLoanDisbursementByprovider();
    this.getDisbursementByMonth();
    this.getTotalEligibleCustomerTillToday();
  }


  GoToMerchantSettle() {
    this.router.navigate(["console/bnpl/merchantsettlements"]);
  }

  // 1***
  GoToLoanReqpage() {
    this.router.navigate(["console/loanrequests/loanrequests"]);
  }

  GoToActLoanPage() {
    this.router.navigate(["console/loanactivity/loanactive"]);
  }

  gotoEligibleCustomer() {
    this.router.navigate(["console/bnpl/eligiblecustomers"]);
  }

  scroll = (event): void => {
    $(".daterangepicker").hide();
  };
  ShowDatePicker() {
    $(".daterangepicker").show();
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }

}
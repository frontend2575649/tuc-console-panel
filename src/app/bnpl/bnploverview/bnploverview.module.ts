import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Daterangepicker } from 'ng2-daterangepicker';
import { ChartsModule } from 'ng2-charts';
import { BsDatepickerModule, BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { FormsModule } from '@angular/forms';
import { BnploverviewRoutingModule } from './bnploverview-routing.module';
import { BnploverviewComponent } from './bnploverview.component';

@NgModule({
  declarations: [BnploverviewComponent],
  imports: [
    CommonModule,
    Daterangepicker,
    ChartsModule,
    FormsModule,
    BnploverviewRoutingModule,
    BsDatepickerModule.forRoot()
  ],
  providers:[BsDatepickerConfig]
})
export class BnploverviewModule { }


import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Ng5SliderModule } from 'ng5-slider';
import { MainPipe } from '../service/main-pipe.module';
import { BnplRoutingModule } from './bnpl-routing.module';

import { EligiblecustomersComponent } from './eligiblecustomers/eligiblecustomers.component';
import { BnploverviewModule } from './bnploverview/bnploverview.module';
import { BnplmerchantsModule } from './bnplmerchants/bnplmerchants.module';
import { InvoicesandreportsModule } from './invoicesandreports/invoicesandreports.module';
import { MerchantsettlementsModule } from './merchantsettlements/merchantsettlements.module';
import { InvoiceComponent } from './invoice/invoice.component';


@NgModule({
  declarations: [EligiblecustomersComponent, InvoiceComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    Ng5SliderModule,
    MainPipe,
    BnplRoutingModule,
    BnploverviewModule,
    BnplmerchantsModule,
    InvoicesandreportsModule,
    MerchantsettlementsModule ,
    TranslateModule
  ]
})
export class BnplModule { }

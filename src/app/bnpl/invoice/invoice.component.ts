import { Component, OnInit,ChangeDetectorRef, } from '@angular/core';
import { ActivatedRoute, Router, Params } from "@angular/router";
import { Observable, Subscription } from 'rxjs';
import { DataHelperService, FilterHelperService, HelperService, OResponse, OSelect, OUserDetails  } from 'src/app/service/service';
import swal from 'sweetalert2';
import * as Feather from 'feather-icons';
import html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html'
})
export class InvoiceComponent implements OnInit {

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
  
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) { }
  public _ObjectSubscription1: Subscription = null;
  ngOnInit() {
    this._HelperService.ValidateData();
    Feather.replace();
    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.MerchantSettlment);

    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveMerchantReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;

      this.GetSettlementDetails();

    }
    else {
      this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound])
      this._HelperService.DeleteStorage('templocmerchant');

    }
  }

  public _SettlmentDetails: any =
    {
      "ReferenceId": null,
      "ReferenceKey": null,
      "DisplayName": null,
      "EmailAddress": null,
      "IconUrl": null,
      "AccountCode": null,
      "WebsiteUrl": null,
      "Address": {
        "Address": null,
        "Latitude": null,
        "Longitude": null
      },
      "ContactPerson": {
        "MobileNumber": null,
        "EmailAddress": null
      },
      // 
      "Rm": {
        "Name": null,
        "MobileNumber": null,
        "EmailAddress": null,
      },
      // 
      "Categories": [
        {
          "ReferenceId": null,
          "ReferenceKey": null,
          "Name": null,
          "IconUrl": null
        }
      ],
      "Stores": null,
      "Terminals": null,
      "Cashiers": null,
      "CreateDate": null,
      "StatusCode": null,
      "StatusName": null

    }

  GetSettlementDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      // Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.Getmerchantsettlement,
      Task: this._HelperService.AppConfig.Api.ThankUCash.Getmerchantsettlement,
      ReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.SettlementDetails, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._SettlmentDetails = _Response.Result;
          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


  htmltoPDF() {
    // this._HelperService.OpenModal('ModalReport');
    setTimeout(() => {
      this.StartDownload();
    }, 1000);
  }


  StartDownload() {
    html2canvas(document.querySelector("#invoice")).then(canvas => {
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;
      const contentDataURL = canvas.toDataURL('image/png');
      let pdf1 = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;
      pdf1.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf1.save('invoice' + '.pdf'); // Generated PDF   
    });
  }

  sendEmail(){

    // this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.SettlementInvoice,
      ReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey
    }
    
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.SettlementDetails, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );

  }
}

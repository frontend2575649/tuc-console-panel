import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { Observable } from 'rxjs';
import swal from "sweetalert2";
declare var $: any;
import {
  DataHelperService,
  FilterHelperService,
  HelperService,
  OList,
  OSelect,
  OResponse,
} from "../../service/service";

@Component({
  selector: 'app-merchantsettlements',
  templateUrl: './merchant-settelment.component.html'
})
export class MerchantsettlementsComponent implements OnInit {

  TodayStartTime = null;
  TodayEndTime = null;
  SettlmentRangeMin: number = this._HelperService.AppConfig.SettlmentRangeMin;
  SettlmentRangeMax: number = this._HelperService.AppConfig.SettlmentRangeMax;
  public ResetFilterControls: boolean = true;
  listData: any = {};
  OverviewData: any = {};

  ListType: number;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService: FilterHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
  ) { }


  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();
    this._HelperService.StopClickPropogation();
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];

      if (this._HelperService.AppConfig.ActiveReferenceKey == null && this._HelperService.AppConfig.ActiveReferenceId == null) {
        if (this._HelperService.AppConfig.ActiveOwnerId != null) {
          this.SettlementList_Setup();
          this.settlementOverview();
          this.TUTr_Filter_Merchants_Load();
          this.TUTr_Filter_Providers_Load();
        }
        else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
        }
      } else {
        this.SettlementList_Setup();
        this.settlementOverview();
        this.TUTr_Filter_Merchants_Load();
        this.TUTr_Filter_Providers_Load();
      }
    });
  }
  // start merchant filter 
  //#region merchant 

  public TUTr_Filter_Merchant_Option: Select2Options;
  public TUTr_Filter_Merchant_Selected = 0;
  TUTr_Filter_Merchants_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      // ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      // ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Merchant_Option = {
      placeholder: 'Search By Merchant',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Merchants_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.SettlementList_Config,
      this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant
    );
    this.MerchantEventProcessing(event);
  }

  MerchantEventProcessing(event: any): void {
    if (event.value == this.TUTr_Filter_Merchant_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
      this.SettlementList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.SettlementList_Config.SearchBaseConditions);
      this.TUTr_Filter_Merchant_Selected = 0;
    }
    else if (event.value != this.TUTr_Filter_Merchant_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
      this.SettlementList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.SettlementList_Config.SearchBaseConditions);
      this.TUTr_Filter_Merchant_Selected = event.value;
      this.SettlementList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '='));
    }
    this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    this._HelperService.ToggleField = true;
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 500);
  }

  //#endregion


  //#region provider 

  public TUTr_Filter_Provider_Option: Select2Options;
  public TUTr_Filter_Provider_Selected = 0;

  TUTr_Filter_Providers_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.getloanproviders,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.loans,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "ProviderName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Provider_Option = {
      placeholder: 'Search By Provider',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }

  TUTr_Filter_Providers_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.SettlementList_Config,
      this._HelperService.AppConfig.OtherFilters.MerchantSales.Provider
    );

    this.ProvidersEventProcessing(event);
  }
  SettlementList_Config
  ProvidersEventProcessing(event: any): void {
    if (event.value == this.TUTr_Filter_Provider_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
      this.SettlementList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.SettlementList_Config.SearchBaseConditions);
      this.TUTr_Filter_Provider_Selected = 0;
    }
    else if (event.value != this.TUTr_Filter_Provider_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
      this.SettlementList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.SettlementList_Config.SearchBaseConditions);
      this.TUTr_Filter_Provider_Selected = event.value;
      this.SettlementList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '='));
    }
    this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    this._HelperService.ToggleField = true;

    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 500);
  }

  // start settlement list setup
  SettlementList_Setup() {

    var SearchCondition = undefined
    SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'merchantsettelment.successful', '='),
      SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'TransactionId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '='),


      this.SettlementList_Config = {
        Id: null,
        Task: this._HelperService.AppConfig.Api.ThankUCash.getsettlementlist,
        Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.Settlementlist,
        Title: "List",
        // Status: this._HelperService.AppConfig.StatusList.defaultitem,
        Type: null,
        Sort:
        {
          SortDefaultName: null,
          SortDefaultColumn: 'LoanId',
          SortName: null,
          SortColumn: null,
          SortOrder: 'desc',
          SortOptions: [],
        },

        TableFields: [
          {
            DisplayName: "Merchant Name",
            SystemName: "MerchantAccountName",
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: "",
            Show: true,
            Search: true,
            Sort: true,
            IsDateSearchField: true,

          },
          {
            DisplayName: "Loan ID",
            SystemName: "LoanId",
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: "",
            Show: true,
            Search: false,
            Sort: true,
          },

          {
            DisplayName: "Customer",
            SystemName: "CustomerName",
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: "",
            Show: true,
            Search: true,
            Sort: true,
          },
          {
            DisplayName: "Redeemed on",
            SystemName: "LoanRedeemDate",
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: "",
            Show: true,
            Search: false,
            Sort: true,

          },
          {
            DisplayName: "Settlement on",
            SystemName: "SettlementDate",
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: "",
            Show: true,
            Search: false,
            Sort: true,
            IsDateSearchField: true,
          },
          {
            DisplayName: "Loan Amt",
            SystemName: "LoanAmount",
            DataType: this._HelperService.AppConfig.DataType.Decimal,
            Class: "",
            Show: true,
            Search: false,
            Sort: true,

          },
          {
            DisplayName: "TUC Fees",
            SystemName: "TUCFees",
            DataType: this._HelperService.AppConfig.DataType.Decimal,
            Class: "",
            Show: true,
            Search: false,
            Sort: true,

          },
          {
            DisplayName: "Settle. Amt",
            SystemName: "SettlementAmount",
            DataType: this._HelperService.AppConfig.DataType.Decimal,
            Class: "",
            Show: true,
            Search: false,
            Sort: true,

          },
        ],
        SearchBaseCondition: SearchCondition,

      }
    this.SettlementList_Config.ListType = this.ListType;
    this.SettlementList_Config.DefaultSortExpression = 'LoanId desc';

    // this.SettlementList_Config.SearchBaseCondition = "";
    this.SettlementList_Config = this._DataHelperService.List_Initialize(this.SettlementList_Config);
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Settlment_Filter,
      this.SettlementList_Config
    );
    this.TUTr_GetSettelmentData();
  }
  // End settlement list setup

  TUTr_GetSettelmentData() {
    this.GetOverviews(this.SettlementList_Config, this._HelperService.AppConfig.Api.ThankUCash.getsettlementlist);
    var TConfig = this._DataHelperService.List_GetData(
      this.SettlementList_Config
    );
    this.SettlementList_Config = TConfig;
  }


  // start add new filter
  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },

      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Settlment_Filter,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Settlment_Filter
        );
        this._FilterHelperService.SetMerchantSettelmentConfig(this.SettlementList_Config);
        this.TUTr_GetSettelmentData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.TUTr_Filter_Merchants_Load();
    this.TUTr_Filter_Providers_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  timeout = null;
  TUTr_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (Type == 'date') {
        event.start = this._HelperService.DateInUTC(event.start);
        event.end = this._HelperService.DateInUTC(event.end);
      }

      this._HelperService.Update_CurrentFilterSnap(event, Type, this.SettlementList_Config);

      this.SettlementList_Config = this._DataHelperService.List_Operations(this.SettlementList_Config, event, Type);


      this.TodayStartTime = this.SettlementList_Config.StartTime;
      this.TodayEndTime = this.SettlementList_Config.EndTime;

      if (event != null) {
        for (let index = 0; index < this.SettlementList_Config.Sort.SortOptions.length; index++) {
          const element = this.SettlementList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }
      if (
        (this.SettlementList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.TUTr_GetSettelmentData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);
  }


  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    if (this.SettlmentRangeMin && this.SettlmentRangeMax && this.SettlmentRangeMin > this.SettlmentRangeMax) {
      this._HelperService.NotifyError("Settlement mimimum amount should be less than maximum amount");
      return;
    }
    if (this.SettlmentRangeMin < 0 || this.SettlmentRangeMax < 0) {
      this._HelperService.NotifyError("Settlement amount should not be negative");
      return;
    }
    this.SetSearchRanges();

    this._HelperService.MakeFilterSnapPermanent();
    this.TUTr_GetSettelmentData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();

    if (ButtonType == 'Sort') {
      $("#Settlementlist_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#Settlementlist_fdropdown").dropdown('toggle');
    }
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantSettelmentConfig(this.SettlementList_Config);

    this.SetSalesRanges();
    this.SetOtherFilters();

    this.TUTr_GetSettelmentData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  SetSalesRanges(): void {
    this.SettlmentRangeMin = this.SettlementList_Config.SalesRange.SettlmentRangeMin;
    this.SettlmentRangeMax = this.SettlementList_Config.SalesRange.SettlmentRangeMax;
  }

  SetOtherFilters(): void {
    this.SettlementList_Config.SearchBaseConditions = [];

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant));
    if (CurrentIndex != -1) {
      this.TUTr_Filter_Merchant_Selected = 0;
      this.MerchantEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Provider));
    if (CurrentIndex != -1) {
      this.TUTr_Filter_Provider_Selected = 0;
      this.ProvidersEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }
  //end new filter

  GetOverviews(ListOptions: any, Task: string): any {

    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);
    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: (SortExpression === "CreateDate desc") ? "LoanId desc" : SortExpression,
      Type: ListOptions.Type,
      ReferenceKey: ListOptions.ReferenceKey,
      StartDate: ListOptions.StartDate,
      EndDate: ListOptions.EndDate,
      ReferenceId: ListOptions.ReferenceId,
      SubReferenceId: ListOptions.SubReferenceId,
      SubReferenceKey: ListOptions.SubReferenceKey,
      AccountId: ListOptions.AccountId,
      AccountKey: ListOptions.AccountKey,
      //OwnerId: ListOptions.OwnerId,
      //OwnerKey: ListOptions.OwnerKey,

      ListType: ListOptions.ListType,
      IsDownload: false,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.Settlementlist, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.listData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });


  }
  //end overview get method

  Settlement_ListTypeChange(Type) {
    this.ListType = Type;

    this.SettlementList_Setup();
  }


  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.SettlementList_Config);

    this.SetSalesRanges();
    this.SetOtherFilters();

    this.TUTr_GetSettelmentData();
  }

  SettlementList_RowSelected(ReferenceData) {
    //#region Save Current Merchant To Storage 

    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.MerchantSettlment,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,

      }
    );


    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;


  }


  TUTr_ToggleOption_Date(event: any, Type: any) {
    this.TUTr_ToggleOption(event, Type);
  }


  SetSearchRanges(): void {
    //#region credit limit 
    this.SettlementList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('SettlementAmount', this.SettlementList_Config.SearchBaseConditions);

    var SearchCase = this._HelperService.GetSearchConditionRange('', 'SettlementAmount', this.SettlmentRangeMin, this.SettlmentRangeMax);
    if (this.SettlmentRangeMin == this._HelperService.AppConfig.SettlmentRangeMin && this.SettlmentRangeMax == this._HelperService.AppConfig.SettlmentRangeMax) {
      this.SettlementList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.SettlementList_Config.SearchBaseConditions);
    }
    else {
      this.SettlementList_Config.SearchBaseConditions.push(SearchCase);
    }
    //#endregion  
  }

  TUTr_ToggleOption(event: any, Type: any) {


    // if (Type == 'date') {
    //   event.start = this._HelperService.DateInUTC(event.start);
    //   event.end = this._HelperService.DateInUTC(event.end);
    // }

    if (event != null) {
      for (let index = 0; index < this.SettlementList_Config.Sort.SortOptions.length; index++) {
        const element = this.SettlementList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    // this.TodayStartTime = this.SettlementList_Config.StartTime;
    // this.TodayEndTime = this.SettlementList_Config.EndTime;

    if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
      event.data = {
        SettlmentRangeMin: this.SettlmentRangeMin,
        SettlmentRangeMax: this.SettlmentRangeMax,
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.SettlementList_Config
    );

    this.SettlementList_Config = this._DataHelperService.List_Operations(
      this.SettlementList_Config,
      event,
      Type
    );

    if (
      (this.SettlementList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TUTr_GetSettelmentData();
    }
  }


  Active_FilterValueChanged(event: any) {

    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantSettelmentConfig(this.SettlementList_Config);

    //#region setOtherFilters
    this.SetSalesRanges();
    this.SetOtherFilters();
    //#endregio

    this.TUTr_GetSettelmentData();
  }


  settlementOverview() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.Getsettlementoverview,
      ReferenceId : this._HelperService.AppConfig.ActiveReferenceId
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.Settlementlistoverview, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.OverviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

  downloadRepot() {
    const pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.getsettlementlist,
      TotalRecords: 0,
      Offset: 0,
      Limit: 25,
      RefreshCount: true,
      SearchCondition: '',
      SortExpression: "LoanId desc",
      Type: null,
      ReferenceKey: null,
      ReferenceId: 0,
      SubReferenceId: 0,
      IsDownload: true,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.Settlementlist, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          const data = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

}

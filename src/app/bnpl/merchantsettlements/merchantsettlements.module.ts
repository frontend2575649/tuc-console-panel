import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from 'ng2-daterangepicker';
import { FormsModule } from '@angular/forms';

import { MerchantsettlementsRoutingModule } from './merchantsettlements-routing.module';
import { MerchantsettlementsComponent } from './merchantsettlements.component';
import { TransactionComponent} from './transaction/transaction.component';

@NgModule({
  declarations: [MerchantsettlementsComponent,TransactionComponent],
  imports: [
    CommonModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    FormsModule,
    MerchantsettlementsRoutingModule
  ]
})
export class MerchantsettlementsModule { }

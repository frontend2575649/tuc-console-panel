import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MerchantsettlementsComponent } from './merchantsettlements.component';
import { TransactionComponent } from './transaction/transaction.component';

const routes: Routes = [
  { path: "", data: { PageName: "Merchant Settlements" }, component: TransactionComponent },
  { path: 'transaction', data: { PageName: "Merchant Settlements" }, component: TransactionComponent },
  { path: 'merchantsettlements/:referencekey/:referenceid', data: { PageName: "Merchant Settlements" }, component: MerchantsettlementsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MerchantsettlementsRoutingModule { }

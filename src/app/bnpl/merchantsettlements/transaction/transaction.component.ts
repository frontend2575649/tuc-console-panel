import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../service/service';
import swal from 'sweetalert2';
import * as Feather from 'feather-icons';
declare var $: any;
declare var moment:any;
@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {

  public TUTr_Config: OList;
  transationList: any = [];
  settlementliteOverviewData: any = {};

  minSettlementAmount = this._HelperService.AppConfig.loanAmountMin;
  maxSettlementAmount = this._HelperService.AppConfig.loanAmountMax;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService: FilterHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
  ) { }

  public ResetFilterControls: boolean = true;

  TuTr_Setup_Fields() {
    var TableFields = [];

    TableFields = [
      {
        DisplayName: 'Status',
        SystemName: 'StatusName',
        DataType: this._HelperService.AppConfig.DataType.Text,
        Show: true,
        Search: false,
        Sort: true,
      },
      {
        DisplayName: 'Transaction Id',
        SystemName: 'ReferenceId',
        DataType: this._HelperService.AppConfig.DataType.Number,
        Show: true,
        Search: true,
        Sort: true,
      },
      {
        DisplayName: 'Transaction Date',
        SystemName: 'TransactionDate',
        DataType: this._HelperService.AppConfig.DataType.Date,
        Show: true,
        Search: false,
        Sort: true,
        IsDateSearchField: true,
      },
      {
        DisplayName: 'Amount',
        SystemName: 'amount',
        DataType: this._HelperService.AppConfig.DataType.Decimal,
        Show: true,
        Search: false,
        Sort: true,
      },
    ]

    return TableFields;
  }

  TUTr_Setup() {
    var SearchCondition = undefined
    this.TUTr_Config =
    {
      Id: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetSettlementsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.Settlementlist,
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'ReferenceId',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },
      Title: "Transaction",
      StatusType: null,
      SearchBaseCondition: SearchCondition,
      TableFields: this.TuTr_Setup_Fields(),
      DefaultSortExpression: "LoanId desc",
    }

    this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Transaction,
      this.TUTr_Config
    );
    this.TUTr_GetData();
  }

  SetSearchRanges(): void {
    //#region Invoice 
    this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('Amount', this.TUTr_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.minSettlementAmount, this.maxSettlementAmount);
    if (this.minSettlementAmount == this._HelperService.AppConfig.loanAmountMin && this.maxSettlementAmount == this._HelperService.AppConfig.loanAmountMax) {
      this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
    }
    else {
      this.TUTr_Config.SearchBaseConditions.push(SearchCase);
    }
    //#endregion      
  }

  SetSalesRanges(): void {
    this.minSettlementAmount = this._HelperService.AppConfig.loanAmountMin
    this.maxSettlementAmount = this._HelperService.AppConfig.loanAmountMax
  }

  SettlementList_RowSelected(item) {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.BNPL.MerchantSettlement,
      item.ReferenceKey,
      item.ReferenceId,
    ]);
  }

  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);
    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: (SortExpression === "CreateDate desc") ? "ReferenceId desc" : SortExpression,
      Type: ListOptions.Type,
      ReferenceKey: ListOptions.ReferenceKey,
      ReferenceId: ListOptions.ReferenceId,
      SubReferenceId: ListOptions.SubReferenceId,
      IsDownload: false
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.Settlementlist, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.transationList = _Response.Result.Data as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

  TUTr_ToggleOption(event: any, Type: any) {
    if (event != null) {
      for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
        const element = this.TUTr_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    if (Type == "date") {
      this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
      this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
  }
    if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
      event.data = {
        loanAmountMin: this.minSettlementAmount,
        loanAmountMax: this.maxSettlementAmount
      }
    }
    this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);
    this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
    if (
      (this.TUTr_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TUTr_GetData();
    }
  }

  timeout = null;
  TUTr_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

      this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
      if (event != null) {
        for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
          const element = this.TUTr_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }

          else {
            element.SystemActive = false;
          }

        }
      }


      if (
        (this.TUTr_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.TUTr_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);

  }


  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Transaction
        );
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
        this.TUTr_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this.SetSearchRanges();
    if (this.minSettlementAmount && this.maxSettlementAmount && this.minSettlementAmount > this.maxSettlementAmount) {
      this._HelperService.NotifyError("Mimimum amount should be less than maximum amount");
      return;
    }
    if (this.minSettlementAmount < 0 || this.maxSettlementAmount < 0) {
      this._HelperService.NotifyError("Amount should not be negative");
      return;
    }
    this._HelperService.MakeFilterSnapPermanent();
    this.TUTr_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();

    if (ButtonType == 'Sort') {
      $("#TUTr_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#TUTr_fdropdown").dropdown('toggle');
    }
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

    //#region setOtherFilters
    this.SetSalesRanges();
    this.SetOtherFilters();
    //#endregion
    this.TUTr_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }


  SetOtherFilters(): void {
    this.TUTr_Config.SearchBaseConditions = [];
  }

  ResetFilterUI(): void {
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

    //#region setOtherFilters
    this.SetSalesRanges();
    this.SetOtherFilters();
    //#endregion

    this.TUTr_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_MerchantSales(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Transaction
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
    if (Type == 'Time') {
      this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
      this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
  }
    //#region setOtherFilters
    this.SetSalesRanges();
    this.SetOtherFilters();
    //#endregion

    this.TUTr_GetData();
  }


  TUTr_GetData() {
    this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetSettlementsLite);
    var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
    this.TUTr_Config = TConfig;
  }

  getSettlementliteOverviewData() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetSettlementOverviewLite
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.Settlementlist, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.settlementliteOverviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

  ngOnInit() {
    Feather.replace();
    window.addEventListener('scroll', this.scroll, true);
    this.TUTr_Setup();
    this.getSettlementliteOverviewData();
  }
  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }
  scroll = (event): void => {
    $(".daterangepicker").hide();
  };
  ShowDatePicker() {
    $(".daterangepicker").show();
  }
}

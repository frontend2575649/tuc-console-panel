import { ChangeDetectorRef, Component, OnInit, OnDestroy, ElementRef, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;
declare var moment:any;
import {
    DataHelperService,
    HelperService,
    OList,
    OSelect,
    FilterHelperService,
    OResponse,
} from "../../service/service";
import { Observable, Subscription } from 'rxjs';


@Component({
    selector: 'app-eligiblecustomers',
    templateUrl: './eligiblecustomers.component.html',
})
export class EligiblecustomersComponent implements OnInit {
    public ListType: number;
    public listData: any = {};
    public overviewData: any = {};
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _FilterHelperService: FilterHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
    ) {
    }
    @ViewChild("offCanvas") divView: ElementRef;
    public ResetFilterControls: boolean = true;
    public AcquirerId = 0;

    CreditLimitMin: number = this._HelperService.AppConfig.CreditLimitMin;
    CreditLimitMax: number = this._HelperService.AppConfig.CreditLimitMax;

    ngOnInit() {
        Feather.replace();
        this._HelperService.StopClickPropogation();
        this.InitBackDropClickEvent();
        this.getEligibleCustomerOverview();
        this.TUTr_Setup();
        window.addEventListener('scroll', this.scroll, true);

        this._ActivatedRoute.params.subscribe((params: Params) => {
            var referenceid = params['referenceid'];
            if (referenceid != undefined && referenceid != null) {
                this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
                this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
                this._HelperService.AppConfig.ActiveAccountId = params['accountid'];
                this._HelperService.AppConfig.ActiveAccountKey = params['accountkey'];
            }

        });

    }

    InitBackDropClickEvent(): void {
        var backdrop: HTMLElement = document.getElementById("backdrop");

        backdrop.onclick = () => {
            $(this.divView.nativeElement).removeClass('show');
            backdrop.classList.remove("show");
        };
    }

    RouteCustomer(ReferenceData) {
        //#region Save Current Merchant To Storage 

        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveCustomer,
            {
                ReferenceKey: ReferenceData.AccountKey,
                ReferenceId: ReferenceData.AccountId,
                DisplayName: ReferenceData.DisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.Customer,
            }
        );

        //#endregion

        //#region Set Active Reference Key To Current Merchant 

        this._HelperService.AppConfig.ActiveReferenceKey =
            ReferenceData.AccountKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.AccountId;

        //#endregion

        //#region navigate 

        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.CustomerPanel.Accounts.WalletInfo,
            ReferenceData.AccountKey,
            ReferenceData.AccountId,
        ]);

        //#endregion

    }

    //#region transactions 

    public TUTr_Config: OList;
    TUTr_Setup() {
        this.TUTr_Config =
        {
            Id: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetEligibleCustomer,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.eligibleCustomers,
            Title: 'Eligible customers',
            Status: this._HelperService.AppConfig.StatusList.defaultitem,
            Type: null,
            ListType: 0,
            Sort:
            {
                SortDefaultName: null,
                SortDefaultColumn: 'ReferenceId',
                SortName: null,
                SortColumn: null,
                SortOrder: 'desc',
                SortOptions: [],
            },
            TableFields: this.TuTr_Setup_Fields()
        }

        this.TUTr_Config.ListType = this.ListType;
        this.TUTr_Config.SearchBaseCondition = "";

        if (this.TUTr_Config.ListType == 1) // Active
        {
            this.TUTr_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUTr_Config.SearchBaseCondition, "AccountStatusCode", 'number', 'default.active', "=");
        }
        else if (this.TUTr_Config.ListType == 2) // Suspend
        {
            this.TUTr_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUTr_Config.SearchBaseCondition, "AccountStatusCode", 'number', 'default.suspended', "=");
        }
        else if (this.TUTr_Config.ListType == 3) // Blocked
        {
            this.TUTr_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUTr_Config.SearchBaseCondition, "AccountStatusCode", 'number', 'default.blocked', "=");
        }

        else if (this.TUTr_Config.ListType == 4) {  // InActive
            this.TUTr_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUTr_Config.SearchBaseCondition, "AccountStatusCode", 'number', 'default.inactive', "=");
        }
        else if (this.TUTr_Config.ListType == 5) {  // All
            this.TUTr_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrictFromArray(this.TUTr_Config.SearchBaseCondition, "AccountStatusCode", 'number', [], "=");
        }
        else {
            this.TUTr_Config.DefaultSortExpression = 'ReferenceId desc';
        }


        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.EligibleCustomer,
            this.TUTr_Config
        );
        this.TUTr_GetData();
    }

    SetSearchRanges(): void {
        //#region credit limit 
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('CreditLimit', this.TUTr_Config.SearchBaseConditions);

        var SearchCase = this._HelperService.GetSearchConditionRange('', 'CreditLimit', this.CreditLimitMin, this.CreditLimitMax);
        if (this.CreditLimitMin == this._HelperService.AppConfig.CreditLimitMin && this.CreditLimitMax == this._HelperService.AppConfig.CreditLimitMax) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }
        //#endregion  
    }

    TuTr_Setup_Fields() {
        var TableFields = [];

        TableFields = [
            {
                DisplayName: 'Customer Details',
                SystemName: 'CustomerName',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: true,
            },
            {
                DisplayName: ' Mobile Number',
                SystemName: 'MobileNumber',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: false,
            },
            {
                DisplayName: 'Credit Limit',
                SystemName: 'CreditLimit',
                DataType: this._HelperService.AppConfig.DataType.Number,
                Show: true,
                Search: false,
                Sort: true,
            },

            {
                DisplayName: 'Approved On',
                SystemName: 'ApprovedOn',
                DataType: this._HelperService.AppConfig.DataType.Date,
                Class: 'td-date',
                IsDateSearchField: true,
                Show: true,
                Search: false,
                Sort: true,
            },
            {
                DisplayName: "Active Loans",
                SystemName: 'ActiveLoans',
                DataType: this._HelperService.AppConfig.DataType.Number,
                Show: true,
                Search: false,
                Sort: true,
            },
            {
                DisplayName: 'Last activity on app',
                SystemName: 'LastActivity',
                DataType: this._HelperService.AppConfig.DataType.Date,
                Class: 'td-date',
                Show: true,
                Search: false,
                Sort: true,
            },

            {
                DisplayName: 'status',
                SystemName: 'AccountStatusCode',
                DataType: this._HelperService.AppConfig.DataType.Number,
                Show: false,
                Search: false,
                Sort: false,
            },
        ]

        return TableFields;
    }



    TUTr_ToggleOption_Date(event: any, Type: any) {
        this.TUTr_ToggleOption(event, Type);
    }



    TUTr_ToggleOption(event: any, Type: any) {

        // if (Type == 'date') {
        //     event.start = this._HelperService.DateInUTC(event.start);
        //     event.end = this._HelperService.DateInUTC(event.end);
        // }
        if (Type == "date") {
            this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
            this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
        }
        if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
            event.data = {
                CreditLimitMin: this.CreditLimitMin,
                CreditLimitMax: this.CreditLimitMax,
            }
        }

        this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

        this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);

        this.TodayStartTime = this.TUTr_Config.StartTime;
        this.TodayEndTime = this.TUTr_Config.EndTime;
        if (event != null) {
            for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                const element = this.TUTr_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }
            }
        }


        if (
            (this.TUTr_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.TUTr_GetData();
        }
    }

    timeout = null;
    TUTr_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {

            if (Type == 'date') {
                event.start = this._HelperService.DateInUTC(event.start);
                event.end = this._HelperService.DateInUTC(event.end);
            }

            this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

            this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);

            this.TodayStartTime = this.TUTr_Config.StartTime;
            this.TodayEndTime = this.TUTr_Config.EndTime;
            if (event != null) {
                for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                    const element = this.TUTr_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {
                        element.SystemActive = true;
                    }
                    else {
                        element.SystemActive = false;
                    }

                }
            }


            if (
                (this.TUTr_Config.RefreshData == true)
                && this._HelperService.DataReloadEligibility(Type)
            ) {
                this.TUTr_GetData();
            }
        }, this._HelperService.AppConfig.SearchInputDelay);


    }

    TUTr_GetData() {
        this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetEligibleCustomer);
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    }

    TUTr_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
        this._HelperService.AppConfig.ActiveAccountKey = ReferenceData.AccountKey;
        this._HelperService.AppConfig.ActiveAccountId = ReferenceData.AccountId;
        // this.ListAppUsage_GetDetails();
    }

    clicked() {
        $(this.divView.nativeElement).addClass('show');
        var backdrop: HTMLElement = document.getElementById("backdrop");
        backdrop.classList.add("show");
    }
    unclick() {
        $(this.divView.nativeElement).removeClass('show');
        var backdrop: HTMLElement = document.getElementById("backdrop");
        backdrop.classList.remove("show");
    }



    //#endregion

    SetOtherFilters(): void {
        this.TUTr_Config.SearchBaseConditions = [];
        this.SetSearchRanges();
    }

    //#region dropdowns 

    TodayStartTime = null;
    TodayEndTime = null;


    ResetFilterUI(): void {
        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();
    }

    //#region SalesOverview 

    GetOverviews(ListOptions: any, Task: string): any {
        this._HelperService.IsFormProcessing = true;

        ListOptions.SearchCondition = '';
        ListOptions = this._DataHelperService.List_GetData(ListOptions);

        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }

        var pData = {
            Task: Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: (SortExpression === "CreateDate desc") ? "ReferenceId desc" : SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            AccountId: ListOptions.AccountId,
            AccountKey: ListOptions.AccountKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.eligibleCustomers, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.listData = _Response.Result as any;
                    if (this.listData && this.listData.Data && this.listData.Data.length > 0) {
                        for (let item of this.listData.Data) {
                            item.StatusC = this._HelperService.GetStatusColor(2);
                        }
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);

            });


    }

    //#endregion

    //#region filterOperations

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
        this.SetSalesRanges();

        //#region setOtherFilters
        //   this.SetOtherFilters();
        //#endregion

        this.TUTr_GetData();
    }

    SetSalesRanges(): void {
        this.CreditLimitMin = this.TUTr_Config.SalesRange.CreditLimitMin;
        this.CreditLimitMax = this.TUTr_Config.SalesRange.CreditLimitMax;
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_Store(Type, index);
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

        if (Type == 'Time') {
            this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
            this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
        }
        //#region setOtherFilters
        this.SetSalesRanges();
        this.SetOtherFilters();

        //#endregion

        this.TUTr_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            // input: "text",
            html:
                '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
                '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
                '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
            focusConfirm: false,
            preConfirm: () => {
                return {
                    filter: document.getElementById('swal-input1')['value'],
                    private: document.getElementById('swal-input2')['checked']
                }
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {

                if (result.value.filter.length < 5) {
                    this._HelperService.NotifyError('Enter filter name length greater than 4');
                    return;
                }

                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

                var AccessType: number = result.value.private ? 0 : 1;
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.EligibleCustomer,
                    AccessType
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._HelperService.Delete_Filter(
                    this._HelperService.AppConfig.FilterTypeOption.EligibleCustomer
                );
                this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
                this.TUTr_GetData();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        if (this.CreditLimitMin && this.CreditLimitMax && this.CreditLimitMin > this.CreditLimitMax) {
            this._HelperService.NotifyError("Minimum credit limit should be less than maximum credit limit");
            return;
        }
        if (this.CreditLimitMin < 0 || this.CreditLimitMax < 0) {
            this._HelperService.NotifyError("Credit limit should not be negative");
            return;
        }
        this.SetSearchRanges();
        this._HelperService.MakeFilterSnapPermanent();
        this.TUTr_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();

        if (ButtonType == 'Sort') {
            $("#TUTr_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#TUTr_fdropdown").dropdown('toggle');
        }
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
        this.SetSalesRanges();

        //#region setOtherFilters
        //   this.SetOtherFilters();
        //#endregion
        this.TUTr_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    //#endregion

    getEligibleCustomerOverview() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetEligibleCustoerOverview,
            ReferenceId: 0,
            ReferenceKey: null,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.GetEligibleCustoerOverview, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.overviewData = _Response.Result as any;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);

            });

    }
    ngOnDestroy() {
        window.removeEventListener('scroll', this.scroll, true);
    }
    scroll = (event): void => {
        $(".daterangepicker").hide();
    };
    ShowDatePicker() {
        $(".daterangepicker").show();
    }

}


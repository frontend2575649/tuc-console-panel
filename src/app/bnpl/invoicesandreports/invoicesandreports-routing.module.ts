import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoicesandreportsComponent } from './invoicesandreports.component';
import { ReconciliationreportComponent } from './reconciliationreport/reconciliationreport.component';
import { RepaymentreportComponent } from './repaymentreport/repaymentreport.component';

const routes: Routes = [
  {
    path: "",
    component: InvoicesandreportsComponent,
    children: [
      { path: "", redirectTo: 'reconciliation' },
      { path: "reconciliation", component: ReconciliationreportComponent },
      { path: "repayment", component: RepaymentreportComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoicesandreportsRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as Feather from 'feather-icons';

import { DataHelperService, FilterHelperService, HelperService, OList, OSelect } from 'src/app/service/service';

@Component({
  selector: 'app-reconciliationreport',
  templateUrl: './reconciliationreport.component.html',
  styleUrls:['./reconciliationreport.component.css']
})
export class ReconciliationreportComponent implements OnInit {
  public ListType: number;

  Data = [
    {
      loanId: 'L0001', customer: 'Edward Cullen', customerPhone: '+234 800 999 0088', Merchant: 'Tribet Superstore', mAddess: 'Lagos', provider: 'Ec',
      pricipalAmt: 380000, month: 4, totalInt: 4480, per: 7, totalAmt: 384480, fees: 5000, status: 'PAID', mSettlement: 375000, settlementStatus: 'PAID'
    },
    {
      loanId: 'L0001', customer: 'Edward Cullen', customerPhone: '+234 800 999 0088', Merchant: 'Tribet Superstore', mAddess: 'Lagos', provider: 'Ec',
      pricipalAmt: 380000, month: 4, totalInt: 4480, per: 7, totalAmt: 384480, fees: 5000, status: 'PAID', mSettlement: 375000, settlementStatus: 'PAID'
    },
    {
      loanId: 'L0001', customer: 'Edward Cullen', customerPhone: '+234 800 999 0088', Merchant: 'Tribet Superstore', mAddess: 'Lagos', provider: 'Ec',
      pricipalAmt: 380000, month: 4, totalInt: 4480, per: 7, totalAmt: 384480, fees: 5000, status: 'PAID', mSettlement: 375000, settlementStatus: 'PAID'
    },
    {
      loanId: 'L0001', customer: 'Edward Cullen', customerPhone: '+234 800 999 0088', Merchant: 'Tribet Superstore', mAddess: 'Lagos', provider: 'Ec',
      pricipalAmt: 380000, month: 4, totalInt: 4480, per: 7, totalAmt: 384480, fees: 5000, status: 'PAID', mSettlement: 375000, settlementStatus: 'PAID'
    },
    {
      loanId: 'L0001', customer: 'Edward Cullen', customerPhone: '+234 800 999 0088', Merchant: 'Tribet Superstore', mAddess: 'Lagos', provider: 'Ec',
      pricipalAmt: 380000, month: 4, totalInt: 4480, per: 7, totalAmt: 384480, fees: 5000, status: 'PAID', mSettlement: 375000, settlementStatus: 'PAID'
    },
    {
      loanId: 'L0001', customer: 'Edward Cullen', customerPhone: '+234 800 999 0088', Merchant: 'Tribet Superstore', mAddess: 'Lagos', provider: 'Ec',
      pricipalAmt: 380000, month: 4, totalInt: 4480, per: 7, totalAmt: 384480, fees: 5000, status: 'PAID', mSettlement: 375000, settlementStatus: 'PAID'
    }
  ]

  constructor(
    public _Router: Router,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService: FilterHelperService) { }

  Save_NewFilter() {

  }

  Delete_Filter() {

  }


  MerchantsList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.MerchantsList_Config.Sort.SortOptions.length; index++) {
        const element = this.MerchantsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.MerchantsList_Config


    );

    this.MerchantsList_Config = this._DataHelperService.List_Operations(
      this.MerchantsList_Config,
      event,
      Type
    );

    if (
      (this.MerchantsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
    }

  }

  MerchantsList_RowSelected(item) {

  }

  goToInvoice() {
    this._Router.navigate(["invoice"]);
  }

  timeout = null;
  MerchantsList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.MerchantsList_Config.Sort.SortOptions.length; index++) {
          const element = this.MerchantsList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.MerchantsList_Config


      );

      this.MerchantsList_Config = this._DataHelperService.List_Operations(
        this.MerchantsList_Config,
        event,
        Type
      );

      if (
        (this.MerchantsList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
      }

    }, this._HelperService.AppConfig.SearchInputDelay);

  }


  public MerchantsList_Filter_Owners_Option: Select2Options;
  public MerchantsList_Filter_Owners_Selected = 0;
  MerchantsList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetAccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      SearchCondition: "",
      SortCondition: ["DisplayName asc"],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ]
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
      [
        this._HelperService.AppConfig.AccountType.Merchant,
        this._HelperService.AppConfig.AccountType.Acquirer,
        this._HelperService.AppConfig.AccountType.PGAccount,
        this._HelperService.AppConfig.AccountType.PosAccount
      ]
      , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.MerchantsList_Filter_Owners_Option = {
      placeholder: 'Sort by Referrer',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  MerchantsList_Filter_Owners_Change(event: any) {
    if (event.value == this.MerchantsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.MerchantsList_Filter_Owners_Selected, '=');
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.MerchantsList_Config.SearchBaseConditions);
      this.MerchantsList_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.MerchantsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.MerchantsList_Filter_Owners_Selected, '=');
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.MerchantsList_Config.SearchBaseConditions);
      this.MerchantsList_Filter_Owners_Selected = event.value;
      this.MerchantsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.MerchantsList_Filter_Owners_Selected, '='));
    }

    this.MerchantsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.MerchantsList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );
  }

  public MerchantsList_Config: OList;
  MerchantsList_Setup() {
    this.MerchantsList_Config = {
      Id: null,
      Sort: null,
      Type: "all",
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      Title: "Available Merchants",
      StatusType: "default",
      DefaultSortExpression: "CreateDate desc",
      // Status: this._HelperService.AppConfig.StatusList.defaultaccountitem,
      StatusName: 'Active',
      IsDownload: true,
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: "Merchant Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Contact No",
          SystemName: "ContactNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Store",
          SystemName: "StoreDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Total POS",
          SystemName: "TotalTerminals",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Active POS",
          SystemName: "ActiveTerminals",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        }, {
          DisplayName: "R%",
          SystemName: "RewardPercentage",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Stores",
          SystemName: "Stores",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Balance",
          SystemName: "MainBalance",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Added On",
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
          NavigateField: "ReferenceKey",
        },
      ],

    };

    this.MerchantsList_Config.ListType = this.ListType;
    this.MerchantsList_Config.SearchBaseCondition = "";

    if (this.MerchantsList_Config.ListType == 1) // Active
    {
      this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.MerchantsList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.active', "=");
    }
    else if (this.MerchantsList_Config.ListType == 2) // Suspend
    {
      this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.MerchantsList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.suspended', "=");
    }
    else if (this.MerchantsList_Config.ListType == 3) // Blocked
    {
      this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.MerchantsList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.blocked', "=");
    }

    else if (this.MerchantsList_Config.ListType == 4) {  // InActive
      this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.MerchantsList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.inactive', "=");
    }
    else if (this.MerchantsList_Config.ListType == 5) {  // All
      this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrictFromArray(this.MerchantsList_Config.SearchBaseCondition, "StatusCode", 'number', [], "=");
    }
    else {
      this.MerchantsList_Config.DefaultSortExpression = 'CreateDate desc';
    }

    this.MerchantsList_Config = this._DataHelperService.List_Initialize(
      this.MerchantsList_Config
    );
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Cashier,
      this.MerchantsList_Config
    );
    this.MerchantsList_GetData();
  }


  MerchantsList_GetData() {
    // this.GetOverviews(this.MerchantsList_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.GetMerchantsOverview);

    var TConfig = this._DataHelperService.List_GetData(
      this.MerchantsList_Config
    );
    this.MerchantsList_Config = TConfig;
  }

  public CategoriesList_Filter_Owners_Option: Select2Options;
  public CategoriesList_Filter_Owners_Selected = 0;
  CategoriesList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetCoreCommons,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ]
    };

    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'TypeCode', this._HelperService.AppConfig.DataType.Text,
      [
        this._HelperService.AppConfig.HelperTypes.MerchantCategories,
      ]
      , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.CategoriesList_Filter_Owners_Option = {
      placeholder: 'Filter By Categories',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }

  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();
    // this.ListType = 1;
    this.MerchantsList_Setup();
    this.MerchantsList_Filter_Owners_Load();
    this.CategoriesList_Filter_Owners_Load();
    // this.MerchantsList_Filter_Stores_Load();
    // this.InitColConfig();
    // this._HelperService.StopClickPropogation();
    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.MerchantsList_GetData();
    // });
  }


}

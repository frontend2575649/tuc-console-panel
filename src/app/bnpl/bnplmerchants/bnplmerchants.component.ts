import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from 'src/app/service/service';
import * as Feather from 'feather-icons';
import { Observable, Subscription } from 'rxjs';
import swal from "sweetalert2";
import { ChangeContext } from 'ng5-slider';
declare var $: any;

@Component({
  selector: 'app-bnplmerchants',
  templateUrl: './bnplmerchants.component.html',
})
export class BnplmerchantsComponent implements OnInit {

  public ListType: number;
  public _ObjectSubscription: Subscription = null;
  public merchantData: any = {};
  public merchantOverviewData: any = {};
  public ResetFilterControls: boolean = true;

  ActiveLoanMin: number = this._HelperService.AppConfig.ActiveLoanMinLimit;
  ActiveLoanMax: number = this._HelperService.AppConfig.ActiveLoanMaxLimit;
  ClosedLoanMin: number = this._HelperService.AppConfig.ClosedLoanMinLimit
  ClosedLoanMax: number = this._HelperService.AppConfig.ClosedLoanMaxLimit;
  TUCFeesMin: number = this._HelperService.AppConfig.TUCFeesMinLimit;
  TUCFeesMax: number = this._HelperService.AppConfig.TUCFeesMaxLimit;


  constructor(
    public _Router: Router,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService: FilterHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
  ) { }

  //#region serach and filter 

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_BNPL_Merchant(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.BnplMerchant,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.BnplMerchant
        );
        this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
        this.MerchantsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetBNPLMerchantConfig(this.MerchantsList_Config);
    this.SetSalesRanges();

    this.MerchantsList_GetData();
  }

  timeout = null;
  MerchantsList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {

      this._HelperService.Update_CurrentFilterSnap(event, Type, this.MerchantsList_Config);

      this.MerchantsList_Config = this._DataHelperService.List_Operations(this.MerchantsList_Config, event, Type);


      if (event != null) {
        for (let index = 0; index < this.MerchantsList_Config.Sort.SortOptions.length; index++) {
          const element = this.MerchantsList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      // this._HelperService.Update_CurrentFilterSnap(
      //   event,
      //   Type,
      //   this.MerchantsList_Config
      // );

      // this.MerchantsList_Config = this._DataHelperService.List_Operations(
      //   this.MerchantsList_Config,
      //   event,
      //   Type
      // );

      if (
        (this.MerchantsList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.MerchantsList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);
  }

  MerchantsList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.MerchantsList_Config.Sort.SortOptions.length; index++) {
        const element = this.MerchantsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
      event.data = {
        ActiveLoanMin: this.ActiveLoanMin,
        ActiveLoanMax: this.ActiveLoanMax,
        ClosedLoanMin: this.ClosedLoanMin,
        ClosedLoanMax: this.ClosedLoanMax,
        TUCFeesMin: this.TUCFeesMin,
        TUCFeesMax: this.TUCFeesMax
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.MerchantsList_Config
    );

    this.MerchantsList_Config = this._DataHelperService.List_Operations(
      this.MerchantsList_Config,
      event,
      Type
    );

    if (
      (this.MerchantsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.MerchantsList_GetData();
    }

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this.SetSearchRanges();
    if (this.ActiveLoanMin && this.ActiveLoanMax && this.ActiveLoanMin > this.ActiveLoanMax) {
      this._HelperService.NotifyError("Mimimum active loan should be less than maximum active loan");
      return;
    }
    if (this.ActiveLoanMin < 0 || this.ActiveLoanMax < 0) {
      this._HelperService.NotifyError("Active loan should not be negative");
      return;
    }

    if (this.ClosedLoanMin && this.ClosedLoanMax && this.ClosedLoanMin > this.ClosedLoanMax) {
      this._HelperService.NotifyError("Mimimum closed loan should be less than maximum closed loan");
      return;
    }
    if (this.ClosedLoanMin < 0 || this.ClosedLoanMax < 0) {
      this._HelperService.NotifyError("Closed loan should not be negative");
      return;
    }

    if (this.TUCFeesMin && this.TUCFeesMax && this.TUCFeesMin > this.TUCFeesMax) {
      this._HelperService.NotifyError("Minimum TUC fees should be less than maximum TUC fees");
      return;
    }
    if (this.TUCFeesMin < 0 || this.TUCFeesMax < 0) {
      this._HelperService.NotifyError("TUC fees should not be negative");
      return;
    }

    this._HelperService.MakeFilterSnapPermanent();
    this.MerchantsList_GetData();
    if (ButtonType == 'Sort') {
      $("#MerchantsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#MerchantsList_fdropdown").dropdown('toggle');
    }
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
    this.SetSalesRanges();

    this.MerchantsList_GetData();
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilterUI(): void {
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  SetSalesRanges(): void {
    this.ActiveLoanMin = this.MerchantsList_Config.SalesRange.ActiveLoanMin;
    this.ActiveLoanMax = this.MerchantsList_Config.SalesRange.ActiveLoanMax;
    this.ClosedLoanMin = this.MerchantsList_Config.SalesRange.ClosedLoanMin
    this.ClosedLoanMax = this.MerchantsList_Config.SalesRange.ClosedLoanMax;
    this.TUCFeesMin = this.MerchantsList_Config.SalesRange.TUCFeesMin;
    this.TUCFeesMax = this.MerchantsList_Config.SalesRange.TUCFeesMax;
  }

  SetSearchRanges(): void {
    //#region Active loan 
    this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('ActiveLoans', this.MerchantsList_Config.SearchBaseConditions);

    var SearchCase = this._HelperService.GetSearchConditionRange('', 'ActiveLoans', this.ActiveLoanMin, this.ActiveLoanMax);
    if (this.ActiveLoanMin == this._HelperService.AppConfig.ActiveLoanMinLimit && this.ActiveLoanMax == this._HelperService.AppConfig.ActiveLoanMaxLimit) {
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.MerchantsList_Config.SearchBaseConditions);
    }
    else {
      this.MerchantsList_Config.SearchBaseConditions.push(SearchCase);
    }
    //#endregion  

    //#region Closed loan
    this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('ClosedLoans', this.MerchantsList_Config.SearchBaseConditions);

    var SearchCase = this._HelperService.GetSearchConditionRange('', 'ClosedLoans', this.ClosedLoanMin, this.ClosedLoanMax);
    if (this.ClosedLoanMin == this._HelperService.AppConfig.ClosedLoanMinLimit && this.ClosedLoanMax == this._HelperService.AppConfig.ClosedLoanMaxLimit) {
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.MerchantsList_Config.SearchBaseConditions);
    }
    else {
      this.MerchantsList_Config.SearchBaseConditions.push(SearchCase);
    }
    //#endregion  

    //#region tuc fees
    this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TUCFees', this.MerchantsList_Config.SearchBaseConditions);

    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TUCFees', this.TUCFeesMin, this.TUCFeesMax);
    if (this.TUCFeesMin == this._HelperService.AppConfig.TUCFeesMinLimit && this.TUCFeesMax == this._HelperService.AppConfig.TUCFeesMaxLimit) {
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.MerchantsList_Config.SearchBaseConditions);
    }
    else {
      this.MerchantsList_Config.SearchBaseConditions.push(SearchCase);
    }
    //#endregion  

  }


  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_bnpl_merchants(Type, index);
    this._FilterHelperService.SetStoreConfig(this.MerchantsList_Config);
  
    this.SetSalesRanges();
    this.setOtherFilters();

    this.MerchantsList_GetData();
  }

  setOtherFilters() {
    this.MerchantsList_Config.SearchBaseConditions = [];
    this.SetSearchRanges();

  }

  //#end region serach and filter 


  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);

    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: (SortExpression === "CreateDate desc") ? "MerchantName desc" : SortExpression,
      Type: null,
      ReferenceKey: null,
      ReferenceId: 0,
      SubReferenceId: 0,
      IsDownload: false
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.merchants, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.merchantData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

  MerchantsList_GetData() {
    this.GetOverviews(this.MerchantsList_Config, this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetMerchantList);
    var TConfig = this._DataHelperService.List_GetData(
      this.MerchantsList_Config
    );
    this.MerchantsList_Config = TConfig;
  }

  getMerchantListOverview() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchantListOverview,
      ReferenceKey: null,
      ReferenceId: 0,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.GetMerchantListOverview, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.merchantOverviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

  MerchantsList_RowSelected(item) {

  }

  public MerchantsList_Config: OList;
  MerchantsList_Setup() {
    this.MerchantsList_Config = {
      Id: null,
      Type: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetMerchantList,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.merchants,
      Title: "BNPL Merchants",
      StatusType: "default",
      DefaultSortExpression: "MerchantName desc",
      IsDownload: true,
      Sort: {
        SortDefaultName: null,
        SortDefaultColumn: 'MerchantName',
        SortName: null,
        SortColumn: '',
        SortOrder: 'desc',
        SortOptions: [],
      },

      TableFields: [
        {
          DisplayName: "Merchant Name",
          SystemName: "MerchantName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "MerchantAddress",
          SystemName: "MerchantAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Active Loans",
          SystemName: "ActiveLoans",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Closed Loans",
          SystemName: "ClosedLoans",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Interest Gain",
          SystemName: "TotalInterest",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "TUC Fees",
          SystemName: "TUCFees",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Merchant Settle.",
          SystemName: "MerchantSettlement",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        }, {
          DisplayName: "Action",
          SystemName: "",
          DataType: '',
          Class: "",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
      ],

    };

    this.MerchantsList_Config.ListType = this.ListType;
    // this.MerchantsList_Config.SearchBaseCondition = "";
    this.MerchantsList_Config.DefaultSortExpression = 'MerchantName desc';

    this.MerchantsList_Config = this._DataHelperService.List_Initialize(
      this.MerchantsList_Config
    );
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.BnplMerchant,
      this.MerchantsList_Config
    );
    this.MerchantsList_GetData();
  }

  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();
    this.getMerchantListOverview();
    this.MerchantsList_Setup();
  }

  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }
  }

}

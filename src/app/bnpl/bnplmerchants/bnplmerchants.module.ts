import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { FormsModule } from "@angular/forms";

import { BnplmerchantsRoutingModule } from './bnplmerchants-routing.module';
import { BnplmerchantsComponent } from './bnplmerchants.component';

@NgModule({
  declarations: [BnplmerchantsComponent],
  imports: [
    CommonModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    TranslateModule,
    FormsModule,
    BnplmerchantsRoutingModule
  ]
})
export class BnplmerchantsModule { }

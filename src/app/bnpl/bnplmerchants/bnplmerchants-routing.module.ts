import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BnplmerchantsComponent } from './bnplmerchants.component';

const routes: Routes = [
  {path: "", component:BnplmerchantsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BnplmerchantsRoutingModule { }

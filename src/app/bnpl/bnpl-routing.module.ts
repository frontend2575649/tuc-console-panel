import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EligiblecustomersComponent } from './eligiblecustomers/eligiblecustomers.component';
import { InvoiceComponent } from './invoice/invoice.component';

const routes: Routes = [
  {
    path: "",
    children: [
      { path: "", redirectTo: 'overview' },
      { path: "overview", data: { PageName: "Overview" }, loadChildren: "./bnploverview/bnploverview.module#BnploverviewModule" },
      { path: "eligiblecustomers", data: { PageName: "Eligible Customers" }, component: EligiblecustomersComponent },
      { path: "transaction", data: { PageName: "Merchant Settlements" }, loadChildren: "./merchantsettlements/merchantsettlements.module#MerchantsettlementsModule" },
      { path: "reports", data: { PageName: "Reports" }, loadChildren: "./invoicesandreports/invoicesandreports.module#InvoicesandreportsModule" },
      { path: "merchants", data: { PageName: "Merchants" }, loadChildren: "./bnplmerchants/bnplmerchants.module#BnplmerchantsModule" },
      { path: "invoice", data: { PageName: "Invoice" }, component: InvoiceComponent },
    ]
  }
]


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})


export class BnplRoutingModule { }

import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;
declare var moment: any;
import {
    DataHelperService,
    HelperService,
    OList,
    OSelect,
    FilterHelperService,
    OResponse,
} from "../../../service/service";
import { Subscription, Observable } from 'rxjs';
import * as cloneDeep from 'lodash/cloneDeep';


@Component({
    selector: "tu-tulogfeature",
    templateUrl: "./tulogfeature.component.html",
})
export class TULogFeatureComponent implements OnInit, OnDestroy {
    public ResetFilterControls: boolean = true;
    public _ObjectSubscription: Subscription = null;

    _DealConfig =
        {

            Images: [],
            StartDateConfig: {
            },
            EndDateConfig: {
            },
            DefaultStartDate: null,
            DefaultEndDate: null,
            DealImages: [],
            ReleaseDate: null,
            EndDate: null,
        }

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _FilterHelperService: FilterHelperService
    ) {
        this._HelperService.showAddNewPosBtn = false;
        this._HelperService.showAddNewStoreBtn = false;
        this._HelperService.showAddNewCashierBtn = true;
        this._HelperService.showAddNewSubAccBtn = false;


    }
    @ViewChild("offCanvas") divView: ElementRef;

    ngOnInit() {

        this._HelperService.StopClickPropogation();
        Feather.replace();
        this.InitBackDropClickEvent();
        this.RequestHistory_Filter_Stores_Load();

        this.RequestHistory_Setup();
        this.Form_Add_Load();
         this.Form_Edit_Load();
        this._HelperService.StopClickPropogation();
        // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
        //     this.RequestHistory_GetData();
        // });


        //deal date
        this._DealConfig.DefaultStartDate = moment();
        this._DealConfig.DefaultEndDate = moment().add(1, 'days').endOf("day");

        this._DealConfig.StartDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            ReleaseDate: this._HelperService.DateInUTC(moment()),
            // endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };


    }

    ngOnDestroy(): void {
        try {
            this._ObjectSubscription.unsubscribe();
        } catch (error) {

        }
    }

    // ************

    InitBackDropClickEvent(): void {
        var backdrop: HTMLElement = document.getElementById("backdrop");

        backdrop.onclick = () => {
            $(this.divView.nativeElement).removeClass('show');
            backdrop.classList.remove("show");
        };
    }

    InitColConfig() {
        var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
        var ColConfigExist: boolean =
            MerchantTableConfig != undefined && MerchantTableConfig != null;
        if (ColConfigExist) {
            this.ColumnConfig = MerchantTableConfig.config;
            this.TempColumnConfig = this._HelperService.CloneJson(
                MerchantTableConfig.config
            );
        }
    }


    TempColumnConfig: any = [
        {
            Name: "Status",
            Value: true,
        },
        {
            Name: "City",
            Value: true,
        },
        {
            Name: "Contact",
            Value: true,
        },
        {
            Name: "Stores",
            Value: true,
        },
        {
            Name: "Reward",
            Value: true,
        },
        {
            Name: "POS",
            Value: true,
        },
        {
            Name: "ActivePOS",
            Value: true,
        },
        {
            Name: "RM",
            Value: true,
        },
        {
            Name: "Added",
            Value: true,
        },
    ];

    ColumnConfig: any = [
        {
            Name: "Status",
            Value: true,
        },
        {
            Name: "City",
            Value: true,
        },
        {
            Name: "Contact",
            Value: true,
        },
        {
            Name: "Stores",
            Value: true,
        },
        {
            Name: "Reward",
            Value: true,
        },
        {
            Name: "POS",
            Value: true,
        },
        {
            Name: "ActivePOS",
            Value: true,
        },
        {
            Name: "RM",
            Value: true,
        },
        {
            Name: "Added",
            Value: true,
        },
    ];

    SaveEditCol() {
        this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
        this._HelperService.SaveStorage("BMerchantTable", {
            config: this.ColumnConfig,
        });
        this._HelperService.CloseModal("EditCol");
    }



    Delete_Confirm() {
        swal({
            position: 'top',
            //title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            title: "Delete Feature?",
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var pData = {
                    Task: this._HelperService.AppConfig.Api.Core.DeleteCoreCommon,
                    ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
                    AuthPin: result.value
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess(_Response.Message);
                            this.unclick();
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {

                        this._HelperService.HandleException(_Error);
                    });
            }
        });
    }
    //   ************
    Form_AddUser_Open() {
        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
                .MerchantOnboarding,
        ]);
    }

    //#region columnConfig
    OpenEditColModal() {
        this._HelperService.OpenModal("EditCol");
    }
    //#endregion
    //#region RequestHistory
    public CurrentRequest_Key: string;

    public RequestHistory_Config: OList;
    RequestHistory_Setup() {
        this.RequestHistory_Config = {
            Id: null,
            Task: 'getchangelogs',
            Location: this._HelperService.AppConfig.NetworkLocation.V2.LogFeature,
            Title: 'Features',
            StatusType: 'LogFeature',
            Type: 'object',
            Sort:
            {
                SortDefaultName: null,
                SortDefaultColumn: 'ReleaseDate',
                SortName: null,
                SortColumn: null,
                SortOrder: 'desc',
                SortOptions: [],
            },
            TableFields: [
                {
                    DisplayName: 'Title',
                    SystemName: 'Title',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Location Name',
                    SystemName: 'LocationName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Comment',
                    SystemName: 'Comment',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Release Date',
                    SystemName: 'ReleaseDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    IsDateSearchField: true,
                },
            ]
        };
        this.RequestHistory_Config = this._DataHelperService.List_Initialize(
            this.RequestHistory_Config
        );

        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.LogFeature,
            this.RequestHistory_Config
        );

        this.RequestHistory_GetData();

    }


    //Add feature
    Form_Add: FormGroup;
    Form_Add_Show() {
        this.Form_Add_Clear();
        this._HelperService.OpenModal('Form_Add_Content');
    }
    Form_Add_Close() {
        this._HelperService.CloseModal('Form_Add_Content');
    }
    Form_Add_Load() {
        this.Form_Add = this._FormBuilder.group({
            OperationType: 'new',
            Task: 'savechangelog',
            StatusId: 'default.inactive',
            ReleaseDate: moment().format('DD-MM-YYYY hh:mm a'),
            Title: [null, Validators.required],
            LocationName: [null],
            Log: [null],
            Comment: [null],
            VersionName: [null],

        });
    }
    Form_Add_Clear() {
        this.Form_Add.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_Add_Load();
    }


    Form_Add_Process(_FormValue: any) {
        if (_FormValue.StatusId == 'default.inactive') {
            _FormValue.StatusId = 1;
        }
        else {
            _FormValue.StatusId = 2;
        }
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _FormValue.ReleaseDate = moment(this._DealConfig.ReleaseDate).format('YYYY-MM-DD hh:mm')
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.LogFeature, _FormValue);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess("Feature Added Successfully");
                    this.RequestHistory_Setup();
                    this.Form_Add_Clear();
                    if (_FormValue.OperationType == 'edit') {
                        this.Form_Add_Close();
                        this._HelperService.AppConfig.ActiveReferenceKey = _Response.Result.ReferenceKey;
                    }

                    else if (_FormValue.OperationType == 'close') {
                        this.Form_Add_Close();
                    }
                    this.Form_Add_Close();
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    //Edit Feature
    Form_Edit: FormGroup;
    Form_Edit_Show() {
    }
    Form_Edit_Close() {
        this._HelperService.CloseModal('Form_Edit_Content');
        // this.RequestHistory_Setup();
    }
    Form_Edit_Load() {
        this.Form_Edit = this._FormBuilder.group({
            OperationType: 'edit',
            Task: 'updatechangelog',

            // ReferenceKey: this._CoreUsage.ReferenceId,
            // ReferenceId: this._CoreUsage.ReferenceKey,
             ReferenceId: null,
             ReferenceKey: null,

            StatusId: 'default.inactive',
            ReleaseDate: moment().format('DD-MM-YYYY hh:mm a'),
            Title: [null, Validators.required],
            LocationName: [null],
            Log: [null],
            Comment: [null],
            VersionName: [null],


        });
    }
    Form_Edit_Clear() {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
    }
    Form_Edit_Process(_FormValue: any) {
        var _FormValue= cloneDeep(this.Form_Edit.value);
        if (_FormValue.StatusId == 'default.inactive') {
            _FormValue.StatusId = 1;
        }
        else {
            _FormValue.StatusId = 2;
        }
          
                // _FormValue.ReferenceKey = this._HelperService._CoreCommon.ReferenceKey;
                // _FormValue.AuthPin = result.value;
                // _FormValue.ReferenceKey = this.CurrentRequest_Key;
                _FormValue.ReferenceKey= this._CoreUsage.ReferenceKey,
                _FormValue.ReferenceId= this._CoreUsage.ReferenceId,
                _FormValue.ReleaseDate= moment(this._DealConfig.ReleaseDate).format('YYYY-MM-DD hh:mm'),
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.LogFeature, _FormValue);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess('Feature Updated successful');
                            this.Form_Edit_Clear();
                            this.Form_Edit_Close();
                            this.RequestHistory_Setup();
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
           
      

    }


    ScheduleLogReleaseDate(value) {

        this._DealConfig.ReleaseDate = value.start;

        this.Form_Add.patchValue(
            {
                ReleaseDate: value.start.format('DD-MM-YYYY hh:mm a'),

            }
        );
    }


    ManageScheduleLogReleaseDate(value) {

        this._DealConfig.ReleaseDate = value.start;

        this.Form_Edit.patchValue(
            {
                ReleaseDate: value.start.format('DD-MM-YYYY hh:mm a'),

            }
        );
    }


    SetOtherFilters(): void {
        this.RequestHistory_Config.SearchBaseConditions = [];
        // this.RequestHistory_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.HelperTypes.Feature, "=");

        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
        if (CurrentIndex != -1) {
            this.RequestHistory_Filter_Stores_Selected = null;
            this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }

    //#region filterOperations

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);

        //#region setOtherFilters
        //this.SetOtherFilters();
        //#endregion

        this.RequestHistory_GetData();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_RequestHistory(Type, index);
        this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);
        if (Type == 'Time') {
            this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
            this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
        }
        this.SetOtherFilters();

        this.RequestHistory_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            input: "text",
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            inputAttributes: {
                autocapitalize: "off",
                autocorrect: "off",
                //maxLength: "4",
                minLength: "4",
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_RequestHistory(result.value);
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.LogFeature
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._HelperService.Delete_Filter(
                    this._HelperService.AppConfig.FilterTypeOption.LogFeature
                );
                this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);
                this.RequestHistory_GetData();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });

    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        this._HelperService.MakeFilterSnapPermanent();
        this.RequestHistory_GetData();

        if (ButtonType == 'Sort') {
            $("#RequestHistory_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#RequestHistory_fdropdown").dropdown('toggle');
        }

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);
        this.SetOtherFilters();

        this.RequestHistory_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    //#endregion

    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();

        this.RequestHistory_Filter_Stores_Load();

        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();
    }


    clicked() {
        $(this.divView.nativeElement).addClass('show');
        var backdrop: HTMLElement = document.getElementById("backdrop");
        backdrop.classList.add("show");
    }
    unclick() {
        $(this.divView.nativeElement).removeClass('show');
        var backdrop: HTMLElement = document.getElementById("backdrop");
        backdrop.classList.remove("show");
    }


    public RequestHistory_Filter_Stores_Option: Select2Options;
    public RequestHistory_Filter_Stores_Selected = null;
    RequestHistory_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            // AccountKey: this._HelperService.UserAccount.AccountKey,
            // AccountId: this._HelperService.UserAccount.AccountId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
            ],
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.RequestHistory_Filter_Stores_Option = {
            placeholder: "Select Store",
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    RequestHistory_Filter_Stores_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.RequestHistory_Config,
            this._HelperService.AppConfig.OtherFilters.Merchant.Owner
        );

        this.OwnerEventProcessing(event);

    }

    OwnerEventProcessing(event: any): void {
        if (event.value == this.RequestHistory_Filter_Stores_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict(
                "",
                "StoreReferenceKey",
                this._HelperService.AppConfig.DataType.Text,
                this.RequestHistory_Filter_Stores_Selected,
                "="
            );
            this.RequestHistory_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
                SearchCase,
                this.RequestHistory_Config.SearchBaseConditions
            );
            this.RequestHistory_Filter_Stores_Selected = null;
        } else if (event.value != this.RequestHistory_Filter_Stores_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict(
                "",
                "StoreReferenceKey",
                this._HelperService.AppConfig.DataType.Text,
                this.RequestHistory_Filter_Stores_Selected,
                "="
            );
            this.RequestHistory_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
                SearchCase,
                this.RequestHistory_Config.SearchBaseConditions
            );
            this.RequestHistory_Filter_Stores_Selected = event.data[0].ReferenceKey;
            this.RequestHistory_Config.SearchBaseConditions.push(
                this._HelperService.GetSearchConditionStrict(
                    "",
                    "StoreReferenceKey",
                    this._HelperService.AppConfig.DataType.Text,
                    this.RequestHistory_Filter_Stores_Selected,
                    "="
                )
            );
        }

        this.RequestHistory_ToggleOption(
            null,
            this._HelperService.AppConfig.ListToggleOption.ResetOffset
        );
    }


    RequestHistory_ToggleOption(event: any, Type: any) {
        if (Type == "date") {
            this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
            this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
        }
        if (event != null) {
            for (let index = 0; index < this.RequestHistory_Config.Sort.SortOptions.length; index++) {
                const element = this.RequestHistory_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }
            }
        }

        this._HelperService.Update_CurrentFilterSnap(
            event,
            Type,
            this.RequestHistory_Config
        );

        this.RequestHistory_Config = this._DataHelperService.List_Operations(
            this.RequestHistory_Config,
            event,
            Type
        );

        if (
            (this.RequestHistory_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.RequestHistory_GetData();
        }

    }
    RequestHistory_GetData() {

        var TConfig = this._DataHelperService.List_GetDataWithOutSort(
            this.RequestHistory_Config,
            null,
            false,
            false,
            true
        );
        this.RequestHistory_Config = TConfig;
    }
    RequestHistory_RowSelected(ReferenceData) {
        this.CurrentRequest_Key = ReferenceData.ReferenceKey;
        // this.ListAppUsage_GetDetails();
    }


    ListAppUsage_GetDetails() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommon,
            Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this.CurrentRequest_Key, '='),
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._CoreUsage = _Response.Result as OCoreUsage;
                    this._CoreUsage.CreateDateS = this._HelperService.GetDateTimeS(this._CoreUsage.CreateDate);
                    this._CoreUsage.ModifyDateS = this._HelperService.GetDateTimeS(this._CoreUsage.ModifyDate);
                    this._CoreUsage.StatusI = this._HelperService.GetStatusIcon(this._CoreUsage.StatusCode);
                    this.clicked()
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    //Edit Feature modal code start--
    EditFeature(logdata) {
        // console.log(logdata);
        this._CoreUsage=logdata;
        this.Form_Edit.controls['ReleaseDate'].setValue(moment( this._CoreUsage.ReleaseDate).format('DD-MM-YYYY hh:mm a'));
        logdata.StatusId == 2 ? this.Form_Edit.controls['StatusId'].setValue('default.active') : this.Form_Edit.controls['StatusId'].setValue('default.inactive') ;
          
          var tStartDate = moment(this._CoreUsage.ReleaseDate);
        
          this._DealConfig.ReleaseDate = tStartDate;
          
        // console.log(this._CoreUsage);
        this._HelperService.OpenModal('Form_Edit_Content');
    }
    //Edit Feature modal code end--


    DeleteLogRequest(): void {
        // this.PromoteDeal = {};
        // this.detetepromoteId = this._CoreUsage.ReferenceId;
        // this.detetepromoteKey = this._CoreUsage.ReferenceKey;

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.RemoveLogFeatureTitle,
            text: this._HelperService.AppConfig.CommonResource.RemoveLogFeatureHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {

                this._HelperService.IsFormProcessing = true;
                var PData =
                {
                    Task: 'deletechangelog',
                    ReferenceId: this._CoreUsage.ReferenceId,
                    ReferenceKey: this._CoreUsage.ReferenceKey,
                    // StatusCode: this.selectedStatusItem.statusCode,

                }
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.LogFeature, PData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Log Feature Deleted Successfully.");
                            this.RequestHistory_Setup();
                            // this._HelperService.CloseModal('FlashDeal');
                            this._HelperService.IsFormProcessing = false;

                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    }
                    ,
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                        this._HelperService.ToggleField = false;
                    });

            }
        });

    }


    public _CoreUsage: OCoreUsage =
        {
            Count: 0,
            CreateDate: null,
            CreatedByDisplayName: null,
            CreatedByIconUrl: null,
            CreatedByKey: null,
            Data: null,
            Description: null,
            PlatformCharge: null,
            HelperCode: null,
            HelperName: null,
            IconUrl: null,
            ModifyByDisplayName: null,
            ModifyByIconUrl: null,
            ModifyByKey: null,
            ModifyDate: null,
            Name: null,
            ParentCode: null,
            ParentKey: null,
            ParentName: null,
            PosterUrl: null,
            Reference: null,
            ReferenceKey: null,
            ReferenceId: null,
            Sequence: null,
            SubItemsCount: null,
            StatusCode: null,
            StatusId: null,
            StatusName: null,
            SubParentCode: null,
            SubParentKey: null,
            SubParentName: null,
            SubValue: null,
            SystemName: null,
            TypeCode: null,
            TypeName: null,
            UserAccountDisplayName: null,
            UserAccountIconUrl: null,
            UserAccountKey: null,
            Value: null,
            StatusI: null,
            CreateDateS: null,
            ModifyDateS: null,

         

            ReleaseDate: null,
            Title: null,
            LocationName: null,
            Log: null,
            Comment: null,
            VersionName: null,
        }
}




export class OCoreUsage {
    public Reference: string;
    public ReferenceKey: string;
    public ReferenceId: string;
    public SystemName: string;
    public UserAccountKey: string;
    public UserAccountDisplayName: string;
    public UserAccountIconUrl: string;
    public TypeCode: string;
    public TypeName: string;
    public HelperCode: string;
    public HelperName: string;
    public ParentKey: string;
    public ParentCode: string;
    public ParentName: string;
    public SubParentKey: string;
    public SubParentCode: string;
    public SubParentName: string;
    public PlatformCharge?: any;
    public Name: string;
    public Value: string;
    public SubValue: string;
    public Description: string;
    public Data: string;
    public Sequence: number = 0;
    public Count: number = 0;
    public SubItemsCount: number = 0;
    public IconUrl: string;
    public PosterUrl: string;
    public CreateDate: Date;
    public CreatedByKey: string;
    public CreatedByDisplayName: string;
    public CreatedByIconUrl: string;
    public ModifyDate: Date;
    public ModifyByKey: string;
    public ModifyByDisplayName: string;
    public ModifyByIconUrl: string;
    public StatusId: number = 0;
    public StatusCode: string;
    public StatusName: string;
    public StatusI: string;
    public CreateDateS: string;
    public ModifyDateS: string;


 

    public ReleaseDate: number;
    public Title: string;
    public LocationName: string;
    public Log: string;
    public Comment: string;
    public VersionName: string;
}


import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;
declare var moment: any;
import { WizardComponent } from 'angular-archwizard';
import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
  OCoreCommon
} from "../../../service/service";
import { Subscription, Observable } from 'rxjs';
import { InputFile, InputFileComponent } from 'ngx-input-file';
import { ImageCroppedEvent } from 'ngx-image-cropper';
@Component({
  selector: "tu-tudealcategories",
  templateUrl: "./tudealcategories.component.html",
})
export class TUDealCategoriesComponent implements OnInit, OnDestroy {
  @ViewChild("mago") wizard: WizardComponent;
  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;
  dealCategoryId: any;
  rootCategoryId: any;
  ReferenceId: any;
  ReferenceKey: any;
  RootCategoryId: any;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = true;
    this._HelperService.showAddNewSubAccBtn = false;


  }
  CurrentImagesCount: number = 0;
  ShowImagePicker: boolean = true;
  public _dealsubcategeory: any[] = [];
  public Root_Category_Option: Select2Options;


  @ViewChild("offCanvas") divView: ElementRef;

  @ViewChild("addimage")
  private InputFileComponent_Add: InputFileComponent;

  @ViewChild("EditImage")
  private InputFileComponent_Edit: InputFileComponent;

  @ViewChild("inputfile")
  private InputFileComponent: InputFileComponent;

  ngOnInit() {

    //   $('#wizard1').steps({
    //     headerTag: 'h3',
    //     bodyTag: 'section',
    //     autoFocus: true,
    //     enablePagination: false,
    //     titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
    //     onFinished: (event, currentIndex) => {
    //         //this.Form_AddUser_Process();
    //     },
    //     labels: {
    //         next: "Save",
    //     }
    // });

    $('#wizard1').steps({
      headerTag: 'h3',
      bodyTag: 'section',
      autoFocus: true,
      enablePagination: false,
      titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
      onFinished: (event, currentIndex) => {
        //this.Form_AddUser_Process();
      },
      labels: {
        next: "Save",
      }
    });
    this._HelperService.StopClickPropogation();

    Feather.replace();
    this._HelperService.ValidateData();

    this.InitBackDropClickEvent();
    this.MerchnatCategories_Setup();
    this.InitColConfig();
    this.Form_Add_Load();
    this.Form_Addsubcat_Load();
    this.Form_Edit_Load();
    this.Root_Category_Load();
    // this._HelperService.StopClickPropogation();
    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.MerchnatCategories_GetData();
    // });
  }

  ResetImagePlaceholder: boolean = true
  ResetImage(): void {
    this.ResetImagePlaceholder = false;
    this._ChangeDetectorRef.detectChanges();
    this.ResetImagePlaceholder = true;
    this._ChangeDetectorRef.detectChanges();
  }


  private InitImagePickerAdd(InputFileComponent: InputFileComponent) {
    if (InputFileComponent != undefined) {
      this.CurrentImagesCount = 0;
      this._HelperService._InputFileComponent = InputFileComponent;
      InputFileComponent.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
  }

  RequestHistory_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveDealRootCategory,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        RootCategoryKey: ReferenceData.RootCategoryKey,
        RootCategoryId: ReferenceData.RootCategoryId,
        DisplayName: ReferenceData.Name,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      }
    );
    
  this._HelperService.AppConfig.ActiveReferenceKey = ReferenceData.ReferenceKey;
  this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
  this._Router.navigate([
    this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Administration.DealSubCategories,
    ReferenceData.ReferenceKey,
    ReferenceData.ReferenceId,


  ]);
  }

  private InitImagePicker(previewurl?: string) {
    if (this.InputFileComponent != undefined) {
      this.CurrentImagesCount = 0;
      if (previewurl) {
        this.InputFileComponent.files[0] = {};
        this.InputFileComponent.files[0].preview = previewurl;
      }
      this._HelperService._InputFileComponent = this.InputFileComponent;
      this.InputFileComponent.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(this.InputFileComponent.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
  }



  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }
  }
  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  public _MerchantDetails: any =
    {
      "ReferenceId": null,
      "ReferenceKey": null,
      "DisplayName": null,
      "EmailAddress": null,
      "IconUrl": null,
      "AccountCode": null,
      "WebsiteUrl": null,
      "Address": {
        "Address": null,
        "Latitude": null,
        "Longitude": null
      },
      "ContactPerson": {
        "MobileNumber": null,
        "EmailAddress": null
      },
      "Categories": [
        {
          "ReferenceId": null,
          "ReferenceKey": null,
          "Name": null,
          "IconUrl": null
        }
      ],
      "Stores": null,
      "Terminals": null,
      "Cashiers": null,
      "CreateDate": null,
      "StatusCode": null,
      "StatusName": null

    }

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region Categories
  public CurrentRequest_Key: string;

  public MerchnatCategories_Config: OList;
  MerchnatCategories_Setup() {
    this.MerchnatCategories_Config = {
      Id: null,
      Type: "all",
      Sort: null,
      Task: this._HelperService.AppConfig.Api.Core.getrootcategories,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.DealCategory,
      Title: 'Deal Categories',
      StatusType: 'default',
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.HelperTypes.MerchantCategories, "="),
      TableFields: [
        {
          DisplayName: 'Name',
          SystemName: 'Name',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.ModifyDate,
          SystemName: 'ModifyDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
      ]

    };
    this.MerchnatCategories_Config = this._DataHelperService.List_Initialize(
      this.MerchnatCategories_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Cashier,
      this.MerchnatCategories_Config
    );

    this.MerchnatCategories_GetData();
  }
  MerchnatCategories_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.MerchnatCategories_Config.Sort.SortOptions.length; index++) {
        const element = this.MerchnatCategories_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }
    if (Type == "date") {
      this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
      this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
  }
    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.MerchnatCategories_Config


    );

    this.MerchnatCategories_Config = this._DataHelperService.List_Operations(
      this.MerchnatCategories_Config,
      event,
      Type
    );

    if (
      (this.MerchnatCategories_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.MerchnatCategories_GetData();
    }

  }
  timeout = null;
  MerchnatCategories_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.MerchnatCategories_Config.Sort.SortOptions.length; index++) {
          const element = this.MerchnatCategories_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.MerchnatCategories_Config


      );

      this.MerchnatCategories_Config = this._DataHelperService.List_Operations(
        this.MerchnatCategories_Config,
        event,
        Type
      );

      if (
        (this.MerchnatCategories_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.MerchnatCategories_GetData();
      }

    }, this._HelperService.AppConfig.SearchInputDelay);

  }

  Root_Category_Change(event: any) {
    this.rootCategoryId = event.value
    this.Form_Add.patchValue(
      {
        RootCategoryId: event.data[0].ReferenceId,
        RootCategoryKey: event.data[0].ReferenceKey,
      }
    );
  }

  MerchnatCategories_GetData() {

    var TConfig = this._DataHelperService.List_GetData(
      this.MerchnatCategories_Config
    );
    this.MerchnatCategories_Config = TConfig;
    
  }
  // MerchnatCategories_RowSelected(ReferenceData) {
  //   var ReferenceKey = ReferenceData.ReferenceKey;
  //   this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
  //   this.MerhchantCategories_GetDetails();
  // }

  MerchnatCategories_RowSelected(ReferenceData) {
    var ReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    // this.MerhchantCategories_GetDetails();
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.DealRequesthistory, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
    // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.FAQs, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
  }

  //#endregion

  MerchantCategories_GetDetails(Refid, Refkey) {
    var pData = {
      Task: 'getdealcategory',
      ReferenceId: Refid,
      ReferenceKey: Refkey
    };
    // console.log(pData);
    
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.DealCategory, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._CoreUsage = _Response.Result;
          this._ChangeDetectorRef.detectChanges();
          this._HelperService.OpenModal('Form_Edit_Content');

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      });
  }
  Form_Addsubcat: FormGroup;
  Form_Addsubcat_Show() {
    this.Form_Addsubcat_Clear();
    this._HelperService.Icon_Crop_Clear();
    this._HelperService.OpenModal('Form_Addsubcat_Content');
    setTimeout(() => {
      this.InitImagePickerAdd(this.InputFileComponent_Add);
    }, 100);

  }
  Form_Addsubcat_Close() {
    this.Emails = [];
    this.Form_Addsubcat_Clear();
    this.Form_Add_Clear();
    this.Form_Add_Load();
    this._HelperService.Icon_Crop_Clear();
    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
    this._HelperService.CloseModal('Form_managecat');

  }

  Updatecategeory(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.updatecategory,
      // Task: this._HelperService.AppConfig.Api.Core.GetUserAccount,
      ReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      Name: _FormValue.Name,
      StatusCode: _FormValue.StatusCode,
      IconContent: this._HelperService._Icon_Cropper_Data,
      ReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System1, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;

          //#region RelocateMarker 
          this._HelperService.NotifySuccess(_Response.Message);
          this._HelperService.CloseModal('Form_managecat');
          // this.MerchnatCategories_Setup();
          this.GetMerchantDetails();

          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  Form_Addsubcat_Load() {
    this._HelperService._Icon_Cropper_Data.Width = 128;
    this._HelperService._Icon_Cropper_Data.Height = 128;
    this._HelperService.Icon_Crop_Clear();
    this.InitImagePickerAdd(this.InputFileComponent_Add);
    this.Form_Addsubcat = this._FormBuilder.group({
      //   AllowDuplicateName: false,
      //   OperationType: 'new',
      //   Task: this._HelperService.AppConfig.Api.Core.SaveCategory,
      //   // TypeCode: this._HelperService.AppConfig.HelperTypes.MerchantCategories,
      //   Name: [null, Validators.required],
      //  percentage: [null],
      //   subcategeory: [null],
      //   Sequence: 0,
      //   StatusCode: 'default.inactive',
      //   IconContent: this._HelperService._Icon_Cropper_Data


      AllowDuplicateName: false,
      OperationType: 'new',
      //ReferenceId:0,
      CategoryId: 0,
      CategoryKey: [null],
      //ReferenceKey: this._HelperService._CoreParameter.ReferenceKey,
      Task: this._HelperService.AppConfig.Api.Core.savesubcategories,
      Name: [null],
      StatusCode: 'default.inactive',
      Subcategories: this.Subcat,
      //Commission:[null,Validators.required],
      // IconContent: this._HelperService._Icon_Cropper_Data
    });
  }

  // Form_Add_Load() {
  //   this._HelperService._Icon_Cropper_Data.Width = 128;
  //   this._HelperService._Icon_Cropper_Data.Height = 128;
  //   this._HelperService.Icon_Crop_Clear();
  //   this.InitImagePickerAdd(this.InputFileComponent_Add);
  //   this.Form_Add = this._FormBuilder.group({
  //     AllowDuplicateName: false,
  //     OperationType: 'new',
  //     Task: this._HelperService.AppConfig.Api.Core.SaveCategory,
  //     // TypeCode: this._HelperService.AppConfig.HelperTypes.MerchantCategories,
  //     Name: [null, Validators.required],
  //     Commission:[null,Validators.required],
  //     //Sequence: 0,
  //     Subcategories:[null],
  //     StatusCode: 'default.inactive',
  //     IconContent: this._HelperService._Icon_Cropper_Data
  //   });
  // }
  Form_Addsubcat_Clear() {
    this.Form_Addsubcat.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_Addsubcat_Load();
    this._HelperService._FileSelect_Icon_Reset();
  }
  arraydata1: any;
  arrayimages = document.getElementById("file");
  createImageNode() {
    var img = document.createElement('img');
    img.src = "https://insider.directv.com/wp-content/uploads/2012/07/nemo_big.jpg";
    // img.width = "300";
    img.style.margin = "15px";
    return img;
  }
  Form_Addsubcat_Process(value?: any) {
    this.uploadFiles();
    // this.Form_Addsubcat.value.IconContent = this._HelperService._Icon_Cropper_Data;
    //this._subcategeoryContent_Data= this.Form_Add.controls['Subcategories'].value.split(' ');
    // this._subcategeoryContent_Data = this.Emails;
    //  this.arraydata1=this.Emails.map(el=>({Name:el,IconContent: {
    //   "Name": null,
    //   "Extension": null,
    //   //"Content": this.Form_Addsubcat.value.IconContent.Content=this.croppedImage1
    //   "Content": this.myFiles.forEach(img => {
    //     this.arrayimages.appendChild(this.createImageNode());
    // })
    // },}))
    this.Form_Addsubcat.value.CategoryId = this.CategeoryResponse1.ReferenceId
    this.Form_Addsubcat.value.CategoryKey = this.CategeoryResponse1.ReferenceKey
    //this.Form_Addsubcat.value.Name=this.CategeoryResponse1.Name
    this.Form_Addsubcat.value.Name = null
    //  this.Form_Addsubcat.value.Description=this.CategeoryResponse.CategoryId
    this.Form_Addsubcat.value.StatusCode = "default.active"

    this.Form_Addsubcat.value.Subcategories = this.Subcat;
    // if (this._HelperService._Icon_Cropper_Data.Content != null) {
    //   _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
    // }

    // if (this._subcategeoryContent_Data.Name != null) {
    //   _FormValue.Subcategories = this._subcategeoryContent_Data;
    // }
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System1, this.Form_Addsubcat.value);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          localStorage.setItem("SubCatData1", JSON.stringify(_Response.Result));
          this._HelperService.NotifySuccess("SubCategories Added Successfully");
          this.Emails = [];
          this.InputFileComponent_Add = null;
          this.ResetImage();
          this._HelperService._FileSelect_Icon_Reset();
          this.MerchnatCategories_Setup()
          this.Form_Addsubcat_Clear();
          $("#wizard1").steps("next", {});
          this._HelperService.Icon_Crop_Clear();
          this.Form_Addsubcat_Close();
          if (this.Form_Addsubcat.value.OperationType == 'edit') {
            this.Form_Addsubcat_Close();
            this._HelperService.AppConfig.ActiveReferenceKey = _Response.Result.ReferenceKey;
            // this.MerhchantCategories_GetDetails();
            this._HelperService.Icon_Crop_Clear();

            // this._Router.navigate([this._HelperService.AppConfig.Pages.System.AddCoreHelpers, _Response.Result.ReferenceKey]);
          }
          else if (this.Form_Addsubcat.value.OperationType == 'close') {
            this.Form_Addsubcat_Close();
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  //form add sub-categeory
  Form_Add: FormGroup;
  Form_Add_Show() {
    this.Form_Add_Clear();
    this._HelperService.Icon_Crop_Clear();
    this._HelperService.OpenModal('Form_Add_Content');
    setTimeout(() => {
      this.InitImagePickerAdd(this.InputFileComponent_Add);
    }, 100);

  }
  isDealRootCategory:boolean=true;
  Form_Add_Close() {

    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
    this._HelperService.CloseModal('Form_Add_Content');
    this.isDealRootCategory=false;
    setTimeout(()=>{
      this.isDealRootCategory=true;
    },500);
    this.Form_Add_Clear();
  }
  Form_Add_Load() {
    this._HelperService._Icon_Cropper_Data.Width = 128;
    this._HelperService._Icon_Cropper_Data.Height = 128;
    this._HelperService.Icon_Crop_Clear();
    this.InitImagePickerAdd(this.InputFileComponent_Add);
    this.Form_Add = this._FormBuilder.group({
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.savedealcategory,
      RootCategoryId: [null, Validators.required],
      RootCategoryKey : [null, Validators.required],
      Name: null,
      Commission: [null, Validators.compose([Validators.required, Validators.min(1), Validators.max(100)])],
      StatusCode: 'default.active',
      IconContent: this._HelperService._Icon_Cropper_Data
    });

  }


  Root_Category_Load() {

    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.getcategories,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Categories,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
      ]
    };

    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.Root_Category_Option = {
      placeholder: 'Select Category',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  // Form_Add_Load() {
  //   this._HelperService._Icon_Cropper_Data.Width = 128;
  //   this._HelperService._Icon_Cropper_Data.Height = 128;
  //   this._HelperService.Icon_Crop_Clear();
  //   this.InitImagePickerAdd(this.InputFileComponent_Add);
  //   this.Form_Add = this._FormBuilder.group({
  //     AllowDuplicateName: false,
  //     OperationType: 'new',
  //     Task: this._HelperService.AppConfig.Api.Core.SaveCategory,
  //     // TypeCode: this._HelperService.AppConfig.HelperTypes.MerchantCategories,
  //     Name: [null, Validators.required],
  //     Commission:[null,Validators.required],
  //     //Sequence: 0,
  //     Subcategories:[null],
  //     StatusCode: 'default.inactive',
  //     IconContent: this._HelperService._Icon_Cropper_Data
  //   });
  // }
  Form_Add_Clear() {
    this.Form_Add.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_Add_Load();
    this._HelperService._FileSelect_Icon_Reset();
  }
  arraydata: any;
  // Form_Add_Process(_FormValue: any) {
  //   _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
  //   //this._subcategeoryContent_Data= this.Form_Add.controls['Subcategories'].value.split(' ');
  //  // this._subcategeoryContent_Data = this.Emails;
  //  this.arraydata=this.Emails.map(el=>({Name:el}))

  //  this.Form_Add.value.Subcategories = this.arraydata;
  //   if (this._HelperService._Icon_Cropper_Data.Content != null) {
  //     this.Form_Add.value.IconContent = this._HelperService._Icon_Cropper_Data;
  //   }

  //   // if (this._subcategeoryContent_Data.Name != null) {
  //   //   _FormValue.Subcategories = this._subcategeoryContent_Data;
  //   // }
  //   this._HelperService.IsFormProcessing = true;
  //   let _OResponse: Observable<OResponse>;
  //   _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System1, this.Form_Add.value);
  //   _OResponse.subscribe(
  //     _Response => {
  //       this._HelperService.IsFormProcessing = false;
  //       if (_Response.Status == this._HelperService.StatusSuccess) {
  //         this._HelperService.NotifySuccess("Categories Added Successfully");
  //         this.Emails=[];
  //         this.InputFileComponent_Add = null;
  //         this.ResetImage();
  //         this._HelperService._FileSelect_Icon_Reset();
  //         this.MerchnatCategories_Setup()
  //         this.Form_Add_Clear();
  //         $("#wizard1").steps("next", {});
  //         this._HelperService.Icon_Crop_Clear();
  //         if (this.Form_Add.value.OperationType == 'edit') {
  //           this.Form_Add_Close();
  //           this._HelperService.AppConfig.ActiveReferenceKey = _Response.Result.ReferenceKey;
  //           this.MerhchantCategories_GetDetails();
  //           this._HelperService.Icon_Crop_Clear();

  //           // this._Router.navigate([this._HelperService.AppConfig.Pages.System.AddCoreHelpers, _Response.Result.ReferenceKey]);
  //         }
  //         else if (this.Form_Add.value.OperationType == 'close') {
  //           this.Form_Add_Close();
  //         }
  //       }
  //       else {
  //         this._HelperService.NotifyError(_Response.Message);
  //       }
  //     },
  //     _Error => {
  //       this._HelperService.IsFormProcessing = false;
  //       this._HelperService.HandleException(_Error);
  //     });
  // }
  //end form add
  CategeoryResponse: any;
  Form_AddApp_Process(_FormValue: any) {
    this._HelperService.ToggleField = true;
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.DealCategory, _FormValue);

    _OResponse.subscribe(
      _Response => {
        this._HelperService.ToggleField = false;
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Category added successfully");
          this.MerchnatCategories_GetData();
          this.isDealRootCategory=false;
          setTimeout(()=>{
            this.isDealRootCategory=true;
          },500);
          this.Form_Add_Clear();
          if (_FormValue.OperationType == 'edit') {
            this.Form_Add_Close();
            // this._Router.navigate([this._HelperService.AppConfig.Pages.System.AddCoreHelpers, _Response.Result.ReferenceKey]);
          }
          else if (_FormValue.OperationType == 'close') {
            _FormValue.OperationType === 'new';
            this.Form_Add_Close();
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  makeOptionalStep(step) {
    this.wizard.model.getStepAtIndex(step).optional = true;
    this.wizard.navigation.goToStep(step + 1);
  }

  Form_Edit: FormGroup;
  Form_Edit_Show(ReferenceData) {
    this.ReferenceId= ReferenceData.ReferenceId;
    this.ReferenceKey= ReferenceData.ReferenceKey;
    this.RootCategoryId = ReferenceData.RootCategoryId;
    
    
    this.MerchantCategories_GetDetails( this.ReferenceId,this.ReferenceKey);
  }
  Form_Edit_Close() {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
    // this.unclick();
    this.Form_Edit_Clear();
    this.MerchnatCategories_Setup();
    this._HelperService.CloseModal('Form_Edit_Content');
  }

  // _subcategeoryContent_Data: subcategeoryContent = {
  //   Name: null,
  //  Value:"123",
  // };

  public TUTr_Config: OList;
  TUTr_Setup() {

    this.TUTr_Config = {
        Id: null,
        Sort: null,
        Task: this._HelperService.AppConfig.Api.Core.getrootcategories,
        Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.DealCategory,
        Title: "Deal",
        StatusType: "AppPromots",
       
        DefaultSortExpression: "CreateDate desc",
        //SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'dealcode.unused', '=='),
        TableFields: [
          {
            DisplayName: 'Name',
            SystemName: 'Name',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: true,
            Search: true,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
            SystemName: 'CreateDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
            IsDateSearchField: true,
          },
          {
            DisplayName: this._HelperService.AppConfig.CommonResource.ModifyDate,
            SystemName: 'ModifyDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
        ]
    };

    this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
    this._HelperService.Active_FilterInit(
        this._HelperService.AppConfig.FilterTypeOption.SoldHistory,
        this.TUTr_Config
    );
    this.TUTr_GetData();
}

TUTr_GetData() {
  // this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.ThankUCash.GetPurchaseHistoryOverview);
  var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
  this.TUTr_Config = TConfig;
}
  Form_Edit_Load() {
    // this.InitImagePicker(this.InputFileComponent_Edit);

    this.Form_Edit = this._FormBuilder.group({
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.updatedealcategory,
      ReferenceId: this.ReferenceId,
      ReferenceKey: this.ReferenceKey,
      Name: null,
      StatusCode: [this._HelperService.AppConfig.StatusCode, Validators.required],
      RootCategoryId: this.RootCategoryId,
      IconContent: this._HelperService._Icon_Cropper_Data,
      Commission:  [null, Validators.compose([Validators.required, Validators.min(1), Validators.max(100)])],
    });
  }
  Form_Edit_Clear() {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
  }
  Form_Edit_Process(_FormValue: any) {
    _FormValue.ReferenceKey = this.ReferenceKey;
    _FormValue.ReferenceId = this.ReferenceId;
    _FormValue.RootCategoryId = this.RootCategoryId;
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.DealCategory, _FormValue);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.Form_Edit_Clear();
          this.Form_Edit_Close();
          this.MerchnatCategories_GetData();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });



  }

  categoryDeal: any = {};
  detetecategoryId: any;
  detetecategoryKey: any;

  UnMarkAscategory(ReferenceData: any): void {
    this.categoryDeal = {};
    this.detetecategoryId = ReferenceData.ReferenceId;
    this.detetecategoryKey = ReferenceData.ReferenceKey;

    swal({
        position: "center",
        title: this._HelperService.AppConfig.CommonResource.RemoveAppCategoryTitle,
        text: this._HelperService.AppConfig.CommonResource.RemoveCategoryHelp,
        animation: false,
        customClass: this._HelperService.AppConfig.Alert_Animation,
        showCancelButton: true,
        confirmButtonColor: this._HelperService.AppConfig.Color_Red,
        cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
        confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
        cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
        if (result.value) {

            this._HelperService.IsFormProcessing = true;
            var PData =
            {
                Task: this._HelperService.AppConfig.Api.ThankUCash.deletedealcategory,
                ReferenceId: this.detetecategoryId,
                ReferenceKey: this.detetecategoryKey,
                // StatusCode: this.selectedStatusItem.statusCode,

            }
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.DealCategory, PData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess("Category Deleted Successfully.");
                        this.TUTr_Setup();
                        // this._HelperService.CloseModal('FlashDeal');
                        this._HelperService.IsFormProcessing = false;

                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                }
                ,
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                    this._HelperService.ToggleField = false;
                });

        }
    });

}

EditMerchant() {
  this._HelperService.OpenModal("Form_Edit_Content");
  // this._Router.navigate([
  //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Profile, this._HelperService.AppConfig.ActiveMerchantReferenceKey,
  //   this._HelperService.AppConfig.ActiveMerchantReferenceId
  // ]);
  // this._HelperService.CloseAllModal();

}

  SetOtherFilters(): void {
    this.MerchnatCategories_Config.SearchBaseConditions = [];
    // this.MerchnatCategories_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.HelperTypes.MerchantCategories, "=");

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetRequestHistoryConfig(this.MerchnatCategories_Config);

    //#region setOtherFilters
    // this.SetOtherFilters();
    //#endregion

    this.MerchnatCategories_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_RequestHistory(Type, index);
    this._FilterHelperService.SetRequestHistoryConfig(this.MerchnatCategories_Config);
    if (Type == 'Time') {
      this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
      this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
  }
    this.SetOtherFilters();
    this.MerchnatCategories_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_RequestHistory(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Cashier
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Cashier
        );
        this._FilterHelperService.SetRequestHistoryConfig(this.MerchnatCategories_Config);
        this.MerchnatCategories_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.MerchnatCategories_GetData();

    if (ButtonType == 'Sort') {
      $("#MerchnatCategories_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#MerchnatCategories_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetRequestHistoryConfig(this.MerchnatCategories_Config);
    this.SetOtherFilters();

    this.MerchnatCategories_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }



  clicked() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

  Delete_Confirm() {
    swal({
      //title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      title: "Delete Business Categories?",
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      input: 'text',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
      inputClass: 'swalText',
      inputValidator: function (value) {
        if (value === '' || value.length < 4) {
          return 'Enter your 4 digit pin!'
        }
      },
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;
        var pData = {
          Task: this._HelperService.AppConfig.Api.Core.DeleteCoreCommon,
          ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
          AuthPin: result.value
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this.Form_Edit_Close();
              this._HelperService.NotifySuccess("Category Deleted Successfully");
              this._HelperService.CloseModal('ModalDetails');
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {

            this._HelperService.HandleException(_Error);
          });
      }
    });
  }

  public _CoreUsage: OCoreUsage =
    {
      Count: 0,
      CreateDate: null,
      CreatedByDisplayName: null,
      CreatedByIconUrl: null,
      CreatedByKey: null,
      Commission: null,
      Data: null,
      Description: null,
      PlatformCharge: null,
      HelperCode: null,
      HelperName: null,
      IconUrl: null,
      ModifyByDisplayName: null,
      ModifyByIconUrl: null,
      ModifyByKey: null,
      ModifyDate: null,
      Name: null,
      ParentCode: null,
      ParentKey: null,
      ParentName: null,
      PosterUrl: null,
      Reference: null,
      ReferenceKey: null,
      ReferenceId: null,
      Sequence: null,
      SubItemsCount: null,
      StatusCode: null,
      StatusId: null,
      StatusName: null,
      SubParentCode: null,
      SubParentKey: null,
      SubParentName: null,
      SubValue: null,
      SystemName: null,
      TypeCode: null,
      TypeName: null,
      UserAccountDisplayName: null,
      UserAccountIconUrl: null,
      UserAccountKey: null,
      Value: null,
      StatusI: null,
      CreateDateS: null,
      ModifyDateS: null

    }

  //deal code--
  // AddDealsubcategeory(): void {


  //     var terminal: any = {
  //         OperationType: 'new',
  //         Task: this._HelperService.AppConfig.Api.Core.SaveTerminal,
  //         TerminalId: this.Form_AddTerminal.controls['TerminalId'].value,
  //         MerchantId: this._SavedMerchant.ReferenceId,
  //         MerchantKey: this._SavedMerchant.ReferenceKey,
  //         StoreId: this._SavedStore.ReferenceId,
  //         StoreKey: this._SavedStore.ReferenceKey,
  //         ProviderId: this._ProviderId,
  //         ProviderKey: this._ProviderKey,
  //         AcquirerId: this._HelperService.AppConfig.ActiveReferenceId,
  //         AcquirerKey: this._HelperService.AppConfig.ActiveReferenceKey,
  //         StatusCode: "default.active"
  //     };

  //     var ElementIndex: number = null;
  //     // for (let index = 0; index < this._TerminalList.length; index++) {
  //     //     if (this._TerminalList[index].ReferenceId == this._SelectedTerminal.ReferenceId) {
  //     //         ElementIndex = index;
  //     //         break;
  //     //     }
  //     // }

  //     for (let index = 0; index < this._TerminalList.length; index++) {
  //         if (this._TerminalList[index].TerminalId == terminal.TerminalId) {
  //             ElementIndex = index;
  //             break;
  //         }
  //     }

  //     if (ElementIndex == null) {
  //         // this._SelectedTerminal.element = undefined;
  //         this._TerminalList.push(terminal);
  //     }


  // }
  //end deal code--
  Emails: any[] = [];
  subcategeory: string = '';
  //   Configuration: any = {
  //     subcategeory: null


  // }
  num = 0;
  Subcategories = [];
  KeyUP(): void {
    // this.Emails = this.Form_Add.controls['subcategeory'].value.split(' ');
    this.Emails = this.Form_Addsubcat.controls['Subcategories'].value.split(' ');
    for (let i = 0; i < this.Emails.length; i++) {
      this.Subcategories.push({
        "Name": this.Form_Addsubcat.controls['Subcategories'].value.split(' ')[i],
        //"Description": this.formData.inputfield

      })
    }



  }

  KeyUP1(): void {
    // this.Emails = this.Form_Add.controls['subcategeory'].value.split(' ');
    this.Emails = this.Form_Add.controls['Name'].value.split(',');



  }
  //const subcatdata = string[];
  subcatdata = new Array();
  //SubCatData:string[] = [];
  AddSubcategeory() {
    this.subcatdata = this.Emails;
    localStorage.setItem("SubCatData", JSON.stringify(this.subcatdata));
    this.gotonextstep();
    // this.showSubcategeory();
  }
  public ShowSubCat_Config = new Array();
  showSubcategeory() {
    this.ShowSubCat_Config = JSON.parse(localStorage.getItem('SubCatData'))
  }

  gotonextstep() {
    $("#wizard1").steps("next", {});
    this.showSubcategeory();
  }

  //image code---
  Imagechanged: string[] = [];
  imageChangedEvent: any = '';
  croppedImage: any = '';
  croppedImage1: any = ''
  urls = new Array<string>();

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    //this.getdata();
  }

  fileChangeEvent1(event: any): void {
    this.imageChangedEvent = event;
    //this.getdata();
  }

  onImgError(event) {
    event.target.src = "https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/defaults/defaulticon.png";
  }

  detectFiles(event) {
    this.urls = [];
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.urls.push(e.target.result);
        }
        reader.readAsDataURL(file);
      }
    }
  }
  myFiles: any[] = [];
  myFiles1: string[] = [];
  sMsg: string = '';
  sub_cat_ind: any
  // getFileDetails (e) {
  // // this.sub_cat_ind = this.Subcategories.findIndex(el => el['Name'] === e.target.files.IconName);
  //   for (var i = 0; i < e.target.files.length; i++) { 
  //     this.myFiles.push(e.target.files[i],);

  //   }
  //   this.sub_cat_ind=this.myFiles;
  // }


  Subcat: any[] = [];
  uploadFiles() {
    //       const frmData = new FormData();

    //       // for (var i = 0; i < this.myFiles.length; i++) { 
    //       //   frmData.append("fileUpload", this.myFiles[i]);
    //       // }




    //       this.Subcategories[this.sub_cat_ind]['IconContent'] = {
    //         "Name": this.Subcategories[this.sub_cat_ind]['Name'],
    //        // "Extension": this.formData.files.extension,
    //        // "Content": [idr jo pugin use kiya hai uska data]
    // }

    this.localimg = this.images;
    localStorage.setItem("localimg", JSON.stringify(this.localimg));
    //$("#wizard1").steps("next", {});
    for (let i = 0; i < this.ShowSubCat_Config.length; i++) {
      // for(let j=0;j<this.images.length;j++)
      // {
      this.Subcat.push({ Name: this.ShowSubCat_Config[i], IconContent: this.images[i] });
      // }
      //this.Subcat.push({Name:this.ShowSubCat_Config[i],IconContent: this._HelperService._Icon_Cropper_Data });
    }
  }


  _Subcat_Data: subcategeoryContent = {
    Name: null,
    img: null,

  };

  getdata() {
    this.Imagechanged = this.imageChangedEvent;
  }


  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    this.croppedImage1 = this.croppedImage.replace("data:image/png;base64,", "")
    this._HelperService._Icon_Cropper_Data.Content = this.croppedImage1;

  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

  // onImgError(event) { 
  //     event.target.src = this._MerchantDetails.IconUrl;
  // }


  //end code---

  getFileDetails(e) {

  }

  Images: any[] = [];
  getFileDetails1(e) {
    this._HelperService._Icon_Cropper_Data.Content = this.croppedImage1
    this.Images.push({ ImageContent: this._HelperService._Icon_Cropper_Data });
  }
  images = [];
  localimg = new Array();
  onFileChange(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event: any) => {
          // this.images.push(event.target.result); 
          this._HelperService._Icon_Cropper_Data.Content = event.target.result.replace("data:image/png;base64,", "")
            .replace("data:image/jpg;base64,", "")
            .replace("data:image/jpeg;base64,", "")
            .replace("data:image/gif;base64,", "");

          this.images.push({ Name: "", Content: this._HelperService._Icon_Cropper_Data.Content, Extension: "", TypeCode: "", Height: "400", Width: "800" });
          this.Form_Addsubcat.patchValue({
            fileSource: this.images
          });
        }

        reader.readAsDataURL(event.target.files[i]);
      }
    }


  }

  CategeoryResponse1: any;
  GetMerchantDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.getcategory,
      // Task: this._HelperService.AppConfig.Api.Core.GetUserAccount,
      ReferenceKey: this.CategeoryResponse.ReferenceKey,
      //Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveMerchantReferenceKey, '='),
      ReferenceId: this.CategeoryResponse.ReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System1, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this.CategeoryResponse1 = _Response.Result;
          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

}



export class OCoreUsage {
  public Reference: string;
  public ReferenceKey: string;
  public ReferenceId: string;
  public SystemName: string;
  public UserAccountKey: string;
  public UserAccountDisplayName: string;
  public UserAccountIconUrl: string;
  public TypeCode: string;
  public TypeName: string;
  public HelperCode: string;
  public HelperName: string;
  public ParentKey: string;
  public ParentCode: string;
  public ParentName: string;
  public SubParentKey: string;
  public SubParentCode: string;
  public SubParentName: string;
  public PlatformCharge?: any;
  public Name: string;
  public Value: string;
  public SubValue: string;
  public Description: string;
  public Data: string;
  public Sequence: number = 0;
  public Count: number = 0;
  public SubItemsCount: number = 0;
  public IconUrl: string;
  public PosterUrl: string;
  public CreateDate: Date;
  public CreatedByKey: string;
  public CreatedByDisplayName: string;
  public CreatedByIconUrl: string;
  public Commission: number;
  public ModifyDate: Date;
  public ModifyByKey: string;
  public ModifyByDisplayName: string;
  public ModifyByIconUrl: string;
  public StatusId: number = 0;
  public StatusCode: string;
  public StatusName: string;
  public StatusI: string;
  public CreateDateS: string;
  public ModifyDateS: string;
}


export class subcategeoryContent {
  public Name: string[] = [];
  public img: string[] = [];

}


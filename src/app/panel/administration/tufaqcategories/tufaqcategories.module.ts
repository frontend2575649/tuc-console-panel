import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { MainPipe } from '../../../service/main-pipe.module'
import { ImageCropperModule } from 'ngx-image-cropper';

import { TUFAQCategoriesComponent } from "./tufaqcategories.component";
import { MerchantguardGuard } from "src/app/service/guard/merchantguard.guard";
const routes: Routes = [{ path: "",canActivate:[MerchantguardGuard], component: TUFAQCategoriesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUFAQCategoriesRoutingModule {}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TUFAQCategoriesRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    MainPipe,
    ImageCropperModule
  ],
  declarations: [TUFAQCategoriesComponent]
})
export class TUFAQCategoriesModule {}

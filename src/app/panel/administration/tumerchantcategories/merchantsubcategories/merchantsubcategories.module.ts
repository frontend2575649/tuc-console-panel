import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { MainPipe } from '../../../../service/main-pipe.module'
import { ImageCropperModule } from 'ngx-image-cropper';
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { ArchwizardModule } from "angular-archwizard";
import { MerchantsubcategoriesComponent } from './merchantsubcategories.component';


//import { TUDealCategoriesComponent } from "../tudealcategories.component"
const routes: Routes = [{ path: "", component: MerchantsubcategoriesComponent }];

const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 1,
};

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
 
})
export class MerchantSubCategoriesRoutingModule {}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    MerchantSubCategoriesRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,ArchwizardModule,
    Ng2FileInputModule,
    MainPipe,
    ImageCropperModule,
    InputFileModule.forRoot(config),

  ],
  declarations: [MerchantsubcategoriesComponent]
})
export class MerchantSubCategoriesModule {}

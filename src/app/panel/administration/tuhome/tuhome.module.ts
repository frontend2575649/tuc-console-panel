import { AgmCoreModule } from '@agm/core';
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Select2Module } from "ng2-select2";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgxPaginationModule } from "ngx-pagination";
import { MerchantguardGuard } from 'src/app/service/guard/merchantguard.guard';
import { MainPipe } from '../../../service/main-pipe.module';
import { TUHomeComponent } from "./tuhome.component";



const routes: Routes = [
    {
        path: "",
        canActivate:[MerchantguardGuard],
        component: TUHomeComponent,
        children: [
          
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUHomeRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUHomeRoutingModule,
        GooglePlaceModule,
        MainPipe,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),

    ],
    declarations: [TUHomeComponent]
})
export class TUHomeModule { }

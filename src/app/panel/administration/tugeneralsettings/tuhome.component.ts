import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, OResponse, OUserDetails } from "../../../service/service";
declare let $: any;
declare var moment: any;

@Component({
  selector: "tu-tuhome",
  templateUrl: "./tuhome.component.html"
})
export class TUHomeComponent implements OnInit {

  defaultToDarkFilter = ["grayscale: 100%", "invert: 100%"];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this._HelperService.ResetDateRange();
  }

  ngOnInit() { 
    this._HelperService.ValidateData();
  }



}

import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../service/service";
import { Observable, Subscription } from 'rxjs';
import { InputFile, InputFileComponent } from 'ngx-input-file';

@Component({
  selector: "tu-tuappslider",
  templateUrl: "./tuappslider.component.html",
})
export class TUAppSliderComponent implements OnInit, OnDestroy {
  public ResetFilterControls: boolean = true;

  CurrentImagesCount: number = 0;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {

  }

  @ViewChild("inputfile")
  private InputFileComponent: InputFileComponent;
  ngOnInit() {
    // this._HelperService._Icon_Cropper_Data.Width = 800;
    // this._HelperService._Icon_Cropper_Data.Height = 450;
    this._HelperService.StopClickPropogation();
    Feather.replace();
    // this.InitImagePicker(this.InputFileComponent);



    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];

      this.AppSlider_Setup();
      this.AppSlider_Filter_Owners_Load();
      this.InitColConfig();
      this.Form_Add_Load();
      // this._HelperService.ValidateData();
    });

    this.timeout = setTimeout(() => {
      this._HelperService.ValidateData();
    }, 1500);


    // this._HelperService.StopClickPropogation();
    // this.InitImagePicker(this.InputFileComponent);
  }

  private InitImagePicker(InputFileComponent: InputFileComponent) {
    if (InputFileComponent != undefined) {
      this.CurrentImagesCount = 0;
      this._HelperService._InputFileComponent = InputFileComponent;
      InputFileComponent.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
  }

  removeImage(): void {
    this.CurrentImagesCount = 0;
    this._HelperService.Icon_Crop_Clear();
  }
resetImage(){
this._HelperService.Icon_Crop_Clear();
this.InputFileComponent.files.pop();
this._HelperService._Icon_Cropper_Data.Content=null;
let formValue=this.Form_Add.value;
formValue.IconContent.Content=null;
}
  

  ngOnDestroy(): void {

  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }
  ResetImagePlaceholder:boolean = true
  ResetImage(): void {
    this.ResetImagePlaceholder = false;
    this._ChangeDetectorRef.detectChanges();
    this.ResetImagePlaceholder = true;
    this._ChangeDetectorRef.detectChanges();
  }
  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region Storelist

  public AppSlider_Config: OList;
  public CurrentRequest_Key: string;
  public CurrentRequest_Id: number;
  AppSlider_Setup() {
    this.AppSlider_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.Core.GetSliderImages,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.System,
      Title: "App Slider",
      StatusType: "default",
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, 'sliderimage', "="),

      TableFields: [
        {
          DisplayName: " Added By",
          SystemName: "CreatedByDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
       


        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.AppSlider_Config = this._DataHelperService.List_Initialize(
      this.AppSlider_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Stores,
      this.AppSlider_Config
    );

    this.AppSlider_GetData();
  }
  AppSlider_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.AppSlider_Config.Sort.SortOptions.length; index++) {
        const element = this.AppSlider_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.AppSlider_Config
    );

    this.AppSlider_Config = this._DataHelperService.List_Operations(
      this.AppSlider_Config,
      event,
      Type
    );

    if (
      (this.AppSlider_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.AppSlider_GetData();
    }

  }
  timeout = null;
  AppSlider_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.AppSlider_Config.Sort.SortOptions.length; index++) {
          const element = this.AppSlider_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.AppSlider_Config
      );

      this.AppSlider_Config = this._DataHelperService.List_Operations(
        this.AppSlider_Config,
        event,
        Type
      );

      if (
        (this.AppSlider_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.AppSlider_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);


  }

  AppSlider_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.AppSlider_Config
    );
    this.AppSlider_Config = TConfig;
  }
  AppSlider_RowSelected(ReferenceData) {
    this.CurrentRequest_Key = ReferenceData.ReferenceKey;
    this.CurrentRequest_Id = ReferenceData.ReferenceId;

  }

  //#endregion

  //#region OwnerFilter

  public AppSlider_Filter_Owners_Option: Select2Options;
  public AppSlider_Filter_Owners_Selected = null;
  AppSlider_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
      // ReferenceKey: this._HelperService.UserAccount.AccountKey,
      // ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant,
    //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     this._HelperService.AppConfig.AccountType.PosAccount
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.AppSlider_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  AppSlider_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.AppSlider_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.AppSlider_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.AppSlider_Filter_Owners_Selected,
        "="
      );
      this.AppSlider_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.AppSlider_Config.SearchBaseConditions
      );
      this.AppSlider_Filter_Owners_Selected = null;
    } else if (event.value != this.AppSlider_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.AppSlider_Filter_Owners_Selected,
        "="
      );
      this.AppSlider_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.AppSlider_Config.SearchBaseConditions
      );
      this.AppSlider_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.AppSlider_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.AppSlider_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.AppSlider_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.AppSlider_Config.SearchBaseConditions = [];
    this.AppSlider_Config.SearchBaseCondition = null;

    // this.AppSlider_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, 'sliderimage', "=");
    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      //this.AppSlider_Filter_Owners_Selected = null;
      // this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.AppSlider_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.AppSlider_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.AppSlider_Config);

    this.SetOtherFilters();

    this.AppSlider_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      // inputAttributes: {
      //   autocapitalize: "off",
      //   autocorrect: "off",
      //   maxLength: "4",
      //   minLength: "4",
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Stores,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      //title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      title: "Delete App Slider",
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Stores
        );
        this._FilterHelperService.SetMerchantConfig(this.AppSlider_Config);
        this.AppSlider_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.AppSlider_GetData();

    if (ButtonType == 'Sort') {
      $("#AppSlider_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#AppSlider_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.AppSlider_Config);
    this.SetOtherFilters();

    this.AppSlider_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.AppSlider_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }
  Form_Add: FormGroup;
  Form_Add_Show() {
      this.Form_Add_Clear();
      this._HelperService.OpenModal('Form_Add_Content');
      setTimeout(() => {
        this.InitImagePicker(this.InputFileComponent);
      }, 100);
  }
  Form_Add_Close() {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
    this._HelperService.CloseModal('Form_Add_Content');
  }
  Form_Add_Load() {
      this._HelperService._Icon_Cropper_Data.Width = 800;
      this._HelperService._Icon_Cropper_Data.Height = 450;
      this.Form_Add = this._FormBuilder.group({
          AllowDuplicateName: true,
          Task: this._HelperService.AppConfig.Api.Core.SaveSliderimage,
          // TypeCode: this._HelperService.AppConfig.HelperTypes.SliderImage,
          // Name: this._HelperService.GetRandomNumber(),
          // Sequence: 0,
          StatusCode: 'default.active',
          IconContent: this._HelperService._Icon_Cropper_Data,
      });
  }
  Form_Add_Clear() {
    this.Form_Add.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_Add_Load();
    this._HelperService._FileSelect_Icon_Reset();
  }
  Form_Add_Process(_FormValue: any) {
    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
    }
      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, _FormValue);
      _OResponse.subscribe(
          _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                  this._HelperService.NotifySuccess(_Response.Message);
                  this.ResetImage();
                  this._HelperService._FileSelect_Icon_Reset();
                  this.AppSlider_Setup();
                  this.Form_Add_Clear();
                  this.Form_Add_Close();
                  this._HelperService.Icon_Crop_Clear();
                  if (_FormValue.OperationType == 'close') {
                      this.Form_Add_Close();
                  }
              }
              else {
                  this._HelperService.NotifyError(_Response.Message);
              }
          },
          _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
          });
  }

  Delete_Confirm(ReferenceData) {
    this.CurrentRequest_Key = ReferenceData.ReferenceKey;
    this.CurrentRequest_Id = ReferenceData.ReferenceId;
      swal({
          //title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
          title: "Delete App Slider?",
          text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
          showCancelButton: true,
          position: this._HelperService.AppConfig.Alert_Position,
          animation: this._HelperService.AppConfig.Alert_AllowAnimation,
          customClass: this._HelperService.AppConfig.Alert_Animation,
          allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
          allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
          confirmButtonColor: this._HelperService.AppConfig.Color_Red,
          cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
          confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
          cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
          input: 'text',
          inputClass:'swalText',

          inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
          inputAttributes: {
              autocapitalize: 'off',
              autocorrect: 'off',
              maxLength: "4",
              minLength: "4"
          },
          inputValidator:function(value) {
            if(value === '' || value.length < 4) { 
              return 'Enter your 4 digit pin!'
            }
        },

      }).then((result) => {
          if (result.value) {
              this._HelperService.IsFormProcessing = true;
              var pData = {
                  Task: this._HelperService.AppConfig.Api.Core.DeleteSliderimage,
                  ReferenceKey: this.CurrentRequest_Key,
                  ReferenceId: this.CurrentRequest_Id,
                  
                  AuthPin: result.value
              };
              let _OResponse: Observable<OResponse>;
              _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, pData);
              _OResponse.subscribe(
                  _Response => {
                      this._HelperService.IsFormProcessing = false;
                      if (_Response.Status == this._HelperService.StatusSuccess) {
                          this._HelperService.NotifySuccess(_Response.Message);
                          this._HelperService.CloseModal('ModalDetails');
                          this.AppSlider_Setup();
                      }
                      else {
                          this._HelperService.NotifyError(_Response.Message);
                      }
                  },
                  _Error => {

                      this._HelperService.HandleException(_Error);
                  });
          }
      });
  }

}

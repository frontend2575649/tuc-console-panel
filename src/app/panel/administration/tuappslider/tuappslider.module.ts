import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { ImageCropperModule } from 'ngx-image-cropper';
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { MainPipe } from '../../../service/main-pipe.module'
import { TUAppSliderComponent } from "./tuappslider.component";
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { MerchantguardGuard } from "src/app/service/guard/merchantguard.guard";
const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 1,
};
const routes: Routes = [{ path: "",canActivate:[MerchantguardGuard], component: TUAppSliderComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUAppSliderRoutingModule {}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TUAppSliderRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    MainPipe,
    ImageCropperModule,
    InputFileModule.forRoot(config),
  ],
  declarations: [TUAppSliderComponent]
})
export class TUAppSliderModule {}

import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../service/service";
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: "tu-tuconfiguration",
  templateUrl: "./tuconfiguration.component.html",
})
export class TUConfigurationsComponent implements OnInit, OnDestroy {


  // Merchant - thankumerchant
  // Acquirer - acquirer
  // PG Account - pgaccount
  // POS Account - posaccount
  // Store - merchantstore
  // Cashier - merchantcashier

  //   Branch - branch
  // Manager - manager
  // Acquirer Subaccount - acquirersubaccount
  // Merchant Subaccount - merchantsubaccount


  AccountTypeCodes: any = [

    { Name: 'Merchant', Value: 'thankumerchant' },
    { Name: 'Acquirer', Value: 'acquirer' },
    { Name: 'POS Account', Value: 'posaccount' },
    { Name: 'PG Account', Value: 'pgaccount' },
    { Name: 'Store', Value: 'merchantstore' },
    { Name: 'Cashier', Value: 'merchantcashier' },

    { Name: 'Manager', Value: 'manager' },
    { Name: 'Branch', Value: 'branch' },

    { Name: 'Acquirer SubAccount', Value: 'acquirersubaccount' },
    { Name: 'Merchant SubAccount', Value: 'merchantsubaccount' },
  ]



  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;


  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = true;
    this._HelperService.showAddNewSubAccBtn = false;


  }
  @ViewChild("offCanvas") divView: ElementRef;

  ngOnInit() {
    this._HelperService.StopClickPropogation();

    Feather.replace();
    this.InitBackDropClickEvent();
    this.Configurations_Setup();
    this.InitColConfig();
    this.Form_Edit_Load();
    this.Form_Add_Load();
    this.TListVChange_Filter_Stores_Load();
    this.TListVChange_Setup();

    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.Configurations_GetData();
    // });
    this.timeout = setTimeout(() => {
      this._HelperService.ValidateData();
    }, 1500);
  }


  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }
  }
  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  ChangeValue(ReferenceData) {

    if (this._Configuration.ReferenceKey != ReferenceData.ReferenceKey) {
      this._Configuration = ReferenceData;
      // this.TList_GetDetails();
    }
    this._HelperService.OpenModal('Change_Value_Content');
  }

  OpenHistory(ReferenceData) {
    if (this._Configuration.ReferenceKey != ReferenceData.ReferenceKey) {
      this._Configuration = ReferenceData;
      // this.TList_GetDetails();
    }

    this.TListVChange_Setup();
    this._HelperService.OpenModal('History_Content');
    setTimeout(() => {
      this._HelperService.StopClickPropogation();
    }, 200);

  }

  closeHistory() {
    this._HelperService.CloseModal('History_Content');

  }

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  public _Configuration: any =
    {
      ReferenceId: 0,
      ReferenceKey: null,
      DataTypeCode: null,
      DataTypeName: null,
      DataTypeId:null,
      Name: null,
      SystemName: null,
      Description: null,
      Value: null,
      CreateDate: null,
      StatusCode: null,
      StatusName: null,
      StatusI: null,
      StatusB: null,


      AccountTypeCode: null,
      ValueHelperCode: null,

    }

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region Configurations
  public CurrentRequest_Key: string;

  public Configurations_Config: OList;
  Configurations_Setup() {
    this.Configurations_Config = {
      Id: null,
      Sort: null,
      Task: 'getconfigurations',
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Configuration,
      Title: 'Configurations',
      StatusType: 'default',
      PageRecordLimit: 9,
      Type: this._HelperService.AppConfig.ListType.All,
      DefaultSortExpression: 'CreateDate desc',
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.HelperTypes.MerchantCategories, "="),
      TableFields: [
        {
          DisplayName: 'Name',
          SystemName: 'Name',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        }
      ]

    };
    this.Configurations_Config = this._DataHelperService.List_Initialize(
      this.Configurations_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Cashier,
      this.Configurations_Config
    );

    this.Configurations_GetData();
  }
  Configurations_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.Configurations_Config.Sort.SortOptions.length; index++) {
        const element = this.Configurations_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.Configurations_Config


    );

    this.Configurations_Config = this._DataHelperService.List_Operations(
      this.Configurations_Config,
      event,
      Type
    );

    if (
      (this.Configurations_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.Configurations_GetData();
    }

  }
  timeout = null;
  Configurations_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.Configurations_Config.Sort.SortOptions.length; index++) {
          const element = this.Configurations_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.Configurations_Config


      );

      this.Configurations_Config = this._DataHelperService.List_Operations(
        this.Configurations_Config,
        event,
        Type
      );

      if (
        (this.Configurations_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.Configurations_GetData();
      }

    }, this._HelperService.AppConfig.SearchInputDelay);

  }

  // Configurations_GetData() {

  //   var TConfig = this._DataHelperService.List_GetData(
  //     this.Configurations_Config
  //   );
  //   this.Configurations_Config = TConfig;
  // }

  Configurations_GetData() {

    // var TConfig = this._DataHelperService.List_GetDataconfig(
    //   this.Configurations_Config
    // );
    var TConfig = this._DataHelperService.List_GetData(
      this.Configurations_Config
    );

    this.Configurations_Config = TConfig;
  }


  Configurations_RowSelected1(ReferenceData) {
    this._HelperService.IsFormProcessing = true;
    var pData = {

      //getconfiguration api path
      Task: this._HelperService.AppConfig.Api.ThankUCash.Getconfiguration,
      // Task: this._HelperService.AppConfig.Api.Core.GetUserAccount,
      ReferenceKey: ReferenceData.ReferenceKey,
      ReferenceId: ReferenceData.ReferenceId
      //Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveMerchantReferenceKey, '='),
      // AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Configuration, pData);
    _OResponse.subscribe(
      _Response => {
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );

  }

  Configurations_RowSelected(ReferenceData) {
    if (this._Configuration.ReferenceKey != ReferenceData.ReferenceKey) {
      this._Configuration = ReferenceData;
      // this.TList_GetDetails();
    }
    this._Configuration = ReferenceData;
    this.Form_Edit.patchValue(
      {
        Task: 'updateconfiguration',
        ReferenceKey: this._Configuration.ReferenceKey,
        Name: this._Configuration.Name,
        SystemName: this._Configuration.SystemName,
        Description: this._Configuration.Description,
        DataTypeCode: this._Configuration.DataTypeCode,
        StatusCode: this._Configuration.StatusCode,
        Value: this._Configuration.Value,

        // AccountTypeCode: this._Configuration.Value,
        // ValueHelperCode: this._Configuration.Value,

        // newly added code start
        AccountTypeCode: this._Configuration.AccountTypeCode,
        ValueHelperCode: this._Configuration.ValueHelperCode,
        // newly added code end


      }
    );
    // this.TList_GetDetails();
    this._HelperService.OpenModal('Form_Edit_Content');
  }

  //#endregion

  //#region StoreFilter

  public TUC_Filter_Acccodes: Select2Options;
  public TUC_Filter_Acccodes_changed = null;

  public TListVChange_Filter_Stores_Option: Select2Options;
  public TListVChange_Filter_Stores_Selected = null;
  TListVChange_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      // AccountKey: this._HelperService.UserAccount.AccountKey,
      // AccountId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TListVChange_Filter_Stores_Option = {
      placeholder: "Select Store",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TListVChange_Filter_Stores_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TListVChange_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.TListVChange_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.TListVChange_Filter_Stores_Selected,
        "="
      );
      this.TListVChange_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TListVChange_Config.SearchBaseConditions
      );
      this.TListVChange_Filter_Stores_Selected = null;
    } else if (event.value != this.TListVChange_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.TListVChange_Filter_Stores_Selected,
        "="
      );
      this.TListVChange_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TListVChange_Config.SearchBaseConditions
      );
      this.TListVChange_Filter_Stores_Selected = event.data[0].ReferenceKey;
      this.TListVChange_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "StoreReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.TListVChange_Filter_Stores_Selected,
          "="
        )
      );
    }

    this.TListVChange_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion



  // TList_GetDetails() {
  //   var pData = {
  //     Task: 'getconfigurationdetails',
  //     ReferenceKey: this._Configuration.ReferenceKey,
  //   };
  //   let _OResponse: Observable<OResponse>;
  //   _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
  //   _OResponse.subscribe(
  //     _Response => {
  //       if (_Response.Status == this._HelperService.StatusSuccess) {
  //         this._Configuration = _Response.Result;
  //         this._Configuration.CreateDate = this._HelperService.GetDateTimeS(this._Configuration.CreateDate);
  //         this._Configuration.ModifyDate = this._HelperService.GetDateTimeS(this._Configuration.ModifyDate);
  //         this._Configuration.StatusI = this._HelperService.GetStatusIcon(this._Configuration.StatusCode);
  //         this._Configuration.StatusB = this._HelperService.GetStatusBadge(this._Configuration.StatusId);
  //         this.Form_Edit.patchValue(
  //           {
  //             Task: 'updateconfiguration',
  //             ReferenceKey: this._Configuration.ReferenceKey,
  //             Name: this._Configuration.Name,
  //             SystemName: this._Configuration.SystemName,
  //             Description: this._Configuration.Description,
  //             DataTypeCode: this._Configuration.DataTypeCode,
  //             StatusCode: this._Configuration.StatusCode,
  //             AccountTypeCode: this._Configuration.AccountTypeCode,
  //             ValueHelperCode: this._Configuration.ValueHelperCode,

  //           }
  //         );
  //         // this.TListVChange_Setup();
  //       }
  //       else {
  //         this._HelperService.NotifyError(_Response.Message);
  //       }
  //     },
  //     _Error => {
  //       this._HelperService.HandleException(_Error);
  //     });
  // }

  public TListVChange_Config: OList;
  TListVChange_Setup() {
    this.TListVChange_Config = {
      Id: null,
      Task: 'getconfigurationhistory',
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.System,
      Title: 'Change History',
      StatusType: 'default',
      ReferenceId: this._Configuration.ReferenceId,
      ReferenceKey: this._Configuration.ReferenceKey,
      Type: this._HelperService.AppConfig.ListType.All,
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'CreateDate',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "ConfigurationKey", this._HelperService.AppConfig.DataType.Text, this._Configuration.ReferenceKey, "="),
      TableFields: [
        {
          DisplayName: 'Value',
          SystemName: 'Value',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: false,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Comment',
          SystemName: 'Description',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: false,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Change On',
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
        },
        {
          DisplayName: 'Change By',
          SystemName: 'CreatedByDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: false,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'End On',
          SystemName: 'ModifyDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: false,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'End By',
          SystemName: 'ModifyByDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: false,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
      ]
    };
    this.TListVChange_Config = this._DataHelperService.List_Initialize(this.TListVChange_Config);

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Merchant,
      this.TListVChange_Config
    );

    this.TListVChange_GetData();
  }
  TListVChange_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.TListVChange_Config.Sort.SortOptions.length; index++) {
        const element = this.TListVChange_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.TListVChange_Config
    );

    this.TListVChange_Config = this._DataHelperService.List_Operations(
      this.TListVChange_Config,
      event,
      Type
    );

    if (
      (this.TListVChange_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TListVChange_GetData();
    }
  }
  timeoutV = null;
  TListVChange_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeoutV);

    this.timeoutV = setTimeout(() => {

      if (event != null) {
        for (let index = 0; index < this.TListVChange_Config.Sort.SortOptions.length; index++) {
          const element = this.TListVChange_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.TListVChange_Config
      );

      this.TListVChange_Config = this._DataHelperService.List_Operations(
        this.TListVChange_Config,
        event,
        Type
      );

      if (
        (this.TListVChange_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.TListVChange_GetData();
      }

    }, this._HelperService.AppConfig.SearchInputDelay);

  }
  TListVChange_GetData() {
    var TConfig = this._DataHelperService.List_GetData(this.TListVChange_Config);
    this.TListVChange_Config = TConfig;
  }

  SetOtherFilters(): void {
    this.Configurations_Config.SearchBaseConditions = [];
    this.Configurations_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.TListVChange_Filter_Stores_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.TListVChange_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.TListVChange_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.TListVChange_Config);

    this.SetOtherFilters();

    this.TListVChange_GetData();
  }

  Save_NewFilter() {
    this._HelperService.CloseModal('History_Content');
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Session
        );
        this._HelperService.OpenModal('History_Content');

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Session
        );
        this._FilterHelperService.SetMerchantConfig(this.TListVChange_Config);
        this.TListVChange_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.TListVChange_GetData();

    if (ButtonType == 'Sort') {
      $("#TListVChange_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#TListVChange_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.TListVChange_Config);
    this.SetOtherFilters();

    this.TListVChange_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();


    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  clicked() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

  Form_Edit: FormGroup;
  Form_Edit_Close() {
    // this.TListVChange_GetData();
    this._HelperService.CloseModal('Form_Edit_Content');
  }
  Form_Edit_Load() {
    this.Form_Edit = this._FormBuilder.group({
      Task: 'updateconfiguration',
      ReferenceKey: this._Configuration.ReferenceKey,
      ReferenceId: this._Configuration.ReferenceId,
      Name: [null, Validators.required],
      SystemName: [null, Validators.required],
      // ValueHelperCode: [null, Validators.required],
      Description: [null, Validators.required],
      Value: [null, Validators.required],

      // DataTypeCode: this._HelperService.AppConfig.SystemDataType.Text,
      DataTypeCode: [null, Validators.required],
      StatusCode: 'default.active',
      AccountTypeCode: null,
      ValueHelperCode: null,
    });
  }
  Form_Edit_Clear() {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_Edit_Load();

  }
  Form_Edit_Process(_FormValue: any) {
    this._HelperService.CloseModal('Form_Edit_Content');
    swal({
      title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Primary,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      input: 'password',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {
        this._HelperService.OpenModal('Form_Edit_Content');
        _FormValue.AuthPin = result.value;
        _FormValue.ReferenceId = this._Configuration.ReferenceId;
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Configuration, _FormValue);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
              this._HelperService.NotifySuccess(_Response.Message);
              this._HelperService._FileSelect_Icon_Reset();
              this.Form_Edit_Clear();
              this.Configurations_GetData();
              if (_FormValue.OperationType == 'close') {
                this.Form_Edit_Close();
              }
              this.Form_Edit_Close();
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          });
      }
    });

  }

  Form_Add: FormGroup;
  Form_Add_Show() {
    // this.TListVChange_GetData();
    this._HelperService.OpenModal('Form_Add_Content');
  }
  Form_Add_Close() {
    // this.TListVChange_GetData();
    this._HelperService.CloseModal('Form_Add_Content');
  }

  changeISD(e) {
    this.Form_Add.get('AccountTypeCode').setValue(e.target.value, {
      onlySelf: true
    })
  }




  Form_Add_Load() {
    this.Form_Add = this._FormBuilder.group({
      Task: 'saveconfiguration',
      Name: [null, Validators.required],
      SystemName: [null, Validators.required],
      Description: [null, Validators.required],
      Value: [null, Validators.required],
      DataTypeCode: this._HelperService.AppConfig.SystemDataType.Text,
      StatusCode: 'default.active',
      AccountTypeCode: null,
      // AccountTypeCode: this._HelperService.AppConfig.AccountTypeCodes,
      ValueHelperCode: null,
      // ValueHelperCode: [null, Validators.required],
    });
  }

  // public SelectDropdown: Select2Options;


  Form_Add_Clear() {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_Add_Load();
  }
  Form_Add_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Configuration, _FormValue);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
          this._HelperService.NotifySuccess(_Response.Message);
          this._HelperService._FileSelect_Icon_Reset();
          this.Form_Add_Clear();
          this.Configurations_GetData();
          if (_FormValue.OperationType == 'close') {
            this.Form_Add_Close();
          }
          this.Form_Add_Close();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public Comment = undefined;
  UpdateConfigurationValue() {
    if (this.Comment == undefined || this.Comment == '') {
      this._HelperService.NotifyError('Please enter comment');
    }
    else {
      this._HelperService.CloseModal('Change_Value_Content');
      swal({
        title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
        text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
        showCancelButton: true,
        position: this._HelperService.AppConfig.Alert_Position,
        animation: this._HelperService.AppConfig.Alert_AllowAnimation,
        customClass: this._HelperService.AppConfig.Alert_Animation,
        allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
        allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
        confirmButtonColor: this._HelperService.AppConfig.Color_Primary,
        cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
        confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
        cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        input: 'password',
        inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
        inputAttributes: {
          autocapitalize: 'off',
          autocorrect: 'off',
          maxLength: "4",
          minLength: "4"
        },
      }).then((result) => {
        if (result.value) {
          this._HelperService.OpenModal('Change_Value_Content');
          var pData = {
            Task: 'updateconfiguration',
            AuthPin: result.value,
            ReferenceKey: this._Configuration.ReferenceKey,
            ReferenceId: this._Configuration.ReferenceId,
            Value: this._Configuration.Value,
            Description: this.Comment,
          }
          this._HelperService.IsFormProcessing = true;
          let _OResponse: Observable<OResponse>;
          _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Configuration, pData);
          _OResponse.subscribe(
            _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.NotifySuccess(_Response.Message);
                this.Comment = '';
                // this.TList_GetDetails();
                this.TListVChange_Setup();
                this.Configurations_GetData();
                this._HelperService.CloseModal('Change_Value_Content');
              }
              else {
                this._HelperService.NotifyError(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
            });
        }
      });
    }
  }

  TListVChange_RowSelected(): void {

  }

  InputTypeSelected(type: any) {
    if (type == 'boolean') {
      this.Form_Add.controls['Value'].setValue('1');
      this.Form_Edit.controls['Value'].setValue('1');
    } else {
      this.Form_Add.controls['Value'].setValue(null);
      this.Form_Edit.controls['Value'].setValue(null);

    }
  }
  SelectedRowRefrence: any;
  EditInputTypeSelected(type: any) {

    if (type == this.SelectedRowRefrence.DataTypeCode) {
      this.Form_Edit.controls['Value'].setValue(this.SelectedRowRefrence.Value);
      this._Configuration.Value = this.SelectedRowRefrence.Value;
      return;
    }

    if (type == this._HelperService.AppConfig.SystemDataType.Boolean) {
      this.Form_Edit.controls['Value'].setValue('1');
      this._Configuration.Value = '1';
    } else {
      this.Form_Edit.controls['Value'].setValue(null);
      this._Configuration.Value = null;
    }
  }



  public DeleteConfiguration(ReferenceData): void {
    if (this._Configuration.ReferenceKey != ReferenceData.ReferenceKey) {
      this._Configuration = ReferenceData;
      // this.TList_GetDetails();
    }

    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "deleteconfiguration",
      ReferenceKey: this._Configuration.ReferenceKey,
      ReferenceId: this._Configuration.ReferenceId,


    };
    let _OResponse: Observable<OResponse>;

    swal({
      title: this._HelperService.AppConfig.CommonResource.DeleteDetails,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      input: 'password',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {
        this._HelperService.OpenModal('Edit_Rolefeature_Content');
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Configuration, pData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.NotifySuccess("Configuration Deleted  Successfully ");

              this.Configurations_Setup();

            } else {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });




  }

}


export class OCoreUsage {
  public Reference: string;
  public ReferenceKey: string;
  public ReferenceId: string;
  public SystemName: string;
  // public ValueHelperCode:string;
  public UserAccountKey: string;
  public UserAccountDisplayName: string;
  public UserAccountIconUrl: string;
  public TypeCode: string;
  public TypeName: string;
  public HelperCode: string;
  public HelperName: string;
  public ParentKey: string;
  public ParentCode: string;
  public ParentName: string;
  public SubParentKey: string;
  public SubParentCode: string;
  public SubParentName: string;
  public PlatformCharge?: any;
  public Name: string;
  public Value: string;
  public SubValue: string;
  public Description: string;
  public Data: string;
  public Sequence: number = 0;
  public Count: number = 0;
  public SubItemsCount: number = 0;
  public IconUrl: string;
  public PosterUrl: string;
  public CreateDate: Date;
  public CreatedByKey: string;
  public CreatedByDisplayName: string;
  public CreatedByIconUrl: string;
  public ModifyDate: Date;
  public ModifyByKey: string;
  public ModifyByDisplayName: string;
  public ModifyByIconUrl: string;
  public StatusId: number = 0;
  public StatusCode: string;
  public StatusName: string;
  public StatusI: string;
  public CreateDateS: string;
  public ModifyDateS: string;
}

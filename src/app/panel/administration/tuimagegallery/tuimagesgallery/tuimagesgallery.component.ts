import { ChangeDetectorRef, ChangeDetectionStrategy, Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;
import { Gallery, GalleryItem, ImageItem, ThumbnailsPosition, ImageSize } from '@ngx-gallery/core';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { map } from 'rxjs/operators';
import 'hammerjs';

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Observable, Subscription } from 'rxjs';
import { InputFile, InputFileComponent } from 'ngx-input-file';
import { Lightbox } from "@ngx-gallery/lightbox";

@Component({
  selector: "tu-tuimagesgallery",
  templateUrl: "./tuimagesgallery.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TUImageGalleryComponent implements OnInit, OnDestroy {
  public ResetFilterControls: boolean = true;
  items: GalleryItem[];
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  CurrentImagesCount: number = 0;
  SelectValue: boolean = false;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService,
    public gallery: Gallery,
    public lightbox: Lightbox
  ) {

  }

  @ViewChild("inputfile")
  private InputFileComponent: InputFileComponent;
  ngOnInit() {
    this.galleryOptions = [

      { image: false, thumbnailsRemainingCount: true, height: "100px" },
      { breakpoint: 500, width: "100%", thumbnailsColumns: 2 },

   
      {
        breakpoint: 400,
        preview: false
      }
    ];
    this.galleryImages = [
      {
        small: 'https://i.picsum.photos/id/237/200/300.jpg?hmac=TmmQSbShHz9CdQm0NkEjx1Dyh_Y984R9LpNrpvH2D_U',
        medium: 'https://i.picsum.photos/id/237/200/300.jpg?hmac=TmmQSbShHz9CdQm0NkEjx1Dyh_Y984R9LpNrpvH2D_U',
        big: 'https://i.picsum.photos/id/237/200/300.jpg?hmac=TmmQSbShHz9CdQm0NkEjx1Dyh_Y984R9LpNrpvH2D_U'
      },
      {
        small: 'https://picsum.photos/seed/picsum/200/300',
        medium: 'https://picsum.photos/seed/picsum/200/300',
        big: 'https://picsum.photos/seed/picsum/200/300'
      },
      {
        small: 'https://picsum.photos/200/300?grayscale',
        medium: 'https://picsum.photos/200/300?grayscale',
        big: 'https://picsum.photos/200/300?grayscale'
      }
    ];

    this._HelperService.StopClickPropogation();
    Feather.replace();
    // this.InitImagePicker(this.InputFileComponent);

    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];
      this.ImageGallery_Setup();
      this.ImageGallery_Filter_Owners_Load();
      this.InitColConfig();
      this.Form_Add_Load();

    });
    // this._HelperService.StopClickPropogation();


  }
  basicLightboxExample() {
    this.timeout = setTimeout(() => {
      this.gallery.ref().load(this.items);
      // 2. Get a lightbox gallery ref
      const lightboxGalleryRef = this.gallery.ref("anotherLightbox");
      // (Optional) Set custom gallery config to this lightbox
      lightboxGalleryRef.setConfig({
        imageSize: ImageSize.Cover,
        thumbPosition: ThumbnailsPosition.Bottom,
      });
      // 3. Load the items into the lightbox
      lightboxGalleryRef.load(this.items);
    }, 2000);





  }

  private InitImagePicker(InputFileComponent: InputFileComponent) {
    if (InputFileComponent != undefined) {
      this.CurrentImagesCount = 0;
      this._HelperService._InputFileComponent = InputFileComponent;
      InputFileComponent.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
  }

  removeImage(): void {
    this.CurrentImagesCount = 0;
    this._HelperService.Icon_Crop_Clear();
  }

  ngOnDestroy(): void {

  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region Storelist

  public ImageGallery_Config: OList;
  public CurrentRequest_Key: string;
  public CurrentRequest_Id: number;
  ImageGallery_Setup() {
    this.ImageGallery_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.Core.GetStorage,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Op,
      Title: "App Slider",
      StatusType: "default",
      DefaultSortExpression: "CreateDate desc",
      PageRecordLimit: 12,

      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, 'sliderimage', "="),

      TableFields: [
        {
          DisplayName: " Added By",
          SystemName: "CreatedByDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },



        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.ImageGallery_Config = this._DataHelperService.List_Initialize(
      this.ImageGallery_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Stores,
      this.ImageGallery_Config
    );
    this.InitImagePicker(this.InputFileComponent);
    this.timeout = setTimeout(() => {
      this.items = this.ImageGallery_Config.Data.map(item => {
        return new ImageItem({ src: item.IconUrl, thumb: item.IconUrl })
      });
    }, 2000);

    this.basicLightboxExample();
    this.ImageGallery_GetData();

  }
  ImageGallery_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.ImageGallery_Config.Sort.SortOptions.length; index++) {
        const element = this.ImageGallery_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.ImageGallery_Config
    );

    this.ImageGallery_Config = this._DataHelperService.List_Operations(
      this.ImageGallery_Config,
      event,
      Type
    );

    if (
      (this.ImageGallery_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.ImageGallery_GetData();
    }

  }
  timeout = null;
  ImageGallery_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.ImageGallery_Config.Sort.SortOptions.length; index++) {
          const element = this.ImageGallery_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.ImageGallery_Config
      );

      this.ImageGallery_Config = this._DataHelperService.List_Operations(
        this.ImageGallery_Config,
        event,
        Type
      );

      if (
        (this.ImageGallery_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.ImageGallery_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);


  }

  ImageGallery_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.ImageGallery_Config
    );
    this.ImageGallery_Config = TConfig;
  }
  ImageGallery_RowSelected(ReferenceData) {
    this.CurrentRequest_Key = ReferenceData.ReferenceKey;
    this.CurrentRequest_Id = ReferenceData.ReferenceId;

  }
  public ImageUrl?: string = 'assets/img/Dummy.png';
  CompressImage(ReferenceData) {
    this.CurrentRequest_Key = ReferenceData.ReferenceKey;
    this.CurrentRequest_Id = ReferenceData.ReferenceId;
    this.ImageUrl = ReferenceData.IconUrl;
    this._HelperService.OpenModal('ImageCompress');
  }
  //#endregion

  //#region OwnerFilter

  public ImageGallery_Filter_Owners_Option: Select2Options;
  public ImageGallery_Filter_Owners_Selected = null;
  ImageGallery_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
      // ReferenceKey: this._HelperService.UserAccount.AccountKey,
      // ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant,
    //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     this._HelperService.AppConfig.AccountType.PosAccount
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.ImageGallery_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  ImageGallery_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.ImageGallery_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.ImageGallery_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.ImageGallery_Filter_Owners_Selected,
        "="
      );
      this.ImageGallery_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.ImageGallery_Config.SearchBaseConditions
      );
      this.ImageGallery_Filter_Owners_Selected = null;
    } else if (event.value != this.ImageGallery_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.ImageGallery_Filter_Owners_Selected,
        "="
      );
      this.ImageGallery_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.ImageGallery_Config.SearchBaseConditions
      );
      this.ImageGallery_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.ImageGallery_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.ImageGallery_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.ImageGallery_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.ImageGallery_Config.SearchBaseConditions = [];
    this.ImageGallery_Config.SearchBaseCondition = null;

    // this.ImageGallery_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, 'sliderimage', "=");
    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      //this.ImageGallery_Filter_Owners_Selected = null;
      // this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.ImageGallery_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.ImageGallery_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.ImageGallery_Config);

    this.SetOtherFilters();

    this.ImageGallery_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      // inputAttributes: {
      //   autocapitalize: "off",
      //   autocorrect: "off",
      //   maxLength: "4",
      //   minLength: "4",
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Stores,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Stores
        );
        this._FilterHelperService.SetMerchantConfig(this.ImageGallery_Config);
        this.ImageGallery_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.ImageGallery_GetData();

    if (ButtonType == 'Sort') {
      $("#ImageGallery_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#ImageGallery_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.ImageGallery_Config);
    this.SetOtherFilters();

    this.ImageGallery_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.ImageGallery_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }



  CheckValue() {
    this.SelectValue = !(this.SelectValue);
    this.Form_Add.controls['IsPublic'].setValue(this.SelectValue);

  }
  Form_Add: FormGroup;
  Form_Add_Show() {
    this.Form_Add_Clear();
    this._HelperService.OpenModal('Form_Add_Content');
    setTimeout(() => {
      this.InitImagePicker(this.InputFileComponent);
    }, 100);
  }
  Form_Add_Close() {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
    this._HelperService.CloseModal('Form_Add_Content');
  }
  Form_Add_Load() {
    this._HelperService._Icon_Cropper_Data.Width = 800;
    this._HelperService._Icon_Cropper_Data.Height = 450;
    this.Form_Add = this._FormBuilder.group({
      AllowDuplicateName: true,
      Task: this._HelperService.AppConfig.Api.Core.SaveStorage,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      IsPublic: null,
      StorageContent: this._HelperService._Icon_Cropper_Data,
    });
  }
  Form_Add_Clear() {
    this.Form_Add.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_Add_Load();
    this._HelperService._FileSelect_Icon_Reset();
  }
  Form_Add_Process(_FormValue: any) {
    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
    }

    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Op, _FormValue);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this._HelperService._FileSelect_Icon_Reset();
          this.ImageGallery_Setup();
          this.Form_Add_Clear();
          this._HelperService.Icon_Crop_Clear();
          if (_FormValue.OperationType == 'close') {
            this.Form_Add_Close();
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  Delete_Confirm(ReferenceData) {
    this.CurrentRequest_Key = ReferenceData.ReferenceKey;
    this.CurrentRequest_Id = ReferenceData.ReferenceId;
    swal({
      title: this._HelperService.AppConfig.CommonResource.DeleteImage,
      text: this._HelperService.AppConfig.CommonResource.DeleteImageHelp,
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      input: 'password',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;
        var pData = {
          Task: this._HelperService.AppConfig.Api.Core.DeleteStorage,
          ReferenceKey: this.CurrentRequest_Key,
          ReferenceId: this.CurrentRequest_Id,

          AuthPin: result.value
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Op, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess(_Response.Message);
              this._HelperService.CloseModal('ModalDetails');
              this.ImageGallery_Setup();

            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {

            this._HelperService.HandleException(_Error);
          });
      }
    });
  }
}

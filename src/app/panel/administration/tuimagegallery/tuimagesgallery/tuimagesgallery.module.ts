import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { ImageCropperModule } from 'ngx-image-cropper';
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { MainPipe } from '../../../../service/main-pipe.module'
import { TUImageGalleryComponent } from "./tuimagesgallery.component";
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { ImageViewerModule } from 'ng2-image-viewer';
import { GalleryModule } from '@ngx-gallery/core';
import { LightboxModule } from '@ngx-gallery/lightbox';
import { NgxGalleryModule } from 'ngx-gallery';

const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 1,
};
const routes: Routes = [{ path: "", component: TUImageGalleryComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUImageGalleryRoutingModule {}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TUImageGalleryRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    MainPipe,
    ImageCropperModule,
    InputFileModule.forRoot(config),
    ImageViewerModule,
    GalleryModule,
    LightboxModule,
    NgxGalleryModule
  ],
  declarations: [TUImageGalleryComponent]
})
export class TUImageGalleryModule {}

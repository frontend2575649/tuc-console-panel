import {Component, ViewChild, ChangeDetectorRef,ElementRef} from '@angular/core'
import {DataHelperService,
    HelperService,
    OList,
    OSelect,
    FilterHelperService,
    OResponse,} from "../../../service/service";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Subscription, Observable } from 'rxjs';
import { InputFile, InputFileComponent } from 'ngx-input-file';
import {  Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;
@Component({
  selector: "tu-tulogomanagement",
  templateUrl: "./tulogomanagement.component.html",
})
export class TULogoManagementComponent{
  @ViewChild('fileinput') FileInput: ElementRef;
    constructor(
    public _FormBuilder: FormBuilder,
    public _Router: Router,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = true;
    this._HelperService.showAddNewSubAccBtn = false;


  }
  AccountData;
  ngOnInit(){
    this.AccountData=this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.Account);
    this.Form_AddLogo_Load()
    this.Logo_GetData()
  }
  public Logo_Config: OList;
  LogoImageDetails:any={}
  


private pData={
  Task: this._HelperService.AppConfig.Api.Core.getapplogo,
  AuthAccountId:this._HelperService.UserAccount.AccountId,
  Panel:"WebConnect",
  ImageContent: {
    Reference: "",
    Name: "Logo",
    Extension: "png",
    Tags: ""
    }
}
  Logo_GetData() {
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin, this.pData);

        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this.LogoImageDetails =_Response.Result;
              this._HelperService.logoImageUrl= _Response.Result.ImageUrl;
              this._HelperService.SaveStorage("logoimageurl", _Response.Result.ImageUrl);
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
          }
            

          })
  } 
 
  Form_Add: FormGroup;
  Form_AddLogo_Show(){
    this._HelperService.OpenModal('Form_Add_Content');

  }
  LogoImageCropped={
    MininmumWidth:180,
    MinimumHeight:42,
    ResizeToWidth:112,
    MaximumWidth: 800,
     MaximumHeight: 400,

  }
  Form_AddLogo_Load(){
    this.Form_Add = this._FormBuilder.group({
      Task:this._HelperService.AppConfig.Api.Core.setapplogo,
      Panel:"WebConnect",
      authAccountId:this._HelperService.UserAccount.AccountId,
      Uploader:this.AccountData.User.EmailAddress,
      ImageContent: this._HelperService._Icon_Cropper_Data,
    })
  }
 
  onDrop(event) {
    event.preventDefault();
    const file = event.dataTransfer.files[0];
    this.handleFile(file);
  }

  onDragOver(event) {
    event.preventDefault();
  }
  onFileSelected(event) {
    const file = event.target.files[0];
    this.handleFile(file);
  }

  handleFile(file) {
    if (this.validateImage(file)) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (e) => {
        this._HelperService._Icon_Cropper_BaseImage = reader.result.toString();
      };
    } else {
      this._HelperService.Icon_Crop_Clear();
    }
  }


  private validateImage(file: File): boolean {
    const allowedTypes = ['image/jpeg', 'image/jpg', 'image/png', 'image/svg'];
    return allowedTypes.includes(file.type);
  }

  Form_Add_Close() {
    this._HelperService.CloseModal('Form_Add_Content');
  }

  Form_Add_Clear() {
    this.Form_Add.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this. Form_AddLogo_Load();
    this._HelperService._FileSelect_Icon_Reset();
  }
  resetImage(){
    this.FileInput.nativeElement.value=null;
    this._HelperService.Icon_Crop_Clear();
    this._HelperService.CloseModal('Form_Add_Content');
    this._HelperService._Icon_Cropper_Data.Content=null;
    }
    ResetImage(): void {
      this._ChangeDetectorRef.detectChanges();
      this._ChangeDetectorRef.detectChanges();
    }

  Form_Add_Process(_FormValue: any) {
    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      _FormValue.ImageContent = this._HelperService._Icon_Cropper_Data;
    }
      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin, _FormValue);
      _OResponse.subscribe(
          _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                  this._HelperService.NotifySuccess(_Response.Message);
                  this.ResetImage();
                  this._HelperService._FileSelect_Icon_Reset();
                  this.Logo_GetData();
                  this.Form_Add_Clear();
                  this.Form_Add_Close();
                  this._HelperService.Icon_Crop_Clear();
              }
              else {
                  this._HelperService.NotifyError(_Response.Message);
                  this._HelperService.Icon_Crop_Clear();

              }
          },
          _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
          });
  }

  Edit_Confirm(){
    swal({

      title: "Edit Logo",
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Blue,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      input: 'text',
      inputClass:'swalText',

      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
          autocapitalize: 'off',
          autocorrect: 'off',
          maxLength: "4",
          minLength: "4"
      },
      inputValidator:function(value) {
        if(value === '' || value.length < 4) { 
          return 'Enter your 4 digit pin!'
        }
    },
    

  }).then((result) => {
      if (result.value) {
          this._HelperService.IsFormProcessing = true;
          this.Form_AddLogo_Show()
      }
  }).catch((e)=>{
    this.Form_AddLogo_Show()
  })
  }

}
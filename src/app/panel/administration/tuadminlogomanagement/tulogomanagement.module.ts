import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { Ng2FileInputModule } from "ng2-file-input";
import { MainPipe } from '../../../service/main-pipe.module'
import { ImageCropperModule } from 'ngx-image-cropper';
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { TranslateModule } from "@ngx-translate/core";

import { TULogoManagementComponent } from "./tulogomanagement.component";
const routes: Routes = [{ path: "", component: TULogoManagementComponent }];

const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 1,
};

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TULogoManagementRoutingModule {}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TULogoManagementRoutingModule,
    Ng2FileInputModule,
    Daterangepicker,
    NgxPaginationModule,
    TranslateModule,
    MainPipe,
    Select2Module,
    ImageCropperModule,
    InputFileModule.forRoot(config),

  ],
  declarations: [TULogoManagementComponent]
})
export class TULogoManagementModule {}

import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;
export class OVerificationRequest {
  public RequestDate: any;
  public ReferenceId: number = 0;
  public ReferenceKey: string = null;
  public TypeCode: string = null;
  public TypeName: string = null;
  public CountryIsd: string = null;
  public MobileNumber: string = null;
  public EmailAddress: string = null;
  public AccessKey: string = null;
  public AccessCode: string = null;
  public AccessCodeStart: string = null;
  public EmailMessage: string = null;
  public MobileMessage: string = null;
  public ExpiaryDate: Date = null;
  public VerifyDate: Date = null;
  public RequestIpAddress: string = null;
  public RequestLatitude: number = 0;
  public RequestLongitude: number = 0;
  public RequestLocation: string = null;
  public VerifyAttemptCount: number = 0;
  public VerifyIpAddress: string = null;
  public VerifyLatitude: number = null;
  public VerifyLongitude: number = null;
  public VerifyLocation: string = null;
  public ReferenceSubKey: string = null;
  public CreateDate: Date = null;
  public StatusId: number = 0;
  public StatusCode: string = null;
  public StatusName: string = null;
  public StatusI: string = null;
}

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../service/service";
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: "tu-tuverficationrequest",
  templateUrl: "./tuverficationrequest.component.html",
})
export class TUVerificationRequestComponent implements OnInit, OnDestroy {
  public _VRequest: OVerificationRequest =
    {
      RequestDate: null,
      AccessCode: null,
      AccessCodeStart: null,
      AccessKey: null,
      CountryIsd: null,
      CreateDate: null,
      EmailAddress: null,
      EmailMessage: null,
      ExpiaryDate: null,
      MobileMessage: null,
      MobileNumber: null,
      ReferenceId: null,
      ReferenceKey: null,
      ReferenceSubKey: null,
      RequestIpAddress: null,
      RequestLatitude: null,
      RequestLocation: null,
      RequestLongitude: null,
      StatusCode: null,
      StatusId: null,
      StatusName: null,
      TypeCode: null,
      TypeName: null,
      VerifyAttemptCount: 0,
      VerifyDate: null,
      VerifyIpAddress: null,
      VerifyLatitude: null,
      VerifyLocation: null,
      VerifyLongitude: null,
      StatusI: null,
    }
  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = true;
    this._HelperService.showAddNewSubAccBtn = false;


  }

  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();
    this.InitBackDropClickEvent();
    this.RequestHistory_Setup();
    this.RequestHistory_Filter_Stores_Load();
    this.InitColConfig();
    // this._HelperService.StopClickPropogation();
    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.RequestHistory_GetData();
    // });
  }

  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }
  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }

  //#region BackdropDismiss 

  @ViewChild("offCanvas") divView: ElementRef;

  clicked() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

  //#endregion

  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region RequestHistory

  public CurrentRequest_Key: string;
  public CurrentRequest_Id: number;

  public RequestHistory_Config: OList;
  RequestHistory_Setup() {
    this.RequestHistory_Config = {
      Id: null,
      Type: "all",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      Task: this._HelperService.AppConfig.Api.Core.GetOtpRequests,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.System,
      Title: 'All mobile verification requests',
      StatusType: 'default',
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'RequestDate',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.HelperTypes.SMS, "="),
      TableFields: [
        {
          DisplayName: '#',
          SystemName: 'ReferenceId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Mobile Number',
          SystemName: 'MobileNumber',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Req Ip',
          SystemName: 'RequestIpAddress',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
        },

        {
          DisplayName: 'Requested On',
          SystemName: 'RequestDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          IsDateSearchField:true,
          ResourceId: null,
        },
        {
          DisplayName: 'Verify Date',
          SystemName: 'VerifyDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Verify Ip',
          SystemName: 'VerifyIpAddress',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
        },
        // {
        //   DisplayName: 'Attempts',
        //   SystemName: 'Attempts',
        //   DataType: this._HelperService.AppConfig.DataType.Number,
        //   Class: 'td-date',
        //   Show: true,
        //   Search: false,
        //   Sort: true,
        //   ResourceId: null,
        // }
        {
          DisplayName: 'Verify Attempt Count',
          SystemName: 'VerifyAttemptCount',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        }

      ]

    };
    this.RequestHistory_Config = this._DataHelperService.List_Initialize(
      this.RequestHistory_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Cashier,
      this.RequestHistory_Config
    );

    this.RequestHistory_GetData();
  }
  RequestHistory_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.RequestHistory_Config.Sort.SortOptions.length; index++) {
        const element = this.RequestHistory_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.RequestHistory_Config


    );

    this.RequestHistory_Config = this._DataHelperService.List_Operations(
      this.RequestHistory_Config,
      event,
      Type
    );

    if (
      (this.RequestHistory_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.RequestHistory_GetData();
    }

  }
  timeout = null;
  RequestHistory_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {

      if (event != null) {
        for (let index = 0; index < this.RequestHistory_Config.Sort.SortOptions.length; index++) {
          const element = this.RequestHistory_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.RequestHistory_Config


      );

      this.RequestHistory_Config = this._DataHelperService.List_Operations(
        this.RequestHistory_Config,
        event,
        Type
      );

      if (
        (this.RequestHistory_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.RequestHistory_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);

  }

  RequestHistory_GetData() {

    var TConfig = this._DataHelperService.List_GetData(
      this.RequestHistory_Config
    );
    this.RequestHistory_Config = TConfig;
  }
  RequestHistory_RowSelected(ReferenceData) {
    this.CurrentRequest_Key = ReferenceData.ReferenceKey;
    this.CurrentRequest_Id = ReferenceData.ReferenceId;

    this.TList_GetDetails();
  }

  TList_GetDetails() {
    swal({
      title: "Enter your pin",
      text: "You must enter pin to view verification details",
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      input: 'text',
      inputClass:'swalText',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;
        var pData = {
          Task: this._HelperService.AppConfig.Api.Core.GetOtpRequest,
          AuthPin: result.value,
          ReferenceId: this.CurrentRequest_Id,
          ReferenceKey: this.CurrentRequest_Key

          // Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this.CurrentRequest_Key, '='),
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, pData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._VRequest = _Response.Result as OVerificationRequest;
              this._VRequest.CreateDate = this._HelperService.GetDateTimeS(this._VRequest.CreateDate);
              this._VRequest.RequestDate = this._HelperService.GetDateTimeS(this._VRequest.RequestDate);

              this._VRequest.ExpiaryDate = this._HelperService.GetDateTimeS(this._VRequest.ExpiaryDate);

              this._VRequest.VerifyDate = this._HelperService.GetDateTimeS(this._VRequest.VerifyDate);
              this._VRequest.StatusI = this._HelperService.GetStatusIcon(this._VRequest.StatusCode);
              //this._HelperService.OpenModal('Form_Edit_Content');
              this.clicked();
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.HandleException(_Error);
          });
      }
    });
  }

  //#endregion

  //#region StoreFilter

  public RequestHistory_Filter_Stores_Option: Select2Options;
  public RequestHistory_Filter_Stores_Selected = null;
  RequestHistory_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      // AccountKey: this._HelperService.UserAccount.AccountKey,
      // AccountId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.RequestHistory_Filter_Stores_Option = {
      placeholder: "Select Store",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  RequestHistory_Filter_Stores_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.RequestHistory_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.RequestHistory_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.RequestHistory_Filter_Stores_Selected,
        "="
      );
      this.RequestHistory_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.RequestHistory_Config.SearchBaseConditions
      );
      this.RequestHistory_Filter_Stores_Selected = null;
    } else if (event.value != this.RequestHistory_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.RequestHistory_Filter_Stores_Selected,
        "="
      );
      this.RequestHistory_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.RequestHistory_Config.SearchBaseConditions
      );
      this.RequestHistory_Filter_Stores_Selected = event.data[0].ReferenceKey;
      this.RequestHistory_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "StoreReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.RequestHistory_Filter_Stores_Selected,
          "="
        )
      );
    }

    this.RequestHistory_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.RequestHistory_Config.SearchBaseConditions = [];
    this.RequestHistory_Config.SearchBaseCondition = null;
    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.RequestHistory_Filter_Stores_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.RequestHistory_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.RequestHistory_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_RequestDate(Type, index);
    this._FilterHelperService.SetRequestDateNotifConfig(this.RequestHistory_Config);

    this.SetOtherFilters();

    this.RequestHistory_GetData();
  }

  // Save_NewFilter() {
  //   swal({
  //     position: "center",
  //     title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
  //     text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
  //     input: "text",
  //     inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
  //     inputAttributes: {
  //       autocapitalize: "off",
  //       autocorrect: "off",
  //       //maxLength: "4",
  //       minLength: "4",
  //     },
  //     animation: false,
  //     customClass: this._HelperService.AppConfig.Alert_Animation,
  //     showCancelButton: true,
  //     confirmButtonColor: this._HelperService.AppConfig.Color_Green,
  //     cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
  //     confirmButtonText: "Save",
  //     cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
  //   }).then((result) => {
  //     if (result.value) {
  //       this._HelperService._RefreshUI = false;
  //       this._ChangeDetectorRef.detectChanges();

  //       this._FilterHelperService._BuildFilterName_Merchant(result.value);
  //       this._HelperService.Save_NewFilter(
  //         this._HelperService.AppConfig.FilterTypeOption.Cashier
  //       );

  //       this._HelperService._RefreshUI = true;
  //       this._ChangeDetectorRef.detectChanges();
  //     }
  //   });
  // }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      // inputAttributes: {
      //   autocapitalize: "off",
      //   autocorrect: "off",
      //   maxLength: "4",
      //   minLength: "4",
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Stores,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Cashier
        );
        this._FilterHelperService.SetMerchantConfig(this.RequestHistory_Config);
        this.RequestHistory_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.RequestHistory_GetData();

    if (ButtonType == 'Sort') {
      $("#RequestHistory_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#RequestHistory_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.RequestHistory_Config);
    this.SetOtherFilters();

    this.RequestHistory_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.RequestHistory_Filter_Stores_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

}

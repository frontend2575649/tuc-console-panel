import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { MainPipe } from '../../../service/main-pipe.module'

import { TUGetCityComponent } from "./tugetcity.component";




import { ImageCropperModule } from 'ngx-image-cropper';

import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

import { InputFileConfig, InputFileModule } from 'ngx-input-file';

const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 1,
};


const routes: Routes = [{ path: "", component: TUGetCityComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUGetCityRoutingModule {}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TUGetCityRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    MainPipe,


   
   
    GooglePlaceModule,
   
    AgmCoreModule.forRoot({
        apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
    }),
    ImageCropperModule,
    InputFileModule.forRoot(config),
  ],
  declarations: [TUGetCityComponent]
})
export class TUGetCityModule {}

import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../service/service";
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: "tu-tugetcity",
  templateUrl: "./tugetcity.component.html",
})
export class TUGetCityComponent implements OnInit, OnDestroy {
  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;
  state: any;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = true;
    this._HelperService.showAddNewSubAccBtn = false;


  }
  @ViewChild("offCanvas") divView: ElementRef;

  ngOnInit() {



    this._HelperService.StopClickPropogation();
    Feather.replace();
    this.InitBackDropClickEvent();

    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
      this._HelperService.AppConfig.ActiveAccountKey = params['accountkey'];
      this._HelperService.AppConfig.ActiveAccountId = params['accountid'];
      this.Form_AddApp_Load();
      this.RequestHistory_Setup();
      this.Form_Edit_Load();
      // this.RequestHistory_Filter_Stores_Load();
      this.InitColConfig();
      this.state = this._HelperService.GetStorage('hcactivestate');
      // console.log(this._UserAccount);
      // console.log(this.state.Name);
      

    });


    // this._HelperService.StopClickPropogation();
    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.RequestHistory_GetData();
    // });
  }


  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }
  }
  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region RequestHistory
  public CurrentRequest_Key: string;
  public CurrentRequest_Id: number;
  public RequestHistory_Config: OList;
  RequestHistory_Setup() {
    this.RequestHistory_Config = {
      Id: null,
      Sort: null,
      Type: "all",
      Task: this._HelperService.AppConfig.Api.Core.getcities,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.City,
      Title: 'Get Cities',
      StatusType: 'transaction',
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
       TableFields: [
        
        {
          DisplayName: 'Name',
          SystemName: 'Name',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
       
      ]

    };
    this.RequestHistory_Config.SearchBaseCondition = "";
    this.RequestHistory_Config = this._DataHelperService.List_Initialize(
      this.RequestHistory_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Cashier,
      this.RequestHistory_Config
    );

    this.RequestHistory_GetData();
  }
  RequestHistory_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.RequestHistory_Config.Sort.SortOptions.length; index++) {
        const element = this.RequestHistory_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.RequestHistory_Config


    );

    this.RequestHistory_Config = this._DataHelperService.List_Operations(
      this.RequestHistory_Config,
      event,
      Type
    );

    if (
      (this.RequestHistory_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.RequestHistory_GetData();
    }

  }
  RequestHistory_GetData() {

    var TConfig = this._DataHelperService.List_GetData(
      this.RequestHistory_Config
    );
    this.RequestHistory_Config = TConfig;
  }

  inactive() {
    alert("City is inactive")
  }
  RequestHistory_RowSelected(ReferenceData) {
    // this.CurrentRequest_Key = ReferenceData.ReferenceKey;
    // this.CurrentRequest_Id= ReferenceData.ReferenceId;

    // this.ListAppUsage_GetDetails();
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveCity,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.Name,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 

 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Administration.GetcityArea,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,


    ]);

  }

  //#endregion

  //#region StoreFilter

  public RequestHistory_Filter_Stores_Option: Select2Options;
  public RequestHistory_Filter_Stores_Selected = null;
  RequestHistory_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      // AccountKey: this._HelperService.UserAccount.AccountKey,
      // AccountId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.RequestHistory_Filter_Stores_Option = {
      placeholder: "Select Store",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  RequestHistory_Filter_Stores_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.RequestHistory_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.RequestHistory_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.RequestHistory_Filter_Stores_Selected,
        "="
      );
      this.RequestHistory_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.RequestHistory_Config.SearchBaseConditions
      );
      this.RequestHistory_Filter_Stores_Selected = null;
    } else if (event.value != this.RequestHistory_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.RequestHistory_Filter_Stores_Selected,
        "="
      );
      this.RequestHistory_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.RequestHistory_Config.SearchBaseConditions
      );
      this.RequestHistory_Filter_Stores_Selected = event.data[0].ReferenceKey;
      this.RequestHistory_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "StoreReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.RequestHistory_Filter_Stores_Selected,
          "="
        )
      );
    }

    this.RequestHistory_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.RequestHistory_Config.SearchBaseConditions = [];
    this.RequestHistory_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.RequestHistory_Filter_Stores_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.RequestHistory_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_RequestHistory(Type, index);
    this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);

    this.SetOtherFilters();

    this.RequestHistory_GetData();
  }

 

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      // inputAttributes: {
      //   autocapitalize: "off",
      //   autocorrect: "off",
      //   maxLength: "4",
      //   minLength: "4",
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_RequestHistory(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Stores,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Cashier
        );
        this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);
        this.RequestHistory_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.RequestHistory_GetData();

    if (ButtonType == 'Sort') {
      $("#RequestHistory_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#RequestHistory_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);
    this.SetOtherFilters();

    this.RequestHistory_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.RequestHistory_Filter_Stores_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }


  clicked() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }


  ListAppUsage_GetDetails() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetRequestHistoryDetails,
      ReferenceKey: this.CurrentRequest_Key,
      ReferenceId: this.CurrentRequest_Id,


      // Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this.CurrentRequest_Key, '='),
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._CoreUsage = _Response.Result as OCoreUsage;
          this._CoreUsage.RequestTime = this._HelperService.GetDateTimeS(this._CoreUsage.RequestTime);
          this._CoreUsage.ResponseTime = this._HelperService.GetDateTimeS(this._CoreUsage.ResponseTime);
          this.clicked()
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      });
  }



  public _CoreUsage: OCoreUsage =
    {
      Coordinates: null,
      ReferenceKey: null, ReferenceId: null, Reference: null, StatusCode: null, StatusId: null, StatusName: null, UserAccountKey: null, UserAccountIconUrl: null, UserAccountDisplayName: null, ApiKey: null, ApiName: null, AppKey: null, AppName: null, AppOwnerKey: null, AppOwnerName: null, AppVersionKey: null, AppVersionName: null, FeatureKey: null, FeatureName: null, IpAddress: null, Latitude: null, Longitude: null, ProcessingTime: null, Request: null, RequestTime: null, Response: null, ResponseTime: null, SessionId: null, SessionKey: null, UserAccountTypeCode: null, UserAccountTypeName: null,
    }


  //Add City---
  Form_AddApp: FormGroup;
  Form_AddApp_Show() {
    this.Form_AddApp_Clear();
    this._HelperService.OpenModal('Form_AddCity_Content');
  }
  Form_AddApp_Close() {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
    this._HelperService.CloseModal('Form_AddCity_Content');
  }
  Form_AddApp_Load() {
    this.Form_AddApp = this._FormBuilder.group({
      // AllowDuplicateName: false,
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.savecity,
      // TypeCode: this._HelperService.AppConfig.HelperTypes.App,
      Name: [null, Validators.required],
      StateId: this._HelperService.AppConfig.ActiveReferenceId,
      StateKey: this._HelperService.AppConfig.ActiveReferenceKey,
      Latitude:  [0, Validators.required],
      Longitude:  [0, Validators.required],
      StatusCode: 'default.active'
      // Description: [null, Validators.required],
      // LogRequest: [null, Validators.required],
      // UserAccountKey: null,
      // Value: this._HelperService.GenerateGuid(),

    });
  }
  Form_AddApp_Clear() {
    this.Form_AddApp.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddApp_Load();
  }
  Form_AddApp_Process(_FormValue: any) {
    this._HelperService.ToggleField = true;
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.City, _FormValue);

    _OResponse.subscribe(
      _Response => {
        this._HelperService.ToggleField = false;
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("City added successfully");

          this.Form_AddApp_Clear();
          this.RequestHistory_Setup()
          if (_FormValue.OperationType == 'edit') {
            this.Form_AddApp_Close();
            // this._Router.navigate([this._HelperService.AppConfig.Pages.System.AddCoreHelpers, _Response.Result.ReferenceKey]);
          }
          else if (_FormValue.OperationType == 'close') {
            this.Form_AddApp_Close();
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }



  //Edit Feature

  GetCityDetails(cId, Ckey) {

    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.getcity,
      ReferenceId: cId,
      ReferenceKey: Ckey,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.City, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this._UserAccount = _Response.Result;
          this._ChangeDetectorRef.detectChanges();
        }
        else {

          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


  cityrefid: any; cityrefkey: any; cityname: any; citylat: any; citylong: any;
  Form_Edit: FormGroup;
  Form_Edit_Show(ReferenceData) {


    // this.cityname=ReferenceData.Name;
    // this.citylat=ReferenceData.Name;
    // this.citylong=ReferenceData.Name;
    this.cityrefid = ReferenceData.ReferenceId,
      this.cityrefkey = ReferenceData.ReferenceKey,
      this.GetCityDetails(this.cityrefid, this.cityrefkey)
    this._HelperService.OpenModal('Form_EditCity_Content');
  }
  Form_Edit_Close() {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
    this._HelperService.CloseModal('Form_EditCity_Content');
    this.RequestHistory_Setup();
  }
  Form_Edit_Load() {
    this.Form_Edit = this._FormBuilder.group({
      //  OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.updatecity,
      ReferenceId: this.cityrefid,
      ReferenceKey: this.cityrefkey,
      StateId: this._HelperService.AppConfig.ActiveReferenceId,
      StateKey: this._HelperService.AppConfig.ActiveReferenceKey,
      Name: [null, Validators.required],
      Latitude:  [null, Validators.required],
      Longitude:  [null, Validators.required],
      StatusCode: [this._HelperService.AppConfig.StatusCode, Validators.required],
   
    });
  }
  Form_Edit_Clear() {

    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();

  }
  Form_Edit_Process(_FormValue: any) {
    _FormValue.ReferenceKey = this.cityrefkey;
    // _FormValue.AuthPin = result.value;
    _FormValue.ReferenceKey = this.cityrefkey;
    _FormValue.ReferenceId = this.cityrefid;

    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.City, _FormValue);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess('City Updated successfully');
          this.Form_Edit_Clear();
          //this.RequestHistory_Setup()
          // this.unclick();
          if (_FormValue.OperationType == 'edit') {
            this.Form_Edit_Close();
            // this._Router.navigate([this._HelperService.AppConfig.Pages.System.AddCoreHelpers, _Response.Result.ReferenceKey]);
          }
          else if (_FormValue.OperationType == 'close') {
            this.Form_Edit_Close();
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });

}

  //ENd Feature
  Deletecityid: any; Deletecitykey: any;
  DeleteCity(ReferenceData) {
    swal({
      //title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      title: "Delete City?",
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      input: 'text',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
      inputClass: 'swalText',
      inputValidator: function (value) {
        if (value === '' || value.length < 4) {
          return 'Enter your 4 digit pin!'
        }
      },
    }).then((result) => {
      if (result.value) {
    
    this.Deletecityid = ReferenceData.ReferenceId;
    this.Deletecitykey = ReferenceData.ReferenceKey;

    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.deletecity,
      ReferenceId: this.Deletecityid,
      ReferenceKey: this.Deletecitykey,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.City, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this._HelperService.NotifySuccess("City Deleted Successfully.");
          this.RequestHistory_Setup();
          // this._HelperService.CloseModal('FlashDeal');
          this._HelperService.IsFormProcessing = false;
        }
        else {

          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
})


  }

  public _UserAccount: any =
    {
      Name: null,
      Latitude: null,
      Longitude: null,
      StatusCode: null
    }
}


export class OCoreUsage {
  Coordinates?: any;
  public ReferenceId: number;
  public Reference: string;
  public ReferenceKey: string;
  public AppOwnerKey: string;
  public AppOwnerName: string;
  public AppKey: string;
  public AppName: string;
  public ApiKey: string;
  public ApiName: string;
  public AppVersionKey: string;
  public AppVersionName: string;
  public FeatureKey: string;
  public FeatureName: string;
  public UserAccountKey: string;
  public UserAccountDisplayName: string;
  public UserAccountIconUrl: string;
  public UserAccountTypeCode: string;
  public UserAccountTypeName: string;
  public SessionId: string;
  public SessionKey: string;
  public IpAddress: string;
  public Latitude: string;
  public Longitude: string;
  public Request: string;
  public Response: string;
  public RequestTime: Date;
  public ResponseTime: Date;
  public ProcessingTime: string;
  public StatusId: number;
  public StatusCode: string;
  public StatusName: string;
}
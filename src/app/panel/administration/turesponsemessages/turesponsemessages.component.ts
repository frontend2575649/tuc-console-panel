import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;
declare var moment: any;
import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../service/service";
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: "tu-turesponsemessages",
  templateUrl: "./turesponsemessages.component.html",
})
export class TUResposnseMessagesComponent implements OnInit, OnDestroy {
  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = true;
    this._HelperService.showAddNewSubAccBtn = false;


  }
  @ViewChild("offCanvas") divView: ElementRef;

  ngOnInit() {
    this._HelperService.ValidateData();

    this._HelperService.StopClickPropogation();
    Feather.replace();
    this.InitBackDropClickEvent();

    this.RequestHistory_Setup();
    this.RequestHistory_Filter_Stores_Load();
    this.InitColConfig();
    this.Form_Add_Load();
    this.Form_Edit_Load();
    // this._HelperService.StopClickPropogation();
    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.RequestHistory_GetData();
    // });
  }


  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }
  }
  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region RequestHistory
  public CurrentRequest_Key: string;
  public Message: string;
  public Description: string;
  public Name: string;


  public CurrentRequest_Id: number;


  public RequestHistory_Config: OList;
  RequestHistory_Setup() {
    this.RequestHistory_Config = {
      Id: null,
      Type: "all",
      Sort: null,
      Task: this._HelperService.AppConfig.Api.Core.GetResponsecodes,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.System,
      Title: 'Features',
      StatusType: 'default',
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.HelperTypes.Feature, "="),
      TableFields: [
        {
          DisplayName: 'Name',
          SystemName: 'Name',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Message',
          SystemName: 'Message',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Description',
          SystemName: 'Description',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: "Updated On",
          SystemName: 'ModifyDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
        },
        // {
        //   DisplayName: this._HelperService.AppConfig.CommonResource.CreatedBy,
        //   SystemName: 'CreatedByDisplayName',
        //   DataType: this._HelperService.AppConfig.DataType.Text,
        //   Show: true,
        //   Search: false,
        //   Sort: true,
        //   ResourceId: null,
        // },
        // {
        //   DisplayName: this._HelperService.AppConfig.CommonResource.ModifyDate,
        //   SystemName: 'ModifyDate',
        //   DataType: this._HelperService.AppConfig.DataType.Date,
        //   Class: 'td-date',
        //   Show: true,
        //   Search: false,
        //   Sort: true,
        //   ResourceId: null,
        // },
        // {
        //   DisplayName: this._HelperService.AppConfig.CommonResource.ModifyBy,
        //   SystemName: 'ModifyByDisplayName',
        //   DataType: this._HelperService.AppConfig.DataType.Text,
        //   Show: true,
        //   Search: false,
        //   Sort: true,
        //   ResourceId: null,
        // },
      ]
    };
    this.RequestHistory_Config = this._DataHelperService.List_Initialize(
      this.RequestHistory_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Cashier,
      this.RequestHistory_Config
    );

    this.RequestHistory_GetData();
  }
  RequestHistory_ToggleOption(event: any, Type: any) {
    if (Type == "date") {
      this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
      this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
  }
    if (event != null) {
      for (let index = 0; index < this.RequestHistory_Config.Sort.SortOptions.length; index++) {
        const element = this.RequestHistory_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.RequestHistory_Config
    );

    this.RequestHistory_Config = this._DataHelperService.List_Operations(
      this.RequestHistory_Config,
      event,
      Type
    );

    if (
      (this.RequestHistory_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.RequestHistory_GetData();
    }

  }
  RequestHistory_GetData() {

    var TConfig = this._DataHelperService.List_GetDataWithOutSort(
      this.RequestHistory_Config
    );
    this.RequestHistory_Config = TConfig;
  }
  RequestHistory_RowSelected(ReferenceData) {
    this.CurrentRequest_Key = ReferenceData.ReferenceKey;
    this.CurrentRequest_Id = ReferenceData.ReferenceId;
    this.Message = ReferenceData.Message
    this.Name = ReferenceData.Name

    this.Description = ReferenceData.Description
    // this.ListAppUsage_GetDetails();
    this.clicked()

  }

  //#endregion

  //#region StoreFilter

  public RequestHistory_Filter_Stores_Option: Select2Options;
  public RequestHistory_Filter_Stores_Selected = null;
  RequestHistory_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      // AccountKey: this._HelperService.UserAccount.AccountKey,
      // AccountId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.RequestHistory_Filter_Stores_Option = {
      placeholder: "Select Store",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  RequestHistory_Filter_Stores_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.RequestHistory_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.RequestHistory_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.RequestHistory_Filter_Stores_Selected,
        "="
      );
      this.RequestHistory_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.RequestHistory_Config.SearchBaseConditions
      );
      this.RequestHistory_Filter_Stores_Selected = null;
    } else if (event.value != this.RequestHistory_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.RequestHistory_Filter_Stores_Selected,
        "="
      );
      this.RequestHistory_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.RequestHistory_Config.SearchBaseConditions
      );
      this.RequestHistory_Filter_Stores_Selected = event.data[0].ReferenceKey;
      this.RequestHistory_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "StoreReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.RequestHistory_Filter_Stores_Selected,
          "="
        )
      );
    }

    this.RequestHistory_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  //Add feature
  Form_Add: FormGroup;
  Form_Add_Show() {
    this.Form_Add_Clear();
    this._HelperService.OpenModal('Form_Add_Content');
  }
  Form_Add_Close() {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
    this._HelperService.CloseModal('Form_Add_Content');
  }
  Form_Add_Load() {
    this.Form_Add = this._FormBuilder.group({
      AllowDuplicateName: false,
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.SaveCoreCommon,
      TypeCode: this._HelperService.AppConfig.HelperTypes.Feature,
      Name: [null, Validators.required],
      SystemName: [null, Validators.required],
      Value: [null, Validators.required],
      Sequence: 0,
      StatusCode: 'default.inactive',
    });
  }
  Form_Add_Clear() {
    this.Form_Add.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_Add_Load();
  }
  Form_Add_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.RequestHistory_Setup();
          this.Form_Add_Clear();
          if (_FormValue.OperationType == 'edit') {
            this.Form_Add_Close();
            this._HelperService.AppConfig.ActiveReferenceKey = _Response.Result.ReferenceKey;
            // this.ListFeatures_GetDetails();
            // this._Router.navigate([this._HelperService.AppConfig.Pages.System.AddCoreHelpers, _Response.Result.ReferenceKey]);
          }
          else if (_FormValue.OperationType == 'close') {
            this.Form_Add_Close();
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  //ENd

  //Edit Feature
  Form_Edit: FormGroup;
  Form_Edit_Show() {
    this._HelperService.OpenModal('Form_Edit_Content');
  }
  Form_Edit_Close() {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
    this._HelperService.CloseModal('Form_Edit_Content');
    this.RequestHistory_Setup();
  }
  Form_Edit_Load() {
    this.Form_Edit = this._FormBuilder.group({
      Task: this._HelperService.AppConfig.Api.Core.UpdateResponsecode,
      ReferenceId: this.CurrentRequest_Id,
      ReferenceKey: this.CurrentRequest_Key,

      Message: [null, Validators.required],
      Description: [null, Validators.required]
    });
  }
  Form_Edit_Clear() {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
  }
  Form_Edit_Process(_FormValue: any) {
    swal({
      title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      input: 'password',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {
        _FormValue.ReferenceKey = this._HelperService._CoreCommon.ReferenceKey;
        _FormValue.AuthPin = result.value;
        _FormValue.ReferenceKey = this.CurrentRequest_Key;
        _FormValue.ReferenceId = this.CurrentRequest_Id;

        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, _FormValue);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess('Response Code Updated successfully');
              this.Form_Edit_Clear();
              this.unclick();
              if (_FormValue.OperationType == 'close') {
                this.Form_Edit_Close();
              }
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          });
      }
    });

  }
  //ENd Feature



  SetOtherFilters(): void {
    this.RequestHistory_Config.SearchBaseConditions = [];
    // this.RequestHistory_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.HelperTypes.Feature, "=");

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.RequestHistory_Filter_Stores_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);

    //#region setOtherFilters
    //this.SetOtherFilters();
    //#endregion

    this.RequestHistory_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_RequestHistory(Type, index);
    this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);
    if (Type == 'Time') {
      this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
      this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
  }
    this.SetOtherFilters();

    this.RequestHistory_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_RequestHistory(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Cashier
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Cashier
        );
        this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);
        this.RequestHistory_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.RequestHistory_GetData();

    if (ButtonType == 'Sort') {
      $("#RequestHistory_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#RequestHistory_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);
    this.SetOtherFilters();

    this.RequestHistory_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.RequestHistory_Filter_Stores_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }


  clicked() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }


  ListAppUsage_GetDetails() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetCoreCommon,
      Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this.CurrentRequest_Key, '='),
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._CoreUsage = _Response.Result as OCoreUsage;
          this._CoreUsage.CreateDateS = this._HelperService.GetDateTimeS(this._CoreUsage.CreateDate);
          this._CoreUsage.ModifyDateS = this._HelperService.GetDateTimeS(this._CoreUsage.ModifyDate);
          this._CoreUsage.StatusI = this._HelperService.GetStatusIcon(this._CoreUsage.StatusCode);
          this.clicked()
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      });
  }
  Delete_Confirm() {
    swal({
      position: 'top',
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      input: 'password',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;
        var pData = {
          Task: this._HelperService.AppConfig.Api.Core.DeleteCoreCommon,
          ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
          AuthPin: result.value
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess(_Response.Message);
              this.unclick();
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {

            this._HelperService.HandleException(_Error);
          });
      }
    });
  }


  public _CoreUsage: OCoreUsage =
    {
      Count: 0,
      CreateDate: null,
      CreatedByDisplayName: null,
      CreatedByIconUrl: null,
      CreatedByKey: null,
      Data: null,
      Description: null,
      PlatformCharge: null,
      HelperCode: null,
      HelperName: null,
      IconUrl: null,
      ModifyByDisplayName: null,
      ModifyByIconUrl: null,
      ModifyByKey: null,
      ModifyDate: null,
      Name: null,
      ParentCode: null,
      ParentKey: null,
      ParentName: null,
      PosterUrl: null,
      Reference: null,
      ReferenceKey: null,
      ReferenceId: null,
      Sequence: null,
      SubItemsCount: null,
      StatusCode: null,
      StatusId: null,
      StatusName: null,
      SubParentCode: null,
      SubParentKey: null,
      SubParentName: null,
      SubValue: null,
      SystemName: null,
      TypeCode: null,
      TypeName: null,
      UserAccountDisplayName: null,
      UserAccountIconUrl: null,
      UserAccountKey: null,
      Value: null,
      StatusI: null,
      CreateDateS: null,
      ModifyDateS: null

    }


}


export class OCoreUsage {
  public Reference: string;
  public ReferenceKey: string;
  public ReferenceId: string;
  public SystemName: string;
  public UserAccountKey: string;
  public UserAccountDisplayName: string;
  public UserAccountIconUrl: string;
  public TypeCode: string;
  public TypeName: string;
  public HelperCode: string;
  public HelperName: string;
  public ParentKey: string;
  public ParentCode: string;
  public ParentName: string;
  public SubParentKey: string;
  public SubParentCode: string;
  public SubParentName: string;
  public PlatformCharge?: any;
  public Name: string;
  public Value: string;
  public SubValue: string;
  public Description: string;
  public Data: string;
  public Sequence: number = 0;
  public Count: number = 0;
  public SubItemsCount: number = 0;
  public IconUrl: string;
  public PosterUrl: string;
  public CreateDate: Date;
  public CreatedByKey: string;
  public CreatedByDisplayName: string;
  public CreatedByIconUrl: string;
  public ModifyDate: Date;
  public ModifyByKey: string;
  public ModifyByDisplayName: string;
  public ModifyByIconUrl: string;
  public StatusId: number = 0;
  public StatusCode: string;
  public StatusName: string;
  public StatusI: string;
  public CreateDateS: string;
  public ModifyDateS: string;
}
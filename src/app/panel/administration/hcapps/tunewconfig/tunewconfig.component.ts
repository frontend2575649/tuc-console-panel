import { ChangeDetectorRef, Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;

import {
    DataHelperService,
    HelperService,
    OList,
    OSelect,
    FilterHelperService,
    OResponse,
} from "../../../../service/service";
import { Observable, Subscription } from 'rxjs';

@Component({
    selector: "tu-tunewconfig",
    templateUrl: "./tunewconfig.component.html",
})
export class TUNewConfigComponent implements OnInit, OnDestroy {
    public ResetFilterControls: boolean = true;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _FilterHelperService: FilterHelperService
    ) {
        this._HelperService.showAddNewPosBtn = false;
        this._HelperService.showAddNewStoreBtn = true;
        this._HelperService.showAddNewCashierBtn = false;
        this._HelperService.showAddNewSubAccBtn = false;

    }

    ngOnInit() {





        this._ActivatedRoute.params.subscribe((params: Params) => {

            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
            this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
            this._HelperService.AppConfig.ActiveAccountId = params['accountid'];
            this._HelperService.AppConfig.ActiveAccountKey = params['accountkey'];

            this.Applist_Setup();
            this.Applist_Filter_Owners_Load();
            this.InitColConfig();
            this.Form_AddApp_Load();
            this.GetAddAppUserAccounts_List();


        });
    }
    ngOnDestroy() { }

    public Applist_Config: OList;
    Applist_Setup() {
        this.Applist_Config = {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.Core.GetApps,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.System,
            Title: "Available Stores",
            StatusType: "default",
            // Type: this._HelperService.AppConfig.ListType.SubOwner,
            // AccountKey: this._HelperService.UserAccount.AccountKey,
            // AccountId: this._HelperService.UserAccount.AccountId,
            DefaultSortExpression: "CreateDate desc",
            // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
            TableFields: [
                {
                    DisplayName: 'App',
                    SystemName: 'Name',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Owner',
                    SystemName: 'AccountDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },

                {
                    DisplayName: 'Version',
                    SystemName: 'ActiveVersion',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Request',
                    SystemName: 'TotalRequest',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Last Request',
                    SystemName: 'LastRequestDateTime',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: 'CreateDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    IsDateSearchField: true,

                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreatedBy,
                    SystemName: 'CreatedByDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                // {
                //     DisplayName: this._HelperService.AppConfig.CommonResource.ModifyDate,
                //     SystemName: 'ModifyDate',
                //     DataType: this._HelperService.AppConfig.DataType.Date,
                //     Class: 'td-date',
                //     Show: true,
                //     Search: false,
                //     Sort: true,
                //     ResourceId: null,
                // },
                // {
                //     DisplayName: this._HelperService.AppConfig.CommonResource.ModifyBy,
                //     SystemName: 'ModifyByDisplayName',
                //     DataType: this._HelperService.AppConfig.DataType.Text,
                //     Show: true,
                //     Search: false,
                //     Sort: true,
                //     ResourceId: null,
                // },
            ]
        };
        this.Applist_Config = this._DataHelperService.List_Initialize(
            this.Applist_Config
        );

        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.Stores,
            this.Applist_Config
        );

        this.Applist_GetData();
    }


    Applist_GetData() {

    }
    Form_AddNewApp_Process(_FormValue: any) {
        this._HelperService.ToggleField = true;
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, _FormValue);

        _OResponse.subscribe(
            _Response => {
                this._HelperService.ToggleField = false;
                this._HelperService.IsFormProcessing = false;

                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                    this.Applist_Setup();
                    this.Form_AddApp_Clear();

                    if (_FormValue.OperationType == 'edit') {
                        this.Form_AddApp_Close();
                    }
                    else if (_FormValue.OperationType == 'close') {
                        this.Form_AddApp_Close();
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });

    }

    Form_AddApp_Close() {
        // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
        this._HelperService.CloseModal('Form_AddApp_Content');
    }
    Form_AddNewApp: FormGroup;
    Form_AddApp_Show() {
        this.Form_AddApp_Clear();
        this._HelperService.OpenModal('Form_AddApp_Content');
    }
    Form_AddApp_Clear() {
        this.Form_AddNewApp.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
    }

    Form_AddApp_Load() {
        this.Form_AddNewApp = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.NewApp,
            Name: [null, Validators.required],
            AccountId: [null, Validators.required],
            AccountKey: [null, Validators.required],
            LogRequest: 0,
            StatusCode: 'default.inactive',
        })

    }



    Applist_Filter_Owners_Load() {

    }

    InitColConfig() {

    }

    GetAddAppUserAccounts_List() {

    }


}

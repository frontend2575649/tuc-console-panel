import { ChangeDetectorRef, Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;
declare var moment: any;
import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: "tu-tuapplist",
  templateUrl: "./tuapplist.component.html",
})
export class TUAppListComponent implements OnInit, OnDestroy {
  public ResetFilterControls: boolean = true;

  public _ObjectSubscription: Subscription = null;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = true;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;

  }


  ngOnInit() {
    this._HelperService.StopClickPropogation();
    this._HelperService.ValidateData();
    Feather.replace();



    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
      this._HelperService.AppConfig.ActiveAccountId = params['accountid'];
      this._HelperService.AppConfig.ActiveAccountKey = params['accountkey'];

      this.Applist_Setup();
      this.Applist_Filter_Owners_Load();
      this.InitColConfig();
      this.Form_AddApp_Load();
      this.GetAddAppUserAccounts_List();


    });

    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.Applist_GetData();
    // });
    // this._HelperService.StopClickPropogation();
  }

  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {
    }
  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region Storelist

  public Applist_Config: OList;
  Applist_Setup() {
    this.Applist_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.Core.GetApps,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.System,
      Title: "Available Stores",
      StatusType: "default",
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      // AccountKey: this._HelperService.UserAccount.AccountKey,
      // AccountId: this._HelperService.UserAccount.AccountId,
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: 'App',
          SystemName: 'Name',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Owner',
          SystemName: 'AccountDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },

        {
          DisplayName: 'Version',
          SystemName: 'ActiveVersion',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Request',
          SystemName: 'TotalRequest',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Last Request',
          SystemName: 'LastRequestDateTime',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,

        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreatedBy,
          SystemName: 'CreatedByDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        // {
        //     DisplayName: this._HelperService.AppConfig.CommonResource.ModifyDate,
        //     SystemName: 'ModifyDate',
        //     DataType: this._HelperService.AppConfig.DataType.Date,
        //     Class: 'td-date',
        //     Show: true,
        //     Search: false,
        //     Sort: true,
        //     ResourceId: null,
        // },
        // {
        //     DisplayName: this._HelperService.AppConfig.CommonResource.ModifyBy,
        //     SystemName: 'ModifyByDisplayName',
        //     DataType: this._HelperService.AppConfig.DataType.Text,
        //     Show: true,
        //     Search: false,
        //     Sort: true,
        //     ResourceId: null,
        // },
      ]
    };
    this.Applist_Config = this._DataHelperService.List_Initialize(
      this.Applist_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Stores,
      this.Applist_Config
    );

    this.Applist_GetData();
  }
  Applist_ToggleOption(event: any, Type: any) {
    if (Type == "date") {
      this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
      this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
  }
    if (event != null) {
      for (let index = 0; index < this.Applist_Config.Sort.SortOptions.length; index++) {
        const element = this.Applist_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.Applist_Config
    );

    this.Applist_Config = this._DataHelperService.List_Operations(
      this.Applist_Config,
      event,
      Type
    );

    if (
      (this.Applist_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.Applist_GetData();
    }

  }

  timeout = null;
  Applist_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.Applist_Config.Sort.SortOptions.length; index++) {
          const element = this.Applist_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.Applist_Config
      );

      this.Applist_Config = this._DataHelperService.List_Operations(
        this.Applist_Config,
        event,
        Type
      );

      if (
        (this.Applist_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.Applist_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);


  }

  Applist_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.Applist_Config
    );
    this.Applist_Config = TConfig;
  }
  Applist_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveApp,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        AccountKey: ReferenceData.AccountKey,
        AccountId: ReferenceData.AccountId,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId
    this._HelperService.AppConfig.ActiveAccountKey = ReferenceData.AccountKey
    this._HelperService.AppConfig.ActiveAccountId = ReferenceData.AccountId

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Administration.AppRequesthistory,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
      ReferenceData.AccountId,
      ReferenceData.AccountKey

    ]);
  }

  //#endregion

  //#region OwnerFilter

  public Applist_Filter_Owners_Option: Select2Options;
  public Applist_Filter_Owners_Selected = null;
  Applist_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
      // ReferenceKey: this._HelperService.UserAccount.AccountKey,
      // ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant,
    //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     this._HelperService.AppConfig.AccountType.PosAccount
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.Applist_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  Applist_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.Applist_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.Applist_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "MerchantReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.Applist_Filter_Owners_Selected,
        "="
      );
      this.Applist_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.Applist_Config.SearchBaseConditions
      );
      this.Applist_Filter_Owners_Selected = null;
    } else if (event.value != this.Applist_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "MerchantReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.Applist_Filter_Owners_Selected,
        "="
      );
      this.Applist_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.Applist_Config.SearchBaseConditions
      );
      this.Applist_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.Applist_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "MerchantReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.Applist_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.Applist_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.Applist_Config.SearchBaseConditions = [];
    this.Applist_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.Applist_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.Applist_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.Applist_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.Applist_Config);
    if (Type == 'Time') {
      this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
      this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
  }
    this.SetOtherFilters();

    this.Applist_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      // inputAttributes: {
      //   autocapitalize: "off",
      //   autocorrect: "off",
      //   maxLength: "4",
      //   minLength: "4",
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Stores,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Stores
        );
        this._FilterHelperService.SetMerchantConfig(this.Applist_Config);
        this.Applist_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.Applist_GetData();

    if (ButtonType == 'Sort') {
      $("#Applist_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#Applist_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.Applist_Config);
    this.SetOtherFilters();

    this.Applist_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.Applist_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }



  LogRequestValue: number = 0;
  SelectValue: boolean = false;
  CheckValue() {
    this.SelectValue = !(this.SelectValue);
    if (this.SelectValue == true) {
      this.LogRequestValue = 1
      this.Form_AddApp.controls['LogRequest'].setValue(this.LogRequestValue);

    }
    else {
      this.LogRequestValue = 0
      this.Form_AddApp.controls['LogRequest'].setValue(this.LogRequestValue);


    }

  }
  Form_AddApp: FormGroup;
  Form_AddApp_Show() {
    this.Form_AddApp_Clear();
    this._HelperService.OpenModal('Form_AddApp_Content');
  }
  Form_AddApp_Close() {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
    this._HelperService.CloseModal('Form_AddApp_Content');
  }
  Form_AddApp_Load() {
    this.Form_AddApp = this._FormBuilder.group({
      // AllowDuplicateName: false,
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.SaveApp,
      // TypeCode: this._HelperService.AppConfig.HelperTypes.App,
      Name: [null, Validators.required],
      AccountId: [null, Validators.required],
      AccountKey: [null, Validators.required],
      // Description: [null, Validators.required],
      // LogRequest: [null, Validators.required],
      // UserAccountKey: null,
      // Value: this._HelperService.GenerateGuid(),
      LogRequest: 0,
      StatusCode: 'default.inactive',
    });
  }
  Form_AddApp_Clear() {
    this.Form_AddApp.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddApp_Load();
  }
  Form_AddApp_Process(_FormValue: any) {
    this._HelperService.ToggleField = true;
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, _FormValue);

    _OResponse.subscribe(
      _Response => {
        this._HelperService.ToggleField = false;
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.Applist_Setup();
          this.Form_AddApp_Clear();
          if (_FormValue.OperationType == 'edit') {
            this.Form_AddApp_Close();
            // this._Router.navigate([this._HelperService.AppConfig.Pages.System.AddCoreHelpers, _Response.Result.ReferenceKey]);
          }
          else if (_FormValue.OperationType == 'close') {
            this.Form_AddApp_Close();
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  AccountId: any;
  public GetAddAppUserAccounts_Option: Select2Options;
  public GetUpdateAppUserAccounts_Option: Select2Options;
  GetAddAppUserAccounts_List() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: '',
      SortCondition: [],
      Fields: [
        {
          SystemName: 'ReferenceKey',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: 'DisplayName',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        // {
        //     SystemName: 'AccountTypeName',
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     Id: false,
        //     Text: true,
        // },
        // {
        //     SystemName: 'StatusCode',
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: '=',
        //     SearchValue: this._HelperService.AppConfig.Status.Active,
        // }
      ],
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
      [
        this._HelperService.AppConfig.AccountType.Merchant,
        this._HelperService.AppConfig.AccountType.Acquirer,
        this._HelperService.AppConfig.AccountType.PGAccount,
        this._HelperService.AppConfig.AccountType.PosAccount
      ]
      , '=');
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.AccountType.AppUser, '!=');
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'StatusCode', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.Status.Active, '=');

    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetAddAppUserAccounts_Option = {
      placeholder: PlaceHolder,
      ajax: _Transport,
      multiple: false,
    };
    this.GetUpdateAppUserAccounts_Option = {
      placeholder: PlaceHolder,
      ajax: _Transport,
      multiple: false,
    };
  }
  GetAddAppUserAccounts_ListChange(event: any) {
    this.Form_AddApp.patchValue(
      {
        // UserAccountKey: event.value
        AccountId: event.data[0].ReferenceId,
        AccountKey: event.data[0].ReferenceKey

      }
    );
  }
  GetUpdateAppUserAccounts_ListChange(event: any) {
  }


}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { FormsModule } from "@angular/forms";
import { Daterangepicker } from "ng2-daterangepicker";

import { OrderhistoryComponent } from '../orderhistory/orderhistory.component';

const routes: Routes = [
  {
    path: '', component: OrderhistoryComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderhistoryRoutingModule { }

@NgModule({
  declarations: [OrderhistoryComponent],
  imports: [
    CommonModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    OrderhistoryRoutingModule,
    FormsModule,
    Daterangepicker
  ]
})
export class OrderhistoryModule { }

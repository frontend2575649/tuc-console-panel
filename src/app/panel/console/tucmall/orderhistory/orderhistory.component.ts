import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
import swal from 'sweetalert2';
import * as Feather from 'feather-icons';
import * as moment from 'moment';
declare var $: any;

@Component({
  selector: 'app-orderhistory',
  templateUrl: './orderhistory.component.html',
  styleUrls: ['./orderhistory.component.css']
})
export class OrderhistoryComponent implements OnInit {
  public TUTr_Config: OList;
  public ResetFilterControls: boolean = true;
  totalAmountMin = this._HelperService.AppConfig.TotalAmountMin;
  totalAmountMax = this._HelperService.AppConfig.TotalAmountMax;
  orderHistoryOverview: any = {};
  orderHistoryList: any = [];
  ngOnInit() {
    Feather.replace();
    this.getOrderHistoryOverviewData();
    this.TUTr_Setup();
  }

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService: FilterHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
  ) { }

  getOrderHistoryOverviewData() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetOrderHistoryOverview
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.OrderHistoryOverview, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.orderHistoryOverview = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

  TuTr_Setup_Fields() {
    var TableFields = [];

    TableFields = [
      {
        DisplayName: 'Order ID',
        SystemName: 'OrderId',
        DataType: this._HelperService.AppConfig.DataType.Number,
        Show: true,
        Search: true,
        Sort: true,
      },
      {
        DisplayName: 'Invoice Id',
        SystemName: 'InvoiceNumber',
        DataType: this._HelperService.AppConfig.DataType.Text,
        Show: true,
        Search: false,
        Sort: false,
      },
      {
        DisplayName: 'Placed On',
        SystemName: 'CreateDate',
        DataType: this._HelperService.AppConfig.DataType.Date,
        Show: true,
        Search: false,
        Sort: true,
        IsDateSearchField: true,
      },
      {
        DisplayName: 'Delivered',
        SystemName: 'DeliveryDate',
        DataType: this._HelperService.AppConfig.DataType.Date,
        Show: true,
        Search: false,
        Sort: true,
        // IsDateSearchField: true,
      },
      {
        DisplayName: 'Carrier',
        SystemName: 'CarrierName',
        DataType: this._HelperService.AppConfig.DataType.Text,
        Show: true,
        Search: false,
        Sort: false,
      },
      {
        DisplayName: 'Customer',
        SystemName: 'CustomerDisplayName',
        DataType: this._HelperService.AppConfig.DataType.Text,
        Show: true,
        Search: true,
        Sort: true,
      },
      {
        DisplayName: 'Items',
        SystemName: 'ItemCount',
        DataType: this._HelperService.AppConfig.DataType.Number,
        Show: true,
        Search: false,
        Sort: true,
      },
      {
        DisplayName: 'Total',
        SystemName: 'TotalAmount',
        DataType: this._HelperService.AppConfig.DataType.Decimal,
        Show: true,
        Search: false,
        Sort: true,
      },
      {
        DisplayName: 'Deal',
        SystemName: 'DealTitle',
        DataType: this._HelperService.AppConfig.DataType.Text,
        Show: false,
        Search: true,
        Sort: false,
      },
    ]

    return TableFields;
  }

  TUTr_Setup() {
    var SearchCondition = undefined
    this.TUTr_Config =
    {
      Id: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetOrders,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.OrderHistory,
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'CreateDate',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },
      Title: "Order History",
      StatusType: null,
      SearchBaseCondition: SearchCondition,
      TableFields: this.TuTr_Setup_Fields(),
      DefaultSortExpression: "CreateDate desc",
    }

    this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.OrderHistory,
      this.TUTr_Config
    );
    this.TUTr_GetData();
  }

  TUTr_ToggleOption(event: any, Type: any) {
    

    if (event != null) {
      for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
        const element = this.TUTr_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
      event.data = {
        TotalAmountMin: this.totalAmountMin,
        TotalAmountMax: this.totalAmountMax
      }
    }
    this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);
    this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
    if (
      (this.TUTr_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TUTr_GetData();
    }
  }

  timeout = null;
  TUTr_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);
      if (event != null) {
        for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
          const element = this.TUTr_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }

          else {
            element.SystemActive = false;
          }

        }
      }
      this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);

      if (
        (this.TUTr_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.TUTr_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);

  }

  TUTr_GetData() {
    var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
    this.TUTr_Config = TConfig;
    this.orderHistoryList= TConfig;
  }

  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetData(ListOptions);

    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }

    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: SortExpression
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.OrderHistory, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.orderHistoryList = _Response.Result as any;
          if (this.orderHistoryList && this.orderHistoryList.Data && this.orderHistoryList.Data.length > 0) {
            this.orderHistoryList.Data.forEach(element => {
              if (element.CreateDate && element.DeliveryDate && element.StatusId !== 494) {
                const startTime = moment(element.CreateDate);
                const endTime = moment(element.DeliveryDate);

                const hoursDiff = endTime.diff(startTime, 'hours');
                element.hoursDiff = hoursDiff;
              }
            });
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });


  }


  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.OrderHistory
        );
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
        this.TUTr_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  SetSearchRanges(): void {
    //#region Invoice 
    this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalAmount', this.TUTr_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalAmount', this.totalAmountMin, this.totalAmountMax);
    if (this.totalAmountMin == this._HelperService.AppConfig.TotalAmountMin && this.totalAmountMax == this._HelperService.AppConfig.TotalAmountMax) {
      this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
    }
    else {
      this.TUTr_Config.SearchBaseConditions.push(SearchCase);
    }
    //#endregion      
  }


  ApplyFilters(event: any, Type: any, ButtonType: any): void {

    if (this.totalAmountMin == null || this.totalAmountMax == null || this.totalAmountMin < 0 || this.totalAmountMax < 0) {
      this._HelperService.NotifyError("Please Enter a valid amount greater than or equal to 0")
    } else if (this.totalAmountMin > this.totalAmountMax) {
      this._HelperService.NotifyError("Min Amount must be less than Max Amount")
    } else if (this.totalAmountMax < this.totalAmountMin) {
      this._HelperService.NotifyError("Max Amount must be greater than Min Amount")
    }
    else {


      this.SetSearchRanges();
      this._HelperService.MakeFilterSnapPermanent();
      this.TUTr_GetData();

      this.ResetFilterUI(); this._HelperService.StopClickPropogation();

      if (ButtonType == 'Sort') {
        $("#TUTr_sdropdown").dropdown('toggle');
      } else if (ButtonType == 'Other') {
        $("#TUTr_fdropdown").dropdown('toggle');
      }
    }
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

    //#region setOtherFilters
    this.SetSalesRanges();
    this.SetOtherFilters();
    //#endregion
    this.TUTr_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  SetOtherFilters(): void {
    this.TUTr_Config.SearchBaseConditions = [];
  }

  ResetFilterUI(): void {
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

    //#region setOtherFilters
    this.SetSalesRanges();
    this.SetOtherFilters();
    //#endregion

    this.TUTr_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_MerchantSales(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.OrderHistory
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

    //#region setOtherFilters
    this.SetSalesRanges();
    this.SetOtherFilters();
    //#endregion

    this.TUTr_GetData();
  }

  SetSalesRanges(): void {
    this.totalAmountMin = this._HelperService.AppConfig.TotalAmountMin
    this.totalAmountMax = this._HelperService.AppConfig.TotalAmountMax
  }

  downloadReport(): any {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetOrderHistory,
      TotalRecords: 0,
      Offset: 0,
      Limit: 25,
      SearchCondition: '',
      SortExpression: 'CreateDate desc',
      IsDownload: true,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.OrderHistory, pData);
    _OResponse.subscribe(
      (_Response) => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          const listData = _Response.Result as any;
          this._HelperService.NotifySuccess('Your download will be available shortly in downloads section');
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      (_Error) => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  

}

import { ChangeDetectorRef, Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { ImageCroppedEvent } from "ngx-image-cropper";
import { InputFile, InputFileComponent } from 'ngx-input-file';
import { Observable } from 'rxjs';
import { DataHelperService, FilterHelperService, HelperService, OResponse, OSelect } from "../../../../service/service";
declare var $: any;


@Component({
    selector: "tu-tuaddambassador",
    templateUrl: "./tuaddambassador.component.html",
})
export class TUAddAmbassadorComponent implements OnInit {
    CurrentImagesCount: number = 0;
    public _VariantStoresList: any[] = [];
    public _SelectedStore: any;
    public disableSubcategory: boolean = true;

    @ViewChild(InputFileComponent)
    private InputFileComponent: InputFileComponent;
    public ResetFilterControls: boolean = true;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _FilterHelperService: FilterHelperService
    ) {



    }

    ngOnInit() {
        this._HelperService.Icon_Crop_Clear();
        this.List_Filter_Stores_Load();
        this.Form_AddProduct_Load();
        this.GetProductCategories_List();
        //this.GetStores();
        this._HelperService._InputFileComponent = this.InputFileComponent;
        this.InputFileComponent.onChange = (files: Array<InputFile>): void => {
            if (files.length >= this.CurrentImagesCount) {
                this._HelperService._SetFirstImageOrNone(this.InputFileComponent.files);
            }
            this.CurrentImagesCount = files.length;
        }
    }

    Form_AddProduct: FormGroup;
    Form_AddProduct_Address: string = null;
    Form_AddProduct_Latitude: number = 0;
    Form_AddProduct_Longitude: number = 0;
    @ViewChild('places') places: GooglePlaceDirective;
    Form_AddProduct_PlaceMarkerClick(event) {
        this.Form_AddProduct_Latitude = event.coords.lat;
        this.Form_AddProduct_Longitude = event.coords.lng;
    }
    public Form_AddProduct_AddressChange(address: Address) {
        this.Form_AddProduct_Latitude = address.geometry.location.lat();
        this.Form_AddProduct_Longitude = address.geometry.location.lng();
        this.Form_AddProduct_Address = address.formatted_address;
    }
    Form_AddProduct_Show() {
    }
    Form_AddProduct_Close() {
        this._Router.navigate(["console/merchant/" + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Product.Ambassadors,
        this._HelperService.AppConfig.ActiveMerchantReferenceKey,
        this._HelperService.AppConfig.ActiveMerchantReferenceId,
        ]);
    }
    // Form_AddProduct_Close() {
    //     this._Router.navigate(["console/merchant/" + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Product.Ambassadors,
    //     this._HelperService.AppConfig.ActiveMerchantReferenceKey,
    //     this._HelperService.AppConfig.ActiveMerchantReferenceId,
    //     ]);
    // }



    Form_AddProduct_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;

        this.Form_AddProduct = this._FormBuilder.group({
            Task: this._HelperService.AppConfig.Api.Product.saveninjaregistration,
            // DealerId: this._HelperService.AppConfig.ActiveReferenceId,
            //DealerKey: this._HelperService.AppConfig.ActiveReferenceKey,
            ReferenceId: 0,
            ReferenceKey: null,
            //  Sku: [null, Validators.compose([Validators.required, Validators.maxLength(20)])],
            FirstName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            LastName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            //VarientName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            Address: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(1024)])],
            FacebookUrl: [null],
            LinkedInUrl: [null],
            TwitterUrl: null,
            InstagramUrl: null,
            VehicleNumber: [null, Validators.compose([Validators.required])],
            ///ActualPrice: [null, Validators.compose([Validators.required, Validators.max(99999999)])],
            // SellingPrice: [null, Validators.compose([Validators.required, Validators.max(99999999)])],
            // StatusCode: this._HelperService.AppConfig.Status.Product.Active,
            // CategoryKey: [null, Validators.compose([Validators.required])],
            //  ReferenceNumber: this._HelperService.GenerateGuid(),   //optional
            ImageContent: this._HelperService._Icon_Cropper_Data,
            Comment: null,
            ProofContent: null,
            UtilityContent: null,
            Reference1Name: [null],
            Reference2Name: [null],
            Reference1ContactNumber: [null],
            Reference2ContactNumber: [null],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2), Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
            "StatusCode": null,
            "CountryId": 0,
            "RegionId": 0,
            "CityId": 0,
            "CityArea1Id": 0,
            "CityArea2Id": 0,
            "CityArea3Id": 0,
            "CityArea4Id": 0,
            "BankName": [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            "BankAccountNumber": [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            "BankAccountName": [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            "BankCode": [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])]

        });
    }
    Form_AddProduct_Clear() {
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 300);
        this._HelperService.IsFormProcessing = false;
        this.Form_AddProduct_Latitude = 0;
        this.Form_AddProduct_Longitude = 0;
        this.Form_AddProduct.reset();
        this._HelperService.Icon_Crop_Clear();
        this._HelperService.GetRandomNumber();
        this.CommonStockAmount = 0;
        this._ProductVarientLocation.forEach(element => {
            element.Quantity = 0;
        });
        this.Form_AddProduct_Load();
    }

    Form_AddProduct_Process(_FormValue: any) {


        for (let i = 0; i < this.images.length; i++) {
            _FormValue.ImageContent = this.images[0];
            _FormValue.ProofContent = this.images[1];
            _FormValue.UtilityContent = this.images[2];
        }

        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCAmbassador, _FormValue);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess('Registration successfully.');
                    this.images = [];
                    this.Form_AddProduct_Clear();
                    this._HelperService._FileSelect_Icon_Reset();
                    //   this._Router.navigate([ "console/merchant" + "/" + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Product.Ambassadors + "/" + this._HelperService.AppConfig.ActiveMerchantReferenceKey + "/" + this._HelperService.AppConfig.ActiveMerchantReferenceId]);
                    this._Router.navigate(["this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Product.Ambassadors"]);

                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });



    }


    public GetProductCategories_Option: Select2Options;

    GetProductCategories_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.getcategories,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.HCProduct,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'Name',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'ReferenceId', this._HelperService.AppConfig.DataType.Number, '0', '>');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;

        this.GetProductCategories_Option = {
            placeholder: PlaceHolder,
            ajax: _Transport,
            multiple: false,
        };
    }

    public CategoryKey: string;
    public CategoryId: number;

    GetProductCategories_ListChange(event: any) {
        if (event != undefined && event.data.length > 0) {
            this._HelperService.IsFormProcessing = true;
            this.CategoryKey = event.data[0].ReferenceKey;
            this.CategoryId = event.data[0].ReferenceId
            this.disableSubcategory = false;

            this.Form_AddProduct.patchValue(
                {
                    CategoryKey: event.value
                }
            );
            this._HelperService.IsFormProcessing = false;
        }

    }


    public List_Filter_Store_Option: Select2Options;

    public List_Filter_Store_Selected = 0;
    List_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStoresLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
                // },
                // {
                //     SystemName: "AccountTypeCode",
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: "=",
                //     SearchValue: this._HelperService.AppConfig.AccountType.Store
                // }
            ]
        };

        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.List_Filter_Store_Option = {
            placeholder: 'Filter by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };

    }

    List_Filter_Stores_Change(event: any) {
        this._SelectedStore = event.data[0];
        // this.Form_Add.controls['DealerLocationId'].setValue(event.data[0].ReferenceId);
        // this.Form_Add.controls['DealerLocationKey'].setValue(event.data[0].ReferenceKey);
    }
    Add_Store(): void {
        if (!(this._SelectedStore == undefined || this._SelectedStore == null)) {
            if (!this._VariantStoresList.includes(this._SelectedStore)) {
                this._VariantStoresList.push(this._SelectedStore);
            } else {
                this._HelperService.NotifyError("Store Already Added");
            }
        }
    }

    Remove_Store(index: number): void {
        this._VariantStoresList.splice(index, 1);
    }

    public _ProductVarientLocation = [];
    public GetStores() {
        this._HelperService.AppConfig.ShowHeader = true;
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: "getstoreslite",
            SearchCondition: this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '='),
            Offset: 0,
            Limit: 200,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._ProductVarientLocation = _Response.Result.Data;
                    this._ProductVarientLocation.forEach(element => {
                        element.Quantity = 0;
                    });

                } else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    public CommonStockAmount = 0;
    UpdateForAllLocation() {
        this._ProductVarientLocation.forEach(element => {
            element.Quantity = this.CommonStockAmount;
        });
    }




    //image code---
    imageChangedEvent: any = '';
    imageChangedEvent1: any = '';
    croppedImage: any = '';
    croppedImage1: any = ''

    croppedImage2: any = '';
    croppedImage3: any = ''

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;

        this._HelperService.OpenModal("_Icon_Cropper_Modal")
    }
    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage = event.base64;
        this.croppedImage1 = this.croppedImage.replace("data:image/png;base64,", "")
    }
    imageLoaded() {
        // show cropper
    }
    cropperReady() {
        // cropper ready
    }
    loadImageFailed() {
        // show message
    }

    onImgError(event) {
        //event.target.src = this._MerchantDetails.IconUrl;
    }

    Icon_Crop_Clear() {
        this.croppedImage1 = '';
        this.croppedImage = '';
        //this._MerchantDetails.IconUrl="https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/defaults/defaulticon.png";
        // this.croppedImage = '';
        // this.croppedImage.Name = null;
        // this.croppedImage.Content = null;
        // this.croppedImage.Extension = null;
        // this.croppedImage.TypeCode = null;
        // this.CloseModal('_Icon_Cropper_Modal');
    }
    chekimgcon: boolean = false
    chekimgcon1: boolean = false
    //   checkimg()
    //   {
    //       if(this._MerchantDetails.IconUrl=="https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/defaults/defaulticon.png")
    //         {
    //            this.chekimgcon=true;
    //         }
    //         else
    //         {
    //             this.chekimgcon=false;
    //         }

    //   }
    //end code---

    fileChangeEvent1(event: any): void {
        this.imageChangedEvent1 = event;
        //this._HelperService.CloseModal("_Icon_Cropper_Modal")
        this._HelperService.OpenModal("_Icon_Cropper_Modal")

        this.croppedImage3 = this.croppedImage2.replace("data:image/png;base64,", "")
    }
    imageCropped1(event: ImageCroppedEvent) {
        this.croppedImage2 = event.base64;
        this.croppedImage3 = this.croppedImage2.replace("data:image/png;base64,", "")
    }

    images1 = [];
    images2 = [];
    images = [];
    onFileChange4(event) {



        if (event.target.files && event.target.files[0]) {
            var filesAmount = event.target.files.length;
            for (let i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = (event: any) => {
                    // this.images.push(event.target.result); 
                    this.images1.push(event.target.result);
                    this._HelperService._Icon_Cropper_Data.Content = event.target.result.replace("data:image/png;base64,", "")
                        .replace("data:image/jpg;base64,", "")
                        .replace("data:image/jpeg;base64,", "")
                        .replace("data:image/gif;base64,", "");

                    this.images.push({ Name: "", Content: this._HelperService._Icon_Cropper_Data.Content, Extension: "", TypeCode: "", Height: "400", Width: "800" })
                    this.Form_AddProduct.patchValue({
                        fileSource: this.images
                    });
                }

                // this.images2.push({ImageContent: {
                //     Name:"",Content:this._HelperService._Icon_Cropper_Data.Content,Extension:"",TypeCode:"",Height:"400",Width:"800"
                //      }})

                reader.readAsDataURL(event.target.files[i]);
            }
        }
    }
}

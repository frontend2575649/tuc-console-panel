import { ChangeDetectorRef, Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { InputFile, InputFileComponent } from 'ngx-input-file';
import { Observable } from 'rxjs';
import { DataHelperService, FilterHelperService, HelperService, OResponse, OSelect } from "../../../../service/service";
declare var $: any;


@Component({
    selector: "tu-tuaddproduct",
    templateUrl: "./tuaddproduct.component.html",
})
export class TUAddProductsComponent implements OnInit {
    CurrentImagesCount: number = 0;
    public _VariantStoresList: any[] = [];
    public _SelectedStore: any;
    public disableSubcategory: boolean = true;

    @ViewChild(InputFileComponent)
    private InputFileComponent: InputFileComponent;
    public ResetFilterControls: boolean = true;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _FilterHelperService: FilterHelperService
    ) {



    }

    ngOnInit() {
        this._HelperService.Icon_Crop_Clear();
        this.List_Filter_Stores_Load();
        this.Form_AddProduct_Load();
        this.GetProductCategories_List();
        this.GetStores();
        this._HelperService._InputFileComponent = this.InputFileComponent;
        this.InputFileComponent.onChange = (files: Array<InputFile>): void => {
            if (files.length >= this.CurrentImagesCount) {
                this._HelperService._SetFirstImageOrNone(this.InputFileComponent.files);
            }
            this.CurrentImagesCount = files.length;
        }
    }

    Form_AddProduct: FormGroup;
    Form_AddProduct_Address: string = null;
    Form_AddProduct_Latitude: number = 0;
    Form_AddProduct_Longitude: number = 0;
    @ViewChild('places') places: GooglePlaceDirective;
    Form_AddProduct_PlaceMarkerClick(event) {
        this.Form_AddProduct_Latitude = event.coords.lat;
        this.Form_AddProduct_Longitude = event.coords.lng;
    }
    public Form_AddProduct_AddressChange(address: Address) {
        this.Form_AddProduct_Latitude = address.geometry.location.lat();
        this.Form_AddProduct_Longitude = address.geometry.location.lng();
        this.Form_AddProduct_Address = address.formatted_address;
    }
    Form_AddProduct_Show() {
    }
    Form_AddProduct_Close() {
        this._Router.navigate(["console/merchant/" + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Product.Products,
        this._HelperService.AppConfig.ActiveMerchantReferenceKey,
        this._HelperService.AppConfig.ActiveMerchantReferenceId,


        ]);
    }
    Form_AddProduct_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;

        this.Form_AddProduct = this._FormBuilder.group({
            Task: this._HelperService.AppConfig.Api.Product.saveproduct,
            DealerId: this._HelperService.AppConfig.ActiveReferenceId,
            DealerKey: this._HelperService.AppConfig.ActiveReferenceKey,
            Sku: [null, Validators.compose([Validators.required, Validators.maxLength(20)])],
            Name: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(256)])],
            VarientName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            Description: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(1024)])],
            ActualPrice: [null, Validators.compose([Validators.required, Validators.max(99999999)])],
            SellingPrice: [null, Validators.compose([Validators.required, Validators.max(99999999)])],
            StatusCode: this._HelperService.AppConfig.Status.Product.Active,
            CategoryKey: [null, Validators.compose([Validators.required])],
            ReferenceNumber: this._HelperService.GenerateGuid(),   //optional
            IconContent: this._HelperService._Icon_Cropper_Data

        });
    }
    Form_AddProduct_Clear() {
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 300);
        this._HelperService.IsFormProcessing = false;
        this.Form_AddProduct_Latitude = 0;
        this.Form_AddProduct_Longitude = 0;
        this.Form_AddProduct.reset();
        this._HelperService.Icon_Crop_Clear();
        this._HelperService.GetRandomNumber();
        this.CommonStockAmount = 0;
        this._ProductVarientLocation.forEach(element => {
            element.Quantity = 0;
        });
        this.Form_AddProduct_Load();
    }

    Form_AddProduct_Process(_FormValue: any) {

        if (this._ProductVarientLocation.filter(x => x.Quantity > 0).length == 0) {
            this._HelperService.NotifyError('Enter store inventory to save product');
        }
        else {
            if (this._HelperService._Icon_Cropper_Data.Content != null) {
                _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
            }
            var Stores: any[] = [];
            if (this._ProductVarientLocation.length > 0) {
                for (let index = 0; index < this._ProductVarientLocation.length; index++) {
                    const element = this._ProductVarientLocation[index];
                    if (element.Quantity > 0) {
                        Stores.push({ DealerLocationId: element.ReferenceId, DealerLocationKey: element.ReferenceKey, Quantity: element.Quantity });
                    }
                }
                _FormValue.DealerLocations = Stores;
            }
            _FormValue.DealerId = this._HelperService.AppConfig.ActiveMerchantReferenceId,
                _FormValue.DealerKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey,
                this._HelperService.IsFormProcessing = true;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, _FormValue);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess('Product  created successfully.');
                        this.Form_AddProduct_Clear();
                        this._HelperService._FileSelect_Icon_Reset();
                        this._Router.navigate(["console/merchant" + "/" + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Product.Products + "/" + this._HelperService.AppConfig.ActiveMerchantReferenceKey + "/" + this._HelperService.AppConfig.ActiveMerchantReferenceId]);

                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
            //}
        }

    }


    public GetProductCategories_Option: Select2Options;

    GetProductCategories_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.getcategories,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.HCProduct,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'Name',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'ReferenceId', this._HelperService.AppConfig.DataType.Number, '0', '>');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;

        this.GetProductCategories_Option = {
            placeholder: PlaceHolder,
            ajax: _Transport,
            multiple: false,
        };
    }

    public CategoryKey: string;
    public CategoryId: number;

    GetProductCategories_ListChange(event: any) {
        if (event != undefined && event.data.length > 0) {
            this._HelperService.IsFormProcessing = true;
            this.CategoryKey = event.data[0].ReferenceKey;
            this.CategoryId = event.data[0].ReferenceId
            this.disableSubcategory = false;

            this.Form_AddProduct.patchValue(
                {
                    CategoryKey: event.value
                }
            );
            this._HelperService.IsFormProcessing = false;
        }

    }


    public List_Filter_Store_Option: Select2Options;

    public List_Filter_Store_Selected = 0;
    List_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStoresLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
                // },
                // {
                //     SystemName: "AccountTypeCode",
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: "=",
                //     SearchValue: this._HelperService.AppConfig.AccountType.Store
                // }
            ]
        };

        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.List_Filter_Store_Option = {
            placeholder: 'Filter by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };

    }

    List_Filter_Stores_Change(event: any) {
        this._SelectedStore = event.data[0];
        // this.Form_Add.controls['DealerLocationId'].setValue(event.data[0].ReferenceId);
        // this.Form_Add.controls['DealerLocationKey'].setValue(event.data[0].ReferenceKey);
    }
    Add_Store(): void {
        if (!(this._SelectedStore == undefined || this._SelectedStore == null)) {
            if (!this._VariantStoresList.includes(this._SelectedStore)) {
                this._VariantStoresList.push(this._SelectedStore);
            } else {
                this._HelperService.NotifyError("Store Already Added");
            }
        }
    }

    Remove_Store(index: number): void {
        this._VariantStoresList.splice(index, 1);
    }

    public _ProductVarientLocation = [];
    public GetStores() {
        this._HelperService.AppConfig.ShowHeader = true;
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: "getstoreslite",
            SearchCondition: this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '='),
            Offset: 0,
            Limit: 200,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._ProductVarientLocation = _Response.Result.Data;
                    this._ProductVarientLocation.forEach(element => {
                        element.Quantity = 0;
                    });

                } else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    public CommonStockAmount = 0;
    UpdateForAllLocation() {
        this._ProductVarientLocation.forEach(element => {
            element.Quantity = this.CommonStockAmount;
        });
    }







}

import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';
import { Observable } from 'rxjs';
declare var $: any;
import { DataHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
import * as Feather from "feather-icons";

@Component({
    selector: 'tu-tuorder',
    templateUrl: './tuorder.component.html',
})
export class TUOrderComponent implements OnInit {

    public StatusUpdate = [

        {
            'id': 488,
            'text': 'Order Confirmed',
            'code': 'orderstatus.confirmed',
            index: 1
        },

        {
            'id': 489,
            'text': 'Preparing',
            'code': 'orderstatus.preparing',
            index: 2


        },
        {
            'id': 491,
            'text': 'Ready To PickUp',
            'code': 'orderstatus.readytopickup',
            index: 3


        },
        {
            'id': 492,
            'text': 'Out  For Delivery',
            'code': 'orderstatus.outfordelivery',
            index: 4


        },
        {
            'id': 512,
            'text': 'Delivered',
            'code': 'orderstatus.delivered',
            index: 5
        },
        {
            'id': 493,
            'text': 'Delivery Failed',
            'code': 'orderstatus.deliveryfailed',
            index: 6

        }
    ];

    public _ProductDetail: any = {};
    public InvoiceKey = null;
    public StoreName = null;
    public _OrderDetail: any = {};
    public _TUOrder: any = null;
    public ListType: number;
    public origin: any;
    public destination: any;
    public selectedStatusItem: any = { index: 0 };
    public FIndStatusItem: any = { index: 0 };
    public commnetvar: boolean;
    public _ShippingAddress: any = {};
    public _BillingAddress: any = {};

    public MapUrl: any;
    public PrintInVoice:boolean = false;


    public renderOptions = {
        suppressMarkers: true,
    }
    public DUrl = '../../../assets/img/Pin.svg';
    public SUrl = '../../../assets/img/PinDestination.svg';
    public markerOptions = {
        origin: {
            icon: '../../../assets/img/Pin.svg',
            draggable: false,

        },
        destination: {
            icon: '../../../assets/img/PinDestination.svg',
            draggable: false,
        },
    }

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {
    }

    ngOnInit() {
        //  this._HelperService.FullContainer = true;
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceId= params["referenceid"];
            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
            this._HelperService.AppConfig.ActiveAccountKey = params["accountid"];
            this._HelperService.AppConfig.ActiveAccountId= params["accountkey"];

            if (this._HelperService.AppConfig.ActiveReferenceId == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
            } else {
                this.Setup_UserOrderDetails();
                this.TrackShipments()
            }
        });

        this.ListType = 1;
        this.InvoiceList_Setup();
        this._HelperService.Icon_Crop_Clear();
        Feather.replace();
    }



    htmltoPDF() {
        //this._HelperService.OpenModal('ModalReport');

        this.PrintInVoice = true;
        setTimeout(() => {

            this.StartDownload();
            this.PrintInVoice = false;
        }, 1000);
    }

    StartDownload() {

        html2canvas(document.querySelector("#PrintInvoice"), { useCORS: true }).then(canvas => {
            var imgWidth = 208;
            var pageHeight = 295;
            var imgHeight = canvas.height * imgWidth / canvas.width;
            var heightLeft = imgHeight;
            const contentDataURL = canvas.toDataURL('image/png');
            let pdf1 = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
            var position = 0;
            pdf1.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
            pdf1.save('invoice_' + this._OrderDetail.ReferenceId + '.pdf'); // Generated PDF  

        });

    }

    public SelectedProductItem =
        {
            ReferenceId: null,
            ReferenceKey: null,
            AccountId: null,
            AccountKey: null,
            AccountDisplayName: null,
            CategoryKey: null,
            CategoryName: null,
            Name: null,
            SystemName: null,
            Description: null,
            IconUrl: null,
            PosterUrl: null,
            MinimumAmount: null,
            MaximumAmount: null,
            ActualPrice: null,
            SellingPrice: null,
            AllowMultiple: null,
            RewardPercentage: null,
            MaximumRewardAmount: null,
            TotalStock: null,
            CreateDate: null,
            CreatedByDisplayName: null,
            CreatedByKey: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
        };

    //new table end

    // public Get_UserOrderDetails(TItem) {
    //     this._HelperService.AppConfig.ShowHeader = true;
    //     this._HelperService.IsFormProcessing = true;
    //     var pData = {
    //         Task: "getshipment",
    //         ReferenceId: TItem.ReferenceId,
    //         ReferenceKey: TItem.ReferenceKey
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
    //     _OResponse.subscribe(
    //         _Response => {
    //             if (_Response.Status == this._HelperService.StatusSuccess) {

    //                 this._OrderDetail = _Response.Result;
    //                 console.log(" this._OrderDetail ",  this._OrderDetail )
    //                 this._OrderDetail.CreateDate = this._HelperService.GetDateS(this._OrderDetail.CreateDate);
    //                 this._OrderDetail.CreateDateS = this._HelperService.GetDateTimeS(this._OrderDetail.CreateDate);
    //                 this._OrderDetail.ModifyDateS = this._HelperService.GetDateTimeS(this._OrderDetail.ModifyDate);
    //                 this._OrderDetail.StatusI = this._HelperService.GetStatusIcon(this._OrderDetail.StatusCode);
    //                 this._OrderDetail.StatusB = this._HelperService.GetStatusBadge(this._OrderDetail.StatusCode);
    //                 this._OrderDetail.StatusC = this._HelperService.GetStatusColor(this._OrderDetail.StatusCode);

    //                 if (this._OrderDetail.CreatedByDisplayName != undefined) {
    //                     if (this._OrderDetail.CreatedByDisplayName.length > 8) {
    //                         this._OrderDetail.CreatedByDisplayNameShort = this._OrderDetail.CreatedByDisplayName.substring(0, 8) + '..';
    //                     }
    //                     else {
    //                         this._OrderDetail.CreatedByDisplayNameShort = this._OrderDetail.CreatedByDisplayName;
    //                     }
    //                 }
    //                 if (this._OrderDetail.ModifyByDisplayName != undefined) {
    //                     if (this._OrderDetail.ModifyByDisplayName.length > 8) { this._OrderDetail.ModifyByDisplayNameShort = this._OrderDetail.ModifyByDisplayName.substring(0, 8) + '..' } else {
    //                         this._OrderDetail.ModifyByDisplayNameShort = this._OrderDetail.ModifyByDisplayName;
    //                     };
    //                 }
    //                 this._HelperService.IsFormProcessing = false;
    //                 this._HelperService.OpenModal('CatForm_Edit_Content');
    //             } else {
    //                 this._HelperService.IsFormProcessing = false;
    //                 this._HelperService.NotifyError(_Response.Message);
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.HandleException(_Error);
    //         }
    //     );
    // }
   public Trackshipment_Details:any;
    public TrackShipments() {
        this._HelperService.AppConfig.ShowHeader = true;
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: "trackorder",
            ReferenceID:this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                   this.Trackshipment_Details=_Response.Result.events;
                    this._HelperService.IsFormProcessing = false;

                } else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }


    public Setup_UserOrderDetails() {
        this._HelperService.AppConfig.ShowHeader = true;
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: "getshipment",
            ReferenceID:this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._OrderDetail = _Response.Result;
                    this._OrderDetail.CreateDateS = this._HelperService.GetDateTimeS(this._OrderDetail.CreateDate);
                    this._OrderDetail.ModifyDateS = this._HelperService.GetDateTimeS(this._OrderDetail.ModifyDate);
                    // this._OrderDetail.CreateDate = this._HelperService.GetDateS(this._OrderDetail.CreateDate);
                    this._OrderDetail.ExpectedDeliveryDateS = this._HelperService.GetDateTimeS(this._OrderDetail.ExpectedDeliveryDate);
                    this._OrderDetail.ActualDeliveryDateS = this._HelperService.GetDateTimeS(this._OrderDetail.ActualDeliveryDate);
                    this._OrderDetail.StatusI = this._HelperService.GetStatusIcon(this._OrderDetail.StatusCode);
                    this._OrderDetail.StatusB = this._HelperService.GetStatusBadge(this._OrderDetail.StatusCode);
                    this._OrderDetail.StatusC = this._HelperService.GetStatusColor(this._OrderDetail.StatusCode);
                    this.origin = { lat: this._OrderDetail.DealerLocationLatitude, lng: this._OrderDetail.DealerLocationLongitude };
                   // this.destination = { lat: this._OrderDetail.ShippingAddress.Latitude, lng: this._OrderDetail.ShippingAddress.Longitude };
                   // this.MapUrl = "https://www.google.com/maps/dir/" + this.origin.lat + "," + this.origin.lng + "/" + this.destination.lat + "," + this.destination.lng;

                    this.selectedStatusItem = _Response.Result;
                    // for (let index = 0; index < this._OrderDetail.OrderActivity.length; index++) {
                    //     const element = this._OrderDetail.OrderActivity[index];
                    //     // this._OrderDetail.OrderActivity[index].CreateDate = this._HelperService.GetDateTimeS(this._OrderDetail.OrderActivity[index].CreateDate);
                    //     // this._OrderDetail.OrderActivity[index].CreateDate = this._OrderDetail.OrderCreateDate ;
                    // }
                    // this.findCurrentIndex(this._OrderDetail.StatusCode)
                    // this._ShippingAddress = this._OrderDetail.ShippingAddress;
                    // this._BillingAddress = this._OrderDetail._BillingAddress;


                    // for (let index = 0; index < this._OrderDetail.OrderActivity.length; index++) {
                    //     const element = this._OrderDetail.OrderActivity[index];
                    //     this._OrderDetail.OrderActivity[index].CreateDate = this._HelperService.GetDateTimeS(this._OrderDetail.OrderActivity[index].CreateDate);
                    //     // this._OrderDetail.OrderActivity[index].CreateDate = this._OrderDetail.OrderCreateDate ;
                    // }

                    // if (this._OrderDetail.CreatedByDisplayName != undefined) {
                    //     if (this._OrderDetail.CreatedByDisplayName.length > 8) {
                    //         this._OrderDetail.CreatedByDisplayNameShort = this._OrderDetail.CreatedByDisplayName.substring(0, 8) + '..';
                    //     }
                    //     else {
                    //         this._OrderDetail.CreatedByDisplayNameShort = this._OrderDetail.CreatedByDisplayName;
                    //     }
                    // }
                    // if (this._OrderDetail.ModifyByDisplayName != undefined) {
                    //     if (this._OrderDetail.ModifyByDisplayName.length > 8) { this._OrderDetail.ModifyByDisplayNameShort = this._OrderDetail.ModifyByDisplayName.substring(0, 8) + '..' } else {
                    //         this._OrderDetail.ModifyByDisplayNameShort = this._OrderDetail.ModifyByDisplayName;
                    //     };
                    // }
                    this._HelperService.IsFormProcessing = false;

                } else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    // findCurrentIndex(name) {
    //     for (let index = 0; index < this.StatusUpdate.length; index++) {
    //         const element = this.StatusUpdate[index];

    //         if (name == element.code) {
    //             this.FIndStatusItem = index

    //         }

    //     }


    // }

    public OrderItemList_Config: OList;
    InvoiceList_Setup() {
        this.OrderItemList_Config = {
            Id:null,
            Sort:null,
            Task: this._HelperService.AppConfig.Api.Core.GetUserInvoices,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
            Title: "Order Details",
            StatusType: "invoice",
            PageRecordLimit: 5,
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'UserAccountKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveOwnerKey, '='),
            Type: this._HelperService.AppConfig.ListType.All,
            DefaultSortExpression: 'CreateDate desc',
            TableFields: [
                {
                    DisplayName: '#REF',
                    SystemName: 'ReferenceId',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'User',
                    SystemName: 'UserAccountDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Invoice',
                    SystemName: 'Name',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },


                {
                    DisplayName: 'Amount',
                    SystemName: 'TotalAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Class: 'text-right',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Invoice Date',
                    SystemName: 'InvoiceDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    IsDateSearchField: true,
                },

                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: 'CreateDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Status',
                    SystemName: 'StatusName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: false,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
            ]
        };
        this.OrderItemList_Config = this._DataHelperService.List_Initialize(this.OrderItemList_Config);
        this.InvoiceList_GetData();
    }
    InvoiceList_ToggleOption(event: any, Type: any) {
        this.OrderItemList_Config = this._DataHelperService.List_Operations(
            this.OrderItemList_Config,
            event,
            Type
        );
        if (this.OrderItemList_Config.RefreshData == true) {
            this.InvoiceList_GetData();
        }
    }
    InvoiceList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(
            this.OrderItemList_Config
        );
        this.OrderItemList_Config = TConfig;
    }
    InvoiceList_RowSelected(ReferenceData) {
        this._TUOrder = ReferenceData;
        this._HelperService.OpenModal('CatForm_Edit_Content');

    }

    public InvoiceList_Filter_TransactionType_Option: Select2Options;
    public InvoiceList_Filter_TransactionType_Selected = 0;
    InvoiceList_Filter_TransactionTypes_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "SystemName",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.InvoiceType
                }
            ]
        };

        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.InvoiceList_Filter_TransactionType_Option = {
            placeholder: 'Search By Type',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    InvoiceList_Filter_TransactionTypes_Change(event: any) {
        if (event.value == this.InvoiceList_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_TransactionType_Selected, '=');
            this.OrderItemList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.OrderItemList_Config.SearchBaseConditions);
            this.InvoiceList_Filter_TransactionType_Selected = 0;
        }
        else if (event.value != this.InvoiceList_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_TransactionType_Selected, '=');
            this.OrderItemList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.OrderItemList_Config.SearchBaseConditions);
            this.InvoiceList_Filter_TransactionType_Selected = event.value;
            this.OrderItemList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_TransactionType_Selected, '='));
        }
        this.InvoiceList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    UpdateStock_Open() {
        this._HelperService.OpenModal('Form_UpdateStock');
    }

    Cancel_Order() {
        this._HelperService.OpenModal('Cancel_Order');
    }
    OpenConfimationModal(Orderdata) {
        this._HelperService.OpenModal('confirmModal');
    }

    storeVariable(TItem, i) {

        this.selectedStatusItem.statusCode = TItem.code;
        if (i == 5) {
            this.commnetvar = true
        }
        else {
            this.commnetvar = false

        }
    }
    StatusCodes(){
       
        let StatusCode1=''
        if(this._OrderDetail.StatusName == 'New'){
            StatusCode1 = "orderstatus.preparing"
        }else{
          StatusCode1 = "orderstatus.readytopickup"
        }
        return StatusCode1
    }
    UpdateStatus(): void {

        this._HelperService.IsFormProcessing = true;
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Product.confirmOrder,
            ReferenceId: this._OrderDetail.ReferenceId,
            ReferenceKey: this._OrderDetail.ReferenceKey,
            StatusCode: this.StatusCodes(),

        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess("Order Placed successfully");
                    //this.Setup_UserOrderDetails();
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.CloseModal('confirmModal')
                    this.returnOrders()
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                    this._HelperService.CloseModal('confirmModal')
                }
            }
            ,
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                this._HelperService.ToggleField = false;
            });
    }

    returnOrders(){
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Delivery.OngoingOrders.NewOrders]);
    }

}

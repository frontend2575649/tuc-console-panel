import { ChangeDetectorRef, Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { BaseChartDirective, Label } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OResponse } from '../../../../service/service';
declare var moment: any;
@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUDashboardComponent implements OnInit {

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef

  ) {
  }

  public BarChartOptions: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      },
    },
    emptyOverlay: {
      fillStyle: 'rgba(255,0,0,0.4)',
      fontColor: 'rgba(255,255,255,1.0)',
      fontStrokeWidth: 0,
      enabled: true
    }
  }
  public barChartLabels: Label[] = [];
  public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#F10875', '#F10875', '#F10875', '#F10875', '#F10875', '#F10875', '#F10875'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Active' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Idle' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Dead' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Inactive' }
  ];

  ngOnInit() {
    this.StartDate = moment().startOf('day');
    this.EndDate = moment().startOf('day');
    this.MonthStartTime = moment().startOf('month').startOf('day');
    this.MonthEndTime = moment().startOf('day');
    this._HelperService.DateRangeStart = moment(this.MonthStartTime);
    this._HelperService.DateRangeEnd = moment(this.MonthEndTime);
    this._HelperService.DateRangeStartO = moment(this.MonthStartTime);
    this._HelperService.DateRangeEndO = moment(this.MonthEndTime);
    Feather.replace();
    this.GetCustomerOverviewLite();
  }

  //#region AccountOverview 
  StartDate = null;
  EndDate = null;
  TodayStartTime = null;
  TodayEndTime = null;
  public MonthStartTime = null;
  public MonthEndTime = null;
  GetCustomerOverviewLite() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getloyaltyoverview',
      StartDate: this._HelperService.DateRangeStart, // new Date(2017, 0, 1, 0, 0, 0, 0),
      EndDate: this._HelperService.DateRangeEnd, // moment().add(2, 'days'),
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      CustomerReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      CustomerReferenceIdKey: this._HelperService.AppConfig.ActiveReferenceKey

    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountCustomerOverview = _Response.Result as OAccountCustomerOverview;
          this._AccountCustomerOverview.AvgSpentVisit = this._AccountCustomerOverview.Transaction / this._AccountCustomerOverview.TransactionInvoiceAmount


          this.components.forEach(a => {
            try {
              if (a.chart) a.chart.update();
            } catch (error) {

            }
          });

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public _AccountCustomerOverview: OAccountCustomerOverview =
    {

      RewardAmount: 0,
      RedeemAmount: 0,
      NewCustomers: 0,
      RepeatingCustomers: 0,
      TransactionInvoiceAmount: 0,
      Transaction: 0,
      RedeemTransaction: 0,
      RedeemInvoiceAmount: 0,
      AvgSpentVisit: 0,
      AvgVisitCustomer: 0,
      VisitsByRepeatingCustomers: 0,
      TotalCustomer: 0,
      RewardInvoiceAmount: 0


    }

}
export class OAccountCustomerOverview {
  public RewardAmount: any;
  public RewardInvoiceAmount: any;

  public RedeemAmount: any;
  public NewCustomers: any;
  public RepeatingCustomers: any;
  public TransactionInvoiceAmount: any;
  public Transaction: any;
  public RedeemTransaction: any;
  public RedeemInvoiceAmount: any;
  public AvgSpentVisit: any;
  public AvgVisitCustomer: any;
  public VisitsByRepeatingCustomers: any;
  public TotalCustomer: any;

}
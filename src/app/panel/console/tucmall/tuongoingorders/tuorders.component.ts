import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of, Subscription } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
declare var $: any;
import * as Feather from 'feather-icons';

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
  OInvoiceDetails,
} from "../../../../service/service";
import swal from 'sweetalert2';
import { Options, LabelType, ChangeContext } from 'ng5-slider';
import moment from 'moment';
@Component({
  selector: 'tu-tuorders',
  templateUrl: './tuorders.component.html',
})
export class TUOrdersComponent implements OnInit {


  public ResetFilterControls: boolean = true;

  public InvoiceKey = null;
  public StoreName = null;
  public _OrderDetail: any = {};
  public _TUOrder: any = null;
  public ListType: number;

  @ViewChild("offCanvas")
  divView: ElementRef;


  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService

  ) {
  }

  loadData() {
    Feather.replace();
    this.ListType = 1;
    this.Filter_Stores_Load();
    this.TUOrdersList_Setup();
    this.GetSalesOverview();
    this._HelperService.Icon_Crop_Clear();
    this.TUTr_Filter_Merchants_Load();
  }
  bill;
  getShipmentDetails(data) {
    this.bill = `${data.Bill}#view=fitH`;
    this.clicked()
  }


  ngOnInit() {
    this.loadData();
  }
  public SearchBaseCondition = ""
  public LatestOrder: any = "asc";
  public OldestOrder: any = "desc"
  //Stores Filter

  public Filter_Store_Option: Select2Options;
  public Filter_Store_Selected = 0;
  Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }

      ]
    };

    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.Filter_Store_Option = {
      placeholder: 'Filter by Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }



  Filter_Stores_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TUOrdersList_Config,
      this._HelperService.AppConfig.OtherFilters.MerchantSales.Store
    );
    this.StoresEventProcessing(event);
  }

  StoresEventProcessing(event: any): void {
    if (event.value == this.Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreId', this._HelperService.AppConfig.DataType.Number, this.Filter_Store_Selected, '=');
      this.TUOrdersList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUOrdersList_Config.SearchBaseConditions);
      this.Filter_Store_Selected = 0;
    }
    else if (event.value != this.Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreId', this._HelperService.AppConfig.DataType.Number, this.Filter_Store_Selected, '=');
      this.TUOrdersList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUOrdersList_Config.SearchBaseConditions);
      this.Filter_Store_Selected = event.value;
      this.TUOrdersList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'StoreId', this._HelperService.AppConfig.DataType.Number, this.Filter_Store_Selected, '='));
    }
    this.TUOrdersList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset, "");
    this._HelperService.ToggleField = true;;
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 500);
  }

  public TUTr_Filter_Merchant_Option: Select2Options;
  public TUTr_Filter_Merchant_Selected = 0;
  TUTr_Filter_Merchants_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetDealMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "StatusCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          // Id: false,
          Text: false,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.Status.Active,
        },

      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Merchant_Option = {
      placeholder: 'Search By Merchant',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }

  TUTr_Filter_Merchants_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TUOrdersList_Config,
      this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant
    );
    this.MerchantEventProcessing(event);
  }

  MerchantEventProcessing(event: any): void {
    if (event.value == this.TUTr_Filter_Merchant_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
      this.TUOrdersList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUOrdersList_Config.SearchBaseConditions);
      this.TUTr_Filter_Merchant_Selected = 0;
    }
    else if (event.value != this.TUTr_Filter_Merchant_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
      this.TUOrdersList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUOrdersList_Config.SearchBaseConditions);
      this.TUTr_Filter_Merchant_Selected = event.value;
      this.TUOrdersList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '='));
    }
    this.TUOrdersList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset,null);
  }

  public _GetoverviewSummary: any = {};

  private pData = {
    Task: 'getordersovrview',
    //   AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    //   AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
  };


  GetSalesOverview() {

    this._HelperService.IsFormProcessing = true;


    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetoverviewSummary = _Response.Result as any;
          this._ChangeDetectorRef.detectChanges();
          return;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  // new table start
  public TUOrdersList_Config: OList;

  TUOrdersList_Setup() {
    this.TUOrdersList_Config =
    {
      Task: "getshipments",
      Id: null,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment,
      PageRecordLimit: 10,
      Title: 'Orders List',
      StatusType: 'orderstatus',
      // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      // ReferenceId: this._HelperService.UserAccount.AccountId,
      // ReferenceKey: this._HelperService.UserAccount.AccountKey,
      // Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
      // Type: this._HelperService.AppConfig.ListType.All,
      DefaultSortExpression: 'CreateDate desc',
      //SearchBaseCondition: "",
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'CreateDate',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },

      TableFields: this.TUOrdersList_Setup_Fields(),
    }

    this.TUOrdersList_Config.ListType = this.ListType;
    this.TUOrdersList_Config.SearchBaseCondition = "";
    // this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict("", "MerchantId", this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, "=");
    if (this.TUOrdersList_Config.ListType == 1) // New
    {
      this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrictFromArray(this.TUOrdersList_Config.SearchBaseCondition, "StatusCode", this._HelperService.AppConfig.DataType.Text,
        ['orderstatus.new', 'orderstatus.pendingconfirmation'], "==");
    }
    else if (this.TUOrdersList_Config.ListType == 2) // preparing
    {
      this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusCode", this._HelperService.AppConfig.DataType.Text, 'orderstatus.preparing', "==");
    }
    else if (this.TUOrdersList_Config.ListType == 3) // readytopickup
    {
      this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrictFromArray(this.TUOrdersList_Config.SearchBaseCondition, "StatusCode", this._HelperService.AppConfig.DataType.Text,
        ['orderstatus.ready', 'orderstatus.readytopickup'], "==");
    }
    else if (this.TUOrdersList_Config.ListType == 4) {  // Dispatch
      this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrictFromArray(this.TUOrdersList_Config.SearchBaseCondition, "StatusCode", this._HelperService.AppConfig.DataType.Text,
        ['orderstatus.departedfromfacility', 'orderstatus.arrivedatfacility', 'orderstatus.departedtofacility', 'orderstatus.procvessedatfacility', 'orderstatus.pickuped', 'orderstatus.delivered', 'orderstatus.outfordelivery'], "==");
    }
    else if (this.TUOrdersList_Config.ListType == 5) {  // deliveryfailed
      this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusCode", this._HelperService.AppConfig.DataType.Text, 'orderstatus.deliveryfailed', "==");
    }
    else if (this.TUOrdersList_Config.ListType == 6) {  // Cancelled
      this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrictFromArray(this.TUOrdersList_Config.SearchBaseCondition, "StatusCode", this._HelperService.AppConfig.DataType.Text,
        ['orderstatus.cancelledbyuser', 'orderstatus.cancelledbyseller', 'orderstatus.cancelledbysystem'], "==");
    }
    else {
      this.TUOrdersList_Config.DefaultSortExpression = 'CreateDate desc';
    }

    this.TUOrdersList_Config = this._DataHelperService.List_Initialize(this.TUOrdersList_Config);

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.GetShipment,
      this.TUOrdersList_Config
    );
    // this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "DealerKey", 'text', this._HelperService.AppConfig.ActiveOwnerKey, "=");
    this.TUOrdersList_GetData();
  }



  TUOrderList_ListTypeChange(Type) {
    this.ListType = Type;
    this.TUOrdersList_Setup();
  }
  TUOrdersList_Setup_Fields() {
    var TableFields = [];
    TableFields = [
      {
        DisplayName: 'Order ID',
        SystemName: 'ReferenceId',
        DataType: this._HelperService.AppConfig.DataType.Number,
        Show: true,
        Search: true,
        Sort: false,
      },
      {
        DisplayName: 'Merchant',
        SystemName: 'MerchantDisplayName',
        DataType: this._HelperService.AppConfig.DataType.Number,
        Show: true,
        Search: false,
        Sort: false,
      },
      {
        DisplayName: 'Placed On',
        SystemName: 'CreateDate',
        DataType: this._HelperService.AppConfig.DataType.Date,
        Show: true,
        Search: false,
        Sort: true,
        IsDateSearchField: true,
      },
      {
        DisplayName: 'Delivered',
        SystemName: 'DeliveryDate',
        DataType: this._HelperService.AppConfig.DataType.Date,
        Show: true,
        Search: false,
        Sort: false,
        // IsDateSearchField: true,
      },
      {
        DisplayName: 'Delivered in',
        SystemName: 'hoursDiff',
        DataType: this._HelperService.AppConfig.DataType.Number,
        Show: true,
        Search: false,
        Sort: false,
      },
      {
        DisplayName: 'Carrier',
        SystemName: 'CarrierName',
        DataType: this._HelperService.AppConfig.DataType.Text,
        Show: true,
        Search: false,
        Sort: false,
      },
      {
        DisplayName: 'Invoice Number',
        SystemName: 'InvoiceNumber',
        DataType: this._HelperService.AppConfig.DataType.Text,
        Show: true,
        Search: false,
        Sort: false,
      },

      {
        DisplayName: 'Customer',
        SystemName: 'CustomerDisplayName',
        DataType: this._HelperService.AppConfig.DataType.Text,
        Show: true,
        Search: true,
        Sort: false,
      },
      {
        DisplayName: 'Delivery Charge',
        SystemName: 'RateAmount',
        DataType: this._HelperService.AppConfig.DataType.Decimal,
        Show: true,
        Search: false,
        Sort: false,
      },
      {
        DisplayName: 'Total',
        SystemName: 'TotalAmount',
        DataType: this._HelperService.AppConfig.DataType.Decimal,
        Show: true,
        Search: false,
        Sort: false,
      },
      {
        DisplayName: 'Deal',
        SystemName: 'DealTitle',
        DataType: this._HelperService.AppConfig.DataType.Text,
        Show: false,
        Search: true,
        Sort: false,
      },
      
    ]
    return TableFields;
  }

  TUTr_ToggleOption(event, type) {
    if (type === 'limit') {
      this.TUOrdersList_Config.PageRecordLimit = event;
    }
    if (type === 'page') {
      this.TUOrdersList_Config.ActivePage = event;
    }
    this.TUOrdersList_GetData();
  }


  TUOrdersList_GetData() {
    //this.GetOverviews(this.TUOrdersList_Config, "getordersovrview");
    var TConfig = this._DataHelperService.List_GetData(this.TUOrdersList_Config);
    this.TUOrdersList_Config = TConfig;

  }

  togglebutton: boolean = false
  TUOrdersList_ToggleOption(event: any, Type: any, sortorder: any) {
    if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
      event.data = {
        SalesMin: this.TUTr_OrderMinAmount,
        SalesMax: this.TUTr_OrderMaxAmount
      }
    }
    if (Type == 'date') {
      event.start = this._HelperService.DateInUTC(event.start);
      event.end = this._HelperService.DateInUTC(event.end);
    }
    this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUOrdersList_Config);
    this.TUOrdersList_Config = this._DataHelperService.List_Operations(this.TUOrdersList_Config, event, Type);
    this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);
    this.TodayStartTime = this.TUOrdersList_Config.StartTime;
    this.TodayEndTime = this.TUOrdersList_Config.EndTime;
    if (Type == 'sort') {
      if (sortorder == "asc") {
        // this.TUOrdersList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.SortOrder, "")
        this.togglebutton = !this.togglebutton;

      } else if (sortorder == "desc") {
        // this.TUOrdersList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.SortOrder, "");
        this.togglebutton = !this.togglebutton;

      }
    }
    if (
      (this.TUOrdersList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TUOrdersList_GetData();
    } else if (Type == "sort") {
      this.ApplyFilters(null, this._HelperService.AppConfig.ListToggleOption.ApplyFilter, 'Sort')
    }
  }

  timeout = null
  TUOrdersList_ToggleOptionSearch(event: any, Type: any) {
    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.TUOrdersList_Config.Sort.SortOptions.length; index++) {
          const element = this.TUOrdersList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUOrdersList_Config);
      this.TUOrdersList_Config = this._DataHelperService.List_Operations(this.TUOrdersList_Config, event, Type);
      if (
        (this.TUOrdersList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.TUOrdersList_GetData();
      }
    }, 2000);
  }

  sort(sortOrder) {
    if (sortOrder == "asc") {
      this.togglebutton = !this.togglebutton;

    } else if (sortOrder == "desc") {
      this.togglebutton = !this.togglebutton;

    }
    this.TUOrdersList_Config.Sort.SortColumn = 'CreateDate';
    this.TUOrdersList_Config.Sort.SortOrder = sortOrder;

    this.TUOrdersList_Config.DefaultSortExpression = `CreateDate ${sortOrder} `;
    this.TUOrdersList_GetData();
  }

  SetSearchRanges(): void {
    //#region Invoice 
    this.TUOrdersList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalAmount', this.TUOrdersList_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalAmount', this.TUTr_OrderMinAmount, this.TUTr_OrderMaxAmount);
    if (this.TUTr_OrderMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_OrderMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
      this.TUOrdersList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUOrdersList_Config.SearchBaseConditions);
    }
    else {
      this.TUOrdersList_Config.SearchBaseConditions.push(SearchCase);
    }
    //#endregion      
  }


  TUTr_OrderMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
  TUTr_OrderMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
  SetSalesRanges(): void {
    this.TUTr_OrderMinAmount = this.TUOrdersList_Config.SalesRange.SalesMin;
    this.TUTr_OrderMaxAmount = this.TUOrdersList_Config.SalesRange.SalesMax;
  }
  SetOtherFilters(): void {
    this.TUOrdersList_Config.SearchBaseConditions = [];
    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Store));
    if (CurrentIndex != -1) {
      this.Filter_Store_Selected = 0;
      this.StoresEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }

  }

  //#region stores 

  




  public SelectedProductItem =
    {
      ReferenceId: null,
      ReferenceKey: null,
      AccountId: null,
      AccountKey: null,
      AccountDisplayName: null,
      CategoryKey: null,
      CategoryName: null,
      Name: null,
      SystemName: null,
      Description: null,
      IconUrl: null,
      PosterUrl: null,
      MinimumAmount: null,
      MaximumAmount: null,
      ActualPrice: null,
      SellingPrice: null,
      AllowMultiple: null,
      RewardPercentage: null,
      MaximumRewardAmount: null,
      TotalStock: null,
      CreateDate: null,
      CreatedByDisplayName: null,
      CreatedByKey: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
    };
  TUOrdersList_RowSelected(ReferenceData) {
    var ReferenceKey = ReferenceData.ReferenceKey;
    this.SelectedProductItem = ReferenceData;
    //this._HelperService.OpenModal('ModalCreditPoints');
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Product.OrderDetail + "/" + ReferenceData.ReferenceKey + "/" + ReferenceData.ReferenceId]);
    // this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;

  }
  //new table end


  OpenAcceptOrder(order: any): void {
    this.Setup_UserOrderDetails(order);
  }

  F_AddProduct_Show() {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.AddProduct]);
  }

  CatForm_Edit_Show(TItem) {
    this.Setup_UserOrderDetails(TItem);
  }

  // public OrderItemList_Config: OList;
  // InvoiceList_Setup() {
  //     this.OrderItemList_Config = {
  //         Id: null,
  //         Sort: null,
  //         Task: this._HelperService.AppConfig.Api.Core.GetUserInvoices,
  //         Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
  //         Title: "Order Details",
  //         StatusType: "invoice",
  //         PageRecordLimit: 5,
  //         SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'UserAccountKey', this._HelperService.AppConfig.DataType.Text, "8bab652699b74c2c9caad9f1baf66cfb", '='),
  //         Type: this._HelperService.AppConfig.ListType.All,
  //         DefaultSortExpression: 'CreateDate desc',
  //         TableFields: [
  //             {
  //                 DisplayName: '#REF',
  //                 SystemName: 'ReferenceId',
  //                 DataType: this._HelperService.AppConfig.DataType.Number,
  //                 Class: '',
  //                 Show: true,
  //                 Search: true,
  //                 Sort: true,
  //                 ResourceId: null,
  //             },
  //             {
  //                 DisplayName: 'User',
  //                 SystemName: 'UserAccountDisplayName',
  //                 DataType: this._HelperService.AppConfig.DataType.Text,
  //                 Class: '',
  //                 Show: true,
  //                 Search: true,
  //                 Sort: true,
  //                 ResourceId: null,
  //             },
  //             {
  //                 DisplayName: 'Invoice',
  //                 SystemName: 'Name',
  //                 DataType: this._HelperService.AppConfig.DataType.Text,
  //                 Class: '',
  //                 Show: true,
  //                 Search: true,
  //                 Sort: true,
  //                 ResourceId: null,
  //             },


  //             {
  //                 DisplayName: 'Amount',
  //                 SystemName: 'TotalAmount',
  //                 DataType: this._HelperService.AppConfig.DataType.Decimal,
  //                 Class: 'text-right',
  //                 Show: true,
  //                 Search: false,
  //                 Sort: true,
  //                 ResourceId: null,
  //             },
  //             {
  //                 DisplayName: 'Invoice Date',
  //                 SystemName: 'InvoiceDate',
  //                 DataType: this._HelperService.AppConfig.DataType.Date,
  //                 Class: 'td-date',
  //                 Show: true,
  //                 Search: false,
  //                 Sort: true,
  //                 ResourceId: null,
  //                 IsDateSearchField: true,
  //             },

  //             {
  //                 DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
  //                 SystemName: 'CreateDate',
  //                 DataType: this._HelperService.AppConfig.DataType.Date,
  //                 Class: 'td-date',
  //                 Show: true,
  //                 Search: false,
  //                 Sort: true,
  //                 ResourceId: null,
  //             },
  //             {
  //                 DisplayName: 'Status',
  //                 SystemName: 'StatusName',
  //                 DataType: this._HelperService.AppConfig.DataType.Text,
  //                 Show: false,
  //                 Search: false,
  //                 Sort: true,
  //                 ResourceId: null,
  //             },
  //         ]
  //     };
  //     this.OrderItemList_Config = this._DataHelperService.List_Initialize(
  //         this.OrderItemList_Config
  //     );
  //     this.InvoiceList_GetData();
  // }
  // InvoiceList_ToggleOption(event: any, Type: any) {
  //     this.OrderItemList_Config = this._DataHelperService.List_Operations(
  //         this.OrderItemList_Config,
  //         event,
  //         Type
  //     );
  //     if (this.OrderItemList_Config.RefreshData == true) {
  //         this.InvoiceList_GetData();
  //     }
  // }
  // InvoiceList_GetData() {
  //     var TConfig = this._DataHelperService.List_GetData(
  //         this.OrderItemList_Config
  //     );
  //     this.OrderItemList_Config = TConfig;
  // }
  // InvoiceList_RowSelected(ReferenceData) {
  //     this._TUOrder = ReferenceData;
  //     this._HelperService.OpenModal('CatForm_Edit_Content');

  // }

  // public InvoiceList_Filter_TransactionType_Option: Select2Options;
  // public InvoiceList_Filter_TransactionType_Selected = 0;
  // InvoiceList_Filter_TransactionTypes_Load() {
  //     var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
  //     var _Select: OSelect = {
  //         Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
  //         Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
  //         SearchCondition: "",
  //         SortCondition: [],
  //         Fields: [
  //             {
  //                 SystemName: "SystemName",
  //                 Type: this._HelperService.AppConfig.DataType.Number,
  //                 Id: true,
  //                 Text: false,
  //             },
  //             {
  //                 SystemName: "Name",
  //                 Type: this._HelperService.AppConfig.DataType.Text,
  //                 Id: false,
  //                 Text: true
  //             },
  //             {
  //                 SystemName: "ParentCode",
  //                 Type: this._HelperService.AppConfig.DataType.Text,
  //                 SearchCondition: "=",
  //                 SearchValue: this._HelperService.AppConfig.HelperTypes.InvoiceType
  //             }
  //         ]
  //     };

  //     var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
  //     this.InvoiceList_Filter_TransactionType_Option = {
  //         placeholder: 'Search By Type',
  //         ajax: _Transport,
  //         multiple: false,
  //         allowClear: true,
  //     };
  // }
  // InvoiceList_Filter_TransactionTypes_Change(event: any) {
  //     if (event.value == this.InvoiceList_Filter_TransactionType_Selected) {
  //         var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_TransactionType_Selected, '=');
  //         this.OrderItemList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.OrderItemList_Config.SearchBaseConditions);
  //         this.InvoiceList_Filter_TransactionType_Selected = 0;
  //     }
  //     else if (event.value != this.InvoiceList_Filter_TransactionType_Selected) {
  //         var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_TransactionType_Selected, '=');
  //         this.OrderItemList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.OrderItemList_Config.SearchBaseConditions);
  //         this.InvoiceList_Filter_TransactionType_Selected = event.value;
  //         this.OrderItemList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_TransactionType_Selected, '='));
  //     }
  //     this.InvoiceList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  // }

  // Get_Invoice() {
  //     var pData = {
  //         Task: this._HelperService.AppConfig.Api.Core.GetUserInvoice,
  //         Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this.InvoiceKey, '=')
  //     };
  //     let _OResponse: Observable<OResponse>;
  //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.ThankU, pData);
  //     _OResponse.subscribe(
  //         _Response => {
  //             if (_Response.Status == this._HelperService.StatusSuccess) {
  //                 this._OInvoiceDetails = null;
  //                 this._OInvoiceDetails = _Response.Result as OInvoiceDetails;
  //                 this._OInvoiceDetails.InvoiceDate = this._HelperService.GetDateS(this._OInvoiceDetails.InvoiceDate);
  //                 this._OInvoiceDetails.PaymentDate = this._HelperService.GetDateS(this._OInvoiceDetails.PaymentDate);
  //                 this._OInvoiceDetails.ModifyDateS = this._HelperService.GetDateTimeS(this._OInvoiceDetails.ModifyDate);
  //                 this._OInvoiceDetails.StatusI = this._HelperService.GetStatusIcon(this._OInvoiceDetails.Status);
  //                 this._OInvoiceDetails.StatusBadge = this._HelperService.GetStatusBadge(this._OInvoiceDetails.StatusCode);
  //                 // this._HelperService.OpenModal('ModalInvoiceDetails');
  //             }
  //             else {

  //                 this._HelperService.NotifyError(_Response.Message);
  //             }
  //         },
  //         _Error => {
  //             console.log(_Error);
  //         });
  // }

  // _OInvoiceDetails: OInvoiceDetails =
  //     {
  //         PaymentDateS: null,
  //         StatusBadge: null,
  //         PaymentApproverDisplayName: null,
  //         PaymentApproverKey: null,
  //         PaymentDate: null,
  //         PaymentModeCode: null,
  //         PaymentModeName: null,
  //         PaymentProofUrl: null,
  //         PaymentReference: null,

  //         Amount: null,
  //         Charge: null,
  //         ChargePercentage: null,
  //         ComissionAmount: null,
  //         ComissionPercentage: null,
  //         Comment: null,
  //         CreateDate: null,
  //         CreatedByDisplayName: null,
  //         CreatedByKey: null,
  //         Description: null,
  //         DiscountAmount: null,
  //         DiscountPercentage: null,
  //         EndDate: null,
  //         FromAddress: null,
  //         FromContactNumber: null,
  //         FromEmailAddress: null,
  //         FromFax: null,
  //         FromName: null,
  //         InvoiceDate: null,
  //         InvoiceDateS: null,
  //         InoviceNumber: null,
  //         ModifyByDisplayName: null,
  //         ModifyByKey: null,
  //         ModifyDate: null,
  //         ModifyDateS: null,
  //         Name: null,
  //         ParentKey: null,
  //         ParentName: null,
  //         ReferenceId: null,
  //         ReferenceKey: null,
  //         StartDate: null,
  //         Status: null,
  //         StatusCode: null,
  //         StatusI: null,
  //         StatusName: null,
  //         ToAddress: null,
  //         ToContactNumber: null,
  //         ToEmailAddress: null,
  //         ToFax: null,
  //         ToName: null,
  //         TotalAmount: null,
  //         TotalItem: null,
  //         TypeCode: null,
  //         TypeName: null,
  //         UnitCost: null,
  //         UserAccountDisplayName: null,
  //         UserAccountIconUrl: null,
  //         UserAccountId: null,
  //         UserAccountKey: null,
  //         UserAccountTypeCode: null,
  //         UserAccountTypeName: null,
  //         Items: [],
  //     };

  // UpdateStock_Open() {
  //     this._HelperService.OpenModal('Form_UpdateStock');
  // }

  // Cancel_Order() {
  //     this._HelperService.OpenModal('Cancel_Order');
  // }

  // TUOrderList_ListTypeChange(Type) {
  //     this.ListType = Type;
  //     this.TUOrdersList_Setup();
  // }

  // SetOtherFilters(): void {
  //     this.TUOrdersList_Config.SearchBaseConditions = [];
  //     this.TUOrdersList_Config.SearchBaseCondition = null;

  //     var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
  //     if (CurrentIndex != -1) {
  //         // this.TerminalsList_Filter_Owners_Selected = null;
  //         // this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
  //     }
  // }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.TUOrdersList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.TUOrdersList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.TUOrdersList_Config);
    this.SetOtherFilters();
    this.TUOrdersList_GetData();
  }




  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    if (this.TUTr_OrderMinAmount == null || this.TUTr_OrderMinAmount == undefined ||
      this.TUTr_OrderMaxAmount == null || this.TUTr_OrderMaxAmount == undefined) {
      this._HelperService.NotifyError("Value not be null or undefined");
    }
    else if (this.TUTr_OrderMinAmount > this.TUTr_OrderMaxAmount) {
      this._HelperService.NotifyError("Minimum  Amount should be less than Maximum  Amount");
    }
    else {
      this.SetSearchRanges();
      this._HelperService.MakeFilterSnapPermanent();
      this.TUOrdersList_GetData();
      if (ButtonType == 'Sort') {
        $("#TUTr_sdropdown").dropdown('toggle');
      } else if (ButtonType == 'Other') {
        $("#TUTr_fdropdown").dropdown('toggle');
      }

      this.ResetFilterUI(); 
      this._HelperService.StopClickPropogation();
    }
  }
  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.TUOrdersList_Config);
    this.SetOtherFilters();
    this.TUOrdersList_Setup();
    this.ResetFilterUI(); 
    this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  UpdateStatus(): void {

    this._HelperService.IsFormProcessing = true;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Product.updateorderstatus,
      ReferenceId: this._OrderDetail.ReferenceId,
      ReferenceKey: this._OrderDetail.ReferenceKey,
      StatusCode: this.selectedStatusItem.statusCode,
      Comment: this.Comment

    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Status Updated successfully");
          this.Setup_UserOrderDetails(this._OrderDetail);
          this._HelperService.IsFormProcessing = false;
          this._HelperService.CloseModal('exampleModal');
          this._HelperService.NotifySuccess(_Response.Message);
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      }
      ,
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;
      });
  }

  public OverviewData: any = {
    Total: 0,
    New: 0,
    PendingConfirmation: 0,
    Confirmed: 0,
    Preparing: 0,
    Ready: 0,
    ReadyToPickUp: 0,
    OutForDelivery: 0,
    DeliveryFailed: 0,
    Cancelled: 0,
    Delivered: 0
  };


  Data: any = {}
  //   GetOverviews(ListOptions: any, Task: string): any {
  //     this._HelperService.IsFormProcessing = true;
  //     ListOptions.SearchCondition = '';
  //     ListOptions = this._DataHelperService.List_GetSearchConditionTur(ListOptions);
  //     if (ListOptions.ActivePage == 1) {
  //         ListOptions.RefreshCount = true;
  //     }
  //     var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

  //     if (ListOptions.Sort.SortDefaultName) {
  //         ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
  //         ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
  //         ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
  //     }

  //     if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
  //         if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
  //             SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
  //         }
  //         else {
  //             SortExpression = ListOptions.Sort.SortColumn + ' desc';
  //         }
  //     }


  //     var pData = {
  //         Task: Task,
  //         TotalRecords: ListOptions.TotalRecords,
  //         Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
  //         Limit: ListOptions.PageRecordLimit,
  //         RefreshCount: ListOptions.RefreshCount,
  //         SearchCondition: ListOptions.SearchCondition,
  //         SortExpression: SortExpression,
  //         Type: ListOptions.Type,
  //         ReferenceKey: ListOptions.ReferenceKey,
  //         StartDate: ListOptions.StartDate,
  //         EndDate: ListOptions.EndDate,
  //         ReferenceId: ListOptions.ReferenceId,
  //         SubReferenceId: ListOptions.SubReferenceId,
  //         SubReferenceKey: ListOptions.SubReferenceKey,
  //         AccountId: ListOptions.AccountId,
  //         AccountKey: ListOptions.AccountKey,
  //         ListType: ListOptions.ListType,
  //         IsDownload: false,
  //     };

  //     let _OResponse: Observable<OResponse>;
  //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
  //     _OResponse.subscribe(
  //         _Response => {
  //             this._HelperService.IsFormProcessing = false;
  //             if (_Response.Status == this._HelperService.StatusSuccess) {
  //                 this.OverviewData = _Response.Result as any;
  //                 console.log(this.OverviewData);
  //             }
  //             else {
  //                 this._HelperService.NotifyError(_Response.Message);
  //             }
  //         },
  //         _Error => {
  //             this._HelperService.IsFormProcessing = false;
  //             this._HelperService.HandleException(_Error);

  //         });


  // }


  //#region Order Details 

  // findCurrentIndex(name) {
  //     for (let index = 0; index < this.StatusUpdate.length; index++) {
  //         const element = this.StatusUpdate[index];

  //         if (name == element.code) {
  //             this.FIndStatusItem = index

  //         }

  //     }


  // }


  public _ProductDetail: any = {};
  public origin: any;
  public destination: any;
  public selectedStatusItem: any = { index: 0 };
  public FIndStatusItem: any = { index: 0 };
  public commnetvar: boolean;
  public _ShippingAddress: any = {};
  public _BillingAddress: any = {};

  public MapUrl: any;
  public PrintInVoice: boolean = false;

  public Get_UserOrderDetails(TItem) {
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: "getorder",
      ReferenceId: TItem.ReferenceId,
      ReferenceKey: TItem.ReferenceKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this._OrderDetail = _Response.Result;
          this._OrderDetail.CreateDate = this._HelperService.GetDateS(this._OrderDetail.CreateDate);
          this._OrderDetail.CreateDateS = this._HelperService.GetDateTimeS(this._OrderDetail.CreateDate);
          this._OrderDetail.ModifyDateS = this._HelperService.GetDateTimeS(this._OrderDetail.ModifyDate);
          this._OrderDetail.StatusI = this._HelperService.GetStatusIcon(this._OrderDetail.StatusCode);
          this._OrderDetail.StatusB = this._HelperService.GetStatusBadge(this._OrderDetail.StatusCode);
          this._OrderDetail.StatusC = this._HelperService.GetStatusColor(this._OrderDetail.StatusCode);

          if (this._OrderDetail.CreatedByDisplayName != undefined) {
            if (this._OrderDetail.CreatedByDisplayName.length > 8) {
              this._OrderDetail.CreatedByDisplayNameShort = this._OrderDetail.CreatedByDisplayName.substring(0, 8) + '..';
            }
            else {
              this._OrderDetail.CreatedByDisplayNameShort = this._OrderDetail.CreatedByDisplayName;
            }
          }
          if (this._OrderDetail.ModifyByDisplayName != undefined) {
            if (this._OrderDetail.ModifyByDisplayName.length > 8) { this._OrderDetail.ModifyByDisplayNameShort = this._OrderDetail.ModifyByDisplayName.substring(0, 8) + '..' } else {
              this._OrderDetail.ModifyByDisplayNameShort = this._OrderDetail.ModifyByDisplayName;
            };
          }
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess(_Response.Message);

          this._HelperService.OpenModal('acceptOrder');
          // this._HelperService.OpenModal('CatForm_Edit_Content');
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


  public Setup_UserOrderDetails(TItem) {
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: "getorder",
      ReferenceId: TItem.ReferenceId,
      ReferenceKey: TItem.ReferenceKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this._OrderDetail = _Response.Result;
          this._OrderDetail.CreateDate = this._HelperService.GetDateS(this._OrderDetail.CreateDate);
          this._OrderDetail.CreateDateS = this._HelperService.GetDateTimeS(this._OrderDetail.CreateDate);
          this._OrderDetail.ModifyDateS = this._HelperService.GetDateTimeS(this._OrderDetail.ModifyDate);
          this._OrderDetail.ExpectedDeliveryDateS = this._HelperService.GetDateTimeS(this._OrderDetail.ExpectedDeliveryDate);
          this._OrderDetail.ActualDeliveryDateS = this._HelperService.GetDateTimeS(this._OrderDetail.ActualDeliveryDate);
          this._OrderDetail.StatusI = this._HelperService.GetStatusIcon(this._OrderDetail.StatusCode);
          this._OrderDetail.StatusB = this._HelperService.GetStatusBadge(this._OrderDetail.StatusCode);
          this._OrderDetail.StatusC = this._HelperService.GetStatusColor(this._OrderDetail.StatusCode);
          this.origin = { lat: this._OrderDetail.DealerLocationLatitude, lng: this._OrderDetail.DealerLocationLongitude };
          this.destination = { lat: this._OrderDetail.ShippingAddress.Latitude, lng: this._OrderDetail.ShippingAddress.Longitude };
          this.MapUrl = "https://www.google.com/maps/dir/" + this.origin.lat + "," + this.origin.lng + "/" + this.destination.lat + "," + this.destination.lng;

          this.selectedStatusItem = _Response.Result;
          for (let index = 0; index < this._OrderDetail.OrderActivity.length; index++) {
            const element = this._OrderDetail.OrderActivity[index];
            this._OrderDetail.OrderActivity[index].CreateDate = this._HelperService.GetDateTimeS(this._OrderDetail.OrderActivity[index].CreateDate);
            // this._OrderDetail.OrderActivity[index].CreateDate = this._OrderDetail.OrderCreateDate ;
          }
          // this.findCurrentIndex(this._OrderDetail.StatusCode)
          this._ShippingAddress = this._OrderDetail.ShippingAddress;
          this._BillingAddress = this._OrderDetail._BillingAddress;

          for (let index = 0; index < this._OrderDetail.OrderActivity.length; index++) {
            const element = this._OrderDetail.OrderActivity[index];
            this._OrderDetail.OrderActivity[index].CreateDate = this._HelperService.GetDateTimeS(this._OrderDetail.OrderActivity[index].CreateDate);
            // this._OrderDetail.OrderActivity[index].CreateDate = this._OrderDetail.OrderCreateDate ;
          }

          if (this._OrderDetail.CreatedByDisplayName != undefined) {
            if (this._OrderDetail.CreatedByDisplayName.length > 8) {
              this._OrderDetail.CreatedByDisplayNameShort = this._OrderDetail.CreatedByDisplayName.substring(0, 8) + '..';
            }
            else {
              this._OrderDetail.CreatedByDisplayNameShort = this._OrderDetail.CreatedByDisplayName;
            }
          }
          if (this._OrderDetail.ModifyByDisplayName != undefined) {
            if (this._OrderDetail.ModifyByDisplayName.length > 8) { this._OrderDetail.ModifyByDisplayNameShort = this._OrderDetail.ModifyByDisplayName.substring(0, 8) + '..' } else {
              this._OrderDetail.ModifyByDisplayNameShort = this._OrderDetail.ModifyByDisplayName;
            };
          }
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess(_Response.Message);

          this._HelperService.OpenModal('acceptOrder');

        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


  storeVariable(TItem, i) {
    this.selectedStatusItem.statusCode = TItem.code;
    if (i == 5) {
      this.commnetvar = true
    }
    else {
      this.commnetvar = false
    }
  }

  Comment: string;

  returnOrders() {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Product.Order]);
  }

  ViewDetail(ReferenceData) {
    var ReferenceKey = ReferenceData.ReferenceKey;
    this.SelectedProductItem = ReferenceData;
    //this._HelperService.OpenModal('ModalCreditPoints');
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Product.OrderDetail + "/" + ReferenceData.ReferenceKey + "/" + ReferenceData.ReferenceId]);
    // this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
  }

  dialogType;
  CancelDetail(order) { }

  //#endregion
  public ConfimationOrderDetails: any;
  OpenConfimation(Orderdata, type) {
    this.dialogType = type;
    this.ConfimationOrderDetails = Orderdata
    this._HelperService.OpenModal('Form_Confirmation');
  }

  TrackingData = [];
  OpenTrackDialog(Orderdata) {
    this.ConfimationOrderDetails = Orderdata;

    const pdata = {
      Task: "trackorder",
      ReferenceId: Orderdata.ReferenceId,
      ReferenceKey: Orderdata.ReferenceKey
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pdata);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this.TrackingData = _Response.Result.events;
          this._ChangeDetectorRef.detectChanges();
          this._HelperService.NotifySuccess(_Response.Message);
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
    this._HelperService.OpenModal('Form_Confirmation');
  }

  cancellationModal(Orderdata) {
    this.ConfimationOrderDetails = Orderdata

    this._HelperService.OpenModal('Form_Cancellation');
  }

  StatusCodes() {

    let StatusCode1 = ''
    if (this.ConfimationOrderDetails.StatusName == 'New' || this.ConfimationOrderDetails.StatusName == 'Pending Confirmation') {
      StatusCode1 = "orderstatus.confirmed"
    }
    // else {
    //   StatusCode1 = "orderstatus.readytopickup"
    // }
    return StatusCode1
  }
  ConfirmOrder() {
    var pData = {
      Task: 'confirmorder',
      ReferenceId: this.ConfimationOrderDetails.ReferenceId,
      ReferenceKey: this.ConfimationOrderDetails.ReferenceKey,
      StatusCode: this.StatusCodes(),

    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;

          if (pData.StatusCode === 'orderstatus.readytopickup') {
            this.TUOrdersList_Config.ListType = 2;
            this.Filter_Stores_Load();
            this.TUOrdersList_Setup();
            this.GetSalesOverview();
            this._HelperService.Icon_Crop_Clear();
            this._HelperService.NotifySuccess(_Response.Message);
          } else {
            this.loadData()
          }
          this._ChangeDetectorRef.detectChanges();
          //#endregion
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  ArrangeOrder() {
    var pData = {
      Task: 'arrangeshipment',
      ReferenceId: this.ConfimationOrderDetails.ReferenceId,
      ReferenceKey: this.ConfimationOrderDetails.ReferenceKey,
      //RateId: Orderdata.RateId,
      //RateAmount: Orderdata.RateAmount
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess("Order Arranged Successfully");
          this._ChangeDetectorRef.detectChanges();
          this._HelperService.NotifySuccess(_Response.Message);
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  // Cancell Order
  CancelOrder() {
    var pData = {
      Task: 'cancelshipment',
      ReferenceId: this.ConfimationOrderDetails.ReferenceId,
      ReferenceKey: this.ConfimationOrderDetails.ReferenceKey,
      StatusCode: "orderstatus.cancelledbysystem",

    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          // this.Filter_Stores_Load();
          this.loadData();
          this._ChangeDetectorRef.detectChanges();
          this._HelperService.NotifySuccess(_Response.Message)
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  // End

  // Mark As ready Order
  ReadyToPickUp() {
    var pData = {
      Task: 'updateorderstatus',
      ReferenceId: this.ConfimationOrderDetails.ReferenceId,
      ReferenceKey: this.ConfimationOrderDetails.ReferenceKey,
      StatusCode: "orderstatus.readytopickup",
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this.loadData();
          this._ChangeDetectorRef.detectChanges();
          this._HelperService.NotifySuccess(_Response.Message);
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  // End

  FormCancellation() {

    var pData = {
      Task: 'cancelshipment',
      ReferenceId: this.ConfimationOrderDetails.ReferenceId,
      ReferenceKey: this.ConfimationOrderDetails.ReferenceKey,
      //RateId: Orderdata.RateId,
      //RateAmount: Orderdata.RateAmount
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._ChangeDetectorRef.detectChanges();
          this._HelperService.NotifySuccess(_Response.Message);
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );

  }
  gotoInvoice(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveCustomer,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Customer,
      }
    );
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Product.InvoiceDetail,
      ReferenceData.ReferenceId,
      ReferenceData.ReferenceKey,
    ]);

    // this._Router.navigate(["/console/ongoingorders/invoice"])
  }

  RouteOrder(ReferenceData) {
    //#region Save Current Merchant To Storage 

    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveCustomer,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Customer,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    //   this._HelperService.AppConfig.ActiveOwnerId =
    //       ReferenceData.MerchantId;
    //   this._HelperService.AppConfig.ActiveOwnerKey= ReferenceData.MerchantKey;


    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Product.OrderDetail,
      ReferenceData.ReferenceId, ReferenceData.ReferenceKey,
      //   ReferenceData.MerchantId,
      //   ReferenceData.MerchantKey,
    ]);


  }


  clicked() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

  TodayStartTime = null;
  TodayEndTime = null;

}
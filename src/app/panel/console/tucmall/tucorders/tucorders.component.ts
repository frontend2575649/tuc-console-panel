import { ChangeDetectorRef, Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
import * as cloneDeep from 'lodash/cloneDeep';
declare var moment: any;
declare var $: any;
import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: "tu-tucorders",
  templateUrl: "./tucorders.component.html",
})
export class TUCOrdersComponent implements OnInit, OnDestroy {
  public ListType: number;

  DateRangeOptions: any;
  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
    this.DateRangeOptions = cloneDeep(this._HelperService.AppConfig.DateRangeOptions);
    this.DateRangeOptions.opens = "left";

  }

  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();
    this.ListType = 1;
    this.TUOrdersList_Setup();
    this.GetOverviews();
    this.TUOrdersList_Filter_Owners_Load();
    this.CategoriesList_Filter_Owners_Load();
    // this.TUOrdersList_Filter_Stores_Load();
    this.InitColConfig();
    // this._HelperService.StopClickPropogation();
    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.TUOrdersList_GetData();
    // });
  }


  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }
  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region TUOrdersList

  public TUOrdersList_Config: OList;
  TUOrdersList_Setup() {
    this.TUOrdersList_Config = {
      Id: null,
      Sort: null,
      Task: "getorders",
      Location: this._HelperService.AppConfig.NetworkLocation.V2.HCProduct,
      Title: 'Orders List',
      StatusType: 'orderstatus',
      DefaultSortExpression: "CreateDate desc",
      ReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      IsDownload: true,
      TableFields: [

        {
          DisplayName: 'Name',
          SystemName: 'CustomerDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: true,
          Sort: false,
        },
        {
          DisplayName: 'Agent DisplayName',
          SystemName: 'AgentDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: true,
          Sort: true,
        }, {
          DisplayName: 'Dealer LocationName',
          SystemName: 'DealerLocationName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: true,
          Sort: true,
        },
        {
          DisplayName: 'Order Id',
          SystemName: 'OrderId',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: true,
          Sort: false,
        },

        {
          DisplayName: 'Items',
          SystemName: 'TotalItem',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Show: true,
          Search: false,
          Sort: true,
        },
        {
          DisplayName: 'Amount',
          SystemName: 'TotalAmount',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Show: true,
          Search: false,
          Sort: true,
        },
        {
          DisplayName: 'Added On',
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Show: true,
          Search: true,
          Sort: true,
          IsDateSearchField: true,
        },
      ]

    };

    this.TUOrdersList_Config.ListType = this.ListType;
    this.TUOrdersList_Config.SearchBaseCondition = "";

    if (this.TUOrdersList_Config.ListType == 1) // Confirmed And preparing
    {
      this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number', 488, "=");
    }
    else if (this.TUOrdersList_Config.ListType == 2) // Ready To Pickup
    {
      this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number', 489, "=");
    }
    else if (this.TUOrdersList_Config.ListType == 3) // Out For Delivery
    {
      this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number', 491, "=");
    }
    else if (this.TUOrdersList_Config.ListType == 4) {  // Delivered Failed
      this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number', 493, "=");
    }
    else if (this.TUOrdersList_Config.ListType == 5) {  // Cancalled
      this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number', 494, "=");
    }
    else if (this.TUOrdersList_Config.ListType == 6) {  // Delivery Failed
      this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrictFromArray(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number', [], "=");
    }
    else {
      this.TUOrdersList_Config.DefaultSortExpression = 'CreateDate desc';
    }

    this.TUOrdersList_Config = this._DataHelperService.List_Initialize(
      this.TUOrdersList_Config
    );
    this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "DealerKey", 'text', this._HelperService.AppConfig.ActiveMerchantReferenceKey, "=");
    this.TUOrdersList_GetData();
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Cashier,
      this.TUOrdersList_Config
    );
    this.TUOrdersList_GetData();
  }
  TUOrdersList_ToggleOption(event: any, Type: any) {
    //   if (Type == "date") {
    //     this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
    //     this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
    // }
    if (Type == "date") {
      this.DateRangeOptions.startDate = event.start;
      this.DateRangeOptions.endDate = event.end;
    }
    this.DateRangeOptions
    if (event != null) {
      for (let index = 0; index < this.TUOrdersList_Config.Sort.SortOptions.length; index++) {
        const element = this.TUOrdersList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.TUOrdersList_Config


    );

    this.TUOrdersList_Config = this._DataHelperService.List_Operations(
      this.TUOrdersList_Config,
      event,
      Type
    );

    if (
      (this.TUOrdersList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TUOrdersList_GetData();
    }

  }
  TUOrdersList_GetData() {
    // this.GetOverviews(this.TUOrdersList_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getorderoverview);
    var TConfig = this._DataHelperService.List_GetData(
      this.TUOrdersList_Config
    );
    this.TUOrdersList_Config = TConfig;
  }
  TUOrdersList_RowSelected(ReferenceData) {
    //#region Save Current Merchant To Storage 

    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveMerchant,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveMerchantReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveMerchantReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.RewardPercentage,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);

    //#endregion
  }

  timeout = null;
  TUOrdersList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.TUOrdersList_Config.Sort.SortOptions.length; index++) {
          const element = this.TUOrdersList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.TUOrdersList_Config


      );

      this.TUOrdersList_Config = this._DataHelperService.List_Operations(
        this.TUOrdersList_Config,
        event,
        Type
      );

      if (
        (this.TUOrdersList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.TUOrdersList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);

  }





  TUOrdersList_ListTypeChange(Type) {
    this.ListType = Type;
    this.TUOrdersList_Setup();


  }
  //#endregion

  public TUOrdersList_Filter_Owners_Option: Select2Options;
  public TUOrdersList_Filter_Owners_Selected = 0;
  TUOrdersList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      SearchCondition: "",
      SortCondition: ["DisplayName asc"],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ]
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'MerchantReferenceId', this._HelperService.AppConfig.DataType.Number,
      [
        this._HelperService.AppConfig.ActiveMerchantReferenceId,
      ]
      , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUOrdersList_Filter_Owners_Option = {
      placeholder: 'Sort by Referrer',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUOrdersList_Filter_Owners_Change(event: any) {
    if (event.value == this.TUOrdersList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'DealerLocationId', this._HelperService.AppConfig.DataType.Number, this.TUOrdersList_Filter_Owners_Selected, '=');
      this.TUOrdersList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUOrdersList_Config.SearchBaseConditions);
      this.TUOrdersList_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.TUOrdersList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'DealerLocationId', this._HelperService.AppConfig.DataType.Number, this.TUOrdersList_Filter_Owners_Selected, '=');
      this.TUOrdersList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUOrdersList_Config.SearchBaseConditions);
      this.TUOrdersList_Filter_Owners_Selected = event.value;
      this.TUOrdersList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'DealerLocationId', this._HelperService.AppConfig.DataType.Number, this.TUOrdersList_Filter_Owners_Selected, '='));
    }
    this.TUOrdersList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }


  SetOtherFilters(): void {
    this.TUOrdersList_Config.SearchBaseConditions = [];
    // this.TUOrdersList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    // if (CurrentIndex != -1) {
    //   this.TUOrdersList_Filter_Stores_Selected = null;
    //   this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    // }
  }




  public CategoriesList_Filter_Owners_Option: Select2Options;
  public CategoriesList_Filter_Owners_Selected = 0;
  CategoriesList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetCoreCommons,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ]
    };

    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'TypeCode', this._HelperService.AppConfig.DataType.Text,
      [
        this._HelperService.AppConfig.HelperTypes.MerchantCategories,
      ]
      , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.CategoriesList_Filter_Owners_Option = {
      placeholder: 'Filter By Categories',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  CategoriesList_Filter_Owners_Change(event: any) {
    if (event.value == this.CategoriesList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CategoryKey', this._HelperService.AppConfig.DataType.Text, this.CategoriesList_Filter_Owners_Selected, '=');
      this.TUOrdersList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUOrdersList_Config.SearchBaseConditions);
      this.CategoriesList_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.CategoriesList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CategoryKey', this._HelperService.AppConfig.DataType.Text, this.CategoriesList_Filter_Owners_Selected, '=');
      this.TUOrdersList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUOrdersList_Config.SearchBaseConditions);
      this.CategoriesList_Filter_Owners_Selected = event.value;
      this.TUOrdersList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CategoryKey', this._HelperService.AppConfig.DataType.Text, this.CategoriesList_Filter_Owners_Selected, '='));
    }
    this.TUOrdersList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {

    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.TUOrdersList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.TUOrdersList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.TUOrdersList_Config);

    //   if (Type == 'Time') {
    //     this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
    //     this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
    // }
    if (Type == 'Time') {
      this.DateRangeOptions.startDate = new Date(2017, 0, 1, 0, 0, 0, 0);
      this.DateRangeOptions.endDate = moment().endOf("day");
    }


    this.SetOtherFilters();

    this.TUOrdersList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      // inputAttributes: {
      //   autocapitalize: "off",
      //   autocorrect: "off",
      //   maxLength: "4",
      //   minLength: "4",
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Cashier,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Cashier
        );
        this._FilterHelperService.SetMerchantConfig(this.TUOrdersList_Config);
        this.TUOrdersList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.TUOrdersList_GetData();

    if (ButtonType == 'Sort') {
      $("#TUOrdersList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#TUOrdersList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.TUOrdersList_Config);
    this.SetOtherFilters();

    this.TUOrdersList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    // this.TUOrdersList_Filter_Stores_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  public OverviewData: any = {
    Total: 0,
    Active: 0,
    Blocked: 0,

  };


  GetOverviews(): any {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getorderoverview,
      ReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.OverviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });



  }
  TUOrderList_ListTypeChange(Type) {
    this.ListType = Type;
    this.TUOrdersList_Setup();
  }
  GotoAddMerchant() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.UserOnboarding.Merchant
    ]);
  }

}

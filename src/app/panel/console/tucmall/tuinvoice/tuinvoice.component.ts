import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as jsPDF from 'jspdf';
import { Observable } from 'rxjs';
declare var $: any;
import { DataHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
import * as Feather from "feather-icons";
import * as cloneDeep from 'lodash/cloneDeep';
@Component({
    selector: 'tu-tuinvoice',
    templateUrl: './tuinvoice.component.html',
    styleUrls: ['./tuinvoice.component.css']


})
export class TUInvoiceComponent implements OnInit {

    Print() {
        window.print();
    }

    Form_Email: FormGroup;

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {
    }
    ngOnInit() {
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
            this._HelperService.AppConfig.ActiveAccountKey = params["accountid"];
            this._HelperService.AppConfig.ActiveAccountId = params["accountkey"];

            if (this._HelperService.AppConfig.ActiveReferenceId == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
            } else {
                this.Setup_UserInvoiceDetails();
            }
        });
        Feather.replace();
        this.Form_Email_Load();
    }

    public _InvoiceDetail: any = {

        CreateDate: null,
        IssuedDate: null,
        InvoiceNumber: null,
        CustomerDetails: {
            Name: null,
            EmailAddress: null,
            Address: null
        },

        MerchantDetails: {
            Name: null,
            EmailAddress: null,
            Address: null,
        },
        Products: [
            {
                ProductTitle: null,
                Quantity: null,
                ItemAmount: null,
                TotalItemAmount: null
            }
        ],
        PaymentDetails: {
            DiscountAmount: null,
            DeliveryCharges: null,
            SubTotalAmount: null,
            TotalAmount: null,
            ComissionAmount: null,
        }
    };


    public Setup_UserInvoiceDetails() {
        this._HelperService.AppConfig.ShowHeader = true;
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: "getdelvieryinvoice",
            ReferenceID: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            IsEmail: false
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._InvoiceDetail = _Response.Result;
                    this._InvoiceDetail.CreateDateS = this._HelperService.GetDateTimeS(this._InvoiceDetail.CreateDate);
                    this._InvoiceDetail.ModifyDateS = this._HelperService.GetDateTimeS(this._InvoiceDetail.ModifyDate);
                    this._InvoiceDetail.CreateDate = this._HelperService.GetDateS(this._InvoiceDetail.CreateDate);
                    this._InvoiceDetail.ExpectedDeliveryDateS = this._HelperService.GetDateTimeS(this._InvoiceDetail.ExpectedDeliveryDate);
                    this._InvoiceDetail.ActualDeliveryDateS = this._HelperService.GetDateTimeS(this._InvoiceDetail.ActualDeliveryDate);
                    this._InvoiceDetail.StatusI = this._HelperService.GetStatusIcon(this._InvoiceDetail.StatusCode);
                    this._InvoiceDetail.StatusB = this._HelperService.GetStatusBadge(this._InvoiceDetail.StatusCode);
                    this._InvoiceDetail.StatusC = this._HelperService.GetStatusColor(this._InvoiceDetail.StatusCode);
                    this._HelperService.IsFormProcessing = false;
                } else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    IsEmail() {
        this._HelperService.OpenModal('Form_Email_Address')
    }
    Form_Email_Close() {
        this._HelperService.CloseModal('Form_Email_Address');
        this.Form_Email.reset();
    }

    Form_Email_Load() {
        this.Form_Email = this._FormBuilder.group({
            bussinessEmail: ['', Validators.compose([Validators.required, Validators.email, Validators.minLength(2),
            Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$')])],
        });
    }

    Form_Email_Clear() {
        this.Form_Email.reset();
        this.Form_Email_Load();
    }

    public SaveAccountRequest: any;

    Form_Email_Process(_FormValue: any) {
        this.SaveAccountRequest = this.ReFormatexp_RequestBody();
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, this.SaveAccountRequest);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                    this._HelperService.CloseModal("Form_Email_Address");
                    this.Form_Email_Clear();
                    this.Form_Email_Close();
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    ReFormatexp_RequestBody(): void {
        var formValue: any = cloneDeep(this.Form_Email.value);
        var formRequest: any = {
            'OperationType': 'new',
            Task: "getdelvieryinvoice",
            ReferenceID: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            EmailAddress:formValue.bussinessEmail,
            IsEmail: true
        };
        return formRequest;
    }


}
import { ChangeDetectorRef, Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ChartDataSets } from 'chart.js';
import * as Feather from 'feather-icons';
import * as cloneDeep from 'lodash/cloneDeep';
import { BaseChartDirective, Color } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OResponse, OSalesTrend, ODealData, OSelect, OList, FilterHelperService } from '../../../../service/service';
declare var moment: any;
declare var $: any;

@Component({
  selector: 'app-deliveryoverview',
  templateUrl: './deliveryoverview.component.html',
  styleUrls: ['./deliveryoverview.component.css']
})
export class DeliveryoverviewComponent implements OnInit {
  isOpen = true;
  public TodayStartTime = null;
  public TodayEndTime = null;
  DealsData = [];
  ActualStartDate = moment().startOf("day");
  ActualEndDate = moment().endOf("day");
  salesOverview: any = {};
  monthStartDate = moment().startOf("month");
  monthEndDate = moment().endOf("month");
  dealOverview: any = {};
  optionData = [{ id: 1, name: 'Thankucash' }]
  pickupStatusData = [128, 22];
  delievryStatusData = [128, 22];
  pickupStatusChartColor = [
    {
      backgroundColor: ["#10B759", "#DC3545"],
    },
  ]
  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [87, 44, 22, 55, 77, 333, 222], borderDash: [10, 5], label: 'Series B' },

  ];
  public LineChartYearlyLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
  public lineChartYearlyData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40, 80, 81, 56, 55, 40] },
    { data: [87, 44, 22, 55, 77, 333, 222, 80, 181, 256, 155, 140], borderDash: [10, 5] },

  ];
  public LineWeelyChartLabels = ['MON', 'TUE', 'WED', 'THUS', 'FRI', 'SAT', 'SUN'];
  public lineChartRedeemData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40] },
    { data: [87, 44, 22, 55, 77, 333, 222], borderDash: [10, 5] },

  ];
  public lineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ]
  public SalelineChartData: ChartDataSets[] = [
    { data: [], label: 'Today' },
    { data: [], borderDash: [10, 5], label: 'Compared Day' },
  ];
  public SalelineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];
  public SalelineChartLabels = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00',
    '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
  showSaleChart = true;
  public deliveryOverviewdata: any = {};

  lastdaytext = "LAST DAY";
  lastweektext = "LAST WEEK";
  lastweekCustom = "LAST WEEK";
  lastmonthtext = "LAST MONTH";
  lastyeartext = "LAST YEAR";

  Types: any = {
    year: 'year',
    month: 'month',
    week: 'week',
    day: 'day',
    hour: 'hour'
  }


  public YearlylineChartData: ChartDataSets[] = [
    { data: [], label: 'Current Year' },
    { data: [], borderDash: [10, 5], label: 'Compared Year' },
  ];
  public YearlylineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];
  public YearlylineChartLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
  showYearlyChart = true;

  public RevenuebarChartLabels = [];
  // public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public RevenuebarChartColors = [{ backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }];
  public RevenuebarChartType = 'bar';
  public RevenuebarChartLegend = true;
  public RevenuebarChartData = [
    { data: [200], label: 'Remote' },
    { data: [22], label: 'Remote' },
    { data: [22], label: 'Visit' },
    { data: [22], label: 'Visit' },
  ];
  public RevenuelineChartData: ChartDataSets[] = [
    { data: [], label: 'Current Month' },
    { data: [], borderDash: [10, 5], label: 'Compared Month' },
  ];

  public RevenuelineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ]

  public _Yearly: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }

  public RedeemlineChartData: ChartDataSets[] = [
    { data: [], label: 'Current Week' },
    { data: [], borderDash: [10, 5], label: 'Week Compared' },
  ];
  public RedeemlineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];
  public RedeemlineChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
  showRedeemChart = true;

  public _Redeem: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }

  showRevenueChart = true;

  public _Revenue: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }



  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService: FilterHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) { }

  public _Sale: any = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }

  close() {
    this.isOpen = false;
  }

  _GetoverviewSummary :any ={};
  GetSalesOverview() {

    const pData = {
      Task: 'getordersovrview',
    };

    
    this._HelperService.IsFormProcessing = true;


    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetoverviewSummary = _Response.Result as any;


          this._ChangeDetectorRef.detectChanges();


          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  private RefreshDateLabels() {
    this.lastyeartext = this._HelperService.GetDateSByFormat(this._Yearly.CompareStartDate, 'YYYY');
    this.lastdaytext = this._HelperService.GetDateS(this._Sale.CompareStartDate);

    this.lastweektext = this._HelperService.GetDateSByFormat(this._Redeem.CompareStartDate, 'DD MMM YY')
      + "-" + this._HelperService.GetDateSByFormat(this._Redeem.CompareEndDate, 'DD MMM YY');

    this.lastmonthtext = this._HelperService.GetDateSByFormat(this._Revenue.CompareStartDate, 'MMM YYYY');
  }

  public CurrentRange: any = {};
  public DateSelected = moment().startOf("day");
  //#region DateChangeHandler
  DateChanged(event: any, Type: any): void {

    var ev: any = cloneDeep(event);

    this.DateSelected = moment(ev.start).startOf("day");
    this.ActualStartDate = moment(ev.start).startOf("day");
    this.ActualEndDate = moment(ev.end).endOf("day");

    //#region Sale 

    this._Sale.ActualStartDate = moment(ev.start).startOf("day");
    this._Sale.ActualEndDate = moment(ev.end).endOf("day");

    this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'day').startOf("day");
    this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'day').endOf("day");

    //#endregion
    //#region Week 

    this._Redeem.ActualStartDate = moment(ev.start).startOf('week').startOf('day');
    this._Redeem.ActualEndDate = moment(ev.end).endOf('week').endOf('day');

    this._Redeem.CompareStartDate = moment(ev.start).subtract(1, 'week').startOf('week').startOf('day');
    this._Redeem.CompareEndDate = moment(ev.end).subtract(1, 'week').endOf('week').endOf('day');

    //#endregion
    //#region Revenue 

    this._Revenue.ActualStartDate = moment(ev.start).startOf('month').startOf('day');
    this._Revenue.ActualEndDate = moment(event.end).endOf('month').endOf('day');

    this._Revenue.CompareStartDate = moment(ev.start).subtract(1, 'months').startOf('month').startOf('day');
    this._Revenue.CompareEndDate = moment(ev.end).subtract(1, 'months').endOf('month').endOf('day');

    this.RevenuebarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate).endOf('month').endOf('day'));
    //#endregion
    //#region Yearly 

    this._Yearly.ActualStartDate = moment(ev.start).startOf('year').startOf('day');
    this._Yearly.ActualEndDate = moment(event.end).endOf('year').endOf('day');

    this._Yearly.CompareStartDate = moment(ev.start).subtract(1, 'year').startOf('year').startOf('day');
    this._Yearly.CompareEndDate = moment(ev.end).subtract(1, 'year').endOf('year').endOf('day');

    //#endregion

    var difference = moment(ev.end).diff(moment(ev.start), 'days');

    if (difference <= 1) {
      this.CurrentRange = this._Sale;
      this._SaleDailyReportGetActualData(this.Types.hour);
    } else if (difference <= 7) {
      this.CurrentRange = this._Redeem;
      this._SaleDailyReportGetActualData(this.Types.week);
    } else if (difference <= 30) {
      this.CurrentRange = this._Revenue;
      this._SaleDailyReportGetActualData(this.Types.month);
    } else {
      this.CurrentRange = this._Yearly;
      this._SaleDailyReportGetActualData(this.Types.year);
    }

    this.getOrderOverviewData();
    this.RefreshDateLabels();

  }

  public _SaleDailyReportGetActualData(Type): void {

    this.SalelineChartData[0].data = [];
    this.RedeemlineChartData[0].data = [];
    this.RevenuelineChartData[0].data = [];

    if (Type == this.Types.hour) {
      this.GetSalesReportSale(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
    }
    else if (Type == this.Types.week) {
      this.GetSalesReportRedeem(this._Redeem.ActualStartDate, this._Redeem.ActualEndDate, this._Redeem.ActualData, Type, 'actual');

    } else if (Type == this.Types.month) {
      this.GetSalesReport(this._Revenue.ActualStartDate, this._Revenue.ActualEndDate, this._Revenue.ActualData, Type, 'actual');
    }

    //this.GetSalesReport(this._Yearly.ActualStartDate, this._Yearly.ActualEndDate, this._Yearly.ActualData, this.Types.year, 'actual');
  }

  GetSalesReport(StartDateTime, EndDateTime, Data: any[], Type, LineType: string) {
    this._HelperService.IsFormProcessing = true;
    this.pSaleData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pSaleData.EndDate = this._HelperService.DateInUTC(EndDateTime);
    this.pSaleData.Type = Type;

    if (Type == this.Types.month) {
      this.pSaleData.Type = this.Types.month;
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, this.pSaleData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result;


          var TempArrayTotalAmount = [];
          var AmountSum = 0;

          if (Type == this._HelperService.AppConfig.GraphTypes.month) {
            var MonthAllDays: any[] = this._HelperService.CalculateIntermediateDate(cloneDeep(StartDateTime),
              cloneDeep(EndDateTime));
            for (let index = 0; index < MonthAllDays.length; index++) {
              const element = MonthAllDays[index];
              var RData = Data.find(x => moment(x.Title, 'DD-MM-YYYY').format('DD MMM') == element);
              if (RData) {
                TempArrayTotalAmount.push(RData.TotalOredersAmount);


                const element = RData;



                AmountSum = AmountSum + RData.TotalOredersAmount;




              } else {
                TempArrayTotalAmount.push(0);
              }
            }

            this.showSaleChart = false;
            this.showRevenueChart = false;
            this.showRedeemChart = false;
            this.showYearlyChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.SalelineChartData[0].data = TempArrayTotalAmount;
              this._Sale.ActualSalesAmount = AmountSum;
              this._Sale.ActualData = AmountSum;

            }
            this.SalelineChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate));
            this.RevenuebarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate));
            this.RedeemlineChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate));

            this.showSaleChart = true;
            this.showRevenueChart = true;
            this.showRedeemChart = true;
            this.showYearlyChart = true;
            this._ChangeDetectorRef.detectChanges();


          } else {


            this.showSaleChart = false;
            this.showRevenueChart = false;
            this.showRedeemChart = false;
            this.showYearlyChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.SalelineChartData[0].data = TempArrayTotalAmount;
              this._Sale.ActualSalesAmount = AmountSum;
              // this._Sale.ActualData = TotalSum;


            }




            this.SalelineChartLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
            this.RevenuebarChartLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
            this.RedeemlineChartLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];

            this.showSaleChart = true;
            this.showRevenueChart = true;
            this.showRedeemChart = true;
            this.showYearlyChart = true;
            this._ChangeDetectorRef.detectChanges();
          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  public SaleAmountData: any;


  GetSalesReportSale(StartDateTime, EndDateTime, Data: any[], Type, LineType: string) {
    this._HelperService.IsFormProcessing = true;
    this.pSaleData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pSaleData.EndDate = this._HelperService.DateInUTC(EndDateTime);
    this.pSaleData.Type = this.Types.hour;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, this.pSaleData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          var DataHourly = _Response.Result as any[];
          this.SaleAmountData = _Response.Result.Data



          var TempArrayTotalAmount = [];
          var AmountSum = 0;

          for (let index = 0; index < 24; index++) {
            var RData = DataHourly.find(x => x.Hour == index);
            if (RData != undefined && RData != null) {


              TempArrayTotalAmount.push(RData.TotalOredersAmount);

              const element = RData;



              AmountSum = AmountSum + RData.TotalOredersAmount;

            }
            else {
              TempArrayTotalAmount.push(0);
            }
          }

          if (Type == this._HelperService.AppConfig.GraphTypes.hour) {

            this.showSaleChart = false;
            this.showRevenueChart = false;
            this.showRedeemChart = false;
            this.showYearlyChart = false;
            this._ChangeDetectorRef.detectChanges();
            if (LineType == 'actual') {
              this.SalelineChartData[0].data = TempArrayTotalAmount;
              this._Sale.ActualSalesAmount = AmountSum;
              this._Sale.ActualData = AmountSum;
            }





            this.SalelineChartLabels = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00',
              '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];

            this.showSaleChart = true;

            this._ChangeDetectorRef.detectChanges();
          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  InitializeDates(): void {

    //#region Sale Dates 

    this._Sale.ActualStartDate = moment().startOf('day');
    this._Sale.ActualEndDate = moment().endOf('day');

    this._Sale.CompareStartDate = moment().subtract(1, 'day').startOf('day');
    this._Sale.CompareEndDate = moment().subtract(1, 'day').endOf('day');

    //#region Yearly Dates 

    this._Yearly.ActualStartDate = moment().startOf('year').startOf('day');
    this._Yearly.ActualEndDate = moment().endOf('year').endOf('day');

    this._Yearly.CompareStartDate = moment().subtract(1, 'year').startOf('year').startOf('day');
    this._Yearly.CompareEndDate = moment().subtract(1, 'year').endOf('year').endOf('day');

    //#endregion

    var difference = moment().diff(moment(), 'days');

    if (difference <= 1) {
      this.CurrentRange = this._Sale;
      this._SaleDailyReportGetActualData(this.Types.hour);
    } else if (difference <= 7) {
      this.CurrentRange = this._Redeem;
      this._SaleDailyReportGetActualData(this.Types.week);
    } else if (difference <= 30) {
      this.CurrentRange = this._Revenue;
      this._SaleDailyReportGetActualData(this.Types.month);
    } else {
      this.CurrentRange = this._Yearly;
      this._SaleDailyReportGetActualData(this.Types.year);
    }

  }

  GetSalesReportRedeem(StartDateTime, EndDateTime, Data: any[], Type, LineType: string) {
    this._HelperService.IsFormProcessing = true;

    this.pSaleData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pSaleData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    this.pSaleData.Type = Type;

    if (Type == this.Types.week) {
      this.pSaleData.Type = 'week';
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, this.pSaleData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as any[];
          this.SaleAmountData = _Response.Result.Data



          var TempArrayTotalAmount = [];
          var AmountSum = 0;


          var TempColorArray = ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'];

          var Heigest = Data[0];
          var HeigestIndex = 0;
          var Lowest = Data[0];
          var LowestIndex = 0;

          var DataSale = _Response.Result;

          for (let index = 0; index < 7; index++) {

            var weekday = '';
            switch (index) {
              case 0: weekday = 'Sunday';
                break;
              case 1: weekday = 'Monday';

                break;
              case 2: weekday = 'Tuesday';

                break;
              case 3: weekday = 'Wednesday';

                break;
              case 4: weekday = 'Thursday';

                break;
              case 5: weekday = 'Friday';

                break;
              case 6: weekday = 'Saturday';
                break;

              default:
                break;
            }

            var RData = DataSale.find(x => x['WeekDay'] == weekday);
            if (RData != undefined && RData != null) {

              TempArrayTotalAmount.push(RData.Amount);


              const element: ODealData = RData;
              AmountSum = AmountSum + RData.Amount;


              // if (Heigest.TotalInvoiceAmount < element.TotalInvoiceAmount) {
              //   Heigest = element;
              //   HeigestIndex = index;
              // }

              // if (Lowest.TotalInvoiceAmount > element.TotalInvoiceAmount) {
              //   Lowest = element;
              //   LowestIndex = index;
              // }
            }
            else {
              TempArrayTotalAmount.push(0);
            }

          }

          if (Type == this._HelperService.AppConfig.GraphTypes.week) {

            this.showSaleChart = false;
            this.showRevenueChart = false;
            this.showRedeemChart = false;
            this.showYearlyChart = false;
            this._ChangeDetectorRef.detectChanges();



            if (LineType == 'actual') {
              this.SalelineChartData[0].data = TempArrayTotalAmount;

              this._Sale.ActualSalesAmount = AmountSum;
              this._Sale.ActualData = AmountSum;


            }

            this.SalelineChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
            this.RevenuebarChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
            this.RedeemlineChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

            this.showSaleChart = true;
            this.showRevenueChart = true;
            this.showRedeemChart = true;
            this.showYearlyChart = true;
            this._ChangeDetectorRef.detectChanges();
          }

          // console.log('hello', this.RedeemlineChartData);

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }


  navigateToDeals() {
    this._Router.navigate(["/console/deals"])
  }

  //#region Store Filter
  public TUTr_Filter_Store_Selected: any;
  public ToggleStoreSelect: boolean = false;
  public TUTr_Filter_Store_Option: Select2Options;
  TUTr_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        //  {
        //    SystemName: "AccountTypeCode",
        //    Type: this._HelperService.AppConfig.DataType.Text,
        //    SearchCondition: "=",
        //    SearchValue: this._HelperService.AppConfig.AccountType.Store
        //  }
      ]
    };

    //  var OwnerKey = this._HelperService.UserAccount.AccountId;
    //  if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
    //    OwnerKey = this._HelperService.UserOwner.AccountId;
    //  }
    //  _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, OwnerKey, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Store_Option = {
      placeholder: 'Select Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }

  private pSaleData = {
    Task: 'getorderhistory',
    StartDate: null,
    EndDate: null,
    // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };

  TUTr_Filter_Stores_Change(event: any) {
    if (event.value == this.TUTr_Filter_Store_Selected) {
      this.pSaleData.StoreReferenceId = 0;
      this.pSaleData.StoreReferenceKey = null;
      this.TUTr_Filter_Store_Selected = 0;
    }
    else if (event.value != this.TUTr_Filter_Store_Selected) {
      this.pSaleData.StoreReferenceId = event.data[0].ReferenceId;
      this.pSaleData.StoreReferenceKey = event.data[0].ReferenceKey;

      this.TUTr_Filter_Store_Selected = event.value;
    }
    // this.LoadData();
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 500);
  }
  //#endregion
  // public DateSelected = moment().startOf("day");
  // //#region DateChangeHandler
  // DateChanged(event: any, Type: any): void {

  //   this._HelperService.DateChanged.next(event);

  //   var ev: any = cloneDeep(event);
  //   this.DateSelected = moment(ev.start).startOf("day");
  //   this.ActualStartDate = moment(ev.start).startOf("day");
  //   this.ActualEndDate = moment(ev.end).endOf("day");

  //   // this.selectedMonth = new Date(ev.start).getMonth() + 1;

  //   this.getOrderOverviewData();
  //   this.GetDealsOverview();
  //   this.GetSalesOverview();
  // }
  //#endregion

  getOrderOverviewData() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetOrderOverview,
      StartDate: this._HelperService.DateInUTC(this.ActualStartDate),
      EndDate: this._HelperService.DateInUTC(this.ActualEndDate),
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.deliveryOverviewdata = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

  GetDealsOverview() {

    this._HelperService.IsFormProcessing = true;

    let pData = {
      Task: 'getdealsoverview',
      StartDate: this._HelperService.DateInUTC(this.ActualStartDate),
      EndDate: this._HelperService.DateInUTC(this.ActualEndDate)
    }

    // this.pData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    // this.pData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);
    // this.pData.Ac/countId = this._HelperService.AppConfig.ActiveMerchantReferenceId
    // this.pData.AccountKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.dealOverview = _Response.Result as any;

          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  DealsList_Setup() {
    let DealPayload = {
      Task: "getdeals",
      TotalRecords: 0,
      Offset: 0,
      Limit: 2,
      RefreshCount: true,
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'AccountId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '=='),
      SortExpression: "CreateDate desc",
      Type: "running",
      ReferenceKey: null,
      ReferenceId: 0,
      SubReferenceId: 0,
      ListType: 3,
      IsDownload: false
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, DealPayload);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this.DealsData = _Response.Result.Data

          //#endregion

        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  ngOnInit() {
    Feather.replace();
    this.getOrderOverviewData();
    this.GetDealsOverview();
    this.DealsList_Setup();
    this.TUTr_Filter_Stores_Load();
    this.GetSalesOverview();

    this.InitializeDates();
  }

}



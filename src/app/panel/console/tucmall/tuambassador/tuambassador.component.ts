import { ChangeDetectorRef, Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { InputFileComponent, InputFile } from 'ngx-input-file';
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Observable } from 'rxjs';
import { OProductDetail } from 'src/app/modules/accountdetails/tuconsole/tucambassador/tucambassador.component';


@Component({
  selector: "tu-tuambassador",
  templateUrl: "./tuambassador.component.html",
})
export class TUAmbassadorComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService.Icon_Crop_Clear();
  }
  remove: any;
  add: any;
  public ResetFilterControls: boolean = true;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = true;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
  }

  @ViewChild("terminal")
  private InputFileComponent_Term: InputFileComponent;

  @ViewChild("cashier")
  private InputFileComponent_Cashier: InputFileComponent;

  CurrentImagesCount: number = 0;

  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();


    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
      this.TUProdVList_Setup();
      this.Get_UserProductDetails();
      this.GetProductCategories_List();

      this.InitColConfig();


    });

    // this._HelperService.StopClickPropogation();
  }

  public _ProductDetail: any = {};
  public Get_UserProductDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: "getproduct",
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._ProductDetail = _Response.Result as OProductDetail;
          this._ProductDetail.CreateDate = this._HelperService.GetDateS(this._ProductDetail.CreateDate);
          this._ProductDetail.CreateDateS = this._HelperService.GetDateTimeS(this._ProductDetail.CreateDate);
          this._ProductDetail.ModifyDateS = this._HelperService.GetDateTimeS(this._ProductDetail.ModifyDate);
          this._ProductDetail.StatusI = this._HelperService.GetStatusIcon(this._ProductDetail.StatusCode);
          this._ProductDetail.StatusB = this._HelperService.GetStatusBadge(this._ProductDetail.StatusCode);
          this._ProductDetail.StatusC = this._HelperService.GetStatusColor(this._ProductDetail.StatusCode);
          this.GetProductCategories_Option.placeholder = this._ProductDetail.CategoryName;
          if (this._ProductDetail.CreatedByDisplayName != undefined) {
            if (this._ProductDetail.CreatedByDisplayName.length > 8) {
              this._ProductDetail.CreatedByDisplayNameShort = this._ProductDetail.CreatedByDisplayName.substring(0, 8) + '..';
            }
            else {
              this._ProductDetail.CreatedByDisplayNameShort = this._ProductDetail.CreatedByDisplayName;
            }
          }
          if (this._ProductDetail.ModifyByDisplayName != undefined) {
            if (this._ProductDetail.ModifyByDisplayName.length > 8) { this._ProductDetail.ModifyByDisplayNameShort = this._ProductDetail.ModifyByDisplayName.substring(0, 8) + '..' } else {
              this._ProductDetail.ModifyByDisplayNameShort = this._ProductDetail.ModifyByDisplayName;
            };
          }
          this.GetStores();

          // this.Form_Product.controls['AllowMultiple'].setValue((this._ProductDetail.AllowMultiple == 1) ? true : false);
          this._HelperService.IsFormProcessing = false;
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Product.AddProducts
      ,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.Icon_Crop_Clear();
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region Variants

  public TUProdVList_Config: OList;
  TUProdVList_Setup() {
    this.TUProdVList_Config = {
      Sort: null,
      Id: null,
      Task: "getninjatransactions",
      Location: this._HelperService.AppConfig.NetworkLocation.V2.HCProduct,
      Title: "Available Variants",
      StatusType: "Product",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'UserAccountKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveOwnerKey, "="),
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      DefaultSortExpression: 'CreateDate desc',
      TableFields: [
        {
          DisplayName: '#REF',
          SystemName: 'ReferenceId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Sku',
          SystemName: 'Sku',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Name',
          SystemName: 'Name',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Price',
          SystemName: 'SellingPrice',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Stock',
          SystemName: 'TotalStock',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: 'text-right',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Added On',
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
        },
        {
          DisplayName: 'Status',
          SystemName: 'StatusName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
      ]
    };
    this.TUProdVList_Config = this._DataHelperService.List_Initialize(
      this.TUProdVList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Variant,
      this.TUProdVList_Config
    );

    this.TUProdVList_GetData();
  }
  TUProdVList_ToggleOption(event: any, Type: any) {
  
    if (event != null) {
      for (let index = 0; index < this.TUProdVList_Config.Sort.SortOptions.length; index++) {
        const element = this.TUProdVList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.TUProdVList_Config


    );

    this.TUProdVList_Config = this._DataHelperService.List_Operations(
      this.TUProdVList_Config,
      event,
      Type
    );

    if (
      (this.TUProdVList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TUProdVList_GetData();
    }
  }
  TUProdVList_GetData() {
    this.GetOverviews(this.TUProdVList_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getproductoverview);
    var TConfig = this._DataHelperService.List_GetData(
      this.TUProdVList_Config
    );
    this.TUProdVList_Config = TConfig;
  }
  TUProdVList_RowSelected(ReferenceData) {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Invoice, ReferenceData.ReferenceKey, ReferenceData.UserAccountKey]);
  }

  timeout = null;
  TUProdVList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      this.TUProdVList_Config = this._DataHelperService.List_Operations(
        this.TUProdVList_Config,
        event,
        Type
      );
      if (this.TUProdVList_Config.RefreshData == true) {
        this.TUProdVList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);


  }

  public SelectedVarient =
    {
      ReferenceId: null,
      ReferenceKey: null,
      Name: null,
      Sku: null,
      ActualPrice: 0,
      SellingPrice: 0,
      StatusCode: null,
    }
  public StockUpdateType = "add";
  TUProdVList_RowSelectedManageInventory(ReferenceData) {
    this.SelectedVarient = ReferenceData;
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: "getvarientstock",
      ReferenceId: ReferenceData.ReferenceId,
      ReferenceKey: ReferenceData.ReferenceKey,
      Offset: 0,
      Limit: 200,
    };
    this._ProductVarientLocation.forEach(element => {
      element.Quantity = 0;
    });
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          var Data = _Response.Result.Data;
          Data.forEach(element => {
            var StoreI = this._ProductVarientLocation.findIndex(x => x.ReferenceId == element.DealerLocationId);
            if (StoreI > -1) {
              this._ProductVarientLocation[StoreI].Quantity = element.Quantity;
            }
          });
          this._HelperService.OpenModal('ModalVarientDetails');
          this._HelperService.IsFormProcessing = false;
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  TUProdVList_RowSelectedUpdateDetails(ReferenceData) {
    this.SelectedVarient = ReferenceData;
    this.InitImagePicker(this.InputFileComponent_Cashier);
    this._HelperService.OpenModal('ModalVarientUpdate');
  }
  UpdateVarient() {
    if (this.SelectedVarient.Name == undefined || this.SelectedVarient.Name == null || this.SelectedVarient.Name == "") {
      this._HelperService.NotifyError("Enter variant name");
    }
    else if (this.SelectedVarient.Sku == undefined || this.SelectedVarient.Sku == null || this.SelectedVarient.Sku == "") {
      this._HelperService.NotifyError("Enter variant sku");
    }
    else if (this.SelectedVarient.ActualPrice == undefined || this.SelectedVarient.ActualPrice == null || this.SelectedVarient.ActualPrice < 0) {
      this._HelperService.NotifyError("Enter variant valid original price");
    }
    else if (this.SelectedVarient.SellingPrice == undefined || this.SelectedVarient.SellingPrice == null || this.SelectedVarient.SellingPrice < 0) {
      this._HelperService.NotifyError("Enter variant valid selling price");
    }
    else {
      this._HelperService.IsFormProcessing = true;
      var pData = {
        Task: "updatevarient",
        ReferenceId: this.SelectedVarient.ReferenceId,
        ReferenceKey: this.SelectedVarient.ReferenceKey,
        Name: this.SelectedVarient.Name,
        Sku: this.SelectedVarient.Sku,
        ActualPrice: this.SelectedVarient.ActualPrice,
        SellingPrice: this.SelectedVarient.SellingPrice,
        IconContent: null
      };

      if (this._HelperService._Icon_Cropper_Data.Content != null) {
        pData.IconContent = this._HelperService._Icon_Cropper_Data;
      }

      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, pData);
      _OResponse.subscribe(
        _Response => {
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.NotifySuccess("Variant details updated");
            this._HelperService.CloseModal('ModalVarientUpdate');
            this.InputFileComponent_Cashier.files.pop();
          } else {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.HandleException(_Error);
        }
      );
    }
  }
  UpdateVarientStock(ReferenceData) {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: "updatevarientstockitem",
      VarientId: this.SelectedVarient.ReferenceId,
      VarientKey: this.SelectedVarient.ReferenceKey,
      DealerLocationId: ReferenceData.ReferenceId,
      DealerLocationKey: ReferenceData.ReferenceKey,
      OperationType: this.StockUpdateType,
      Quantity: ReferenceData.NewQuantity
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Variant Stock updated");
          this.TUProdVList_RowSelectedManageInventory(this.SelectedVarient);
          this._HelperService.IsFormProcessing = false;
          this._HelperService.CloseModal('ModalVarientDetails');
          this._ProductVarientLocation.forEach(element => {
            element.NewQuantity = 0;
          });
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  public _ProductVarientLocation = [];
  public GetStores() {
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: "getstoreslite",
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number,  this._ProductDetail.AccountId, '='),
      Offset: 0,
      Limit: 200,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._ProductVarientLocation = _Response.Result.Data;
          this._ProductVarientLocation.forEach(element => {
            element.Quantity = 0;
          });

        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public NewVarient =
    {
      Name: null,
      Sku: null,
      ActualPrice: 0,
      SellingPrice: 0,
    }
  SaveNewVaroentOpen() {
    this._HelperService.Icon_Crop_Clear();
    this.InitImagePicker(this.InputFileComponent_Term);
    this._HelperService.OpenModal('ModalNewVarient');
  }
  SaveNewVarient() {
    if (this.NewVarient.Name == undefined || this.NewVarient.Name == null || this.NewVarient.Name == "") {
      this._HelperService.NotifyError("Enter variant name");
    }
    else if (this.NewVarient.Sku == undefined || this.NewVarient.Sku == null || this.NewVarient.Sku == "") {
      this._HelperService.NotifyError("Enter variant sku");
    }
    else if (this.NewVarient.ActualPrice == undefined || this.NewVarient.ActualPrice == null || this.NewVarient.ActualPrice < 0) {
      this._HelperService.NotifyError("Enter variant valid original price");
    }
    else if (this.NewVarient.SellingPrice == undefined || this.NewVarient.SellingPrice == null || this.NewVarient.SellingPrice < 0) {
      this._HelperService.NotifyError("Enter variant valid selling price");
    }
    else {
      this._HelperService.IsFormProcessing = true;
      var pData = {
        Task: "savevarient",
        ProductId: this._HelperService.AppConfig.ActiveReferenceId,
        ProductKey: this._HelperService.AppConfig.ActiveReferenceKey,
        Name: this.NewVarient.Name,
        Sku: this.NewVarient.Sku,
        ActualPrice: this.NewVarient.ActualPrice,
        SellingPrice: this.NewVarient.SellingPrice,
        StatusCode: this._HelperService.AppConfig.Status.Product.Active,
        IconContent: null
      };

      if (this._HelperService._Icon_Cropper_Data.Content != null) {
        pData.IconContent = this._HelperService._Icon_Cropper_Data;
      }
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, pData);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this.NewVarient =
            {
              Name: null,
              Sku: null,
              ActualPrice: 0,
              SellingPrice: 0,
            };
            this._HelperService.CloseModal('ModalNewVarient');
            this.TUProdVList_RowSelectedManageInventory(_Response.Result);
            this._HelperService.NotifySuccess("Varient details updated");
            this._HelperService.CloseModal('ModalNewVarient');

            this.InputFileComponent_Term.files.pop();
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.HandleException(_Error);
        }
      );
    }
  }

  UpdateProductStatus(StatusCode) {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: "updateproduct",
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      StatusCode: StatusCode
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Product details updated");
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  UpdateProductVarientStatus(StatusCode) {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: "updatevarient",
      ReferenceId: this.SelectedVarient.ReferenceId,
      ReferenceKey: this.SelectedVarient.ReferenceKey,
      StatusCode: StatusCode
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.SelectedVarient.StatusCode = StatusCode;
          this._HelperService.NotifySuccess("Variant status updated");
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  public GetProductCategories_Option: Select2Options;
  GetProductCategories_List() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Product.getcategories,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.HCProduct,
      SearchCondition: '',
      SortCondition: ['Name asc'],
      Fields: [
        {
          SystemName: 'ReferenceKey',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: 'Name',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        {
          SystemName: 'StatusCode',
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: '=',
          SearchValue: this._HelperService.AppConfig.Status.Active,
        }
      ],
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'ReferenceId', this._HelperService.AppConfig.DataType.Number, '0', '>');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;

    this.GetProductCategories_Option = {
      placeholder: PlaceHolder,
      ajax: _Transport,
      multiple: false,
    };
  }
  public CategoryKey: string;
  public CategoryId: number;

  GetProductCategories_ListChange(event: any) {
    this.CategoryKey = event.data[0].ReferenceKey;
    this.CategoryId = event.data[0].ReferenceId
  }
  //#endregion


  SetOtherFilters(): void {
    this.TUProdVList_Config.SearchBaseConditions = [];
    this.TUProdVList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      // this.TUProdVList_Filter_Owners_Selected = null;
      // this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.TUProdVList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.TUProdVList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.TUProdVList_Config);

    this.SetOtherFilters();

    this.TUProdVList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Merchant
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Merchant
        );
        this._FilterHelperService.SetMerchantConfig(this.TUProdVList_Config);
        this.TUProdVList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.TUProdVList_GetData();

    if (ButtonType == 'Sort') {
      $("#TUProdVList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#TUProdVList_fdropdown").dropdown('toggle');
    }
    
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.TUProdVList_Config);
    this.SetOtherFilters();

    this.TUProdVList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  public OverviewData: any = {
    Total: 0,
    InStock: 0,
    OutOfStock: 0,
    LowStock: 0,
    UnavailableProduct: 0
  };
  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;
    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetData(ListOptions);
    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: SortExpression,
      Type: ListOptions.Type,
      ReferenceKey: ListOptions.ReferenceKey,
      StartDate: ListOptions.StartDate,
      EndDate: ListOptions.EndDate,
      ReferenceId: ListOptions.ReferenceId,
      SubReferenceId: ListOptions.SubReferenceId,
      SubReferenceKey: ListOptions.SubReferenceKey,
      AccountId: ListOptions.AccountId,
      AccountKey: ListOptions.AccountKey,
      ListType: ListOptions.ListType,
      IsDownload: false,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.OverviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });


  }


  private InitImagePicker(InputFileComponent: InputFileComponent) {
    if (InputFileComponent != undefined) {
      this.CurrentImagesCount = 0;
      this._HelperService._InputFileComponent = InputFileComponent;
      InputFileComponent.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
  }

}



import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import * as Feather from 'feather-icons';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { DataHelperService, FilterHelperService, HelperService, OInvoiceDetails, OList, OResponse, OSelect } from "../../../../service/service";
declare var $: any;

@Component({
    selector: 'tu-tuorders',
    templateUrl: './tuorders.component.html',
})
export class TUOrdersComponent implements OnInit {
    public ResetFilterControls: boolean = true;

    public InvoiceKey = null;
    public StoreName = null;
    public _OrderDetail: any = {};
    public _TUOrder: any = null;
    public ListType: number;

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _FilterHelperService: FilterHelperService

    ) {
        this._HelperService.showAddNewPosBtn = false;
        this._HelperService.showAddNewStoreBtn = false;
        this._HelperService.showAddNewCashierBtn = false;
        this._HelperService.showAddNewSubAccBtn = false;
    }

    ngOnInit() {
        Feather.replace();
        this.ListType = 1;
        this.Filter_Stores_Load();
        this._HelperService.Icon_Crop_Clear();

        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
            this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];

            this.TUOrdersList_Setup();
            this.InvoiceList_Setup();
            this.GetOverviews()

        });

    }

    //Stores Filter

    public Filter_Store_Option: Select2Options;
    public Filter_Store_Selected = 0;
    Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStoresLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
                // },
                // {
                //     SystemName: "AccountTypeCode",
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: "=",
                //     SearchValue: this._HelperService.AppConfig.AccountType.Store
                // }
            ]
        };

        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.Filter_Store_Option = {
            placeholder: 'Filter by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    Filter_Stores_Change(event: any) {
        this.StoreName = event.data[0].text;
        //        var StoreId:number = event.data[0].ReferenceId;
        if (event.value == this.Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'DealerLocationId', this._HelperService.AppConfig.DataType.Number, this.Filter_Store_Selected, '=');
            this.TUOrdersList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUOrdersList_Config.SearchBaseConditions);
            this.Filter_Store_Selected = 0;
        }
        else if (event.value != this.Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'DealerLocationId', this._HelperService.AppConfig.DataType.Number, this.Filter_Store_Selected, '=');
            this.TUOrdersList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUOrdersList_Config.SearchBaseConditions);
            this.Filter_Store_Selected = event.value;
            this.TUOrdersList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'DealerLocationId', this._HelperService.AppConfig.DataType.Number, this.Filter_Store_Selected, '='));
        }
        this.TUOrdersList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }





    // new table start
    public TUOrdersList_Config: OList;
    public TUOrdersList_Columns =
        {
            Date: true,
            User: true,
            Type: true,
            Invoice: true,
            Redeem: true,
            Merchant: true,
            DoneBy: true,
            Cashier: true,
        }
    public TUOrdersList_Filters_List =
        {
            Customer: false,
            Type: false,
            Name: true,
            Merchant: false,
            Date: false,
            Sort: false,
            Status: false,
            Store: false,
            Acquirer: false,
            Provider: false,
            Issuer: false,
            InvoiceAmountRange: false,
            RedeemAmountRange: false,
            Cashier: false,
        }
    TUOrdersList_Setup() {
        this.TUOrdersList_Config =
        {
            Task: "getorders",
            Id: null,
            Sort: null,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.HCProduct,
            Title: 'Orders List',
            StatusType: 'orderstatus',
            ReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            // Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
            // Type: this._HelperService.AppConfig.ListType.All,
            DefaultSortExpression: 'CreateDate desc',
            SearchBaseCondition: this.TUOrdersList_Setup_SearchCondition(),
            TableFields: this.TUOrdersList_Setup_Fields(),
        }

        this.TUOrdersList_Config.ListType = this.ListType;
        this.TUOrdersList_Config.SearchBaseCondition = "";

        if (this.TUOrdersList_Config.ListType == 1) // Confirmed And preparing
        {
            this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number', 488, "=");
        }
        else if (this.TUOrdersList_Config.ListType == 2) // Ready To Pickup
        {
            this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number', 489, "=");
        }
        else if (this.TUOrdersList_Config.ListType == 3) // Out For Delivery
        {
            this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number', 491, "=");
        }
        else if (this.TUOrdersList_Config.ListType == 4) {  // Delivered Failed
            this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number', 493, "=");
        }
        else if (this.TUOrdersList_Config.ListType == 5) {  // Cancalled
            this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number', 494, "=");
        }
        else if (this.TUOrdersList_Config.ListType == 6) {  // Delivery Failed
            this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrictFromArray(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number', [], "=");
        }
        else {
            this.TUOrdersList_Config.DefaultSortExpression = 'CreateDate desc';
        }

        this.TUOrdersList_Config = this._DataHelperService.List_Initialize(this.TUOrdersList_Config);
        this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "DealerKey", 'text', this._HelperService.AppConfig.ActiveMerchantReferenceKey, "=");
        this.TUOrdersList_GetData();
    }
    TUOrdersList_Setup_SearchCondition() {
        this.TUOrdersList_Columns =
        {
            Date: true,
            User: true,
            Type: true,
            Invoice: true,
            Redeem: true,
            Merchant: true,
            DoneBy: true,
            Cashier: true,
        }
        var SearchCondition = "";
        // SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'AccountId', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveOwnerId, '=');
        // SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'ParentId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '=');
        // SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'SourceKey', this._HelperService.AppConfig.DataType.Text, 'transaction.source.giftcards', '=');
        // SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'ModeKey', this._HelperService.AppConfig.DataType.Text, 'transaction.mode.credit', '=');
        // SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'TypeKey', this._HelperService.AppConfig.DataType.Text, 'transaction.type.giftcard', '=');

        this.TUOrdersList_Filters_List =
        {
            Customer: true,
            Type: true,
            Merchant: false,
            Date: true,
            Sort: true,
            Name: true,
            Status: true,
            Store: false,
            Acquirer: false,
            Provider: false,
            Issuer: false,
            InvoiceAmountRange: true,
            RedeemAmountRange: true,
            Cashier: true,
        }
        return SearchCondition;
    }
    TUOrdersList_Setup_Fields() {
        var TableFields = [];
        TableFields = [

            {
                DisplayName: 'Name',
                SystemName: 'CustomerDisplayName',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: false,
            },
            {
                DisplayName: 'Agent DisplayName',
                SystemName: 'AgentDisplayName',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: true,
            }, {
                DisplayName: 'Dealer LocationName',
                SystemName: 'DealerLocationName',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: true,
            },
            {
                DisplayName: 'Order Id',
                SystemName: 'OrderId',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: false,
            },
            {
                DisplayName: 'Price',
                SystemName: 'SellingPrice',
                DataType: this._HelperService.AppConfig.DataType.Decimal,
                Show: true,
                Search: false,
                Sort: false,
            },
            {
                DisplayName: 'Reward %',
                SystemName: 'RewardPercentage',
                DataType: this._HelperService.AppConfig.DataType.Decimal,
                Show: true,
                Search: false,
                Sort: false,
            },
            {
                DisplayName: 'Stock',
                SystemName: 'TotalStock',
                DataType: this._HelperService.AppConfig.DataType.Decimal,
                Show: true,
                Search: false,
                Sort: false,
            },
            {
                DisplayName: 'Added On',
                SystemName: 'CreateDate',
                DataType: this._HelperService.AppConfig.DataType.Date,
                Show: true,
                Search: true,
                Sort: true,
                IsDateSearchField: true,
            },
        ];
        return TableFields;
    }
    TUOrdersList_ToggleOption_Date(event: any, Type: any) {
        this.TUOrdersList_ToggleOption(event, Type);
    }
    TUOrdersList_ToggleOption(event: any, Type: any) {
        this.TUOrdersList_Config = this._DataHelperService.List_Operations(this.TUOrdersList_Config, event, Type);
        if (this.TUOrdersList_Config.RefreshData == true) {
            this.TUOrdersList_GetData();
        }
    }
    TUOrdersList_GetData() {
        // this.GetOverviews(this.TUOrdersList_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getorderoverview);
        var TConfig = this._DataHelperService.List_GetData(this.TUOrdersList_Config);
        this.TUOrdersList_Config = TConfig;
    }
    public SelectedProductItem =
        {
            ReferenceId: null,
            ReferenceKey: null,
            AccountId: null,
            AccountKey: null,
            AccountDisplayName: null,
            CategoryKey: null,
            CategoryName: null,
            Name: null,
            SystemName: null,
            Description: null,
            IconUrl: null,
            PosterUrl: null,
            MinimumAmount: null,
            MaximumAmount: null,
            ActualPrice: null,
            SellingPrice: null,
            AllowMultiple: null,
            RewardPercentage: null,
            MaximumRewardAmount: null,
            TotalStock: null,
            CreateDate: null,
            CreatedByDisplayName: null,
            CreatedByKey: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
        };
    TUOrdersList_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this.SelectedProductItem = ReferenceData;
        //this._HelperService.OpenModal('ModalCreditPoints');
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Product.OrderDetail + "/" + ReferenceData.ReferenceKey + "/" + ReferenceData.ReferenceId]);
        // this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;

    }
    //new table end

    F_AddProduct_Show() {
        // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AddProduct]);
    }

    CatForm_Edit_Show(TItem) {
        this.Get_UserOrderDetails(TItem);
    }


    public Get_UserOrderDetails(TItem) {
        this._HelperService.AppConfig.ShowHeader = true;
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: "getorder",
            ReferenceId: TItem.ReferenceId,
            ReferenceKey: TItem.ReferenceKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.CAProduct, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {

                    this._OrderDetail = _Response.Result;
                    this._OrderDetail.CreateDate = this._HelperService.GetDateS(this._OrderDetail.CreateDate);
                    this._OrderDetail.CreateDateS = this._HelperService.GetDateTimeS(this._OrderDetail.CreateDate);
                    this._OrderDetail.ModifyDateS = this._HelperService.GetDateTimeS(this._OrderDetail.ModifyDate);
                    this._OrderDetail.StatusI = this._HelperService.GetStatusIcon(this._OrderDetail.StatusCode);
                    this._OrderDetail.StatusB = this._HelperService.GetStatusBadge(this._OrderDetail.StatusCode);
                    this._OrderDetail.StatusC = this._HelperService.GetStatusColor(this._OrderDetail.StatusCode);
                    if (this._OrderDetail.CreatedByDisplayName != undefined) {
                        if (this._OrderDetail.CreatedByDisplayName.length > 8) {
                            this._OrderDetail.CreatedByDisplayNameShort = this._OrderDetail.CreatedByDisplayName.substring(0, 8) + '..';
                        }
                        else {
                            this._OrderDetail.CreatedByDisplayNameShort = this._OrderDetail.CreatedByDisplayName;
                        }
                    }
                    if (this._OrderDetail.ModifyByDisplayName != undefined) {
                        if (this._OrderDetail.ModifyByDisplayName.length > 8) { this._OrderDetail.ModifyByDisplayNameShort = this._OrderDetail.ModifyByDisplayName.substring(0, 8) + '..' } else {
                            this._OrderDetail.ModifyByDisplayNameShort = this._OrderDetail.ModifyByDisplayName;
                        };
                    }
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.OpenModal('CatForm_Edit_Content');
                } else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }



    public OrderItemList_Config: OList;
    InvoiceList_Setup() {
        this.OrderItemList_Config = {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.Core.GetUserInvoices,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
            Title: "Order Details",
            StatusType: "invoice",
            PageRecordLimit: 5,
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'UserAccountKey', this._HelperService.AppConfig.DataType.Text, "8bab652699b74c2c9caad9f1baf66cfb", '='),
            Type: this._HelperService.AppConfig.ListType.All,
            DefaultSortExpression: 'CreateDate desc',
            TableFields: [
                {
                    DisplayName: '#REF',
                    SystemName: 'ReferenceId',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'User',
                    SystemName: 'UserAccountDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Invoice',
                    SystemName: 'Name',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },


                {
                    DisplayName: 'Amount',
                    SystemName: 'TotalAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Class: 'text-right',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Invoice Date',
                    SystemName: 'InvoiceDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    IsDateSearchField: true,
                },

                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: 'CreateDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Status',
                    SystemName: 'StatusName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: false,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
            ]
        };
        this.OrderItemList_Config = this._DataHelperService.List_Initialize(
            this.OrderItemList_Config
        );
        this.InvoiceList_GetData();
    }
    InvoiceList_ToggleOption(event: any, Type: any) {
        this.OrderItemList_Config = this._DataHelperService.List_Operations(
            this.OrderItemList_Config,
            event,
            Type
        );
        if (this.OrderItemList_Config.RefreshData == true) {
            this.InvoiceList_GetData();
        }
    }
    timeout = null;
    InvoiceList_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {
            this.OrderItemList_Config = this._DataHelperService.List_Operations(
                this.OrderItemList_Config,
                event,
                Type
            );
            if (this.OrderItemList_Config.RefreshData == true) {
                this.InvoiceList_GetData();
            }
        }, this._HelperService.AppConfig.SearchInputDelay);
    }
    InvoiceList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(
            this.OrderItemList_Config
        );
        this.OrderItemList_Config = TConfig;
    }
    InvoiceList_RowSelected(ReferenceData) {
        this._TUOrder = ReferenceData;
        this._HelperService.OpenModal('CatForm_Edit_Content');

    }

    public InvoiceList_Filter_TransactionType_Option: Select2Options;
    public InvoiceList_Filter_TransactionType_Selected = 0;
    InvoiceList_Filter_TransactionTypes_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "SystemName",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.InvoiceType
                }
            ]
        };

        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.InvoiceList_Filter_TransactionType_Option = {
            placeholder: 'Search By Type',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    InvoiceList_Filter_TransactionTypes_Change(event: any) {
        if (event.value == this.InvoiceList_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_TransactionType_Selected, '=');
            this.OrderItemList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.OrderItemList_Config.SearchBaseConditions);
            this.InvoiceList_Filter_TransactionType_Selected = 0;
        }
        else if (event.value != this.InvoiceList_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_TransactionType_Selected, '=');
            this.OrderItemList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.OrderItemList_Config.SearchBaseConditions);
            this.InvoiceList_Filter_TransactionType_Selected = event.value;
            this.OrderItemList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_TransactionType_Selected, '='));
        }
        this.InvoiceList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    Get_Invoice() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserInvoice,
            Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this.InvoiceKey, '=')
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.ThankU, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._OInvoiceDetails = null;
                    this._OInvoiceDetails = _Response.Result;
                    this._OInvoiceDetails.InvoiceDate = this._HelperService.GetDateS(this._OInvoiceDetails.InvoiceDate);
                    this._OInvoiceDetails.PaymentDate = this._HelperService.GetDateS(this._OInvoiceDetails.PaymentDate);
                    this._OInvoiceDetails.ModifyDateS = this._HelperService.GetDateTimeS(this._OInvoiceDetails.ModifyDate);
                    this._OInvoiceDetails.StatusI = this._HelperService.GetStatusIcon(this._OInvoiceDetails.Status);
                    this._OInvoiceDetails.StatusBadge = this._HelperService.GetStatusBadge(this._OInvoiceDetails.StatusCode);
                    // this._HelperService.OpenModal('ModalInvoiceDetails');
                }
                else {

                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
            });
    }

    _OInvoiceDetails: any =
        {
            PaymentDateS: null,
            StatusBadge: null,
            PaymentApproverDisplayName: null,
            PaymentApproverKey: null,
            PaymentDate: null,
            PaymentModeCode: null,
            PaymentModeName: null,
            PaymentProofUrl: null,
            PaymentReference: null,

            Amount: null,
            Charge: null,
            ChargePercentage: null,
            ComissionAmount: null,
            ComissionPercentage: null,
            Comment: null,
            CreateDate: null,
            CreatedByDisplayName: null,
            CreatedByKey: null,
            Description: null,
            DiscountAmount: null,
            DiscountPercentage: null,
            EndDate: null,
            FromAddress: null,
            FromContactNumber: null,
            FromEmailAddress: null,
            FromFax: null,
            FromName: null,
            InvoiceDate: null,
            InvoiceDateS: null,
            InoviceNumber: null,
            ModifyByDisplayName: null,
            ModifyByKey: null,
            ModifyDate: null,
            ModifyDateS: null,
            Name: null,
            ParentKey: null,
            ParentName: null,
            ReferenceId: null,
            ReferenceKey: null,
            StartDate: null,
            Status: null,
            StatusCode: null,
            StatusI: null,
            StatusName: null,
            ToAddress: null,
            ToContactNumber: null,
            ToEmailAddress: null,
            ToFax: null,
            ToName: null,
            TotalAmount: null,
            TotalItem: null,
            TypeCode: null,
            TypeName: null,
            UnitCost: null,
            UserAccountDisplayName: null,
            UserAccountIconUrl: null,
            UserAccountId: null,
            UserAccountKey: null,
            UserAccountTypeCode: null,
            UserAccountTypeName: null,
            Items: [],
        };

    UpdateStock_Open() {
        this._HelperService.OpenModal('Form_UpdateStock');
    }

    Cancel_Order() {
        this._HelperService.OpenModal('Cancel_Order');
    }

    TUOrderList_ListTypeChange(Type) {
        this.ListType = Type;
        this.TUOrdersList_Setup();

        // this.TUOrdersList_Config.ListType = Type;
        // this.TUOrdersList_Config.SearchBaseCondition = "";

        // if (this.TUOrdersList_Config.ListType == 1) // Active
        // {
        //     this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number',488, "=");
        // }
        // else if (this.TUOrdersList_Config.ListType == 2) // Ready To Preparing
        // {
        //     this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number',489, "=");
        // }
        // else if (this.TUOrdersList_Config.ListType == 3) // Out For Ready
        // {
        //     this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number',490, "=");
        // }
        // else if (this.TUOrdersList_Config.ListType == 4) {  // Ready To Pickup
        //     this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number',491, "=");
        // }
        // else if (this.TUOrdersList_Config.ListType == 5) {  // Out For delivery
        //     this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number',492, "=");
        // }
        // else if (this.TUOrdersList_Config.ListType == 6) {  // Delivery Failed
        //     this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number',493, "=");
        // }
        // else if (this.TUOrdersList_Config.ListType == 7) {  // Delivered
        //     this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number',512, "=");
        // }
        // else if (this.TUOrdersList_Config.ListType == 8) {  // Cancalled
        //     this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number',494, "=");
        // }
        // else if (this.TUOrdersList_Config.ListType == 9) {  // Pending
        //     this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "StatusId", 'number',487, "=");
        // }
        // else {
        //     this.TUOrdersList_Config.DefaultSortExpression = 'CreateDate desc';
        // }
        // this.TUOrdersList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.TUOrdersList_Config.SearchBaseCondition, "DealerKey", 'text', this._HelperService.AppConfig.ActiveOwnerKey, "=");
        // this.TUOrdersList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    SetOtherFilters(): void {
        this.TUOrdersList_Config.SearchBaseConditions = [];
        this.TUOrdersList_Config.SearchBaseCondition = null;

        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
        if (CurrentIndex != -1) {
            // this.TerminalsList_Filter_Owners_Selected = null;
            // this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }

    //#region filterOperations

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetMerchantConfig(this.TUOrdersList_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion

        this.TUOrdersList_GetData();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_Merchant(Type, index);
        this._FilterHelperService.SetMerchantConfig(this.TUOrdersList_Config);

        this.SetOtherFilters();

        this.TUOrdersList_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            input: "text",
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            inputAttributes: {
                autocapitalize: "off",
                autocorrect: "off",
                //maxLength: "4",
                minLength: "4",
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_Merchant(result.value);
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.Order
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._HelperService.Delete_Filter(
                    this._HelperService.AppConfig.FilterTypeOption.Order
                );
                this._FilterHelperService.SetMerchantConfig(this.TUOrdersList_Config);
                this.TUOrdersList_Setup();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });

    }

    public OverviewData: any = {
        Total: 0,
        New: 0,
        PendingConfirmation: 0,
        Confirmed: 0,
        Preparing: 0,
        Ready: 0,
        ReadyToPickUp: 0,
        OutForDelivery: 0,
        DeliveryFailed: 0,
        Cancelled: 0,
        Delivered: 0
    };
    GetOverviews(): any {
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getorderoverview,
            ReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.OverviewData = _Response.Result as any;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);

            });



    }


    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        this._HelperService.MakeFilterSnapPermanent();
        this.TUOrdersList_Setup();

        if (ButtonType == 'Sort') {
            $("#TerminalsList_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#TerminalsList_fdropdown").dropdown('toggle');
        }

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetMerchantConfig(this.TUOrdersList_Config);
        this.SetOtherFilters();

        this.TUOrdersList_Setup();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    //#endregion

    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();

        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();
    }

    UpdateStatus(event, TItem): void {
        this._HelperService.IsFormProcessing = true;
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Product.creategiftcard,
            ReferenceId: TItem.ReferenceId,
            ReferenceKey: TItem.ReferenceKey,
            StatusCode: event.data[0].code,
            Comment: ""
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.CAProduct, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.TUOrdersList_Setup();
                    this._HelperService.IsFormProcessing = false;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            }
            ,
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                this._HelperService.ToggleField = false;
            });
    }

}
import { ChangeDetectorRef, Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ChartDataSets, ChartOptions, ChartType, Chart } from 'chart.js';
import * as Feather from 'feather-icons';
import * as cloneDeep from 'lodash/cloneDeep';
import { BaseChartDirective, Color, Label } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OResponse, OSalesTrend, ODealData, OSelect } from '../../../../service/service';
declare var moment: any;
declare var $: any;
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
// import * as Chart from "..../../../../../../../../node_modules/chart.js/dist/Chart.js";

@Component({
  selector: "tucustomeranalysis",
  templateUrl: "./tucustomeranalysis.component.html",
  styles: [
    `
    agm-map {
      height: 300px;
    }
  `
  ]
})
export class TuCustomerAnalysisComponent implements OnInit {
  showDailyChart = true;
  public LoadingChart: boolean = true;
  public CustomerChart: boolean = true;
  public CustomerReferrals: boolean = true;


  Piechartlable;
  GenderLabel = ['Male', 'Female'];
  AgeLabel;
  Stackeddata: {
    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
    datasets: [{
      data: [1, 5, 3],
      label: 'Jan',
      backgroundColor: "#3e95cd",
    }
      , {
        data: [10, 5],
        label: 'Feb',
        backgroundColor: "#8e5ea2",
      }, {
        data: [4, 8],
        label: 'Mar',
        backgroundColor: "#4287f5",
      }
      , {
        data: [7],
        label: 'Apr',
        backgroundColor: "#23ebbc",
      }
      , {
        data: [3, 1, 5, 0, 7],
        label: 'May',
        backgroundColor: "#e63057",
      }
    ]
  }


  RouteCustomerList() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.CustomerPanel.AllCustomers
    ]);
  }

  CardCash = ['Card Users', 'Cash Users'];
  AppUsers = ['App Users', 'POS Users'];

  public DataSales = [
    { data: [0], label: '1M-2M' },
    { data: [0], label: '2M-3M' },
    { data: [0], label: '3M-4M ' },
    { data: [0], label: 'More Than 5' },
  ]

  public DataReward = [
    { data: [0], label: '1M-2M' },
    { data: [0], label: '2M-3M' },
    { data: [0], label: '3M-4M ' },
    { data: [0], label: 'More Than 5' },
  ]
  StoreLabel: any;

  AgePiedata = []
  GenderSegment = []
  CustomerSpendRange = [];
  public PostHorizontalData = [
    { data: [80], label: 'Active' },
    { data: [72], label: 'In-Active' },
    // { data: [65], label: 'Dead ' },
    // { data: [75], label: 'Idle' },
  ];

  lastdaytext = "LAST DAY";
  lastweektext = "LAST WEEK";
  lastweekCustom = "LAST WEEK";
  lastmonthtext = "LAST MONTH";
  lastyeartext = "LAST YEAR";

  Types: any = {
    year: 'year',
    month: 'month',
    week: 'week',
    day: 'day',
    hour: 'hour'
  }

  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [87, 44, 22, 55, 77, 333, 222], borderDash: [10, 5], label: 'Series B' },

  ];
  public LineChartYearlyLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
  public lineChartYearlyData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40, 80, 81, 56, 55, 40] },
    { data: [87, 44, 22, 55, 77, 333, 222, 80, 181, 256, 155, 140], borderDash: [10, 5] },

  ];
  public LineWeelyChartLabels = ['MON', 'TUE', 'WED', 'THUS', 'FRI', 'SAT', 'SUN'];
  public lineChartRedeemData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40] },
    { data: [87, 44, 22, 55, 77, 333, 222], borderDash: [10, 5] },

  ];
  public lineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ]
  public TodayDate: any;

  public CustomerTUC_Class = [
    { data: [0], label: '1M-2M' },
    { data: [0], label: '2M-3M' },
    { data: [0], label: '3M-4M ' },
    { data: [0], label: 'More Than 5' },
  ]

  CustomerTUC_ClassLables: any;

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {

  }
  StartDateTime: null
  EndDateTime: null
  //#endregion
  ngOnInit() {
    Chart.defaults.global.defaultFontColor = 'white'
    this.TodayStartTime = moment().startOf('day');
    this.TodayEndTime = moment().endOf('day');
    this._HelperService.FullContainer = false;
    Feather.replace();
    this.TodayDate = this._HelperService.GetDateTime(new Date());
    //start time and end time for overview

    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];

      // this.pData.AccountId = this._HelperService.AppConfig.ActiveMerchantReferenceId;
      // this.pData.AccountKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;

      this.InitializeDates();

    });

    this.Customer_TUC_Class()

  }


  MerchantList() {
    this._Router.navigate([
      'console/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AccountSettings.Merchants,

    ]);
  }

  InitializeDates(): void {

    //#region Sale Dates 
    this.TodayStartTime = moment().startOf('day');
    this.TodayEndTime = moment().endOf('day');
    this._Sale.ActualStartDate = moment().startOf('day');
    this._Sale.ActualEndDate = moment().endOf('day');

    this._Sale.CompareStartDate = moment().subtract(1, 'day').startOf('day');
    this._Sale.CompareEndDate = moment().subtract(1, 'day').endOf('day');

    //#endregion

    //#region Redeem Dates 

    this._Redeem.ActualStartDate = moment().startOf('week').startOf('day');
    this._Redeem.ActualEndDate = moment().endOf('day');

    this._Redeem.CompareStartDate = moment().subtract(1, 'week').startOf('week').startOf('day');
    this._Redeem.CompareEndDate = moment().subtract(1, 'week').endOf('week').endOf('day');

    //#endregion

    //#region Revenue Dates 

    this._Revenue.ActualStartDate = moment().startOf('month').startOf('day');
    this._Revenue.ActualEndDate = moment().endOf('month').endOf('day');

    this._Revenue.CompareStartDate = moment().subtract(1, 'months').startOf('month').startOf('day');
    this._Revenue.CompareEndDate = moment().subtract(1, 'months').endOf('month').endOf('day');

    this.RevenuebarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate));

    //#endregion

    //#region Yearly Dates 

    this._Yearly.ActualStartDate = moment().startOf('year').startOf('day');
    this._Yearly.ActualEndDate = moment().endOf('year').endOf('day');

    this._Yearly.CompareStartDate = moment().subtract(1, 'year').startOf('year').startOf('day');
    this._Yearly.CompareEndDate = moment().subtract(1, 'year').endOf('year').endOf('day');

    //#endregion



    var difference = moment().diff(moment(), 'days');
    if (difference <= 1) {
      this._SaleDailyReportGetActualData(this.Types.hour);
    } else if (difference <= 7) {
      this._SaleDailyReportGetActualData(this.Types.week);
    } else if (difference <= 30) {
      this._SaleDailyReportGetActualData(this.Types.month);
    } else {
      this._SaleDailyReportGetActualData(this.Types.year);
    }

    // this.GetCustomerOverview();
    this.GetCustActivityOverview();
    this.GetSpendersOverview();
    this.GetVisitersOverview();
    // this.GetAgeWiseOverview();
    this.GetGenderWiseOverview();
    // this.GetSalesWiseOverview();
    this.GetTotalUsersOverview();
    this.GetMerchantCategories();
    this.GetPurchaseOverview();
    this.GetReferalsOverview();
    this.GetRewardOverview();
    this.GetPaymentsModeOverview();
    this.GetGenderGroupCategories();
  }

  public DateSelected = moment().startOf("day");
  //#region DateChangeHandler
  DateChanged(event: any, Type: any): void {

    var ev: any = cloneDeep(event);

    this.DateSelected = moment(ev.start).startOf("day");

    //#region Sale 
    this.TodayStartTime = moment(ev.start).startOf('day');
    this.TodayEndTime = moment(ev.end).endOf('day');

    this._Sale.ActualStartDate = moment(ev.start).startOf("day");
    this._Sale.ActualEndDate = moment(ev.end).endOf("day");

    this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'day').startOf("day");
    this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'day').endOf("day");

    //#endregion
    //#region Week 

    this._Redeem.ActualStartDate = moment(ev.start).startOf('week').startOf('day');
    this._Redeem.ActualEndDate = moment(ev.end).endOf('week').endOf('day');

    this._Redeem.CompareStartDate = moment(ev.start).subtract(1, 'week').startOf('week').startOf('day');
    this._Redeem.CompareEndDate = moment(ev.end).subtract(1, 'week').endOf('week').endOf('day');

    //#endregion
    //#region Revenue 

    this._Revenue.ActualStartDate = moment(ev.start).startOf('month').startOf('day');
    this._Revenue.ActualEndDate = moment(event.end).endOf('month').endOf('day');

    this._Revenue.CompareStartDate = moment(ev.start).subtract(1, 'months').startOf('month').startOf('day');
    this._Revenue.CompareEndDate = moment(ev.end).subtract(1, 'months').endOf('month').endOf('day');

    this.RevenuebarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate).endOf('month').endOf('day'));
    //#endregion
    //#region Yearly 

    this._Yearly.ActualStartDate = moment(ev.start).startOf('year').startOf('day');
    this._Yearly.ActualEndDate = moment(event.end).endOf('year').endOf('day');

    this._Yearly.CompareStartDate = moment(ev.start).subtract(1, 'year').startOf('year').startOf('day');
    this._Yearly.CompareEndDate = moment(ev.end).subtract(1, 'year').endOf('year').endOf('day');

    //#endregion

    var difference = moment(ev.end).diff(moment(ev.start), 'days');
    if (difference <= 1) {
      this._SaleDailyReportGetActualData(this.Types.hour);
    } else if (difference <= 7) {
      this._SaleDailyReportGetActualData(this.Types.week);
    } else if (difference <= 30) {
      this._SaleDailyReportGetActualData(this.Types.month);
    } else {
      this._SaleDailyReportGetActualData(this.Types.year);
    }

    // this.RefreshDateLabels();
    // this.GetCustomerOverview();
    this.GetCustActivityOverview();
    this.GetSpendersOverview();
    this.GetVisitersOverview();
    // this.GetAgeWiseOverview();
    this.GetGenderWiseOverview();
    // this.GetSalesWiseOverview();
    this.GetTotalUsersOverview();
    this.GetMerchantCategories();
    this.GetPurchaseOverview();
    this.GetReferalsOverview();
    this.GetRewardOverview();
    this.GetPaymentsModeOverview();
    this.GetGenderGroupCategories();


  }
  //#endregion

  //#region BarChartConfig 
  public Options: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      }
    }
  }
  public barChartLabels = [];
  // public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public barChartColors = [{ backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [200], label: 'Remote' },
    { data: [22], label: 'Remote' },
    { data: [22], label: 'Visit' },
    { data: [22], label: 'Visit' },
  ];

  //#endregion

  //#region RevenueBarChartConfig 
  public RevenueOptions: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      }
    }
  }
  public RevenuebarChartLabels = [];
  // public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public RevenuebarChartColors = [{ backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }];
  public RevenuebarChartType = 'bar';
  public RevenuebarChartLegend = true;
  public RevenuebarChartData = [
    { data: [200], label: 'Remote' },
    { data: [22], label: 'Remote' },
    { data: [22], label: 'Visit' },
    { data: [22], label: 'Visit' },
  ];


  public RevenuelineChartData: ChartDataSets[] = [
    { data: [], label: 'Current Month' },
    { data: [], borderDash: [10, 5], label: 'Compared Month' },
  ];

  public RevenuelineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ]

  //#endregion

  //#region Sale Sales Report 

  public SalelineChartData: ChartDataSets[] = [
    { data: [], label: 'Today' },
    { data: [], borderDash: [10, 5], label: 'Compared Day' },
  ];
  public SalelineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];
  public SalelineChartLabels = ['00:00', 'O1:00', '02:00', 'O3:00', '04:00', 'O5:00', '06:00', 'O7:00', '08:00', '09:00', '10:00', '11:00',
    '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
  showSaleChart = true;

  public _Sale: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    CompareSalesTotal: 0,
    ActualSalesTotal: 0,
    SalesAmountDifference: 0
  }

  private RefreshDateLabels() {
    this.lastyeartext = this._HelperService.GetDateSByFormat(this._Yearly.CompareStartDate, 'YYYY');
    this.lastdaytext = this._HelperService.GetDateS(this._Sale.CompareStartDate);

    this.lastweektext = this._HelperService.GetDateSByFormat(this._Redeem.CompareStartDate, 'DD MMM YY')
      + "-" + this._HelperService.GetDateSByFormat(this._Redeem.CompareEndDate, 'DD MMM YY');

    this.lastmonthtext = this._HelperService.GetDateSByFormat(this._Revenue.CompareStartDate, 'MMM YYYY');
  }

  public _SaleSalesReportReset(): void {


  }

  public _SaleDailyReportGetActualData(Type): void {

    this.SalelineChartData[0].data = [];
    this.RedeemlineChartData[0].data = [];
    this.RevenuelineChartData[0].data = [];

    if (Type == this.Types.hour) {
      this.GetSalesReportSale(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
    } else if (Type == this.Types.week) {
      this.GetSalesReportRedeem(this._Redeem.ActualStartDate, this._Redeem.ActualEndDate, this._Redeem.ActualData, Type, 'actual');
    } else if (Type == this.Types.month || Type == this.Types.year) {
      this.GetSalesReport(this._Revenue.ActualStartDate, this._Revenue.ActualEndDate, this._Revenue.ActualData, Type, 'actual');
    }

    //this.GetSalesReport(this._Yearly.ActualStartDate, this._Yearly.ActualEndDate, this._Yearly.ActualData, this.Types.year, 'actual');

  }

  //#endregion

  //#region Redeem Sales Report 

  public RedeemlineChartData: ChartDataSets[] = [
    { data: [], label: 'Current Week' },
    { data: [], borderDash: [10, 5], label: 'Week Compared' },
  ];
  public RedeemlineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];
  public RedeemlineChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
  showRedeemChart = true;

  public _Redeem: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,

    CompareSalesTotal: 0,
    ActualSalesTotal: 0,

    SalesAmountDifference: 0
  }


  public _RedeemSalesReportReset(): void {


  }


  //#endregion

  //#region Revenue Sales Report 

  showRevenueChart = true;

  public _Revenue: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,

    CompareSalesTotal: 0,
    ActualSalesTotal: 0,

    SalesAmountDifference: 0
  }

  public _RevenueSalesReportReset(): void {


  }
  RouteCustomer(ReferenceData) {
    //#region Save Current Merchant To Storage 

    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveCustomer,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Customer,
      }
    );

    //#endregion
    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.CustomerPanel.Accounts.WalletInfo,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);

    //#endregion

  }

  //#endregion

  //#region Customer Sales Report 

  public _Customer: any = {
    TotalCustomers: 0
  }

  //#endregion

  public _GetoverviewSummary: any = {};
  public TodayStartTime = null;
  public TodayEndTime = null;
  public Counts: any = {}
  public Spenders: any = []
  public TopVisitors: any = []
  public GenderRange: any = []
  public TotalAppUsers = ['0', '0'];
  public TotalReferalsUsers = ['0', '0', '0'];
  public PaymentsModeStatus = ['0','0'];
  public PaymentModes = ['0', '0'];
  public PaymentModelabel = ['Card', 'Cash'];
  public ReferalLabel = ['By Customers', 'Merchants', 'Bank,Partners & PTSP'];
  public AgeRange: any = []
  public GenderTotal: number = 0;
  public AgeTotal: number = 0;
  public SpenderTotal: number = 0;
  public VisitorTotal: number = 0;

  public SpendRange: any = []
  private pCustomerData = {
    Task: 'getcustomersoverview',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetCustomerOverview() {

    this._HelperService.IsFormProcessing = true;
    this.LoadingChart = true;

    this.pCustomerData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pCustomerData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analytics, this.pCustomerData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.LoadingChart = false;

          //#region reset totals 

          this.GenderTotal = 0;
          this.AgeTotal = 0;
          this.SpenderTotal = 0;
          this.VisitorTotal = 0;

          //#endregion

          this._GetoverviewSummary = _Response.Result as any;
          this.Counts = this._GetoverviewSummary.Counts;
          this.Spenders = this._GetoverviewSummary.TopSpenders;
          this.TopVisitors = this._GetoverviewSummary.TopVisitors;
          // this.GenderRange = this._GetoverviewSummary.GenderRange;
          this.SpendRange = this._GetoverviewSummary.SpendRange;
          // this.AgeRange = this._GetoverviewSummary.AgeRange;
          // this.AgePiedata = this.AgeRange.Count;
          // this.AgePiedata = []
          // for (let index = 0; index < this.AgeRange.length; index++) {
          //   const element = this.AgeRange[index];
          //   this.AgeTotal = this.AgeTotal + element.Count;

          //   this.AgePiedata.push(element.Count);

          //   switch (index) {
          //     case 0: this.AgeRange[index].text = 'tx-verve';

          //       break;
          //     case 1: this.AgeRange[index].text = 'tx-MasterCard';

          //       break;
          //     case 2: this.AgeRange[index].text = 'tx-teal';

          //       break;

          //     default:
          //       break;
          //   }

          // }

          // this.GenderSegment = []
          // for (let index = 0; index < this.GenderRange.length; index++) {
          //   const element1 = this.GenderRange[index];

          //   this.GenderTotal = this.GenderTotal + element1.Count;

          //   this.GenderRange.Percentage =

          //     this.GenderSegment.push(element1.Count);

          //   switch (index) {
          //     case 0: this.GenderRange[index].text = 'tx-verve';

          //       break;
          //     case 1: this.GenderRange[index].text = 'tx-teal';

          //       break;
          //       // case 2: this.GenderRange[index].text = 'tx-rubik';

          //       break;

          //     default:
          //       break;
          //   }

          // }

          this.CustomerSpendRange = [];
          for (let index = 0; index < this.SpendRange.length; index++) {
            const element2 = this.SpendRange[index];

            this.SpenderTotal = this.SpenderTotal + element2.Count;
            this.CustomerSpendRange.push(element2.Count);

            switch (index) {
              case 0: this.SpendRange[index].text = 'tx-verve';

                break;
              case 1: this.SpendRange[index].text = 'tx-MasterCard';

                break;
              case 2: this.SpendRange[index].text = 'tx-teal';

                break;
              case 3: this.SpendRange[index].text = 'tx-primary';

                break;

              default:
                break;
            }

          }
          this.LoadingChart = false;
          this.showDailyChart = true;
          this._ChangeDetectorRef.detectChanges();
          //Percentage

          //ENd PErcentage

          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public CustActivity: any = {
    Total: 0,
    Active: 0,
    Suspended: 0,
    Blocked: 0,
    AppUsers: 0,
    NonAppUsers: 0,
    AppDownloads: {
      Total: 0,
      Active: 0,
      Idle: 0,
      Dead: 0
    }
  }
  private pCustActivityData = {
    Task: 'getcustactivitystatuscounts',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetCustActivityOverview() {

    this._HelperService.IsFormProcessing = true;
    this.LoadingChart = true;

    this.pCustActivityData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pCustActivityData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.CustAnalytics, this.pCustActivityData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.CustActivity = _Response.Result as any;



         
          this.LoadingChart = false;

          //Percentage

          //ENd PErcentage

          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public _GetPurchaseoverviewSummary = []
  SaleElement1: any = {};
  SaleElement2: any = {};
  SaleElement3: any = {};
  SaleElement4: any = {};
  private pSaleData = {
    Task: 'getcustspendrange',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetPurchaseOverview() {

    this._HelperService.IsFormProcessing = true;

    this.pSaleData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pSaleData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.CustAnalytics, this.pSaleData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetPurchaseoverviewSummary = _Response.Result as any;
          this.DataSales = [
            { data: [this._GetPurchaseoverviewSummary[0].Count], label: '1M-2M' },
            { data: [this._GetPurchaseoverviewSummary[1].Count], label: '2M-3M' },
            { data: [this._GetPurchaseoverviewSummary[2].Count], label: '3M-4M' },
            { data: [this._GetPurchaseoverviewSummary[3].Count], label: 'More Than 4M' },
          ];

          this.SaleElement1 = this._GetPurchaseoverviewSummary[0];
          this.SaleElement2 = this._GetPurchaseoverviewSummary[1];
          this.SaleElement3 = this._GetPurchaseoverviewSummary[2];
          this.SaleElement4 = this._GetPurchaseoverviewSummary[3];

          // for (let index = 0; index < this._GetPurchaseoverviewSummary.length; index++) {
          //   const element = this._GetPurchaseoverviewSummary[index];

          // }  


          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public _GetRewardSummary = []
  RewardElement1: any = {};
  RewardElement2: any = {};
  RewardElement3: any = {};
  RewardElement4: any = {};
  private pRewardData = {
    Task: 'getcustrewardrange',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetRewardOverview() {

    this._HelperService.IsFormProcessing = true;

    this.pRewardData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pRewardData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.CustAnalytics, this.pRewardData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetRewardSummary = _Response.Result as any;
          if (this._GetRewardSummary != undefined) {
            this.DataReward = [
              { data: [this._GetRewardSummary[0].Count], label: '1M-2M' },
              { data: [this._GetRewardSummary[1].Count], label: '2M-3M' },
              { data: [this._GetRewardSummary[2].Count], label: '3M-4M' },
              { data: [this._GetRewardSummary[3].Count], label: 'More Than 4M' },
            ];
            this.RewardElement1 = this._GetRewardSummary[0];
            this.RewardElement2 = this._GetRewardSummary[1];
            this.RewardElement3 = this._GetRewardSummary[2];
            this.RewardElement4 = this._GetRewardSummary[3];
          }

          // for (let index = 0; index < this._GetPurchaseoverviewSummary.length; index++) {
          //   const element = this._GetPurchaseoverviewSummary[index];

          // }  


          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public TopSpenders: any = []
  private pSpendersData = {
    Task: 'getcusttopspenders',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetSpendersOverview() {

    this._HelperService.IsFormProcessing = true;
    this.LoadingChart = true;

    this.pSpendersData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pSpendersData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.CustAnalytics, this.pSpendersData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.TopSpenders = _Response.Result as any;




          this.LoadingChart = false;

          //Percentage

          //ENd PErcentage

          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  public TopVisits: any = []
  private pVisitorsData = {
    Task: 'getcustomerstopvisitors',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetVisitersOverview() {

    this._HelperService.IsFormProcessing = true;
    this.LoadingChart = true;

    this.pVisitorsData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pVisitorsData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.CustAnalytics, this.pVisitorsData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.TopVisits = _Response.Result as any;




          this.LoadingChart = false;

          //Percentage

          //ENd PErcentage

          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  public AgeWise: any = []
  private pAgeWiseData = {
    Task: 'getcustagegroups',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetAgeWiseOverview() {

    this._HelperService.IsFormProcessing = true;
    this.LoadingChart = true;
    this.pAgeWiseData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pAgeWiseData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.CustAnalytics, this.pAgeWiseData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.LoadingChart = false;
          //#endregion

          this.AgeWise = _Response.Result as any;
          this.AgeRange = this.AgeWise;
          // this.AgePiedata = this.AgeRange.Count;
          this.AgePiedata = []
          for (let index = 0; index < this.AgeRange.length; index++) {
            const element = this.AgeRange[index];
            this.AgeTotal = this.AgeTotal + element.Count;

            this.AgePiedata.push(element.Count);

            switch (index) {
              case 0: this.AgeRange[index].text = 'tx-verve';

                break;
              case 1: this.AgeRange[index].text = 'tx-MasterCard';

                break;
              case 2: this.AgeRange[index].text = 'tx-teal';

                break;

              default:
                break;
            }

          }


          this.LoadingChart = false;




        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  public GenderWise: any = []
  public percentage = null;
  private pGenderWiseData = {
    Task: 'getcustgenders',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetGenderWiseOverview() {

    this._HelperService.IsFormProcessing = true;
    this.LoadingChart = true;
    this.pGenderWiseData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pGenderWiseData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.CustAnalytics, this.pGenderWiseData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.LoadingChart = false;
          this.showDailyChart = false;
          this._ChangeDetectorRef.detectChanges();
          this.GenderTotal = 0;
          this.VisitorTotal = 0;

          //#endregion

          this.GenderWise = _Response.Result as any;
          this.GenderRange = this.GenderWise;
          this.GenderSegment = []
          for (let index = 0; index < this.GenderRange.length; index++) {
            const element1 = this.GenderRange[index];

            this.GenderTotal = this.GenderTotal + element1.Count;

            this.GenderRange.Percentage =

              this.GenderSegment.push(element1.Count);
            switch (index) {
              case 0: this.GenderRange[index].text = 'text-primary';

                break;
              case 1: this.GenderRange[index].text = 'tx-verve';

                break;
                // case 2: this.GenderRange[index].text = 'tx-rubik';

                break;

              default:
                break;
            }

          }
          this.LoadingChart = false;
          this.showDailyChart = true;
          this._ChangeDetectorRef.detectChanges();


        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public barChartOptions: ChartOptions = {
    responsive: true,
    layout: {
      padding: {
        right: 0,
        left: 5,
      },
    },
    scales: {
      xAxes: [{
        display: false,
        gridLines: {
          display: false
        },

      }],
      yAxes: [{
        display: false,
        gridLines: {
          display: false
        }
      }]
    }
  };
  public SbarChartLabels: Label[] = ['', '', '', ''];
  public SbarChartType: ChartType = 'horizontalBar';
  public SbarChartLegend = false;
  public barChartPlugins = [];
  public SbarChartData: ChartDataSets[] = [
    { data: [0, 0, 0, 0], label: 'Male', stack: 'a' },
    { data: [0, 0, 0, 0], label: 'Female', stack: 'a' }
  ];




  public _GetGetMerchantCategories:any = [];
  public MerchantCategotries = [
    { data: [0], label: '' },
    { data: [0], label: '' },
    { data: [0], label: '' },
    { data: [0], label: '' },
  ]
  CategoryElement1: any = {};
  CategoryElement2: any = {};
  CategoryElement3: any = {};
  CategoryElement4: any = {};
  private pGetMerchantCategoriesData = {
    Task: 'getcusttopcategory',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    Offset: 0,
    Limit: 10,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetMerchantCategories() {

    this._HelperService.IsFormProcessing = true;
    this.pGetMerchantCategoriesData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pGetMerchantCategoriesData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.CustAnalytics, this.pGetMerchantCategoriesData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetGetMerchantCategories = _Response.Result as any;
          if (this._GetGetMerchantCategories != undefined && this._GetGetMerchantCategories.Data.length > 0) {
            this.MerchantCategotries =[];
            for(let i = 0 ;i < this._GetGetMerchantCategories.Data.length;i++){
            this.MerchantCategotries.push(
                { data: [this._GetGetMerchantCategories.Data[i].Count ? this._GetGetMerchantCategories.Data[i].Count : '--'], label: this._GetGetMerchantCategories.Data[i].Name ? this._GetGetMerchantCategories.Data[i].Name : '--' },
              )
            }
            if(this._GetGetMerchantCategories.Data.length < 4){
              let n = 4 - this._GetGetMerchantCategories.Data.length;
              for(let i = 0 ;i < n;i++){
                this.MerchantCategotries.push(
                    { data: [0], label: '--' },
                  )
                }
            }
            this.CategoryElement1 = this._GetGetMerchantCategories.Data[0];
            this.CategoryElement2 = this._GetGetMerchantCategories.Data[1];
            this.CategoryElement3 = this._GetGetMerchantCategories.Data[2];
            this.CategoryElement4 = this._GetGetMerchantCategories.Data[3];
          }
          return;
        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  SalesLabels: any;
  RewardLabels: any;

  public _GetGenderGroupCategories = []
  public GenderGroupCategotries = [
    { data: [0], label: '1M-2M' },
    { data: [0], label: '2M-3M' },
    { data: [0], label: '3M-4M ' },
    { data: [0], label: 'More Than 5' },
  ]
  GenderGroupElement1: any = {};
  GenderGroupElement2: any = {};
  GenderGroupElement3: any = {};
  GenderGroupElement4: any = {};
  private pGenderGroupData = {
    Task: 'getcustagegroupsbygender',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetGenderGroupCategories() {

    this._HelperService.IsFormProcessing = true;
    this.pGenderGroupData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pGenderGroupData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.CustAnalytics, this.pGenderGroupData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetGenderGroupCategories = _Response.Result as any;

          this.showDailyChart = false;
          this._ChangeDetectorRef.detectChanges();
          this.GenderGroupElement1 = this._GetGenderGroupCategories[0];
          this.GenderGroupElement2 = this._GetGenderGroupCategories[1];
          this.GenderGroupElement3 = this._GetGenderGroupCategories[2];
          this.GenderGroupElement4 = this._GetGenderGroupCategories[3];
          for (let i = 0; i < this._GetGenderGroupCategories.length; i++) {
            // this.barChartData[0].data[i] = this._LastSevenDaysData[i].Data.Total;
            this.SbarChartData[0].data[i] = this._GetGenderGroupCategories[i].Male;
            this.SbarChartData[1].data[i] = this._GetGenderGroupCategories[i].Female;
            this.SbarChartLabels[i] = this._GetGenderGroupCategories[i].Title;

          }


          this.showDailyChart = true;
          this._ChangeDetectorRef.detectChanges();

          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public TotalUsers: any = []
  public TotalUserCust = null
  private pTotalUsersData = {
    Task: 'getcuststatuscounts',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetTotalUsersOverview() {

    this._HelperService.IsFormProcessing = true;
    this.CustomerChart = true;
    this.pTotalUsersData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pTotalUsersData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.CustAnalytics, this.pTotalUsersData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.CustomerChart = false;
          this.GenderTotal = 0;
          this.VisitorTotal = 0;
          //#endregion
          this.showDailyChart = false;
          this._ChangeDetectorRef.detectChanges();
          this.TotalUsers = _Response.Result as any;
          this.TotalAppUsers[0] = this.TotalUsers.AppUsers;
          this.TotalAppUsers[1] = this.TotalUsers.NonAppUsers;
          this.TotalUserCust = this.TotalUsers.AppUsers + this.TotalUsers.NonAppUsers
          this.showDailyChart = true;
          this._ChangeDetectorRef.detectChanges();

          return;


        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  public SalesWise: any = []
  private pSalesWiseData = {
    Task: 'getcustspendrange',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetSalesWiseOverview() {

    this._HelperService.IsFormProcessing = true;
    this.LoadingChart = true;
    this.pSalesWiseData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pSalesWiseData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.CustAnalytics, this.pSalesWiseData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.LoadingChart = false;

          //#endregion

          this.SalesWise = _Response.Result as any;
          this.SpendRange = this.SalesWise;


          this.CustomerSpendRange = [];
          for (let index = 0; index < this.SpendRange.length; index++) {
            const element2 = this.SpendRange[index];

            this.SpenderTotal = this.SpenderTotal + element2.Count;
            this.CustomerSpendRange.push(element2.Count);

            switch (index) {
              case 0: this.SpendRange[index].text = 'tx-verve';

                break;
              case 1: this.SpendRange[index].text = 'tx-MasterCard';

                break;
              case 2: this.SpendRange[index].text = 'tx-teal';

                break;
              case 3: this.SpendRange[index].text = 'tx-primary';

                break;

              default:
                break;
            }

          }

          this.LoadingChart = false;

          //Percentage

          //ENd PErcentage

          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  public ReferalsWise: any = {};
  public TotalReferalsWise: any = {};

  private pReferalsData = {
    Task: 'getcustreferralcounts',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetReferalsOverview() {

    this._HelperService.IsFormProcessing = true;
    this.CustomerReferrals = true;
    this.pReferalsData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pReferalsData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.CustAnalytics, this.pReferalsData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.CustomerReferrals = false;
          //#endregion
          this.showDailyChart = false;
          this._ChangeDetectorRef.detectChanges();
          this.ReferalsWise = _Response.Result as any;
          this.TotalReferalsUsers[0] = this.ReferalsWise.Customers;
          this.TotalReferalsUsers[1] = this.ReferalsWise.Merchants;
          this.TotalReferalsUsers[2] = this.ReferalsWise.Partners;
          this.TotalReferalsWise = this.ReferalsWise.Customers + this.ReferalsWise.Merchants + this.ReferalsWise.Partners
          this.showDailyChart = true;
          this._ChangeDetectorRef.detectChanges();
          this.CustomerReferrals = false;
          return;



        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public PaymentsMode: any = {};
  private pPaymenttsModeData = {
    Task: 'getcustpaymentmodes',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetPaymentsModeOverview() {

    this._HelperService.IsFormProcessing = true;
    this.LoadingChart = true;
    this.pPaymenttsModeData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pPaymenttsModeData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.CustAnalytics, this.pPaymenttsModeData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.LoadingChart = false;
          //#endregion
          this.showDailyChart = false;
          this._ChangeDetectorRef.detectChanges();
          // console.log(_Response.Result,"payment modes response");

          
          this.PaymentsMode = _Response.Result as any;
          this.PaymentModes[0] = this.PaymentsMode.CardTransactions;
          this.PaymentModes[1] = this.PaymentsMode.CashTransactions;

        this.PaymentsModeStatus[0]=this.PaymentsMode.CardTransactions;
        this.PaymentsModeStatus[1]=this.PaymentsMode.CashTransactions;
          this.showDailyChart = true;
          this._ChangeDetectorRef.detectChanges();
          this.LoadingChart = false;




        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }






  //#region Yearly Sales Report 

  public YearlylineChartData: ChartDataSets[] = [
    { data: [], label: 'Current Year' },
    { data: [], borderDash: [10, 5], label: 'Compared Year' },
  ];
  public YearlylineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];
  public YearlylineChartLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
  showYearlyChart = true;

  public _Yearly: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,

    CompareSalesTotal: 0,
    ActualSalesTotal: 0,

    SalesAmountDifference: 0
  }


  public _YearlySalesReportReset(): void {


  }


  //#endregion

  //#region Sales History General Method 

  private pData = {
    Task: 'getdealhistory',
    StartDate: null,
    EndDate: null,
    // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };

  GetSalesReport(StartDateTime, EndDateTime, Data: ODealData[], Type, LineType: string) {


    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);
    this.pData.Type = Type;

    if (Type == this.Types.month) {
      this.pData.Type = this.Types.day;
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as ODealData[];
          // Data = [
          //   {
          //     "Title": "2020-11",
          //     "Hour": 0,
          //     "Year": 2020,
          //     "Month": 11,
          //     "Customer": 1,
          //     "Amount": 184.0,
          //     "Total": 8,
          //     "Unused": 8,
          //     "Used": 33,
          //     "Expired": 0
          //   },
          //   {
          //     "Title": "2020-11",
          //     "Hour": 0,
          //     "Year": 2020,
          //     "Month": 11,
          //     "Customer": 1,
          //     "Amount": 184.0,
          //     "Total": 8,
          //     "Unused": 8,
          //     "Used": 33,
          //     "Expired": 0
          //   }
          // ];

          var TempArrayTotal = [];
          var TotalSum = 0;

          var TempArrayUnused = [];
          var UnusedSum = 0;

          var TempArrayUsed = [];
          var UsedSum = 0;

          var TempArrayExpired = [];
          var ExpiredSum = 0;

          var TempArrayAmount = [];
          var AmountSum = 0;

          var TempArrayCustomer = [];
          var CustomerSum = 0;

          if (Type == this._HelperService.AppConfig.GraphTypes.month) {
            var MonthAllDays: any[] = this._HelperService.CalculateIntermediateDate(cloneDeep(StartDateTime),
              cloneDeep(EndDateTime));
            for (let index = 0; index < MonthAllDays.length; index++) {
              const element = MonthAllDays[index];
              var RData: ODealData = Data.find(x => moment(x.Title, 'DD-MM-YYYY').format('DD MMM') == element);
              if (RData) {
                TempArrayTotal.push(RData.Total);
                TempArrayUnused.push(RData.Unused);
                TempArrayUsed.push(RData.Used);
                TempArrayExpired.push(RData.Expired);
                TempArrayAmount.push(RData.Amount);
                TempArrayCustomer.push(RData.Customer);

                TotalSum = TotalSum + element.Total;
                UnusedSum = UnusedSum + element.Unused;
                UsedSum = UsedSum + element.Used;
                ExpiredSum = ExpiredSum + element.Expired;
                AmountSum = AmountSum + element.Amount;
                CustomerSum = CustomerSum + element.Customer;
              } else {
                TempArrayTotal.push(0);
                TempArrayUnused.push(0);
                TempArrayUsed.push(0);
                TempArrayExpired.push(0);
                TempArrayAmount.push(0);
                TempArrayCustomer.push(0);
              }
            }

            this.showSaleChart = false;
            this.showRevenueChart = false;
            this.showRedeemChart = false;
            this.showYearlyChart = false;
            this._ChangeDetectorRef.detectChanges();




            this.SalelineChartData[0].data = TempArrayTotal;
            this.RedeemlineChartData[0].data = TempArrayUsed;
            this.RevenuelineChartData[0].data = TempArrayUnused;

            this._Sale.ActualSalesAmount = TotalSum;
            this._Redeem.ActualSalesAmount = UnusedSum;
            this._Revenue.ActualSalesAmount = UsedSum;
            this._Customer.TotalCustomers = CustomerSum;

            this.SalelineChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate));
            this.RevenuebarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate));
            this.RedeemlineChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate));

            this.showSaleChart = true;
            this.showRevenueChart = true;
            this.showRedeemChart = true;
            this.showYearlyChart = true;
            this._ChangeDetectorRef.detectChanges();


          } else {
            for (let index = 0; index < 12; index++) {
              var RData: ODealData = Data.find(x => x['Month'] == index + 1);
              if (RData) {
                TempArrayTotal[index] = (RData.Total);
                TempArrayUnused[index] = (RData.Unused);
                TempArrayUsed[index] = (RData.Used);
                TempArrayExpired[index] = (RData.Expired);
                TempArrayAmount[index] = (RData.Amount);
                TempArrayCustomer[index] = (RData.Customer);

                TotalSum = TotalSum + RData.Total;
                UnusedSum = UnusedSum + RData.Unused;
                UsedSum = UsedSum + RData.Used;
                ExpiredSum = ExpiredSum + RData.Expired;
                AmountSum = AmountSum + RData.Amount;

              } else {
                TempArrayTotal[index] = (0);
                TempArrayUnused[index] = (0);
                TempArrayUsed[index] = (0);
                TempArrayExpired[index] = (0);
                TempArrayAmount[index] = (0);
                TempArrayCustomer[index] = (0);
              }
            }

            this.showSaleChart = false;
            this.showRevenueChart = false;
            this.showRedeemChart = false;
            this.showYearlyChart = false;
            this._ChangeDetectorRef.detectChanges();

            this.SalelineChartData[0].data = TempArrayTotal;
            this.RedeemlineChartData[0].data = TempArrayUsed;
            this.RevenuelineChartData[0].data = TempArrayUnused;

            this._Sale.ActualSalesAmount = TotalSum;
            this._Redeem.ActualSalesAmount = UnusedSum;
            this._Revenue.ActualSalesAmount = UsedSum;

            this.SalelineChartLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
            this.RevenuebarChartLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
            this.RedeemlineChartLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];

            this.showSaleChart = true;
            this.showRevenueChart = true;
            this.showRedeemChart = true;
            this.showYearlyChart = true;
            this._ChangeDetectorRef.detectChanges();
          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  GetSalesReportSale(StartDateTime, EndDateTime, Data: ODealData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);


    this.pData.Type = this.Types.hour;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {

          var DataHourly = _Response.Result as ODealData[];

          // DataHourly = [
          //   {
          //     "Title": "2020-11",
          //     "Hour": 0,
          //     "Year": 2020,
          //     "Month": 11,
          //     "Customer": 1,
          //     "Amount": 184.0,
          //     "Total": 8,
          //     "Unused": 8,
          //     "Used": 33,
          //     "Expired": 0
          //   },
          //   {
          //     "Title": "2020-11",
          //     "Hour": 1,
          //     "Year": 2020,
          //     "Month": 11,
          //     "Customer": 1,
          //     "Amount": 184.0,
          //     "Total": 8,
          //     "Unused": 8,
          //     "Used": 33,
          //     "Expired": 0
          //   }
          // ];

          var TempArrayTotal = [];
          var TotalSum = 0;

          var TempArrayUnused = [];
          var UnusedSum = 0;

          var TempArrayUsed = [];
          var UsedSum = 0;

          var TempArrayExpired = [];
          var ExpiredSum = 0;

          var TempArrayAmount = [];
          var AmountSum = 0;

          var TempArrayAmount = [];
          var AmountSum = 0;

          var TempArrayCustomer = [];
          var CustomerSum = 0;

          // var Heigest: ODealData = {
          //   Hour: null,
          //   HourAmPm: '0:00 AM',
          //   HourAmPmNext: '1:00 AM',
          //   TotalTransaction: 0.0,
          //   TotalInvoiceAmount: 0.0
          // };

          // var Lowest: ODealData = {
          //   Hour: null,
          //   HourAmPm: '0:00 AM',
          //   HourAmPmNext: '1:00 AM',
          //   TotalTransaction: 0.0,
          //   TotalInvoiceAmount: 0.0
          // };


          for (let index = 0; index < 24; index++) {
            var RData: ODealData = DataHourly.find(x => x.Hour == index);

            if (RData != undefined && RData != null) {

              TempArrayTotal.push(RData.Total);
              TempArrayUnused.push(RData.Unused);
              TempArrayUsed.push(RData.Used);
              TempArrayExpired.push(RData.Expired);
              TempArrayAmount.push(RData.Amount);
              TempArrayCustomer.push(RData.Customer);

              const element: ODealData = RData;

              TotalSum = TotalSum + element.Total;
              UnusedSum = UnusedSum + element.Unused;
              UsedSum = UsedSum + element.Used;
              ExpiredSum = ExpiredSum + element.Expired;
              AmountSum = AmountSum + element.Amount;
              CustomerSum = CustomerSum + element.Customer;
            }
            else {
              TempArrayTotal.push(0);
              TempArrayUnused.push(0);
              TempArrayUsed.push(0);
              TempArrayExpired.push(0);
              TempArrayAmount.push(0);
              TempArrayCustomer.push(0);
            }

          }



          if (Type == this._HelperService.AppConfig.GraphTypes.hour) {

            this.showSaleChart = false;
            this.showRevenueChart = false;
            this.showRedeemChart = false;
            this.showYearlyChart = false;
            this._ChangeDetectorRef.detectChanges();

            this.SalelineChartData[0].data = TempArrayTotal;
            this.RedeemlineChartData[0].data = TempArrayUsed;
            this.RevenuelineChartData[0].data = TempArrayUnused;

            this._Sale.ActualSalesAmount = TotalSum;
            this._Redeem.ActualSalesAmount = UnusedSum;
            this._Revenue.ActualSalesAmount = UsedSum;
            this._Customer.TotalCustomers = CustomerSum;

            this.SalelineChartLabels = ['00:00', 'O1:00', '02:00', 'O3:00', '04:00', 'O5:00', '06:00', 'O7:00', '08:00', '09:00', '10:00', '11:00',
              '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
            this.RevenuebarChartLabels = ['00:00', 'O1:00', '02:00', 'O3:00', '04:00', 'O5:00', '06:00', 'O7:00', '08:00', '09:00', '10:00', '11:00',
              '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
            this.RedeemlineChartLabels = ['00:00', 'O1:00', '02:00', 'O3:00', '04:00', 'O5:00', '06:00', 'O7:00', '08:00', '09:00', '10:00', '11:00',
              '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];

            this.showSaleChart = true;
            this.showRevenueChart = true;
            this.showRedeemChart = true;
            this.showYearlyChart = true;
            this._ChangeDetectorRef.detectChanges();
          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  GetSalesReportRedeem(StartDateTime, EndDateTime, Data: ODealData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    this.pData.Type = Type;

    if (Type == this.Types.day) {
      this.pData.Type = 'week';
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as ODealData[];

          // Data = [
          //   {
          //     "Title": "2020-11",
          //     "Hour": 0,
          //     "Year": 2020,
          //     "Month": 11,
          //     "Customer": 1,
          //     "Amount": 184.0,
          //     "Total": 8,
          //     "Unused": 8,
          //     "Used": 33,
          //     "Expired": 0
          //   },
          //   {
          //     "Title": "2020-11",
          //     "Hour": 0,
          //     "Year": 2020,
          //     "Month": 11,
          //     "Customer": 1,
          //     "Amount": 184.0,
          //     "Total": 8,
          //     "Unused": 8,
          //     "Used": 33,
          //     "Expired": 0
          //   }
          // ];

          var TempArrayTotal = [];
          var TotalSum = 0;

          var TempArrayUnused = [];
          var UnusedSum = 0;

          var TempArrayUsed = [];
          var UsedSum = 0;

          var TempArrayExpired = [];
          var ExpiredSum = 0;

          var TempArrayAmount = [];
          var AmountSum = 0;

          var TempArrayCustomer = [];
          var CustomerSum = 0;

          var TempColorArray = ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'];

          var Heigest = Data[0];
          var HeigestIndex = 0;
          var Lowest = Data[0];
          var LowestIndex = 0;

          var DataSale = _Response.Result as ODealData[];

          for (let index = 0; index < 7; index++) {

            var weekday = '';
            switch (index) {
              case 0: weekday = 'Sunday';
                break;
              case 1: weekday = 'Monday';

                break;
              case 2: weekday = 'Tuesday';

                break;
              case 3: weekday = 'Wednesday';

                break;
              case 4: weekday = 'Thursday';

                break;
              case 5: weekday = 'Friday';

                break;
              case 6: weekday = 'Saturday';
                break;

              default:
                break;
            }

            var RData: ODealData = DataSale.find(x => x['WeekDay'] == weekday);
            if (RData != undefined && RData != null) {

              TempArrayTotal.push(RData.Total);
              TempArrayUnused.push(RData.Unused);
              TempArrayUsed.push(RData.Used);
              TempArrayExpired.push(RData.Expired);
              TempArrayAmount.push(RData.Amount);
              TempArrayCustomer.push(RData.Customer);

              const element: ODealData = RData;

              TotalSum = TotalSum + element.Total;
              UnusedSum = UnusedSum + element.Unused;
              UsedSum = UsedSum + element.Used;
              ExpiredSum = ExpiredSum + element.Expired;
              AmountSum = AmountSum + element.Amount;
              CustomerSum = CustomerSum + element.Customer;

              // if (Heigest.TotalInvoiceAmount < element.TotalInvoiceAmount) {
              //   Heigest = element;
              //   HeigestIndex = index;
              // }

              // if (Lowest.TotalInvoiceAmount > element.TotalInvoiceAmount) {
              //   Lowest = element;
              //   LowestIndex = index;
              // }

              TotalSum = TotalSum + element.Total;
              UnusedSum = UnusedSum + element.Unused;
              UsedSum = UsedSum + element.Used;
              ExpiredSum = ExpiredSum + element.Expired;
              AmountSum = AmountSum + element.Amount;
              CustomerSum = CustomerSum + element.Customer;
            }
            else {
              TempArrayTotal.push(0);
              TempArrayUnused.push(0);
              TempArrayUsed.push(0);
              TempArrayExpired.push(0);
              TempArrayAmount.push(0);
              TempArrayCustomer.push(0);
            }

          }

          if (Type == this._HelperService.AppConfig.GraphTypes.week) {

            this.showSaleChart = false;
            this.showRevenueChart = false;
            this.showRedeemChart = false;
            this.showYearlyChart = false;
            this._ChangeDetectorRef.detectChanges();

            this.SalelineChartData[0].data = TempArrayTotal;
            this.RedeemlineChartData[0].data = TempArrayUsed;
            this.RevenuelineChartData[0].data = TempArrayUnused;

            this._Sale.ActualSalesAmount = TotalSum;
            this._Redeem.ActualSalesAmount = UnusedSum;
            this._Revenue.ActualSalesAmount = UsedSum;
            this._Customer.TotalCustomers = CustomerSum;

            this.SalelineChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
            this.RevenuebarChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
            this.RedeemlineChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

            this.showSaleChart = true;
            this.showRevenueChart = true;
            this.showRedeemChart = true;
            this.showYearlyChart = true;
            this._ChangeDetectorRef.detectChanges();
          }
          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }



  htmltoPDF() {
    // this._HelperService.OpenModal('ModalReport');
    setTimeout(() => {
      this.StartDownload();
    }, 1000);
  }

  StartDownload() {
    html2canvas(document.querySelector("#customer")).then(canvas => {
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;
      const contentDataURL = canvas.toDataURL('image/png');
      let pdf1 = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;
      pdf1.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf1.save('Customer Data' + '.pdf'); // Generated PDF   
    });
  }

  //#endregion

  computePerc(num: OSalesTrend): any {
    if (num.CompareSalesAmount == 0) {
      return '100 %';
    }
    if (num.ActualSalesAmount > num.CompareSalesAmount) {
      return Math.round(((num.ActualSalesAmount - num.CompareSalesAmount) / num.CompareSalesAmount) * 100) + ' %';
    } else if (num.ActualSalesAmount < num.CompareSalesAmount) {
      return Math.round(((num.CompareSalesAmount - num.ActualSalesAmount) / num.CompareSalesAmount) * 100) + ' %';
    } else {
      return '0 %';
    }
  }

  computeFlag(num: OSalesTrend): number {
    if (num.ActualSalesAmount > num.CompareSalesAmount) {
      return 1;
    } else if (num.ActualSalesAmount < num.CompareSalesAmount) {
      return -1;
    } else {
      return 0;
    }
  }

  getAbsolute(num: number): any {
    // return Math.abs(num);
    return num;
  }



  hideSalePicker: boolean = true;
  hideRedeemPicker: boolean = true;
  hideRevenuePicker: boolean = true;
  hideYearlyPicker: boolean = true;

  ShowHideCalendar(type: string) {
    switch (type) {
      case this._HelperService.AppConfig.DatePickerTypes.hour: {
        this.hideSalePicker = !this.hideSalePicker;
      }

        break;
      case this._HelperService.AppConfig.DatePickerTypes.week: {
        this.hideRedeemPicker = !this.hideRedeemPicker;
      }

        break;
      case this._HelperService.AppConfig.DatePickerTypes.month: {
        this.hideRevenuePicker = !this.hideRevenuePicker;
      }

        break;
      case this._HelperService.AppConfig.DatePickerTypes.year: {
        this.hideYearlyPicker = !this.hideYearlyPicker;
      }

        break;

      default:
        break;
    }
  }

  CloseRowModal(): void { }
  TUC_class:any;
  Customer_TUC_Class(){

    this.TUC_class={
      Platinum: 56,
      Gold: 24,
      Dimand: 40,
      Silver: 5,
      
  }
    this.CustomerTUC_Class = [
      { data: [this.TUC_class.Platinum], label: "Platinum" },
      { data: [this.TUC_class.Gold], label: "Gold" },
      { data: [this.TUC_class.Dimand], label: "Dimand" },
      { data: [this.TUC_class.Silver], label: "Silver"  },
    ];
  }
}


export class OAccountOverview {
  public TerminalStatus?: any;
  public TotalTerminals?: number;
  public TotalSale?: number;
  public AverageTransactionAmount?: number;
  public TotalTransactions?: number;
  public ActiveTerminalsPerc?: number;
  public IdleTerminalsPerc?: number;
  public DeadTerminalsPerc?: number;
  public UnusedTerminalsPerc?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public Idle?: number;
  public Total?: number;

  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
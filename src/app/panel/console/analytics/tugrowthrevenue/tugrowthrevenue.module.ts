import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { DatePipe } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { ChartsModule } from 'ng2-charts';

import { TransactionReportComponent } from './transactionreport/transactionreport.component';
import { TuGrowthRevenueComponent } from './tugrowthrevenue.component';
import { RedeemReportComponent } from './redeemreport/redeemreport.component';
import { RewardReportComponent } from './rewardreport/rewardreport.component';
import { RevenueReportComponent } from './revenuereport/revenuereport.component';
import { SalesReportComponent } from './salesreport/salesreport.component';


import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { AgmCoreModule } from '@agm/core';
import { AgmOverlays } from 'agm-overlays';
import { MerchantguardGuard } from 'src/app/service/guard/merchantguard.guard';
const routes: Routes = [
    { path: '', canActivate:[MerchantguardGuard],component: TuGrowthRevenueComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TuGrowthRevenueRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        ChartsModule,
        TuGrowthRevenueRoutingModule,
        AgmOverlays,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    declarations: [
        TuGrowthRevenueComponent,
        RedeemReportComponent,
        RewardReportComponent,
        RevenueReportComponent,
        TransactionReportComponent,
        SalesReportComponent
    ]
})
export class TuGrowthRevenueModule {
} 

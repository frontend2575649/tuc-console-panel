import { ChangeDetectorRef, Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ChartDataSets } from 'chart.js';
import * as Feather from 'feather-icons';
import * as cloneDeep from 'lodash/cloneDeep';
import { BaseChartDirective, Color } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OResponse, OSalesTrend, ODealData, OSelect } from '../../../../service/service';
declare var moment: any;
declare var $: any;
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
@Component({
  selector: "tugrowthrevenue",
  templateUrl: "./tugrowthrevenue.component.html",
  styles: [
    `
      agm-map {
        height: 300px;
      }
    `
  ]
})
export class TuGrowthRevenueComponent implements OnInit {

  public _Sale: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    CompareSalesTotal: 0,
    ActualSalesTotal: 0,
    SalesAmountDifference: 0
  }

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {

  }

  //#endregion
  ngOnInit() {
    this._HelperService.FullContainer = false;
    Feather.replace();
    //start time and end time for overview

    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];

      this.InitializeDates();

    });


  }


  RewardList(){
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.RewardHistory
     ]);
  }
  ReedeemList(){
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.RedeemHistory
     ]);
  }
  TransactionList(){
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.SalesHistory
     ]);
  }
  htmltoPDF() {
    this._HelperService.OpenModal('ModalReport');
    setTimeout(() => {
        this.StartDownload();
    }, 1000);
}

StartDownload() {
    html2canvas(document.querySelector("#revenue")).then(canvas => {
        var imgWidth = 208;
        var pageHeight = 295;
        var imgHeight = canvas.height * imgWidth / canvas.width;
        var heightLeft = imgHeight;
        const contentDataURL = canvas.toDataURL('image/png');
        let pdf1 = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
        var position = 0;
        pdf1.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
        pdf1.save('Revenue' + '.pdf'); // Generated PDF   
    });
}

  InitializeDates(): void {

    // this._Sale.ActualStartDate = moment().startOf('day');
    // this._Sale.ActualEndDate = moment().endOf('day');

    // this._Sale.CompareStartDate = moment().subtract(1, 'day').startOf('day');
    // this._Sale.CompareEndDate = moment().subtract(1, 'day').endOf('day');
    this._Sale.ActualStartDate = moment().subtract(6, "days").startOf('day');
    this._Sale.ActualEndDate = moment().endOf('day');
    this._Sale.CompareStartDate = moment().subtract(13, "days").endOf('day');
    this._Sale.CompareEndDate = moment().subtract(7, "days").startOf('day');

    // var difference = moment().diff(moment(), 'days');
    var difference = moment().endOf('day').diff(moment().subtract(5,'days').startOf('day'), 'days');

    this.GetSalesOverview();

  }

  public DateSelected = moment().startOf("day");
  //#region DateChangeHandler
  DateChanged(event: any, Type: any): void {

    this._HelperService.DateChanged.next(event);

    var ev: any = cloneDeep(event);
    this.DateSelected = moment(ev.start).startOf("day");

    var difference = moment(ev.end).diff(moment(ev.start), 'days');
    if (difference <= 5) {
      // this._Sale.ActualStartDate = moment(ev.start).startOf("day");
      // this._Sale.ActualEndDate = moment(ev.end).endOf("day");

      // this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'day').startOf("day");
      // this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'day').endOf("day");

      this._Sale.ActualStartDate = moment(ev.start).subtract(6, "days").startOf('day');;
      this._Sale.ActualEndDate = moment(ev.end).endOf("day");

      this._Sale.CompareStartDate = moment(ev.start).subtract(13, "days").endOf('day');
      this._Sale.CompareEndDate = moment(ev.end).subtract(7, "days").startOf('day');

    } else if (difference <= 7) {
      // this._Sale.ActualStartDate = moment(ev.start).startOf('week').startOf('day');
      // this._Sale.ActualEndDate = moment(ev.end).endOf('week').endOf('day');

      // this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'week').startOf('week').startOf('day');
      // this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'week').endOf('week').endOf('day');

      this._Sale.ActualStartDate = moment(ev.start).startOf('day');
      this._Sale.ActualEndDate = moment(ev.end).endOf('day');

      this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'week').startOf('day');
      this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'week').endOf('day');

    } else if (difference <= 30) {
      this._Sale.ActualStartDate = moment(ev.start).startOf('month').startOf('day');
      this._Sale.ActualEndDate = moment(event.end).endOf('month').endOf('day');

      this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'months').startOf('month').startOf('day');
      this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'months').endOf('month').endOf('day');

    } else {
      this._Sale.ActualStartDate = moment(ev.start).startOf('year').startOf('day');
      this._Sale.ActualEndDate = moment(event.end).endOf('year').endOf('day');

      this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'year').startOf('year').startOf('day');
      this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'year').endOf('year').endOf('day');

      

    }

    this.GetSalesOverview();

  }
  //#endregion

  //#region Get Overview 

  public _GetoverviewSummary: any = {};
  private pGrowthData = {
    Task: 'getrevenueoverview',
    StartDate: null,
    EndDate: null,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetSalesOverview() {

    this._HelperService.IsFormProcessing = true;
    this.pGrowthData.StartDate = this._HelperService.DateInUTC(this._Sale.ActualStartDate);
    this.pGrowthData.EndDate = this._HelperService.DateInUTC(this._Sale.ActualEndDate);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analytics, this.pGrowthData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {

          // this._GetoverviewSummary = {};
          this._GetoverviewSummary = _Response.Result as any;

          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  //#endregion

  //#region Purchase Overview 

  public _GetPurchaseoverviewSummary: any = {};
  private pDealPurchaseData = {
    Task: 'getdealspurchaseoverview',
    StartDate: null,
    EndDate: null,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetDealPurchaseOverview() {

    this._HelperService.IsFormProcessing = true;

    this.pDealPurchaseData.StartDate = this._HelperService.DateInUTC(this._Sale.ActualStartDate);
    this.pDealPurchaseData.EndDate = this._HelperService.DateInUTC(this._Sale.ActualEndDate);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analytics, this.pDealPurchaseData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetPurchaseoverviewSummary = _Response.Result as any;
          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  //#endregion

  CloseRowModal(): void { }
}


export class OAccountOverview {
  public TerminalStatus?: any;
  public TotalTerminals?: number;
  public TotalSale?: number;
  public AverageTransactionAmount?: number;
  public TotalTransactions?: number;
  public ActiveTerminalsPerc?: number;
  public IdleTerminalsPerc?: number;
  public DeadTerminalsPerc?: number;
  public UnusedTerminalsPerc?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public Idle?: number;
  public Total?: number;

  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
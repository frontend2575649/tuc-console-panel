import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import Highcharts from "highcharts";
import Exporting from "highcharts/modules/exporting";
import funnel from "highcharts/modules/funnel";
import { BaseChartDirective } from 'ng2-charts';
import { Observable, Subscription } from 'rxjs';
import { DataHelperService, HelperService, ODealData, OResponse, OSalesTrend, OSalesTrendData, OSalesTrendDataHourly } from '../../../../../service/service';
declare var moment: any;
declare var $: any;
import { ChartDataSets } from 'chart.js';
import * as cloneDeep from 'lodash/cloneDeep';

Exporting(Highcharts);
funnel(Highcharts);

@Component({
  selector: "tucustomersoverviews",
  templateUrl: "./tucustomersoverviews.component.html",
  styles: [
    `
      agm-map {
        height: 300px;
      }
    `
  ]
})
export class TuCustomerOverviewOComponent implements OnInit {
  public LoadingChart: boolean = true;

  @ViewChild("container", { read: ElementRef }) container: ElementRef;

  SalelineChartLabels = [];
  public StoreLabel = []
  public PostHorizontalData = [
    { data: [80], label: 'Active' },
    { data: [72], label: 'In-Active' },
    { data: [65], label: 'Dead ' },
    { data: [75], label: 'Idle' },
  ];

  lastdaytext = "LAST DAY";
  lastweektext = "LAST WEEK";
  lastweekCustom = "LAST WEEK";
  lastmonthtext = "LAST MONTH";
  lastyeartext = "LAST YEAR";

  Types: any = {
    year: 'year',
    month: 'month',
    week: 'week',
    day: 'day',
    hour: 'hour'
  }

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];
  public TodayDate: any;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this.TodayDate = this._HelperService.GetDateTime(new Date());
  }
  StartDateTime = null;

  EndDateTime = null;
  //#endregion
  public _DateChangeSubscription: Subscription = null;


  firstTime: boolean = true;

  ngOnInit() {

    this._HelperService.FullContainer = false;
    // this.StartDateTime = moment().startOf('week').startOf('day');
    // this.EndDateTime= moment().endOf('week').endOf('day');
    // this.LoadData();

    Feather.replace();
    this.InitializeDates();


    this._DateChangeSubscription = this._HelperService.DateChanged.subscribe(value => {
      if (!this.firstTime) {
        this.DateChanged(value, null);
      } else {
        //  this.InitializeDates();
        this.firstTime = false;
      }
    });

  }

  ngOnDestroy(): void {
    this._DateChangeSubscription.unsubscribe();
  }
  CustomerRoute() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Analytics.CustomerAnalytics
    ]);

  }
  WalletCustomerRoute() {
    this._Router.navigate([
      "console/" + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AccountSettings.CustomerWallets
    ]);

  }

  public _Sale: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    CompareSalesTotal: 0,
    ActualSalesTotal: 0,
    SalesAmountDifference: 0
  }


  InitializeDates(): void {

    this._Sale.ActualStartDate = moment().startOf('week').startOf('day');
    this._Sale.ActualEndDate = moment().endOf('week').endOf('day');

    // this._Sale.CompareStartDate = moment().subtract(1, 'week').startOf('').startOf('day');
    // this._Sale.CompareEndDate = moment().subtract(1, 'week').endOf('day');

    var difference = moment().endOf('week').endOf('day').diff(moment().startOf('week').startOf('day'), 'days');
    var differenceWeek = moment().endOf('week').endOf('day').diff(moment().startOf('week').startOf('day'), 'days');

    if (difference <= 1) {
      this._SaleDailyReportGetActualData(this.Types.hour);
    } else if (difference <= 7) {
      this._SaleDailyReportGetActualData(this.Types.week);
    } else if (difference <= 30) {
      this._SaleDailyReportGetActualData(this.Types.month);
    } else {
      this._SaleDailyReportGetActualData(this.Types.year);
    }

  }


  public DateSelected = moment().startOf("day");
  //#region DateChangeHandler
  DateChanged(event: any, Type: any): void {
    this.VisitslineChartData[0].data = [];
    var ev: any = cloneDeep(event);
    this.DateSelected = moment(ev.start).startOf("day");

    var difference = moment(ev.start).endOf('month').endOf('day').diff(moment(ev.start).startOf('month').startOf('day'), 'days');
    var differenceWeek = moment().endOf('week').endOf('day').diff(moment().startOf('week').startOf('day'), 'days');
    if (differenceWeek <= 7) {
      this._Sale.ActualStartDate = moment(ev.start).startOf('week').startOf('day');
      this._Sale.ActualEndDate = moment(ev.end).endOf('week').endOf('day');

      this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'week').startOf('week').startOf('day');
      this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'week').endOf('week').endOf('day');

      this._SaleDailyReportGetActualData(this.Types.week);
    }

    if (difference <= 1) {
      this._Sale.ActualStartDate = moment(ev.start).startOf("day");
      this._Sale.ActualEndDate = moment(ev.end).endOf("day");

      this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'day').startOf("day");
      this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'day').endOf("day");

      this._SaleDailyReportGetActualData(this.Types.hour);
    } else if (difference <= 7) {
      this._Sale.ActualStartDate = moment(ev.start).startOf('week').startOf('day');
      this._Sale.ActualEndDate = moment(ev.end).endOf('week').endOf('day');

      this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'week').startOf('week').startOf('day');
      this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'week').endOf('week').endOf('day');

      this._SaleDailyReportGetActualData(this.Types.week);
    } else if (difference <= 30) {
      this._Sale.ActualStartDate = moment(ev.start).startOf('month').startOf('day');
      this._Sale.ActualEndDate = moment(event.end).endOf('month').endOf('day');

      this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'months').startOf('month').startOf('day');
      this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'months').endOf('month').endOf('day');

      this.SalelineChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Sale.ActualStartDate), moment(this._Sale.ActualEndDate).endOf('month').endOf('day'));

      this._SaleDailyReportGetActualData(this.Types.month);
    } else {
      this._Sale.ActualStartDate = moment(ev.start).startOf('year').startOf('day');
      this._Sale.ActualEndDate = moment(event.end).endOf('year').endOf('day');

      this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'year').startOf('year').startOf('day');
      this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'year').endOf('year').endOf('day');

      this._SaleDailyReportGetActualData(this.Types.year);
    }

    // this.RefreshDateLabels();

  }

  public _SaleDailyReportGetActualData(Type): void {

    this.SalelineChartData[0].data = [];
    // this.SalelineChartData[1].data = [];

    if (Type == this.Types.hour) {
      this.GettodayOverviewSale(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GettodayOverviewSale(this._Sale.CompareStartDate, this._Sale.CompareEndDate, this._Sale.ActualData, Type, 'compare');
    } else if (Type == this.Types.week) {
      this.GetSalesReportDaily(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetLoyalityOverviewSale(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetLoyalityOverview(this._Sale.ActualStartDate, this._Sale.ActualStartDate, this._Sale.ActualData, Type, 'actual');
      // this.GetSalesReportDaily(this._Sale.CompareStartDate, this._Sale.CompareEndDate, this._Sale.ActualData, Type, 'compare');
    } else if (Type == this.Types.month || Type == this.Types.year) {
      this.GetLoyalityOverviewSale(this._Sale.ActualStartDate, this._Sale.ActualStartDate, this._Sale.ActualData, Type, 'actual');
      this.GetLoyalityOverview(this._Sale.ActualStartDate, this._Sale.ActualStartDate, this._Sale.ActualData, Type, 'actual');
    }

    //this.GetSalesReport(this._Yearly.ActualStartDate, this._Yearly.ActualEndDate, this._Yearly.ActualData, this.Types.year, 'actual');

  }


  // LoadData() {
  //   this.GetLoyalityOverview(this.StartDateTime, this.EndDateTime)
  //   this.GetLoyalityOverviewSale(this.StartDateTime, this.EndDateTime);
  //   this.GettodayOverviewSale(this.StartDateTime, this.EndDateTime)
  //   this.RefreshTerminalStatusCount();
  //   this._DailySalesReportGetActualData();
  //   this._VisitsSalesReportGetActualData();

  // }

  RefreshTerminalStatusCount() {
    this._HelperService.GetTerminalStatusCount(this._HelperService.UserAccount.AccountId, this._HelperService.AppConfig.AccountType.MerchantId, 0, 0);
  }


  MerchantList() {
    this._Router.navigate([
      'console/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AccountSettings.Merchants,

    ]);
  }

  public LoyalityOverview: any =
    {


    }
  private LoyalityOvereview = {
    Task: 'getcustomerscount',
    BaseDate: null,
    EndDate: null,
    // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };

  GetLoyalityOverview(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {


    this._HelperService.IsFormProcessing = true;

    this.LoyalityOvereview.BaseDate = this._HelperService.DateInUTC(StartDateTime);


    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analac, this.LoyalityOvereview);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.LoyalityOverview = _Response.Result;


        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public SalesOverview: any =
    {


    }
  private pData = {
    Task: 'getcustomerbalanceoverview',
    BaseDate: null,
    // EndDate: null,
    // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };

  GetLoyalityOverviewSale(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pData.BaseDate = this._HelperService.DateInUTC(StartDateTime);
    // this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);
    // this.pData.Type = this.Types.hour;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analac, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this.SalesOverview = _Response.Result as ODealData[];
       


        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public todayDataSalesOverview: any =
    {


    }
  public NewMerchants: any = []
  public LowBalance: any = []
  public NewPos: any = []
  private ptodayOverviewData = {
    Task: 'gettodaysoverview',
    StartDate: null,
    EndDate: null,
    // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };

  GettodayOverviewSale(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.ptodayOverviewData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.ptodayOverviewData.EndDate = this._HelperService.DateInUTC(EndDateTime);


    // this.ptodayOverviewData.Type = this.Types.hour;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analytics, this.ptodayOverviewData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this.todayDataSalesOverview = _Response.Result;
          this.NewMerchants = this.todayDataSalesOverview.NewMerchants;
          this.LowBalance = this.todayDataSalesOverview.LowBalancewMerchants;
          this.NewPos = this.todayDataSalesOverview.NewTerminals;


        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  MerchantRoute(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveMerchant,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveMerchantReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveMerchantReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.RewardPercentage,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);

  }

  TerminalRoute(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveTerminal,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveTerminalReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveTerminalReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Terminal.SalesHistory,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);
  }


  GetLoyalityOverviewRedeem(StartDateTime, EndDateTime, Data: ODealData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    // this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    // this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    this.pData.Type = Type;

    if (Type == this.Types.day) {
      this.pData.Type = 'week';
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as ODealData[];




          var DataSale = _Response.Result as ODealData[];




          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }


  CloseRowModal(): void { }

  public _Daily: OSalesTrend = {
    ActualStartDate: moment().subtract(6, "days"),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0,
    HeigestSales: 0,
    LowestSales: 0,
  }
  public _Visits: OSalesTrend = {
    ActualStartDate: moment().subtract(6, "days"),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0,

    HeigestSales: {},
    LowestSales: {}
  }
  // public _VisitsSalesReportGetActualData(): void {
  //   this.VisitslineChartData[0].data = [];
  //   this.GetSalesReportDaily(this._Daily.ActualStartDate, this._Daily.ActualEndDate, this._Daily.ActualData, 'visit', 'actual');
  // }

  public _DailySalesReportGetActualData(): void {

    this.VisitslineChartData[0].data = [];
    // this.VisitslineChartData[1].data = [];
    this.GetSalesReportDaily(this._Daily.ActualStartDate, this._Daily.ActualEndDate, this._Daily.ActualData, this.Types.hour, 'actual');
    // this.GetSalesReportDaily(this._Daily.CompareStartDate, this._Daily.CompareEndDate, this._Daily.CompareData, this.Types.hour, 'compare');

  }
  showDailyChart = true;
  showVisitsChart = true;
  LowestDay: string = "--";
  HeighestDay: string = "--";
  LowestIndex: number;
  HeighestIndex: number;
  HeighestDayCustomer: string = "0";
  LowestDayCustomer: string = "0";

  public SalelineChartData: ChartDataSets[] = [
    { data: [], label: 'Today' },

  ];
  public VisitslineChartData: ChartDataSets[] = [
    { data: [], label: 'Visits' }
  ];
  public DailylineChartData: ChartDataSets[] = [
    { data: [87, 110, 22, 80, 77, 100, 150], label: 'Today' },
    { data: [87, 110, 22, 80, 77, 100, 150], label: 'Last Day' },
  ];
  private p1Data = {
    Task: 'getcustomervisits',
    StartDate: null,
    EndDate: null,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };
  public WeeklylineChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

  GetSalesReportDaily(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {
    this._HelperService.IsFormProcessing = true;
    this.LoadingChart = true;

    this.p1Data.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.p1Data.EndDate = this._HelperService.DateInUTC(EndDateTime);

    this.p1Data.Type = this.Types.week;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analac, this.p1Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OSalesTrendData[];
          this.LoadingChart = false;
          var TempArray = [];
          var TempArraylabel = [];

          var SalesAmount = 0;
          var Heigest: OSalesTrendDataHourly = {
            Hour: null,
            HourAmPm: '0:00 AM',
            HourAmPmNext: '1:00 AM',
            TotalTransaction: 0.0,
            TotalInvoiceAmount: 0.0,
            TotalCustomer: 0.0,
            Total: 0,
            Title: null
          };

          var Lowest: OSalesTrendDataHourly = {
            Hour: null,
            HourAmPm: null,
            HourAmPmNext: null,
            TotalTransaction: 0.0,
            TotalInvoiceAmount: 0.0,
            TotalCustomer: 0.0,
            Total: 999999999,
            Title: null

          };
   const date = new Date();
          var DataHourly = _Response.Result as OSalesTrendDataHourly[];
          for (let index = 0; index <= date.getDay(); index++) {

            var weekday = '';
            switch (index) {
              case 0: weekday = 'Sunday';
                break;
              case 1: weekday = 'Monday';

                break;
              case 2: weekday = 'Tuesday';

                break;
              case 3: weekday = 'Wednesday';

                break;
              case 4: weekday = 'Thursday';

                break;
              case 5: weekday = 'Friday';

                break;
              case 6: weekday = 'Saturday';
                break;

              default:
                break;
            }

            var RData: OSalesTrendDataHourly = DataHourly.find(x => x['Title'] == weekday);
            if (RData != undefined && RData != null) {
              if (Type == 'visit') {
                TempArray.push(RData.Total);
                // TempArraylabel.push(RData.Title);
              } else {
                TempArray.push(RData.Total);
                // TempArraylabel.push(RData.Title);

              }


              const element: OSalesTrendDataHourly = RData;

              if (Heigest.Total < element.Total) {
                Heigest = element;
                this.HeighestDay = Heigest.Title;
                this.HeighestDayCustomer = Heigest.Total

              }

              if (Lowest.Total > element.Total) {
                Lowest = element;
                // this.LowestDay = index;    
                this.LowestDay = Lowest.Title;
                this.LowestDayCustomer = Lowest.Total
              }

              this.VisitslineChartData[0].data = TempArray;
              this.SalelineChartLabels = TempArraylabel


            }
            else {
              TempArray.push(0);
            }

          }
          // if (Type == this._HelperService.AppConfig.GraphTypes.hour) {
          //   this.showDailyChart = false;
          //   this._ChangeDetectorRef.detectChanges();

          //   if (LineType == 'actual') {
          //     this.VisitslineChartData[0].data = TempArray;
          //     this.SalelineChartLabels = TempArraylabel
          //     this._Daily.ActualSalesAmount = SalesAmount;



          //   } else if (LineType == 'compare') {
          //     // this.VisitslineChartData[1].data = TempArray;
          //     this._Daily.CompareSalesAmount = SalesAmount;
          //   }
          //   this.LoadingChart = false;
          //   this.showDailyChart = true;
          //   this._ChangeDetectorRef.detectChanges();
          // } else if (Type == 'visit') {
          //   this._Visits.HeigestSales = Heigest;
          //   this._Visits.LowestSales = Lowest;

          //   this.showVisitsChart = false;
          //   this._ChangeDetectorRef.detectChanges();

          //   if (LineType == 'actual') {
          //     this.VisitslineChartData[0].data = TempArray;
          //     this.SalelineChartLabels = TempArraylabel

          //     this._Visits.ActualSalesAmount = SalesAmount;
          //   } else if (LineType == 'compare') {
          //     // this.VisitslineChartData[1].data = TempArray;

          //     this._Visits.CompareSalesAmount = SalesAmount;
          //   }

          //   this.showVisitsChart = true;
          //   this.LoadingChart = false;

          //   this._ChangeDetectorRef.detectChanges();
          // }
          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }





}


export class OAccountOverview {
  public TerminalStatus?: any;
  public TotalTerminals?: number;
  public TotalSale?: number;
  public AverageTransactionAmount?: number;
  public TotalTransactions?: number;
  public ActiveTerminalsPerc?: number;
  public IdleTerminalsPerc?: number;
  public DeadTerminalsPerc?: number;
  public UnusedTerminalsPerc?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public Idle?: number;
  public Total?: number;

  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
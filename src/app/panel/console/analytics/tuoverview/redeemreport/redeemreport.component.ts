import { ChangeDetectorRef, Component, OnInit, ViewChildren, OnDestroy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ChartDataSets } from 'chart.js';
import * as Feather from 'feather-icons';
import * as cloneDeep from 'lodash/cloneDeep';
import { BaseChartDirective, Color } from 'ng2-charts';
import { Observable, Subscription } from 'rxjs';
import { DataHelperService, HelperService, ODealData, OResponse, OSalesTrend, OGraphData } from '../../../../../service/service';
declare var moment: any;
declare var $: any;

@Component({
  selector: "redeemreport",
  templateUrl: "./redeemreport.component.html",
  styles: [
    `
      agm-map {
        height: 300px;
      }
    `
  ]
})
export class RedeemReportComponent implements OnInit, OnDestroy {
  public LoadingChart: boolean = true;
  lastdaytext = "LAST DAY";
  lastweektext = "LAST WEEK";
  lastweekCustom = "LAST WEEK";
  lastmonthtext = "LAST MONTH";
  lastyeartext = "LAST YEAR";

  Types: any = {
    year: 'year',
    month: 'month',
    week: 'week',
    day: 'day',
    hour: 'hour'
  }

  //#region Destruction 

  public _DateChangeSubscription: Subscription = null;


  //#endregion

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
  }

  //#endregion
  firstTime: boolean = true;

  ngOnInit() {
    this._HelperService.FullContainer = false;
    Feather.replace();
    //start time and end time for overview
    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];
      // this.pData.AccountId = this._HelperService.AppConfig.ActiveMerchantReferenceId;
      // this.pData.AccountKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;
      this.InitializeDates();

    });
    this._DateChangeSubscription = this._HelperService.DateChanged.subscribe(value => {
      // if (!this.firstTime) {
      this.DateChanged(value, null);
      // } else {
      //   this.firstTime = false;
      // }
    });

  }

  ngOnDestroy(): void {
    this._DateChangeSubscription.unsubscribe();
  }

  InitializeDates(): void {

    this._Sale.ActualStartDate = moment().startOf('week').startOf('day');
    this._Sale.ActualEndDate = moment().endOf('week').endOf('day');

    // this._Sale.CompareStartDate = moment().subtract(1, 'week').startOf('').startOf('day');
    // this._Sale.CompareEndDate = moment().subtract(1, 'week').endOf('day');

    var difference = moment().endOf('week').endOf('day').diff(moment().startOf('week').startOf('day'), 'days');
    if (difference <= 1) {
      this._SaleDailyReportGetActualData(this.Types.hour);
    } else if (difference <= 7) {
      this._SaleDailyReportGetActualData(this.Types.week);
    } else if (difference <= 30) {
      this._SaleDailyReportGetActualData(this.Types.month);
    } else {
      this._SaleDailyReportGetActualData(this.Types.year);
    }

  }

  Event: any = null;
  public DateSelected = moment().startOf("day");
  //#region DateChangeHandler
  DateChanged(event: any, Type: any): void {
    this.Event = event;

    var ev: any = cloneDeep(event);
    this.DateSelected = moment(ev.start).startOf("day");

    var difference = null;
    if (this.Type == 'week') {
      difference = moment(ev.end).endOf('week').diff(moment(ev.start).startOf('week'), 'days');
    } else if (this.Type == 'year') {
      difference = moment(ev.end).endOf('year').diff(moment(ev.start).startOf('year'), 'days');
    }
    else if (this.Type == 'month') {
      difference = moment(ev.end).endOf('month').diff(moment(ev.start).startOf('month'), 'days');
    }
    if (difference <= 1) {
      this._Sale.ActualStartDate = moment(ev.start).startOf("day");
      this._Sale.ActualEndDate = moment(ev.end).endOf("day");

      this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'day').startOf("day");
      this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'day').endOf("day");

      this._SaleDailyReportGetActualData(this.Types.hour);
    } else if (difference <= 7) {
      this._Sale.ActualStartDate = moment(ev.start).startOf('week').startOf('day');
      this._Sale.ActualEndDate = moment(ev.end).endOf('week').endOf('day');

      this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'week').startOf('week').startOf('day');
      this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'week').endOf('week').endOf('day');

      this._SaleDailyReportGetActualData(this.Types.week);
    } else if (difference <= 30) {
      this._Sale.ActualStartDate = moment(ev.start).startOf('month').startOf('day');
      this._Sale.ActualEndDate = moment(event.end).endOf('month').endOf('day');

      this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'months').startOf('month').startOf('day');
      this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'months').endOf('month').endOf('day');

      this.SalelineChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Sale.ActualStartDate), moment(this._Sale.ActualEndDate).endOf('month').endOf('day'));

      this._SaleDailyReportGetActualData(this.Types.month);
    } else {
      this._Sale.ActualStartDate = moment(ev.start).startOf('year').startOf('day');
      this._Sale.ActualEndDate = moment(event.end).endOf('year').endOf('day');

      this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'year').startOf('year').startOf('day');
      this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'year').endOf('year').endOf('day');

      this._SaleDailyReportGetActualData(this.Types.year);
    }

    this.RefreshDateLabels();

  }
  //#endregion

  //#region BarChartConfig 
  public Options: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      }
    }
  }
  public barChartLabels = [];
  // public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public barChartColors = [{ backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [200], label: 'Remote' },
    { data: [22], label: 'Remote' },
    { data: [22], label: 'Visit' },
    { data: [22], label: 'Visit' },
  ];

  //#endregion

  //#region Sale Sales Report 
  public RegilineChartData: ChartDataSets[] = [
    { data: [], label: 'registration' },

  ];
  public SalelineChartData: ChartDataSets[] = [
    { data: [], label: 'Today', type: "bar" },
    // { data: []  ,type:"line" },
  ];
  public SalelineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 1)',
    },
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    }
  ];
  public SalelineChartLabels = ['00:00', 'O1:00', '02:00', 'O3:00', '04:00', 'O5:00', '06:00', 'O7:00', '08:00', '09:00', '10:00', '11:00',
    '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
  showSaleChart = true;

  public _Sale: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    CompareSalesTotal: 0,
    ActualSalesTotal: 0,
    SalesAmountDifference: 0
  }
  private RefreshDateLabels() {
    this.lastyeartext = this._HelperService.GetDateSByFormat(this._Sale.CompareStartDate, 'YYYY');
    this.lastdaytext = this._HelperService.GetDateS(this._Sale.CompareStartDate);

    this.lastweektext = this._HelperService.GetDateSByFormat(this._Sale.CompareStartDate, 'DD MMM YY')
      + "-" + this._HelperService.GetDateSByFormat(this._Sale.CompareEndDate, 'DD MMM YY');

    this.lastmonthtext = this._HelperService.GetDateSByFormat(this._Sale.CompareStartDate, 'MMM YYYY');
  }
  public _SaleSalesReportReset(): void {
  }

  public _SaleDailyReportGetActualData(Type): void {

    this.SalelineChartData[0].data = [];
    // this.SalelineChartData[1].data = [];

    if (Type == this.Types.hour) {
      this.GetSalesReportSale(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetSalesReportSale(this._Sale.CompareStartDate, this._Sale.CompareEndDate, this._Sale.ActualData, Type, 'compare');
    } else if (Type == this.Types.week) {
      this.GetSalesReportRedeem(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetSalesReportRedeem(this._Sale.CompareStartDate, this._Sale.CompareEndDate, this._Sale.ActualData, Type, 'compare');
    } else if (Type == this.Types.month || Type == this.Types.year) {
      this.GetSalesReport(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetSalesReport(this._Sale.CompareStartDate, this._Sale.CompareEndDate, this._Sale.ActualData, Type, 'compare');
    }

    //this.GetSalesReport(this._Yearly.ActualStartDate, this._Yearly.ActualEndDate, this._Yearly.ActualData, this.Types.year, 'actual');

  }

  //#endregion

  //#region Sales History General Method 

  private pData = {
    Task: 'getcustomerregistrations',
    StartDate: null,
    EndDate: null,
    // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };


  GetSalesReport(StartDateTime, EndDateTime, Data: OGraphData[], Type, LineType: string) {


    this._HelperService.IsFormProcessing = true;
    this.LoadingChart = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    this.pData.Type = Type;

    if (Type == this.Types.month) {
      this.pData.Type = this.Types.day;
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analac, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OGraphData[];
          this.LoadingChart = false;
          // Data = [
          //   {
          //     "Title": "2020-11",
          //     "Hour": 0,
          //     "Year": 2020,
          //     "Month": 11,
          //     "Customer": 1,
          //     "Amount": 184.0,
          //     "Total": 8,
          //     "Unused": 8,
          //     "Used": 33,
          //     "Expired": 0
          //   },
          //   {
          //     "Title": "2020-11",
          //     "Hour": 0,
          //     "Year": 2020,
          //     "Month": 11,
          //     "Customer": 1,
          //     "Amount": 184.0,
          //     "Total": 8,
          //     "Unused": 8,
          //     "Used": 33,
          //     "Expired": 0
          //   }
          // ];

          var TempArrayTotal = [];
          var TotalSum = 0;

          var TempArrayUnused = [];
          var UnusedSum = 0;

          var TempArrayUsed = [];
          var UsedSum = 0;

          var TempArrayExpired = [];
          var ExpiredSum = 0;

          var TempArrayAmount = [];
          var AmountSum = 0;

          var TempArrayRedeem = [];
          var RedeemSum = 0;

          var TempArrayCustomer = [];
          var CustomerSum = 0;

          if (Type == this._HelperService.AppConfig.GraphTypes.month) {
            var MonthAllDays: any[] = this._HelperService.CalculateIntermediateDate(cloneDeep(StartDateTime),
              cloneDeep(EndDateTime));
            for (let index = 0; index < MonthAllDays.length; index++) {
              const element = MonthAllDays[index];
              var RData: OGraphData = Data.find(x => moment(x.Title, 'DD-MM-YYYY').format('DD MMM') == element);
              if (RData) {
                TempArrayTotal.push(RData.TotalTransaction);
                TempArrayRedeem.push(RData.Total);
                // TempArrayUnused.push(RData.Unused);
                // TempArrayUsed.push(RData.Used);
                // TempArrayExpired.push(RData.Expired);
                TempArrayAmount.push(RData.TotalInvoiceAmount);
                // TempArrayCustomer.push(RData.Customer);

                TotalSum = TotalSum + RData.TotalTransaction;
                // UnusedSum = UnusedSum + RData.Unused;
                // UsedSum = UsedSum + RData.Used;
                // ExpiredSum = ExpiredSum + RData.Expired;
                AmountSum = AmountSum + (RData.TotalInvoiceAmount ? RData.TotalInvoiceAmount : 0);
                // CustomerSum = CustomerSum + RData.Customer;

                RedeemSum = RedeemSum + (RData.RedeemAmount ? RData.RedeemAmount : 0);
              } else {
                TempArrayTotal.push(0);
                TempArrayUnused.push(0);
                TempArrayUsed.push(0);
                TempArrayExpired.push(0);
                TempArrayAmount.push(0);
                TempArrayCustomer.push(0);
                TempArrayRedeem.push(0);
              }
            }

            this.showSaleChart = false;
            this._ChangeDetectorRef.detectChanges();




            if (LineType == 'actual') {

              this.SalelineChartData[0].data = TempArrayRedeem;
              // this.SalelineChartData[1].data = TempArrayRedeem;


              this._Sale.ActualSalesAmount = AmountSum;
              this._Sale.ActualSalesTotal = TotalSum;

              // this._Redeem.ActualSalesAmount = UsedSum;
            } else {

              // this.SalelineChartData[1].data = TempArrayRedeem;

              this._Sale.CompareSalesAmount = AmountSum;
              this._Sale.CompareSalesTotal = TotalSum;

              // this._Redeem.CompareSalesAmount = UsedSum;
            }

            this.SalelineChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Sale.ActualStartDate), moment(this._Sale.ActualEndDate));

            this.showSaleChart = true;
            this._ChangeDetectorRef.detectChanges();


          } else {
            for (let index = 0; index < 12; index++) {

              var RData: OGraphData = Data.find(x => x['Month'] == index + 1);
              if (RData) {
                TempArrayTotal[index] = (RData.TotalTransaction);
                TempArrayRedeem.push(RData.Total);
                // TempArrayUnused[index] = (RData.Unused);
                // TempArrayUsed[index] = (RData.Used);
                // TempArrayExpired[index] = (RData.Expired);
                TempArrayAmount[index] = (RData.TotalInvoiceAmount);
                // TempArrayCustomer[index] = (RData.Customer);

                TotalSum = TotalSum + RData.TotalTransaction;
                // UnusedSum = UnusedSum + RData.Unused;
                // UsedSum = UsedSum + RData.Used;
                // ExpiredSum = ExpiredSum + RData.Expired;
                AmountSum = AmountSum + RData.TotalInvoiceAmount;
                RedeemSum = RedeemSum + (RData.RedeemAmount ? RData.RedeemAmount : 0);

              } else {
                TempArrayTotal[index] = (0);
                TempArrayUnused[index] = (0);
                TempArrayUsed[index] = (0);
                TempArrayExpired[index] = (0);
                TempArrayAmount[index] = (0);
                TempArrayCustomer[index] = (0);
                TempArrayRedeem[index] = (0);
              }
            }

            this.showSaleChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {

              this.SalelineChartData[0].data = TempArrayRedeem;
              // this.SalelineChartData[1].data = TempArrayRedeem;


              this._Sale.ActualSalesAmount = AmountSum;
              this._Sale.ActualSalesTotal = TotalSum;


              // this._Redeem.ActualSalesAmount = UsedSum;
            } else {

              // this.SalelineChartData[1].data = TempArrayRedeem;

              this._Sale.CompareSalesAmount = AmountSum;
              this._Sale.CompareSalesTotal = TotalSum;


              // this._Redeem.CompareSalesAmount = UsedSum;
            }

            this.SalelineChartLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];

            this.showSaleChart = true;
            this._ChangeDetectorRef.detectChanges();
          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  GetSalesReportSale(StartDateTime, EndDateTime, Data: OGraphData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;
    this.LoadingChart = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);


    this.pData.Type = this.Types.hour;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analac, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {

          var DataHourly = _Response.Result as OGraphData[];

          this.LoadingChart = false;

          var TempArrayTotal = [];
          var TotalSum = 0;

          var TempArrayUnused = [];
          var UnusedSum = 0;

          var TempArrayUsed = [];
          var UsedSum = 0;

          var TempArrayExpired = [];
          var ExpiredSum = 0;

          var TempArrayAmount = [];
          var AmountSum = 0;

          var TempArrayRedeem = [];
          var RedeemSum = 0;

          var TempArrayCustomer = [];
          var CustomerSum = 0;



          for (let index = 0; index < 24; index++) {
            var RData: OGraphData = DataHourly.find(x => x.Hour == index);

            if (RData != undefined && RData != null) {

              TempArrayTotal.push(RData.TotalTransaction);
              TempArrayRedeem.push(RData.Total);
              // TempArrayUnused.push(RData.Unused);
              // TempArrayUsed.push(RData.Used);
              // TempArrayExpired.push(RData.Expired);
              TempArrayAmount.push(RData.TotalInvoiceAmount);
              // TempArrayCustomer.push(RData.Customer);

              const element: OGraphData = RData;

              TotalSum = TotalSum + element.TotalTransaction;
              // UnusedSum = UnusedSum + element.Unused;
              // UsedSum = UsedSum + element.Used;
              // ExpiredSum = ExpiredSum + element.Expired;
              AmountSum = AmountSum + element.TotalInvoiceAmount;
              // CustomerSum = CustomerSum + element.Customer;
            }
            else {
              TempArrayTotal.push(0);
              TempArrayUnused.push(0);
              TempArrayUsed.push(0);
              TempArrayExpired.push(0);
              TempArrayAmount.push(0);
              TempArrayCustomer.push(0);
              TempArrayRedeem.push(0);
            }

          }



          if (Type == this._HelperService.AppConfig.GraphTypes.hour) {

            this.showSaleChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.SalelineChartData[0].data = TempArrayRedeem;

              this._Sale.ActualSalesAmount = AmountSum;
              this._Sale.ActualSalesTotal = TotalSum;

              // this._Redeem.ActualSalesAmount = UsedSum;
            } else {
              // this.SalelineChartData[1].data = TempArrayRedeem;

              this._Sale.CompareSalesAmount = AmountSum;
              this._Sale.CompareSalesTotal = TotalSum;

              // this._Redeem.CompareSalesAmount = UsedSum;
            }

            this.SalelineChartLabels = ['00:00', 'O1:00', '02:00', 'O3:00', '04:00', 'O5:00', '06:00', 'O7:00', '08:00', '09:00', '10:00', '11:00',
              '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];


            this.showSaleChart = true;
            this._ChangeDetectorRef.detectChanges();
          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  GetSalesReportRedeem(StartDateTime, EndDateTime, Data: OGraphData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;
    this.LoadingChart = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    this.pData.Type = Type;

    if (Type == this.Types.day) {
      this.pData.Type = 'week';
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analac, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OGraphData[];
          this.LoadingChart = false;



          var TempArrayTotal = [];
          var TotalSum = 0;

          var TempArrayUnused = [];
          var UnusedSum = 0;

          var TempArrayUsed = [];
          var UsedSum = 0;

          var TempArrayExpired = [];
          var ExpiredSum = 0;

          var TempArrayAmount = [];
          var AmountSum = 0;

          var TempArrayRedeem = [];
          var TempArrayRedeemIndex2 = [];

          var RedeemSum = 0;

          var TempArrayCustomer = [];
          var CustomerSum = 0;

          var TempColorArray = ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'];

          var Heigest = Data[0];
          var HeigestIndex = 0;
          var Lowest = Data[0];
          var LowestIndex = 0;

          var DataSale = _Response.Result as OGraphData[];

          for (let index = 0; index < 7; index++) {

            var weekday = '';
            switch (index) {
              case 0: weekday = 'Sunday';
                break;
              case 1: weekday = 'Monday';

                break;
              case 2: weekday = 'Tuesday';

                break;
              case 3: weekday = 'Wednesday';

                break;
              case 4: weekday = 'Thursday';

                break;
              case 5: weekday = 'Friday';

                break;
              case 6: weekday = 'Saturday';
                break;

              default:
                break;
            }

            var RData: OGraphData = DataSale.find(x => x['Title'] == weekday);
            if (RData != undefined && RData != null) {

              TempArrayTotal.push(RData.TotalTransaction);
              TempArrayRedeem.push(RData.Total);

              // TempArrayUnused.push(RData.Unused);
              // TempArrayUsed.push(RData.Used);
              // TempArrayExpired.push(RData.Expired);
              TempArrayAmount.push(RData.TotalInvoiceAmount);
              // TempArrayCustomer.push(RData.Customer);

              const element: OGraphData = RData;

              TotalSum = TotalSum + element.TotalTransaction;
              // UnusedSum = UnusedSum + element.Unused;
              // UsedSum = UsedSum + element.Used;
              // ExpiredSum = ExpiredSum + element.Expired;
              AmountSum = AmountSum + element.TotalInvoiceAmount;
              // CustomerSum = CustomerSum + element.Customer;


              TotalSum = TotalSum + element.TotalTransaction;
              // UnusedSum = UnusedSum + element.Unused;
              // UsedSum = UsedSum + element.Used;
              // ExpiredSum = ExpiredSum + element.Expired;
              AmountSum = AmountSum + element.TotalInvoiceAmount;
              // CustomerSum = CustomerSum + element.Customer;
            }
            else {
              TempArrayTotal.push(0);
              TempArrayUnused.push(0);
              TempArrayUsed.push(0);
              TempArrayExpired.push(0);
              TempArrayAmount.push(0);
              TempArrayCustomer.push(0);
              TempArrayRedeem.push(0);
            }

          }

          if (Type == this._HelperService.AppConfig.GraphTypes.week) {

            this.showSaleChart = false;
            this._ChangeDetectorRef.detectChanges();



            if (LineType == 'actual') {
              this.SalelineChartData[0].data = TempArrayRedeem;
              // this.SalelineChartData[1].data = TempArrayRedeem;


              this._Sale.ActualSalesAmount = AmountSum;
              this._Sale.ActualSalesTotal = TotalSum;


            } else {
              // this.SalelineChartData[1].data = TempArrayRedeem;

              this._Sale.CompareSalesAmount = AmountSum;
              this._Sale.CompareSalesTotal = TotalSum;

            }

            this.SalelineChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

            this.showSaleChart = true;
            this._ChangeDetectorRef.detectChanges();
          }
          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  //#endregion

  computePerc(num: OSalesTrend): any {
    if (num.CompareSalesAmount == 0) {
      return '100 %';
    }
    if (num.ActualSalesAmount > num.CompareSalesAmount) {
      return Math.round(((num.ActualSalesAmount - num.CompareSalesAmount) / num.CompareSalesAmount) * 100) + ' %';
    } else if (num.ActualSalesAmount < num.CompareSalesAmount) {
      return Math.round(((num.CompareSalesAmount - num.ActualSalesAmount) / num.CompareSalesAmount) * 100) + ' %';
    } else {
      return '0 %';
    }
  }

  computeFlag(num: OSalesTrend): number {
    if (num.ActualSalesAmount > num.CompareSalesAmount) {
      return 1;
    } else if (num.ActualSalesAmount < num.CompareSalesAmount) {
      return -1;
    } else {
      return 0;
    }
  }


  Type: string = 'week';
  changeToWeek(): void {
    this.Type = 'week';
    this.DateChanged(this.Event, null);

  }
  changeToYear(): void {
    this.Type = 'year';
    this.DateChanged(this.Event, null);
  }
  changeToMonth(): void {
    this.Type = 'month';
    this.DateChanged(this.Event, null);
  }

  CloseRowModal(): void { }
}


export class OAccountOverview {
  public TerminalStatus?: any;
  public TotalTerminals?: number;
  public TotalSale?: number;
  public AverageTransactionAmount?: number;
  public TotalTransactions?: number;
  public ActiveTerminalsPerc?: number;
  public IdleTerminalsPerc?: number;
  public DeadTerminalsPerc?: number;
  public UnusedTerminalsPerc?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public Idle?: number;
  public Total?: number;

  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
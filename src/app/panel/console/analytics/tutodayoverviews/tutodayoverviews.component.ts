import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import Highcharts from "highcharts";
import Exporting from "highcharts/modules/exporting";
import funnel from "highcharts/modules/funnel";
import { BaseChartDirective } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, ODealData, OResponse, OSalesTrend, OSalesTrendData, OSalesTrendDataHourly } from '../../../../service/service';
declare var moment: any;
declare var $: any;
import { ChartDataSets } from 'chart.js';

Exporting(Highcharts);
funnel(Highcharts);

@Component({
  selector: "tutodayoverviews",
  templateUrl: "./tutodayoverviews.component.html",
  styles: [
    `
      agm-map {
        height: 300px;
      }
    `
  ]
})
export class TuTodayOverviewComponent implements OnInit {
  @ViewChild("container", { read: ElementRef }) container: ElementRef;

  public StoreLabel = []
  public PostHorizontalData = [
    { data: [80], label: 'Active' },
    { data: [72], label: 'In-Active' },
    { data: [65], label: 'Dead ' },
    { data: [75], label: 'Idle' },
  ];

  lastdaytext = "LAST DAY";
  lastweektext = "LAST WEEK";
  lastweekCustom = "LAST WEEK";
  lastmonthtext = "LAST MONTH";
  lastyeartext = "LAST YEAR";

  Types: any = {
    year: 'year',
    month: 'month',
    week: 'week',
    day: 'day',
    hour: 'hour'
  }

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];
  public TodayDate: any;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this.TodayDate = this._HelperService.GetDateTime(new Date());
  }
  StartDateTime = null;

  EndDateTime = null;
  //#endregion
  ngOnInit() {

    this._HelperService.FullContainer = false;

    this.StartDateTime = moment().startOf('day');
    this.EndDateTime = moment().endOf('day');

    this.LoadData();

    Highcharts.chart(this.container.nativeElement, {
      credits: {
        enabled: false
      },
      chart: {
        type: "funnel",
      },
      exporting: { enabled: false },
      title: {
        text: null,
        enabled: false
      },
      plotOptions: {
        xAxis: {
          labels: {
            rotation: 180,

          }
        },
        series: {
          dataLabels: {
            format: "<b>{point.name}</b> ({point.y:,.0f})",
            color:
              (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
              "black",
            softConnector: false,
            align: "center",
            allowOverlap: true,
            enabled: true,
            showInLegend: true,
            x: -250,
            y: -10
          },
          neckWidth: ["20%"],
          neckHeight: ["00%"],
          width: "30%",
          height: "100%",

        }
      },
      legend: {
        enabled: true
      },
      series: [
        {
          name: "Unique users",
          data: [
            ["Website visits", 40],
            ["Downloads", 20],
            ["Requested price", 21],
            ["Invoice sent", 24],
          ]
        }
      ]
    });

    Feather.replace();

  }

  LoadData() {
    this.GetLoyalityOverview(this.StartDateTime, this.EndDateTime)
    this.GetLoyalityOverviewSale(this.StartDateTime, this.EndDateTime);
    this.GettodayOverviewSale(this.StartDateTime, this.EndDateTime)
    this.RefreshTerminalStatusCount();
    this._DailySalesReportGetActualData();
    this._VisitsSalesReportGetActualData();

  }

  RefreshTerminalStatusCount() {
    this._HelperService.GetTerminalStatusCount(this._HelperService.UserAccount.AccountId, this._HelperService.AppConfig.AccountType.MerchantId, 0, 0);
  }


  MerchantList() {
    this._Router.navigate([
      'console/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AccountSettings.Merchants,

    ]);
  }

  public LoyalityOverview: any =
    {


    }
  private LoyalityOvereview = {
    Task: 'getloyaltyoverview',
    StartDate: null,
    EndDate: null,
    // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };

  GetLoyalityOverview(StartDateTime, EndDateTime) {


    this._HelperService.IsFormProcessing = true;

    this.LoyalityOvereview.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.LoyalityOvereview.EndDate = this._HelperService.DateInUTC(EndDateTime);


    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analytics, this.LoyalityOvereview);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.LoyalityOverview = _Response.Result;


        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }



  public SalesOverview: any =
    {


    }
  private pData = {
    Task: 'getsalesoverview',
    StartDate: null,
    EndDate: null,
    // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };

  GetLoyalityOverviewSale(StartDateTime, EndDateTime) {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);


    // this.pData.Type = this.Types.hour;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analytics, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this.SalesOverview = _Response.Result as ODealData[];


        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public todayDataSalesOverview: any =
    {


    }
  public NewMerchants: any = []
  public LowBalance: any = []
  public NewPos: any = []
  private ptodayOverviewData = {
    Task: 'gettodaysoverview',
    StartDate: null,
    EndDate: null,
    // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };

  GettodayOverviewSale(StartDateTime, EndDateTime) {

    this._HelperService.IsFormProcessing = true;

    this.ptodayOverviewData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.ptodayOverviewData.EndDate = this._HelperService.DateInUTC(EndDateTime);


    // this.ptodayOverviewData.Type = this.Types.hour;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analytics, this.ptodayOverviewData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this.todayDataSalesOverview = _Response.Result;
          this.NewMerchants = this.todayDataSalesOverview.NewMerchants;
          this.LowBalance = this.todayDataSalesOverview.LowBalancewMerchants;
          this.NewPos = this.todayDataSalesOverview.NewTerminals;


        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  MerchantRoute(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveMerchant,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveMerchantReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveMerchantReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.RewardPercentage,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);

  }

  TerminalRoute(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveTerminal,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveTerminalReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveTerminalReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Terminal.SalesHistory,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);
  }


  GetLoyalityOverviewRedeem(StartDateTime, EndDateTime, Data: ODealData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    this.pData.Type = Type;

    if (Type == this.Types.day) {
      this.pData.Type = 'week';
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as ODealData[];
          var DataSale = _Response.Result as ODealData[];
          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }


  CloseRowModal(): void { }

  public _Daily: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0,
    HeigestSales: 0,
    LowestSales: 0,
  }
  public _Visits: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0,

    HeigestSales: {},
    LowestSales: {}
  }
  public _VisitsSalesReportGetActualData(): void {
    this.VisitslineChartData[0].data = [];
    // this.GetSalesReportDaily(this._Daily.ActualStartDate, this._Daily.ActualEndDate, this._Daily.ActualData, 'visit', 'actual');
  }

  public _DailySalesReportGetActualData(): void {

    this.DailylineChartData[0].data = [];
    this.DailylineChartData[1].data = [];
    // this.GetSalesReportDaily(this._Daily.ActualStartDate, this._Daily.ActualEndDate, this._Daily.ActualData, this.Types.hour, 'actual');
    // this.GetSalesReportDaily(this._Daily.CompareStartDate, this._Daily.CompareEndDate, this._Daily.CompareData, this.Types.hour, 'compare');

  }
  showDailyChart = true;
  showVisitsChart = true;

  public VisitslineChartData: ChartDataSets[] = [
    { data: [], label: 'Visits' }
  ];
  public DailylineChartData: ChartDataSets[] = [
    { data: [87, 110, 22, 80, 77, 100, 150], label: 'Today' },
    { data: [87, 110, 22, 80, 77, 100, 150], label: 'Last Day' },
  ];
  private p1Data = {
    Task: 'getsaleshistory',
    StartDate: null,
    EndDate: null,
    AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
    AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };

  GetSalesReportDaily(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.p1Data.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.p1Data.EndDate = this._HelperService.DateInUTC(EndDateTime);

    this.p1Data.Type = this.Types.hour;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.p1Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OSalesTrendData[];

          var TempArray = [];
          var SalesAmount = 0;
          var Heigest: OSalesTrendDataHourly = {
            Hour: null,
            HourAmPm: '0:00 AM',
            HourAmPmNext: '1:00 AM',
            TotalTransaction: 0.0,
            TotalInvoiceAmount: 0.0,
            TotalCustomer: 0.0
          };

          var Lowest: OSalesTrendDataHourly = {
            Hour: null,
            HourAmPm: null,
            HourAmPmNext: null,
            TotalTransaction: 0.0,
            TotalInvoiceAmount: 0.0,
            TotalCustomer: 0.0
          };

          var DataHourly = _Response.Result as OSalesTrendDataHourly[];
          for (let index = 0; index < 24; index++) {
            var RData: OSalesTrendDataHourly = DataHourly.find(x => x.Hour == index);
            if (RData != undefined && RData != null) {

              if (Type == 'visit') {
                TempArray.push(RData.TotalCustomer);
              } else {
                TempArray.push(RData.TotalInvoiceAmount);
              }

              const element: OSalesTrendDataHourly = RData;
              if (Heigest.TotalCustomer < element.TotalCustomer) {

                Heigest = element;

                //#region current 

                let dd = " AM";
                let h = index;
                if (h >= 12) {
                  h = index - 12;
                  dd = " PM";
                }
                if (h == 0) {
                  h = 12;
                }
                let Hour = h + ":00" + dd;
                Heigest.HourAmPm = Hour;

                //#endregion

                //#region next 

                let dd2 = " AM";
                let h2 = index + 1;
                if (h2 == 24) {
                  h2 = 1;
                }
                else if (h2 >= 12) {
                  h2 = (index + 1) - 12;
                  dd2 = " PM";
                }
                if (h2 == 0) {
                  h2 = 12;
                }
                let Hour2 = Math.abs(h2) + ":00" + dd2;
                Heigest.HourAmPmNext = Hour2;

                //#endregion
              }

              if (Lowest.HourAmPm == null) {
                Lowest = element;

                //#region current 

                let dd3 = " AM";
                let h3 = index;
                if (h3 >= 12) {
                  h3 = index - 12;
                  dd3 = " PM";
                }
                if (h3 == 0) {
                  h3 = 12;
                }
                let Hour3 = h3 + ':00' + dd3;
                Lowest.HourAmPm = Hour3;

                //#endregion

                //#region next 

                let dd4 = " AM";
                let h4 = index + 1;
                if (h4 == 24) {
                  h4 = 1;
                } else if (h4 >= 12) {
                  h4 = (index + 1) - 12;
                  dd4 = " PM";
                }
                if (h4 == 0) {
                  h4 = 12;
                }
                let Hour4 = Math.abs(h4) + ":00" + dd4;
                Lowest.HourAmPmNext = Hour4;

                //#endregion
              }

              if (Lowest.TotalCustomer > element.TotalCustomer && element.TotalCustomer != 0) {
                Lowest = element;

                //#region current 

                let dd3 = " AM";
                let h3 = index;
                if (h3 >= 12) {
                  h3 = index - 12;
                  dd3 = " PM";
                }
                if (h3 == 0) {
                  h3 = 12;
                }
                let Hour3 = h3 + ':00' + dd3;
                Lowest.HourAmPm = Hour3;

                //#endregion

                //#region next 

                let dd4 = " AM";
                let h4 = index + 1;
                if (h4 == 24) {
                  h4 = 1;
                } else if (h4 >= 12) {
                  h4 = (index + 1) - 12;
                  dd4 = " PM";
                }
                if (h4 == 0) {
                  h4 = 12;
                }
                let Hour4 = Math.abs(h4) + ":00" + dd4;
                Lowest.HourAmPmNext = Hour4;

                //#endregion
              }

              SalesAmount = SalesAmount + element.TotalInvoiceAmount;
            }
            else {
              TempArray.push(0);
            }

          }



          if (Type == this._HelperService.AppConfig.GraphTypes.hour) {

            this.showDailyChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.DailylineChartData[0].data = TempArray;
              this._Daily.ActualSalesAmount = SalesAmount;
            } else if (LineType == 'compare') {
              this.DailylineChartData[1].data = TempArray;
              this._Daily.CompareSalesAmount = SalesAmount;
            }

            this.showDailyChart = true;
            this._ChangeDetectorRef.detectChanges();
          } else if (Type == 'visit') {
            this._Visits.HeigestSales = Heigest;
            this._Visits.LowestSales = Lowest;

            this.showVisitsChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.VisitslineChartData[0].data = TempArray;
              this._Visits.ActualSalesAmount = SalesAmount;
            } else if (LineType == 'compare') {
              this.VisitslineChartData[1].data = TempArray;
              this._Visits.CompareSalesAmount = SalesAmount;
            }

            this.showVisitsChart = true;
            this._ChangeDetectorRef.detectChanges();
          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }





}


export class OAccountOverview {
  public TerminalStatus?: any;
  public TotalTerminals?: number;
  public TotalSale?: number;
  public AverageTransactionAmount?: number;
  public TotalTransactions?: number;
  public ActiveTerminalsPerc?: number;
  public IdleTerminalsPerc?: number;
  public DeadTerminalsPerc?: number;
  public UnusedTerminalsPerc?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public Idle?: number;
  public Total?: number;

  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
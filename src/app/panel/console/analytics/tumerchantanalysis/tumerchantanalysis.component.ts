import { ChangeDetectorRef, Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ChartDataSets } from 'chart.js';
import * as Feather from 'feather-icons';
import * as cloneDeep from 'lodash/cloneDeep';
import { BaseChartDirective, Color } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OResponse, OSalesTrend, ODealData, OSelect ,OList} from '../../../../service/service';
declare var moment: any;
declare var $: any;
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
@Component({
  selector: "tumerchantanalysis",
  templateUrl: "./tumerchantanalysis.component.html",
  styles: [
    `
      agm-map {
        height: 300px;
      }
    `
  ]
})
export class TuMerchantAnalysisComponent implements OnInit {
  public ViewMerchantAnalytics :boolean = false;
  StoreLabel: any;
  public PostHorizontalData = [
    { data: [80], label: 'Active' },
    { data: [72], label: 'In-Active' },
    { data: [65], label: 'Dead ' },
    { data: [75], label: 'Idle' },
  ];

  lastdaytext = "LAST DAY";
  lastweektext = "LAST WEEK";
  lastweekCustom = "LAST WEEK";
  lastmonthtext = "LAST MONTH";
  lastyeartext = "LAST YEAR";

  Types: any = {
    year: 'year',
    month: 'month',
    week: 'week',
    day: 'day',
    hour: 'hour'
  }
  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [87, 44, 22, 55, 77, 333, 222], borderDash: [10, 5], label: 'Series B' },

  ];
  public LineChartYearlyLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
  public lineChartYearlyData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40, 80, 81, 56, 55, 40] },
    { data: [87, 44, 22, 55, 77, 333, 222, 80, 181, 256, 155, 140], borderDash: [10, 5] },

  ];
  public LineWeelyChartLabels = ['MON', 'TUE', 'WED', 'THUS', 'FRI', 'SAT', 'SUN'];
  public lineChartRedeemData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40] },
    { data: [87, 44, 22, 55, 77, 333, 222], borderDash: [10, 5] },

  ];
  public lineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ]
  public TodayDate: any;

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {

  }
  lat = 9.0820;
  long = 8.6753;
  zoom = 5;
  markers = [
    {
      lat: 6.4500,
      lng: 3.4000,
      label: 'Lagos',
      IconUrl:'https://homepages.cae.wisc.edu/~ece533/images/airplane.png',

    },
    {
      lat: 6.1667,
      lng: 6.7833,
      label: 'Onitsha',
      IconUrl:'https://homepages.cae.wisc.edu/~ece533/images/airplane.png',

    },
    {
      lat: 12.0000,
      lng: 8.5167,
      label: 'Kano',
      IconUrl:'https://homepages.cae.wisc.edu/~ece533/images/airplane.png',

    },
    {
      lat: 7.3964,
      lng: 3.9167,
      label: 'Ibadan',
      IconUrl:'https://homepages.cae.wisc.edu/~ece533/images/airplane.png',

    },
    {
      lat: 5.4833,
      lng: 7.0333,
      label: 'Owerri',
      IconUrl:'https://homepages.cae.wisc.edu/~ece533/images/airplane.png',

    },
    {
      lat: 9.0556,
      lng: 7.4914,
      label: 'Abuja',
      IconUrl:'https://homepages.cae.wisc.edu/~ece533/images/airplane.png',

    }

  ];


  //#endregion
  ngOnInit() {
    this._HelperService.FullContainer = false;
    Feather.replace();
    this.TodayDate = this._HelperService.GetDateTime(new Date());
    this.TodayStartTime = moment().startOf('day');
    this.TodayEndTime = moment().endOf('day');
    //start time and end time for overview

    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];

      // this.pData.AccountId = this._HelperService.AppConfig.ActiveMerchantReferenceId;
      // this.pData.AccountKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;

      this.InitializeDates();

    });
    this.StoresList_Setup();

  }





  RefreshTerminalStatusCount() {
    this._HelperService.GetTerminalStatusCount(this._HelperService.UserAccount.AccountId, this._HelperService.AppConfig.AccountType.MerchantId, 0, 0);
  }

  MerchantList() {
    this._Router.navigate([
      'console/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AccountSettings.Merchants,

    ]);
  }

  InitializeDates(): void {
    this.TodayStartTime = moment().startOf('day');
    this.TodayEndTime = moment().endOf('day');
    //#region Sale Dates 

    this._Sale.ActualStartDate = moment().startOf('day');
    this._Sale.ActualEndDate = moment().endOf('day');

    this._Sale.CompareStartDate = moment().subtract(1, 'day').startOf('day');
    this._Sale.CompareEndDate = moment().subtract(1, 'day').endOf('day');

    //#endregion

    //#region Redeem Dates 

    this._Redeem.ActualStartDate = moment().startOf('week').startOf('day');
    this._Redeem.ActualEndDate = moment().endOf('day');

    this._Redeem.CompareStartDate = moment().subtract(1, 'week').startOf('week').startOf('day');
    this._Redeem.CompareEndDate = moment().subtract(1, 'week').endOf('week').endOf('day');

    //#endregion

    //#region Revenue Dates 

    this._Revenue.ActualStartDate = moment().startOf('month').startOf('day');
    this._Revenue.ActualEndDate = moment().endOf('month').endOf('day');

    this._Revenue.CompareStartDate = moment().subtract(1, 'months').startOf('month').startOf('day');
    this._Revenue.CompareEndDate = moment().subtract(1, 'months').endOf('month').endOf('day');

    this.RevenuebarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate));

    //#endregion

    //#region Yearly Dates 

    this._Yearly.ActualStartDate = moment().startOf('year').startOf('day');
    this._Yearly.ActualEndDate = moment().endOf('year').endOf('day');

    this._Yearly.CompareStartDate = moment().subtract(1, 'year').startOf('year').startOf('day');
    this._Yearly.CompareEndDate = moment().subtract(1, 'year').endOf('year').endOf('day');

    //#endregion

    var difference = moment().diff(moment(), 'days');
    if (difference <= 1) {
      this._SaleDailyReportGetActualData(this.Types.hour);
    } else if (difference <= 7) {
      this._SaleDailyReportGetActualData(this.Types.week);
    } else if (difference <= 30) {
      this._SaleDailyReportGetActualData(this.Types.month);
    } else {
      this._SaleDailyReportGetActualData(this.Types.year);
    }

  }



  htmltoPDF() {
    this.ViewMerchantAnalytics = true;
    setTimeout(() => {
        this.StartDownload();
        this.ViewMerchantAnalytics = false;
    }, 1000);
}

StartDownload() {
    html2canvas(document.querySelector("#ViewMerchantAnalytics"),{ useCORS: true }).then(canvas => {
        var imgWidth = 208;
        var pageHeight = 295;
        var imgHeight = canvas.height * imgWidth / canvas.width;
        var heightLeft = imgHeight;
        const contentDataURL = canvas.toDataURL('image/png');
        let pdf1 = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
        var position = 0;
        pdf1.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
        pdf1.save('Merchant_Analytics' + '.pdf'); // Generated PDF   
    });
}

  public DateSelected = moment().startOf("day");
  //#region DateChangeHandler
  DateChanged(event: any, Type: any): void {

    var ev: any = cloneDeep(event);
    this.TodayStartTime = moment(ev.start).startOf('day');
    this.TodayEndTime = moment(ev.end).endOf('day');
    this.DateSelected = moment(ev.start).startOf("day");

    //#region Sale 

    this._Sale.ActualStartDate = moment(ev.start).startOf("day");
    this._Sale.ActualEndDate = moment(ev.end).endOf("day");

    this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'day').startOf("day");
    this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'day').endOf("day");

    //#endregion
    //#region Week 

    this._Redeem.ActualStartDate = moment(ev.start).startOf('week').startOf('day');
    this._Redeem.ActualEndDate = moment(ev.end).endOf('week').endOf('day');

    this._Redeem.CompareStartDate = moment(ev.start).subtract(1, 'week').startOf('week').startOf('day');
    this._Redeem.CompareEndDate = moment(ev.end).subtract(1, 'week').endOf('week').endOf('day');

    //#endregion
    //#region Revenue 

    this._Revenue.ActualStartDate = moment(ev.start).startOf('month').startOf('day');
    this._Revenue.ActualEndDate = moment(event.end).endOf('month').endOf('day');

    this._Revenue.CompareStartDate = moment(ev.start).subtract(1, 'months').startOf('month').startOf('day');
    this._Revenue.CompareEndDate = moment(ev.end).subtract(1, 'months').endOf('month').endOf('day');

    this.RevenuebarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate).endOf('month').endOf('day'));
    //#endregion
    //#region Yearly 

    this._Yearly.ActualStartDate = moment(ev.start).startOf('year').startOf('day');
    this._Yearly.ActualEndDate = moment(event.end).endOf('year').endOf('day');

    this._Yearly.CompareStartDate = moment(ev.start).subtract(1, 'year').startOf('year').startOf('day');
    this._Yearly.CompareEndDate = moment(ev.end).subtract(1, 'year').endOf('year').endOf('day');

    //#endregion

    var difference = moment(ev.end).diff(moment(ev.start), 'days');
    if (difference <= 1) {
      this._SaleDailyReportGetActualData(this.Types.hour);
    } else if (difference <= 7) {
      this._SaleDailyReportGetActualData(this.Types.week);
    } else if (difference <= 30) {
      this._SaleDailyReportGetActualData(this.Types.month);
    } else {
      this._SaleDailyReportGetActualData(this.Types.year);
    }
    this.RefreshDateLabels();
    this.RefreshTerminalStatusCount();

  }
  //#endregion

  //#region BarChartConfig 
  public Options: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      }
    }
  }

  public barChartLabels = [];
  // public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public barChartColors = [{ backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [200], label: 'Remote' },
    { data: [22], label: 'Remote' },
    { data: [22], label: 'Visit' },
    { data: [22], label: 'Visit' },
  ];

  //#endregion

  //#region RevenueBarChartConfig 
  public RevenueOptions: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      }
    }
  }
  public RevenuebarChartLabels = [];
  // public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public RevenuebarChartColors = [{ backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }];
  public RevenuebarChartType = 'bar';
  public RevenuebarChartLegend = true;
  public RevenuebarChartData = [
    { data: [200], label: 'Remote' },
    { data: [22], label: 'Remote' },
    { data: [22], label: 'Visit' },
    { data: [22], label: 'Visit' },
  ];


  public RevenuelineChartData: ChartDataSets[] = [
    { data: [], label: 'Current Month' },
    { data: [], borderDash: [10, 5], label: 'Compared Month' },
  ];

  public RevenuelineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ]

  //#endregion

  //#region Sale Sales Report 

  public SalelineChartData: ChartDataSets[] = [
    { data: [], label: 'Today' },
    { data: [], label: 'Compared Date' },


  ];
  RewardLabels = []
  public DataSales = [
    { data: [0], label: '1M-2M' },
    { data: [0], label: '2M-3M' },
    { data: [0], label: '3M-4M ' },
    { data: [0], label: 'More Than 5' },
  ]

  public MerchantCategotries = [
    { data: [0], label: '1M-2M' },
    { data: [0], label: '2M-3M' },
    { data: [0], label: '3M-4M ' },
    { data: [0], label: 'More Than 5' },
  ]


  public DataReward = [
    { data: [0], label: '1M-2M' },
    { data: [0], label: '2M-3M' },
    { data: [0], label: '3M-4M ' },
    { data: [0], label: 'More Than 5' },
  ]
  SalesLabels: any;
  // public SalesLabels  = ["1m-2m", "2m-3m", "3m-4m", "More Than $m",]
  public SalelineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];
  public SalelineChartLabels = ['00:00', 'O1:00', '02:00', 'O3:00', '04:00', 'O5:00', '06:00', 'O7:00', '08:00', '09:00', '10:00', '11:00',
    '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
  showSaleChart = true;

  public _Sale: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    CompareSalesTotal: 0,
    ActualSalesTotal: 0,
    SalesAmountDifference: 0
  }

  private RefreshDateLabels() {
    this.lastyeartext = this._HelperService.GetDateSByFormat(this._Yearly.CompareStartDate, 'YYYY');
    this.lastdaytext = this._HelperService.GetDateS(this._Sale.CompareStartDate);

    this.lastweektext = this._HelperService.GetDateSByFormat(this._Redeem.CompareStartDate, 'DD MMM YY')
      + "-" + this._HelperService.GetDateSByFormat(this._Redeem.CompareEndDate, 'DD MMM YY');

    this.lastmonthtext = this._HelperService.GetDateSByFormat(this._Revenue.CompareStartDate, 'MMM YYYY');
  }

  public _SaleSalesReportReset(): void {


  }

  public _SaleDailyReportGetActualData(Type): void {

    this.SalelineChartData[0].data = [];
    this.RedeemlineChartData[0].data = [];
    this.RevenuelineChartData[0].data = [];

    if (Type == this.Types.hour) {
      this.GetRewardReport(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetMerchantSaleOverview(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetMerchantActivity(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetPurchaseOverview(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual')
      this.GetVisitReport(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetPurchaseOverview(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual')
      this.GetRewardOverview(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual')
      this.GetMerchantCategories(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual')
      this.RefreshTerminalStatusCount();
      // this.TUTr_Setup(this._Sale.ActualStartDate, this._Sale.ActualEndDate);
    } else if (Type == this.Types.week) {
      this.GetMerchantActivity(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetRewardReport(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetVisitReport(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetMerchantSaleOverview(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetPurchaseOverview(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual')
      this.GetRewardOverview(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual')
      this.GetMerchantCategories(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual')
      // this.TUTr_Setup(this._Sale.ActualStartDate, this._Sale.ActualEndDate);

    } else if (Type == this.Types.month || Type == this.Types.year) {
      this.GetRewardReport(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetMerchantActivity(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetMerchantSaleOverview(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetVisitReport(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetPurchaseOverview(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual')
      this.GetRewardOverview(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual')
      this.GetMerchantCategories(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual')
      // this.TUTr_Setup(this._Sale.ActualStartDate, this._Sale.ActualEndDate);

    }

    //this.GetSalesReport(this._Yearly.ActualStartDate, this._Yearly.ActualEndDate, this._Yearly.ActualData, this.Types.year, 'actual');

  }

  //#endregion

  //#region Redeem Sales Report 

  public RedeemlineChartData: ChartDataSets[] = [
    { data: [], label: 'Current Week' },
    { data: [], borderDash: [10, 5], label: 'Week Compared' },
  ];
  public RedeemlineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];
  public RedeemlineChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
  showRedeemChart = true;

  public _Redeem: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    CompareSalesTotal: 0,
    ActualSalesTotal: 0,
    SalesAmountDifference: 0
  }


  public _RedeemSalesReportReset(): void {


  }


  //#endregion

  //#region Revenue Sales Report 

  showRevenueChart = true;

  public _Revenue: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    CompareSalesTotal: 0,
    ActualSalesTotal: 0,
    SalesAmountDifference: 0
  }

  public _RevenueSalesReportReset(): void {


  }


  //#endregion

  //#region Customer Sales Report 

  public _Customer: any = {
    TotalCustomers: 0
  }

  //#endregion
  MerchantRoute(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveMerchant,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveMerchantReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveMerchantReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.RewardPercentage,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);

  }

  public _GetoverviewSummary = [];
  public TodayStartTime = null;
  public TodayEndTime = null;
  public NewMerchants: any = []
  public LowBalance: any = []
  private MerchantData = {
    Task: 'gettopsale',
    StartDate: null,
    EndDate: null,
    Limit:5,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetMerchantSaleOverview(StartDateTime, EndDateTime, Data: ODealData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.MerchantData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.MerchantData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.MAnalytics, this.MerchantData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetoverviewSummary = _Response.Result.Data;
         

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public _GetPurchaseoverviewSummary = []
  SaleElement1 : any = {};
  SaleElement2 : any = {};
  SaleElement3 : any = {};
  SaleElement4 : any = {};
  private pSaleData = {
    Task: 'getsalerange',
    StartDate: null,
    EndDate: null,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetPurchaseOverview(StartDateTime, EndDateTime, Data: ODealData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pSaleData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pSaleData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.MAnalytics, this.pSaleData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetPurchaseoverviewSummary = _Response.Result as any;
          this.DataSales = [
            { data: [this._GetPurchaseoverviewSummary[0].Count], label: '1M-2M'  },
            { data: [this._GetPurchaseoverviewSummary[1].Count], label: '2M-3M' },
            { data: [this._GetPurchaseoverviewSummary[2].Count], label: '3M-4M'  },
            { data: [this._GetPurchaseoverviewSummary[3].Count], label: 'More Than 4M'  },
          ];

          this.SaleElement1 = this._GetPurchaseoverviewSummary[0];
          this.SaleElement2 = this._GetPurchaseoverviewSummary[1];
          this.SaleElement3 = this._GetPurchaseoverviewSummary[2];
          this.SaleElement4 = this._GetPurchaseoverviewSummary[3];

          // for (let index = 0; index < this._GetPurchaseoverviewSummary.length; index++) {
          //   const element = this._GetPurchaseoverviewSummary[index];
            
          // }  


          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public _GetRewardSummary = []
  RewardElement1 : any = {};
  RewardElement2 : any = {};
  RewardElement3 : any = {};
  RewardElement4 : any = {};
  private pRewardData = {
    Task: 'getrewardrange',
    StartDate: null,
    EndDate: null,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetRewardOverview(StartDateTime, EndDateTime, Data: ODealData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pRewardData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pRewardData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.MAnalytics, this.pRewardData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetRewardSummary = _Response.Result as any;
          this.DataReward = [
            { data: [this._GetRewardSummary[0].Count], label: '1M-2M'  },
            { data: [this._GetRewardSummary[1].Count], label: '2M-3M' },
            { data: [this._GetRewardSummary[2].Count], label: '3M-4M'  },
            { data: [this._GetRewardSummary[3].Count], label: 'More Than 4M'  },
          ];
          this.RewardElement1 = this._GetRewardSummary[0];
          this.RewardElement2 = this._GetRewardSummary[1];
          this.RewardElement3 = this._GetRewardSummary[2];
          this.RewardElement4 = this._GetRewardSummary[3];

          // for (let index = 0; index < this._GetPurchaseoverviewSummary.length; index++) {
          //   const element = this._GetPurchaseoverviewSummary[index];
            
          // }  


          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public _GetGetMerchantCategories = []
  CategoryElement1 : any = {};
  CategoryElement2 : any = {};
  CategoryElement3 : any = {};
  CategoryElement4 : any = {};

  private pGetMerchantCategoriesData = {
    Task: 'gettopcategory',
    StartDate: null,
    EndDate: null,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetMerchantCategories(StartDateTime, EndDateTime, Data: ODealData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pGetMerchantCategoriesData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pGetMerchantCategoriesData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.MAnalytics, this.pGetMerchantCategoriesData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetGetMerchantCategories = _Response.Result.Data as any;
          // console.log(this._GetGetMerchantCategories)
         
          //this.CategoryElement1 = this._GetGetMerchantCategories[0];
          this.CategoryElement1 = this._GetGetMerchantCategories[0];
          this.CategoryElement2 = this._GetGetMerchantCategories[1];
          this.CategoryElement3 = this._GetGetMerchantCategories[2];
          this.CategoryElement4 = this._GetGetMerchantCategories[3];
          // console.log(this.CategoryElement1)
          this.MerchantCategotries = [
            { data: [this._GetGetMerchantCategories[0].Count], label: this._GetGetMerchantCategories[0].Name  },
            { data: [this._GetGetMerchantCategories[1].Count], label: this._GetGetMerchantCategories[1].Name },
            { data: [this._GetGetMerchantCategories[2].Count], label: this._GetGetMerchantCategories[2].Name  },
            { data: [this._GetGetMerchantCategories[3].Count], label: this._GetGetMerchantCategories[3].Name  },
          ];
          // for (let index = 0; index < this._GetPurchaseoverviewSummary.length; index++) {
          //   const element = this._GetPurchaseoverviewSummary[index];
            
          // }  


          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  //#region Yearly Sales Report 

  public YearlylineChartData: ChartDataSets[] = [
    { data: [], label: 'Current Year' },
    { data: [], borderDash: [10, 5], label: 'Compared Year' },
  ];
  public YearlylineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];
  public YearlylineChartLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
  showYearlyChart = true;

  public _Yearly: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,

    CompareSalesTotal: 0,
    ActualSalesTotal: 0,

    SalesAmountDifference: 0
  }

  //#endregion

  //#region Sales History General Method 
  public MerchantActivity:any = {}
  private pData = {
    Task: 'getactivitystatuscount',
    StartDate: null,
    EndDate: null,
    // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };
  GetMerchantActivity(StartDateTime, EndDateTime, Data: ODealData[], Type, LineType: string) {


    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);
    this.pData.Type = Type;

    if (Type == this.Types.month) {
      this.pData.Type = this.Types.day;
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.MAnalytics, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.MerchantActivity = _Response.Result;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }
  public TopRewards = [];

  private pRewardsData = {
    Task: 'gettoprewards',
    StartDate: null,
    EndDate: null,
    Limit :5,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
   // Type: null
  };
  GetRewardReport(StartDateTime, EndDateTime, Data: ODealData[], Type, LineType: string) {
    this._HelperService.IsFormProcessing = true;
    this.pRewardsData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pRewardsData.EndDate = this._HelperService.DateInUTC(EndDateTime);
  //  this.pRewardsData.Type = this.Types.hour;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.MAnalytics, this.pRewardsData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this.TopRewards = _Response.Result.Data;

          


        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }
 
  public MerchantsVisit = [];
  private pVisitsData = {
    Task: 'gettopvisits',
    StartDate: null,
    EndDate: null,
    Limit:5,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };
  GetVisitReport(StartDateTime, EndDateTime, Data: ODealData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pVisitsData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pVisitsData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    this.pVisitsData.Type = Type;

    if (Type == this.Types.day) {
      this.pData.Type = 'week';
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.MAnalytics, this.pVisitsData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.MerchantsVisit = _Response.Result.Data;
          }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }



  public StoresList_Config: OList;
  StoresList_Setup() {
    this.StoresList_Config = {
      Id: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStorelocations,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      Title: "Available Stores",
      StatusType: "default",
      DefaultSortExpression: "CreateDate desc",
      PageRecordLimit:100,
      Sort:
      {
          SortDefaultName: null,
          SortDefaultColumn: 'CreateDate',
          SortName: null,
          SortColumn: '',
          SortOrder: 'desc',
          SortOptions: [],
      },
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: " Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Contact No",
          SystemName: "ContactNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Total Terminals",
          SystemName: "TotalTerminals",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Active Terminals",
          SystemName: "ActiveTerminals",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Cashiers",
          SystemName: "Cashiers",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        }, {
          DisplayName: "RM",
          SystemName: "RmDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.StoresList_Config = this._DataHelperService.List_Initialize(
      this.StoresList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Stores,
      this.StoresList_Config
    );

    this.StoresList_GetData();
  }

  StoresList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.StoresList_Config
    );
    this.StoresList_Config = TConfig;
  }



  //#endregion

  computePerc(num: OSalesTrend): any {
    if (num.CompareSalesAmount == 0) {
      return '100 %';
    }
    if (num.ActualSalesAmount > num.CompareSalesAmount) {
      return Math.round(((num.ActualSalesAmount - num.CompareSalesAmount) / num.CompareSalesAmount) * 100) + ' %';
    } else if (num.ActualSalesAmount < num.CompareSalesAmount) {
      return Math.round(((num.CompareSalesAmount - num.ActualSalesAmount) / num.CompareSalesAmount) * 100) + ' %';
    } else {
      return '0 %';
    }
  }

  computeFlag(num: OSalesTrend): number {
    if (num.ActualSalesAmount > num.CompareSalesAmount) {
      return 1;
    } else if (num.ActualSalesAmount < num.CompareSalesAmount) {
      return -1;
    } else {
      return 0;
    }
  }

  getAbsolute(num: number): any {
    // return Math.abs(num);
    return num;
  }



  hideSalePicker: boolean = true;
  hideRedeemPicker: boolean = true;
  hideRevenuePicker: boolean = true;
  hideYearlyPicker: boolean = true;

  ShowHideCalendar(type: string) {
    switch (type) {
      case this._HelperService.AppConfig.DatePickerTypes.hour: {
        this.hideSalePicker = !this.hideSalePicker;
      }

        break;
      case this._HelperService.AppConfig.DatePickerTypes.week: {
        this.hideRedeemPicker = !this.hideRedeemPicker;
      }

        break;
      case this._HelperService.AppConfig.DatePickerTypes.month: {
        this.hideRevenuePicker = !this.hideRevenuePicker;
      }

        break;
      case this._HelperService.AppConfig.DatePickerTypes.year: {
        this.hideYearlyPicker = !this.hideYearlyPicker;
      }

        break;

      default:
        break;
    }
  }

  CloseRowModal(): void { }


onMouseOver(infoWindow, $event: MouseEvent) {
  
  infoWindow.open();
}

onMouseOut(infoWindow, $event: MouseEvent) {
  infoWindow.close();
}
}


export class OAccountOverview {
  public TerminalStatus?: any;
  public TotalTerminals?: number;
  public TotalSale?: number;
  public AverageTransactionAmount?: number;
  public TotalTransactions?: number;
  public ActiveTerminalsPerc?: number;
  public IdleTerminalsPerc?: number;
  public DeadTerminalsPerc?: number;
  public UnusedTerminalsPerc?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public Idle?: number;
  public Total?: number;

  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
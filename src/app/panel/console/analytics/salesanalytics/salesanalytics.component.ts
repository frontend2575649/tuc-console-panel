import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DaterangePickerComponent } from 'ng2-daterangepicker';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OList, OResponse } from '../../../../service/service';
declare var moment: any;

@Component({
    selector: 'salesanalytics',
    templateUrl: './salesanalytics.component.html',
    styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUSalesAnalyticsComponent implements OnInit {
    @ViewChild(DaterangePickerComponent)
    private picker: DaterangePickerComponent;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {
    }
    public TodayStartTime = null;
    public TodayEndTime = null;
    Type = 5;
    StartTime = null;
    StartTimeS = null;
    EndTime = null;
    EndTimeS = null;
    CustomType = 1;
    // StartTimeCustom = null;
    // EndTimeCustom = null;

    ngOnInit() {
        this._HelperService.FullContainer = false;
        if (this.StartTime == undefined) {
            this.StartTime = moment().subtract(15, 'day').endOf('day');
            this.EndTime = moment().subtract(1, 'day').endOf('day');
        }
        if (this.TodayStartTime == undefined) {
            // this.TodayStartTime = moment().subtract(2, 'day').startOf('day');
            // this.TodayEndTime = moment().subtract(2, 'day').add(1, 'minutes');

            this.TodayStartTime = moment().startOf('day');
            this.TodayEndTime = moment().add(1, 'minutes');
        }
        this.LoadData();
    }

    UserAnalytics_DateChange(Type) {
        this.Type = Type;
        var SDate;
        if (Type == 1) { // today
            SDate =
            {
                start: moment().startOf('day'),
                end: moment().endOf('day'),
            }
        }
        else if (Type == 2) { // yesterday
            SDate =
            {
                start: moment().subtract(1, 'days').startOf('day'),
                end: moment().subtract(1, 'days').endOf('day'),
            }
        }
        else if (Type == 3) {  // this week
            SDate =
            {
                start: moment().startOf('isoWeek'),
                end: moment().endOf('isoWeek'),
            }
        }
        else if (Type == 4) { // last week
            SDate =
            {
                start: moment().subtract(1, 'weeks').startOf('isoWeek'),
                end: moment().subtract(1, 'weeks').endOf('isoWeek'),
            }
        }
        else if (Type == 5) { // this month
            SDate =
            {
                start: moment().startOf('month'),
                end: moment().endOf('month'),
            }
        }
        else if (Type == 6) { // last month
            SDate =
            {
                start: moment().startOf('month').subtract(1, 'months'),
                end: moment().startOf('month').subtract(1, 'days'),
            }
        }
        else if (Type == 7) {
            SDate =
            {
                start: new Date(2017, 0, 1, 0, 0, 0, 0),
                end: moment().endOf('day'),
            }
        }
        // if (this.picker.datePicker != undefined) {
        //     this.picker.datePicker.setStartDate(SDate.start);
        //     this.picker.datePicker.setEndDate(SDate.end);
        // }
        this.StartTime = SDate.start;
        this.EndTime = SDate.end;
        this.LoadData();
    }
    LoadData() {
        this.StartTimeS = this._HelperService.GetDateS(this.StartTime);
        this.EndTimeS = this._HelperService.GetDateS(this.EndTime);
        this._HelperService.GetSalesOverview(this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.AccountType.AcquirerId, 0, 0);
        this._HelperService.GetSalesSummary(this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.AccountType.AcquirerId, 0, 0, this.StartTime, this.EndTime);
        this._HelperService.GetSalesHistory(this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.AccountType.AcquirerId, 0, 0, this.StartTime, this.EndTime);
        this._HelperService.GetUserCounts(this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.AccountType.AcquirerId, 0, 0, this.StartTime, this.EndTime);
        this._HelperService.GetCardTypeSalesSummary(this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.AccountType.AcquirerId, 0, 0, this.StartTime, this.EndTime);
    }

    GetAccountOverviewLite() {
        this._HelperService.IsFormProcessing = true;
        var Data = {
            Task: 'getaccountoverviewlite',
            StartTime: this.TodayStartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: this.TodayEndTime, // moment().add(2, 'days'),
            UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._AccountOverview = _Response.Result as OAccountOverview;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }
    public TUTr_Config: OList;
    TUTr_Setup() {
        this.TUTr_Config =
        {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetSaleTransactions,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore,
            Title: 'Sales History',
            StatusType: 'transaction',
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '='),
            Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
            StatusName:"Success",
            Type: this._HelperService.AppConfig.ListType.All,
            DefaultSortExpression: 'TransactionDate desc',
            RefreshCount: false,
            PageRecordLimit: 6,
            TableFields: [
                {
                    DisplayName: '#',
                    SystemName: 'ReferenceId',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Show: true,
                    Search: false,
                    Sort: false,
                    // NavigateField: 'UserAccountKey',
                    // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Customer,
                },
                {
                    DisplayName: 'Date',
                    SystemName: 'TransactionDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Show: true,
                    Search: false,
                    Sort: true,
                    IsDateSearchField: true,
                },
            ]

        }
        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this.TUTr_GetData();
    }
    TUTr_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    }
    public _AccountOverview: OAccountOverview =
        {
            ActiveMerchants: 0,
            ActiveMerchantsDiff: 0,
            ActiveTerminals: 0,
            ActiveTerminalsDiff: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardPurchaseAmountDiff: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardPurchaseAmountDiff: 0,
            Merchants: 0,
            PurchaseAmount: 0,
            PurchaseAmountDiff: 0,
            Terminals: 0,
            Transactions: 0,
            TransactionsDiff: 0,
            UnusedTerminals: 0,
            IdleTerminals: 0,
            DeadTerminals: 0,
        }
}


export class OAccountOverview {

    public DeadTerminals?: number;
    public IdleTerminals?: number;
    public UnusedTerminals?: number;
    public Merchants: number;
    public ActiveMerchants: number;
    public ActiveMerchantsDiff: number;
    public Terminals: number;
    public ActiveTerminals: number;
    public ActiveTerminalsDiff: number;
    public Transactions: number;
    public TransactionsDiff: number;
    public PurchaseAmount: number;
    public PurchaseAmountDiff: number;
    public CashRewardPurchaseAmount: number;
    public CashRewardPurchaseAmountDiff: number;
    public CardRewardPurchaseAmount: number;
    public CardRewardPurchaseAmountDiff: number;
}
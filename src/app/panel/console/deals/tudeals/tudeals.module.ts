import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { MainPipe } from '../../../../service/main-pipe.module'
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { TUDealsComponent } from "./tudeals.component";
import { TimepickerModule } from 'ngx-bootstrap';
import { ImageCropperModule } from 'ngx-image-cropper';
import { DatePipe } from "@angular/common";
import { MerchantguardGuard } from "src/app/service/guard/merchantguard.guard";

const routes: Routes = [{ path: "",canActivate:[MerchantguardGuard], component: TUDealsComponent }];
const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 1,
};
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUDealsRoutingModule { }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TUDealsRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    MainPipe,
    ImageCropperModule,
    InputFileModule.forRoot(config),
    TimepickerModule.forRoot()
  ],
  declarations: [TUDealsComponent]
})
export class TUDealsModule { }

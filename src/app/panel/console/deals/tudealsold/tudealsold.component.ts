import { ChangeDetectorRef, Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import { InputFileComponent, InputFile } from 'ngx-input-file';
import swal from "sweetalert2";
import { ImageCroppedEvent } from 'ngx-image-cropper';
declare var $: any;
declare var moment: any;


import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Observable, Subscription } from 'rxjs';
import { ChangeContext } from 'ng5-slider';
declare var moment: any;
import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: "tu-tudealsold",
  templateUrl: "./tudealsold.component.html",
})
export class TUDealsOldComponent implements OnInit, OnDestroy {
  CurrentImagesCount: number = 0;
  ShowImagePicker: boolean = true;

  _DealConfig =
    {

      Images: [],
      DealImages: [],

      StartDateConfig: {
      },
      EndDateConfig: {
      },
      DefaultStartDate: null,
      DefaultEndDate: null,
      StartDate: null,
      EndDate: null,
    }


  _ImageManager =
    {
      TCroppedImage: null,
      ActiveImage: null,
      ActiveImageName: null,
      ActiveImageSize: null,
      Option: {
        MaintainAspectRatio: "true",
        MinimumWidth: 800,
        MinimumHeight: 400,
        MaximumWidth: 800,
        MaximumHeight: 400,
        ResizeToWidth: 800,
        ResizeToHeight: 400,
        Format: "jpg",
      }
    }


  private InitImagePicker(InputFileComponent: InputFileComponent) {
    if (InputFileComponent != undefined) {
      this.CurrentImagesCount = 0;
      this._HelperService._InputFileComponent = InputFileComponent;
      InputFileComponent.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
  }
  FlashTimings: any = {
    Start: new Date(),
    End: new Date()
  }

  DealTimings: any = {
    Start: new Date(),
    End: new Date()
  }

  public showFlashDeals: boolean = true;
  public ResetFilterControls: boolean = true;
  UpdateStatusArray: any = ['deal.draft', 'deal.approvalpending', 'deal.approved', 'deal.published', 'deal.paused', 'deal.expired',];
  public _ObjectSubscription: Subscription = null;
  @ViewChild("terminal")
  private InputFileComponent_Term: InputFileComponent;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService,
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = true;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;

  }
  SelectStatusApprovalpending: boolean = false
  SelectStatusDraft: boolean = false
  SelectStatusRunning: boolean = true
  SelectStatusPaused: boolean = false
  SelectStatusUpcoming: boolean = false
  SelectStatusExpired: boolean = false
  currentdate: any;
  ngOnInit() {
    this.currentdate = moment().format('DD-MM-YYYY hh:mm a'),
      this.TUTr_Filter_Merchants_Load();
    this._HelperService.ValidateData();
    this._HelperService.StopClickPropogation();
    Feather.replace();
    this.ListType = 3;
    this.TodayStartTime = this._HelperService.AppConfig.DefaultStartTimeAll;
    this.TodayEndTime = this._HelperService.AppConfig.DefaultEndTimeToday;
    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];


    });
    this.DealsList_Setup();
    this.GetSalesOverview();
    this.DealsList_Filter_Owners_Load();
    this.InitColConfig();
    this.Form_AddUser_Load();
    this.Form_Promote_Load()
    this.Form_ManagePromote_Load()
    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.DealsList_GetData();
    // });



    //deal date
    this._DealConfig.DefaultStartDate = moment();
    this._DealConfig.DefaultEndDate = moment().add(1, 'days').endOf("day");
    this._DealConfig.StartDate = this._DealConfig.DefaultStartDate;
    this._DealConfig.EndDate = this._DealConfig.DefaultEndDate;
    this._DealConfig.StartDateConfig = {
      autoUpdateInput: false,
      singleDatePicker: true,
      timePicker: true,
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      showDropdowns: true,
      startDate: this._HelperService.DateInUTC(moment()),
      endDate: this._HelperService.DateInUTC(moment().endOf("day")),
      minDate: moment(),
    };
    this._DealConfig.EndDateConfig = {
      autoUpdateInput: false,
      singleDatePicker: true,
      timePicker: true,
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      showDropdowns: true,
      startDate: this._HelperService.DateInUTC(moment()),
      endDate: this._HelperService.DateInUTC(moment().endOf("day")),
      minDate: moment(),
    };
    //end deal date


  }

  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {
    }
  }


  MerchantRoute(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveMerchant,
      {
        ReferenceKey: ReferenceData.AccountKey,
        ReferenceId: ReferenceData.AccountId,
        DisplayName: ReferenceData.AccountDisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveMerchantReferenceKey = ReferenceData.AccountKey;
    this._HelperService.AppConfig.ActiveMerchantReferenceId = ReferenceData.AccountId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.RewardPercentage,
      ReferenceData.AccountKey,
      ReferenceData.AccountId,
    ]);

  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  AddNewDeal() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.AddDeal
    ]);
  }


  //#region merchant 

  public TUTr_Filter_Merchant_Option: Select2Options;
  public TUTr_Filter_Merchant_Selected = 0;
  TUTr_Filter_Merchants_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },

      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Merchant_Option = {
      placeholder: 'Search By Merchant',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }

  TUTr_Filter_Merchants_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.DealsList_Config,
      this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant
    );
    this.MerchantEventProcessing(event);
  }

  MerchantEventProcessing(event: any): void {
    if (event.value == this.TUTr_Filter_Merchant_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
      this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.DealsList_Config.SearchBaseConditions);
      this.TUTr_Filter_Merchant_Selected = 0;
    }
    else if (event.value != this.TUTr_Filter_Merchant_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
      this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.DealsList_Config.SearchBaseConditions);
      this.TUTr_Filter_Merchant_Selected = event.value;
      this.DealsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '='));
    }
    this.DealsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);

  }

  //#endregion

  //#region merchantlist
  public ListType: number;

  public DealsList_Config: OList;
  DealsList_Setup1() {
    this.DealsList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.Getdeals,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals,
      Title: "Available Stores",
      StatusType: "Deal",
      Status: this._HelperService.AppConfig.StatusList.defaultaccountitem1,
      StatusName: 'Rejected',

      DefaultSortExpression: "CreateDate desc",

      TableFields: [

        {
          DisplayName: " Merchant Name",
          SystemName: "AccountDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: " Title",
          SystemName: "Title",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Stores",
          SystemName: "Locations",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Description",
          SystemName: "Description",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Budget",
          SystemName: "Budget",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Maximum Unit Sale",
          SystemName: "MaximumUnitSale",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Payable To Merchant",
          SystemName: "MerchantAmount",
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Sold Deals",
          SystemName: "TotalPurchase",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",

        },
        {
          DisplayName: "Total Availability",
          SystemName: "TotalAvailable",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",

        }
        , {
          DisplayName: "Category Name",
          SystemName: "CategoryName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: 'End Date',
          SystemName: "EndDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: 'Start Date',
          SystemName: "StartDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]


    };
    this.DealsList_Config.ListType = this.ListType;
    this.DealsList_Config.SearchBaseCondition = "";

    if (this.DealsList_Config.ListType == 1) //  approvalpending
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.approvalpending', "=");
      this.SelectStatusApprovalpending = true;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }
    else if (this.DealsList_Config.ListType == 2) //  approved
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.approved', "=");
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = true;
      this.SelectStatusExpired = false;
    }
    else if (this.DealsList_Config.ListType == 3) // published
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.published', "=");
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = true;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }
    else if (this.DealsList_Config.ListType == 4) // paused
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.paused', "=");
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = true;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }
    else if (this.DealsList_Config.ListType == 5) // draft
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.draft', "=");
      // this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.expired', "=");
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = true;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }
    else if (this.DealsList_Config.ListType == 6) // expired
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.expired', "=");

      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = true;
    }

    else {
      this.DealsList_Config.DefaultSortExpression = 'CreateDate desc';
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }

    this.DealsList_Config = this._DataHelperService.List_Initialize(
      this.DealsList_Config
    );


    this.DealsList_GetData();

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Deals,
      this.DealsList_Config
    );

    this.DealsList_GetData();
  }

  DealsList_Setup() {
    this.DealsList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.Getdeals,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals,
      Title: "Available Stores",
      StatusType: "Deal",

      DefaultSortExpression: "CreateDate desc",

      TableFields: [
        {
          DisplayName: " Merchant Name",
          SystemName: "AccountDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: " Title",
          SystemName: "Title",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Stores",
          SystemName: "Locations",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Description",
          SystemName: "Description",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Budget",
          SystemName: "Budget",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Maximum Unit Sale",
          SystemName: "MaximumUnitSale",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Payable To Merchant",
          SystemName: "MerchantAmount",
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Sold Deals",
          SystemName: "TotalPurchase",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",

        },
        {
          DisplayName: "Total Availability",
          SystemName: "TotalAvailable",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",

        }
        , {
          DisplayName: "Category Name",
          SystemName: "CategoryName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: 'End Date',
          SystemName: "EndDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: 'Start Date',
          SystemName: "StartDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.DealsList_Config.ListType = this.ListType;
    this.DealsList_Config.SearchBaseCondition = "";

    if (this.DealsList_Config.ListType == 1) //  approvalpending
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.approvalpending', "=");
      this.SelectStatusApprovalpending = true;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }
    else if (this.DealsList_Config.ListType == 2) //  approved
    {
      this.DealsList_Config.Type = "upcoming";

      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = true;
      this.SelectStatusExpired = false;
    }
    else if (this.DealsList_Config.ListType == 3) // published
    {
      this.DealsList_Config.Type = "running";

      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = true;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }
    else if (this.DealsList_Config.ListType == 4) // paused
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.paused', "=");
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = true;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }
    else if (this.DealsList_Config.ListType == 5) // draft
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.draft', "=");

      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = true;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }
    else if (this.DealsList_Config.ListType == 6) // expired
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.expired', "=");

      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = true;
    }
    else {
      this.DealsList_Config.DefaultSortExpression = 'CreateDate desc';
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }

    this.DealsList_Config = this._DataHelperService.List_Initialize(
      this.DealsList_Config
    );


    this.DealsList_GetData();

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Deals,
      this.DealsList_Config
    );

    this.DealsList_GetData();

  }
  DealsList_ToggleOption(event: any, Type: any) {
    if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
      event.data = {
        DealMin: this.Deal_AvailableRangeMinAmount,
        DealMax: this.Deal_AvailableRangeMaxAmount,
        DealPurchaseMin: this.Deal_SoldRangeMinAmount,
        DealPurchaseMax: this.Deal_SoldRangeMaxAmount
      }
    }

    if (event != null) {
      for (let index = 0; index < this.DealsList_Config.Sort.SortOptions.length; index++) {
        const element = this.DealsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.DealsList_Config
    );

    this.DealsList_Config = this._DataHelperService.List_Operations(
      this.DealsList_Config,
      event,
      Type
    );

    if (
      (this.DealsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.DealsList_GetData();
    }

  }
  timeout = null;
  DealsList_ToggleOptionSearch(event: any, Type: any) {



    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.DealsList_Config.Sort.SortOptions.length; index++) {
          const element = this.DealsList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.DealsList_Config
      );

      this.DealsList_Config = this._DataHelperService.List_Operations(
        this.DealsList_Config,
        event,
        Type
      );

      if (
        (this.DealsList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.DealsList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);
  }
  DealsList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.DealsList_Config
    );
    this.DealsList_Config = TConfig;
  }
  DealsList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveDeal,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        AccountKey: ReferenceData.AccountKey,
        AccountId: ReferenceData.AccountId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
      }
    );
    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
    this._HelperService.AppConfig.ActiveAccountKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveAccountId = ReferenceData.ReferenceId;


    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Analytics,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
      ReferenceData.AccountId,
      ReferenceData.AccountKey,

    ]);


  }

  //#endregion
  enabledealview: boolean = false;
  DealerList_ListTypeChange(Type) {
    this.ListType = Type;
    this.DealsList_Setup();
    this.GetSalesOverview();


    if (this.ListType == 0) {
      this.enabledealview = true;
    }
    else {
      this.enabledealview = false;
    }

  }



  public _GetoverviewSummary: any = {};
  public TodayStartTime = null;
  public TodayEndTime = null;
  private pData = {
    Task: 'getdealsoverview',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),

    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };

  RejectedDeal: number = 0
  DisableRejectedDiv: any;
  GetSalesOverview() {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetoverviewSummary = _Response.Result as any;
          this.RejectedDeal = _Response.Result.Rejected;
          if (this.RejectedDeal != 0) {
            this.DisableRejectedDiv = true;
          }
          else {
            this.DisableRejectedDiv = false;
          }


          this._ChangeDetectorRef.detectChanges();


          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }



  //#region OwnerFilter

  public DealsList_Filter_Owners_Option: Select2Options;
  public StoresList_Filter_Owners_Selected = null;
  DealsList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };

    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.DealsList_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  DealsList_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.DealsList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.StoresList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.StoresList_Filter_Owners_Selected,
        "="
      );
      this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.DealsList_Config.SearchBaseConditions
      );
      this.StoresList_Filter_Owners_Selected = null;
    } else if (event.value != this.StoresList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.StoresList_Filter_Owners_Selected,
        "="
      );
      this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.DealsList_Config.SearchBaseConditions
      );
      this.StoresList_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.DealsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.StoresList_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.DealsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetSalesRanges(): void {
    this.Deal_AvailableRangeMinAmount = this._HelperService.AppConfig.DealMinimumLimit;
    this.Deal_AvailableRangeMaxAmount = this._HelperService.AppConfig.DealMaximumLimit;
    this.Deal_SoldRangeMinAmount = this._HelperService.AppConfig.DealPurchaseMinimumLimit;
    this.Deal_SoldRangeMaxAmount = this._HelperService.AppConfig.DealPurchaseMaximumLimit;
  }

  SetOtherFilters(): void {
    this.DealsList_Config.SearchBaseConditions = [];



    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant));
    if (CurrentIndex != -1) {
      this.TUTr_Filter_Merchant_Selected = null;
      this.MerchantEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.DealsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    this.SetSalesRanges();
    //#endregion

    this.DealsList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.DealsList_Config);

    this.SetOtherFilters();
    this.SetSalesRanges();
    this.DealsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      // inputAttributes: {
      //   autocapitalize: "off",
      //   autocorrect: "off",
      //   maxLength: "4",
      //   minLength: "4",
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Deals,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Deals
        );
        this._FilterHelperService.SetMerchantConfig(this.DealsList_Config);
        this.DealsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this.SetSearchRanges();
    this._HelperService.MakeFilterSnapPermanent();
    this.DealsList_GetData();

    if (ButtonType == 'Sort') {
      $("#DealsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#DealsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }




  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantSalesConfig(this.DealsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    this.SetSalesRanges();

    this.DealsList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }



  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.DealsList_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  CloseRowModal(index: number): void {
    $("#SubAccountsList_rdropdown_" + index).dropdown('toggle');
  }
  Update_RowSelected(ReferenceData: any): void {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.EditDeal,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
      ReferenceData.AccountId,
      ReferenceData.AccountKey,
    ]);
  }


  DeleteDeal(ReferenceData): void {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteDeal,
      text: this._HelperService.AppConfig.CommonResource.DeleteDealHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PData =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.deletedeal,
          ReferenceId: ReferenceData.ReferenceId,
          ReferenceKey: ReferenceData.ReferenceKey,
          AccountId: ReferenceData.AccountId,
          AccountKey: ReferenceData.AccountKey
          // StatusCode: this.selectedStatusItem.statusCode,

        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, PData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Status Updated successfully. It will take upto 5 minutes to update changes.");
              this.DealsList_Setup();
              this._HelperService.IsFormProcessing = false;
              this._HelperService.CloseModal('exampleModal')
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });

      }
    });




  }

  StatusUpdate(ReferenceData, i) {
    swal({
      title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      input: 'text',
      inputClass: 'swalText',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
      inputValidator: function (value) {
        if (value === '' || value.length < 4) {
          return 'Enter your 4 digit pin!'
        }
      },

    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var P1Data =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.updatedealstatus,
          ReferenceId: ReferenceData.ReferenceId,
          ReferenceKey: ReferenceData.ReferenceKey,
          AccountId: ReferenceData.AccountId,
          AccountKey: ReferenceData.AccountKey,
          StatusCode: this.UpdateStatusArray[i],
          AuthPin: result.value
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, P1Data);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Status Updated successfully. It will take upto 5 minutes to update changes.");
              this.DealsList_Setup();
              this.GetSalesOverview();
              this._HelperService.IsFormProcessing = false;
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });

      }
    });



  }

  Approve_RequestBody(ApproveReferencedata): void {
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'approvedeal',

      'ReferenceId': ApproveReferencedata.ReferenceId,
      'ReferenceKey': ApproveReferencedata.ReferenceKey,

    };
    //#region Set Schedule 

    //#endregion

    return formRequest;

  }

  ApproveReferencedata: any;
  Deal_Approve_popup(dealapprove) {
    this.ApproveReferencedata = dealapprove;
    this._HelperService.OpenModal('Deal_approve1');
  }

  Deal_Approve() {
    var Req = this.Approve_RequestBody(this.ApproveReferencedata);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, Req);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);

          this.ClearScheduleUpdate();
          this._HelperService.CloseModal('Deal_approve1');
          this.DealsList_GetData();
        }
        else {
          this.SelectedDeal.Schedule = [];
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.SelectedDeal.Schedule = [];
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });

  }

  DenyReferencedata: any
  Deal_deny(Deal_deny) {
    this.DenyReferencedata = Deal_deny;
    this._HelperService.OpenModal('Deal_deny');
  }


  Deny_RequestBody(_FormValue): void {
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'rejectdeal',

      'Comment': _FormValue.comment,
      'ReferenceId': this.DenyReferencedata.ReferenceId,
      'ReferenceKey': this.DenyReferencedata.ReferenceKey,

    };
    //#region Set Schedule 

    //#endregion

    return formRequest;

  }

  deal_deny_confirm() {
    var _FormValue = this.Form_AddUser.value;
    var Req = this.Deny_RequestBody(_FormValue);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, Req);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.Form_AddUser_Clear();
          //this.ClearScheduleUpdate();
          this._HelperService.CloseModal('Deal_deny');
          this.DealsList_GetData();
        }
        else {
          this.SelectedDeal.Schedule = [];
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.SelectedDeal.Schedule = [];
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  Form_AddUser: FormGroup;
  Form_AddUser_Load() {


    this.Form_AddUser = this._FormBuilder.group({

      comment: [null, Validators.required],

    });
  }

  Form_AddUser_Clear() {
    this.Form_AddUser_Load();
  }


  FlashDeal: any = {};
  PromoteDeal: any = {};
  ManagePromoteDeal: any = {};
  FlashSentDeal: any = {};

  MarkAsFlash(ReferenceData: any): void {
    this.FlashDeal = {};
    this.FlashDeal.ReferenceKey = ReferenceData.ReferenceKey;
    this.FlashDeal.ReferenceId = ReferenceData.ReferenceId;
    this.GetDealDetails(ReferenceData, 'Flash');
    this._HelperService.OpenModal('FlashDeal');
  }


  MarkAsPromote(ReferenceData: any): void {
    this.PromoteDeal = {};
    this.PromoteDeal.ReferenceKey = ReferenceData.ReferenceKey;
    this.PromoteDeal.ReferenceId = ReferenceData.ReferenceId;
    this.GetDealDetails(ReferenceData, 'Promote');
    this.SelectedBusinessCategories = [];
    this._HelperService.OpenModal('PromoteDeal');
  }


  SelectedDealStartDatePromote: any;
  SelectedDealEndDatePromote: any;
  Selectedurl: any;
  DealTitle: any;
  SelectedDealStartDatePromote1: any;
  SelectedDealEndDatePromote1: any;
  Selectedimage: any;
  ManageAsPromote(ReferenceData: any): void {
    //this.ngOnInit();
    this.GetDealDetails(ReferenceData, '');
    this.ManagePromoteDeal = {};
    this.ManagePromoteDeal.ReferenceKey = ReferenceData.ReferenceKey;
    this.ManagePromoteDeal.ReferenceId = ReferenceData.ReferenceId;
    this.ManagePromoteDeal.Locations = ReferenceData.Locations;

    this.Selectedurl = ReferenceData.Url,
      this.DealTitle = ReferenceData.DealTitle,
      this._HelperService.OpenModal('ManagePromoteDeal');
  }

  ManageStartDateRangeChange(value) {
    this._DealConfig.EndDateConfig = {
      autoUpdateInput: false,
      singleDatePicker: true,
      timePicker: true,
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      showDropdowns: true,
      startDate: value.start,
      minDate: value.start,
    };
    this._DealConfig.StartDate = value.start;
    this._DealConfig.EndDate = value.start;

    this.Form_ManagePromote.patchValue(
      {
        StartDate: value.start.format('DD-MM-YYYY hh:mm a'),
        EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
        CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
      }
    );
  }
  ManageEndDateRangeChange(value) {
    this._DealConfig.EndDate = value.start;

    this.Form_ManagePromote.patchValue(
      {
        EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
        CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
      }
    );
  }


  SaveFlash() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFlashTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFlashHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PData =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.SaveFlashDeal,
          ReferenceId: this.FlashDeal.ReferenceId,
          ReferenceKey: this.FlashDeal.ReferenceKey,
          StartDate: this.FlashSentDeal.StartDate,
          EndDate: this.FlashSentDeal.EndDate,
          StartTime: moment(this.FlashTimings.Start).format('hh:mm'),
          EndTime: moment(this.FlashTimings.End).format('hh:mm')

          // StatusCode: this.selectedStatusItem.statusCode,

        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, PData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess(_Response.Message);
              this.DealsList_Setup();
              this._HelperService.CloseModal('FlashDeal');
              this._HelperService.IsFormProcessing = false;

            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });

      }
    });



  }


  ChangeStartDate(event: any): void {
    this.FlashDeal.StartDate = event.start;
  }

  ChangeEndDate(event: any): void {
    this.FlashDeal.EndDate = event.end;
  }


  UnMarkAsFlash(ReferenceData: any): void {
    this.FlashDeal = {};
    this.FlashDeal.ReferenceKey = ReferenceData.ReferenceKey;
    this.FlashDeal.ReferenceId = ReferenceData.ReferenceId;

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.RemoveFlasTitle,
      text: this._HelperService.AppConfig.CommonResource.RemoveFlashHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PData =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.RemoveFlashDeal,
          ReferenceId: this.FlashDeal.ReferenceId,
          ReferenceKey: this.FlashDeal.ReferenceKey,
          // StatusCode: this.selectedStatusItem.statusCode,

        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, PData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess(_Response.Message);
              this.DealsList_Setup();
              this._HelperService.CloseModal('FlashDeal');
              this._HelperService.IsFormProcessing = false;

            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });

      }
    });

  }


  UnMarkAsPromote(ReferenceData: any): void {
    this.GetDealDetails(ReferenceData, '');

    swal({
      position: "center",
      title: "Unmark Promote Deal?",
      text: "Click Continue to Unmark Promote Deal",
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PData =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.deletedealpromotion,
          "ReferenceId": this._DealDetails.DealPromotionId,
          "ReferenceKey": this._DealDetails.DealPromotionKey,


        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promote, PData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess(_Response.Message);
              this.DealsList_Setup();
              this._HelperService.CloseModal('FlashDeal');
              this._HelperService.IsFormProcessing = false;

            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });

      }
    });

  }


  flashOptionCurrent: string = 'false';
  FlashOptionSelected: any;
  FlashOptions: any = [
    {
      id: 0,
      text: 'All',
      code: 'all'
    },
    {
      id: 1,
      text: 'Flash',
      code: 'flash'
    }
  ];
  FlashList_ToggleOption(): void {
    var ev: any = {
      target: 'IsFlashDeal',
      value: this.flashOptionCurrent
    };
    this.DealList_Filter_Flash_Change(ev);
  }

  // //#region range selectors 
  // Deal_AvailableRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
  // Deal_AvailableRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;

  // Deal_SoldRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
  // Deal_SoldRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;


  // SetSearchRanges(): void {

  //   //#region Invoice 
  //   this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('MaximumUnitSale', this.DealsList_Config.SearchBaseConditions);

  //   var SearchCase = this._HelperService.GetSearchConditionRange('', 'MaximumUnitSale', this.Deal_AvailableRangeMinAmount, this.Deal_AvailableRangeMaxAmount);
  //   if (this.Deal_AvailableRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.Deal_AvailableRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
  //     this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.DealsList_Config.SearchBaseConditions);
  //   }
  //   else {
  //     this.DealsList_Config.SearchBaseConditions.push(SearchCase);
  //   }

  //   //#endregion

  //   //#region Redeem 
  //   this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('MaximumUnitSale', this.DealsList_Config.SearchBaseConditions);

  //   var SearchCase = this._HelperService.GetSearchConditionRange('', 'MaximumUnitSale', this.Deal_SoldRangeMinAmount, this.Deal_SoldRangeMaxAmount);
  //   if (this.Deal_SoldRangeMinAmount == this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit && this.Deal_SoldRangeMaxAmount == this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit) {
  //     this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.DealsList_Config.SearchBaseConditions);
  //   }
  //   else {
  //     this.DealsList_Config.SearchBaseConditions.push(SearchCase);
  //   }

  //   //#endregion


  // }


  Deal_AvailableRangeMinAmount: number = this._HelperService.AppConfig.DealMinimumLimit;
  Deal_AvailableRangeMaxAmount: number = this._HelperService.AppConfig.DealMaximumLimit;
  Deal_SoldRangeMinAmount: number = this._HelperService.AppConfig.DealPurchaseMinimumLimit;
  Deal_SoldRangeMaxAmount: number = this._HelperService.AppConfig.DealPurchaseMaximumLimit;
  SetSearchRanges(): void {
    //#region Invoice 
    this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalAvailable', this.DealsList_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalAvailable', this.Deal_AvailableRangeMinAmount, this.Deal_AvailableRangeMaxAmount);
    if (this.Deal_AvailableRangeMinAmount == this._HelperService.AppConfig.DealMinimumLimit && this.Deal_AvailableRangeMaxAmount == this._HelperService.AppConfig.DealMaximumLimit) {
      this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.DealsList_Config.SearchBaseConditions);
    }
    else {
      this.DealsList_Config.SearchBaseConditions.push(SearchCase);
    }

    //#endregion

    //#region Redeem 
    this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalPurchase', this.DealsList_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalPurchase', this.Deal_SoldRangeMinAmount, this.Deal_SoldRangeMaxAmount);
    if (this.Deal_SoldRangeMinAmount == this._HelperService.AppConfig.DealPurchaseMinimumLimit && this.Deal_SoldRangeMaxAmount == this._HelperService.AppConfig.DealPurchaseMaximumLimit) {
      this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.DealsList_Config.SearchBaseConditions);
    }
    else {
      this.DealsList_Config.SearchBaseConditions.push(SearchCase);
    }

    //#endregion


  }

  //#endregion

  DealList_Filter_Flash_Change(event: any) {
    if (event.value == this.FlashOptionSelected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'IsFlashDeal', this._HelperService.AppConfig.DataType.Text, this.FlashOptionSelected, '=');
      this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.DealsList_Config.SearchBaseConditions);
      this.FlashOptionSelected = 0;
    }
    else if (event.value != this.FlashOptionSelected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'IsFlashDeal', this._HelperService.AppConfig.DataType.Text, this.FlashOptionSelected, '=');
      this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.DealsList_Config.SearchBaseConditions);
      this.FlashOptionSelected = event.value;
      this.DealsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'IsFlashDeal', this._HelperService.AppConfig.DataType.Text, this.FlashOptionSelected, '='));
    }
    this.DealsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  public _DealDetails: any =
    {
      MerchantDisplayName: null,
      SecondaryEmailAddress: null,
      BankDisplayName: null,
      BankKey: null,
      OwnerName: null,
      SubOwnerAddress: null,
      SubOwnerLatitude: null,
      SubOwnerDisplayName: null,
      SubOwnerKey: null,
      SubOwnerLongitude: null,
      AccessPin: null,
      LastLoginDateS: null,
      AppKey: null,
      AppName: null,
      AppVersionKey: null,
      CreateDate: null,
      CreateDateS: null,
      CreatedByDisplayName: null,
      CreatedByIconUrl: null,
      CreatedByKey: null,
      Description: null,
      IconUrl: null,
      ModifyByDisplayName: null,
      ModifyByIconUrl: null,
      ModifyByKey: null,
      ModifyDate: null,
      ModifyDateS: null,
      PosterUrl: null,
      ReferenceKey: null,
      StatusCode: null,
      StatusI: null,
      StatusId: null,
      StatusName: null,
      AccountCode: null,
      AccountOperationTypeCode: null,
      AccountOperationTypeName: null,
      AccountTypeCode: null,
      AccountTypeName: null,
      Address: null,
      AppVersionName: null,
      ApplicationStatusCode: null,
      ApplicationStatusName: null,
      AverageValue: null,
      CityAreaKey: null,
      CityAreaName: null,
      CityKey: null,
      CityName: null,
      ContactNumber: null,
      CountValue: null,
      CountryKey: null,
      CountryName: null,
      DateOfBirth: null,
      DisplayName: null,
      EmailAddress: null,
      EmailVerificationStatus: null,
      EmailVerificationStatusDate: null,
      FirstName: null,
      GenderCode: null,
      GenderName: null,
      LastLoginDate: null,
      LastName: null,
      Latitude: null,
      Longitude: null,
      MobileNumber: null,
      Name: null,
      NumberVerificationStatus: null,
      NumberVerificationStatusDate: null,
      OwnerDisplayName: null,
      OwnerKey: null,
      Password: null,
      Reference: null,
      ReferralCode: null,
      ReferralUrl: null,
      RegionAreaKey: null,
      RegionAreaName: null,
      RegionKey: null,
      RegionName: null,
      RegistrationSourceCode: null,
      RegistrationSourceName: null,
      RequestKey: null,
      RoleKey: null,
      RoleName: null,
      SecondaryPassword: null,
      SystemPassword: null,
      UserName: null,
      WebsiteUrl: null,

      StateKey: null,
      StateName: null,


      DealPromotionId: null,
      DealPromotionKey: null,
      DealPromotionStartDate: null,
      DealPrmotionEndDate: null,

    }

  public _DealAccount: any =
    {
      "ReferenceId": 0,
      "ReferenceKey": null,
      "DealId": 0,
      "DealKey": null,
      "DealTitle": null,
      "SellingPrice": null,
      "ActualPrice": null,
      "DealStartDate": null,
      "DealEndDate": null,
      "Url": null,
      "ImageUrl": null,
      "TypeId": null,
      "TypeName": null,
      "TypeCode": null,
      "TypeSystemName": null,
      "StartDate": null,
      "EndDate": null,
      "CreateDate": null,
      "CreatedById": 0,
      "ModifyDate": null,
      "ModifyById": 0,
      "StatusCode": null,
      "StatusName": null

    }

  // SelectedDealStartDatePromote: any;
  // SelectedDealEndDatePromote: any;
  checkpromoteimage: boolean = false;

  GetDealDetails(ReferenceData: any, modal: string) {
    this.ESelectedBusinessCategories = [];
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetDeal,
      ReferenceId: ReferenceData.ReferenceId,
      ReferenceKey: ReferenceData.ReferenceKey,
      AccountId: ReferenceData.AccountId,
      AccountKey: ReferenceData.AccountKey
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._DealDetails = this._HelperService.HCXGetDateComponent(_Response.Result);

          this.DealStatus = _Response.Result.DealPromotionStatus;
          this.Selectedimage = _Response.Result.DealPromotionImageUrl;
          if (_Response.Result.DealPromotionImageUrl == _Response.Result.ImageUrl) {
            this.checkpromoteimage = true;
          }
          else {
            this.checkpromoteimage = false;
          }
          this.CouponCount = this._DealDetails.MaximumUnitSale;
          this.SelectedDeal = {};

          this.SelectedDeal.ReferenceKey = this._DealDetails.ReferenceKey;
          this.SelectedDeal.ReferenceId = this._DealDetails.ReferenceId;
          this.ShowCategorySelector = false;
          if (_Response.Result.DealPromotionLocations != undefined && _Response.Result.DealPromotionLocations != null && _Response.Result.DealPromotionLocations != '') {
            var ter = _Response.Result.DealPromotionLocations.toString();
            var array = ter.split(",");
            //code added by me
            for (var i = 0; i < array.length; i++) {
              this.ESelectedBusinessCategories.push(array[i]);
            }
            //end of the code

            setTimeout(() => {
              this.ShowCategorySelector = true;
            }, 300);
          }

          this.SelectedDeal.EndDate = this._DealDetails.EndDatePart.Object;
          this.SelectedDeal.StartDate = this._DealDetails.StartDatePart.Object;
          this.SelectedDealStartDateS = this._DealDetails.StartDatePart.Date;
          this.SelectedDealEndDateS = this._DealDetails.EndDatePart.Date;

          if (this._DealDetails.DealPromotionStartDate != undefined && this._DealDetails.DealPromotionStartDate != null) {
            this.Form_ManagePromote.controls['StartDate'].setValue(this._DealDetails.DealPromotionStartDatePart.DateTime);
            this.Form_ManagePromote.controls['EndDate'].setValue(this._DealDetails.DealPromotionEndDatePart.DateTime);
            this._DealConfig.StartDate = this._DealDetails.DealPromotionStartDatePart.Object;
            this._DealConfig.EndDate = this._DealDetails.DealPromotionEndDatePart.Object;
          }


          this.FlashDeal.StartDate = this._HelperService.GetDateS(this._DealDetails.StartDate);
          this.FlashDeal.EndDate = this._HelperService.GetDateS(this._DealDetails.EndDate);

          this.FlashSentDeal.StartDate = this._DealDetails.StartDate;
          this.FlashSentDeal.EndDate = this._DealDetails.EndDate;
          this.SelectedDeal.Schedule = [];



          //#region Remove Redeem Schedules 
          if (this._DealDetails.Schedule) {
            for (let index = 0; index < this._DealDetails.Schedule.length; index++) {
              const element = this._DealDetails.Schedule[index];
              if (element.Type != 'dealshedule') {
                this.SelectedDeal.Schedule.push(element);
              }
            }
          }
          //#endregion

          if (modal == 'schedule') {
            this._HelperService.OpenModal('EditSchedule');
          }
          else if (modal == 'Flash') {
            this._HelperService.OpenModal('FlashDeal');
          }
          else if (modal == 'Promote') {
            this.SelectedBusinessCategories = [];
            this.AShowCategorySelector = false;
            setTimeout(() => {
              this.AShowCategorySelector = true;

            }, 300);
            this._HelperService.OpenModal('PromoteDeal');
          }
          else if (modal == 'ManagePromote') {
            this._HelperService.OpenModal('ManagePromoteDeal');
          }
          else {
            if (modal == 'coupons') {
              this._HelperService.OpenModal('AddCoupons');
            }

          }

        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  imgurl: any;
  GetDealPromotion(iddata: any, keydata: any) {

    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.getdealpromotion,
      ReferenceId: iddata,
      ReferenceKey: keydata,

    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promote, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._DealAccount = _Response.Result;
          this.Form_ManagePromote.controls['StartDate'].setValue(moment(this._DealAccount.DealStartDate).format('DD-MM-YYYY hh:mm a'));
          this.Form_ManagePromote.controls['EndDate'].setValue(moment(this._DealAccount.DealEndDate).format('DD-MM-YYYY hh:mm a'));
          this.imgurl = this._DealAccount.ImageUrl
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#region Run Time 

  IsRunAllTime: boolean = true;
  RunAllTimeToogle(): void {
    this.IsRunAllTime = !(this.IsRunAllTime);
    this.SelectedDeal.Schedule = [];
  }

  //#endregion

  //#region Schedule Update 
  SelectedDeal: any = {};
  SelectedDealStartDateS: any = "";
  SelectedDealEndDateS: any = "";
  ShowEditSchedule(ReferenceData: any): void {
  }
  ScheduleEndDateRangeChange_edit(value) {
    this.SelectedDeal.EndDate = value.end;
  }

  enddate: any;
  ScheduleEndDateRangeChange1(value) {
    this.SelectedDeal.EndDate = value.end;
    this.enddate = this.SelectedDeal.EndDate
  }

  ScheduleStartDateRangeChange(value) {
    this._DealConfig.StartDate = value.start;
    this.Form_Promote.patchValue(
      {
        StartDate: value.start.format('DD-MM-YYYY hh:mm a'),
        //StartDate: this._DealConfig.StartDate,
      }
    );
  }

  ScheduleEndDateRangeChange(value) {
    this._DealConfig.EndDate = value.start;
    this.Form_Promote.patchValue(
      {
        EndDate: value.start.format('DD-MM-YYYY hh:mm a'),

      }
    );
  }


  ScheduleManageStartDateRangeChange(value) {
    this.Form_ManagePromote.patchValue(
      {
        StartDate: value.start.format('YYYY-MM-DD hh:mm a'),

      }
    );
  }

  ScheduleManageEndDateRangeChange(value) {

    this.Form_ManagePromote.patchValue(
      {
        EndDate: value.start.format('YYYY-MM-DD hh:mm a'),

      }
    );
  }


  ReFormat_RequestBody(): void {
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'updatedeal',
      'ReferenceKey': this.SelectedDeal.ReferenceKey,
      'ReferenceId': this.SelectedDeal.ReferenceId,
      "TypeCode": "deal",
      "StartDate": this.SelectedDeal.StartDate,
      "EndDate": this.SelectedDeal.EndDate,
      "Schedule": this.SelectedDeal.Schedule
    };
    //#region Set Schedule 

    for (let index = 0; index < 7; index++) {

      if (this.IsRunAllTime) {
        formRequest.Schedule.push({
          DayOfWeek: index,
          StartHour: '00:00',
          EndHour: '23:59',
          Type: 'dealshedule'
        });
      } else {
        formRequest.Schedule.push({
          DayOfWeek: index,
          StartHour: moment(this.DealTimings.Start).format('hh:mm'),
          EndHour: moment(this.DealTimings.End).format('hh:mm'),
          Type: 'dealshedule'
        });

      }
    }

    //#endregion

    return formRequest;

  }




  UpdateSchedule(): void {

    var Req = this.ReFormat_RequestBody_Old();
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, Req);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess('Deal Schedule updated successfully. It will take upto 5 minutes to update changes.');

          this.ClearScheduleUpdate();
          this._HelperService.CloseModal('EditSchedule');
          this.DealsList_GetData();
        }
        else {
          this.SelectedDeal.Schedule = [];
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.SelectedDeal.Schedule = [];
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  ReFormat_RequestBody_Old(): void {
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'updatedeal',
      'ReferenceKey': this.SelectedDeal.ReferenceKey,
      'ReferenceId': this.SelectedDeal.ReferenceId,
      "TypeCode": "deal",
      "StartDate": this.SelectedDeal.StartDate,
      "EndDate": this.SelectedDeal.EndDate,
      "Schedule": this.SelectedDeal.Schedule,
      "TitleTypeId": this._DealDetails.TitleTypeId
    };
    //#region Set Schedule 

    for (let index = 0; index < 7; index++) {

      if (this.IsRunAllTime) {
        formRequest.Schedule.push({
          DayOfWeek: index,
          StartHour: '00:00',
          EndHour: '23:59',
          Type: 'dealshedule'
        });
      } else {
        formRequest.Schedule.push({
          DayOfWeek: index,
          StartHour: moment(this.DealTimings.Start).format('hh:mm'),
          EndHour: moment(this.DealTimings.End).format('hh:mm'),
          Type: 'dealshedule'
        });

      }
    }

    //#endregion

    return formRequest;

  }

  ClearScheduleUpdate(): void {
    this.SelectedDeal = {};
    this.SelectedDealStartDateS = "";
  }

  //#endregion

  //#region Stock Update 

  CouponCount: number = null;

  ShowEditStock(ReferenceData: any): void {

    this.SelectedDeal.ReferenceKey = ReferenceData.ReferenceKey;
    this.SelectedDeal.ReferenceId = ReferenceData.ReferenceId;

    this._HelperService.OpenModal('AddCoupons');
  }

  EditStock(): void {

    var formRequest: any = {
      'OperationType': 'new',
      //'Task': 'updatedeal',
      'Task': 'updatedealcoupons',
      'ReferenceKey': this.SelectedDeal.ReferenceKey,
      'ReferenceId': this.SelectedDeal.ReferenceId,
      "TypeCode": "deal",
      "MaximumUnitSale": this.CouponCount
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, formRequest);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess('Deal Coupon Count updated successfully. It will take upto 5 minutes to update changes.');

          this.CouponCount = null;
          this._HelperService.CloseModal('AddCoupons');
          this.DealsList_GetData();

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  ClearCouponUpdate(): void {
    this.CouponCount = null;
  }

  //#endregion

  //promote deal code start---
  Form_Promote: FormGroup;
  Form_ManagePromote: FormGroup;

  Form_Promote_Show() {
    this._HelperService.Icon_Crop_Clear();
    this.InitImagePicker(this.InputFileComponent_Term);
    this._HelperService.OpenModal("_Icon_Cropper_Modal");
    // this._HelperService.OpenModal("_PreviewGeneral");



  }
  Form_Promote_Load() {
    this._HelperService._Icon_Cropper_Data.Width = 128;
    this._HelperService._Icon_Cropper_Data.Height = 128;
    this.Form_Promote = this._FormBuilder.group({
      OperationType: 'new',
      Task: 'savedealpromotion',

      TypeCode: [null, Validators.required],
      TypeId: [null, Validators.required],

      ImageContent: null,
      StartDate: moment().format('DD-MM-YYYY hh:mm'),
      EndDate: moment().add(1, 'days').endOf("day").format('DD-MM-YYYY hh:mm'),

    });
  }


  Form_ManagePromote_Load() {
    this._HelperService._Icon_Cropper_Data.Width = 128;
    this._HelperService._Icon_Cropper_Data.Height = 128;
    this.Form_ManagePromote = this._FormBuilder.group({
      OperationType: 'new',
      Task: 'updatedealpromotion',

      TypeCode: [null, Validators.required],
      TypeId: [null, Validators.required],

      ImageContent: null,

      EndDate: null,
      StartDate: null,

    });
  }


  GetRoles_ListChange(event: any) {
    this.Form_Promote.patchValue(
      {
        TypeCode: event.data[0].apival,
        TypeId: event.value

      }
    );
  }

  GetRoles_ManageListChange(event: any) {
    this.Form_ManagePromote.patchValue(
      {
        TypeCode: event.data[0].apival,
        TypeId: event.value
      }
    );
  }


  IsNoDaySelected: boolean = false;
  toogleWeekDay(index: number): void {
    this.promotion[index].selected = !this.promotion[index].selected;
    for (let i = 0; i < this.promotion.length; i++) {
      const element = this.promotion[i];
      if (element.selected) {
        this.IsNoDaySelected = false;
        return;
      }
    }
    this.IsNoDaySelected = true;
  }


  public _Roles = [
    {
      'id': 0,
      'text': 'Select Role',
      'apival': 'Select Role'
    },
    {
      'id': 758,
      'text': 'Home',
      'apival': 'home'
    },
    {
      'id': 759,
      'text': 'Features',
      'apival': 'featured'
    },
    {
      'id': 760,
      'text': 'Top Offers',
      'apival': 'topoffers'
    },

  ];

  CreateCashierRequest(_FormValue: any): void {

    var IconContent: any = undefined;

    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      IconContent = this._HelperService._Icon_Cropper_Data;
    }

    _FormValue.IconContent = IconContent;

    return _FormValue;
  }


  ReFormat_RequestBody1() {
    var formValue: any = cloneDeep(this.Form_Promote.value);
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'savedealpromotion',

      "DealId": this._DealDetails.ReferenceId,
      "DealKey": this._DealDetails.ReferenceKey,
      StartDate: moment(this._DealConfig.StartDate).format('YYYY-MM-DD HH:mm'),
      EndDate: moment(this._DealConfig.EndDate).format('YYYY-MM-DD HH:mm'),
      Locations: null,

      "TypeCode": 'home',
      "TypeId": 758,
      "Status": "default.active",
      //"ImageContent": this._HelperService._Icon_Cropper_Data,
      "ImageContent": this._DealConfig.DealImages[0]


    };


    return formRequest;

  }

  urldata: any; dealtitledata: any; Sdate: any; Edate: any;
  ReFormat_RequestBody2() {
    var formValue: any = cloneDeep(this.Form_ManagePromote.value);

    if (formValue.Url != undefined) {
      this.urldata = formValue.Url
    }
    else {
      this.dealtitledata = formValue.DealTitle
    }
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'updatedealpromotion',

      "ReferenceId": this._DealDetails.DealPromotionId,
      "ReferenceKey": this._DealDetails.DealPromotionKey,

      "Url": formValue.Url,
      "DealTitle": formValue.DealTitle,
      StartDate: moment(this._DealConfig.StartDate).format('YYYY-MM-DD HH:mm'),
      EndDate: moment(this._DealConfig.EndDate).format('YYYY-MM-DD HH:mm'),

      "TypeCode": 'home',
      "TypeId": 758,
      "Status": "default.active",
      //"ImageContent": this._HelperService._Icon_Cropper_Data,
      "ImageContent": this._DealConfig.DealImages[0]


    };


    return formRequest;

  }

  Form_Promote_Clear() {
    this.Form_Promote.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_Promote_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }

  Form_Promote_Close() {
    this._HelperService.CloseModal("PromoteDeal");
    this._HelperService.CloseAllModal();

  }


  Form_Promote_Process(_FormValue?: any) {
    if (this.SelectedBusinessCategories.length < 1) {
      this._HelperService.NotifyError("Please select visiblity location");
    }
    else {
      var _tLoc = '';
      for (let index = 0; index < this.SelectedBusinessCategories.length; index++) {
        const element = this.SelectedBusinessCategories[index];
        if (index == 0) {
          _tLoc = element;
        }
        else {
          _tLoc = _tLoc + "," + element;
        }
      }
      var _FormValue = this.Form_Promote.value;
      this._HelperService.IsFormProcessing = true;
      // var Request = this.CreateCashierRequest(_FormValue);
      var Request = this.ReFormat_RequestBody1();
      Request.Locations = _tLoc;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(
        this._HelperService.AppConfig.NetworkLocation.Console.V3.Promote, Request);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess(_Response.Message);
            this.SelectedBusinessCategories = [];
            this.DealsList_Setup();
            this._DealConfig =
            {
              DefaultStartDate: null,
              DefaultEndDate: null,
              StartDate: null,
              EndDate: null,
              DealImages: [],

              Images: [],
              StartDateConfig: {
              },
              EndDateConfig: {
              },

            }
            this._ImageManager =
            {
              TCroppedImage: null,
              ActiveImage: null,
              ActiveImageName: null,
              ActiveImageSize: null,
              Option: {
                MaintainAspectRatio: "true",
                MinimumWidth: 800,
                MinimumHeight: 400,
                MaximumWidth: 800,
                MaximumHeight: 400,
                ResizeToWidth: 800,
                ResizeToHeight: 400,
                Format: "jpg",
              }
            }

            this.DealsList_Setup();
            this._HelperService.CloseModal('PromoteDeal');
            this.ngOnInit();
            this._HelperService.IsFormProcessing = false;

            this._DealConfig.DealImages = [];
            this.Form_Promote_Clear();
            this.ResetFilterUI();
            this._HelperService.Icon_Crop_Clear();

            this.Form_Promote_Close();


            if (_FormValue.OperationType == "close") {
              this.Form_Promote_Close();
            }
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );

    }

  }

  Form_ManagePromote_Process(_FormValue?: any) {

    if (this.ESelectedBusinessCategories.length < 1) {
      this._HelperService.NotifyError("Please select visiblity location");
    }
    else {
      var _EtLoc = '';
      for (let index = 0; index < this.ESelectedBusinessCategories.length; index++) {
        const element = this.ESelectedBusinessCategories[index];
        if (index == 0) {
          _EtLoc = element;
        }
        else {
          _EtLoc = _EtLoc + "," + element;
        }
      }




      var _FormValue = this.Form_ManagePromote.value;

      this._HelperService.IsFormProcessing = true;
      var Request = this.ReFormat_RequestBody2();
      //   Request.Locations = this.ESelectedBusinessCategories;
      Request.Locations = _EtLoc;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(
        this._HelperService.AppConfig.NetworkLocation.Console.V3.Promote,
        Request
      );
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess(_Response.Message);
            this.DealsList_Setup();
            this._HelperService.CloseModal('ManagePromoteDeal');
            this._HelperService.IsFormProcessing = false;

            this._DealConfig.DealImages = [];
            this.Form_Promote_Clear();
            this.ResetFilterUI();
            this._HelperService.Icon_Crop_Clear();

            this.Form_Promote_Close();
            if (_FormValue.OperationType == "close") {
              this.Form_Promote_Close();
            }
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );
    }
  }



  Form_ManagePromote_Clear() {
    this.Form_Promote.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_Promote_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }

  Form_ManagePromote_Close() {
    this._HelperService.CloseModal("ManagePromoteDeal");
    this._HelperService.CloseAllModal();

  }


  imageChangedEvent: any = '';
  croppedImage: any = '';
  croppedImage1: any = ''
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    this.croppedImage1 = this.croppedImage.replace("data:image/png;base64,", "")
    this._HelperService._Icon_Cropper_Data.Content = this.croppedImage1;
  }
  //promote deal code end---
  onImgError(event) {
    event.target.src = this._DealDetails.ImageUrl;
  }

  coupancount: boolean = false;
  checkcount() {
    if (this.CouponCount > 5000 || this.CouponCount == null) {
      this.coupancount = true;
    }
    else {
      this.coupancount = false;
    }
  }

  ShowAllLocations(): void {
    this._HelperService.OpenModal("AllLocation");
  }


  //#region Image Manager
  onImageAccept(value) {
    setTimeout(() => {
      this.Form_AddUser.patchValue(
        {
          TImage: null,
        }
      );
    }, 300);
    this._ImageManager.ActiveImage = value;
    this._ImageManager.ActiveImageName = value.file.name;
    this._ImageManager.ActiveImageName = value.file.size;
  }

  onImageAccept1(value) {
    setTimeout(() => {
      this.Form_ManagePromote.patchValue(
        {
          ImageContent: null,
        }
      );
    }, 300);
    this._ImageManager.ActiveImage = value;
    this._ImageManager.ActiveImageName = value.file.name;
    this._ImageManager.ActiveImageName = value.file.size;
  }

  onImageAccept2(value) {
    setTimeout(() => {
      this.Form_ManagePromote.patchValue(
        {
          ImageContent: null,
        }
      );
    }, 300);
    this._ImageManager.ActiveImage = value;
    this._ImageManager.ActiveImageName = value.file.name;
    this._ImageManager.ActiveImageName = value.file.size;
  }
  Icon_B64Cropped(base64: string) {
    this._ImageManager.TCroppedImage = base64;
  }
  Icon_B64CroppedDone() {
    var ImageDetails = this._HelperService.GetImageDetails(this._ImageManager.TCroppedImage);
    var ImageContent =
    {
      //OriginalContent: this._ImageManager.TCroppedImage,
      Name: this._ImageManager.ActiveImageName,
      Size: this._ImageManager.ActiveImageSize,
      Extension: ImageDetails.Extension,
      Content: ImageDetails.Content
    };

    if (this._DealConfig.DealImages.length == 0) {
      this._DealConfig.DealImages.push(
        {
          //ImageContent: ImageItem,
          Name: this._ImageManager.ActiveImageName,
          Size: this._ImageManager.ActiveImageSize,
          Extension: ImageDetails.Extension,
          Content: ImageDetails.Content,
          IsDefault: 1,
        }
      );
    }
    else {
      this._DealConfig.DealImages.push(
        {
          //  ImageContent: ImageItem,
          Name: this._ImageManager.ActiveImageName,
          Size: this._ImageManager.ActiveImageSize,
          Extension: ImageDetails.Extension,
          Content: ImageDetails.Content,
          IsDefault: 0,
        }
      );
    }


    this._ImageManager.TCroppedImage = null;
    this._ImageManager.ActiveImage = null;
    this._ImageManager.ActiveImageName = null;
    this._ImageManager.ActiveImageSize = null;
  }
  Icon_Crop_Clear() {
    this._ImageManager.TCroppedImage = null;
    this._ImageManager.ActiveImage = null;
    this._ImageManager.ActiveImageName = null;
    this._ImageManager.ActiveImageSize = null;
    this._HelperService.CloseModal('_Icon_Cropper_Modal');
  }
  RemoveImage(Item) {
    this._DealConfig.DealImages = this._DealConfig.DealImages.filter(x => x != Item);
  }


  removeImage(): void {
    this.CurrentImagesCount = 0;
    this._HelperService.Icon_Crop_Clear();
    var Req = this.DeleteApprove_RequestBody();
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promote, Req);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Business Logo Removed Successfully. It will take upto 5 minutes to update changes.");

        }
        else {

          this._HelperService.NotifyError("Business Logo Not Found");
        }
      },
      _Error => {

        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  DeleteApprove_RequestBody(): void {
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'removepromotionimage',

      "ReferenceKey": this._DealDetails.DealPromotionKey,
      'ReferenceId': this._DealDetails.DealPromotionId,
    };
    //#region Set Schedule 

    //#endregion

    return formRequest;

  }
  //#endregion

  detetepromoteId: any;
  detetepromoteKey: any;


  DealStatus: any;
  OnOffPromoteDeal() {

    if (this._DealDetails.DealPromotionStatus == 'default.inactive') {
      this.StartAsPromote();
    }
    else {
      this.StopAsPromote();
    }
  }
  StopAsPromote(): void {


    this.PromoteDeal = {};
    this.detetepromoteId = this._DealDetails.ReferenceId;
    this.detetepromoteKey = this._DealDetails.ReferenceKey;

    swal({
      position: "center",
      title: "Stop this Deal to Promote?",
      text: "Click Continue to Stop Promote this Deal",
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {

      if (result.value) {
        var _EtLoc = '';
        for (let index = 0; index < this.ESelectedBusinessCategories.length; index++) {
          const element = this.ESelectedBusinessCategories[index];
          if (index == 0) {
            _EtLoc = element;
          }
          else {
            _EtLoc = _EtLoc + "," + element;
          }
        }

        this._HelperService.IsFormProcessing = true;
        var PData =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.updatedealpromotion,
          "ReferenceId": this._DealDetails.DealPromotionId,
          "ReferenceKey": this._DealDetails.DealPromotionKey,
          StartDate: this._DealDetails.DealPromotionStartDate,
          EndDate: this._DealDetails.DealPrmotionEndDate,
          Locations: _EtLoc,

          "TypeCode": 'home',
          "TypeId": 758,
          "StatusCode": "default.inactive",

          "ImageContent": this._DealConfig.DealImages[0]


        }


        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promote, PData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Deal Promotion Stopped Successfully. It will take upto 5 minutes to update changes.");
              this._HelperService.CloseModal('ManagePromoteDeal')
              this.DealsList_Setup();
              // this._HelperService.CloseModal('FlashDeal');
              this._HelperService.IsFormProcessing = false;

            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });

      }
    });

  }

  StartAsPromote(): void {
    this.PromoteDeal = {};
    this.detetepromoteId = this._DealDetails.ReferenceId;
    this.detetepromoteKey = this._DealDetails.ReferenceKey;

    swal({
      position: "center",
      title: "Start this Deal to Promote?",
      text: "Click Continue to Start Promote this Deal",
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        var _EtLoc = '';
        for (let index = 0; index < this.ESelectedBusinessCategories.length; index++) {
          const element = this.ESelectedBusinessCategories[index];
          if (index == 0) {
            _EtLoc = element;
          }
          else {
            _EtLoc = _EtLoc + "," + element;
          }
        }
        this._HelperService.IsFormProcessing = true;
        var PData =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.updatedealpromotion,
          "ReferenceId": this._DealDetails.DealPromotionId,
          "ReferenceKey": this._DealDetails.DealPromotionKey,
          Locations: _EtLoc,

          StartDate: moment(this._DealConfig.StartDate).format('YYYY-MM-DD HH:mm'),
          EndDate: moment(this._DealConfig.EndDate).format('YYYY-MM-DD HH:mm'),
          "TypeCode": 'home',
          "TypeId": 758,
          "StatusCode": "default.active",

          "ImageContent": this._DealConfig.DealImages[0]


        }


        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promote, PData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Deal Promotion Started Successfully. It will take upto 5 minutes to update changes.");
              this._HelperService.CloseModal('ManagePromoteDeal')
              this.DealsList_Setup();
              // this._HelperService.CloseModal('FlashDeal');
              this._HelperService.IsFormProcessing = false;

            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });

      }
    });

  }


  public promotion: any[] = [
    { name: 'TUC Web', selected: false },
    { name: 'TUC App', selected: false },
    { name: 'On Both', selected: true },

  ]
  public BusinessCategories = [

  ];
  AShowCategorySelector = true;
  ShowCategorySelector = true;
  public S2BusinessCategories = [
    {
      id: 'Promotional Slider',
      text: 'Promotional Slider',
    },
    {
      id: 'Top Deals',
      text: 'Top Deals'
    },
    {
      id: 'Featured Deals',
      text: 'Featured Deals'
    },
    {
      id: 'Deal Of The Day',
      text: 'Deal Of The Day'
    }
  ];
  public SelectedBusinessCategories = [];
  CategoriesSelected(Items) {
    if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
      this.SelectedBusinessCategories = Items.value;
    }
    else {
      this.SelectedBusinessCategories = [];
    }
  }


  public ESelectedBusinessCategories = [];
  ECategoriesSelected(Items) {
    if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
      this.ESelectedBusinessCategories = Items.value;
    }
    else {
      this.ESelectedBusinessCategories = [];
    }

  }

}


// DealsList_Config
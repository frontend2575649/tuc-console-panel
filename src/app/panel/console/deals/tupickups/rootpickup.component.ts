import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, OResponse } from "../../../../service/service";

@Component({
  selector: "tu-rootpickup",
  templateUrl: "rootpickup.component.html"
})
export class RootPickupComponent implements OnInit {
  _ShowMap = false;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) { }

  ngOnInit() {


  }
}
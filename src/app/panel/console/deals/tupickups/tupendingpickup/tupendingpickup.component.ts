import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../../service/service';
declare var moment: any;
declare var $: any;


@Component({
    selector: 'tu-pendingpickup',
    templateUrl: './tupendingpickup.component.html',
})
export class TUPendingPickupComponent implements OnInit {

      public TUTr_Config: OList;
      public ResetFilterControls: boolean = true;
      totalAmountMin = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
      totalAmountMax = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
      orderList: any = [];
    
      constructor(public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _FilterHelperService: FilterHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,) { }
        public AcquirerId = 0;
        ngOnInit() {
          this._HelperService.StopClickPropogation();
          Feather.replace();
          this.TUTr_Setup();
          this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null && this._HelperService.AppConfig.ActiveReferenceId == null) {
                if (this._HelperService.AppConfig.ActiveOwnerId != null) {
                    this.AcquirerId = this._HelperService.AppConfig.ActiveOwnerId;
                    this.TUTr_Setup();
                    this.GetSalesOverview();
                    this.TUTr_Filter_Stores_Load();
                    
                }
                else {
                    this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
                }
            } 
            this.TodayStartTime = new Date(2017, 0, 1, 0, 0, 0, 0), moment();
            this.TodayEndTime = moment();
           
        });

        }


        DigitForm:FormGroup;
        CancelForm:FormGroup;

        public _GetoverviewSummary: any = {};

        private pData = {
          Task: 'getordersovrview'
        };
      
      
        GetSalesOverview() {
      
          this._HelperService.IsFormProcessing = true;
    
      
          let _OResponse: Observable<OResponse>;
          _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, this.pData);
          _OResponse.subscribe(
            _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._GetoverviewSummary = _Response.Result as any;
                this._ChangeDetectorRef.detectChanges();
                return;
              }
      
              else {
                this._HelperService.NotifyError(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
            });
        }



        public LatestOrder:any = "asc";
        public OldestOrder:any = "desc"
        TuTr_Setup_Fields() {
          var TableFields = [];
      
          TableFields = [
            {
              DisplayName: 'Order ID',
              SystemName: 'ReferenceId',
              DataType: this._HelperService.AppConfig.DataType.Number,
              Show: true,
              Search: true,
              Sort: false,
            },
            {
              DisplayName: 'Store',
              SystemName: 'StoreDisplayName',
              DataType: this._HelperService.AppConfig.DataType.Text,
              Show: true,
              Search: true,
              Sort: false,
            },
            {
              DisplayName: 'CreateDate',
              SystemName: 'CreateDate',
              DataType: this._HelperService.AppConfig.DataType.Date,
              Show: true,
              Search: false,
              Sort: true,
              IsDateSearchField: true,
            },
            {
              DisplayName: 'Delivered',
              SystemName: 'DeliveryDate',
              DataType: this._HelperService.AppConfig.DataType.Date,
              Show: true,
              Search: false,
              Sort: false,
              // IsDateSearchField: true,
            },
            // {
            //   DisplayName: 'Delivered in',
            //   SystemName: 'hoursDiff',
            //   DataType: this._HelperService.AppConfig.DataType.Number,
            //   Show: true,
            //   Search: false,
            //   Sort: false,
            // },
            {
              DisplayName: 'Carrier',
              SystemName: 'CarrierName',
              DataType: this._HelperService.AppConfig.DataType.Text,
              Show: true,
              Search: true,
              Sort: false,
            },
            {
              DisplayName: 'Customer',
              SystemName: 'CustomerDisplayName',
              DataType: this._HelperService.AppConfig.DataType.Text,
              Show: true,
              Search: true,
              Sort: false,
            },
            {
              DisplayName: 'Delivery Charge',
              SystemName: 'RateAmount',
              DataType: this._HelperService.AppConfig.DataType.Decimal,
              Show: true,
              Search: false,
              Sort: false,
            },
            {
              DisplayName: 'Total',
              SystemName: 'TotalAmount',
              DataType: this._HelperService.AppConfig.DataType.Decimal,
              Show: true,
              Search: false,
              Sort: false,
            },
            {
              DisplayName: 'Deal',
              SystemName: 'DealTitle',
              DataType: this._HelperService.AppConfig.DataType.Text,
              Show: false,
              Search: true,
              Sort: false,
            },
          ]
      
          return TableFields;
        }
        TUTr_Setup() {
          var SearchCondition = undefined
          this.TUTr_Config =
          {
            Id: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetShipments,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment,
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'orderstatus.readytopickup', '=='),
            Sort:
            {
              SortDefaultName: null,
              SortDefaultColumn: 'CreateDate',
              SortName: null,
              SortColumn: null,
              SortOrder: 'desc',
              SortOptions: [],
            },
            Title: "Orders",
            StatusType: null,
            TableFields: this.TuTr_Setup_Fields(),
            DefaultSortExpression: "CreateDate desc",
          }
      
          this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
          this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.GetShipment,
            this.TUTr_Config
          );
          this.TUTr_GetData();
        }
      
    
      TUTr_GetData() {
        //this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.ThankUCash.GetShipment);
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.TUTr_Config = TConfig;
      }
    
      togglebutton:boolean = false
      TUTr_ToggleOption(event: any, Type: any, sortorder:any) {
        if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
            event.data = {
                SalesMin: this.TUTr_OrderMinAmount,
                SalesMax: this.TUTr_OrderMaxAmount
            }
        }


        if (Type == 'date') {
            event.start = this._HelperService.DateInUTC(event.start);
            event.end = this._HelperService.DateInUTC(event.end);
        }

        this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);
        this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
        this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);
        
        this.TodayStartTime = this.TUTr_Config.StartTime;
        this.TodayEndTime = this.TUTr_Config.EndTime;

        if (Type == 'sort'){
          if(sortorder =="asc"){
            this.TUTr_ToggleOption( null, this._HelperService.AppConfig.ListToggleOption.SortOrder,"")
            this.togglebutton = !this.togglebutton;

          }else if(sortorder =="desc"){
            this.TUTr_ToggleOption( null, this._HelperService.AppConfig.ListToggleOption.SortOrder,"");
            this.togglebutton = !this.togglebutton;

          }
          
          
        }
        if (
            (this.TUTr_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.TUTr_GetData();
        }else if(Type == "sort"){
          this.ApplyFilters(null, this._HelperService.AppConfig.ListToggleOption.ApplyFilter,'Sort')
        }
    }
      
      timeout = null;
      TUTr_ToggleOptionSearch(event: any, Type: any) {
          clearTimeout(this.timeout);
          this.timeout = setTimeout(() => {
              if (Type == 'date') {
                  event.start = this._HelperService.DateInUTC(event.start);
                  event.end = this._HelperService.DateInUTC(event.end);
              }
  
              this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);
  
              this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
              this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);
  
              this.TodayStartTime = this.TUTr_Config.StartTime;
              this.TodayEndTime = this.TUTr_Config.EndTime;
              if (event != null) {
                  for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                      const element = this.TUTr_Config.Sort.SortOptions[index];
                      if (event.SystemName == element.SystemName) {
                          element.SystemActive = true;
                      }
                      else {
                          element.SystemActive = false;
                      }
  
                  }
              }
  
  
              if (
                  (this.TUTr_Config.RefreshData == true)
                  && this._HelperService.DataReloadEligibility(Type)
              ) {
                  this.TUTr_GetData();
              }
          }, this._HelperService.AppConfig.SearchInputDelay);
      }
      
      SetSearchRanges(): void {
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalAmount', this.TUTr_Config.SearchBaseConditions);
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalAmount', this.TUTr_OrderMinAmount, this.TUTr_OrderMaxAmount);
        if (this.TUTr_OrderMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_OrderMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }

    }


      TUTr_OrderMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
      TUTr_OrderMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
      SetSalesRanges(): void {
        this.TUTr_OrderMinAmount = this.TUTr_Config.SalesRange.SalesMin;
        this.TUTr_OrderMaxAmount = this.TUTr_Config.SalesRange.SalesMax;
    }
    SetOtherFilters(): void {
      this.TUTr_Config.SearchBaseConditions = [];
      var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Store));
      if (CurrentIndex != -1) {
          this.TUTr_Filter_Store_Selected = 0;
          this.StoresEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
      }

  }


    public TUTr_Filter_Store_Option: Select2Options;
    public TUTr_Filter_Store_Toggle = false;
    public TUTr_Filter_Store_Selected = 0;
    TUTr_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            AccountId: this._HelperService.AppConfig.ActiveOwnerId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Store_Option = {
            placeholder: 'Search by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Stores_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Store
        );
        this.StoresEventProcessing(event);
    }

    StoresEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'StoreId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset,"");
        this._HelperService.ToggleField = true;;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }
      Data :any = {};


      GetOverviews(ListOptions: any, Task: string): any {
        this._HelperService.IsFormProcessing = true;
    
        ListOptions.SearchCondition = '';
        ListOptions = this._DataHelperService.List_GetData(ListOptions);
    
        if (ListOptions.ActivePage == 1) {
          ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;
    
        if (ListOptions.Sort.SortDefaultName) {
          ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
          ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
          ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }
    
        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
          if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
            SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
          }
          else {
            SortExpression = ListOptions.Sort.SortColumn + ' desc';
          }
        }
    
        var pData = {
          Task: Task,
          TotalRecords: ListOptions.TotalRecords,
          Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
          Limit: ListOptions.PageRecordLimit,
          SearchCondition: ListOptions.SearchCondition,
          SortExpression: SortExpression
        };
    
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this.orderList = _Response.Result as any;
              // console.log("this.orderList.......", this.orderList)
              if (this.orderList && this.orderList.Data && this.orderList.Data.length > 0) {
                this.orderList.Data.forEach(element => {
                  if (element.CreateDate && element.DeliveryDate && element.StatusId !== 494) {
                    const startTime = moment(element.CreateDate);
                    const endTime = moment(element.DeliveryDate);
    
                    const hoursDiff = endTime.diff(startTime, 'hours');
                    element.hoursDiff = hoursDiff;
                    // console.log('Hours:' + hoursDiff);
                  }
                });
              }
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
    
          });
    
    
      }
    
      ResetOrderAmount(Type:any){
        if (Type == "SalesRange") {
          this._HelperService.FilterSnapTemprary.SalesRange.SalesMin = 0;
          this._HelperService.FilterSnapTemprary.SalesRange.SalesMax = 10000000;
        }
      }
      RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_MerchantSales(Type, index);
        this._FilterHelperService.SetMerchantSalesConfig(this.TUTr_Config);
        this.ResetOrderAmount(Type);
        this.SetOtherFilters();
        this.SetSalesRanges();
        this.SetSearchRanges()
        this.TUTr_GetData();
    }
  
        ResetFilterUI(): void {
          this.ResetFilterControls = false;
          this._ChangeDetectorRef.detectChanges();
          this.TUTr_Filter_Stores_Load();
          this.ResetFilterControls = true;
          this._ChangeDetectorRef.detectChanges();
  
      }
        ApplyFilters(event: any, Type: any, ButtonType: any): void {
          this.SetSearchRanges();
          this._HelperService.MakeFilterSnapPermanent();
          this.TUTr_GetData();
          this.ResetFilterUI(); this._HelperService.StopClickPropogation();
          if (ButtonType == 'Other') {
              $("#TUTr_fdropdown").dropdown('toggle');
          }
      }
  
      ResetFilters(event: any, Type: any): void {
          this._HelperService.ResetFilterSnap();
          this._FilterHelperService.SetMerchantSalesConfig(this.TUTr_Config);
          this.SetOtherFilters();
          this.SetSalesRanges();
          this.TUTr_GetData();
          this.SetSalesRanges();
          this.ResetFilterUI(); this._HelperService.StopClickPropogation();
      }

      public ConfimationOrderDetails:any;
      OpenConfimation(Orderdata){
        this.ConfimationOrderDetails = Orderdata;
       
        this._HelperService.OpenModal('Form_Confirmation');
        }

        ArrangeOrder(){
          var pData = {
              Task: 'arrangeshipment',
              ReferenceId: this.ConfimationOrderDetails.ReferenceId,
              ReferenceKey:this.ConfimationOrderDetails.ReferenceKey ,
              //RateId: Orderdata.RateId,
              //RateAmount: Orderdata.RateAmount
          };
  
          let _OResponse: Observable<OResponse>;
          _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
          _OResponse.subscribe(
            _Response => {
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.IsFormProcessing = false;
                
                this._HelperService.NotifySuccess("Order Arranged Successfully");
                this._ChangeDetectorRef.detectChanges();
              }
              else {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.NotifyError(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.HandleException(_Error);
            }
          );
          }
      RouteOrder(ReferenceData) {
  
        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveCustomer,
            {
                ReferenceKey: ReferenceData.ReferenceKey,
                ReferenceId: ReferenceData.ReferenceId,
                DisplayName: ReferenceData.DisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.Customer,
            }
        );
  
        this._HelperService.AppConfig.ActiveOwnerId =
            ReferenceData.MerchantId;
        this._HelperService.AppConfig.ActiveOwnerKey= ReferenceData.MerchantKey;
        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Product.OrderDetail,
            ReferenceData.ReferenceId,
            ReferenceData.ReferenceKey
        ]);
  
    }
      TodayStartTime = null;
      TodayEndTime = null;
     
    
    }
    



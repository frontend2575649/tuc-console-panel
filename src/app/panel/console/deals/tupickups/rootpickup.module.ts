import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { AgmCoreModule } from '@agm/core';
import { RootPickupComponent } from './rootpickup.component';
// import { LeafletModule } from '@asymmetrik/ngx-leaflet';
const routes: Routes = [
    {
        path: "",
        component: RootPickupComponent,
        children: [
            { path: "", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../tupickups/tupendingpickup/tupendingpickup.module#TUPendingPickupModule" },
            { path: "cancelled", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../tupickups/tucancelledpickups/tucancelledpickup.module#TUCancelledPickupModule" },
            { path: "pendingpickup", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../tupickups/tupendingpickup/tupendingpickup.module#TUPendingPickupModule" },
           { path: "returnedrequest", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../tupickups/tureturnpickups/tureturnpickup.module#TUReturnPickupModule" },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: [RootPickupComponent]
})
export class  RootPickupRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        RootPickupRoutingModule,
        // LeafletModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
   
})
export class RootPickupModule { }

import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../../service/service';
declare var moment: any;
declare var $: any;


@Component({
    selector: 'tu-returnpickup',
    templateUrl: './tureturnpickup.component.html',
})
export class TUReturnPickupComponent implements OnInit {

      public TUTr_Config: OList;
      public ResetFilterControls: boolean = true;
      totalAmountMin = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
      totalAmountMax = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
      orderList: any = [];
    
      constructor(public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _FilterHelperService: FilterHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,) { }
    
    
      TuTr_Setup_Fields() {
        var TableFields = [];
    
        TableFields = [
          {
            DisplayName: 'Order ID',
            SystemName: 'ReferenceId',
            DataType: this._HelperService.AppConfig.DataType.Number,
            Show: true,
            Search: false,
            Sort: true,
          },
          {
            DisplayName: 'Store',
            SystemName: 'StoreDisplayName',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Show: true,
            Search: true,
            Sort: true,
          },
          {
            DisplayName: 'Placed On',
            SystemName: 'CreateDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Show: true,
            Search: false,
            Sort: true,
            IsDateSearchField: true,
          },
          {
            DisplayName: 'Delivered',
            SystemName: 'DeliveryDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Show: true,
            Search: false,
            Sort: true,
            // IsDateSearchField: true,
          },
          {
            DisplayName: 'Delivered in',
            SystemName: 'hoursDiff',
            DataType: this._HelperService.AppConfig.DataType.Number,
            Show: true,
            Search: false,
            Sort: false,
          },
          {
            DisplayName: 'Carrier',
            SystemName: 'CarrierName',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Show: true,
            Search: true,
            Sort: true,
          },
          {
            DisplayName: 'Customer',
            SystemName: 'CustomerDisplayName',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Show: true,
            Search: true,
            Sort: true,
          },
          {
            DisplayName: 'Delivery Charge',
            SystemName: 'RateAmount',
            DataType: this._HelperService.AppConfig.DataType.Decimal,
            Show: true,
            Search: false,
            Sort: true,
          },
          {
            DisplayName: 'Total',
            SystemName: 'TotalAmount',
            DataType: this._HelperService.AppConfig.DataType.Decimal,
            Show: true,
            Search: false,
            Sort: true,
          },
        ]
    
        return TableFields;
      }
    
      TUTr_Setup() {
        var SearchCondition = undefined
        this.TUTr_Config =
        {
          Id: null,
          Task: this._HelperService.AppConfig.Api.ThankUCash.GetShipments,
          Location: this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment,
          Sort:
          {
            SortDefaultName: null,
            SortDefaultColumn: 'CreateDate',
            SortName: null,
            SortColumn: null,
            SortOrder: 'desc',
            SortOptions: [],
          },
          Title: "Orders",
          StatusType: null,
          SearchBaseCondition: SearchCondition,
          TableFields: this.TuTr_Setup_Fields(),
          DefaultSortExpression: "CreateDate desc",
        }
    
        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this._HelperService.Active_FilterInit(
          this._HelperService.AppConfig.FilterTypeOption.GetShipment,
          this.TUTr_Config
        );
        this.TUTr_GetData();
      }
    
      TUTr_GetData() {
        //this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.ThankUCash.GetShipment);
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.TUTr_Config = TConfig;
      }
    
      GetOverviews(ListOptions: any, Task: string): any {
        this._HelperService.IsFormProcessing = true;
    
        ListOptions.SearchCondition = '';
        ListOptions = this._DataHelperService.List_GetData(ListOptions);
    
        if (ListOptions.ActivePage == 1) {
          ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;
    
        if (ListOptions.Sort.SortDefaultName) {
          ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
          ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
          ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }
    
        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
          if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
            SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
          }
          else {
            SortExpression = ListOptions.Sort.SortColumn + ' desc';
          }
        }
    
        var pData = {
          Task: Task,
          TotalRecords: ListOptions.TotalRecords,
          Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
          Limit: ListOptions.PageRecordLimit,
          SearchCondition: ListOptions.SearchCondition,
          SortExpression: SortExpression
        };
    
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this.orderList = _Response.Result as any;
              // console.log("this.orderList.......", this.orderList)
              if (this.orderList && this.orderList.Data && this.orderList.Data.length > 0) {
                this.orderList.Data.forEach(element => {
                  if (element.CreateDate && element.DeliveryDate && element.StatusId !== 494) {
                    const startTime = moment(element.CreateDate);
                    const endTime = moment(element.DeliveryDate);
    
                    const hoursDiff = endTime.diff(startTime, 'hours');
                    element.hoursDiff = hoursDiff;
                    // console.log('Hours:' + hoursDiff);
                  }
                });
              }
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
    
          });
    
    
      }
    
      ngOnInit() {
        this._HelperService.StopClickPropogation();
        Feather.replace();
        this.TUTr_Setup();
      }
    
    }
    



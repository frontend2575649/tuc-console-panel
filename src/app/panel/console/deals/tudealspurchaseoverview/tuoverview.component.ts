import { ChangeDetectorRef, Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ChartDataSets } from 'chart.js';
import * as Feather from 'feather-icons';
import * as cloneDeep from 'lodash/cloneDeep';
import { BaseChartDirective, Color } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OResponse, OSalesTrend, ODealData, OSelect } from '../../../../service/service';
declare var moment: any;
declare var $: any;

@Component({
  selector: "tuoverview",
  templateUrl: "./tuoverview.component.html",
  styles: [
    `
      agm-map {
        height: 300px;
      }
    `
  ]
})
export class TuOverviewComponent implements OnInit {

  lastdaytext = "LAST DAY";
  lastweektext = "LAST WEEK";
  lastweekCustom = "LAST WEEK";
  lastmonthtext = "LAST MONTH";
  lastyeartext = "LAST YEAR";

  Types: any = {
    year: 'year',
    month: 'month',
    week: 'week',
    day: 'day',
    hour: 'hour'
  }

  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [87, 44, 22, 55, 77, 333, 222], borderDash: [10, 5], label: 'Series B' },

  ];
  public LineChartYearlyLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
  public lineChartYearlyData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40, 80, 81, 56, 55, 40] },
    { data: [87, 44, 22, 55, 77, 333, 222, 80, 181, 256, 155, 140], borderDash: [10, 5] },

  ];
  public LineWeelyChartLabels = ['MON', 'TUE', 'WED', 'THUS', 'FRI', 'SAT', 'SUN'];
  public lineChartRedeemData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40] },
    { data: [87, 44, 22, 55, 77, 333, 222], borderDash: [10, 5] },

  ];
  public lineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ]

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {

  }

  //#endregion
  ngOnInit() {
    window.addEventListener('scroll', this.scroll, true);
    this._HelperService.FullContainer = false;
    Feather.replace();
    //start time and end time for overview

    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];

      // this.pData.AccountId = this._HelperService.AppConfig.ActiveMerchantReferenceId;
      // this.pData.AccountKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;

      this.InitializeDates();

    });

  }

  scroll = (event): void => {
    $(".daterangepicker").hide();
    $(".form-daterangepicker").blur();
  };
  openDatepicker(event) {
    $(".daterangepicker").show();
  }
  ngOnDestroy(): void {
    window.removeEventListener('scroll', this.scroll, true);
  }

  InitializeDates(): void {

    //#region Sale Dates 

    this._Sale.ActualStartDate = moment().startOf('day');
    this._Sale.ActualEndDate = moment().endOf('day');

    this._Sale.CompareStartDate = moment().subtract(1, 'day').startOf('day');
    this._Sale.CompareEndDate = moment().subtract(1, 'day').endOf('day');

    //#endregion

    //#region Redeem Dates 

    this._Redeem.ActualStartDate = moment().startOf('week').startOf('day');
    this._Redeem.ActualEndDate = moment().endOf('day');

    this._Redeem.CompareStartDate = moment().subtract(1, 'week').startOf('week').startOf('day');
    this._Redeem.CompareEndDate = moment().subtract(1, 'week').endOf('week').endOf('day');

    //#endregion

    //#region Revenue Dates 

    this._Revenue.ActualStartDate = moment().startOf('month').startOf('day');
    this._Revenue.ActualEndDate = moment().endOf('month').endOf('day');

    this._Revenue.CompareStartDate = moment().subtract(1, 'months').startOf('month').startOf('day');
    this._Revenue.CompareEndDate = moment().subtract(1, 'months').endOf('month').endOf('day');

    this.RevenuebarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate));

    //#endregion

    //#region Yearly Dates 

    this._Yearly.ActualStartDate = moment().startOf('year').startOf('day');
    this._Yearly.ActualEndDate = moment().endOf('year').endOf('day');

    this._Yearly.CompareStartDate = moment().subtract(1, 'year').startOf('year').startOf('day');
    this._Yearly.CompareEndDate = moment().subtract(1, 'year').endOf('year').endOf('day');

    //#endregion

    var difference = moment().diff(moment(), 'days');

    if (difference <= 1) {
      this.CurrentRange = this._Sale;
      this._SaleDailyReportGetActualData(this.Types.hour);
    } else if (difference <= 7) {
      this.CurrentRange = this._Redeem;
      this._SaleDailyReportGetActualData(this.Types.week);
    } else if (difference <= 30) {
      this.CurrentRange = this._Revenue;
      this._SaleDailyReportGetActualData(this.Types.month);
    } else {
      this.CurrentRange = this._Yearly;
      this._SaleDailyReportGetActualData(this.Types.year);
    }

  }

  public CurrentRange: any = {};
  public DateSelected = moment().startOf("day");
  //#region DateChangeHandler
  DateChanged(event: any, Type: any): void {

    var ev: any = cloneDeep(event);

    this.DateSelected = moment(ev.start).startOf("day");

    //#region Sale 

    this._Sale.ActualStartDate = moment(ev.start).startOf("day");
    this._Sale.ActualEndDate = moment(ev.end).endOf("day");

    this._Sale.CompareStartDate = moment(ev.start).subtract(1, 'day').startOf("day");
    this._Sale.CompareEndDate = moment(ev.end).subtract(1, 'day').endOf("day");

    //#endregion
    //#region Week 

    this._Redeem.ActualStartDate = moment(ev.start).startOf('week').startOf('day');
    this._Redeem.ActualEndDate = moment(ev.start).endOf('week').endOf('day');

    this._Redeem.CompareStartDate = moment(ev.start).subtract(1, 'week').startOf('week').startOf('day');
    this._Redeem.CompareEndDate = moment(ev.start).subtract(1, 'week').endOf('week').endOf('day');

    //#endregion
    //#region Revenue 

    this._Revenue.ActualStartDate = moment(ev.start).startOf('month').startOf('day');
    this._Revenue.ActualEndDate = moment(event.end).endOf('month').endOf('day');

    this._Revenue.CompareStartDate = moment(ev.start).subtract(1, 'months').startOf('month').startOf('day');
    this._Revenue.CompareEndDate = moment(ev.end).subtract(1, 'months').endOf('month').endOf('day');

    this.RevenuebarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate).endOf('month').endOf('day'));
    //#endregion
    //#region Yearly 

    this._Yearly.ActualStartDate = moment(ev.start).startOf('year').startOf('day');
    this._Yearly.ActualEndDate = moment(event.end).endOf('year').endOf('day');

    this._Yearly.CompareStartDate = moment(ev.start).subtract(1, 'year').startOf('year').startOf('day');
    this._Yearly.CompareEndDate = moment(ev.end).subtract(1, 'year').endOf('year').endOf('day');

    //#endregion

    var difference = moment(ev.end).diff(moment(ev.start), 'days');

    if (difference <= 1) {
      this.CurrentRange = this._Sale;
      this._SaleDailyReportGetActualData(this.Types.hour);
    } else if (difference <= 7) {
      this.CurrentRange = this._Redeem;
      this._SaleDailyReportGetActualData(this.Types.week);
    } else if (difference <= 30) {
      this.CurrentRange = this._Revenue;
      this._SaleDailyReportGetActualData(this.Types.month);
    } else {
      this.CurrentRange = this._Yearly;
      this._SaleDailyReportGetActualData(this.Types.year);
    }

    this.RefreshDateLabels();

  }
  //#endregion

  //#region BarChartConfig 
  public Options: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      }
    }
  }
  public barChartLabels = [];
  // public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public barChartColors = [{ backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [200], label: 'Remote' },
    { data: [22], label: 'Remote' },
    { data: [22], label: 'Visit' },
    { data: [22], label: 'Visit' },
  ];

  //#endregion

  //#region RevenueBarChartConfig 
  public RevenueOptions: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      }
    }
  }
  public RevenuebarChartLabels = [];
  // public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public RevenuebarChartColors = [{ backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }];
  public RevenuebarChartType = 'bar';
  public RevenuebarChartLegend = true;
  public RevenuebarChartData = [
    { data: [200], label: 'Remote' },
    { data: [22], label: 'Remote' },
    { data: [22], label: 'Visit' },
    { data: [22], label: 'Visit' },
  ];


  public RevenuelineChartData: ChartDataSets[] = [
    { data: [], label: 'Current Month' },
    { data: [], borderDash: [10, 5], label: 'Compared Month' },
  ];

  public RevenuelineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ]

  //#endregion

  //#region Sale Sales Report 

  public SalelineChartData: ChartDataSets[] = [
    { data: [], label: 'Today' },
    { data: [], borderDash: [10, 5], label: 'Compared Day' },
  ];
  public SalelineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];

  public SalelineChartLabels = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00',
    '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
  showSaleChart = true;

  public _Sale: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    ActualSalesTotal: 0,
    CompareSalesTotal: 0,
    SalesAmountDifference: 0
  }

  private RefreshDateLabels() {
    this.lastyeartext = this._HelperService.GetDateSByFormat(this._Yearly.CompareStartDate, 'YYYY');
    this.lastdaytext = this._HelperService.GetDateS(this._Sale.CompareStartDate);

    this.lastweektext = this._HelperService.GetDateSByFormat(this._Redeem.CompareStartDate, 'DD MMM YY')
      + "-" + this._HelperService.GetDateSByFormat(this._Redeem.CompareEndDate, 'DD MMM YY');

    this.lastmonthtext = this._HelperService.GetDateSByFormat(this._Revenue.CompareStartDate, 'MMM YYYY');
  }

  public _SaleSalesReportReset(): void {


  }

  public _SaleDailyReportGetActualData(Type): void {

    this.SalelineChartData[0].data = [];
    this.RedeemlineChartData[0].data = [];
    this.RevenuelineChartData[0].data = [];

    this.SalelineChartData[1].data = [];
    this.RedeemlineChartData[1].data = [];
    this.RevenuelineChartData[1].data = [];

    if (Type == this.Types.hour) {
      this.GetSalesReportSale(this._Sale.ActualStartDate, this._Sale.ActualEndDate, this._Sale.ActualData, Type, 'actual');
      this.GetSalesReportSale(this._Sale.CompareStartDate, this._Sale.CompareEndDate, this._Sale.ActualData, Type, 'compare');
    } else if (Type == this.Types.week) {
      this.GetSalesReportRedeem(this._Redeem.ActualStartDate, this._Redeem.ActualEndDate, this._Redeem.ActualData, Type, 'actual');
      this.GetSalesReportRedeem(this._Redeem.CompareStartDate, this._Redeem.CompareEndDate, this._Redeem.ActualData, Type, 'compare');
    } else if (Type == this.Types.month || Type == this.Types.year) {
      this.GetSalesReport(this._Revenue.ActualStartDate, this._Revenue.ActualEndDate, this._Revenue.ActualData, Type, 'actual');
      this.GetSalesReport(this._Revenue.CompareStartDate, this._Revenue.CompareEndDate, this._Revenue.ActualData, Type, 'compare');
    }

    //this.GetSalesReport(this._Yearly.ActualStartDate, this._Yearly.ActualEndDate, this._Yearly.ActualData, this.Types.year, 'actual');

  }

  //#endregion

  //#region Redeem Sales Report 

  public RedeemlineChartData: ChartDataSets[] = [
    { data: [], label: 'Current Week' },
    { data: [], borderDash: [10, 5], label: 'Week Compared' },
  ];
  public RedeemlineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];
  public RedeemlineChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
  showRedeemChart = true;

  public _Redeem: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    ActualSalesTotal: 0,
    CompareSalesTotal: 0,

    SalesAmountDifference: 0
  }


  public _RedeemSalesReportReset(): void {


  }


  //#endregion

  //#region Revenue Sales Report 

  showRevenueChart = true;

  public _Revenue: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,

    ActualSalesTotal: 0,
    CompareSalesTotal: 0,

    SalesAmountDifference: 0
  }

  public _RevenueSalesReportReset(): void {


  }


  //#endregion

  //#region Customer Sales Report 

  public _Customer: any = {
    TotalCustomers: 0
  }

  //#endregion

  public _GetoverviewSummary: any = {};
  public TodayStartTime = null;
  public TodayEndTime = null;
  private pDealData = {
    Task: 'getdealsoverview',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetDealSalesOverview() {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.pDealData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetoverviewSummary = _Response.Result as any;
          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public _GetPurchaseoverviewSummary: any = {};
  private pDealPurchaseData = {
    Task: 'getdealspurchaseoverview',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetDealPurchaseOverview() {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.pDealPurchaseData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetPurchaseoverviewSummary = _Response.Result as any;
          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }



  //#region Yearly Sales Report 

  public YearlylineChartData: ChartDataSets[] = [
    { data: [], label: 'Current Year' },
    { data: [], borderDash: [10, 5], label: 'Compared Year' },
  ];
  public YearlylineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];
  public YearlylineChartLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
  showYearlyChart = true;

  public _Yearly: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,

    ActualSalesTotal: 0,
    CompareSalesTotal: 0,

    SalesAmountDifference: 0
  }


  public _YearlySalesReportReset(): void {


  }


  //#endregion

  //#region Sales History General Method 

  private pData = {
    Task: 'getdealhistory',
    StartDate: null,
    EndDate: null,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null,
    // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
  };


  GetSalesReport(StartDateTime, EndDateTime, Data: ODealData[], Type, LineType: string) {


    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);
    this.pData.Type = Type;

    if (Type == this.Types.month) {
      this.pData.Type = this.Types.day;
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as ODealData[];
          // Data = [
          //   {
          //     "Title": "2020-11",
          //     "Hour": 0,
          //     "Year": 2020,
          //     "Month": 11,
          //     "Customer": 1,
          //     "Amount": 184.0,
          //     "Total": 8,
          //     "Unused": 8,
          //     "Used": 33,
          //     "Expired": 0
          //   },
          //   {
          //     "Title": "2020-11",
          //     "Hour": 0,
          //     "Year": 2020,
          //     "Month": 11,
          //     "Customer": 1,
          //     "Amount": 184.0,
          //     "Total": 8,
          //     "Unused": 8,
          //     "Used": 33,
          //     "Expired": 0
          //   }
          // ];

          var TempArrayTotal = [];
          var TotalSum = 0;

          var TempArrayUnused = [];
          var UnusedSum = 0;

          var TempArrayUsed = [];
          var UsedSum = 0;

          var TempArrayAmountUsed = [];
          var UsedAmountSum = 0;

          var TempArrayAmountCommis = [];
          var CommisAmountSum = 0;

          var TempArrayExpired = [];
          var ExpiredSum = 0;

          var TempArrayAmount = [];
          var AmountSum = 0;

          var TempArrayCustomer = [];
          var CustomerSum = 0;

          if (Type == this._HelperService.AppConfig.GraphTypes.month) {

            var MonthAllDays: any[] = this._HelperService.CalculateIntermediateDate(cloneDeep(StartDateTime),
              cloneDeep(EndDateTime));
            for (let index = 0; index < MonthAllDays.length; index++) {
              const element = MonthAllDays[index];
              var RData: ODealData = Data.find(x => moment(x.Title, 'DD-MM-YYYY').format('DD MMM') == element);
              if (RData) {
                TempArrayTotal.push(RData.Total);
                TempArrayUnused.push(RData.Unused);
                TempArrayUsed.push(RData.Used);
                TempArrayExpired.push(RData.Expired);
                TempArrayAmount.push(RData.Amount);
                TempArrayCustomer.push(RData.Customer);
                TempArrayAmountUsed.push(RData.UsedAmount);
                TempArrayAmountCommis.push(RData.CommissionAmount);

                TotalSum = TotalSum + RData.Total;
                UnusedSum = UnusedSum + RData.Unused;
                UsedSum = UsedSum + RData.Used;

                ExpiredSum = ExpiredSum + RData.Expired;
                UsedAmountSum = UsedAmountSum + (RData.UsedAmount ? RData.UsedAmount : 0.0);
                CommisAmountSum = CommisAmountSum + (RData.CommissionAmount ? RData.CommissionAmount : 0.0);

                AmountSum = AmountSum + (RData.Amount ? RData.Amount : 0.0);
                CustomerSum = CustomerSum + RData.Customer;
              } else {
                TempArrayTotal.push(0);
                TempArrayUnused.push(0);
                TempArrayUsed.push(0);
                TempArrayAmountUsed.push(0);
                TempArrayExpired.push(0);
                TempArrayAmount.push(0);
                TempArrayCustomer.push(0);
                TempArrayAmountUsed.push(0);
                TempArrayAmountCommis.push(0);
              }
            }

            this.showSaleChart = false;
            this.showRevenueChart = false;
            this.showRedeemChart = false;
            this.showYearlyChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {

              this.SalelineChartData[0].data = TempArrayAmount;
              this.RedeemlineChartData[0].data = TempArrayAmountUsed;
              this.RevenuelineChartData[0].data = TempArrayAmountCommis;

              this._Sale.ActualSalesAmount = AmountSum;
              this._Sale.ActualSalesTotal = TotalSum;

              this._Redeem.ActualUsedAmount = UsedAmountSum;
              this._Redeem.ActualUsedTotal = UsedSum;

              // this._Redeem.ActualSalesAmount = UsedSum;
              this._Revenue.ActualCommissionAmount = CommisAmountSum;

              this._Customer.TotalCustomers = CustomerSum;
            } else {

              this.SalelineChartData[1].data = TempArrayAmount;
              this.RedeemlineChartData[1].data = TempArrayAmountUsed;
              this.RevenuelineChartData[1].data = TempArrayUnused;
              this.RevenuelineChartData[1].data = TempArrayAmountCommis;

              this._Sale.CompareSalesAmount = AmountSum;
              this._Sale.CompareSalesTotal = TotalSum;

              this._Redeem.CompareUsedAmount = UsedAmountSum;
              this._Redeem.CompareUsedTotal = UsedSum;

              // this._Redeem.CompareSalesAmount = UsedSum;
              this._Revenue.CompareCommissionAmount = CommisAmountSum;
              this._Customer.TotalCustomers = CustomerSum;
            }



            this.SalelineChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate));
            this.RevenuebarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate));
            this.RedeemlineChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Revenue.ActualStartDate), moment(this._Revenue.ActualEndDate));

            this.showSaleChart = true;
            this.showRevenueChart = true;
            this.showRedeemChart = true;
            this.showYearlyChart = true;
            this._ChangeDetectorRef.detectChanges();


          } else {

            for (let index = 0; index < 12; index++) {
              var RData: ODealData = Data.find(x => x['Month'] == index + 1);
              if (RData) {
                TempArrayTotal[index] = (RData.Total);
                TempArrayUnused[index] = (RData.Unused);
                TempArrayUsed[index] = (RData.Used);
                TempArrayExpired[index] = (RData.Expired);
                TempArrayAmount[index] = (RData.Amount);
                TempArrayAmountUsed[index] = (RData.UsedAmount);
                TempArrayCustomer[index] = (RData.Customer);
                TempArrayAmountCommis[index] = (RData.CommissionAmount);

                TotalSum = TotalSum + RData.Total;
                UnusedSum = UnusedSum + RData.Unused;
                UsedSum = UsedSum + RData.Used;
                ExpiredSum = ExpiredSum + RData.Expired;
                UsedAmountSum = UsedAmountSum + (RData.UsedAmount ? RData.UsedAmount : 0.0);
                CommisAmountSum = CommisAmountSum + (RData.CommissionAmount ? RData.CommissionAmount : 0.0);

                AmountSum = AmountSum + (RData.Amount ? RData.Amount : 0.0);

              } else {
                TempArrayTotal[index] = (0);
                TempArrayUnused[index] = (0);
                TempArrayUsed[index] = (0);
                TempArrayExpired[index] = (0);
                TempArrayAmount[index] = (0);
                TempArrayCustomer[index] = (0);
                TempArrayAmountCommis[index] = (0);
                TempArrayAmountUsed[index] = (0);
              }
            }

            this.showSaleChart = false;
            this.showRevenueChart = false;
            this.showRedeemChart = false;
            this.showYearlyChart = false;
            this._ChangeDetectorRef.detectChanges();



            if (LineType == 'actual') {

              this.SalelineChartData[0].data = TempArrayAmount;
              this.RedeemlineChartData[0].data = TempArrayAmountUsed;
              this.RevenuelineChartData[0].data = TempArrayAmountCommis;

              this._Sale.ActualSalesAmount = AmountSum;
              this._Sale.ActualSalesTotal = TotalSum;

              this._Redeem.ActualUsedAmount = UsedAmountSum;
              this._Redeem.ActualUsedTotal = UsedSum;

              // this._Redeem.ActualSalesAmount = UsedSum;
              this._Revenue.ActualCommissionAmount = CommisAmountSum;
            } else {

              this.SalelineChartData[1].data = TempArrayAmount;
              this.RedeemlineChartData[1].data = TempArrayAmountUsed;
              this.RevenuelineChartData[1].data = TempArrayAmountCommis;

              this._Sale.CompareSalesAmount = AmountSum;
              this._Sale.CompareSalesTotal = TotalSum;

              this._Redeem.CompareUsedAmount = UsedAmountSum;
              this._Redeem.CompareUsedTotal = UsedSum;

              // this._Redeem.CompareSalesAmount = UsedSum;
              this._Revenue.CompareCommissionAmount = CommisAmountSum;
            }



            this.SalelineChartLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
            this.RevenuebarChartLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
            this.RedeemlineChartLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];

            this.showSaleChart = true;
            this.showRevenueChart = true;
            this.showRedeemChart = true;
            this.showYearlyChart = true;
            this._ChangeDetectorRef.detectChanges();
          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  GetSalesReportSale(StartDateTime, EndDateTime, Data: ODealData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);


    this.pData.Type = this.Types.hour;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {

          var DataHourly = _Response.Result as ODealData[];

          // DataHourly = [
          //   {
          //     "Title": "2020-11",
          //     "Hour": 0,
          //     "Year": 2020,
          //     "Month": 11,
          //     "Customer": 1,
          //     "Amount": 184.0,
          //     "Total": 8,
          //     "Unused": 8,
          //     "Used": 33,
          //     "Expired": 0
          //   },
          //   {
          //     "Title": "2020-11",
          //     "Hour": 1,
          //     "Year": 2020,
          //     "Month": 11,
          //     "Customer": 1,
          //     "Amount": 184.0,
          //     "Total": 8,
          //     "Unused": 8,
          //     "Used": 33,
          //     "Expired": 0
          //   }
          // ];

          var TempArrayTotal = [];
          var TotalSum = 0;

          var TempArrayUnused = [];
          var UnusedSum = 0;

          var TempArrayUsed = [];
          var UsedSum = 0;

          var TempArrayAmountUsed = [];
          var UsedAmountSum = 0;

          var TempArrayAmountCommis = [];
          var CommisAmountSum = 0;

          var TempArrayExpired = [];
          var ExpiredSum = 0;

          var TempArrayAmount = [];
          var AmountSum = 0;

          var TempArrayAmount = [];
          var AmountSum = 0;

          var TempArrayCustomer = [];
          var CustomerSum = 0;

          // var Heigest: ODealData = {
          //   Hour: null,
          //   HourAmPm: '0:00 AM',
          //   HourAmPmNext: '1:00 AM',
          //   TotalTransaction: 0.0,
          //   TotalInvoiceAmount: 0.0
          // };

          // var Lowest: ODealData = {
          //   Hour: null,
          //   HourAmPm: '0:00 AM',
          //   HourAmPmNext: '1:00 AM',
          //   TotalTransaction: 0.0,
          //   TotalInvoiceAmount: 0.0
          // };


          for (let index = 0; index < 24; index++) {
            var RData: ODealData = DataHourly.find(x => x.Hour == index);

            if (RData != undefined && RData != null) {

              TempArrayTotal.push(RData.Total);
              TempArrayUnused.push(RData.Unused);
              TempArrayUsed.push(RData.Used);
              TempArrayExpired.push(RData.Expired);
              TempArrayAmount.push(RData.Amount);
              TempArrayCustomer.push(RData.Customer);
              TempArrayAmountUsed.push(RData.UsedAmount);
              TempArrayAmountCommis.push(RData.CommissionAmount);

              const element: ODealData = RData;

              TotalSum = TotalSum + element.Total;
              UnusedSum = UnusedSum + element.Unused;
              UsedSum = UsedSum + element.Used;
              ExpiredSum = ExpiredSum + element.Expired;
              AmountSum = AmountSum + element.Amount;
              CustomerSum = CustomerSum + element.Customer;
              UsedAmountSum = UsedAmountSum + (element.UsedAmount ? element.UsedAmount : 0.0);
              CommisAmountSum = CommisAmountSum + (element.CommissionAmount ? element.CommissionAmount : 0.0);
            }
            else {
              TempArrayTotal.push(0);
              TempArrayUnused.push(0);
              TempArrayUsed.push(0);
              TempArrayExpired.push(0);
              TempArrayAmount.push(0);
              TempArrayCustomer.push(0);
              TempArrayAmountUsed.push(0);
              TempArrayAmountCommis.push(0);
            }

          }



          if (Type == this._HelperService.AppConfig.GraphTypes.hour) {

            this.showSaleChart = false;
            this.showRevenueChart = false;
            this.showRedeemChart = false;
            this.showYearlyChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.SalelineChartData[0].data = TempArrayAmount;
              this.RedeemlineChartData[0].data = TempArrayAmountUsed;
              this.RevenuelineChartData[0].data = TempArrayAmountCommis;

              this._Sale.ActualSalesAmount = AmountSum;
              this._Sale.ActualSalesTotal = TotalSum;

              this._Redeem.ActualUsedAmount = UsedAmountSum;
              this._Redeem.ActualUsedTotal = UsedSum;

              // this._Redeem.ActualSalesAmount = UsedSum;
              this._Revenue.ActualCommissionAmount = CommisAmountSum;
              this._Customer.TotalCustomers = CustomerSum;
            } else {
              this.SalelineChartData[1].data = TempArrayAmount;
              this.RedeemlineChartData[1].data = TempArrayAmountUsed;
              this.RevenuelineChartData[1].data = TempArrayAmountCommis;

              this._Sale.CompareSalesAmount = AmountSum;
              this._Sale.CompareSalesTotal = TotalSum;

              this._Redeem.CompareUsedAmount = UsedAmountSum;
              this._Redeem.CompareUsedTotal = UsedSum;

              // this._Redeem.CompareSalesAmount = UsedSum;
              this._Revenue.CompareCommissionAmount = CommisAmountSum;
              this._Customer.TotalCustomers = CustomerSum;
            }

            this.SalelineChartLabels = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00',
              '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
            this.RevenuebarChartLabels = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00',
              '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
            this.RedeemlineChartLabels = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00',
              '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];

            this.showSaleChart = true;
            this.showRevenueChart = true;
            this.showRedeemChart = true;
            this.showYearlyChart = true;
            this._ChangeDetectorRef.detectChanges();
          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  GetSalesReportRedeem(StartDateTime, EndDateTime, Data: ODealData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    this.pData.Type = Type;

    if (Type == this.Types.day) {
      this.pData.Type = 'week';
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as ODealData[];

          // Data = [
          //   {
          //     "Title": "2020-11",
          //     "Hour": 0,
          //     "Year": 2020,
          //     "Month": 11,
          //     "Customer": 1,
          //     "Amount": 184.0,
          //     "Total": 8,
          //     "Unused": 8,
          //     "Used": 33,
          //     "Expired": 0
          //   },
          //   {
          //     "Title": "2020-11",
          //     "Hour": 0,
          //     "Year": 2020,
          //     "Month": 11,
          //     "Customer": 1,
          //     "Amount": 184.0,
          //     "Total": 8,
          //     "Unused": 8,
          //     "Used": 33,
          //     "Expired": 0
          //   }
          // ];

          var TempArrayTotal = [];
          var TotalSum = 0;

          var TempArrayUnused = [];
          var UnusedSum = 0;

          var TempArrayUsed = [];
          var UsedSum = 0;

          var TempArrayAmountUsed = [];
          var UsedAmountSum = 0;

          var TempArrayAmountCommis = [];
          var CommisAmountSum = 0;

          var TempArrayExpired = [];
          var ExpiredSum = 0;

          var TempArrayAmount = [];
          var AmountSum = 0;

          var TempArrayCustomer = [];
          var CustomerSum = 0;

          var TempColorArray = ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'];

          var Heigest = Data[0];
          var HeigestIndex = 0;
          var Lowest = Data[0];
          var LowestIndex = 0;

          var DataSale = _Response.Result as ODealData[];

          for (let index = 0; index < 7; index++) {

            var weekday = '';
            switch (index) {
              case 0: weekday = 'Sunday';
                break;
              case 1: weekday = 'Monday';

                break;
              case 2: weekday = 'Tuesday';

                break;
              case 3: weekday = 'Wednesday';

                break;
              case 4: weekday = 'Thursday';

                break;
              case 5: weekday = 'Friday';

                break;
              case 6: weekday = 'Saturday';
                break;

              default:
                break;
            }

            var RData: ODealData = DataSale.find(x => x['WeekDay'] == weekday);
            if (RData != undefined && RData != null) {

              TempArrayTotal.push(RData.Total);
              TempArrayUnused.push(RData.Unused);
              TempArrayUsed.push(RData.Used);
              TempArrayExpired.push(RData.Expired);
              TempArrayAmount.push(RData.Amount);
              TempArrayCustomer.push(RData.Customer);
              TempArrayAmountUsed.push(RData.UsedAmount);
              TempArrayAmountCommis.push(RData.CommissionAmount);

              const element: ODealData = RData;

              TotalSum = TotalSum + element.Total;
              UnusedSum = UnusedSum + element.Unused;
              UsedSum = UsedSum + element.Used;
              ExpiredSum = ExpiredSum + element.Expired;
              AmountSum = AmountSum + element.Amount;
              CustomerSum = CustomerSum + element.Customer;
              UsedAmountSum = UsedAmountSum + (element.UsedAmount ? element.UsedAmount : 0.0);
              CommisAmountSum = CommisAmountSum + (element.CommissionAmount ? element.CommissionAmount : 0.0);

              // if (Heigest.TotalInvoiceAmount < element.TotalInvoiceAmount) {
              //   Heigest = element;
              //   HeigestIndex = index;
              // }

              // if (Lowest.TotalInvoiceAmount > element.TotalInvoiceAmount) {
              //   Lowest = element;
              //   LowestIndex = index;
              // }

              TotalSum = TotalSum + element.Total;
              UnusedSum = UnusedSum + element.Unused;
              UsedSum = UsedSum + element.Used;
              ExpiredSum = ExpiredSum + element.Expired;
              AmountSum = AmountSum + element.Amount;
              CustomerSum = CustomerSum + element.Customer;
              UsedAmountSum = UsedAmountSum + (element.UsedAmount ? element.UsedAmount : 0.0);
              CommisAmountSum = CommisAmountSum + (element.CommissionAmount ? element.CommissionAmount : 0.0);
            }
            else {
              TempArrayTotal.push(0);
              TempArrayUnused.push(0);
              TempArrayUsed.push(0);
              TempArrayExpired.push(0);
              TempArrayAmount.push(0);
              TempArrayCustomer.push(0);
              TempArrayAmountUsed.push(0);
              TempArrayAmountCommis.push(0);
            }

          }

          if (Type == this._HelperService.AppConfig.GraphTypes.week) {

            this.showSaleChart = false;
            this.showRevenueChart = false;
            this.showRedeemChart = false;
            this.showYearlyChart = false;
            this._ChangeDetectorRef.detectChanges();



            if (LineType == 'actual') {
              this.SalelineChartData[0].data = TempArrayAmount;
              this.RedeemlineChartData[0].data = TempArrayAmountUsed;
              this.RevenuelineChartData[0].data = TempArrayAmountCommis;

              this._Sale.ActualSalesAmount = AmountSum;
              this._Sale.ActualSalesTotal = TotalSum;

              this._Redeem.ActualUsedAmount = UsedAmountSum;
              this._Redeem.ActualUsedTotal = UsedSum;

              // this._Redeem.ActualSalesAmount = UsedSum;
              this._Revenue.ActualCommissionAmount = CommisAmountSum;
              this._Customer.TotalCustomers = CustomerSum;
            } else {
              this.SalelineChartData[1].data = TempArrayAmount;
              this.RedeemlineChartData[1].data = TempArrayAmountUsed;
              this.RevenuelineChartData[1].data = TempArrayAmountCommis;

              this._Sale.CompareSalesAmount = AmountSum;
              this._Sale.CompareSalesTotal = TotalSum;

              this._Redeem.CompareUsedAmount = UsedAmountSum;
              this._Redeem.CompareUsedTotal = UsedSum;

              // this._Redeem.CompareSalesAmount = UsedSum;
              this._Revenue.CompareCommissionAmount = CommisAmountSum;
              this._Customer.TotalCustomers = CustomerSum;
            }

            this.SalelineChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
            this.RevenuebarChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
            this.RedeemlineChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

            this.showSaleChart = true;
            this.showRevenueChart = true;
            this.showRedeemChart = true;
            this.showYearlyChart = true;
            this._ChangeDetectorRef.detectChanges();
          }


          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  //#endregion

  computePerc(num: OSalesTrend): any {
    if (num.CompareSalesAmount == 0) {
      return '100 %';
    }
    if (num.ActualSalesAmount > num.CompareSalesAmount) {
      return Math.round(((num.ActualSalesAmount - num.CompareSalesAmount) / num.CompareSalesAmount) * 100) + ' %';
    } else if (num.ActualSalesAmount < num.CompareSalesAmount) {
      return Math.round(((num.CompareSalesAmount - num.ActualSalesAmount) / num.CompareSalesAmount) * 100) + ' %';
    } else {
      return '0 %';
    }
  }

  computeFlag(num: OSalesTrend): number {
    if (num.ActualSalesAmount > num.CompareSalesAmount) {
      return 1;
    } else if (num.ActualSalesAmount < num.CompareSalesAmount) {
      return -1;
    } else {
      return 0;
    }
  }

  getAbsolute(num: number): any {
    //return Math.abs(num);
    return num;
  }

  getAbs(numb: number): any {
    return Math.abs(numb);
  }



  hideSalePicker: boolean = true;
  hideRedeemPicker: boolean = true;
  hideRevenuePicker: boolean = true;
  hideYearlyPicker: boolean = true;

  ShowHideCalendar(type: string) {
    switch (type) {
      case this._HelperService.AppConfig.DatePickerTypes.hour: {
        this.hideSalePicker = !this.hideSalePicker;
      }

        break;
      case this._HelperService.AppConfig.DatePickerTypes.week: {
        this.hideRedeemPicker = !this.hideRedeemPicker;
      }

        break;
      case this._HelperService.AppConfig.DatePickerTypes.month: {
        this.hideRevenuePicker = !this.hideRevenuePicker;
      }

        break;
      case this._HelperService.AppConfig.DatePickerTypes.year: {
        this.hideYearlyPicker = !this.hideYearlyPicker;
      }

        break;

      default:
        break;
    }
  }

  CloseRowModal(): void { }
}


export class OAccountOverview {
  public TerminalStatus?: any;
  public TotalTerminals?: number;
  public TotalSale?: number;
  public AverageTransactionAmount?: number;
  public TotalTransactions?: number;
  public ActiveTerminalsPerc?: number;
  public IdleTerminalsPerc?: number;
  public DeadTerminalsPerc?: number;
  public UnusedTerminalsPerc?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public Idle?: number;
  public Total?: number;

  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
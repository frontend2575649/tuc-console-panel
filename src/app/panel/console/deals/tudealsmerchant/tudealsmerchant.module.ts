import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Ng5SliderModule } from 'ng5-slider';
import { MainPipe } from '../../../../service/main-pipe.module'
import { TUDealsMerchantComponent } from "./tudealsmerchant.component";
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { ImageCropperModule } from 'ngx-image-cropper';
const routes: Routes = [{ path: "", component: TUDealsMerchantComponent }];
const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 1,
};
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUDealsMerchantRoutingModule { }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    Ng5SliderModule,
    ImageCropperModule,
    TUDealsMerchantRoutingModule,
    InputFileModule.forRoot(config),
    MainPipe
  ],
  declarations: [TUDealsMerchantComponent]
})
export class TUDealsMerchantModule { }

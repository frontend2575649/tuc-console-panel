import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ChangeContext } from 'ng5-slider';
import { Observable, Subscription } from 'rxjs';
import swal from 'sweetalert2';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
declare var moment: any;
import { InputFileComponent, InputFile } from 'ngx-input-file';
declare var $: any;
import * as cloneDeep from 'lodash/cloneDeep';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { Select2OptionData } from 'ng2-select2';

@Component({
    selector: 'tu-tudealsmerchant',
    templateUrl: './tudealsmerchant.component.html',
})


export class TUDealsMerchantComponent implements OnInit {
    public SaveAccountRequest: any;
    public ResetFilterControls: boolean = true;
    public ShowCategorySelector: boolean = true;
    public _ObjectSubscription: Subscription = null;

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _FilterHelperService: FilterHelperService
    ) {
        this._HelperService.showAddNewPosBtn = false;
        this._HelperService.showAddNewStoreBtn = true;
        this._HelperService.showAddNewCashierBtn = false;
        this._HelperService.showAddNewSubAccBtn = false;

    }

    ngOnInit() {
        this._HelperService.StopClickPropogation();
        Feather.replace();
        this._ActivatedRoute.params.subscribe((params: Params) => {

            this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
            this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];

            this.DealMerchantList_Setup();
            // this.StoresList_Filter_Owners_Load();
            this.InitColConfig();
        });

        // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
        //     this.DealMerchantList_GetData();
        // });
        // this._HelperService.StopClickPropogation();
    }


    ngOnDestroy(): void {
        try { this._ObjectSubscription.unsubscribe(); } catch (error) { }
    }


    TempColumnConfig: any = [
        { Name: "DisplayName", Value: true, },
        { Name: "TotalDeals", Value: true, },
        { Name: "RunningDeals", Value: true, },
        { Name: "UpcomingDeals", Value: true, },
        { Name: "SoldDeals", Value: true, },
        { Name: "RedeemedDeals", Value: true, },
        { Name: "Added", Value: true, },
    ];

    ColumnConfig: any = [
        { Name: "DisplayName", Value: true, },
        { Name: "TotalDeals", Value: true, },
        { Name: "RunningDeals", Value: true, },
        { Name: "UpcomingDeals", Value: true, },
        { Name: "SoldDeals", Value: true, },
        { Name: "RedeemedDeals", Value: true, },
        { Name: "Added", Value: true, },
    ];

    InitColConfig() {
        var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
        var ColConfigExist: boolean =
            MerchantTableConfig != undefined && MerchantTableConfig != null;
        if (ColConfigExist) {
            this.ColumnConfig = MerchantTableConfig.config;
            this.TempColumnConfig = this._HelperService.CloneJson(MerchantTableConfig.config);
        }
    }

    OpenEditColModal() {
        this._HelperService.OpenModal("EditCol");
    }

    SaveEditCol() {
        this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
        this._HelperService.SaveStorage("BMerchantTable", { config: this.ColumnConfig, });
        this._HelperService.CloseModal("EditCol");
    }

    public DealMerchantList_Config: OList;
    DealMerchantList_Setup() {
        this.DealMerchantList_Config = {
            Id: null,
            // Sort: null,
            Task: 'getmerchantdealoverviewlist',
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals,
            Title: "Available Deal Merchant",
            StatusType: "default",
            DefaultSortExpression: "ReferenceId desc",
            IsDownload: true,
            Sort:
            {
                SortDefaultName: null,
                SortDefaultColumn: 'ReferenceId',
                SortName: null,
                SortColumn: '',
                SortOrder: 'desc',
                SortOptions: [],
            },
            // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
            TableFields: [
                {
                    DisplayName: " Merchant Name",
                    SystemName: "DisplayName",
                    DataType: this._HelperService.AppConfig.DataType.String,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Total Deals",
                    SystemName: "TotalDeals",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "text-center",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    DefaultValue: "ThankUCash",
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Running Deals",
                    SystemName: "RunningDeals",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },

                {
                    DisplayName: "Upcoming Deals",
                    SystemName: "UpcomingDeals",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "text-right",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Sold Deals",
                    SystemName: "SoldDeals",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "text-right",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Redeemed Deals",
                    SystemName: "RedeemedDeals",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "text-right",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: "CreateDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date text-right",
                    Show: true,
                    IsDateSearchField: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
            ]
        };

        this.DealMerchantList_Config.SearchBaseCondition = "";
        this.DealMerchantList_Config = this._DataHelperService.List_Initialize(this.DealMerchantList_Config);
        this._HelperService.Active_FilterInit(this._HelperService.AppConfig.FilterTypeOption.Stores, this.DealMerchantList_Config);
        this.DealMerchantList_GetData();
    }
    DealMerchantList_ToggleOption(event: any, Type: any) {

        if (event != null) {
            for (let index = 0; index < this.DealMerchantList_Config.Sort.SortOptions.length; index++) {
                const element = this.DealMerchantList_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }
            }
        }

        this._HelperService.Update_CurrentFilterSnap(event, Type, this.DealMerchantList_Config);
        this.DealMerchantList_Config = this._DataHelperService.List_Operations(this.DealMerchantList_Config, event, Type);
        if ((this.DealMerchantList_Config.RefreshData == true) && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.DealMerchantList_GetData();
        }

    }

    timeout = null;
    DealMerchantList_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {
            if (event != null) {
                for (let index = 0; index < this.DealMerchantList_Config.Sort.SortOptions.length; index++) {
                    const element = this.DealMerchantList_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {
                        element.SystemActive = true;
                    }
                    else {
                        element.SystemActive = false;
                    }
                }
            }

            this._HelperService.Update_CurrentFilterSnap(event, Type, this.DealMerchantList_Config);
            this.DealMerchantList_Config = this._DataHelperService.List_Operations(this.DealMerchantList_Config, event, Type);
            if ((this.DealMerchantList_Config.RefreshData == true) && this._HelperService.DataReloadEligibility(Type)) {
                this.DealMerchantList_GetData();
            }
        }, this._HelperService.AppConfig.SearchInputDelay);
    }

    DealMerchantList_GetData() {
        // this.GetOverviews(this.DealMerchantList_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.Getstoreoverview);
        var TConfig = this._DataHelperService.List_GetDataas(this.DealMerchantList_Config);
        this.DealMerchantList_Config = TConfig;
    }




    SetOtherFilters(): void {
        this.DealMerchantList_Config.SearchBaseConditions = [];
        this.DealMerchantList_Config.SearchBaseCondition = null;
        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
        if (CurrentIndex != -1) {
            // this.StoresList_Filter_Owners_Selected = null;
            this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetMerchantConfig(this.DealMerchantList_Config);
        this.SetOtherFilters();
        this.DealMerchantList_GetData();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_DealMerchant(Type, index);
        this._FilterHelperService.SetMerchantConfig(this.DealMerchantList_Config);
        this.SetOtherFilters();
        this.DealMerchantList_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            // input: "text",
            html:
                '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
                '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
                '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
            focusConfirm: false,
            preConfirm: () => {
                return {
                    filter: document.getElementById('swal-input1')['value'],
                    private: document.getElementById('swal-input2')['checked']
                }
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                if (result.value.filter.length < 5) {
                    this._HelperService.NotifyError('Enter filter name length greater than 4');
                    return;
                }

                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();
                this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);
                var AccessType: number = result.value.private ? 0 : 1;
                this._HelperService.Save_NewFilter(this._HelperService.AppConfig.FilterTypeOption.Stores, AccessType);
                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();
                this._HelperService.Delete_Filter(this._HelperService.AppConfig.FilterTypeOption.Stores);
                this._FilterHelperService.SetMerchantConfig(this.DealMerchantList_Config);
                this.DealMerchantList_GetData();
                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });

    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        this._HelperService.MakeFilterSnapPermanent();
        this.DealMerchantList_GetData();
        if (ButtonType == 'Sort') {
            $("#DealMerchantList_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#DealMerchantList_fdropdown").dropdown('toggle');
        }
        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetMerchantConfig(this.DealMerchantList_Config);
        this.SetOtherFilters();
        this.DealMerchantList_GetData();
        this.ResetFilterUI();
        this._HelperService.StopClickPropogation();
    }

    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();
        // this.StoresList_Filter_Owners_Load();
        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();
    }

    public OverviewData = { Total: 0, New: 0, Suspended: 0, Blocked: 0, Active: 0 };
    GetOverviews(ListOptions: any, Task: string): any {
        this._HelperService.IsFormProcessing = true;
        ListOptions.SearchCondition = '';
        ListOptions = this._DataHelperService.List_GetData(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;
        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }
        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }

        var pData = {
            Task: Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            IsDownload: false,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.accounts, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.OverviewData = _Response.Result as any;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });


    }






    // public StoresList_Filter_Owners_Option: Select2Options;
    // public StoresList_Filter_Owners_Selected = null;
    // StoresList_Filter_Owners_Load() {
    //     var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    //     var _Select: OSelect = {
    //         Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
    //         Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
    //         // ReferenceKey: this._HelperService.UserAccount.AccountKey,
    //         // ReferenceId: this._HelperService.UserAccount.AccountId,
    //         SearchCondition: "",
    //         SortCondition: [],
    //         Fields: [
    //             {
    //                 SystemName: "ReferenceId",
    //                 Type: this._HelperService.AppConfig.DataType.Number,
    //                 Id: true,
    //                 Text: false,
    //             },
    //             {
    //                 SystemName: "DisplayName",
    //                 Type: this._HelperService.AppConfig.DataType.Text,
    //                 Id: false,
    //                 Text: true,
    //             },
    //         ],
    //     };
    //     // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //     //   [
    //     //     this._HelperService.AppConfig.AccountType.Merchant,
    //     //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     //     this._HelperService.AppConfig.AccountType.PosAccount
    //     //   ]
    //     //   , '=');
    //     var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    //     this.StoresList_Filter_Owners_Option = {
    //         placeholder: "Sort by Referrer",
    //         ajax: _Transport,
    //         multiple: false,
    //         allowClear: true,
    //     };
    // }
    // StoresList_Filter_Owners_Change(event: any) {
    //     this._HelperService.Update_CurrentFilterSnap(
    //         event,
    //         this._HelperService.AppConfig.ListToggleOption.Other,
    //         this.DealMerchantList_Config,
    //         this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    //     );

    //     this.OwnerEventProcessing(event);

    // }

    OwnerEventProcessing(event: any): void {
        // if (event.value == this.StoresList_Filter_Owners_Selected) {
        //     var SearchCase = this._HelperService.GetSearchConditionStrict(
        //         "",
        //         "MerchantReferenceKey",
        //         this._HelperService.AppConfig.DataType.Text,
        //         this.StoresList_Filter_Owners_Selected,
        //         "="
        //     );
        //     this.DealMerchantList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        //         SearchCase,
        //         this.DealMerchantList_Config.SearchBaseConditions
        //     );
        //     this.StoresList_Filter_Owners_Selected = null;
        // } else if (event.value != this.StoresList_Filter_Owners_Selected) {
        //     var SearchCase = this._HelperService.GetSearchConditionStrict(
        //         "",
        //         "MerchantReferenceKey",
        //         this._HelperService.AppConfig.DataType.Text,
        //         this.StoresList_Filter_Owners_Selected,
        //         "="
        //     );
        //     this.DealMerchantList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        //         SearchCase,
        //         this.DealMerchantList_Config.SearchBaseConditions
        //     );
        //     this.StoresList_Filter_Owners_Selected = event.data[0].ReferenceKey;
        //     this.DealMerchantList_Config.SearchBaseConditions.push(
        //         this._HelperService.GetSearchConditionStrict(
        //             "",
        //             "MerchantReferenceKey",
        //             this._HelperService.AppConfig.DataType.Text,
        //             this.StoresList_Filter_Owners_Selected,
        //             "="
        //         )
        //     );
        // }

        this.DealMerchantList_ToggleOption(
            null,
            this._HelperService.AppConfig.ListToggleOption.ResetOffset
        );
    }




}




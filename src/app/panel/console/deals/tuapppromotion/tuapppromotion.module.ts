import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Ng5SliderModule } from 'ng5-slider';
import { MainPipe } from '../../../../service/main-pipe.module'
import { TUApppromotionComponent } from "./tuapppromotion.component";
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { ImageCropperModule } from 'ngx-image-cropper';
import { MerchantguardGuard } from "src/app/service/guard/merchantguard.guard";
const routes: Routes = [{ path: "",canActivate:[MerchantguardGuard], component: TUApppromotionComponent }];
const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 1,
};
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUApppromotionRoutingModule { }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    Ng5SliderModule,
    ImageCropperModule,
    TUApppromotionRoutingModule,
    InputFileModule.forRoot(config),
    MainPipe
  ],
  declarations: [TUApppromotionComponent]
})
export class TUApppromotionModule { }

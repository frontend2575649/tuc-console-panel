import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
declare var moment: any;
import { InputFileComponent, InputFile } from 'ngx-input-file';
declare var $: any;
import * as cloneDeep from 'lodash/cloneDeep';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { Select2OptionData } from 'ng2-select2';
@Component({
    selector: 'tu-tuapppromotion',
    templateUrl: './tuapppromotion.component.html',
})
export class TUApppromotionComponent implements OnInit {
    public isUpdate: boolean = false;
    public isDelete: boolean = false;
    public _SubAccountDetails: any =
        {
            "Amount": null,
            "PurchaseDate": null,
            "ExpiaryDate": null,
            "RedeemDate": null,
            "RedeemLocationId": null,
            "RedeemLocationKey": null,
            "RedeemLocationName": null,
            "RedeemLocationAddress": null,
            "ItemCode": null,
            "StatusCode": null,
            "StatusName": null,
            "Merchant": {
                "ReferenceId": null,
                "ReferenceKey": null,
                "DisplayName": null,
                "IconUrl": null
            },
            "Customer": {
                "ReferenceId": null,
                "ReferenceKey": null,
                "Name": null,
                "MobileNumber": null,
                "ImageUrl": null
            },
            "Deal": {
                "ReferenceId": null,
                "ReferenceKey": null,
                "Title": null,
                "ImageUrl": null
            }
        }


    _DealConfig =
        {

            Images: [],
            StartDateConfig: {
            },
            EndDateConfig: {
            },
            DefaultStartDate: null,
            DefaultEndDate: null,
            DealImages: [],
            StartDate: null,
            EndDate: null,
        }


    _ImageManager =
        {
            TCroppedImage: null,
            ActiveImage: null,
            ActiveImageName: null,
            ActiveImageSize: null,
            Option: {
                MaintainAspectRatio: "true",
                MinimumWidth: 800,
                MinimumHeight: 400,
                MaximumWidth: 800,
                MaximumHeight: 400,
                ResizeToWidth: 800,
                ResizeToHeight: 400,
                Format: "jpg",
            }
        }


    @ViewChild("inputfile")
    private InputFileComponent_Term: InputFileComponent;

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _FilterHelperService: FilterHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
    ) {

    }
    @ViewChild("offCanvas") divView: ElementRef;
    public ResetFilterControls: boolean = true;
    public AcquirerId = 0;
    ngOnInit() {
        Feather.replace();
        this._HelperService.StopClickPropogation();
        this.isUpdate = this._HelperService.SystemName.includes("apppromotionupdate");
        this.isDelete = this._HelperService.SystemName.includes("deleteapppromotion");
        this._HelperService.ValidateData();

        this.TUTr_Setup();
        this.GetAppCategories();
        this.Form_Promote_Load();
        this.Form_ManagePromote_Load();
        //deal date
        this._DealConfig.DefaultStartDate = moment();
        this._DealConfig.DefaultEndDate = moment().add(1, 'days').endOf("day");
        this._DealConfig.StartDate = this._DealConfig.DefaultStartDate;
        this._DealConfig.EndDate = this._DealConfig.DefaultEndDate;
        this._DealConfig.StartDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: this._HelperService.DateInUTC(moment()),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: this._HelperService.DateInUTC(moment()),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        //end deal date


    }


    UrlClick(Url) {
        // if (Url.startsWith("234")) {
        //     this._SubAccountDetails.MobileNumber = this._SubAccountDetails.MobileNumber.substring(3, this._SubAccountDetails.length);
        // }

        let url: string = '';
        if (!/^http[s]?:\/\//.test(Url)) {
            url += 'http://';
        }

        url += Url;
        window.open(url, '_blank');


        // window.open(Url, '_blank').focus();
    }

    public TUTr_Config: OList;
    public TuTr_Columns =
        {
            Status: true,
            Date: true,
            Account: true,
            AccountName: true,
            Mob: true,
            Biller: true,
            Amount: true,
            Source: true,
            PaymentRef: true
        }
    public TuTr_Filters_List =
        {
            Search: false,
            Date: false,
            Sort: false,
            Status: false,
            Biller: false
        }
    TUTr_Setup() {
        this.TUTr_Config = {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.getapppromotions,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.AppPromote,
            Title: "Deal",
            StatusType: "AppPromots",
            DefaultSortExpression: "CreateDate desc",
            //SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'dealcode.unused', '=='),
            TableFields: [
                {
                    DisplayName: "Navigate Url",
                    SystemName: "NavigateUrl",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "NavigateUrl"
                },
                {
                    DisplayName: "Navigation Type",
                    SystemName: "NavigationType",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "NavigationType"
                },

                {
                    DisplayName: "Start Date",
                    SystemName: "StartDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date text-right",
                    Show: true,
                    IsDateSearchField: false,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
                {
                    DisplayName: "End Date",
                    SystemName: "EndDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date text-right",
                    Show: true,
                    IsDateSearchField: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
            ]
        };

        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.SoldHistory,
            this.TUTr_Config
        );
        this.TUTr_GetData();
    }
    TuTr_Setup_SearchCondition() {
        this.TuTr_Columns =
        {
            Status: true,
            Date: true,
            Account: true,
            AccountName: true,
            Mob: true,
            Biller: true,
            Amount: true,
            Source: true,
            PaymentRef: true
        }
        var SearchCondition = '';
        this.TuTr_Filters_List =
        {
            Search: true,
            Date: true,
            Sort: true,
            Status: true,
            Biller: true
        }
        return SearchCondition;
    }
    TuTr_Setup_Fields() {
        var TableFields = [];
        TableFields = [
            {
                DisplayName: "Date",
                SystemName: 'CreateDate',
                DataType: this._HelperService.AppConfig.DataType.Date,
                Class: 'td-date',
                Show: true,
                Search: false,
                IsDateSearchField: true,
                Sort: true,
                ResourceId: null,
            },
            {
                DisplayName: "Expiry Date",
                SystemName: 'EndDate',
                DataType: this._HelperService.AppConfig.DataType.Date,
                Class: 'td-date',
                Show: true,
                Search: false,
                IsDateSearchField: true,
                Sort: true,
                ResourceId: null,
            },
            {
                DisplayName: 'Payment Account Number',
                SystemName: 'AccountNumber',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: false,
            },
            {
                DisplayName: 'Payment Account',
                SystemName: 'AccountDisplayName',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: false,
            },
            {
                DisplayName: 'Mobile',
                SystemName: 'AccountMobileNumber',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: true,
            },
            {
                DisplayName: 'Biller',
                SystemName: 'BillerName',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: true,
            },
            {
                DisplayName: 'Amount',
                SystemName: 'Amount',
                DataType: this._HelperService.AppConfig.DataType.Decimal,
                Show: true,
                Search: false,
                Sort: true,
            },
            {
                DisplayName: 'Source',
                SystemName: 'PaymentSource',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Class: '',
                Show: true,
                Search: false,
                Sort: false,
            },
            {
                DisplayName: 'Payment Reference',
                SystemName: 'PaymentReference',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: false,
            },
        ]

        return TableFields;
    }
    TUTr_ToggleOption_Date(event: any, Type: any) {
        this.TUTr_ToggleOption(event, Type);
    }

    TUTr_ToggleOption(event: any, Type: any) {
        if (event != null) {
            for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                const element = this.TUTr_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }
            }
        }
        if (Type == "date") {
            this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
            this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
        }
        this._HelperService.Update_CurrentFilterSnap(
            event,
            Type,
            this.TUTr_Config


        );

        this.TUTr_Config = this._DataHelperService.List_Operations(
            this.TUTr_Config,
            event,
            Type
        );

        if (
            (this.TUTr_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.TUTr_GetData();
        }

    }

    timeout = null;
    TUTr_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {


            this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

            this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
            this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

            this.TodayStartTime = this.TUTr_Config.StartTime;
            this.TodayEndTime = this.TUTr_Config.EndTime;
            if (event != null) {
                for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                    const element = this.TUTr_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {
                        element.SystemActive = true;
                    }
                    else {
                        element.SystemActive = false;
                    }

                }
            }


            if (
                (this.TUTr_Config.RefreshData == true)
                && this._HelperService.DataReloadEligibility(Type)
            ) {
                this.TUTr_GetData();
            }
        }, this._HelperService.AppConfig.SearchInputDelay);


    }

    public OverviewData: any = {
        Transactions: 0,
        InvoiceAmount: 0.0
    };

    TUTr_GetData() {
        // this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.ThankUCash.GetPurchaseHistoryOverview);
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    }
    dealId: any = null;
    dealkey: any = null;

    TUTr_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this.dealkey = ReferenceKey;
        this.dealId = ReferenceData.ReferenceId;
        this.ListAppUsage_GetDetails();

    }


    clicked() {
        $(this.divView.nativeElement).addClass('show');
        var backdrop: HTMLElement = document.getElementById("backdrop");
        backdrop.classList.add("show");
    }
    unclick() {
        $(this.divView.nativeElement).removeClass('show');
        var backdrop: HTMLElement = document.getElementById("backdrop");
        backdrop.classList.remove("show");
    }
    public _CoreUsage: OCoreUsage =
        {
            AccountDisplayName: null,
            Title: null,
            ToAccountId: null,
            ToAccountDisplayName: null,
            ToAccountMobileNumber: null,
            MerchantAmount: null,
            BankName: null,
            BankAccountNumber: null,
            BankAccountName: null,
            Charge: null,
            TotalAmount: null,
            ReferenceNumber: null,
            CreatedByDisplayName: null,
            ModifyByDisplayName: null,
            SystemComment: null,

            StatusB: null,
            ProductCategoryName: null,
            PaymentReference: null,
            ProductItemName: null,
            UserRewardAmount: null,
            Amount: null,
            RewardAmount: null,
            Coordinates: null,
            CommisonAmount: null,
            StartDate: null,
            EndDate: null,
            ModifyDate: null,
            CreateDate: null,
            MerchantDisplayName: null,
            LastUseLocationDisplayName: null,
            CashierDisplayName: null,
            MerchantIconUrl: null,
            CashierId: null,
            TerminalId: null,
            LastUseLocationSourceName: null,


            ProviderDisplayName: null,
            AccountMobileNumber: null,
            AccountKey: null, AccountId: null,
            ReferenceKey: null, ReferenceId: null, Reference: null, StatusCode: null, StatusId: null, StatusName: null, UserAccountKey: null, UserAccountIconUrl: null, UserAccountDisplayName: null, ApiKey: null, ApiName: null, AppKey: null, AppName: null, AppOwnerKey: null, AppOwnerName: null, AppVersionKey: null, AppVersionName: null, FeatureKey: null, FeatureName: null, IpAddress: null, Latitude: null, Longitude: null, ProcessingTime: null, Request: null, RequestTime: null, Response: null, ResponseTime: null, SessionId: null, SessionKey: null, UserAccountTypeCode: null, UserAccountTypeName: null,
        }

    ListAppUsage_GetDetails() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.GetPurchaseDetails,
            ReferenceKey: this.dealkey,
            ReferenceId: this.dealId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._CoreUsage = _Response.Result as any;
                    this._CoreUsage.StartDate = this._HelperService.GetDateTimeS(this._CoreUsage.StartDate);
                    this._CoreUsage.CreateDate = this._HelperService.GetDateTimeS(this._CoreUsage.CreateDate);
                    this._CoreUsage.EndDate = this._HelperService.GetDateTimeS(this._CoreUsage.EndDate);
                    this._CoreUsage.ModifyDate = this._HelperService.GetDateTimeS(this._CoreUsage.ModifyDate);
                    this._CoreUsage.StatusB = this._HelperService.GetStatusBadge(this._CoreUsage.StatusCode);
                    this.clicked()

                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }


    //#endregion

    SetOtherFilters(): void {
        this.TUTr_Config.SearchBaseConditions = [];
    }



    //#endregion






    //#endregion

    TodayStartTime = null;
    TodayEndTime = null;

    public _AccountOverview: OAccountOverview =
        {
            ActiveMerchants: 0,
            ActiveMerchantsDiff: 0,
            ActiveTerminals: 0,
            Idle: 0,
            Dead: 0,
            Active: 0,
            Inactive: 0,
            ActiveTerminalsDiff: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardPurchaseAmountDiff: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardPurchaseAmountDiff: 0,
            Merchants: 0,
            PurchaseAmount: 0,
            PurchaseAmountDiff: 0,
            Terminals: 0,
            Transactions: 0,
            TransactionsDiff: 0,
            UnusedTerminals: 0,
            IdleTerminals: 0,
            TerminalStatus: 0,
            DeadTerminals: 0,
            Total: 0,
            TotalTransactions: 0,
            TotalSale: 0,
        }

    Data: any = {};
    GetAccountOverviewLite(): void {
        this._HelperService.IsFormProcessing = true;
        this.Data = {
            Task: 'getaccountoverview',
            StartTime: this.TodayStartTime,
            EndTime: this.TodayEndTime,
            AccountKey: this._HelperService.UserAccount.AccountKey,
            AccountId: this._HelperService.UserAccount.AccountId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.Data);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._AccountOverview = _Response.Result as OAccountOverview;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();
        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();
    }

    SetSearchRanges(): void {
        //#region Invoice 
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('Amount', this.TUTr_Config.SearchBaseConditions);
    }

    //#region SalesOverview 

    GetOverviews(ListOptions: any, Task: string): any {
        this._HelperService.IsFormProcessing = true;

        ListOptions.SearchCondition = '';
        ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }


        var pData = {
            Task: Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            AccountId: ListOptions.AccountId,
            AccountKey: ListOptions.AccountKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.OverviewData = _Response.Result as any;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);

            });


    }

    //#endregion

    //#region filterOperations

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

        //#region setOtherFilters
        this.SetOtherFilters();

        //#endregion

        this.TUTr_GetData();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_Store(Type, index);
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
        if (Type == 'Time') {
            this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
            this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
        }
        //#region setOtherFilters
        this.SetOtherFilters();

        //#endregion

        this.TUTr_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            // input: "text",
            html:
                '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
                '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
                '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
            focusConfirm: false,
            preConfirm: () => {
                return {
                    filter: document.getElementById('swal-input1')['value'],
                    private: document.getElementById('swal-input2')['checked']
                }
            },
            // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            // inputAttributes: {
            //   autocapitalize: "off",
            //   autocorrect: "off",
            //   maxLength: "4",
            //   minLength: "4",
            // },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {

                if (result.value.filter.length < 5) {
                    this._HelperService.NotifyError('Enter filter name length greater than 4');
                    return;
                }

                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

                var AccessType: number = result.value.private ? 0 : 1;
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.SoldHistory,
                    AccessType
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._HelperService.Delete_Filter(
                    this._HelperService.AppConfig.FilterTypeOption.SoldHistory
                );
                this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
                this.TUTr_GetData();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {

        this.SetSearchRanges();

        this._HelperService.MakeFilterSnapPermanent();
        this.TUTr_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();

        if (ButtonType == 'Sort') {
            $("#TUTr_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#TUTr_fdropdown").dropdown('toggle');
        }
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

        //#region setOtherFilters
        this.SetOtherFilters();

        //#endregion
        this.TUTr_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }



    //varrify deal code start---
    Form_EditUser: FormGroup;

    VarifyDealCode() {
        this._HelperService.OpenModal("Form_Varify_deal_code");
    }

    VarifyDealdetails() {
        this._HelperService.OpenModal("Form_Deal_Code_Details");
    }



    public SaveAccountRequest: any;
    SelectStatusunused = false;
    SelectStatusredeem = false;
    SelectStatusExpired = false;
    SelectStatusBloacked = false;
    Form_EditUser_Process(_FormValue: any) {
        this.SaveAccountRequest = this.ReFormat_RequestBody();
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.SaveAccountRequest);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.SelectStatusunused = false;
                    this.SelectStatusredeem = false;
                    this.SelectStatusExpired = false;
                    this.SelectStatusBloacked = true;
                    // this.InputFileComponent_Term.files.pop();
                    this._HelperService.CloseModal("Form_Varify_deal_code");
                    this.VarifyDealdetails();
                    // if (_FormValue.OperationType == 'edit') {
                    // }
                    // else if (_FormValue.OperationType == 'close') {
                    //     this.Form_EditUser_Close();
                    // }
                    this.Form_EditUser_Close();
                }


                else {
                    this._HelperService.NotifyError(_Response.Message);

                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    ReFormat_RequestBody(): void {
        var formValue: any = cloneDeep(this.Form_EditUser.value);
        var formRequest: any = {
            'OperationType': 'new',
            'Task': 'validatedealcode',
            //"AccountId": formValue.AccountId,
            // "AccountKey": formValue.AccountKey,
            // "TypeCode": "deal",
            // "Title": formValue.Title,
            // "Description": formValue.Description,
            // "Terms": formValue.Terms,
            // "StartDate": formValue.StartDate,
            "ReferenceCode": formValue.Name,

            //  CodeValidityDays: "10",

            //  CodeValidityEndDate: "2020-11-14 00:00:00",
            //  UsageTypeCode: "hour",


        };


        //#endregion




        return formRequest;

    }


    Form_EditUser_Show() {
        this._HelperService.OpenModal("Form_EditUser_Content");
    }
    Form_EditUser_Close() {
        // this._Router.navigate([
        //     this._HelperService.AppConfig.Pages.System.AdminUsers
        // ]);
        this._HelperService.CloseModal("Form_EditUser_Content");
    }

    //#endregion

    imageChangedEvent: any = '';
    croppedImage: any = '';
    croppedImage1: any = ''
    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage = event.base64;
        this.croppedImage1 = this.croppedImage.replace("data:image/png;base64,", "")
        this._HelperService._Icon_Cropper_Data.Content = this.croppedImage1;
    }
    //promote deal code end---
    onImgError(event) {
        event.target.src = this.Selectedimage;
    }
    //promote deal code start---
    Form_Promote: FormGroup;
    Form_ManagePromote: FormGroup;

    Form_Promote_Show() {
        this._HelperService.Icon_Crop_Clear();
        this.InitImagePicker(this.InputFileComponent_Term);
        this._HelperService.OpenModal("_Icon_Cropper_Modal");
        // this._HelperService.OpenModal("_PreviewGeneral");
    }
    Form_Promote_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_Promote = this._FormBuilder.group({
            OperationType: 'new',
            Task: 'saveapppromotion',
            NavigationType: [null, Validators.required],
            NavigateUrl: [null, Validators.compose([Validators.minLength(2), Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')])],
            NavigationData: [null, Validators.required],
            ImageContent: null,
            StartDate: moment().format('DD-MM-YYYY hh:mm a'),
            EndDate: moment().add(1, 'days').endOf("day").format('DD-MM-YYYY hh:mm a'),
        });
    }

    ScheduleStartDateRangeChange(value) {
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: value.start,
            minDate: value.start,
        };
        this._DealConfig.StartDate = value.start;
        this._DealConfig.EndDate = value.start;
        this.Form_Promote.patchValue(
            {
                StartDate: value.start.format('DD-MM-YYYY hh:mm a'),
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    ScheduleEndDateRangeChange(value) {
        this._DealConfig.EndDate = value.start;
        this.Form_Promote.patchValue(
            {
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }

    ManageStartDateRangeChange(value) {
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: value.start,
            minDate: value.start,
        };
        this._DealConfig.StartDate = value.start;
        this._DealConfig.EndDate = value.start;
        this.Form_ManagePromote.patchValue(
            {
                StartDate: value.start.format('DD-MM-YYYY hh:mm a'),
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    ManageEndDateRangeChange(value) {
        this._DealConfig.EndDate = value.start;
        this.Form_ManagePromote.patchValue(
            {
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }

    model: any = {};
    Form_Promote_Process(_FormValue?: any) {
        this._HelperService.IsFormProcessing = true;
        var _FormValue = this.Form_Promote.value;
        this._HelperService.IsFormProcessing = true;
        var Request = this.ReFormat_RequestBody1();
        if (_FormValue.StartDate == undefined) {
            this.Form_Promote.patchValue(
                {
                    StartDate: moment(),
                }
            );
        }
        else if (_FormValue.EndDate == undefined) {
            this.Form_Promote.patchValue(
                {
                    EndDate: moment(),
                }
            );
        }
        else {
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(
                this._HelperService.AppConfig.NetworkLocation.Console.V3.AppPromote,
                Request
            );
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess(_Response.Message);
                        this.Form_Promote.reset();
                        this._DealConfig =
                        {
                            DefaultStartDate: null,
                            DefaultEndDate: null,

                            StartDate: null,
                            EndDate: null,
                            DealImages: [],

                            Images: [],
                            StartDateConfig: {
                            },
                            EndDateConfig: {
                            }
                        }
                        this._ImageManager =
                        {
                            TCroppedImage: null,
                            ActiveImage: null,
                            ActiveImageName: null,
                            ActiveImageSize: null,
                            Option: {
                                MaintainAspectRatio: "true",
                                MinimumWidth: 800,
                                MinimumHeight: 400,
                                MaximumWidth: 800,
                                MaximumHeight: 400,
                                ResizeToWidth: 800,
                                ResizeToHeight: 400,
                                Format: "jpg",
                            }
                        }
                        this._ImageManager.ActiveImage = null;
                        this._DealConfig.DealImages = [];
                        this._HelperService.CloseModal('Form_Varify_deal_code');
                        this.Form_Promote_Clear();
                        this.ngOnInit();
                        this.ngAfterViewInit();
                        this._HelperService.IsFormProcessing = false;
                        this.Form_Promote_Close();
                        this._HelperService.ObjectCreated.next(true);
                        this.Form_Promote_Clear();
                        this.Form_Promote_Close();
                        if (_FormValue.OperationType == "close") {
                            this.Form_Promote_Close();
                        }
                    } else {
                        this._HelperService.NotifyError(_Response.Message);
                    }

                    // } else {
                    //     this._HelperService.NotifyError(_Response.Message);
                    // }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                }
            );
        }

    }

    ngAfterViewInit() {
        $('#NavigationType').val(null).trigger('change');
    }

    Form_Promote_Clear() {
        this.Form_Promote.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_Promote_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }

    Form_Promote_Close() {
        this._HelperService.CloseModal("PromoteDeal");
        this._HelperService.CloseAllModal();

    }


    ReFormat_RequestBody1(): void {
        var formValue: any = cloneDeep(this.Form_Promote.value);
        var formRequest: any = {
            'OperationType': 'new',
            'Task': 'saveapppromotion',
            'NavigationType': formValue.NavigationType,
            "NavigateUrl": formValue.NavigateUrl,
            "NavigationData": formValue.NavigationData,
            StartDate: this._HelperService.HCXConvertDate(this._DealConfig.StartDate),
            EndDate: this._HelperService.HCXConvertDate(this._DealConfig.EndDate),
            "StatusCode": "default.active",
            "ImageContent": this._DealConfig.DealImages[0]
        };
        return formRequest;
    }

    CurrentImagesCount: number = 0;
    private InitImagePicker(InputFileComponent: InputFileComponent) {
        if (InputFileComponent != undefined) {
            this.CurrentImagesCount = 0;
            this._HelperService._InputFileComponent = InputFileComponent;
            InputFileComponent.onChange = (files: Array<InputFile>): void => {
                if (files.length >= this.CurrentImagesCount) {
                    this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
                }
                this.CurrentImagesCount = files.length;
            };
        }
    }

    //#region Image Manager
    onImageAccept(value) {
        setTimeout(() => {
            this.Form_Promote.patchValue(
                {
                    ImageContent: null,
                }
            );
        }, 300);
        this._ImageManager.ActiveImage = value;
        this._ImageManager.ActiveImageName = value.file.name;
        this._ImageManager.ActiveImageName = value.file.size;
    }

    onImageAccept1(value) {
        setTimeout(() => {
            this.Form_ManagePromote.patchValue(
                {
                    ImageContent: null,
                }
            );
        }, 300);
        this._ImageManager.ActiveImage = value;
        this._ImageManager.ActiveImageName = value.file.name;
        this._ImageManager.ActiveImageName = value.file.size;
    }

    Icon_B64Cropped(base64: string) {
        this._ImageManager.TCroppedImage = base64;
    }
    Icon_B64CroppedDone() {
        var ImageDetails = this._HelperService.GetImageDetails(this._ImageManager.TCroppedImage);
        var ImageContent =
        {
            //OriginalContent: this._ImageManager.TCroppedImage,
            Name: this._ImageManager.ActiveImageName,
            Size: this._ImageManager.ActiveImageSize,
            Extension: ImageDetails.Extension,
            Content: ImageDetails.Content
        };
        // if (this._DealConfig.DealImages.length == 0) {
        //     this._DealConfig.DealImages.push(
        //         {
        //             //ImageContent: ImageItem,
        //             ImageContent,
        //             IsDefault: 1,
        //         }
        //     );
        // }
        // else {
        //     this._DealConfig.DealImages.push(
        //         {
        //           //  ImageContent: ImageItem,
        //           ImageContent,
        //             IsDefault: 0,
        //         }
        //     );
        // }


        if (this._DealConfig.DealImages.length == 0) {
            this._DealConfig.DealImages.push(
                {
                    //ImageContent: ImageItem,
                    Name: this._ImageManager.ActiveImageName,
                    Size: this._ImageManager.ActiveImageSize,
                    Extension: ImageDetails.Extension,
                    Content: ImageDetails.Content,
                    IsDefault: 1,
                }
            );
        }
        else {
            this._DealConfig.DealImages.push(
                {
                    //  ImageContent: ImageItem,
                    Name: this._ImageManager.ActiveImageName,
                    Size: this._ImageManager.ActiveImageSize,
                    Extension: ImageDetails.Extension,
                    Content: ImageDetails.Content,
                    IsDefault: 0,
                }
            );
        }


        this._ImageManager.TCroppedImage = null;
        this._ImageManager.ActiveImage = null;
        this._ImageManager.ActiveImageName = null;
        this._ImageManager.ActiveImageSize = null;
    }
    Icon_Crop_Clear() {
        this._ImageManager.TCroppedImage = null;
        this._ImageManager.ActiveImage = null;
        this._ImageManager.ActiveImageName = null;
        this._ImageManager.ActiveImageSize = null;
        this._HelperService.CloseModal('_Icon_Cropper_Modal');
    }
    RemoveImage(Item) {
        this._DealConfig.DealImages = this._DealConfig.DealImages.filter(x => x != Item);
    }

    // removeImage(): void {
    //     //this.CurrentImagesCount = 0;
    //     this._DealConfig.DealImages = []
    // }

    removeImage(): void {
        this.CurrentImagesCount = 0;
        this._HelperService.Icon_Crop_Clear();
        var Req = this.DeleteApprove_RequestBody();
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.AppPromote, Req);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess("Image Removed Successfully");
                    this.Selectedimage = 'https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/defaults/defaulticon.png'
                }
                else {

                    this._HelperService.NotifyError("Image Not Found");
                }
            },
            _Error => {

                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    DeleteApprove_RequestBody(): void {
        var formRequest: any = {
            'OperationType': 'new',
            'Task': 'removeapppromtoionimage',
            //'ReferenceKey': this.SelectedDeal.ReferenceKey,
            // 'ReferenceId': this.SelectedDeal.ReferenceId,

            "ReferenceKey": this.ManageReferenceKey,
            'ReferenceId': this.ManageReferenceId,
            // 'IconUrl':'https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/o/2021/12/51da6698fc8044b1a2d4ed78aee6f2f8.png'


        };
        //#region Set Schedule 

        //#endregion

        return formRequest;

    }

    Approve_RequestBody(): void {
        var formRequest: any = {
            'OperationType': 'new',
            'Task': 'removeprofileimage',
            //'ReferenceKey': this.SelectedDeal.ReferenceKey,
            // 'ReferenceId': this.SelectedDeal.ReferenceId,
            "ReferenceKey": this.ManageReferenceKey,
            'ReferenceId': this.ManageReferenceId,
            // 'IconUrl':'https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/o/2021/12/51da6698fc8044b1a2d4ed78aee6f2f8.png'


        };
        //#region Set Schedule 

        //#endregion

        return formRequest;

    }


    //Edit promoted deal--
    //  Form_ManagePromote:FormGroup;
    ManagePromoteDeal: any = {};
    SelectedDealStartDatePromote: any;
    SelectedDealEndDatePromote: any;
    Selectedurl: any;
    SelectedRole: any;
    SelectedDealStartDatePromote1: any;
    SelectedDealEndDatePromote1: any;
    // NavigationType:any;
    NavigationData: any;
    ManageReferenceKey: any;
    ManageReferenceId: any;
    Selectedimage: any

    checklist: boolean = false;
    CheckStatus: any;
    ManageAsPromote(ReferenceData: any): void {
        this.ManagePromoteDeal = {};
        this.ManageReferenceKey = ReferenceData.ReferenceKey;
        this.ManageReferenceId = ReferenceData.ReferenceId;
        this.SelectedDealStartDatePromote = this._HelperService.GetDateTimeSPromote(ReferenceData.StartDate);
        this.SelectedDealEndDatePromote = this._HelperService.GetDateTimeSPromote(ReferenceData.EndDate);
        this.Selectedurl = ReferenceData.NavigateUrl;
        this.DealStatus = ReferenceData.StatusCode;
        this.SelectedRole = ReferenceData.NavigationType;
        //  this.NavigationType=ReferenceData.NavigationType
        this.NavigationData = ReferenceData.NavigationData;
        this.Form_ManagePromote.controls['StartDate'].setValue(moment(ReferenceData.StartDate).format('DD-MM-YYYY hh:mm a'));
        this.Form_ManagePromote.controls['EndDate'].setValue(moment(ReferenceData.EndDate).format('DD-MM-YYYY hh:mm a'));
        var tStartDate = moment(ReferenceData.StartDate);
        var tEndDate = moment(ReferenceData.EndDate);


        this._DealConfig.StartDate = tStartDate;
        this._DealConfig.EndDate = tEndDate;



        if (ReferenceData.ImageUrl != null || ReferenceData.ImageUrl != undefined) {
            this.Selectedimage = ReferenceData.ImageUrl
        }
        else {
            this.Selectedimage = 'https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/defaults/defaulticon.png';
        }

        this.CheckStatus = ReferenceData.StatusCode

        // this.SelectedDealStartDatePromote1= ReferenceData.StartDate;
        // this.SelectedDealEndDatePromote1= ReferenceData.EndDate;
        // this.Form_ManagePromote.controls['StartDate'].setValue(moment(ReferenceData.StartDate).format('DD-MM-YYYY hh:mm a'));
        // this.Form_ManagePromote.controls['EndDate'].setValue(moment(ReferenceData.EndDate).format('DD-MM-YYYY hh:mm a'));

        if (this.SelectedRole == 'app') {
            this.checklist = true;
        }
        else {
            this.checklist = false;
        }
        this._HelperService.OpenModal('ManagePromoteDeal');
    }


    Form_ManagePromote_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_ManagePromote = this._FormBuilder.group({
            OperationType: 'new',
            Task: 'updateapppromotion',
            //TypeCode: 'deal',
            NavigationType: [null, Validators.required],
            NavigateUrl: [null, Validators.required],
            NavigationData: [null, Validators.required],

            ImageContent: null,
            StartDate: moment().format('DD-MM-YYYY hh:mm'),
            EndDate: moment().add(1, 'days').endOf("day").format('DD-MM-YYYY hh:mm'),


        });
    }

    Form_ManagePromote_Process(_FormValue?: any) {
        var _FormValue = this.Form_ManagePromote.value;

        this._HelperService.IsFormProcessing = true;
        // var Request = this.CreateCashierRequest(_FormValue);
        var Request = this.ReFormat_RequestBody2();
        if (_FormValue.StartDate == this.SelectedDealStartDatePromote) {
            this.Form_ManagePromote.patchValue(
                {
                    StartDate: this.SelectedDealStartDatePromote1
                }

            );
        }

        if (_FormValue.EndDate == this.SelectedDealEndDatePromote) {
            this.Form_ManagePromote.patchValue(
                {
                    EndDate: this.SelectedDealEndDatePromote1
                }

            );
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.NetworkLocation.Console.V3.AppPromote,
            Request
        );
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    // this._HelperService.FlashSwalSuccess("New pos terminal has been added successfully",
                    //   "You have successfully created new POS terminal");
                    // this._HelperService.ObjectCreated.next(true);

                    this._HelperService.NotifySuccess(_Response.Message);
                    // this.TUTr_Setup();

                    this._DealConfig =
                    {
                        DefaultStartDate: null,
                        DefaultEndDate: null,
                        // SelectedDealCodeHours: 12,

                        StartDate: null,
                        EndDate: null,
                        DealImages: [],

                        Images: [],
                        StartDateConfig: {
                        },
                        EndDateConfig: {
                        },

                    }
                    this._ImageManager =
                    {
                        TCroppedImage: null,
                        ActiveImage: null,
                        ActiveImageName: null,
                        ActiveImageSize: null,
                        Option: {
                            MaintainAspectRatio: "true",
                            MinimumWidth: 800,
                            MinimumHeight: 400,
                            MaximumWidth: 800,
                            MaximumHeight: 400,
                            ResizeToWidth: 800,
                            ResizeToHeight: 400,
                            Format: "jpg",
                        }
                    }
                    this._ImageManager.ActiveImage = null;
                    this._DealConfig.DealImages = [];

                    //this.removeImage();


                    this._HelperService.CloseModal('ManagePromoteDeal');
                    this._HelperService.IsFormProcessing = false;

                    this._DealConfig.DealImages = [];
                    this.Form_ManagePromote_Clear();
                    this.ngOnInit();
                    this.ResetFilterUI();
                    this._HelperService.Icon_Crop_Clear();

                    this.Form_ManagePromote_Close();
                    if (_FormValue.OperationType == "close") {
                        this.Form_ManagePromote_Close();
                    }
                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            }
        );
    }

    Form_ManagePromote_Clear() {
        this.Form_ManagePromote.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_ManagePromote_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }

    Form_ManagePromote_Close() {
        this._HelperService.CloseModal("ManagePromoteDeal");
        this._HelperService.CloseAllModal();
    }

    ReFormat_RequestBody2(): void {
        var formValue: any = cloneDeep(this.Form_ManagePromote.value);
        var formRequest: any = {
            'OperationType': 'new',
            'Task': 'updateapppromotion',

            "ReferenceId": this.ManageReferenceId,
            "ReferenceKey": this.ManageReferenceKey,
            //   "DealId": this._UserAccount.ReferenceId,
            //   "DealKey": this._UserAccount.ReferenceKey,


            'NavigationType': formValue.NavigationType,

            "NavigateUrl": formValue.NavigateUrl,
            "NavigationData": formValue.NavigationData,

            StartDate: moment(this._DealConfig.StartDate).format('YYYY-MM-DD HH:mm'),
            EndDate: moment(this._DealConfig.EndDate).format('YYYY-MM-DD HH:mm'),

            "StatusCode": "default.active",

            // "ImageContent": this._DealConfig.DealImages[0]

            // "Url": formValue.Url,

            // "StartDate": formValue.StartDate,
            // "EndDate": formValue.EndDate,

            //"ImageContent": this._HelperService._Icon_Cropper_Data,
            "ImageContent": this._DealConfig.DealImages[0]


        };


        return formRequest;

    }

    ScheduleManageStartDateRangeChange(value) {
        this.Form_ManagePromote.patchValue(
            {
                StartDate: value.start,

            }
        );
    }

    ScheduleManageEndDateRangeChange(value) {
        this.Form_ManagePromote.patchValue(
            {
                EndDate: value.start,

            }
        );
    }
    //#endregion

    //stop promotion
    PromoteDeal: any = {};
    detetepromoteId: any;
    detetepromoteKey: any;
    UnMarkAsPromote(ReferenceData: any): void {
        this.PromoteDeal = {};
        this.detetepromoteId = ReferenceData.ReferenceId;
        this.detetepromoteKey = ReferenceData.ReferenceKey;

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.RemoveAppPromoteTitle,
            text: this._HelperService.AppConfig.CommonResource.RemoveAppPromoteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {

                this._HelperService.IsFormProcessing = true;
                var PData =
                {
                    Task: this._HelperService.AppConfig.Api.ThankUCash.deleteapppromotion,
                    ReferenceId: this.detetepromoteId,
                    ReferenceKey: this.detetepromoteKey,
                    // StatusCode: this.selectedStatusItem.statusCode,

                }
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.AppPromote, PData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("App Promotion Deleted Successfully. It will take upto 5 minutes to update changes.");
                            this.TUTr_Setup();
                            // this._HelperService.CloseModal('FlashDeal');
                            this._HelperService.IsFormProcessing = false;

                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    }
                    ,
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                        this._HelperService.ToggleField = false;
                    });

            }
        });

    }

    //#region Navigation Type - Start
    public _NavigationType = [
        {
            'id': 0,
            'text': 'Navigation Type',
            'apival': 'Navigation Type'
        },
        {
            'id': 758,
            'text': 'APP',
            'apival': 'app'
        },
        {
            'id': 759,
            'text': 'Url',
            'apival': 'url'
        }
    ];
    public GetNavigationType_Option: Select2Options;
    public GetNavigationType_Transport: any;
    GetNavigationType_list() {
        this.GetNavigationType_Option = {
            placeholder: 'List',
            ajax: this.GetNavigationType_Transport,
            multiple: false,
        };
    }
    Checkrole: boolean = false;
    GetNavigationType_ListChange(event: any) {
        if (event.data[0].apival == 'app') {
            this.Checkrole = true;
        }
        else {
            this.Checkrole = false;
        }
        this.Form_Promote.patchValue(
            {
                NavigationType: event.data[0].apival,
            }
        );
    }
    Checkmanagerole: boolean = false;
    public GetNavigationTypeManage_Option: Select2Options;
    public GetNavigationTypeManage_Transport: any;
    GetNavigationTypeManage_ListChange(event: any) {
        if (event.data[0].apival == 'app') {
            this.Checkmanagerole = true;
            this.SelectedRole = 'app'
        }
        else {
            this.Checkmanagerole = false;
            this.SelectedRole = 'url'
        }
        this.Form_ManagePromote.patchValue(
            {
                NavigationType: event.data[0].apival,
            }
        );
    }

    //#endregion Navigation Type - End


    //#region App Pages - Start
    public _AppPage = [
        // {
        //     'id': 0,
        //     'text': 'Select Page',
        //     'apival': 'Select Page'
        // },
        {
            'id': 758,
            'text': 'Dashboard',
            'apival': 'dashboard'
        },
        {
            'id': 759,
            'text': 'About',
            'apival': 'about'
        },
        {
            'id': 759,
            'text': 'Live Support',
            'apival': 'livesupport'
        },
        {
            'id': 759,
            'text': 'Profile',
            'apival': 'profile'
        },
        {
            'id': 759,
            'text': 'Update Pin',
            'apival': 'updatepin'
        },
        {
            'id': 759,
            'text': 'Notifications',
            'apival': 'notifications'
        },
        {
            'id': 759,
            'text': 'FAQ',
            'apival': 'faq'
        },
        {
            'id': 759,
            'text': 'Sales History',
            'apival': 'saleshistory'
        },
        {
            'id': 759,
            'text': 'Points History',
            'apival': 'pointshistory'
        },
        {
            'id': 759,
            'text': 'LCC TopUp',
            'apival': 'lcctopup'
        },
        {
            'id': 759,
            'text': 'Select Biller',
            'apival': 'selectbiller'
        },
        {
            'id': 759,
            'text': 'Stores',
            'apival': 'stores'
        },
        {
            'id': 759,
            'text': 'Deals Dashboard',
            'apival': 'dealsdashboard'
        },
        {
            'id': 759,
            'text': 'Flash Deals',
            'apival': 'flashdeals'
        }
    ];
    //#endregion App Pages - End




    //state
    public StateCategories = [];
    public S2AppPages = [];
    public stateerrro: boolean = false;
    GetAppCategories() {
        this._HelperService.ToggleField = true;
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.ThankUCash.getappsliders,
            TotalRecords: 0,
            Offset: 0,
            Limit: 10,
            RefreshCount: true,
            SearchCondition: "",
            SortExpression: "",
            ReferenceKey: null,
            ReferenceId: 0,
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.AppPromote, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {

                    if (_Response.Result != undefined) {
                        this.StateCategories = _Response.Result;
                        this._ChangeDetectorRef.detectChanges();
                        this.S2AppPages.push(
                            {
                                id: 0,
                                key: "0",
                                text: "Select Page",
                                apival: 'Navigation Type'
                            }
                        );
                        for (let index = 0; index < this.StateCategories.length; index++) {
                            const element = this.StateCategories[index];
                            this.S2AppPages.push(
                                {
                                    id: 0,
                                    //  key: element.ReferenceKey,
                                    text: element.Name,
                                    apival: element.SystemName
                                }
                            );
                        }
                        this._ChangeDetectorRef.detectChanges();
                        this._HelperService.ToggleField = false;
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                this._HelperService.ToggleField = false;

            });
    }
    // public SelectedStateCategories = [];
    // public selectedstate: boolean = false;
    // statekey: any; stateid: any; statename: any;
    // AppSelected(Items) {
    //     if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
    //         this.SelectedStateCategories = Items.value;
    //         this.statekey = Items.data[0].key;
    //         this.stateid = Items.data[0].id;
    //         this.statename = Items.data[0].text;
    //         this.selectedstate = true;
    //         this.Form_Promote.patchValue(
    //             {
    //                 NavigationUrl: this.statename,
    //                 // TypeId: event.value
    //             }
    //         );
    //         // this.GetCityCategories()
    //     }
    //     else {
    //         this.SelectedStateCategories = [];
    //     }
    // }


    // public GetApps_Option: Select2Options;
    // public GetApps_Transport: any;
    // getrApps_list() {
    //     this.GetApps_Option = {
    //         placeholder: 'List',
    //         ajax: this.GetApps_Transport,
    //         multiple: false,
    //     };
    // }
    GetApp_ListChange(event: any) {
        this.Form_Promote.patchValue(
            {
                NavigateUrl: event.data[0].apival,
                // TypeId: event.value
            }
        );
    }


    // public GetManageApp_Option: Select2Options;
    // public GetManageApp_Transport: any;
    // getManageApp_list() {
    //     this.GetManageApp_Option = {
    //         placeholder: 'List',
    //         ajax: this.GetManageApp_Transport,
    //         multiple: false,
    //     };
    // }
    GetAppmanage_ListChange(event: any) {
        this.Form_ManagePromote.patchValue(
            {
                NavigateUrl: event.data[0].apival,
                // TypeId: event.value
            }
        );
        this.Selectedurl = event.data[0].apival;
    }


    //start stop app promote
    DealStatus: any;
    OnOffPromoteDeal() {

        if (this.DealStatus == 'default.inactive') {
            this.StartAsPromote();
        }
        else {
            this.StopAsPromote();
        }
    }
    StopAsPromote(): void {


        this.PromoteDeal = {};
        this.detetepromoteId = this.ManageReferenceId;
        this.detetepromoteKey = this.ManageReferenceKey;
        var formValue: any = cloneDeep(this.Form_ManagePromote.value);
        swal({
            position: "center",
            title: "Stop this App to Promote?",
            text: "Click Continue to Stop Promote this App",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {

                this._HelperService.IsFormProcessing = true;
                var PData =
                {
                    Task: 'updateapppromotion',
                    "ReferenceId": this.detetepromoteId,
                    "ReferenceKey": this.detetepromoteKey,

                    'NavigationType': formValue.NavigationType,
                    "NavigateUrl": formValue.NavigateUrl,
                    "NavigationData": formValue.NavigationData,

                    StartDate: moment(this._DealConfig.StartDate).format('YYYY-MM-DD hh:mm'),
                    EndDate: moment(this._DealConfig.EndDate).format('YYYY-MM-DD hh:mm'),


                    "TypeCode": 'home',
                    "TypeId": 758,
                    "StatusCode": "default.inactive",

                    "ImageContent": this._DealConfig.DealImages[0]


                }


                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.AppPromote, PData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("App Promotion Stopped Successfully. It will take upto 5 minutes to update changes.");
                            this._HelperService.CloseModal('ManagePromoteDeal')
                            this.TUTr_Setup();
                            // this._HelperService.CloseModal('FlashDeal');
                            this._HelperService.IsFormProcessing = false;

                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    }
                    ,
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                        this._HelperService.ToggleField = false;
                    });

            }
        });

    }

    StartAsPromote(): void {
        var formValue: any = cloneDeep(this.Form_ManagePromote.value);
        this.PromoteDeal = {};
        this.detetepromoteId = this.ManageReferenceId;
        this.detetepromoteKey = this.ManageReferenceKey;

        swal({
            position: "center",
            title: "Start this App to Promote?",
            text: "Click Continue to Start Promote this App",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {

                this._HelperService.IsFormProcessing = true;
                var PData =
                {
                    Task: 'updateapppromotion',
                    "ReferenceId": this.detetepromoteId,
                    "ReferenceKey": this.detetepromoteKey,

                    'NavigationType': formValue.NavigationType,
                    "NavigateUrl": formValue.NavigateUrl,
                    "NavigationData": formValue.NavigationData,

                    StartDate: moment(this._DealConfig.StartDate).format('YYYY-MM-DD HH:mm'),
                    EndDate: moment(this._DealConfig.EndDate).format('YYYY-MM-DD HH:mm'),
                    "TypeCode": 'home',
                    "TypeId": 758,
                    "StatusCode": "default.active",

                    "ImageContent": this._DealConfig.DealImages[0]


                }


                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.AppPromote, PData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("App Promotion Started Successfully. It will take upto 5 minutes to update changes.");
                            this._HelperService.CloseModal('ManagePromoteDeal')
                            this.TUTr_Setup();
                            // this._HelperService.CloseModal('FlashDeal');
                            this._HelperService.IsFormProcessing = false;

                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    }
                    ,
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                        this._HelperService.ToggleField = false;
                    });

            }
        });

    }

}
export class OAccountOverview {
    public TotalTransactions?: number;
    public TotalSale?: number;
    public DeadTerminals?: number;
    public IdleTerminals?: number;
    public TerminalStatus?: number;
    public Total?: number;
    public Idle?: number;
    public Active?: number;
    public Dead?: number;
    public Inactive?: number;
    public UnusedTerminals?: number;
    public Merchants: number;
    public ActiveMerchants: number;
    public ActiveMerchantsDiff: number;
    public Terminals: number;
    public ActiveTerminals: number;
    public ActiveTerminalsDiff: number;
    public Transactions: number;
    public TransactionsDiff: number;
    public PurchaseAmount: number;
    public PurchaseAmountDiff: number;
    public CashRewardPurchaseAmount: number;
    public CashRewardPurchaseAmountDiff: number;
    public CardRewardPurchaseAmount: number;
    public CardRewardPurchaseAmountDiff: number;
}

export class OCoreUsage {

    public UserRewardAmount: number;
    public BankAccountNumber: number;
    public Charge: number;
    public TotalAmount: number;
    public MerchantAmount: number;
    public ToAccountId: number;
    public ToAccountMobileNumber: number;
    public AccountDisplayName: string;
    public ToAccountDisplayName: string;
    public Title: string;
    public BankName: string;
    public ReferenceNumber: string;
    public CreatedByDisplayName: string;
    public BankAccountName: string;
    public ModifyByDisplayName: string;
    public SystemComment: string;
    public ProductCategoryName: string;
    public ProductItemName: string;
    public RewardAmount: number;
    public Amount: number;
    public CommisonAmount: number;
    public AccountMobileNumber: number;
    public PaymentReference: string;
    public StatusB: string;
    Coordinates?: any;
    public ReferenceId: number;
    public AccountId: number;
    public AccountKey: string;
    public Reference: string;
    public ReferenceKey: string;
    public AppOwnerKey: string;
    public AppOwnerName: string;
    public AppKey: string;
    public AppName: string;
    public ApiKey: string;
    public ApiName: string;
    public AppVersionKey: string;
    public AppVersionName: string;
    public FeatureKey: string;
    public FeatureName: string;
    public UserAccountKey: string;
    public UserAccountDisplayName: string;
    public UserAccountIconUrl: string;
    public UserAccountTypeCode: string;
    public UserAccountTypeName: string;
    public SessionId: string;
    public SessionKey: string;
    public IpAddress: string;
    public Latitude: string;
    public Longitude: string;
    public Request: string;
    public Response: string;
    public RequestTime: Date;
    public StartDate: Date;
    public ModifyDate: Date;
    public EndDate: Date;
    public CreateDate: Date;
    public ResponseTime: Date;
    public ProcessingTime: string;
    public StatusId: number;
    public StatusCode: string;
    public StatusName: string;
    public MerchantDisplayName: string;
    public LastUseLocationDisplayName: string;
    public CashierDisplayName: string;
    public MerchantIconUrl: string;
    public CashierId: number;
    public TerminalId: number;
    public ProviderDisplayName: string;
    public LastUseLocationSourceName: string;
}



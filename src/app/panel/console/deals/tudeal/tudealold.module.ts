import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AgmOverlays } from 'agm-overlays';
import { ChartsModule } from 'ng2-charts';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { TUDealOldComponent } from './tudealold.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { CountdownModule } from 'ngx-countdown';
import { TimepickerModule } from 'ngx-bootstrap';

const routes: Routes = [
    {
        path: "",
        component: TUDealOldComponent,
        children: [
            { path: "", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../tudeal/tuanalytics/tuanalytics.module#TUAnalyticsModule" },
            { path: "soldhistory/:referencekey/:referenceid/:accountid/:accountkey", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../tudeal/tusoldhistory/tusoldhistory.module#TUSoldHistoryModule" },
            { path: 'dealredeemhistory/:referencekey/:referenceid/:accountid/:accountkey', data: { 'permission': 'dashboard', PageName: 'System.Menu.DealDetails' }, loadChildren: '../tudeal/turedeemhistory/turedeemhistory.module#TURedeemHistoryModule' },
            { path: 'analytics/:referencekey/:referenceid/:accountid/:accountkey', data: { 'permission': 'dashboard', PageName: 'System.Menu.DealDetails' }, loadChildren: '../tudeal/tuanalytics/tuanalytics.module#TUAnalyticsModule' },
            { path: 'notificationhistory/:referencekey/:referenceid/:accountid/:accountkey', data: { 'permission': 'dashboard', PageName: 'System.Menu.DealDetails' }, loadChildren: '../tudeal/tunotifhistory/tunotifhistory.module#TUNotifHistoryModule' }]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUDealOldRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUDealOldRoutingModule,
        AgmOverlays,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        ChartsModule,
        ImageCropperModule,
        CountdownModule,
        TimepickerModule.forRoot()
    ],
    declarations: [TUDealOldComponent]
})
export class TUDealOldModule {

} 

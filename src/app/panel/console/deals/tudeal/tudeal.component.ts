import { ChangeDetectorRef, Component, OnInit, ViewChildren, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { BaseChartDirective } from 'ng2-charts';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { DataHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
declare var moment: any;
declare var $: any;
import * as Feather from "feather-icons";
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { InputFile, InputFileComponent } from 'ngx-input-file';
import * as cloneDeep from 'lodash/cloneDeep';
@Component({
  selector: 'tudeal',
  templateUrl: './tudeal.component.html',
  styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUDealComponent implements OnInit {
  showDailyChart: boolean = true;

  Piechartlable = ['Available Deals', 'Sold Deals', 'Redeemed Deals'];
  public TodayDate: any;
  Piedata = ['0', '0', '0']
  public dougnut: any = {
    type: 'doughnut',
    data: {
      labels: ["Red", "Orange", "Green"],
      datasets: [{
        label: '# of Votes',
        data: [33, 33, 33],
        backgroundColor: [
          'rgba(231, 76, 60, 1)',
          'rgba(255, 164, 46, 1)',
          'rgba(46, 204, 113, 1)'
        ],
        borderColor: [
          'rgba(255, 255, 255 ,1)',
          'rgba(255, 255, 255 ,1)',
          'rgba(255, 255, 255 ,1)'
        ],
        borderWidth: 5
      }]

    },
    options: {
      rotation: 1 * Math.PI,
      circumference: 1 * Math.PI,
      legend: {
        display: false
      },
      tooltip: {
        enabled: false
      },
      cutoutPercentage: 95
    }
  }
  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {

    this.StartDatePicker = {
      singleDatePicker: true,
      timePicker: false,
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      showDropdowns: true,
      startDate: this._HelperService.DateInUTC(moment().startOf("day")),
      endDate: this._HelperService.DateInUTC(moment().endOf("day")),
      minDate: new Date()
    }

    this.EndDatePicker = {
      singleDatePicker: true,
      timePicker: false,
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      showDropdowns: true,
      startDate: this._HelperService.DateInUTC(moment().startOf("day")),
      endDate: this._HelperService.DateInUTC(moment().endOf("day")),
      minDate: new Date()
    }

  }

  VarAccordion: boolean = false;
  VarAccordion1: boolean = false;
  VarAccordionbanner: boolean = false;


  ngOnInit() {
    Feather.replace();
    this.Form_AddUser_Load();
    this._HelperService.ValidateData();
    this.TodayStartTime = this._HelperService.AppConfig.DefaultStartTimeAll;
    this.TodayEndTime = this._HelperService.AppConfig.DefaultEndTimeToday;
    this._HelperService.FullContainer = false;
    this._HelperService.FullContainer = false;

    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveDeal);
    if (StorageDetails != null) {

      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveAccountKey = StorageDetails.AccountKey;
      this._HelperService.AppConfig.ActiveAccountId = StorageDetails.AccountId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;


      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this.pData.ReferenceKey = StorageDetails.ReferenceKey;
      this.pDealPurchaseData.ReferenceKey = StorageDetails.ReferenceKey;

      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this.pData.ReferenceId = StorageDetails.ReferenceId;
      this.pDealPurchaseData.ReferenceId = StorageDetails.ReferenceId;


      this._HelperService.AppConfig.ActiveAccountKey = StorageDetails.AccountKey;
      this.pData.AccountKey = StorageDetails.AccountKey;
      this.pDealPurchaseData.AccountKey = StorageDetails.AccountKey;


      this._HelperService.AppConfig.ActiveAccountId = StorageDetails.AccountId;
      this.pData.AccountId = StorageDetails.AccountId;
      this.pDealPurchaseData.AccountId = StorageDetails.AccountId;


      this._HelperService.ResetDateRange();
      this.GetDetails();
      this.PurchaseHistory_Setup();
      this.GetSalesOverview();
      //this.GetPromoteDeal();
      this.ReviewHistory_Setup();
    } else {
      this._Router.navigate([
        this._HelperService.AppConfig.Pages.System.NotFound
      ]);
    }

    //#region Edit modal 

    $('[data-toggle="tooltip"]').tooltip();

    if (this.InputFileComponent_Term != undefined) {
      this.CurrentImagesCount = 0;
      this._HelperService._InputFileComponent = this.InputFileComponent_Term;
      this.InputFileComponent_Term.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(this.InputFileComponent_Term.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }

    this.Form_EditUser_Load();
    this.GetRoles_List();
    this.GetCategories_List();
    this.GetMerchants_List();
    this.GetStores_List();
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
      this._HelperService.AppConfig.ActiveAccountKey = params["accountkey"];
      this._HelperService.AppConfig.ActiveAccountId = params["accountid"];
    });
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
    //#endregion
  }


  RouteEditDealDetails() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.EditDeal,
      this._HelperService.AppConfig.ActiveReferenceKey,
      this._HelperService.AppConfig.ActiveReferenceId,
      this._HelperService.AppConfig.ActiveAccountId,
      this._HelperService.AppConfig.ActiveAccountKey,
    ]);
  }


  DenyRouteEditDealDetails() {
    this._HelperService.CloseModal('Deal_deny');
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.EditDeal,
      this._HelperService.AppConfig.ActiveReferenceKey,
      this._HelperService.AppConfig.ActiveReferenceId,
      this._HelperService.AppConfig.ActiveAccountId,
      this._HelperService.AppConfig.ActiveAccountKey,
    ]);

  }

  VarAccordion1F() {
    this.VarAccordion1 = !this.VarAccordion1;
  }

  VarAccordionbanner1() {
    this.VarAccordionbanner = !this.VarAccordionbanner;

  }

  VarAccordionF() {
    this.VarAccordion = !this.VarAccordion;
  }


  public _UserAccount: any =
    {
      MerchantDisplayName: null,
      SecondaryEmailAddress: null,
      BankDisplayName: null,
      BankKey: null,
      OwnerName: null,
      SubOwnerAddress: null,
      SubOwnerLatitude: null,
      SubOwnerDisplayName: null,
      SubOwnerKey: null,
      SubOwnerLongitude: null,
      AccessPin: null,
      LastLoginDateS: null,
      AppKey: null,
      AppName: null,
      AppVersionKey: null,
      CreateDate: null,
      CreateDateS: null,
      CreatedByDisplayName: null,
      CreatedByIconUrl: null,
      CreatedByKey: null,
      Description: null,
      IconUrl: null,
      ModifyByDisplayName: null,
      ModifyByIconUrl: null,
      ModifyByKey: null,
      ModifyDate: null,
      ModifyDateS: null,
      PosterUrl: null,
      ReferenceKey: null,
      StatusCode: null,
      StatusI: null,
      StatusId: null,
      StatusName: null,
      AccountCode: null,
      AccountOperationTypeCode: null,
      AccountOperationTypeName: null,
      AccountTypeCode: null,
      AccountTypeName: null,
      Address: null,
      AppVersionName: null,
      ApplicationStatusCode: null,
      ApplicationStatusName: null,
      AverageValue: null,
      CityAreaKey: null,
      CityAreaName: null,
      CityKey: null,
      CityName: null,
      ContactNumber: null,
      CountValue: null,
      CountryKey: null,
      CountryName: null,
      DateOfBirth: null,
      DisplayName: null,
      EmailAddress: null,
      EmailVerificationStatus: null,
      EmailVerificationStatusDate: null,
      FirstName: null,
      GenderCode: null,
      GenderName: null,
      LastLoginDate: null,
      LastName: null,
      Latitude: null,
      Longitude: null,
      MobileNumber: null,
      Name: null,
      NumberVerificationStatus: null,
      NumberVerificationStatusDate: null,
      OwnerDisplayName: null,
      OwnerKey: null,
      Password: null,
      Reference: null,
      ReferralCode: null,
      ReferralUrl: null,
      RegionAreaKey: null,
      RegionAreaName: null,
      RegionKey: null,
      RegionName: null,
      RegistrationSourceCode: null,
      RegistrationSourceName: null,
      RequestKey: null,
      RoleKey: null,
      RoleName: null,
      SecondaryPassword: null,
      SystemPassword: null,
      UserName: null,
      WebsiteUrl: null,

      StateKey: null,
      StateName: null

    }
  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
    //    this._ChangeDetectorRef.detectChanges();
  }
  public _Address: any = {};
  public _ContactPerson: any = {};
  timeRemaining = "--";
  isTimeLessThanDay: boolean = false;
  timerConfig: any = { leftTime: 30 };
  CodeValidityDays = null
  CodeValidityDaysB: any = null;
  getstartdate: any;
  dealcheckbodtype: any;
  valueofradio: any;
  catfees: any;
  IsDetailsLoaded = false;
  GetDetails() {

    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetDeal,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveAccountId,
      AccountKey: this._HelperService.AppConfig.ActiveAccountKey
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.IsDetailsLoaded = true;
          this.toogleIsFormProcessing(false);
          this._UserAccount = this._HelperService.HCXGetDateComponent(_Response.Result);
          // console.log(this._UserAccount);
          // this._UserAccount = _Response.Result;
          this._UserAccount = this._HelperService.HCXGetDateComponent(this._UserAccount);
          this.SelectedDeal.EndDate = moment(this._UserAccount.EndDate);
          this.SelectedDeal.EndDate = this._UserAccount.EndDate;
          this.SelectedDeal.StartDate = moment(this._UserAccount.StartDate);
          this.SelectedDeal.StartDate = this._UserAccount.StartDate;
          this.getstartdate = this._HelperService.GetDateS(this._UserAccount.StartDate);
          this.EndDatevalue = this._HelperService.GetDateS(this._UserAccount.EndDate)
          this._HelperService.CurrentDealDetails = this._UserAccount;
          this._Address = this._UserAccount.Address;
          this._ContactPerson = this._UserAccount.ContactPerson;
          this.dealcheckbodtype = this._UserAccount.TitleTypeId;
          if (this.dealcheckbodtype == '1' || this.dealcheckbodtype == '2' || this.dealcheckbodtype == '3') {
            this.valueofradio = this.dealcheckbodtype
          }
          if (this._UserAccount.CodeValidityDays > 24) {
            this.CodeValidityDaysB = true;
            this.CodeValidityDays = (this._UserAccount.CodeValidityDays / 24)
          }
          else {
            this.CodeValidityDaysB = false;

          }
          let stillUtc = moment.utc(this._UserAccount.EndDate).toDate();
          let local = moment(stillUtc).local().format('YYYY-MM-DD HH:mm:ss');
          var difference = moment(local).diff(moment(), 'hours');
          // var difference = moment(this._UserAccount.StartDate).diff(moment(), 'hours');
          this.IsDealSold = (this._UserAccount.TotalPurchase == 1) ? true : false;

          this.Form_EditUser.controls['AccountKey'].setValue(this._UserAccount.AccountKey);
          this.Form_EditUser.controls['AccountId'].setValue(this._UserAccount.AccountId);
          // this.Form_EditUser.controls['CategoryKey'].setValue(this._UserAccount.CategoryKey);

          this.Form_EditUser.controls['CategoryKey'].setValue(this._UserAccount.SubCategoryKey);
          this.catfees = this._UserAccount.CategoryFee;

          if (difference < 0) {
            this.timeRemaining = "Deal No Longer Available";
            this.isTimeLessThanDay = false;
          } else if (difference >= 0 && difference <= 24) {
            this.isTimeLessThanDay = true;
            this.timerConfig.leftTime= moment(local).diff(moment(), 'seconds');
          } else if (difference > 24 && difference >= 48) {
            this.timeRemaining = Math.round(difference / 24) + " Day Remaining";
            this.isTimeLessThanDay = false;
          } else {
            this.timeRemaining = Math.round(difference / 24) + " Days Remaining";
            this.isTimeLessThanDay = false;
          }

          //#region Set Start and End Time 

          this.ShowDatePickers = false;
          this._ChangeDetectorRef.detectChanges();

          this.StartDatePicker.startDate = moment(this._UserAccount.StartDate);
          this.StartDatePicker.endDate = moment(this._UserAccount.StartDate);
          this.EndDatePicker.startDate = moment(this._UserAccount.EndDate);
          this.EndDatePicker.endDate = moment(this._UserAccount.EndDate);
          setTimeout(() => {
            this.ShowDatePickers = true;
            this._ChangeDetectorRef.detectChanges();
          }, 100);

          //#endregion

          //#region RelocateMarker 

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }


          //#endregion
          //#region DatesAndStatusInit 
          this._UserAccount.CodeValidityStartDate = this._HelperService.GetDateS(this._UserAccount.CodeValidityStartDate);
          this._UserAccount.CodeValidityEndDate = this._HelperService.GetDateS(this._UserAccount.CodeValidityEndDate);
          this._UserAccount.StartDateS = this._HelperService.GetDateS(this._UserAccount.StartDate);
          this._UserAccount.StartDateTimeS = this._HelperService.GetDateTimeS(this._UserAccount.StartDate);
          //
          this._UserAccount.EndDateS = this._HelperService.GetDateS(this._UserAccount.EndDate);
          this._UserAccount.EndTime = this._HelperService.GetTimeS(this._UserAccount.EndDate);
          this._UserAccount.StartTime = this._HelperService.GetTimeS(this._UserAccount.StartDate);
          this._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(this._UserAccount.CreateDate);
          this._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(this._UserAccount.ModifyDate);
          this._UserAccount.StatusI = this._HelperService.GetStatusIcon(this._UserAccount.StatusCode);
          this._UserAccount.StatusB = this._HelperService.GetStatusBadge(this._UserAccount.StatusCode);
          this._UserAccount.StatusC = this._HelperService.GetStatusColor(this._UserAccount.StatusCode);
          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  EditDealDetails() {
    if (this._UserAccount.StatusCode == 'deal.expired') {
      this._HelperService.NotifyError("Operation failed. You cannot edit expired deals")
    }
    else {
      this._Router.navigate([
        this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.EditDeal,
        this._HelperService.AppConfig.ActiveReferenceKey,
        this._HelperService.AppConfig.ActiveReferenceId,
        this._HelperService.AppConfig.ActiveAccountId,
        this._HelperService.AppConfig.ActiveAccountKey,
      ]);
    }
  }

  GetPromoteDeal() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetDealPromotion,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      // AccountId: this._HelperService.AppConfig.ActiveAccountId,
      //AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
      // Reference: this._HelperService.GetSearchConditionStrict(
      //   "",
      //   "ReferenceKey",
      //   this._HelperService.AppConfig.DataType.Text,
      //   this._HelperService.AppConfig.ActiveReferenceKey,
      //   "="
      // ),
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.toogleIsFormProcessing(false);
          this._UserAccount = _Response.Result;
          this.SelectedDeal.EndDate = moment(this._UserAccount.EndDate);
          this.SelectedDeal.StartDate = moment(this._UserAccount.StartDate);
          this.getstartdate = this._HelperService.GetDateS(this._UserAccount.StartDate);
          this.EndDatevalue = this._HelperService.GetDateS(this._UserAccount.EndDate)
          this._HelperService.CurrentDealDetails = this._UserAccount;
          this._Address = this._UserAccount.Address;
          this._ContactPerson = this._UserAccount.ContactPerson;
          this.dealcheckbodtype = this._UserAccount.TitleTypeId;
          if (this.dealcheckbodtype == '1' || this.dealcheckbodtype == '2' || this.dealcheckbodtype == '3') {
            this.valueofradio = this.dealcheckbodtype
          }
          if (this._UserAccount.CodeValidityDays > 24) {
            this.CodeValidityDaysB = true;
            this.CodeValidityDays = (this._UserAccount.CodeValidityDays / 24)
          }
          else {
            this.CodeValidityDaysB = false;

          }
          var difference = moment(this._UserAccount.EndDate).diff(moment(), 'hours');

          this.IsDealSold = (this._UserAccount.TotalPurchase == 1) ? true : false;

          this.Form_EditUser.controls['AccountKey'].setValue(this._UserAccount.AccountKey);
          this.Form_EditUser.controls['AccountId'].setValue(this._UserAccount.AccountId);
          this.Form_EditUser.controls['CategoryKey'].setValue(this._UserAccount.CategoryKey);

          if (difference < 0) {
            this.timeRemaining = "Deal No Longer Available";
            this.isTimeLessThanDay = false;
          } else if (difference >= 0 && difference <= 24) {
            this.isTimeLessThanDay = true;
            this.timerConfig.leftTime = (difference * 60 * 60);
          } else if (difference > 24 && difference >= 48) {
            this.timeRemaining = Math.round(difference / 24) + " Day Remaining";
            this.isTimeLessThanDay = false;
          } else {
            this.timeRemaining = Math.round(difference / 24) + " Days Remaining";
            this.isTimeLessThanDay = false;
          }

          //#region Set Start and End Time 

          this.ShowDatePickers = false;
          this._ChangeDetectorRef.detectChanges();

          this.StartDatePicker.startDate = moment(this._UserAccount.StartDate);
          this.StartDatePicker.endDate = moment(this._UserAccount.StartDate);

          this.EndDatePicker.startDate = moment(this._UserAccount.EndDate);
          this.EndDatePicker.endDate = moment(this._UserAccount.EndDate);

          setTimeout(() => {
            this.ShowDatePickers = true;
            this._ChangeDetectorRef.detectChanges();
          }, 100);

          //#endregion

          //#region RelocateMarker 

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          // this.FormC_EditUser_Latitude = this._UserAccount.Latitude;
          // this.FormC_EditUser_Longitude = this._UserAccount.Longitude;

          // this._HelperService._ReLocate();

          //#endregion

          //#region DatesAndStatusInit 
          this._UserAccount.CodeValidityStartDate = this._HelperService.GetDateS(
            this._UserAccount.CodeValidityStartDate
          );
          this._UserAccount.CodeValidityEndDate = this._HelperService.GetDateS(
            this._UserAccount.CodeValidityEndDate
          );
          this._UserAccount.StartDateS = this._HelperService.GetDateS(
            this._UserAccount.StartDate
          );
          this._UserAccount.EndDateS = this._HelperService.GetDateS(
            this._UserAccount.EndDate
          );
          this._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(
            this._UserAccount.CreateDate
          );
          this._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(
            this._UserAccount.ModifyDate
          );
          this._UserAccount.StatusI = this._HelperService.GetStatusIcon(
            this._UserAccount.StatusCode
          );
          this._UserAccount.StatusB = this._HelperService.GetStatusBadge(
            this._UserAccount.StatusCode
          );
          this._UserAccount.StatusC = this._HelperService.GetStatusColor(
            this._UserAccount.StatusCode
          );


          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  public PurchaseHistory_Config: OList;
  PurchaseHistory_Setup() {
    this.PurchaseHistory_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetPurchaseHistory,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals,
      Title: "Purchase History",
      StatusType: "default",
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveAccountId,
      AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      DefaultSortExpression: "CreateDate desc",
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'dealcode.unused', '=='),
      TableFields: [
        {
          DisplayName: " Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Contact No",
          SystemName: "ContactNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Terminals",
          SystemName: "Terminals",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Cashiers",
          SystemName: "Cashiers",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",

        }, {
          DisplayName: "Manager",
          SystemName: "ManagerName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.PurchaseHistory_Config = this._DataHelperService.List_Initialize(
      this.PurchaseHistory_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Stores,
      this.PurchaseHistory_Config
    );

    this.PurchaseHistory_GetData();
  }
  PurchaseHistory_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.PurchaseHistory_Config.Sort.SortOptions.length; index++) {
        const element = this.PurchaseHistory_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.PurchaseHistory_Config
    );

    this.PurchaseHistory_Config = this._DataHelperService.List_Operations(
      this.PurchaseHistory_Config,
      event,
      Type
    );

    if (
      (this.PurchaseHistory_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.PurchaseHistory_GetData();
    }

  }
  timeout = null;
  PurchaseHistory_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.PurchaseHistory_Config.Sort.SortOptions.length; index++) {
          const element = this.PurchaseHistory_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.PurchaseHistory_Config
      );

      this.PurchaseHistory_Config = this._DataHelperService.List_Operations(
        this.PurchaseHistory_Config,
        event,
        Type
      );

      if (
        (this.PurchaseHistory_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.PurchaseHistory_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);
  }
  PurchaseHistory_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.PurchaseHistory_Config
    );
    this.PurchaseHistory_Config = TConfig;
  }
  PurchaseHistory_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveDeal,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
    this._HelperService.AppConfig.ActiveAccountKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveAccountId = ReferenceData.ReferenceId;
    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Store
    //     .Dashboard,
    //   ReferenceData.ReferenceKey,
    //   ReferenceData.ReferenceId,
    // ]);

    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deal,
    //   ReferenceData.ReferenceKey,
    //   ReferenceData.ReferenceId,
    //   ReferenceData.AccountId,
    //   ReferenceData.AccountKey,

    // ]);


  }


  //#region Redeem History 

  public RedeemHistory_Config: OList;
  RedeemHistory_Setup() {
    this.RedeemHistory_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetPurchaseHistory,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals,
      Title: "Redeem History",
      StatusType: "default",
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveAccountId,
      AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      DefaultSortExpression: "CreateDate desc",
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'dealcode.used', '=='),
      TableFields: [
        {
          DisplayName: " Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Contact No",
          SystemName: "ContactNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Terminals",
          SystemName: "Terminals",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Cashiers",
          SystemName: "Cashiers",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
          //   .PanelAcquirer.Merchant.Dashboard,
        }, {
          DisplayName: "Manager",
          SystemName: "ManagerName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.RedeemHistory_Config = this._DataHelperService.List_Initialize(
      this.RedeemHistory_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Stores,
      this.RedeemHistory_Config
    );

    this.RedeemHistory_GetData();
  }
  RedeemHistory_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.RedeemHistory_Config.Sort.SortOptions.length; index++) {
        const element = this.RedeemHistory_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.RedeemHistory_Config
    );

    this.RedeemHistory_Config = this._DataHelperService.List_Operations(
      this.RedeemHistory_Config,
      event,
      Type
    );

    if (
      (this.RedeemHistory_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.RedeemHistory_GetData();
    }

  }

  RedeemHistory_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.RedeemHistory_Config.Sort.SortOptions.length; index++) {
          const element = this.RedeemHistory_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.RedeemHistory_Config
      );

      this.RedeemHistory_Config = this._DataHelperService.List_Operations(
        this.RedeemHistory_Config,
        event,
        Type
      );

      if (
        (this.RedeemHistory_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.RedeemHistory_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);
  }
  RedeemHistory_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.RedeemHistory_Config
    );
    this.RedeemHistory_Config = TConfig;
  }
  RedeemHistory_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveDeal,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
    this._HelperService.AppConfig.ActiveAccountKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveAccountId = ReferenceData.ReferenceId;
    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Store
    //     .Dashboard,
    //   ReferenceData.ReferenceKey,
    //   ReferenceData.ReferenceId,
    // ]);

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deal,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
      ReferenceData.AccountId,
      ReferenceData.AccountKey,

    ]);


  }

  //#endregion

  public _GetoverviewSummary: any = {};
  public TodayStartTime = null;
  public TodayEndTime = null;
  private pData = {
    Task: 'getdealsoverview',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
    ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
    AccountId: this._HelperService.AppConfig.ActiveAccountId,
    AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetSalesOverview() {


    this.showDailyChart = false;
    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetoverviewSummary = _Response.Result as any;
          // this.Piedata[0] = this._GetoverviewSummary.Total;
          this.showDailyChart = true;
          this._ChangeDetectorRef.detectChanges();
          this.GetDealPurchaseOverview();

          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  public _GetPurchaseoverviewSummary: any = {};
  TotalCountDeal: any = null;

  private pDealPurchaseData = {
    Task: 'getdealspurchaseoverview',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
    ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
    AccountId: this._HelperService.AppConfig.ActiveAccountId,
    AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetDealPurchaseOverview() {
    this.showDailyChart = false;
    this._ChangeDetectorRef.detectChanges();

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.pDealPurchaseData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetPurchaseoverviewSummary = _Response.Result as any;
          this.Piedata[1] = this._GetPurchaseoverviewSummary.Total;
          this.Piedata[2] = this._GetPurchaseoverviewSummary.Used;
          this.TotalCountDeal = this._GetPurchaseoverviewSummary.MaxLimit - this._GetPurchaseoverviewSummary.Total
          this.Piedata[0] = this.TotalCountDeal;
          this.showDailyChart = true;
          this._ChangeDetectorRef.detectChanges();

          return;

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  public ReviewHistory_Config: OList;
  ReviewHistory_Setup() {
    this.ReviewHistory_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.getdealreviews,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals,
      Title: "Purchase History",
      StatusType: "default",
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveAccountId,
      AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      DefaultSortExpression: "CreateDate desc",
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: " Name",
          SystemName: "AccountDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Contact No",
          SystemName: "AccountMobileNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Rating",
          SystemName: "Rating",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Review",
          SystemName: "Review",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "IsWorking",
          SystemName: "IsWorking",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
          //   .PanelAcquirer.Merchant.Dashboard,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.ReviewHistory_Config = this._DataHelperService.List_Initialize(
      this.ReviewHistory_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Stores,
      this.ReviewHistory_Config
    );

    //this.ReviewHistory_GetData();
  }
  ReviewHistory_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.ReviewHistory_Config.Sort.SortOptions.length; index++) {
        const element = this.ReviewHistory_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.ReviewHistory_Config
    );

    this.ReviewHistory_Config = this._DataHelperService.List_Operations(
      this.ReviewHistory_Config,
      event,
      Type
    );

    if (
      (this.ReviewHistory_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      //this.ReviewHistory_GetData();
    }

  }
  ReviewHistory_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.ReviewHistory_Config.Sort.SortOptions.length; index++) {
          const element = this.ReviewHistory_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.ReviewHistory_Config
      );

      this.ReviewHistory_Config = this._DataHelperService.List_Operations(
        this.ReviewHistory_Config,
        event,
        Type
      );

      if (
        (this.ReviewHistory_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        //this.ReviewHistory_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);
  }
  ReviewHistory_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.ReviewHistory_Config
    );
    this.ReviewHistory_Config = TConfig;
  }
  // ReviewHistory_RowSelected(ReferenceData) {
  //    this._HelperService.SaveStorage(
  //      this._HelperService.AppConfig.Storage.ActiveDeal,
  //      {
  //        ReferenceKey: ReferenceData.ReferenceKey,
  //        ReferenceId: ReferenceData.ReferenceId,
  //        DisplayName: ReferenceData.DisplayName,
  //        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
  //      }
  //    );

  //    this._HelperService.AppConfig.ActiveReferenceKey =
  //      ReferenceData.ReferenceKey;
  //    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
  //    this._HelperService.AppConfig.ActiveAccountKey =
  //      ReferenceData.ReferenceKey;
  //    this._HelperService.AppConfig.ActiveAccountId = ReferenceData.ReferenceId;


  //    this._Router.navigate([
  //      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deal,
  //      ReferenceData.ReferenceKey,
  //      ReferenceData.ReferenceId,
  //      ReferenceData.AccountId,
  //      ReferenceData.AccountKey,

  //    ]);


  //  }



  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    //this.ReviewHistory_GetData();

    if (ButtonType == 'Sort') {
      $("#DealsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#DealsList_fdropdown").dropdown('toggle');
    }
    this._HelperService.StopClickPropogation();
    // this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }


  tabSwitched(arg1: any): void { }
  ResetFilters(arg1: any, arg2: any): void { }
  OpenEditColModal(): void { }


  UpdateStatusArray: any = ['deal.draft', 'deal.approvalpending', 'deal.approved', 'deal.published', 'deal.paused', 'deal.expired',];
  StatusUpdate(ReferenceData, i) {
    if (i == 4) {
      swal({
        title: "Pause Deal?",
        text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
        showCancelButton: true,
        position: this._HelperService.AppConfig.Alert_Position,
        animation: this._HelperService.AppConfig.Alert_AllowAnimation,
        customClass: this._HelperService.AppConfig.Alert_Animation,
        allowOutsideClick: true,
        allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
        confirmButtonColor: this._HelperService.AppConfig.Color_Red,
        cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
        confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
        cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        input: 'text',
        inputClass: 'swalText',
        inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
        inputAttributes: {
          autocapitalize: 'off',
          autocorrect: 'off',
          // maxLength: "4",
          // minLength: "4"
        },

        inputValidator: function (value) {
          if (value === '' || value.length < 4) {
            return 'Enter your 4 digit pin!'
          }
        },

      }).then((result) => {
        if (result.value) {
          //   if (result.value.length < 4) {
          //     this._HelperService.NotifyError('Pin length must be 4 digits');
          //     return;
          // }

          this._HelperService.IsFormProcessing = true;
          var P1Data =
          {
            Task: this._HelperService.AppConfig.Api.ThankUCash.updatedealstatus,
            ReferenceId: ReferenceData.ReferenceId,
            ReferenceKey: ReferenceData.ReferenceKey,
            AccountId: ReferenceData.AccountId,
            AccountKey: ReferenceData.AccountKey,
            StatusCode: this.UpdateStatusArray[i],
            AuthPin: result.value,
          }

          let _OResponse: Observable<OResponse>;
          _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, P1Data);
          _OResponse.subscribe(
            _Response => {
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.NotifySuccess("Status Updated successfully. It will take upto 5 minutes to update changes.");
                this.GetDetails();
                this._HelperService.IsFormProcessing = false;
              }
              else {
                this._HelperService.NotifyError(_Response.Message);
              }
            }
            ,
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
              this._HelperService.ToggleField = false;
            });

        }
      });
    }
    else {
      swal({
        title: "Resume Deal?",
        text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
        showCancelButton: true,
        position: this._HelperService.AppConfig.Alert_Position,
        animation: this._HelperService.AppConfig.Alert_AllowAnimation,
        customClass: this._HelperService.AppConfig.Alert_Animation,
        allowOutsideClick: true,
        allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
        confirmButtonColor: this._HelperService.AppConfig.Color_Red,
        cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
        confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
        cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        input: 'password',
        inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
        inputAttributes: {
          autocapitalize: 'off',
          autocorrect: 'off',
          // maxLength: "4",
          // minLength: "4"
        },

        inputValidator: function (value) {
          if (value === '' || value.length < 4) {
            return 'Enter your 4 digit pin!'
          }
        },

      }).then((result) => {
        if (result.value) {
          //   if (result.value.length < 4) {
          //     this._HelperService.NotifyError('Pin length must be 4 digits');
          //     return;
          // }

          this._HelperService.IsFormProcessing = true;
          var P1Data =
          {
            Task: this._HelperService.AppConfig.Api.ThankUCash.updatedealstatus,
            ReferenceId: ReferenceData.ReferenceId,
            ReferenceKey: ReferenceData.ReferenceKey,
            AccountId: ReferenceData.AccountId,
            AccountKey: ReferenceData.AccountKey,
            StatusCode: this.UpdateStatusArray[i],
            AuthPin: result.value,
          }

          let _OResponse: Observable<OResponse>;
          _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, P1Data);
          _OResponse.subscribe(
            _Response => {
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.NotifySuccess("Status Updated successfully. It will take upto 5 minutes to update changes.");
                this.GetDetails();
                this._HelperService.IsFormProcessing = false;
              }
              else {
                this._HelperService.NotifyError(_Response.Message);
              }
            }
            ,
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
              this._HelperService.ToggleField = false;
            });

        }
      });
    }



  }



  DealTimings: any = {
    Start: new Date(),
    End: new Date()
  }

  public _UserAccountNew: any =
    {
      MerchantDisplayName: null,
      SecondaryEmailAddress: null,
      BankDisplayName: null,
      BankKey: null,
      OwnerName: null,
      SubOwnerAddress: null,
      SubOwnerLatitude: null,
      SubOwnerDisplayName: null,
      SubOwnerKey: null,
      SubOwnerLongitude: null,
      AccessPin: null,
      LastLoginDateS: null,
      AppKey: null,
      AppName: null,
      AppVersionKey: null,
      CreateDate: null,
      CreateDateS: null,
      CreatedByDisplayName: null,
      CreatedByIconUrl: null,
      CreatedByKey: null,
      Description: null,
      IconUrl: null,
      ModifyByDisplayName: null,
      ModifyByIconUrl: null,
      ModifyByKey: null,
      ModifyDate: null,
      ModifyDateS: null,
      PosterUrl: null,
      ReferenceKey: null,
      StatusCode: null,
      StatusI: null,
      StatusId: null,
      StatusName: null,
      AccountCode: null,
      AccountOperationTypeCode: null,
      AccountOperationTypeName: null,
      AccountTypeCode: null,
      AccountTypeName: null,
      Address: null,
      AppVersionName: null,
      ApplicationStatusCode: null,
      ApplicationStatusName: null,
      AverageValue: null,
      CityAreaKey: null,
      CityAreaName: null,
      CityKey: null,
      CityName: null,
      ContactNumber: null,
      CountValue: null,
      CountryKey: null,
      CountryName: null,
      DateOfBirth: null,
      DisplayName: null,
      EmailAddress: null,
      EmailVerificationStatus: null,
      EmailVerificationStatusDate: null,
      FirstName: null,
      GenderCode: null,
      GenderName: null,
      LastLoginDate: null,
      LastName: null,
      Latitude: null,
      Longitude: null,
      MobileNumber: null,
      Name: null,
      NumberVerificationStatus: null,
      NumberVerificationStatusDate: null,
      OwnerDisplayName: null,
      OwnerKey: null,
      Password: null,
      Reference: null,
      ReferralCode: null,
      ReferralUrl: null,
      RegionAreaKey: null,
      RegionAreaName: null,
      RegionKey: null,
      RegionName: null,
      RegistrationSourceCode: null,
      RegistrationSourceName: null,
      RequestKey: null,
      RoleKey: null,
      RoleName: null,
      SecondaryPassword: null,
      SystemPassword: null,
      UserName: null,
      WebsiteUrl: null,

      StateKey: null,
      StateName: null

    }
  GetDetailsNew(ReferenceData: any, modal: string) {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetDeal,
      ReferenceId: ReferenceData.ReferenceId,
      ReferenceKey: ReferenceData.ReferenceKey,
      AccountId: ReferenceData.AccountId,
      AccountKey: ReferenceData.AccountKey
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._UserAccountNew = _Response.Result;

          this.CouponCount = this._UserAccount.MaximumUnitSale;

          this.SelectedDeal = {};

          this.SelectedDeal.ReferenceKey = this._UserAccount.ReferenceKey;
          this.SelectedDeal.ReferenceId = this._UserAccount.ReferenceId;

          this.SelectedDeal.EndDate = moment(this._UserAccount.EndDate);
          this.SelectedDeal.StartDate = moment(this._UserAccount.StartDate);
          this.SelectedDealStartDateS = this._HelperService.GetDateS(this._UserAccount.StartDate);
          this.SelectedDealEndDateS = this._HelperService.GetDateS(this._UserAccount.EndDate);
          this.SelectedDeal.Schedule = [];

          //#region Remove Redeem Schedules 

          if (this._UserAccount.Schedule) {
            for (let index = 0; index < this._UserAccount.Schedule.length; index++) {
              const element = this._UserAccount.Schedule[index];
              if (element.Type != 'dealshedule') {
                this.SelectedDeal.Schedule.push(element);
              }
            }
          }
          //#endregion

          if (modal == 'schedule') {
            this._HelperService.OpenModal('EditSchedule');
          } else {
            this._HelperService.OpenModal('AddCoupons');
          }

        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#region Run Time 

  IsRunAllTime: boolean = true;
  RunAllTimeToogle(): void {
    this.IsRunAllTime = !(this.IsRunAllTime);
    this.SelectedDeal.Schedule = [];
  }

  //#endregion

  //#region Schedule Update 

  SelectedDeal: any = {};
  SelectedDealStartDateS: any = "";
  SelectedDealEndDateS: any = "";

  ShowEditSchedule(ReferenceData: any): void {

  }
  EndDatevalue: any = null;
  enddate: any;
  ScheduleEndDateRangeChange(value) {
    this.SelectedDeal.EndDate = value.end;
    this.enddate = this.SelectedDeal
    // this.EndDatevalue = value.end
  }

  ReFormat_RequestBody_Old(): void {
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'extenddeal',
      'ReferenceKey': this.SelectedDeal.ReferenceKey,
      'ReferenceId': this.SelectedDeal.ReferenceId,
      "TypeCode": "deal",
      "StartDate": this.SelectedDeal.StartDate,
      "EndDate": this.SelectedDeal.EndDate,
      "Schedule": this.SelectedDeal.Schedule,
      "TitleTypeId": this._UserAccount.TitleTypeId,
      "UsageTypeCode": this._UserAccount.UsageTypeCode
    };
    //#region Set Schedule 

    for (let index = 0; index < 7; index++) {

      if (this.IsRunAllTime) {
        formRequest.Schedule.push({
          DayOfWeek: index,
          StartHour: '00:00',
          EndHour: '23:59',
          Type: 'dealshedule'
        });
      } else {
        formRequest.Schedule.push({
          DayOfWeek: index,
          StartHour: moment(this.DealTimings.Start).format('hh:mm'),
          EndHour: moment(this.DealTimings.End).format('hh:mm'),
          Type: 'dealshedule'
        });

      }
    }

    //#endregion

    return formRequest;

  }

  UpdateSchedule(): void {

    var Req = this.ReFormat_RequestBody_Old();
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, Req);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess('Deal Schedule updated successfully. It will take upto 5 minutes to update changes.');

          this.ClearScheduleUpdate();
          this._HelperService.CloseModal('EditSchedule');
          this.GetDetails();
        }
        else {
          this.SelectedDeal.Schedule = [];
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.SelectedDeal.Schedule = [];
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  ClearScheduleUpdate(): void {
    this.SelectedDeal = {};
    this.SelectedDealStartDateS = "";
    this.SelectedDealEndDateS = "";
  }

  //#endregion

  //#region Stock Update 

  CouponCount: number = null;

  ShowEditStock(ReferenceData: any): void {

    this.SelectedDeal.ReferenceKey = ReferenceData.ReferenceKey;
    this.SelectedDeal.ReferenceId = ReferenceData.ReferenceId;

    this._HelperService.OpenModal('AddCoupons');
  }

  EditStock(): void {

    var formRequest: any = {
      'OperationType': 'new',
      //'Task': 'updatedeal',
      'Task': 'updatedealcoupons',
      'ReferenceKey': this.SelectedDeal.ReferenceKey,
      'ReferenceId': this.SelectedDeal.ReferenceId,
      "TypeCode": "deal",

      "MaximumUnitSale": this.CouponCount
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, formRequest);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess('Deal Coupon Count updated successfully. It will take upto 5 minutes to update changes.');

          this.CouponCount = null;
          this._HelperService.CloseModal('AddCoupons');
          this.GetDetails();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  ClearCouponUpdate(): void {
    this.CouponCount = null;
  }

  //#endregion

  DuplicateDeal(): void {
    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.DuplicateDeal,
    //   this._UserAccount.ReferenceKey,
    //   this._UserAccount.ReferenceId,
    //   this._UserAccount.AccountId,
    //   this._UserAccount.AccountKey,
    // ]);
    swal({
      title: "Duplicate Deal ?",
      text: "Click on continue button to duplicate deal",
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;

        var P1Data =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.DuplicateDeal,
          ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
          ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
          AccountId: this._HelperService.AppConfig.ActiveAccountId,
          AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, P1Data);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess(_Response.Message);
              this._Router.navigate([
                this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.EditDeal,
                _Response.Result.ReferenceKey,
                _Response.Result.ReferenceId,
                this._HelperService.AppConfig.ActiveAccountId,
                this._HelperService.AppConfig.ActiveAccountKey,
              ]);
              // this._HelperService.NotifySuccess("Notification Sent successfully");
              // this._HelperService.NotifEventEmit.emit(0);
              // this._HelperService.IsFormProcessing = false;
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });
      }
    });
  }

  ShowAllLocations(): void {
    this._HelperService.OpenModal("AllLocation");
  }


  SendNotification() {
    swal({
      //title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
      title: "Send Notification?",
      //text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      text: "Enter your 4 digit pin to send notification",
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      input: 'text',
      inputClass: 'swalText',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
      inputValidator: function (value) {
        if (value === '' || value.length < 4) {
          return 'Enter your 4 digit pin!'
        }
      },

    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;
        var P1Data =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.SendDealNotification,
          ReferenceId: this._UserAccount.ReferenceId,
          ReferenceKey: this._UserAccount.ReferenceKey,
          AuthPin: result.value
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, P1Data);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Notification Sent successfully");
              this._HelperService.NotifEventEmit.emit(0);
              this._HelperService.IsFormProcessing = false;
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });
      }
    });

  }

  //#region Edit Modal 

  public SaveAccountRequest: any;
  public value: any;
  public IsDealSold: boolean = false;

  ShowDatePickers: boolean = true;

  public showPickers: boolean = true;
  public merchantPlaceholder: string = "Select Merchant";
  public categoryPlaceholder: string = "Select Category";

  StartDatePicker: any;

  EndDatePicker: any;

  public NiraOrPercent: any = {
    DiscountAmount: "1",
    CommissionAmount: "1"
  }

  // DealTimings: any = {
  //   Start: new Date(),
  //   End: new Date()
  // }

  ValidTimings: any = {
    Start: new Date(),
    End: new Date()
  }

  Expiry: any = {
    Hours: 0,
    Days: 0,
    Date: new Date()
  }

  public WeekDays: any[] = [
    { name: 'Monday', selected: true },
    { name: 'Tuesday', selected: true },
    { name: 'Wednesday', selected: true },
    { name: 'Thursday', selected: true },
    { name: 'Friday', selected: true },
    { name: 'Saturday', selected: true },
    { name: 'Sunday', selected: true }
  ]

  @ViewChild(InputFileComponent)
  private InputFileComponent_Term: InputFileComponent;

  CurrentImagesCount: number = 0;


  public GetRoles_Option: Select2Options;
  public GetRoles_Transport: any;
  GetRoles_List() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetCoreParameters,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: '',
      SortCondition: ['Name asc'],
      Fields: [
        {
          SystemName: 'ReferenceKey',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: 'Name',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        {
          SystemName: 'StatusCode',
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: '=',
          SearchValue: this._HelperService.AppConfig.Status.Active,
        }
      ],
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
    this.GetRoles_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetRoles_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetRoles_Transport,
      multiple: false,
    };
  }
  GetRoles_ListChange(event: any) {
    this.Form_EditUser.patchValue(
      {
        RoleKey: event.value
      }
    );
  }

  public GetMerchants_Option: Select2Options;
  public GetMerchants_Transport: any;
  public SelectedMerchant: any = {};
  GetMerchants_List() {
    var PlaceHolder = this.merchantPlaceholder;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },

        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: 'StatusCode',
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: '=',
          SearchValue: this._HelperService.AppConfig.Status.Active,
        }
      ]
    }

    this.GetMerchants_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetMerchants_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetMerchants_Transport,
      multiple: false,
    };
  }
  GetMerchants_ListChange(event: any) {

    this.SelectedMerchant = event.data[0];
    this.Form_EditUser.patchValue(
      {
        AccountKey: event.data[0].ReferenceKey,
        AccountId: event.data[0].ReferenceId
      }
    );



  }

  public GetCategories_Option: Select2Options;
  public GetCategories_Transport: any;
  GetCategories_List() {
    var PlaceHolder = this.categoryPlaceholder;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetCoreCommons,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },

        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: 'StatusCode',
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: '=',
        //     SearchValue: this._HelperService.AppConfig.Status.Active,
        // }
      ]
    }


    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'TypeCode', this._HelperService.AppConfig.DataType.Text,
      [
        this._HelperService.AppConfig.HelperTypes.MerchantCategories,

      ]
      , '=');
    this.GetCategories_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetCategories_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetCategories_Transport,
      multiple: false,
    };


  }
  GetCategories_ListChange(event: any) {
    this.Form_EditUser.patchValue(
      {
        CategoryKey: event.data[0].ReferenceKey

      }
    );
  }

  public GetStores_Option: Select2Options;
  public GetStores_Transport: any;
  public StoresList: any = []
  GetStores_List() {
    var PlaceHolder = "Select Stores";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },

        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: 'StatusCode',
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: '=',
        //     SearchValue: this._HelperService.AppConfig.Status.Active,
        // }
      ]
    }


    this.GetStores_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetStores_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetStores_Transport,
      multiple: true,
    };
  }
  GetStores_ListChange(event: any) {
    this.StoresList = event.data
  }

  ScheduleStartDateRangeChange_Prev(value) {

    this.IsStartDateAfterEnd = moment(this.Form_EditUser.controls['StartDate'].value).isBefore(moment(value.start));

    // this.Form_EditUser.patchValue(
    //     {
    //         StartDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
    //     }
    // );
    this.Form_EditUser.patchValue(
      {
        StartDate: value.end,
      }
    );
  }


  ScheduleEndDateRangeChange_Prev(value) {

    this.IsStartDateAfterEnd = moment(this.Form_EditUser.controls['EndDate'].value).isBefore(moment(value.start));

    this.Form_EditUser.patchValue(
      {
        EndDate: value.end,
      }
    );
  }

  ScheduleEndDateRangeChange_Modal(value) {

    this.IsStartDateAfterEnd = moment(moment(value.start)).isBefore(this.Form_EditUser.controls['StartDate'].value);

    // this.Form_EditUser.patchValue(
    //     {
    //         EndDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
    //     }
    // );
    this.Form_EditUser.patchValue(
      {
        EndDate: value.end,
      }
    );

  }

  _CurrentAddress: any = {};
  IsStartDateAfterEnd: boolean = false;
  Form_EditUser: FormGroup;
  Form_EditUser_Address: string = null;
  Form_EditUser_Latitude: number = 0;
  Form_EditUser_Longitude: number = 0;

  @ViewChild('places') places: GooglePlaceDirective;
  Form_EditUser_PlaceMarkerClick(event) {
    this.Form_EditUser_Latitude = event.coords.lat;
    this.Form_EditUser_Longitude = event.coords.lng;
  }
  public Form_EditUser_AddressChange(address: Address) {
    this.Form_EditUser_Latitude = address.geometry.location.lat();
    this.Form_EditUser_Longitude = address.geometry.location.lng();
    this.Form_EditUser_Address = address.formatted_address;
    this.Form_EditUser.controls['Latitude'].setValue(this.Form_EditUser_Latitude);
    this.Form_EditUser.controls['Longitude'].setValue(this.Form_EditUser_Longitude);
    this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
    this.Form_EditUser.controls['Address'].setValue(address.name + ',' + address.formatted_address);
  }
  Form_EditUser_Show() {
  }
  Form_EditUser_Close() {
    this._HelperService.CloseModal('Account_Edit_Content');
  }

  Form_EditUser_Load() {
    this._HelperService._Icon_Cropper_Data.Width = 128;
    this._HelperService._Icon_Cropper_Data.Height = 128;
    this.Form_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      Task: 'updatedeal',
      TypeCode: 'deal',
      CategoryKey: null,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: [null, Validators.required],
      AccountKey: [null, Validators.required],

      Title: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      Description: null,
      Terms: [null, Validators.required],
      CommissionAmount: [null, Validators.compose([Validators.required])],
      DiscountAmount: [null, Validators.compose([Validators.required, Validators.min(1)])],
      MaximumUnitSalePerDay: [null, Validators.compose([Validators.required, Validators.min(1)])],
      MaximumUnitSale: [null, Validators.compose([Validators.required, Validators.min(1)])],
      MaximumUnitSalePerPerson: [null, Validators.compose([Validators.required, Validators.min(1)])],
      IconContent: this._HelperService._Icon_Cropper_Data,
      //SellingPrice: [null, Validators.required],
      Amount: [null, Validators.required],
      ActualPrice: [null, Validators.compose([Validators.required, Validators.min(1)])],
      EndDate: null,
      StartDate: null,
      TitleContent: null,
      DealTitle: ["0", Validators.required], //transient
      CouponValidity: null, //transient
      SettlementTypeCode: ['postpaid']
    });
  }
  Form_EditUser_Clear() {
    this._HelperService.ToggleField = true;
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 300);
    this.Form_EditUser_Latitude = 0;
    this.Form_EditUser_Longitude = 0;
    this.Form_EditUser.reset();
    this._HelperService.Icon_Crop_Clear();
    this.Form_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_EditUser_Process(_FormValue: any, IsDraft: boolean) {

    if (this._UserAccount.StatusCode == 'deal.rejected') {
      this._HelperService.IsFormProcessing = true;
      _FormValue.Latitude = this.Form_EditUser_Latitude;
      _FormValue.Longitude = this.Form_EditUser_Longitude;
      this.SaveAccountRequest = this.ReFormat_RequestBody();

      if (IsDraft) {
        this.SaveAccountRequest.StatusCode = 'deal.draft';
      } else {
        this.SaveAccountRequest.StatusCode = 'deal.approvalpending';
      }

      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.SaveAccountRequest);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess('Deal updated successfully. It will take upto 5 minutes to update changes.');
            this.GetDetails()
            this._HelperService.CloseModal('Account_Edit_Content_approve');

            this.InputFileComponent_Term.files.pop();
            this.Form_EditUser_Clear();
            if (_FormValue.OperationType == 'edit') {
            }
            else if (_FormValue.OperationType == 'close') {
              this.Form_EditUser_Close();
            }
            this.Form_EditUser_Close();

          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });

    }
    else {
      this._HelperService.IsFormProcessing = true;
      _FormValue.Latitude = this.Form_EditUser_Latitude;
      _FormValue.Longitude = this.Form_EditUser_Longitude;
      this.SaveAccountRequest = this.ReFormat_RequestBody();

      if (IsDraft) {
        this.SaveAccountRequest.StatusCode = 'deal.draft';
      } else {
        this.SaveAccountRequest.StatusCode = 'deal.published';
      }

      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.SaveAccountRequest);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess('Deal updated successfully. It will take upto 5 minutes to update changes.');
            this.GetDetails()
            this._HelperService.CloseModal('Account_Edit_Content');

            this.InputFileComponent_Term.files.pop();
            this.Form_EditUser_Clear();
            if (_FormValue.OperationType == 'edit') {
            }
            else if (_FormValue.OperationType == 'close') {
              this.Form_EditUser_Close();
            }
            this.Form_EditUser_Close();

          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });

    }
  }

  //#region DealDetails 

  //#endregion

  printform(): void {
    this.ReFormat_RequestBody();
  }

  ReFormat_RequestBody(): void {
    var formValue: any = cloneDeep(this.Form_EditUser.value);
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'updatedeal',
      "AccountId": formValue.AccountId,
      "AccountKey": formValue.AccountKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      "TypeCode": "deal",
      "Title": formValue.Title,
      "TitleContent": formValue.TitleContent,
      //"TitleTypeId": formValue.TitleTypeId,
      "TitleTypeId": this._UserAccount.TitleTypeId,
      "Description": formValue.Description,
      "Terms": formValue.Terms,
      "StartDate": this.SelectedDeal.StartDate,
      // "EndDate": formValue.EndDate,
      "EndDate": this.SelectedDeal.EndDate,
      // "TitleContent":formValue.TitleContent,
      // "TitleTypeId": formValue.TitleTypeId,
      CodeValidityDays: "10",
      // CodeValidityStartDate: "2020-11-04 00:00:00",
      CodeValidityEndDate: "2020-11-14 00:00:00",
      UsageTypeCode: "hour",

      "ActualPrice": formValue.ActualPrice,
      "SellingPrice": formValue.Amount,
      "DiscountAmount": formValue.DiscountAmount,

      "DiscountPercentage": 0,

      "Amount": formValue.Amount,
      "Charge": 0.0,
      "CommissionAmount": formValue.CommissionAmount,
      "TotalAmount": 0.0,

      "MaximumUnitSale": formValue.MaximumUnitSale,
      "MaximumUnitSalePerDay": formValue.MaximumUnitSalePerDay,
      "MaximumUnitSalePerPerson": formValue.MaximumUnitSalePerPerson,
      "CategoryKey": formValue.CategoryKey,
      "Schedule": [

      ]
    };

    if (this.NiraOrPercent.DiscountAmount == '0') {
      formRequest.DiscountPercentage = formValue.DiscountAmount;
      formRequest.DiscountAmount = (formValue.DiscountAmount / 100) * formValue.ActualPrice;
    } else if (this.NiraOrPercent.DiscountAmount == '1' && formValue.ActualPrice != 0.0) {
      formRequest.DiscountPercentage = (formValue.DiscountAmount / formValue.ActualPrice) * 100;
    }

    if (this.NiraOrPercent.CommissionAmount == '0') {
      formRequest.CommissionAmount = (formValue.CommissionAmount / 100) * formValue.ActualPrice;
    }

    //#region set image 

    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      formRequest.Images = [
        {
          "ImageContent": this._HelperService._Icon_Cropper_Data,
          "IsDefault": 1
        }
      ];
    }

    //#endregion

    //#region Set Title (not required)

    // switch (formValue.DealTitle) {
    //     case '0': {
    //         formRequest.Title =
    //             formValue.ActualPrice + " " + formRequest.Title + ' for ' +
    //             formValue.SellingPrice + ' at ' + this.SelectedMerchant.DisplayName;
    //     }

    //         break;
    //     case '1': {
    //         formRequest.Title = formValue.DiscountAmount + ' off on ' + formRequest.Title;
    //     }

    //         break;
    //     case '2': {
    //         formRequest.Title = ((formValue.DiscountAmount / formValue.ActualPrice) * 100) + '% off on ' + formRequest.Title;
    //     }

    //         break;

    //     default:
    //         break;
    // }

    // switch (formValue.DealTitle) {
    //   case '1': {
    //     formRequest.TitleContent =
    //       formValue.ActualPrice + " " + formRequest.Title + ' for ' + formValue.Amount + ' at ' + this.SelectedMerchant.DisplayName;
    //     formRequest.TitleTypeId = 1;
    //     // var a = document.getElementById("titleone").innerHTML;
    //     // formRequest.Title = a;
    //   }

    //     break;
    //   case '2': {


    //     if (this.NiraOrPercent.DiscountAmount == '0') {
    //       formRequest.TitleContent = Math.round(((formValue.DiscountAmount / 100) * formValue.ActualPrice)) + ' off on ' + formRequest.Title + ' at ' + this.SelectedMerchant.DisplayName;
    //     } else if (this.NiraOrPercent.DiscountAmount == '1') {
    //       formRequest.TitleContent = formValue.DiscountAmount + ' off on ' + formRequest.Title + ' at ' + this.SelectedMerchant.DisplayName;
    //     }
    //     formRequest.TitleTypeId = 2;
    //   }

    //     break;
    //   case '3': {

    //     if (this.NiraOrPercent.DiscountAmount == '0') {
    //       formRequest.TitleContent = Math.round(formValue.DiscountAmount) + '% off on ' + formRequest.Title + ' at ' + this.SelectedMerchant.DisplayName;
    //     } else if ((this.NiraOrPercent.DiscountAmount == '1' && formValue.ActualPrice != 0)) {
    //       formRequest.TitleContent = Math.round(((formValue.DiscountAmount / formValue.ActualPrice) * 100)) + '% off on ' + formRequest.Title + ' at ' + this.SelectedMerchant.DisplayName;
    //     }
    //     formRequest.TitleTypeId = 3;
    //   }

    //     break;

    //   default:
    //     break;
    // }

    switch (formValue.DealTitle) {
      case '1': {
        formRequest.Title =
          formValue.ActualPrice + " " + formRequest.TitleContent + ' for ' + formValue.Amount + ' at ' + this.SelectedMerchant.DisplayName;
        // formRequest.TitleContent =
        //     formValue.ActualPrice + " " + formRequest.Title + ' for ' + formValue.Amount + ' at ' + this.SelectedMerchant.DisplayName;
        formRequest.TitleTypeId = 1;
        // var a = document.getElementById("titleone").innerHTML;
        // formRequest.Title = a;
      }

        break;
      case '2': {


        if (this.NiraOrPercent.DiscountAmount == '0') {
          formRequest.Title = Math.round(((formValue.DiscountAmount / 100) * formValue.ActualPrice)) + ' off on ' + formRequest.Title + ' at ' + this.SelectedMerchant.DisplayName;
        } else if (this.NiraOrPercent.DiscountAmount == '1') {
          formRequest.Title = formValue.DiscountAmount + ' off on ' + formRequest.TitleContent + ' at ' + this.SelectedMerchant.DisplayName;
        }
        formRequest.TitleTypeId = 2;
      }

        break;
      case '3': {

        if (this.NiraOrPercent.DiscountAmount == '0') {
          formRequest.Title = Math.round(formValue.DiscountAmount) + '% off on ' + formRequest.TitleContent + ' at ' + this.SelectedMerchant.DisplayName;
        } else if ((this.NiraOrPercent.DiscountAmount == '1' && formValue.ActualPrice != 0)) {
          formRequest.Title = Math.round(((formValue.DiscountAmount / formValue.ActualPrice) * 100)) + '% off on ' + formRequest.TitleContent + ' at ' + this.SelectedMerchant.DisplayName;
        }
        formRequest.TitleTypeId = 3;
      }

        break;

      default:
        break;
    }

    //#endregion 

    //#region Add store locations 

    if (!this.IsAllStores) {

      formRequest.Locations = [
      ];

      for (let index = 0; index < this.StoresList.length; index++) {
        const element = this.StoresList[index];

        formRequest.Locations.push({
          ReferenceId: element.ReferenceId,
          ReferenceKey: element.ReferenceKey
        });
      }
    }

    //#endregion

    //#region Add Redeem schedule days 

    for (let index = 0; index < this.WeekDays.length; index++) {
      const element = this.WeekDays[index];

      if (element.selected) {
        // formRequest.Schedule[0].DayOfWeek.push(element.name);
        formRequest.Schedule.push({ DayOfWeek: index });
      }

    }

    //#endregion

    //#region Set Redeem Schedule Timing 

    for (let index = 0; index < formRequest.Schedule.length; index++) {

      if (this.IsValidAllTime) {
        formRequest.Schedule[index].StartHour = '00:00';
        formRequest.Schedule[index].EndHour = '23:59';
        formRequest.Schedule[index].Type = 'dealredeemshedule';
      } else {
        formRequest.Schedule[index].StartHour = moment(this.ValidTimings.Start).format('hh:mm');
        formRequest.Schedule[index].EndHour = moment(this.ValidTimings.End).format('hh:mm');
        formRequest.Schedule[index].Type = 'dealredeemshedule';
      }
    }

    //#endregion

    //#region Set Schedule 

    for (let index = 0; index < 7; index++) {

      if (this.IsRunAllTime) {
        formRequest.Schedule.push({
          DayOfWeek: index,
          StartHour: '00:00',
          EndHour: '23:59',
          Type: 'dealshedule'
        });
      } else {
        formRequest.Schedule.push({
          DayOfWeek: index,
          StartHour: moment(this.DealTimings.Start).format('hh:mm'),
          EndHour: moment(this.DealTimings.End).format('hh:mm'),
          Type: 'dealshedule'
        });

      }
    }

    //#endregion

    //#region Set All Stores 



    //#endregion

    //#region Set Coupon Validity 

    switch (formValue.CouponValidity) {
      case '0': {
        formRequest.CodeValidityDays = this.Expiry.Days * 24;
        formRequest.UsageTypeCode = 'hour'
      }

        break;
      case '1': {
        formRequest.CodeValidityDays = this.Expiry.Hours;
        formRequest.UsageTypeCode = 'hour'
      }

        break;
      case '2': {
        formRequest.CodeValidityEndDate = moment(formRequest.EndDate);
        formRequest.UsageTypeCode = 'date';
      }

        break;
      case '3': {
        formRequest.CodeValidityEndDate = moment(this.Expiry.Date);
        formRequest.UsageTypeCode = 'date';
      }

        break;

      default:
        break;
    }

    //#endregion

    return formRequest;

  }




  //#region Redeem Time 

  IsValidAllTime: boolean = true;
  ValidAllTimeToogle(): void {
    this.IsValidAllTime = !(this.IsValidAllTime);
  }

  //#endregion

  //#region All Stores 

  IsAllStores: boolean = true;
  AllStoresToogle(): void {
    this.IsAllStores = !(this.IsAllStores);
  }

  //#endregion

  IsNoDaySelected: boolean = false;
  toogleWeekDay(index: number): void {
    this.WeekDays[index].selected = !this.WeekDays[index].selected;

    for (let i = 0; i < this.WeekDays.length; i++) {
      const element = this.WeekDays[i];
      if (element.selected) {
        this.IsNoDaySelected = false;
        return;
      }
    }

    this.IsNoDaySelected = true;
  }

  ChangeExpiryDate(event): void {
    this.Expiry.Date = event.end;
  }

  removeImage(): void {
    this.CurrentImagesCount = 0;
    this._HelperService.Icon_Crop_Clear();
  }

  Demo() {
    setTimeout(() => {
      var formValue: any = cloneDeep(this.Form_EditUser.value);
      var formRequest: any = {
        'OperationType': 'new',
        'Task': 'savedeal',
        "AccountId": formValue.AccountId,
        "AccountKey": formValue.AccountKey,
        "TypeCode": "deal",
        "Title": formValue.Title,
        "Description": formValue.Description,
        "Terms": formValue.Terms,
        "StartDate": formValue.StartDate,
        "EndDate": formValue.EndDate,

        CodeValidityDays: "10",
        // CodeValidityStartDate: "2020-11-04 00:00:00",
        CodeValidityEndDate: "2020-11-14 00:00:00",
        UsageTypeCode: "hour",

        "ActualPrice": formValue.ActualPrice,
        "SellingPrice": formValue.Amount,
        "DiscountAmount": formValue.DiscountAmount,

        "DiscountPercentage": 0.0,

        "Amount": formValue.Amount,
        "Charge": 0.0,
        "CommissionAmount": formValue.CommissionAmount,
        "TotalAmount": 0.0,

        "MaximumUnitSale": formValue.MaximumUnitSale,
        "MaximumUnitSalePerDay": formValue.MaximumUnitSalePerDay,
        "MaximumUnitSalePerPerson": formValue.MaximumUnitSalePerPerson,
        "CategoryKey": formValue.CategoryKey,
        "Schedule": [
        ]
      };
      if (this.NiraOrPercent.DiscountAmount == '0') { // percentage
        formRequest.DiscountPercentage = formValue.DiscountAmount;
        formRequest.DiscountAmount = (formValue.DiscountAmount / 100) * formValue.ActualPrice;
        formRequest.SellingPrice = formValue.ActualPrice - formRequest.DiscountAmount;
        formRequest.Amount = formRequest.SellingPrice;
        this.Form_EditUser.patchValue({ SellingPrice: (formValue.ActualPrice - formRequest.DiscountAmount) });
        this.Form_EditUser.patchValue({ Amount: (formValue.ActualPrice - formRequest.DiscountAmount) });
      } else if (this.NiraOrPercent.DiscountAmount == '1' && formValue.ActualPrice != 0.0) { // naira
        formRequest.DiscountPercentage = Math.round((formValue.DiscountAmount / formValue.ActualPrice) * 100);
        formRequest.DiscountAmount = formRequest.DiscountAmount;
        formRequest.Amount = (formValue.ActualPrice - formRequest.DiscountAmount);
        formRequest.SellingPrice = (formValue.ActualPrice - formRequest.DiscountAmount);
        this.Form_EditUser.patchValue({ SellingPrice: (formValue.ActualPrice - formRequest.DiscountAmount) });
        this.Form_EditUser.patchValue({ Amount: (formValue.ActualPrice - formRequest.DiscountAmount) });
      }

      if (this.NiraOrPercent.CommissionAmount == '0') {
        formRequest.CommissionAmount = (formValue.CommissionAmount / 100) * formValue.ActualPrice;
      }
    }, 100);

  }


  //deal approve and deny code

  Approve_RequestBody(ApproveReferencedata): void {
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'approvedeal',
      //'ReferenceKey': this.SelectedDeal.ReferenceKey,
      // 'ReferenceId': this.SelectedDeal.ReferenceId,

      'ReferenceId': this._UserAccount.ReferenceId,
      'ReferenceKey': this._UserAccount.ReferenceKey,

    };
    //#region Set Schedule 

    //#endregion

    return formRequest;

  }

  ApproveReferencedata: any;
  Deal_Approve_popup() {
    this._HelperService.OpenModal('Deal_approve1');
  }

  Deal_Approve() {
    var Req = this.Approve_RequestBody(this.ApproveReferencedata);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, Req);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);

          this.ClearScheduleUpdate();
          this._HelperService.CloseModal('Deal_approve1');
          this.GetDetails();
        }
        else {
          this.SelectedDeal.Schedule = [];
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.SelectedDeal.Schedule = [];
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });

  }

  DenyReferencedata: any
  Deal_deny() {
    // this.DenyReferencedata=Deal_deny;
    this._HelperService.OpenModal('Deal_deny');
  }


  Deny_RequestBody(_FormValue): void {
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'rejectdeal',
      //'ReferenceKey': this.SelectedDeal.ReferenceKey,
      // 'ReferenceId': this.SelectedDeal.ReferenceId,
      'Comment': _FormValue.comment,
      'ReferenceId': this._UserAccount.ReferenceId,
      'ReferenceKey': this._UserAccount.ReferenceKey,

    };
    //#region Set Schedule 

    //#endregion

    return formRequest;

  }

  deal_deny_confirm() {
    var _FormValue = this.Form_AddUser.value;
    var Req = this.Deny_RequestBody(_FormValue);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, Req);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.Form_AddUser_Clear();
          //this.ClearScheduleUpdate();
          this._HelperService.CloseModal('Deal_deny');
          this.GetDetails();
        }
        else {
          this.SelectedDeal.Schedule = [];
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.SelectedDeal.Schedule = [];
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  Form_AddUser: FormGroup;
  Form_AddUser_Load() {


    this.Form_AddUser = this._FormBuilder.group({

      comment: [null, Validators.required],

    });
  }

  Form_AddUser_Clear() {
    this.Form_AddUser_Load();
  }


  coupancount: boolean = false;
  checkcount() {
    if (this.CouponCount > 5000 || this.CouponCount == null) {
      this.coupancount = true;
    }
    else {
      this.coupancount = false;
    }
  }

  showMore = false;


  //#endregion
}


import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
declare var moment: any;
declare var $: any;
import * as cloneDeep from 'lodash/cloneDeep';

@Component({
    selector: 'tu-tusoldhistory',
    templateUrl: './tusoldhistory.component.html',
})
export class TUSoldHistoryComponent implements OnInit {

    public _SubAccountDetails: any =
        {
            "Amount": null,
            "PurchaseDate": null,
            "ExpiaryDate": null,
            "RedeemDate": null,
            "RedeemLocationId": null,
            "RedeemLocationKey": null,
            "RedeemLocationName": null,
            "RedeemLocationAddress": null,
            "ItemCode": null,
            "StatusCode": null,
            "StatusName": null,
            "Merchant": {
                "ReferenceId": null,
                "ReferenceKey": null,
                "DisplayName": null,
                "IconUrl": null
            },
            "Customer": {
                "ReferenceId": null,
                "ReferenceKey": null,
                "Name": null,
                "MobileNumber": null,
                "ImageUrl": null
            },
            "Deal": {
                "ReferenceId": null,
                "ReferenceKey": null,
                "Title": null,
                "ImageUrl": null
            }
        }

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _FilterHelperService: FilterHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
    ) {

    }
    @ViewChild("offCanvas") divView: ElementRef;
    public ResetFilterControls: boolean = true;
    public AcquirerId = 0;
    ngOnInit() {
        Feather.replace();
        this._HelperService.StopClickPropogation();
        this.InitBackDropClickEvent();

        this.TUTr_Setup();
        this.TUTr_Filter_Partners_Load();
        this.TUTr_Filter_Carriers_Load();
        this.TUTr_Filter_Merchants_Load();
        this.TUTr_Filter_UserAccounts_Load();
        this.TUTr_Filter_Stores_Load();
        this.TUTr_Filter_Providers_Load();
        this.TUTr_Filter_Issuers_Load();
        this.TUTr_Filter_CardBrands_Load();
        this.TUTr_Filter_TransactionTypes_Load();
        this.TUTr_Filter_CardBanks_Load();
        this.TUTr_Filter_Banks_Load();
        this.TUTr_Filter_Terminals_Load();
        this.TUTr_Filter_Cashiers_Load();
        this.Form_Update_Load();
        this.Form_EditUser_Load();
    }

    InitBackDropClickEvent(): void {
        var backdrop: HTMLElement = document.getElementById("backdrop");

        backdrop.onclick = () => {
            $(this.divView.nativeElement).removeClass('show');
            backdrop.classList.remove("show");
        };
    }

    RouteDeal(ReferenceData) {
        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveDeal,
            {
                ReferenceKey: ReferenceData.DealKey,
                ReferenceId: ReferenceData.DealId,
                AccountKey: ReferenceData.MerchantKey,
                AccountId: ReferenceData.MerchantId,
                DisplayName: ReferenceData.DisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
            }
        );

        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceData.DealKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.DealId;
        this._HelperService.AppConfig.ActiveAccountKey = ReferenceData.DealKey;
        this._HelperService.AppConfig.ActiveAccountId = ReferenceData.DealId;
        // this._Router.navigate([
        //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Store
        //     .Dashboard,
        //   ReferenceData.ReferenceKey,
        //   ReferenceData.ReferenceId,
        // ]);

        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Analytics,
            ReferenceData.DealKey, ReferenceData.DealId,
            ReferenceData.MerchantId, ReferenceData.MerchantKey,
        ]);
    }
    MerchantRoute(ReferenceData) {
        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveMerchant,
            {
                ReferenceKey: ReferenceData.MerchantKey,
                ReferenceId: ReferenceData.MerchantId,
                DisplayName: ReferenceData.MerchantDisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
            }
        );

        //#endregion
        //#region Set Active Reference Key To Current Merchant 
        this._HelperService.AppConfig.ActiveMerchantReferenceKey = ReferenceData.MerchantKey;
        this._HelperService.AppConfig.ActiveMerchantReferenceId = ReferenceData.MerchantId;
        //#endregion
        //#region navigate 
        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.RewardPercentage,
            ReferenceData.MerchantKey, ReferenceData.MerchantId,
        ]);
    }

    RouteCustomer(ReferenceData) {
        //#region Save Current Merchant To Storage 

        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveCustomer,
            {
                ReferenceKey: ReferenceData.AccountKey,
                ReferenceId: ReferenceData.AccountId,
                DisplayName: ReferenceData.DisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.Customer,
            }
        );

        //#endregion
        //#region Set Active Reference Key To Current Merchant 

        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceData.AccountKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.AccountId;

        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.CustomerPanel.Accounts.WalletInfo,
            ReferenceData.AccountKey, ReferenceData.AccountId,
        ]);

    }

    TUTr_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
    TUTr_InvoiceRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
    TUTr_InvoiceRange_OnChange(changeContext: ChangeContext): void {
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        this.TUTr_InvoiceRangeMinAmount = changeContext.value;
        this.TUTr_InvoiceRangeMaxAmount = changeContext.highValue;
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TUTr_Config: OList;
    public TuTr_Columns =
        {
            Status: true,
            Date: true,
            Account: true,
            AccountName: true,
            Mob: true,
            Biller: true,
            Amount: true,
            Source: true,
            PaymentRef: true
        }

    public TuTr_Filters_List =
        {
            Search: false,
            Date: false,
            Sort: false,
            Status: false,
            Biller: false
        }

    TUTr_Setup() {

        this.TUTr_Config = {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetPurchaseHistory,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals,
            Title: "Purchase History",
            StatusType: "DealCode",
            Type: "soldhistory",
            DefaultSortExpression: "CreateDate desc",
            IsfileDownloaded: false,
            //SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'dealcode.unused', '=='),
            TableFields: [
                {
                    DisplayName: "Title",
                    SystemName: "Title",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Contact No",
                    SystemName: "AccountMobileNumber",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "text-center",
                    Show: true,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                    DefaultValue: "ThankUCash",
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Customer Name",
                    SystemName: "AccountDisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Sold Date",
                    SystemName: "StartDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date text-right",
                    Show: true,
                    IsDateSearchField: false,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
                {
                    DisplayName: "Expiry Date",
                    SystemName: "EndDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date text-right",
                    Show: true,
                    IsDateSearchField: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
                {
                    DisplayName: "Amount",
                    SystemName: "Amount",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "text-center",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    DefaultValue: "ThankUCash",
                    NavigateField: "ReferenceKey"
                },
            ]
        };

        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this._HelperService.Active_FilterInit(this._HelperService.AppConfig.FilterTypeOption.SoldHistory, this.TUTr_Config);
        this.TUTr_GetData();
    }
    TuTr_Setup_SearchCondition() {
        this.TuTr_Columns =
        {
            Status: true,
            Date: true,
            Account: true,
            AccountName: true,
            Mob: true,
            Biller: true,
            Amount: true,
            Source: true,
            PaymentRef: true
        }
        var SearchCondition = '';
        this.TuTr_Filters_List =
        {
            Search: true,
            Date: true,
            Sort: true,
            Status: true,
            Biller: true
        }

        return SearchCondition;
    }

    TuTr_Setup_Fields() {
        var TableFields = [];

        TableFields = [
            {
                DisplayName: "Date",
                SystemName: 'CreateDate',
                DataType: this._HelperService.AppConfig.DataType.Date,
                Class: 'td-date',
                Show: true,
                Search: false,
                IsDateSearchField: true,
                Sort: true,
                ResourceId: null,
            },
            {
                DisplayName: "Expiry Date",
                SystemName: 'EndDate',
                DataType: this._HelperService.AppConfig.DataType.Date,
                Class: 'td-date',
                Show: true,
                Search: false,
                IsDateSearchField: true,
                Sort: true,
                ResourceId: null,
            },
            {
                DisplayName: 'Payment Account Number',
                SystemName: 'AccountNumber',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: false,
            },
            {
                DisplayName: 'Payment Account',
                SystemName: 'AccountDisplayName',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: false,
            },
            {
                DisplayName: 'Mobile',
                SystemName: 'AccountMobileNumber',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: true,
            },
            {
                DisplayName: 'Biller',
                SystemName: 'BillerName',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: true,
            },
            {
                DisplayName: 'Amount',
                SystemName: 'Amount',
                DataType: this._HelperService.AppConfig.DataType.Decimal,
                Show: true,
                Search: false,
                Sort: true,
            },
            {
                DisplayName: 'Source',
                SystemName: 'PaymentSource',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Class: '',
                Show: true,
                Search: false,
                Sort: false,
            },
            {
                DisplayName: 'Payment Reference',
                SystemName: 'PaymentReference',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: false,
            },
        ]

        return TableFields;
    }

    TUTr_ToggleOption_Date(event: any, Type: any) {
        this.TUTr_ToggleOption(event, Type);
    }

    TUTr_ToggleOption(event: any, Type: any, isDownloadClick: boolean = false) {

        if (Type == "csv") {
            this.TUTr_Config.IsfileDownloaded = true;
        }
        
        else {
            this.TUTr_Config.IsfileDownloaded = false;
        }
        if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
            event.data = {
                SalesMin: this.TUTr_InvoiceRangeMinAmount,
                SalesMax: this.TUTr_InvoiceRangeMaxAmount
            }
        }

        if (Type == "date") {
            this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
            this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
        }
        if (event != null) {
            for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                const element = this.TUTr_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }
            }
        }

        this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);
        this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
        if (
            (this.TUTr_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.TUTr_GetData();
        }
        if (isDownloadClick == true) {
            this.TUTr_GetData();
            this.TUTr_Config.IsfileDownloaded = false;
            this.TUTr_GetData();
        }
    }
    

    timeout = null;
    TUTr_ToggleOptionSearch(event: any, Type: any) {
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
            // if (Type == 'date') {
            //     event.start = this._HelperService.DateInUTC(event.start);
            //     event.end = this._HelperService.DateInUTC(event.end);
            // }
            this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

            this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
            this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

            this.TodayStartTime = this.TUTr_Config.StartTime;
            this.TodayEndTime = this.TUTr_Config.EndTime;
            if (event != null) {
                for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                    const element = this.TUTr_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {
                        element.SystemActive = true;
                    }
                    else {
                        element.SystemActive = false;
                    }
                }
            }
            if (
                (this.TUTr_Config.RefreshData == true)
                && this._HelperService.DataReloadEligibility(Type)
            ) {
                this.TUTr_GetData();
            }
        }, this._HelperService.AppConfig.SearchInputDelay);


    }

    public OverviewData: any = {
        Transactions: 0,
        InvoiceAmount: 0.0
    };

    TUTr_GetData() {
        this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.ThankUCash.GetPurchaseHistoryOverview);
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    }
    dealId: any = null;
    dealkey: any = null;

    TUTr_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this.dealkey = ReferenceKey;
        this.dealId = ReferenceData.ReferenceId;
        this.ListAppUsage_GetDetails();
    }


    clicked() {
        $(this.divView.nativeElement).addClass('show');
        var backdrop: HTMLElement = document.getElementById("backdrop");
        backdrop.classList.add("show");
    }

    unclick() {
        $(this.divView.nativeElement).removeClass('show');
        var backdrop: HTMLElement = document.getElementById("backdrop");
        backdrop.classList.remove("show");
    }

    public _CoreUsage: OCoreUsage =
        {
            DealKey: null,
            Quantity: null,
            DealId: null,
            DealCodeAmount: null,
            PaybleAmount: null,
            DealPurchaseDate: null,
            DeliveryTypeName: null,
            DealTypeCode: null,
            DealTypeName: null,
            MerchantIconUrl: null,
            LastUseLocationSourceName: null,
            ProviderDisplayName: null,
            TerminalId: null,
            CashierId: null,
            CashierDisplayName: null,
            LastUseLocationDisplayName: null,
            MerchantDisplayName: null,
            AccountDisplayName: null,
            Title: null,
            ToAccountId: null,
            ToAccountDisplayName: null,
            ToAccountMobileNumber: null,
            MerchantAmount: null,
            BankName: null,
            BankAccountNumber: null,
            BankAccountName: null,
            Charge: null,
            TotalAmount: null,
            ReferenceNumber: null,
            CreatedByDisplayName: null,
            ModifyByDisplayName: null,
            SystemComment: null,

            StatusB: null,
            ProductCategoryName: null,
            PaymentReference: null,
            ProductItemName: null,
            UserRewardAmount: null,
            Amount: null,
            RewardAmount: null,
            Coordinates: null,
            CommisonAmount: null,
            StartDate: null,
            EndDate: null,
            ModifyDate: null,
            CreateDate: null,

            AccountMobileNumber: null,
            AccountKey: null, AccountId: null,
            ReferenceKey: null,
            ReferenceId: null,
            Reference: null,
            StatusCode: null,
            StatusId: null,
            StatusName: null,
            UserAccountKey: null,
            UserAccountIconUrl: null,
            UserAccountDisplayName: null,
            ApiKey: null,
            ApiName: null,
            AppKey: null,
            AppName: null,
            AppOwnerKey: null,
            AppOwnerName: null,
            AppVersionKey: null,
            AppVersionName: null,
            FeatureKey: null,
            FeatureName: null,
            IpAddress: null,
            Latitude: null,
            Longitude: null,
            ProcessingTime: null,
            Request: null,
            RequestTime: null,
            Response: null,
            ResponseTime: null,
            SessionId: null,
            SessionKey: null,
            UserAccountTypeCode: null,
            UserAccountTypeName: null,
            AccountAddress: null
        }

    ListAppUsage_GetDetails() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.GetPurchaseDetails,
            ReferenceKey: this.dealkey,
            ReferenceId: this.dealId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._CoreUsage = _Response.Result as any;

                    this.CountDeal = this._CoreUsage.Quantity;
                    this._CoreUsage.StartDate = this._HelperService.GetDateTimeS(this._CoreUsage.StartDate);
                    this._CoreUsage.CreateDate = this._HelperService.GetDateTimeS(this._CoreUsage.CreateDate);
                    this._CoreUsage.DealPurchaseDate = this._HelperService.GetDateTimeS(this._CoreUsage.DealPurchaseDate);
                    this._CoreUsage.EndDate = this._HelperService.GetDateTimeS(this._CoreUsage.EndDate);
                    this._CoreUsage.ModifyDate = this._HelperService.GetDateTimeS(this._CoreUsage.ModifyDate);
                    this._CoreUsage.StatusB = this._HelperService.GetStatusBadge(this._CoreUsage.StatusCode);
                    this.clicked()
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }


    SetOtherFilters(): void {
        this.TUTr_Config.SearchBaseConditions = [];
        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_Merchant_Selected = 0;
            this.MerchantEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Provider));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_Provider_Selected = 0;
            this.ProvidersEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Store));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_Store_Selected = 0;
            this.StoresEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.TransactionType));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_TransactionType_Selected = 0;
            this.TransTypeEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBank));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_CardBank_Selected = 0;
            this.CardBankEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBrand));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_CardBrand_Selected = 0;
            this.CardBrandEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }

    public TUTr_Filter_Merchant_Option: Select2Options;
    public TUTr_Filter_Merchant_Selected = 0;
    TUTr_Filter_Merchants_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,

            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },

            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Merchant_Option = {
            placeholder: 'Search By Merchant',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Merchants_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant
        );
        this.MerchantEventProcessing(event);
    }

    MerchantEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Merchant_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Merchant_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Store_Toggle = true;

        this.TUTr_Filter_Stores_Load();
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
            this.TUTr_Filter_Store_Toggle = false;
        }, 500);
    }
    // 

    public TUTr_Filter_Partner_Option: Select2Options;
    public TUTr_Filter_Partners_Selected = 0;
    TUTr_Filter_Partners_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetDealPartners,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
            Limit: this._HelperService.AppConfig.LimitFilter,
            RefreshCount: this._HelperService.AppConfig.BooleanTrue,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Partner_Option = {
            placeholder: 'Sort by Partner',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };


    }
    TUTr_Filter_Partners_Change(event: any) {
        if (event.value == this.TUTr_Filter_Partners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'PartnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Partners_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Partners_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Partners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'PartnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Partners_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Partners_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'PartnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Partners_Selected, '='));
        }

        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Partner
        );
    }
    //
    public TUTr_Filter_Carrier_Option: Select2Options;
    public TUTr_Filter_Carriers_Selected = 0;
    TUTr_Filter_Carriers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetCarriers,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Carriers,
            Limit: this._HelperService.AppConfig.LimitFilter,
            RefreshCount: this._HelperService.AppConfig.BooleanTrue,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Carrier_Option = {
            placeholder: 'Sort by Carrier',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Carriers_Change(event: any) {
        if (event.value == this.TUTr_Filter_Carriers_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CarrierId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Carriers_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Carriers_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Carriers_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CarrierId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Carriers_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Carriers_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CarrierId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Carriers_Selected, '='));
        }

        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Carrier
        );
    }
    //

    public TUTr_Filter_Store_Option: Select2Options;
    public TUTr_Filter_Store_Toggle = false;
    public TUTr_Filter_Store_Selected = 0;
    TUTr_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Store_Option = {
            placeholder: 'Search by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Stores_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Store
        );
        this.StoresEventProcessing(event);
    }

    StoresEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    public TUTr_Filter_Cashier_Option: Select2Options;
    public TUTr_Filter_Cashier_Toggle = false;
    public TUTr_Filter_Cashier_Selected = 0;
    TUTr_Filter_Cashiers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetCashiers,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Cashier_Option = {
            placeholder: 'Search by Cashier',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Cashiers_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Cashier
        );
        this.CashiersEventProcessing(event);
    }

    CashiersEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Cashier_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CashierId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Cashier_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Cashier_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Cashier_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CashierId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Cashier_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Cashier_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CashierId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Cashier_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    public TUTr_Filter_Terminal_Option: Select2Options;
    public TUTr_Filter_Terminal_Toggle = false;
    public TUTr_Filter_Terminal_Selected = 0;
    TUTr_Filter_Terminals_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminals,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "TerminalId",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Terminal_Option = {
            placeholder: 'Search by Terminal',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Terminals_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Terminal
        );
        this.TerminalsEventProcessing(event);
    }

    TerminalsEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Terminal_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Terminal_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Terminal_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Terminal_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Terminal_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Terminal_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Terminal_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    public TUTr_Filter_UserAccount_Option: Select2Options;
    public TUTr_Filter_UserAccount_Selected = 0;
    TUTr_Filter_UserAccounts_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "MobileNumber",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.AppUser
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_UserAccount_Option = {
            placeholder: 'Search Customer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_UserAccounts_Change(event: any) {
        if (event.value == this.TUTr_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_UserAccount_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_UserAccount_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TUTr_Filter_Bank_Option: Select2Options;
    public TUTr_Filter_Bank_Selected = 0;
    TUTr_Filter_Banks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
                }]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Bank_Option = {
            placeholder: 'Search By Bank / Acquirer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Banks_Change(event: any) {

        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Bank
        );

        this.BanksEventProcessing(event);
    }

    BanksEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Bank_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Bank_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion

    //#region provider 

    public TUTr_Filter_Provider_Option: Select2Options;
    public TUTr_Filter_Provider_Selected = 0;
    TUTr_Filter_Providers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Provider_Option = {
            placeholder: 'Search By Provider',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Providers_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Provider
        );

        this.ProvidersEventProcessing(event);
    }

    ProvidersEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Provider_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Provider_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    public TUTr_Filter_Issuer_Option: Select2Options;
    public TUTr_Filter_Issuer_Selected = 0;
    TUTr_Filter_Issuers_Load() {
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }]
        };


        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
            [
                this._HelperService.AppConfig.AccountType.PosTerminal,
                this._HelperService.AppConfig.AccountType.PGAccount,
                this._HelperService.AppConfig.AccountType.Cashier,
            ], "=");

        if (this.TUTr_Filter_Provider_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
        }
        if (this.TUTr_Filter_Bank_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'BankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'SubOwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '=');
        //_Select.SearchCondition = this._HelperService.GetSearchConditionStrictOr(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Issuer_Option = {
            placeholder: 'Search By Issuer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Issuers_Change(event: any) {
        if (event.value == this.TUTr_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Issuer_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Issuer_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TUTr_Filter_TransactionType_Option: Select2Options;
    public TUTr_Filter_TransactionType_Selected = 0;
    TUTr_Filter_TransactionTypes_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionType
                },
                {
                    SystemName: "SubParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionTypeReward
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_TransactionType_Option = {
            placeholder: 'Search By Tran. Type',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_TransactionTypes_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.TransactionType
        );

        this.TransTypeEventProcessing(event);
    }

    TransTypeEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TUTr_Filter_CardBrand_Option: Select2Options;
    public TUTr_Filter_CardBrand_Selected = 0;
    TUTr_Filter_CardBrands_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "TypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.CardBrand
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_CardBrand_Option = {
            placeholder: 'Search By Card Brand',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_CardBrands_Change(event: any) {

        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBrand
        );
        this.CardBrandEventProcessing(event);
    }

    CardBrandEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBrand_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBrand_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TUTr_Filter_CardBank_Option: Select2Options;
    public TUTr_Filter_CardBank_Selected = 0;
    TUTr_Filter_CardBanks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields:
                [
                    {
                        SystemName: "ReferenceId",
                        Type: this._HelperService.AppConfig.DataType.Number,
                        Id: true,
                        Text: false,
                    },
                    {
                        SystemName: "Name",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        Id: false,
                        Text: true
                    },
                    {
                        SystemName: "TypeCode",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        SearchCondition: "=",
                        SearchValue: this._HelperService.AppConfig.HelperTypes.CardBank
                    }
                ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_CardBank_Option = {
            placeholder: 'Search By Card Bank',
            ajax: _Transport,
            multiple: false,
            allowClear: true,

        };
    }

    TUTr_Filter_CardBanks_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBank
        );

        this.CardBankEventProcessing(event);
    }

    CardBankEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBank_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBank_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    TodayStartTime = null;
    TodayEndTime = null;

    public _AccountOverview: OAccountOverview =
        {
            ActiveMerchants: 0,
            ActiveMerchantsDiff: 0,
            ActiveTerminals: 0,
            Idle: 0,
            Dead: 0,
            Active: 0,
            Inactive: 0,
            ActiveTerminalsDiff: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardPurchaseAmountDiff: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardPurchaseAmountDiff: 0,
            Merchants: 0,
            PurchaseAmount: 0,
            PurchaseAmountDiff: 0,
            Terminals: 0,
            Transactions: 0,
            TransactionsDiff: 0,
            UnusedTerminals: 0,
            IdleTerminals: 0,
            TerminalStatus: 0,
            DeadTerminals: 0,
            Total: 0,
            TotalTransactions: 0,
            TotalSale: 0,
        }

    Data: any = {};
    GetAccountOverviewLite(): void {
        this._HelperService.IsFormProcessing = true;
        this.Data = {
            Task: 'getaccountoverview',
            StartTime: this.TodayStartTime,
            EndTime: this.TodayEndTime,
            AccountKey: this._HelperService.UserAccount.AccountKey,
            AccountId: this._HelperService.UserAccount.AccountId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.Data);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._AccountOverview = _Response.Result as OAccountOverview;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();
        this.TUTr_Filter_Carriers_Load();
        this.TUTr_Filter_Partners_Load();
        this.TUTr_Filter_Merchants_Load();
        this.TUTr_Filter_UserAccounts_Load();
        this.TUTr_Filter_Stores_Load();
        this.TUTr_Filter_Providers_Load();
        this.TUTr_Filter_Issuers_Load();
        this.TUTr_Filter_CardBrands_Load();
        this.TUTr_Filter_TransactionTypes_Load();
        this.TUTr_Filter_CardBanks_Load();

        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();

    }

    SetSearchRanges(): void {
        //#region Invoice 
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('Amount', this.TUTr_Config.SearchBaseConditions);
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }

        //#endregion      

    }

    //#region SalesOverview 

    GetOverviews(ListOptions: any, Task: string): any {
        this._HelperService.IsFormProcessing = true;

        ListOptions.SearchCondition = '';
        ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }


        var pData = {
            Task: Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            AccountId: ListOptions.AccountId,
            AccountKey: ListOptions.AccountKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.OverviewData = _Response.Result as any;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);

            });
    }

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
        this.SetOtherFilters();
        this.SetSalesRanges();
        this.TUTr_GetData();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_Store(Type, index);
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
        if (Type == 'Time') {
            this._HelperService.AppConfig.DateRangeOptions.startDate = new Date(2017, 0, 1, 0, 0, 0, 0);
            this._HelperService.AppConfig.DateRangeOptions.endDate = moment().endOf("day");
        }
        this.SetOtherFilters();
        this.SetSalesRanges();
        this.TUTr_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            // input: "text",
            html:
                '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
                '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
                '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
            focusConfirm: false,
            preConfirm: () => {
                return {
                    filter: document.getElementById('swal-input1')['value'],
                    private: document.getElementById('swal-input2')['checked']
                }
            },
            // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            // inputAttributes: {
            //   autocapitalize: "off",
            //   autocorrect: "off",
            //   maxLength: "4",
            //   minLength: "4",
            // },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {

                if (result.value.filter.length < 5) {
                    this._HelperService.NotifyError('Enter filter name length greater than 4');
                    return;
                }
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();
                this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);
                var AccessType: number = result.value.private ? 0 : 1;
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.SoldHistory,
                    AccessType
                );
                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();
                this._HelperService.Delete_Filter(this._HelperService.AppConfig.FilterTypeOption.SoldHistory);
                this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
                this.TUTr_GetData();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        if (this.TUTr_InvoiceRangeMinAmount == null || this.TUTr_InvoiceRangeMinAmount == undefined ||
            this.TUTr_InvoiceRangeMaxAmount == null || this.TUTr_InvoiceRangeMaxAmount == undefined
        ) {
            this._HelperService.NotifyError("Value not be null or undefined");
        }
        else if (this.TUTr_InvoiceRangeMinAmount > this.TUTr_InvoiceRangeMaxAmount) {
            this._HelperService.NotifyError("Minimum  Amount should be less than Maximum  Amount");
        }

        else {
            this.SetSearchRanges();
            this._HelperService.MakeFilterSnapPermanent();
            this.TUTr_GetData();
            this.ResetFilterUI(); this._HelperService.StopClickPropogation();
            if (ButtonType == 'Sort') {
                $("#TUTr_sdropdown").dropdown('toggle');
            } else if (ButtonType == 'Other') {
                $("#TUTr_fdropdown").dropdown('toggle');
            }
            this.ResetFilterUI(); this._HelperService.StopClickPropogation();
        }
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
        this.SetOtherFilters();
        this.SetSalesRanges();
        this.TUTr_GetData();
        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }


    SetSalesRanges(): void {
        this.TUTr_InvoiceRangeMinAmount = this.TUTr_Config.SalesRange.SalesMin;
        this.TUTr_InvoiceRangeMaxAmount = this.TUTr_Config.SalesRange.SalesMax;
    }


    //varrify deal code start---
    Form_EditUser: FormGroup;

    VarifyDealCode() {
        this._HelperService.OpenModal("Form_Varify_deal_code");
    }

    VarifyDealdetails() {
        this._HelperService.OpenModal("Form_Deal_Code_Details");
    }

    Form_EditUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;
        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_EditUser = this._FormBuilder.group({

            ReferenceId: this._HelperService.AppConfig.ActiveSubAccountReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveSubAccountReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveReferenceId,
            AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
            Name: [null, Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11)])]
            // OperationType: "new",
            //  Task: this._HelperService.AppConfig.Api.Core.UpdateSubAccount,
            // RoleId:[7, Validators.required],
            // RoleKey:['admin', Validators.required],
        });
        // this.Form_EditUser = this._FormBuilder.group({
        //     OperationType: 'new',
        //     Task: 'validatedealcode',
        //     TypeCode: 'deal',
        //     Name: [null, Validators.required,Validators.minLength(2),
        //         Validators.maxLength(11)],

        // });
    }
    Form_EditUser_Clear() {
        this.Form_EditUser.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_EditUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }

    public SaveAccountRequest: any;
    SelectStatusunused = false;
    SelectStatusredeem = false;
    SelectStatusExpired = false;
    SelectStatusBloacked = false;
    Form_EditUser_Process(_FormValue: any) {
        this.SaveAccountRequest = this.ReFormat_RequestBody();
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.SaveAccountRequest);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._SubAccountDetails = _Response.Result;
                    if (this._SubAccountDetails.StatusCode == "dealcode.unused") //  unused
                    {
                        this.SelectStatusunused = true;
                        this.SelectStatusredeem = false;
                        this.SelectStatusExpired = false;
                        this.SelectStatusBloacked = false;
                    }
                    else if (this._SubAccountDetails.StatusCode == "dealcode.used") //  used
                    {
                        this.SelectStatusunused = false;
                        this.SelectStatusredeem = true;
                        this.SelectStatusExpired = false;
                        this.SelectStatusBloacked = false;
                    }
                    else if (this._SubAccountDetails.StatusCode == "dealcode.expired") // expired
                    {
                        this.SelectStatusunused = false;
                        this.SelectStatusredeem = false;
                        this.SelectStatusExpired = true;
                        this.SelectStatusBloacked = false;
                    }
                    else if (this._SubAccountDetails.StatusCode == "dealcode.blocked") // expired
                    {
                        this.SelectStatusunused = false;
                        this.SelectStatusredeem = false;
                        this.SelectStatusExpired = false;
                        this.SelectStatusBloacked = true;
                    }
                    else {
                        this.SelectStatusunused = false;
                        this.SelectStatusredeem = false;
                        this.SelectStatusExpired = false;
                        this.SelectStatusBloacked = true;
                    }
                    // this.InputFileComponent_Term.files.pop();
                    this._HelperService.CloseModal("Form_Varify_deal_code");
                    this.VarifyDealdetails();
                    this.Form_EditUser_Clear();
                    // if (_FormValue.OperationType == 'edit') {
                    // }
                    // else if (_FormValue.OperationType == 'close') {
                    //     this.Form_EditUser_Close();
                    // }
                    this.Form_EditUser_Close();
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);

                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    ReFormat_RequestBody(): void {
        var formValue: any = cloneDeep(this.Form_EditUser.value);
        var formRequest: any = {
            'OperationType': 'new',
            'Task': 'validatedealcode',
            "ReferenceCode": formValue.Name,
            //"AccountId": formValue.AccountId,
            // "AccountKey": formValue.AccountKey,
            // "TypeCode": "deal",
            // "Title": formValue.Title,
            // "Description": formValue.Description,
            // "Terms": formValue.Terms,
            // "StartDate": formValue.StartDate,
            //  CodeValidityDays: "10",
            //  CodeValidityEndDate: "2020-11-14 00:00:00",
            //  UsageTypeCode: "hour"
        };
        //#endregion
        return formRequest;
    }


    Form_EditUser_Show() {
        this._HelperService.OpenModal("Form_EditUser_Content");
    }
    Form_EditUser_Close() {
        // this._Router.navigate([
        //     this._HelperService.AppConfig.Pages.System.AdminUsers
        // ]);
        this._HelperService.CloseModal("Form_EditUser_Content");
    }

    //#endregion


    //exp code started
    UpdateDealCode() {
        this._HelperService.OpenModal("Form_Update_deal_code");
    }
    Form_Update: FormGroup;

    Form_Update_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_Update = this._FormBuilder.group({
            ReferenceId: this._HelperService.AppConfig.ActiveSubAccountReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveSubAccountReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveReferenceId,
            AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
            Name: [null, Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11)])]
        });
    }

    Form_Update_Clear() {
        this.Form_Update.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_Update_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }

    ResetStatusValue: boolean = true;

    Form_Update_Process(_FormValue: any) {
        this.SaveAccountRequest = this.ReFormatexp_RequestBody();
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.SaveAccountRequest);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                    // this._SubAccountDetails = _Response.Result;
                    // if (this._SubAccountDetails.StatusCode == "dealcode.unused") //  unused
                    // {

                    //     this.SelectStatusunused = true;
                    //     this.SelectStatusredeem = false;
                    //     this.SelectStatusExpired = false;
                    //     this.SelectStatusBloacked = false;
                    // }
                    // else if (this._SubAccountDetails.StatusCode == "dealcode.used") //  used
                    // {

                    //     this.SelectStatusunused = false;
                    //     this.SelectStatusredeem = true;
                    //     this.SelectStatusExpired = false;
                    //     this.SelectStatusBloacked = false;
                    // }
                    // else if (this._SubAccountDetails.StatusCode == "dealcode.expired") // expired
                    // {
                    //     this.SelectStatusunused = false;
                    //     this.SelectStatusredeem = false;
                    //     this.SelectStatusExpired = true;
                    //     this.SelectStatusBloacked = false;
                    // }
                    // else {
                    //     this.SelectStatusunused = false;
                    //     this.SelectStatusredeem = false;
                    //     this.SelectStatusExpired = false;
                    //     this.SelectStatusBloacked = true;
                    // }



                    // this.InputFileComponent_Term.files.pop();
                    this._HelperService.CloseModal("Form_Update_deal_code");
                    // this.VarifyDealdetails();
                    this.Form_Update_Clear();
                    this.Form_Update_Close();
                    this.TUTr_GetData();
                    this.ResetStatusValue = false;
                    setTimeout(() => {
                        this.ResetStatusValue = true;
                    }, 1000);
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    ReFormatexp_RequestBody(): void {
        var formValue: any = cloneDeep(this.Form_Update.value);
        var formRequest: any = {
            'OperationType': 'new',
            'Task': 'updatedealcodestatus',
            "ReferenceCode": formValue.Name,
            "StatusCode": this.UpdatedStatusCode,
        };
        return formRequest;
    }

    UpdatedStatusCode: any;

    typeFilterToApply(event, type) {
        this.UpdatedStatusCode = event.data[0].code;
    }

    Form_Update_Close() {
        // this._Router.navigate([
        //     this._HelperService.AppConfig.Pages.System.AdminUsers
        // ]);
        this._HelperService.CloseModal("Form_Update_deal_code");
    }
    //end code


    // mark as delivered for product deal start
    DeliverProduct() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeliverTitle,
            text: "Click on Deliverd button to confirm",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: 'Deliverd',
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                var pData = {
                    Task: 'updatedealcodestatus',
                    ReferenceId: this._CoreUsage.ReferenceId,
                    ReferenceKey: this._CoreUsage.ReferenceKey,
                    ReferenceCode: null,
                    Statuscode: "dealcode.used"
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Order Status Updated Successfully");
                            this.unclick();
                            this.TUTr_GetData();
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this.unclick();
                    });
            }
            else { }
        });

    }
    // mark as delivered for product deal end

    i = 1;
    CountDeal: any;

    subQuantity() {
        this.i = this.CountDeal;
        if (this.CountDeal > 1) {
            this.CountDeal = this.CountDeal - 1;
        }
    }

    CancelDealQuantity() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.CancelTitle,
            text: "Click on Ok button to Update Deal Count",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: 'Ok',
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                var pData = {
                    Task: 'canceldealquantity',
                    TotalDealCount: this._CoreUsage.Quantity,
                    ReferenceId: this._CoreUsage.ReferenceId,
                    ReferenceKey: this._CoreUsage.ReferenceKey,
                    CancelCount: this.CountDeal,
                    DealPrice: this._CoreUsage.Amount,
                    DealId: this._CoreUsage.DealId,
                    DealKey: this._CoreUsage.DealKey,
                    PaymentReference: this._CoreUsage.PaymentReference,
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, pData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess(_Response.Message);
                            this.unclick();
                            this.TUTr_GetData();
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this.unclick();
                    });
            }
            else { }
        });


    }


}
export class OAccountOverview {
    public TotalTransactions?: number;
    public TotalSale?: number;
    public DeadTerminals?: number;
    public IdleTerminals?: number;
    public TerminalStatus?: number;
    public Total?: number;
    public Idle?: number;
    public Active?: number;
    public Dead?: number;
    public Inactive?: number;
    public UnusedTerminals?: number;
    public Merchants: number;
    public ActiveMerchants: number;
    public ActiveMerchantsDiff: number;
    public Terminals: number;
    public ActiveTerminals: number;
    public ActiveTerminalsDiff: number;
    public Transactions: number;
    public TransactionsDiff: number;
    public PurchaseAmount: number;
    public PurchaseAmountDiff: number;
    public CashRewardPurchaseAmount: number;
    public CashRewardPurchaseAmountDiff: number;
    public CardRewardPurchaseAmount: number;
    public CardRewardPurchaseAmountDiff: number;
}

export class OCoreUsage {
    public DealKey: string;
    public Quantity: number;
    public DealId: number;
    public DealCodeAmount: string;
    public PaybleAmount: string;
    public DealPurchaseDate: string;
    public DeliveryTypeName: string;
    public MerchantIconUrl: string;
    public LastUseLocationSourceName: string;
    public ProviderDisplayName: string;
    public TerminalId: number;
    public CashierId: number;
    public CashierDisplayName: string;
    public LastUseLocationDisplayName: string;
    public MerchantDisplayName: string;
    public DealTypeName: string;
    public DealTypeCode: string;
    public UserRewardAmount: number;
    public BankAccountNumber: number;
    public Charge: number;
    public TotalAmount: number;
    public MerchantAmount: number;
    public ToAccountId: number;
    public ToAccountMobileNumber: number;
    public AccountDisplayName: string;
    public ToAccountDisplayName: string;
    public Title: string;
    public BankName: string;
    public ReferenceNumber: string;
    public CreatedByDisplayName: string;
    public BankAccountName: string;
    public ModifyByDisplayName: string;
    public SystemComment: string;
    public ProductCategoryName: string;
    public ProductItemName: string;
    public RewardAmount: number;
    public Amount: number;
    public CommisonAmount: number;
    public AccountMobileNumber: number;
    public PaymentReference: string;
    public StatusB: string;
    Coordinates?: any;
    public ReferenceId: number;
    public AccountId: number;
    public AccountKey: string;
    public Reference: string;
    public ReferenceKey: string;
    public AppOwnerKey: string;
    public AppOwnerName: string;
    public AppKey: string;
    public AppName: string;
    public ApiKey: string;
    public ApiName: string;
    public AppVersionKey: string;
    public AppVersionName: string;
    public FeatureKey: string;
    public FeatureName: string;
    public UserAccountKey: string;
    public UserAccountDisplayName: string;
    public UserAccountIconUrl: string;
    public UserAccountTypeCode: string;
    public UserAccountTypeName: string;
    public SessionId: string;
    public SessionKey: string;
    public IpAddress: string;
    public Latitude: string;
    public Longitude: string;
    public Request: string;
    public Response: string;
    public RequestTime: Date;
    public StartDate: Date;
    public ModifyDate: Date;
    public EndDate: Date;
    public CreateDate: Date;
    public ResponseTime: Date;
    public ProcessingTime: string;
    public StatusId: number;
    public StatusCode: string;
    public StatusName: string;
    public AccountAddress: string;
}



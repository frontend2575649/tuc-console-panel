import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from "../../../../service/service";;
import swal from 'sweetalert2';
declare var $: any;
import * as Feather from 'feather-icons';
declare var google: any;
declare var moment: any;
import * as cloneDeep from 'lodash/cloneDeep';
import { InputFileComponent, InputFile } from 'ngx-input-file';
import * as _ from 'lodash';
import { Select2OptionData } from 'ng2-select2';

@Component({
    selector: 'app-tuadddeals',
    templateUrl: './tuadddeals.component.html',
    styleUrls: ['./tuadddeals.component.css']
})
export class AddDealsComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {

    }
    //#region  Variables

    //#endregion
    ngOnInit() {
        $('[data-toggle="tooltip"]').tooltip();
        if (this.InputFileComponent_Term != undefined) {
            this.CurrentImagesCount = 0;
            this._HelperService._InputFileComponent = this.InputFileComponent_Term;
            this.InputFileComponent_Term.onChange = (files: Array<InputFile>): void => {
                if (files.length >= this.CurrentImagesCount) {
                    this._HelperService._SetFirstImageOrNone(this.InputFileComponent_Term.files);
                }
                this.CurrentImagesCount = files.length;
            };
        }
        this.Form_AddUser_Load();
        this.GetDealCategories_List();
        this.GetMerchants_List();
    }


    //#region Deal Form Manager  
    Form_AddUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deals]);
    }
    Form_AddUser_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_AddUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: 'savedeal',
            TypeCode: 'deal',
            //  CategoryKey: [null, Validators.required],
            SubCategoryKey: [null, Validators.required],
            AccountId: [null, Validators.required],
            AccountKey: [null, Validators.required],
            // Title: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            TitleContent: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            Description: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(1256)])],
            Terms: [null, Validators.required],
            CommissionAmount: [null, Validators.compose([Validators.required])],
            DiscountAmount: [null],
            // MaximumUnitSalePerDay: [null, Validators.compose([Validators.required, Validators.min(1)])],
            MaximumUnitSale: [null, Validators.compose([Validators.required, Validators.min(1)])],
            MaximumUnitSalePerPerson: [null, Validators.compose([Validators.required, Validators.min(1)])],
            // IconContent: this._HelperService._Icon_Cropper_Data,
            Images: this.images,
            //SellingPrice: [null, Validators.required],
            //Amount: [null, Validators.required],
            ActualPrice: [null, Validators.compose([Validators.required, Validators.min(1)])],
            EndDate: moment().add(24, 'hours'),

            //  EndDate:[null, Validators.compose([Validators.required])],
            StartDate: moment(),
            DealTitle: ["0", Validators.required], //transient
            CouponValidity: null, //transient
            SettlementTypeCode: ['postpaid']
        });
    }
    Form_AddUser_Clear() {
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 300);
        this.Form_AddUser.reset();
        this._HelperService.Icon_Crop_Clear();
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_AddUser_Process(_FormValue: any, IsDraft: boolean) {
        this._HelperService.IsFormProcessing = true;
        this.DuplicateAccountId = _FormValue.AccountId;
        this.DuplicateAccountKey = _FormValue.AccountKey;
        this.SaveAccountRequest = this.ReFormat_RequestBody();
        if (IsDraft) {
            this.SaveAccountRequest.StatusCode = 'deal.draft';
        } else {
            this.SaveAccountRequest.StatusCode = 'deal.published';
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.SaveAccountRequest);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess('Deal created successfully. It will take upto 5 minutes to update changes.');
                    this.Form_AddUser_Clear();
                    if (_FormValue.OperationType == 'edit') {
                    }
                    else if (_FormValue.OperationType == 'close') {
                        this.Form_AddUser_Close();
                    }
                    this.Form_AddUser_Close();
                }

                else if (_Response.Message == 'HCP0142: Deal title already exists. Please update existing deal or use new title for deal') {
                    this._DealDetails = _Response.Result;
                    this._HelperService.OpenModal('exampleModal');
                    return;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }
    //#endregion
    //#region Server Lists
    GetDealCategories_List() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.getallcategories,
            Offset: 0,
            Limit: 1000,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System2, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    var _Data = _Response.Result.Data as any[];
                    if (_Data.length > 0) {
                        var parentCategories = [];
                        _Data.forEach(element => {
                            var itemIndex = parentCategories.findIndex(x => x.ParentCategoryId == element.ParentCategoryId);
                            if (itemIndex == -1) {
                                var categories = _Data.filter(x => x.ParentCategoryId == element.ParentCategoryId);
                                parentCategories.push(
                                    {
                                        ParentCategoryId: element.ParentCategoryId,
                                        ParentCategoryKey: element.ParentCategoryKey,
                                        ParentCategoryName: element.ParentCategoryName,
                                        Categories: categories,
                                    }
                                );
                            }
                        });
                        var finalCat = [];
                        parentCategories.forEach(element => {
                            var Item = {
                                id: element.ParentCategoryId,
                                text: element.ParentCategoryName,
                                children: [],
                                additional: element,
                            };
                            element.Categories.forEach(CategoryItem => {
                                var cItem = {
                                    id: CategoryItem.ReferenceId,
                                    text: CategoryItem.Name + " (" + CategoryItem.Fees + "%)",
                                    additional: CategoryItem,
                                };
                                Item.children.push(cItem);
                            });
                            finalCat.push(Item);
                        });
                        this._S2Categories_Data = finalCat;
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    _SelectedSubCategory =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Fee: 5,
            Name: null,
            IconUrl: null,

            ParentCategoryId: 0,
            ParentCategoryKey: 0,
            ParentCategoryName: 0
        };
    GetDealCategories_Selected(event: any) {
        this.catfees = event.data[0].additional.Fees;
        this._SelectedSubCategory.ReferenceId = event.data[0].additional.ReferenceId;
        this._SelectedSubCategory.ReferenceKey = event.data[0].additional.ReferenceKey;
        this._SelectedSubCategory.Name = event.data[0].additional.Name;
        this._SelectedSubCategory.IconUrl = event.data[0].additional.IconUrl;
        this._SelectedSubCategory.Fee = event.data[0].additional.Fees;
        this._SelectedSubCategory.ParentCategoryId = event.data[0].additional.ParentCategoryId;
        this._SelectedSubCategory.ParentCategoryKey = event.data[0].additional.ParentCategoryKey;
        this._SelectedSubCategory.ParentCategoryName = event.data[0].additional.ParentCategoryName;
        this.Form_AddUser.patchValue(
            {
                CategoryKey: this._SelectedSubCategory.ReferenceKey,
                SubCategoryKey: this._SelectedSubCategory.ParentCategoryKey
            }
        );
        this._AmountDistribution.TUCAmount = this._SelectedSubCategory.Fee;
        this.ProcessAmounts();
        this.Demo();
    }
    public GetMerchants_Option: Select2Options;
    public GetMerchants_Transport: any;
    public SelectedMerchant: any = {};
    GetMerchants_List() {
        var PlaceHolder = "Select Merchant";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ]
        }

        this.GetMerchants_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetMerchants_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetMerchants_Transport,
            multiple: false,
        };
    }
    GetMerchants_ListChange(event: any) {
        this.SelectedMerchant = event.data[0];
        this.Form_AddUser.patchValue(
            {
                AccountKey: event.data[0].ReferenceKey,
                AccountId: event.data[0].ReferenceId
            }
        );
        this.CheckDuplicate();
        this.MerchantNameCheck = true;



    }
    //#endregion

    //#region  Title Manager
    _Titles =
        {
            OriginalTitle: "",
            Title1: this._HelperService.UserCountry.CurrencyCode + " X deal title for " + this._HelperService.UserCountry.CurrencyCode + "Y",
            Title2: this._HelperService.UserCountry.CurrencyCode + " Y off on deal title",
            Title3: "x% off on deal title"
        };
    ProcessTitle() {
        if (this._AmountDistribution.ActualPrice == undefined) {
            this._AmountDistribution.ActualPrice = 0;
        }
        if (this._AmountDistribution.SellingPrice == undefined) {
            this._AmountDistribution.SellingPrice = 0;
        }
        if (this._AmountDistribution.SellingPricePercentage == undefined) {
            this._AmountDistribution.SellingPricePercentage = 0;
        }
        if (this._AmountDistribution.SellingPriceDifference == undefined) {
            this._AmountDistribution.SellingPriceDifference = 0;
        }
        if (this._AmountDistribution.TUCPercentage == undefined) {
            this._AmountDistribution.TUCPercentage = 0;
        }
        if (this._AmountDistribution.TUCAmount == undefined) {
            this._AmountDistribution.TUCAmount = 0;
        }
        if (this._AmountDistribution.MerchantPercentage == undefined) {
            this._AmountDistribution.MerchantPercentage = 0;
        }
        if (this._AmountDistribution.MerchantAmount == undefined) {
            this._AmountDistribution.MerchantAmount = 0;
        }
        this._Titles.Title1 = this._HelperService.UserCountry.CurrencyCode + " " + this._AmountDistribution.ActualPrice + " " + this._Titles.OriginalTitle + " for " + this._HelperService.UserCountry.CurrencyCode + " " + this._AmountDistribution.SellingPrice;
        this._Titles.Title2 = this._HelperService.UserCountry.CurrencyCode + " " + this._AmountDistribution.SellingPriceDifference + " off on " + this._Titles.OriginalTitle;
        this._Titles.Title3 = this._AmountDistribution.SellingPricePercentage + "% off on " + this._Titles.OriginalTitle;



        // if (this._Titles.OriginalTitle != undefined && this._Titles.OriginalTitle != null && this._Titles.OriginalTitle != "") {
        //     this._Titles.Title1 = this._HelperService.UserCountry.CurrencyCode + " " + this._AmountDistribution.ActualPrice + " " + this._Titles.OriginalTitle + " for " + this._HelperService.UserCountry.CurrencyCode + " " + this._AmountDistribution.SellingPrice;
        //     this._Titles.Title2 = this._HelperService.UserCountry.CurrencyCode + " " + this._AmountDistribution.SellingPriceDifference + " off on " + this._Titles.OriginalTitle;
        //     this._Titles.Title3 = this._AmountDistribution.SellingPricePercentage + "% off on " + this._Titles.OriginalTitle;
        // }
        // else {
        //     // this._Titles.Title1 = this._HelperService.UserCountry.CurrencyCode + " X deal title for " + this._HelperService.UserCountry.CurrencyCode + "Y";
        //     // this._Titles.Title2 = this._HelperService.UserCountry.CurrencyCode + " Y off on deal title";
        //     // this._Titles.Title3 = "x% off on deal title";
        // }
    }
    //#endregion


    //#region  Amount Distrubution
    _AmountDistribution: any =
        {
            ActualPrice: 0,
            SellingPrice: 0,
            SellingPricePercentage: 0,
            SellingPriceDifference: 0,
            TUCPercentage: 5,
            TUCAmount: 0,
            MerchantPercentage: 0,
            MerchantAmount: 0,
        };
    ProcessAmounts() {
        var basePercentage = 5;
        var tAmount = this._AmountDistribution;
        if (tAmount.TUCPercentage != undefined && tAmount.TUCPercentage != null && tAmount.TUCPercentage > 0) {
            basePercentage = tAmount.TUCPercentage;
        }
        if (tAmount.ActualPrice != undefined && tAmount.ActualPrice != null && tAmount.ActualPrice > 0) {
            if (tAmount.ActualPrice != undefined && tAmount.ActualPrice != null && tAmount.ActualPrice > 0) {
                if (tAmount.ActualPrice >= tAmount.SellingPrice) {
                    tAmount.SellingPriceDifference = this._HelperService.GetFixedDecimalNumber(tAmount.ActualPrice - tAmount.SellingPrice);
                    tAmount.SellingPricePercentage = this._HelperService.GetPercentageFromNumber(tAmount.SellingPriceDifference, tAmount.ActualPrice);
                    tAmount.TUCAmount = this._HelperService.GetAmountFromPercentage(tAmount.SellingPrice, tAmount.TUCPercentage);
                    tAmount.MerchantAmount = this._HelperService.GetFixedDecimalNumber(tAmount.SellingPrice - tAmount.TUCAmount);
                    tAmount.MerchantPercentage = this._HelperService.GetPercentageFromNumber(tAmount.MerchantAmount, tAmount.SellingPrice);
                }
                else {
                    if (parseFloat(this._AmountDistribution.SellingPrice) >= parseFloat(this._AmountDistribution.ActualPrice)) {
                        {
                            setTimeout(() => {
                                this._AmountDistribution =
                                {
                                    ActualPrice: this._AmountDistribution.ActualPrice,
                                    SellingPrice: 0,
                                    SellingPricePercentage: 0,
                                    SellingPriceDifference: 0,
                                    TUCPercentage: this._AmountDistribution.TUCPercentage,
                                    TUCAmount: 0,
                                    MerchantPercentage: 0,
                                    MerchantAmount: 0,
                                };
                            }, 200);
                        }
                    }
                }
            }
            this.ProcessTitle();
        }
    }
    //#endregion



    public SaveAccountRequest: any;
    public value: any;

    public DealNameExist1: boolean = false;
    public DealNameExist2: boolean = false;
    public DealNameExist3: boolean = false;
    public MerchantNameCheck: boolean = false;
    public DealTitleNameCheck: boolean = false;
    public DiscountAmountCheck: boolean = false;
    public ActualPriceCheck: boolean = false;


    public NiraOrPercent: any = {
        DiscountAmount: "1",
        CommissionAmount: "1"
    }

    DealTimings: any = {
        Start: new Date(),
        End: new Date()
    }

    ValidTimings: any = {
        Start: new Date(),
        End: new Date()
    }

    Expiry: any = {
        Hours: 0,
        Days: 0,
        Date: new Date()
    }

    public WeekDays: any[] = [
        { name: 'Monday', selected: true },
        { name: 'Tuesday', selected: true },
        { name: 'Wednesday', selected: true },
        { name: 'Thursday', selected: true },
        { name: 'Friday', selected: true },
        { name: 'Saturday', selected: true },
        { name: 'Sunday', selected: true }
    ]

    @ViewChild(InputFileComponent)
    private InputFileComponent_Term: InputFileComponent;
    CurrentImagesCount: number = 0;

    catfees: any;
    public _S2Categories_Data: Array<Select2OptionData>;


    actualprice: any;
    discountprice: any;
    actualpricecheck: boolean = false;
    CheckValue(CheckValue) {

        if (CheckValue == 'ActualPrice') {
            this.ActualPriceCheck = true;

        }
        else if (CheckValue == 'DiscountAmount') {
            this.DiscountAmountCheck = true;
        }

        else if (CheckValue == 'TitleContent') {
            this.DealTitleNameCheck = true;
        }

        this.TrimTitle = this.Form_AddUser.controls['TitleContent'].value.trim();
        this.CheckDuplicate();

    }

    CheckPrice() {
        this.actualpricecheck = false;
        this.actualprice = this.Form_AddUser.controls['ActualPrice'].value;
        this.discountprice = this.Form_AddUser.controls['CommissionAmount'].value;
        if (this.discountprice > this.actualprice) {
            this.actualpricecheck = true;
        }


    }

    maxdealavailable: any;
    maxdealperperson: any;
    maxdealperday: any;
    maxdealcheck: boolean = false;
    maxdealcheckperson: boolean = false;
    CheckDealCoupons() {
        this.maxdealcheck = false;
        this.maxdealavailable = this.Form_AddUser.controls['MaximumUnitSale'].value;
        // this.maxdealperperson = this.Form_AddUser.controls['MaximumUnitSalePerDay'].value;
        this.maxdealperday = this.Form_AddUser.controls['MaximumUnitSalePerPerson'].value;
        if (this.maxdealperperson > this.maxdealavailable) {
            this.maxdealcheck = true;
        }


    }

    CheckDealCoupons1() {
        this.maxdealcheckperson = false;
        this.maxdealavailable = this.Form_AddUser.controls['MaximumUnitSale'].value;
        // this.maxdealperperson = this.Form_AddUser.controls['MaximumUnitSalePerDay'].value;
        this.maxdealperday = this.Form_AddUser.controls['MaximumUnitSalePerPerson'].value;
        if (this.maxdealperday > this.maxdealavailable) {
            this.maxdealcheckperson = true;
        }
    }

    SendNotification: boolean = false;
    SendNotificationToggle(): void {
        this.SendNotification = !(this.SendNotification);
    }



    public GetStores_Option: Select2Options;
    public GetStores_Transport: any;
    public StoresList: any = []
    GetStores_List() {
        var PlaceHolder = "Select Stores";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: 'StatusCode',
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: '=',
                //     SearchValue: this._HelperService.AppConfig.Status.Active,
                // }
            ]
        }


        this.GetStores_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetStores_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetStores_Transport,
            multiple: true,
        };
    }
    GetStores_ListChange(event: any) {
        this.StoresList = event.data
    }

    ScheduleStartDateRangeChange(value) {

        this.IsStartDateAfterEnd = moment(this.Form_AddUser.controls['EndDate'].value).isBefore(moment(value.start));

        this.Form_AddUser.patchValue(
            {
                StartDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
            }
        );
        this.Form_AddUser.patchValue(
            {
                StartDate: value.start,

            }
        );
    }
    EndDatevalue: any = null;

    ScheduleEndDateRangeChange(value) {

        this.IsStartDateAfterEnd = moment(moment(value.start)).isBefore(this.Form_AddUser.controls['StartDate'].value);
        this.EndDatevalue = moment(value.start).format('DD-MM-YYYY'),

            this.Form_AddUser.patchValue(
                {
                    EndDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
                }
            );
        this.Form_AddUser.patchValue(
            {
                EndDate: value.start,
            }
        );

    }

    _CurrentAddress: any = {};
    Form_AddUser: FormGroup;
    IsStartDateAfterEnd: boolean = false;
    DuplicateDealKey: string;
    DuplicateDealId: number;
    DuplicateAccountId: number;
    DuplicateAccountKey: number;
    public _DealDetails: any =
        {
        };
    custsellingpricedis: any;
    custsellingpricedisper: any;
    merchantamt: any;
    paybletomerchant: any;
    catfees1: boolean = false;
    Demo() {
        if (this.catfees == null || this.catfees == undefined) {
            this.catfees1 = true;
        } else {
            // this.catfees1 = false;
            // setTimeout(() => {
            //     var formValue: any = cloneDeep(this.Form_AddUser.value);
            //     var formRequest: any = {
            //         //     'OperationType': 'new',
            //         //     'Task': 'savedeal',
            //         //     "AccountId": formValue.AccountId,
            //         //     "AccountKey": formValue.AccountKey,
            //         //     "TypeCode": "deal",
            //         //     "Title": formValue.Title,
            //         //     "Description": formValue.Description,
            //         //     "Terms": formValue.Terms,
            //         //     "StartDate": formValue.StartDate,
            //         //     "EndDate": formValue.EndDate,

            //         //     CodeValidityDays: "10",
            //         //     // CodeValidityStartDate: "2020-11-04 00:00:00",
            //         //     CodeValidityEndDate: "2020-11-14 00:00:00",
            //         //     UsageTypeCode: "hour",

            //         //     "ActualPrice": formValue.ActualPrice,
            //         //     "SellingPrice": formValue.Amount,
            //         //     "DiscountAmount": formValue.DiscountAmount,

            //         //     "DiscountPercentage": 0.0,

            //         //     "Amount": formValue.Amount,
            //         //     "Charge": 0.0,
            //         //     "CommissionAmount": formValue.CommissionAmount,
            //         //     "TotalAmount": 0.0,

            //         //     "MaximumUnitSale": formValue.MaximumUnitSale,
            //         //     "MaximumUnitSalePerDay": formValue.MaximumUnitSalePerDay,
            //         //     "MaximumUnitSalePerPerson": formValue.MaximumUnitSalePerPerson,
            //         //     //"CategoryKey": formValue.CategoryKey,
            //         //     SubCategoryKey:formValue.SubCategoryKey,
            //         //     "Schedule": [
            //         //     ],
            //         //     "Images":[

            //         //     ]
            //     };
            //     if (this.NiraOrPercent.DiscountAmount == '0') { // percentage
            //         formRequest.DiscountPercentage = formValue.DiscountAmount;
            //         formRequest.DiscountAmount = (formValue.DiscountAmount / 100) * formValue.ActualPrice;
            //         formRequest.SellingPrice = formValue.ActualPrice - formRequest.DiscountAmount;
            //         // formRequest.Amount = formRequest.SellingPrice;
            //         this.Form_AddUser.patchValue({ SellingPrice: (formValue.ActualPrice - formRequest.DiscountAmount) });
            //         this.Form_AddUser.patchValue({ Amount: (formValue.ActualPrice - formRequest.DiscountAmount) });
            //     } else if (this.NiraOrPercent.DiscountAmount == '1' && formValue.ActualPrice != 0.0) { // naira
            //         formRequest.DiscountPercentage = Math.round((formValue.DiscountAmount / formValue.ActualPrice) * 100);
            //         formRequest.DiscountAmount = formRequest.DiscountAmount;
            //         //   formRequest.Amount = (formValue.ActualPrice - formRequest.DiscountAmount);
            //         formRequest.SellingPrice = (formValue.ActualPrice - formRequest.DiscountAmount);
            //         this.Form_AddUser.patchValue({ SellingPrice: (formValue.ActualPrice - formRequest.DiscountAmount) });
            //         this.Form_AddUser.patchValue({ Amount: (formValue.ActualPrice - formRequest.DiscountAmount) });
            //     }

            //     if (this.NiraOrPercent.CommissionAmount == '0') {
            //         formRequest.CommissionAmount = (formValue.CommissionAmount / 100) * formValue.ActualPrice;

            //     }
            //     // this.AKAmount = formRequest.Amount - formRequest.CommissionAmount;

            //     this.custsellingpricedis = formValue.ActualPrice - formValue.CommissionAmount;
            //     this.custsellingpricedisper = ((formValue.ActualPrice - formValue.CommissionAmount) / formValue.ActualPrice) * 100;
            //     this.custsellingpricedisper = this.custsellingpricedisper.toFixed(2)

            //     this.merchantamt = (this.catfees / 100) * formValue.CommissionAmount;
            //     // this.merchantamt =((this.catfees /formValue.ActualPrice)*100|| "--");
            //     this.paybletomerchant = (formValue.CommissionAmount - this.merchantamt);



            //     if (this.NiraOrPercent.CommissionAmount == '0') {
            //         formRequest.CommissionAmount = (formValue.CommissionAmount / 100) * formValue.ActualPrice;

            // },

            //     100);




        }
    }
    AKAmount: number = 0;
    ReFormat_RequestBody(): void {

        var formValue: any = cloneDeep(this.Form_AddUser.value);
        var formRequest: any = {
            'OperationType': 'new',
            'Task': 'savedeal',
            "AccountId": formValue.AccountId,
            "AccountKey": formValue.AccountKey,
            "TypeCode": "deal",
            "Title": formValue.Title,
            "Description": formValue.Description,
            "Terms": formValue.Terms,
            "StartDate": formValue.StartDate,
            "EndDate": formValue.EndDate,
            "TitleContent": formValue.TitleContent,
            "TitleTypeId": formValue.TitleTypeId,

            CodeValidityDays: "10",

            CodeValidityEndDate: "2020-11-14 00:00:00",
            UsageTypeCode: "hour",

            "ActualPrice": formValue.ActualPrice,
            // "SellingPrice": formValue.Amount,
            "SellingPrice": formValue.CommissionAmount,
            // "DiscountAmount": formValue.DiscountAmount,
            "DiscountAmount": this.custsellingpricedis,
            'SettlementTypeCode': formValue.SettlementTypeCode,
            'SendNotification': this.SendNotification,
            //"DiscountPercentage": 0.0,
            "DiscountPercentage": this.custsellingpricedisper,

            // "Amount": formValue.Amount,
            "Amount": this.merchantamt,
            "Charge": 0.0,
            // "CommissionAmount": formValue.CommissionAmount,
            "CommissionAmount": this.merchantamt,
            "TotalAmount": 0.0,
            "DiscountTypeCode": this.NiraOrPercent.DiscountAmount == '0' ? "percent" : "amount",
            "MaximumUnitSale": formValue.MaximumUnitSale,
            // "MaximumUnitSalePerDay": formValue.MaximumUnitSalePerDay,
            "MaximumUnitSalePerPerson": formValue.MaximumUnitSalePerPerson,
            // "CategoryKey": formValue.CategoryKey,
            "SubCategoryKey": formValue.SubCategoryKey,
            "Schedule": [
            ],
            "Images": null,
        };
        if (this.NiraOrPercent.DiscountAmount == '0') { // percentage
            if (formValue.DiscountAmount < 0 && formValue.DiscountAmount > 100) {
                this._HelperService.NotifyError("Discount percentage must be between 0 to 100");
            }
            else {
                formRequest.DiscountPercentage = formValue.DiscountAmount;
                formRequest.DiscountAmount = (formValue.DiscountAmount / 100) * formValue.ActualPrice;
                formRequest.SellingPrice = formValue.ActualPrice - formRequest.DiscountAmount;
                formRequest.Amount = formRequest.SellingPrice;
                this.Form_AddUser.patchValue({ SellingPrice: (formValue.ActualPrice - formRequest.DiscountAmount) });
                this.Form_AddUser.patchValue({ Amount: (formValue.ActualPrice - formRequest.DiscountAmount) });
            }

        } else if (this.NiraOrPercent.DiscountAmount == '1' && formValue.ActualPrice != 0.0) { // naira
            formRequest.DiscountPercentage = Math.round((formValue.DiscountAmount / formValue.ActualPrice) * 100);
            formRequest.DiscountAmount = formRequest.DiscountAmount;
            formRequest.Amount = (formValue.ActualPrice - formRequest.DiscountAmount);
            formRequest.SellingPrice = (formValue.ActualPrice - formRequest.DiscountAmount);
            this.Form_AddUser.patchValue({ SellingPrice: (formValue.ActualPrice - formRequest.DiscountAmount) });
            this.Form_AddUser.patchValue({ Amount: (formValue.ActualPrice - formRequest.DiscountAmount) });
        }
        if (this.NiraOrPercent.CommissionAmount == '0') {
            formRequest.CommissionAmount = (formValue.CommissionAmount / 100) * formValue.ActualPrice;
            // this.Form_AddUser.patchValue({CommissionAmount : formRequest.CommissionAmount})
        }
        else if (this.NiraOrPercent.CommissionAmount == '1') {
            formRequest.CommissionAmount = formRequest.CommissionAmount;
        }


        // if (this._HelperService._Icon_Cropper_Data.Content != null) {
        //     formRequest.Images = [
        //         {
        //             "ImageContent": this._HelperService._Icon_Cropper_Data,
        //             "IsDefault": 1
        //         }
        //     ];
        // }

        // if (this._HelperService._Icon_Cropper_Data.Content != null) {
        // formRequest.Images = [
        //   {
        //         "ImageContent": this.Images,
        //         //"IsDefault": 1
        //     }
        // ];

        // formRequest.Images = [
        //     {
        //         "ImageContent":  this.images1,
        //     }


        // ]
        // this.images2.push({ ImageContent: this.images });
        formRequest.Images = this.images


        // }

        //#endregion

        //#region Set Title 

        switch (formValue.DealTitle) {
            case '1': {
                formRequest.Title =
                    formValue.ActualPrice + " " + formRequest.TitleContent + ' for ' + formValue.CommissionAmount + ' at ' + this.SelectedMerchant.DisplayName;
                // formRequest.TitleContent =
                //     formValue.ActualPrice + " " + formRequest.Title + ' for ' + formValue.Amount + ' at ' + this.SelectedMerchant.DisplayName;
                formRequest.TitleTypeId = 1;
                // var a = document.getElementById("titleone").innerHTML;
                // formRequest.Title = a;
            }

                break;
            case '2': {


                // if (this.NiraOrPercent.DiscountAmount == '0') {
                //     formRequest.Title = Math.round(((formValue.DiscountAmount / 100) * formValue.ActualPrice)) + ' off on ' + formRequest.TitleContent + ' at ' + this.SelectedMerchant.DisplayName;
                // } else if (this.NiraOrPercent.DiscountAmount == '1') {
                //     formRequest.Title = formValue.DiscountAmount + ' off on ' + formRequest.TitleContent + ' at ' + this.SelectedMerchant.DisplayName;
                // }

                if (this.NiraOrPercent.DiscountAmount == '0') {
                    formRequest.Title = this.custsellingpricedis + ' off on ' + formRequest.TitleContent + ' at ' + this.SelectedMerchant.DisplayName;
                } else if (this.NiraOrPercent.DiscountAmount == '1') {
                    formRequest.Title = this.custsellingpricedis + ' off on ' + formRequest.TitleContent + ' at ' + this.SelectedMerchant.DisplayName;
                }
                formRequest.TitleTypeId = 2;
            }

                break;
            case '3': {

                // if (this.NiraOrPercent.DiscountAmount == '0') {
                //     formRequest.Title = Math.round(formValue.DiscountAmount) + '% off on ' + formRequest.TitleContent + ' at ' + this.SelectedMerchant.DisplayName;
                // } else if ((this.NiraOrPercent.DiscountAmount == '1' && formValue.ActualPrice != 0)) {
                //     formRequest.Title = Math.round(((formValue.DiscountAmount / formValue.ActualPrice) * 100)) + '% off on ' + formRequest.TitleContent + ' at ' + this.SelectedMerchant.DisplayName;
                // }

                if (this.NiraOrPercent.DiscountAmount == '0') {
                    formRequest.Title = this.custsellingpricedisper + '% off on ' + formRequest.TitleContent + ' at ' + this.SelectedMerchant.DisplayName;
                } else if ((this.NiraOrPercent.DiscountAmount == '1' && formValue.ActualPrice != 0)) {
                    formRequest.Title = this.custsellingpricedisper + '% off on ' + formRequest.TitleContent + ' at ' + this.SelectedMerchant.DisplayName;
                }

                formRequest.TitleTypeId = 3;
            }

                break;

            default:
                break;
        }

        //#endregion 

        //#region Add store locations 

        if (!this.IsAllStores) {

            formRequest.Locations = [
            ];

            for (let index = 0; index < this.StoresList.length; index++) {
                const element = this.StoresList[index];

                formRequest.Locations.push({
                    ReferenceId: element.ReferenceId,
                    ReferenceKey: element.ReferenceKey
                });
            }
        }

        //#endregion

        //#region Add Redeem schedule days 

        for (let index = 0; index < this.WeekDays.length; index++) {
            const element = this.WeekDays[index];

            if (element.selected) {
                // formRequest.Schedule[0].DayOfWeek.push(element.name);
                formRequest.Schedule.push({ DayOfWeek: index });
            }

        }

        //#endregion

        //#region Set Redeem Schedule Timing 

        for (let index = 0; index < formRequest.Schedule.length; index++) {

            if (this.IsValidAllTime) {
                formRequest.Schedule[index].StartHour = '00:00';
                formRequest.Schedule[index].EndHour = '23:59';
                formRequest.Schedule[index].Type = 'dealredeemshedule';
            } else {
                formRequest.Schedule[index].StartHour = moment(this.ValidTimings.Start).format('hh:mm');
                formRequest.Schedule[index].EndHour = moment(this.ValidTimings.End).format('hh:mm');
                formRequest.Schedule[index].Type = 'dealredeemshedule';
            }
        }

        //#endregion

        //#region Set Schedule 

        for (let index = 0; index < 7; index++) {

            if (this.IsRunAllTime) {
                formRequest.Schedule.push({
                    DayOfWeek: index,
                    StartHour: '00:00',
                    EndHour: '23:59',
                    Type: 'dealshedule'
                });
            } else {
                formRequest.Schedule.push({
                    DayOfWeek: index,
                    StartHour: moment(this.DealTimings.Start).format('hh:mm'),
                    EndHour: moment(this.DealTimings.End).format('hh:mm'),
                    Type: 'dealshedule'
                });

            }
        }

        //#endregion

        //#region Set All Stores 



        //#endregion

        //#region Set Coupon Validity 

        switch (formValue.CouponValidity) {
            case '0': {
                formRequest.CodeValidityDays = this.Expiry.Days * 24;
                formRequest.UsageTypeCode = 'hour'
            }

                break;
            case '1': {
                formRequest.CodeValidityDays = this.Expiry.Hours;
                formRequest.UsageTypeCode = 'hour'
            }

                break;
            case '2': {
                formRequest.CodeValidityEndDate = moment(formRequest.EndDate);
                formRequest.UsageTypeCode = 'date';
            }

                break;
            case '3': {
                formRequest.CodeValidityEndDate = moment(this.Expiry.Date);
                formRequest.UsageTypeCode = 'date';
            }

                break;

            default:
                break;
        }

        //#endregion
        return formRequest;

    }
    //#region Run Time 
    IsRunAllTime: boolean = true;
    RunAllTimeToogle(): void {
        this.IsRunAllTime = !(this.IsRunAllTime);
    }
    //#endregion

    //#region Redeem Time 
    IsValidAllTime: boolean = true;
    ValidAllTimeToogle(): void {
        this.IsValidAllTime = !(this.IsValidAllTime);
    }

    //#endregion

    //#region All Stores 

    IsAllStores: boolean = true;
    AllStoresToogle(): void {
        this.IsAllStores = !(this.IsAllStores);
    }

    //#endregion

    IsNoDaySelected: boolean = false;
    toogleWeekDay(index: number): void {
        this.WeekDays[index].selected = !this.WeekDays[index].selected;

        for (let i = 0; i < this.WeekDays.length; i++) {
            const element = this.WeekDays[i];
            if (element.selected) {
                this.IsNoDaySelected = false;
                return;
            }
        }

        this.IsNoDaySelected = true;
    }

    ChangeExpiryDate(event): void {
        this.Expiry.Date = event.end;
    }

    removeImage(): void {
        this.CurrentImagesCount = 0;
        this._HelperService.Icon_Crop_Clear();
    }

    RouteDealDetails(number) {
        // this.DuplicateDealDetails[number].ReferenceKey = number.ReferenceKey
        // this.DuplicateDealDetails[number].ReferenceId = number.ReferenceId
        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveDeal,
            {
                ReferenceKey: this.DuplicateDealDetails[number].ReferenceKey,
                ReferenceId: this.DuplicateDealDetails[number].ReferenceId,
                AccountKey: this.Form_AddUser.controls['AccountKey'].value,
                AccountId: this.Form_AddUser.controls['AccountId'].value,
            }
        );


        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Analytics,
            this.DuplicateDealDetails[number].ReferenceKey,
            this.DuplicateDealDetails[number].ReferenceId,
            this.Form_AddUser.controls['AccountId'].value,
            this.Form_AddUser.controls['AccountKey'].value

        ]);
    }

    RouteEditDealDetails() {
        // this.DuplicateDealDetails[number].ReferenceKey = number.ReferenceKey
        // this.DuplicateDealDetails[number].ReferenceId = number.ReferenceId
        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveDeal,
            {
                ReferenceKey: this._DealDetails.ReferenceKey,
                ReferenceId: this._DealDetails.ReferenceId,
                AccountKey: this.DuplicateAccountKey,
                AccountId: this.DuplicateAccountId,
            }
        );


        this._HelperService.CloseModal('exampleModal');

        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Analytics,
            this._DealDetails.ReferenceKey,
            this._DealDetails.ReferenceId,
            this.DuplicateAccountId,
            this.DuplicateAccountKey

        ]);
    }
    //#region check duplicate name 
    Titles: any = [
        {
            "Title": null
        },
        {
            "Title": null
        },
        {

            "Title": null
        },
    ];
    DuplicateDealDetails: any = []

    changetitle() {
        this.CheckDuplicate();
    }
    public TrimTitle: string = "dunny";
    CheckDuplicate() {
        if (this.MerchantNameCheck && this.ActualPriceCheck && this.DiscountAmountCheck && this.DealTitleNameCheck) {
            this._HelperService.IsFormProcessing = true;
            var pData = {
                Task: this._HelperService.AppConfig.Api.ThankUCash.ValidateDealTitle,
                AccountId: this.Form_AddUser.controls['AccountId'].value,
                AccountKey: this.Form_AddUser.controls['AccountKey'].value,
                Titles: []


            }
            // this.Form_AddUser.controls['Title'].setValue(this.Form_AddUser.controls['Title'].value.trim())

            this.Titles[0].Title =
                this.Form_AddUser.controls['ActualPrice'].value + " " + this.TrimTitle + ' for ' + this.Form_AddUser.controls['Amount'].value + ' at ' + this.SelectedMerchant.DisplayName;

            if (this.NiraOrPercent.DiscountAmount == '0') {
                this.Titles[1].Title = Math.round(((this.Form_AddUser.controls['DiscountAmount'].value / 100) * this.Form_AddUser.controls['ActualPrice'].value)) + ' off on ' + this.TrimTitle + ' at ' + this.SelectedMerchant.DisplayName;
            } else if (this.NiraOrPercent.DiscountAmount == '1') {
                this.Titles[1].Title = this.Form_AddUser.controls['DiscountAmount'].value + ' off on ' + this.TrimTitle + ' at ' + this.SelectedMerchant.DisplayName;
            }

            if (this.NiraOrPercent.DiscountAmount == '0') {
                this.Titles[2].Title = Math.round(this.Form_AddUser.controls['DiscountAmount'].value) + '% off on ' + this.TrimTitle + ' at ' + this.SelectedMerchant.DisplayName;
            } else if ((this.NiraOrPercent.DiscountAmount == '1' && this.Form_AddUser.controls['ActualPrice'].value != 0)) {
                this.Titles[2].Title = Math.round(((this.Form_AddUser.controls['DiscountAmount'].value / this.Form_AddUser.controls['ActualPrice'].value) * 100)) + '% off on ' + this.TrimTitle + ' at ' + this.SelectedMerchant.DisplayName;
            }







            let _OResponse: Observable<OResponse>;
            pData.Titles = this.Titles;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.IsFormProcessing = false;
                        this.DealNameExist1 = !_Response.Result.Titles[0].IsAvailable;
                        this.DealNameExist2 = !_Response.Result.Titles[1].IsAvailable;
                        this.DealNameExist3 = !_Response.Result.Titles[2].IsAvailable;
                        this.DuplicateDealDetails = _Response.Result.Titles;
                    }
                    else {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                }
            );
        }


    }

    //multiple file code--
    myFiles: any[] = [];
    Images: any[] = [];
    myFiles1: string[] = [];
    sMsg: string = '';
    sub_cat_ind: any
    image1: any;
    image2: any; image3: any;
    _Icon_Cropper_Data1: OStorageContent = {
        Name: null,
        Content: null,
        Extension: null,
        TypeCode: null,
        Height: 400,
        Width: 800
    };

    getFileDetails(e) {
        //this.myFiles.push(this._HelperService._Icon_Cropper_Data)
        //  this.Images.push(this._HelperService._Icon_Cropper_Data)
        //this._HelperService._Icon_Cropper_Data1  = cloneDeep(this._HelperService._Icon_Cropper_Data);
        // this._HelperService._Icon_Cropper_Data2=this._HelperService._Icon_Cropper_Data1;
        //   this._HelperService._Icon_Cropper_Data2=this._HelperService._Icon_Cropper_Data1;
        //   this._HelperService._Icon_Cropper_Data3=this._HelperService._Icon_Cropper_Data2;

        this.Images.push({ ImageContent: this._HelperService._Icon_Cropper_Data });
        //  this.Images.push({ ImageContent: this._HelperService._Icon_Cropper_Data1},{ ImageContent: this._HelperService._Icon_Cropper_Data2},{ ImageContent: this._HelperService._Icon_Cropper_Data3});
        // this.sub_cat_ind = this.Subcategories.findIndex(el => el['Name'] === e.target.files.IconName);
        //   this.sub_cat_ind=this.myFiles;
    }



    getFileDetails2(e, value) {

        if (value == 0) {
            this.image1 = this._HelperService._Icon_Cropper_Data;
            this.Images.push({ ImageContent: this.image1 });
        }
        else if (value == 1) {
            this.image2 = this._HelperService._Icon_Cropper_Data;
            this.Images.push({ ImageContent: this.image2 });
            //  this._HelperService.Icon_Crop_Clear();
        }
        else {
            this.image1 = this._HelperService._Icon_Cropper_Data;
            this.Images.push({ ImageContent: this.image1 });
        }
        this._HelperService.Icon_Crop_Clear();
    }
    images = [];
    onFileChange1(event) {
        if (event.target.files && event.target.files[0]) {
            var filesAmount = event.target.files.length;
            for (let i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = (event: any) => {
                    this.images.push(event.target.result);

                    this.Form_AddUser.patchValue({
                        Images: this.images
                    });
                }

                reader.readAsDataURL(event.target.files[i]);
            }
        }
    }

    ufile() {
        for (let i = 0; i < this.Images.length; i++) {
            this.myFiles.push(this.image1)
        }
    }

    uploadFiles() {
        // const frmData = new FormData();
        // for (var i = 0; i < this.myFiles.length; i++) { 
        //   frmData.append("fileUpload", this.myFiles[i]);
        // }


        for (var i = 0; i < this.myFiles.length; i++) {
            // this.myFiles.push(e.target.files[i],);
            this.Images.push(this.myFiles[i],);

        }
    }


    getFileDetails1(e) {
        // this.sub_cat_ind = this.Subcategories.findIndex(el => el['Name'] === e.target.files.IconName);

        //for (var i = 0; i < e.target.files.length; i++) { 
        // this.myFiles.push(e.target.files[i],);
        this.myFiles.push(this._HelperService._Icon_Cropper_Data)

        // }
        this.sub_cat_ind = this.myFiles;
        this.Images = this.myFiles;
    }


    onFileChange(event: any) {
        if (event.target.files && event.target.files[0]) {
            var filesAmount = event.target.files.length;
            for (let i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = (event: any) => {
                    this._HelperService._Icon_Cropper_Data.Content = event.target.result.replace("data:image/png;base64,", "");
                    // event.target.result=event.target.result.replace("data:image/png;base64,", "")
                    //  this.images.push(event.target.result);
                    this.images.push({
                        ImageContent: event.target.result.replace("data:image/png;base64,", "").replace("data:image/jpg;base64,", "")
                            .replace("data:image/jpeg;base64,", "")
                            .replace("data:image/gif;base64,", "")
                    })

                    this.Form_AddUser.patchValue({
                        fileSource: this.images
                    });
                }

                reader.readAsDataURL(event.target.files[i]);
            }
        }
        // this.Images.push({ ImageContent: this._HelperService._Icon_Cropper_Data });
        // for(let j=0;j<this.images.length;j++)
        // {
        //     this._HelperService._Icon_Cropper_Data=this.images[j];
        //     this.Images.push({ ImageContent: this._HelperService._Icon_Cropper_Data });
        // }
        // this.Images.push({ ImageContent: this._HelperService._Icon_Cropper_Data });
        this.Images = this.images;
    }

    images1 = [];
    images2 = [];
    onFileChange4(event) {

        if (this.images.length > 2) {
            // this._HelperService.NotifySwalError("Sorry",
            // "You can added maximun 3 images only.");

            this._HelperService.NotifyError("You can added maximun 3 images only.");
        }
        else {
            if (event.target.files && event.target.files[0]) {
                var filesAmount = event.target.files.length;
                for (let i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = (event: any) => {
                        // this.images.push(event.target.result); 
                        this.images1.push(event.target.result);
                        this._HelperService._Icon_Cropper_Data.Content = event.target.result.replace("data:image/png;base64,", "")
                            .replace("data:image/jpg;base64,", "")
                            .replace("data:image/jpeg;base64,", "")
                            .replace("data:image/gif;base64,", "");

                        this.images.push({ ImageContent: { Name: "", Content: this._HelperService._Icon_Cropper_Data.Content, Extension: "", TypeCode: "", Height: "400", Width: "800" } })
                        this.Form_AddUser.patchValue({
                            fileSource: this.images
                        });
                    }
                    // this.images2.push({ImageContent: {
                    //     Name:"",Content:this._HelperService._Icon_Cropper_Data.Content,Extension:"",TypeCode:"",Height:"400",Width:"800"
                    //      }})
                    reader.readAsDataURL(event.target.files[i]);
                }
            }
        }
    }




    RemoveElementFromStringArray(element: string, element1: string) {
        this.images1.forEach((value, index) => {
            if (value == element) this.images1.splice(index, 1);
        });
        this.images.forEach((value, index) => {
            if (value == element1) this.images.splice(index, 1);
        });
    }


    imageError: string;
    isImageSaved: boolean;
    cardImageBase64: string;

    removeImage1(i) {
        this.images.splice(i, 1);
        this.images1.splice(i, 1);
    }


    removeItem(value) {

        const index: number = this.images.indexOf(value);

        this.images.splice(index, 1);

    }

    resetimage(imageName: any) {
        for (var i = this.images.length; i--;) {
            if (this.images[i].imageChecked) {
                this.images.splice(i, 1);
            }
            if (this.images1[i].imageChecked) {
                this.images1.splice(i, 1);
            }
        }
    }

    removeallimages() {
        this.images = [];
        this.images1 = [];
    }

    removeoneimg(url) {
    }

    removeSelectedFile(index) {
        // Delete the item from fileNames list
        this.images.splice(index, 1);
        // delete file from FileList
        this.images1.splice(index, 1);
    }

    //end multiple file---



    //#endregion
}

import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
declare var moment: any;
import { InputFileComponent, InputFile } from 'ngx-input-file';
declare var $: any;
import * as cloneDeep from 'lodash/cloneDeep';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { createArrayBindingPattern } from 'typescript';
import { isNull, isUndefined } from 'util';

@Component({
    selector: 'tu-tuexternalurl',
    templateUrl: './tuexternalurl.component.html',
})
export class TUExternalUrlComponent implements OnInit {

    public isUpdate: boolean = false;
    public isStartStopPromotion: boolean = false;
    public _SubAccountDetails: any =
        {
            "Amount": null,
            "PurchaseDate": null,
            "ExpiaryDate": null,
            "RedeemDate": null,
            "RedeemLocationId": null,
            "RedeemLocationKey": null,
            "RedeemLocationName": null,
            "RedeemLocationAddress": null,
            "ItemCode": null,
            "StatusCode": null,
            "StatusName": null,
            "Merchant": {
                "ReferenceId": null,
                "ReferenceKey": null,
                "DisplayName": null,
                "IconUrl": null
            },
            "Customer": {
                "ReferenceId": null,
                "ReferenceKey": null,
                "Name": null,
                "MobileNumber": null,
                "ImageUrl": null
            },
            "Deal": {
                "ReferenceId": null,
                "ReferenceKey": null,
                "Title": null,
                "ImageUrl": null
            }
        }


    _DealConfig =
        {

            Images: [],
            StartDateConfig: {
            },
            EndDateConfig: {
            },
            DefaultStartDate: null,
            DefaultEndDate: null,
            DealImages: [],
            StartDate: null,
            EndDate: null,
        }


    _ImageManager =
        {
            TCroppedImage: null,
            ActiveImage: null,
            ActiveImageName: null,
            ActiveImageSize: null,
            Option: {
                MaintainAspectRatio: "true",
                MinimumWidth: 800,
                MinimumHeight: 400,
                MaximumWidth: 800,
                MaximumHeight: 400,
                ResizeToWidth: 800,
                ResizeToHeight: 400,
                Format: "jpg",
            }
        }



    private InputFileComponent_Term: InputFileComponent;

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _FilterHelperService: FilterHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
    ) {

    }
    @ViewChild("offCanvas") divView: ElementRef;
    public ResetFilterControls: boolean = true;
    public AcquirerId = 0;
    currentdate: any;
    currentdate1: any;
    ngOnInit() {
        Feather.replace();
        this._HelperService.ValidateData();
        this.currentdate = moment().format('DD-MM-YYYY hh:mm'),
            this.currentdate1 = moment().format('DD-MM-YYYY'),
            this._HelperService.StopClickPropogation();
        this.InitBackDropClickEvent();
        this.Form_Promote_Load();
        this.TUTr_Setup();
        this.isUpdate = this._HelperService.SystemName.includes("dealpromotionupdate");
        this.isStartStopPromotion = this._HelperService.SystemName.includes("stopdealpromotion");
        this.Form_EditUser_Load();
        this.Form_ManagePromote_Load();


        //deal date
        this._DealConfig.DefaultStartDate = moment();
        this._DealConfig.DefaultEndDate = moment().add(1, 'days').endOf("day");
        this._DealConfig.StartDate = this._DealConfig.DefaultStartDate;
        this._DealConfig.EndDate = this._DealConfig.DefaultEndDate;
        this._DealConfig.StartDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            parentEl: '#addpromotion_startdate',
            startDate: this._HelperService.DateInUTC(moment()),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            parentEl: '#addpromotion_enddate',
            startDate: this._HelperService.DateInUTC(moment()),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        //end deal date


    }
    InitBackDropClickEvent(): void {
        var backdrop: HTMLElement = document.getElementById("backdrop");
        backdrop.onclick = () => {
            $(this.divView.nativeElement).removeClass('show');
            backdrop.classList.remove("show");
        };
    }

    MerchantRoute(ReferenceData) {
        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveMerchant,
            {
                ReferenceKey: ReferenceData.MerchantKey,
                ReferenceId: ReferenceData.MerchantId,
                DisplayName: ReferenceData.MerchantDisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
            }
        );
        //#endregion
        //#region Set Active Reference Key To Current Merchant 
        this._HelperService.AppConfig.ActiveMerchantReferenceKey = ReferenceData.MerchantKey;
        this._HelperService.AppConfig.ActiveMerchantReferenceId = ReferenceData.MerchantId;

        //#endregion

        //#region navigate 

        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.RewardPercentage,
            ReferenceData.MerchantKey,
            ReferenceData.MerchantId,
        ]);

    }


    RouteCustomer(ReferenceData) {
        //#region Save Current Merchant To Storage 

        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveCustomer,
            {
                ReferenceKey: ReferenceData.AccountKey,
                ReferenceId: ReferenceData.AccountId,
                DisplayName: ReferenceData.DisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.Customer,
            }
        );

        //#endregion

        //#region Set Active Reference Key To Current Merchant 

        this._HelperService.AppConfig.ActiveReferenceKey =
            ReferenceData.AccountKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.AccountId;

        //#endregion

        //#region navigate 

        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.CustomerPanel.Accounts.WalletInfo,
            ReferenceData.AccountKey,
            ReferenceData.AccountId,
        ]);

        //#endregion

    }
    TUTr_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
    TUTr_InvoiceRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
    TUTr_InvoiceRange_OnChange(changeContext: ChangeContext): void {
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        this.TUTr_InvoiceRangeMinAmount = changeContext.value;
        this.TUTr_InvoiceRangeMaxAmount = changeContext.highValue;
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#region transactions 
    public OverviewData: any = {
        Transactions: 0,
        InvoiceAmount: 0.0
    };
    timeout = null;
    checkvalue: boolean = false;
    public TUTr_Config: OList;
    public TuTr_Columns =
        {
            Status: true,
            Date: true,
            Account: true,
            AccountName: true,
            Mob: true,
            Biller: true,
            Amount: true,
            Source: true,
            PaymentRef: true
        }
    public TuTr_Filters_List =
        {
            Search: false,
            Date: false,
            Sort: false,
            Status: false,
            Biller: false
        }
    TUTr_Setup() {
        this.TUTr_Config = {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.getdealpromotions,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Promote,
            Title: "Deal",
            StatusType: "AppPromots",
            // AccountId: this._HelperService.AppConfig.ActiveAccountId,
            // AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
            // Type: this._HelperService.AppConfig.ListType.SubOwner,
            DefaultSortExpression: "CreateDate desc",
            //SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'dealcode.unused', '=='),
            TableFields: [
                {
                    DisplayName: "Url",
                    SystemName: "Url",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "Url"
                },
                {
                    DisplayName: "Deal Title",
                    SystemName: "DealTitle",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "DealTitle"
                },

                {
                    DisplayName: "Start Date",
                    SystemName: "StartDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date text-right",
                    Show: true,
                    IsDateSearchField: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
                {
                    DisplayName: "End Date",
                    SystemName: "EndDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date text-right",
                    Show: true,
                    IsDateSearchField: false,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
            ]
        };

        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.SoldHistory,
            this.TUTr_Config
        );
        this.TUTr_GetData();

        // if(this.TUTr_Config.EndDate >this.currentdate)
        // {
        //    this.checkvalue=true
        // }


    }
    TuTr_Setup_SearchCondition() {
        this.TuTr_Columns =
        {
            Status: true,
            Date: true,
            Account: true,
            AccountName: true,
            Mob: true,
            Biller: true,
            Amount: true,
            Source: true,
            PaymentRef: true
        }
        var SearchCondition = '';
        this.TuTr_Filters_List =
        {
            Search: true,
            Date: true,
            Sort: true,
            Status: true,
            Biller: true
        }

        return SearchCondition;
    }
    TuTr_Setup_Fields() {
        var TableFields = [];

        TableFields = [
            {
                DisplayName: "Date",
                SystemName: 'CreateDate',
                DataType: this._HelperService.AppConfig.DataType.Date,
                Class: 'td-date',
                Show: true,
                Search: false,
                IsDateSearchField: true,
                Sort: true,
                ResourceId: null,
            },
            {
                DisplayName: "Expiry Date",
                SystemName: 'EndDate',
                DataType: this._HelperService.AppConfig.DataType.Date,
                Class: 'td-date',
                Show: true,
                Search: false,
                IsDateSearchField: true,
                Sort: true,
                ResourceId: null,
            },
            {
                DisplayName: 'Payment Account Number',
                SystemName: 'AccountNumber',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: false,
            },
            {
                DisplayName: 'Payment Account',
                SystemName: 'AccountDisplayName',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: false,
            },
            {
                DisplayName: 'Mobile',
                SystemName: 'AccountMobileNumber',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: true,
            },
            {
                DisplayName: 'Biller',
                SystemName: 'BillerName',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: true,
            },
            {
                DisplayName: 'Amount',
                SystemName: 'Amount',
                DataType: this._HelperService.AppConfig.DataType.Decimal,
                Show: true,
                Search: false,
                Sort: true,
            },
            {
                DisplayName: 'Source',
                SystemName: 'PaymentSource',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Class: '',
                Show: true,
                Search: false,
                Sort: false,
            },
            {
                DisplayName: 'Payment Reference',
                SystemName: 'PaymentReference',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: false,
            },
        ]

        return TableFields;
    }
    TUTr_ToggleOption_Date(event: any, Type: any) {
        this.TUTr_ToggleOption(event, Type);
    }



    TUTr_ToggleOption(event: any, Type: any) {
        if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
            event.data = {
                SalesMin: this.TUTr_InvoiceRangeMinAmount,
                SalesMax: this.TUTr_InvoiceRangeMaxAmount
            }
        }
        if (Type == "date") {
            this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
            this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
        }

        if (event != null) {
            for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                const element = this.TUTr_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }
            }
        }

        this._HelperService.Update_CurrentFilterSnap(
            event,
            Type,
            this.TUTr_Config


        );

        this.TUTr_Config = this._DataHelperService.List_Operations(
            this.TUTr_Config,
            event,
            Type
        );

        if (
            (this.TUTr_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.TUTr_GetData();
        }

    }
    TUTr_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {
            // if (Type == 'date') {
            //     event.start = this._HelperService.DateInUTC(event.start);
            //     event.end = this._HelperService.DateInUTC(event.end);
            // }

            this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

            this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
            this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

            this.TodayStartTime = this.TUTr_Config.StartTime;
            this.TodayEndTime = this.TUTr_Config.EndTime;
            if (event != null) {
                for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                    const element = this.TUTr_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {
                        element.SystemActive = true;
                    }
                    else {
                        element.SystemActive = false;
                    }

                }
            }


            if (
                (this.TUTr_Config.RefreshData == true)
                && this._HelperService.DataReloadEligibility(Type)
            ) {
                this.TUTr_GetData();
            }
        }, this._HelperService.AppConfig.SearchInputDelay);


    }
    TUTr_GetData() {
        // this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.ThankUCash.GetPurchaseHistoryOverview);
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.TUTr_Config = TConfig;

    }

    dealId: any = null;
    dealkey: any = null;

    clicked() {
        $(this.divView.nativeElement).addClass('show');
        var backdrop: HTMLElement = document.getElementById("backdrop");
        backdrop.classList.add("show");
    }
    unclick() {
        $(this.divView.nativeElement).removeClass('show');
        var backdrop: HTMLElement = document.getElementById("backdrop");
        backdrop.classList.remove("show");
    }
    //#endregion

    SetOtherFilters(): void {
        this.TUTr_Config.SearchBaseConditions = [];
        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_Merchant_Selected = 0;
            this.MerchantEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Provider));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_Provider_Selected = 0;
            this.ProvidersEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Store));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_Store_Selected = 0;
            this.StoresEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.TransactionType));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_TransactionType_Selected = 0;
            this.TransTypeEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBank));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_CardBank_Selected = 0;
            this.CardBankEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBrand));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_CardBrand_Selected = 0;
            this.CardBrandEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }

    //#region dropdowns 

    //#region merchant 

    public TUTr_Filter_Merchant_Option: Select2Options;
    public TUTr_Filter_Merchant_Selected = 0;
    TUTr_Filter_Merchants_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,

            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: "AccountTypeCode",
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: "=",
                //     SearchValue: this._HelperService.AppConfig.AccountType.Merchant
                // }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Merchant_Option = {
            placeholder: 'Search By Merchant',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_Merchants_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant
        );
        this.MerchantEventProcessing(event);
    }

    MerchantEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Merchant_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Merchant_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Store_Toggle = true;

        this.TUTr_Filter_Stores_Load();
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
            this.TUTr_Filter_Store_Toggle = false;
        }, 500);
    }

    //#endregion

    //#region stores 

    public TUTr_Filter_Store_Option: Select2Options;
    public TUTr_Filter_Store_Toggle = false;
    public TUTr_Filter_Store_Selected = 0;
    TUTr_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Store_Option = {
            placeholder: 'Search by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Stores_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Store
        );
        this.StoresEventProcessing(event);
    }

    StoresEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion

    //#region Cashiers 

    public TUTr_Filter_Cashier_Option: Select2Options;
    public TUTr_Filter_Cashier_Toggle = false;
    public TUTr_Filter_Cashier_Selected = 0;
    TUTr_Filter_Cashiers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetCashiers,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Cashier_Option = {
            placeholder: 'Search by Cashier',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Cashiers_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Cashier
        );
        this.CashiersEventProcessing(event);
    }

    CashiersEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Cashier_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CashierId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Cashier_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Cashier_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Cashier_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CashierId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Cashier_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Cashier_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CashierId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Cashier_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion


    //#region Terminal Filter
    public TUTr_Filter_Terminal_Option: Select2Options;
    public TUTr_Filter_Terminal_Toggle = false;
    public TUTr_Filter_Terminal_Selected = 0;
    TUTr_Filter_Terminals_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminals,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "TerminalId",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Terminal_Option = {
            placeholder: 'Search by Terminal',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Terminals_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Terminal
        );
        this.TerminalsEventProcessing(event);
    }

    TerminalsEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Terminal_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Terminal_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Terminal_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Terminal_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Terminal_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Terminal_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Terminal_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }
    //#endregion End


    //#region useraccount 

    public TUTr_Filter_UserAccount_Option: Select2Options;
    public TUTr_Filter_UserAccount_Selected = 0;
    TUTr_Filter_UserAccounts_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "MobileNumber",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.AppUser
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_UserAccount_Option = {
            placeholder: 'Search Customer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_UserAccounts_Change(event: any) {
        if (event.value == this.TUTr_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_UserAccount_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_UserAccount_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region bank 

    public TUTr_Filter_Bank_Option: Select2Options;
    public TUTr_Filter_Bank_Selected = 0;
    TUTr_Filter_Banks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
                }]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Bank_Option = {
            placeholder: 'Search By Bank / Acquirer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Banks_Change(event: any) {

        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Bank
        );

        this.BanksEventProcessing(event);
    }

    BanksEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Bank_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Bank_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion

    //#region provider 

    public TUTr_Filter_Provider_Option: Select2Options;
    public TUTr_Filter_Provider_Selected = 0;
    TUTr_Filter_Providers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Provider_Option = {
            placeholder: 'Search By Provider',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Providers_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Provider
        );

        this.ProvidersEventProcessing(event);
    }

    ProvidersEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Provider_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Provider_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion

    //#region issuers 

    public TUTr_Filter_Issuer_Option: Select2Options;
    public TUTr_Filter_Issuer_Selected = 0;
    TUTr_Filter_Issuers_Load() {
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }]
        };


        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
            [
                this._HelperService.AppConfig.AccountType.PosTerminal,
                this._HelperService.AppConfig.AccountType.PGAccount,
                this._HelperService.AppConfig.AccountType.Cashier,
            ], "=");

        if (this.TUTr_Filter_Provider_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
        }
        if (this.TUTr_Filter_Bank_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'BankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'SubOwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '=');
        //_Select.SearchCondition = this._HelperService.GetSearchConditionStrictOr(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Issuer_Option = {
            placeholder: 'Search By Issuer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Issuers_Change(event: any) {
        if (event.value == this.TUTr_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Issuer_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Issuer_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region transactionType 

    public TUTr_Filter_TransactionType_Option: Select2Options;
    public TUTr_Filter_TransactionType_Selected = 0;
    TUTr_Filter_TransactionTypes_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionType
                },
                {
                    SystemName: "SubParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionTypeReward
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_TransactionType_Option = {
            placeholder: 'Search By Tran. Type',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_TransactionTypes_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.TransactionType
        );

        this.TransTypeEventProcessing(event);
    }

    TransTypeEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region cardbrands 

    public TUTr_Filter_CardBrand_Option: Select2Options;
    public TUTr_Filter_CardBrand_Selected = 0;
    TUTr_Filter_CardBrands_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "TypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.CardBrand
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_CardBrand_Option = {
            placeholder: 'Search By Card Brand',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_CardBrands_Change(event: any) {

        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBrand
        );
        this.CardBrandEventProcessing(event);
    }

    CardBrandEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBrand_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBrand_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region cardbank 

    public TUTr_Filter_CardBank_Option: Select2Options;
    public TUTr_Filter_CardBank_Selected = 0;
    TUTr_Filter_CardBanks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields:
                [
                    {
                        SystemName: "ReferenceId",
                        Type: this._HelperService.AppConfig.DataType.Number,
                        Id: true,
                        Text: false,
                    },
                    {
                        SystemName: "Name",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        Id: false,
                        Text: true
                    },
                    {
                        SystemName: "TypeCode",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        SearchCondition: "=",
                        SearchValue: this._HelperService.AppConfig.HelperTypes.CardBank
                    }
                ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_CardBank_Option = {
            placeholder: 'Search By Card Bank',
            ajax: _Transport,
            multiple: false,
            allowClear: true,

        };
    }

    TUTr_Filter_CardBanks_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBank
        );

        this.CardBankEventProcessing(event);
    }

    CardBankEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBank_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBank_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#endregion

    TodayStartTime = null;
    TodayEndTime = null;



    Data: any = {};
    _AccountOverview : any;
    GetAccountOverviewLite(): void {
        this._HelperService.IsFormProcessing = true;
        this.Data = {
            Task: 'getaccountoverview',
            StartTime: this.TodayStartTime,
            EndTime: this.TodayEndTime,
            AccountKey: this._HelperService.UserAccount.AccountKey,
            AccountId: this._HelperService.UserAccount.AccountId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.Data);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    // this._AccountOverview = _Response.Result as OAccountOverview;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }
    AShowCategorySelector = true;
    // ShowCategorySelector = true;
    public S2BusinessCategories = [
        {
            id: 'Promotional Banner',
            text: 'Promotional Banner',
        },
        {
            id: 'Promotional Slider',
            text: 'Promotional Slider',
        },
        {
            id: 'Top Deals',
            text: 'Top Deals'
        },
        {
            id: 'Featured Deals',
            text: 'Featured Deals'
        },
        {
            id: 'Deal Of The Day',
            text: 'Deal Of The Day'
        }
    ];
    public SelectedCategories: any;
    public selectedCategoriesArray = [];
    public selectCategoryError: boolean = false;
    CategoriesSelected(Items) {
        if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
            if (Items.data.length == 0) {
                this.selectedCategoriesArray = Items.value.split(",");
                this.selectCategoryError = false;
            }
            else {
                this.selectedCategoriesArray = [];
                this.selectedCategoriesArray.push(Items.value);

                this.SelectedCategories = Items.value;
                this.selectCategoryError=false;

            }
        }
        else {
            this.SelectedCategories = [];
            this.selectCategoryError = true;
        }
    }
    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();

        this.TUTr_Filter_Merchants_Load();
        this.TUTr_Filter_UserAccounts_Load();
        this.TUTr_Filter_Stores_Load();
        this.TUTr_Filter_Providers_Load();
        this.TUTr_Filter_Issuers_Load();
        this.TUTr_Filter_CardBrands_Load();
        this.TUTr_Filter_TransactionTypes_Load();
        this.TUTr_Filter_CardBanks_Load();

        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();

    }

    SetSearchRanges(): void {
        //#region Invoice 
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('Amount', this.TUTr_Config.SearchBaseConditions);
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }

        //#endregion      

    }

    //#region SalesOverview 

    GetOverviews(ListOptions: any, Task: string): any {
        this._HelperService.IsFormProcessing = true;

        ListOptions.SearchCondition = '';
        ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }


        var pData = {
            Task: Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            AccountId: ListOptions.AccountId,
            AccountKey: ListOptions.AccountKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.OverviewData = _Response.Result as any;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);

            });


    }

    //#endregion

    //#region filterOperations

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        this.SetSalesRanges();
        //#endregion

        this.TUTr_GetData();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_Store(Type, index);
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
        if (Type == 'Time') {
            this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
            this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
        }
        //#region setOtherFilters
        this.SetOtherFilters();
        this.SetSalesRanges();
        //#endregion

        this.TUTr_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            // input: "text",
            html:
                '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
                '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
                '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
            focusConfirm: false,
            preConfirm: () => {
                return {
                    filter: document.getElementById('swal-input1')['value'],
                    private: document.getElementById('swal-input2')['checked']
                }
            },
            // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            // inputAttributes: {
            //   autocapitalize: "off",
            //   autocorrect: "off",
            //   maxLength: "4",
            //   minLength: "4",
            // },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {

                if (result.value.filter.length < 5) {
                    this._HelperService.NotifyError('Enter filter name length greater than 4');
                    return;
                }

                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

                var AccessType: number = result.value.private ? 0 : 1;
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.SoldHistory,
                    AccessType
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();
                this._HelperService.Delete_Filter(this._HelperService.AppConfig.FilterTypeOption.SoldHistory);
                this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
                this.TUTr_GetData();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {

        this.SetSearchRanges();

        this._HelperService.MakeFilterSnapPermanent();
        this.TUTr_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();

        if (ButtonType == 'Sort') {
            $("#TUTr_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#TUTr_fdropdown").dropdown('toggle');
        }
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        this.SetSalesRanges();
        //#endregion
        this.TUTr_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }


    SetSalesRanges(): void {
        this.TUTr_InvoiceRangeMinAmount = this.TUTr_Config.SalesRange.SalesMin;
        this.TUTr_InvoiceRangeMaxAmount = this.TUTr_Config.SalesRange.SalesMax;
    }


    //varrify deal code start---
    Form_EditUser: FormGroup;

    VarifyDealCode() {
        //         $("#workload-selector").val("");
        // $("#workload-selector").trigger("change");
        // $("#workload-selector").select2('val', '');
        this._HelperService.OpenModal("Form_Varify_deal_code");
    }

    VarifyDealdetails() {
        this._HelperService.OpenModal("Form_Deal_Code_Details");
    }

    Form_EditUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_EditUser = this._FormBuilder.group({
            // OperationType: "new",
            //  Task: this._HelperService.AppConfig.Api.Core.UpdateSubAccount,
            ReferenceId: this._HelperService.AppConfig.ActiveSubAccountReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveSubAccountReferenceKey,

            AccountId: this._HelperService.AppConfig.ActiveReferenceId,
            AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
            // RoleId:[7, Validators.required],
            // RoleKey:['admin', Validators.required],


            Name: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(11),
                    Validators.maxLength(11)
                ])
            ]



        });


        // this.Form_EditUser = this._FormBuilder.group({
        //     OperationType: 'new',
        //     Task: 'validatedealcode',
        //     TypeCode: 'deal',
        //     Name: [null, Validators.required,Validators.minLength(2),
        //         Validators.maxLength(11)],

        // });
    }
    Form_EditUser_Clear() {
        this.Form_EditUser.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_EditUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }

    public SaveAccountRequest: any;
    SelectStatusunused = false;
    SelectStatusredeem = false;
    SelectStatusExpired = false;
    SelectStatusBloacked = false;
    Form_EditUser_Process(_FormValue: any) {
        this.SaveAccountRequest = this.ReFormat_RequestBody();
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.SaveAccountRequest);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._SubAccountDetails = _Response.Result;
                    if (this._SubAccountDetails.StatusCode == "dealcode.unused") //  unused
                    {

                        this.SelectStatusunused = true;
                        this.SelectStatusredeem = false;
                        this.SelectStatusExpired = false;
                        this.SelectStatusBloacked = false;
                    }
                    else if (this._SubAccountDetails.StatusCode == "dealcode.used") //  used
                    {

                        this.SelectStatusunused = false;
                        this.SelectStatusredeem = true;
                        this.SelectStatusExpired = false;
                        this.SelectStatusBloacked = false;
                    }
                    else if (this._SubAccountDetails.StatusCode == "dealcode.expired") // expired
                    {
                        this.SelectStatusunused = false;
                        this.SelectStatusredeem = false;
                        this.SelectStatusExpired = true;
                        this.SelectStatusBloacked = false;
                    }
                    else {
                        this.SelectStatusunused = false;
                        this.SelectStatusredeem = false;
                        this.SelectStatusExpired = false;
                        this.SelectStatusBloacked = true;
                    }



                    // this.InputFileComponent_Term.files.pop();
                    this._HelperService.CloseModal("Form_Varify_deal_code");
                    this.VarifyDealdetails();
                    this.Form_EditUser_Clear();
                    // if (_FormValue.OperationType == 'edit') {
                    // }
                    // else if (_FormValue.OperationType == 'close') {
                    //     this.Form_EditUser_Close();
                    // }
                    this.Form_EditUser_Close();


                }


                else {
                    this._HelperService.NotifyError(_Response.Message);

                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    ReFormat_RequestBody(): void {
        var formValue: any = cloneDeep(this.Form_EditUser.value);
        var formRequest: any = {
            'OperationType': 'new',
            'Task': 'validatedealcode',
            //"AccountId": formValue.AccountId,
            // "AccountKey": formValue.AccountKey,
            // "TypeCode": "deal",
            // "Title": formValue.Title,
            // "Description": formValue.Description,
            // "Terms": formValue.Terms,
            // "StartDate": formValue.StartDate,
            "ReferenceCode": formValue.Name,
            Locations: 'Promotional Banner'

            //  CodeValidityDays: "10",

            //  CodeValidityEndDate: "2020-11-14 00:00:00",
            //  UsageTypeCode: "hour",


        };


        //#endregion




        return formRequest;

    }


    Form_EditUser_Show() {
        this._HelperService.OpenModal("Form_EditUser_Content");
    }
    Form_EditUser_Close() {
        // this._Router.navigate([
        //     this._HelperService.AppConfig.Pages.System.AdminUsers
        // ]);
        this._HelperService.CloseModal("Form_EditUser_Content");
    }

    //#endregion

    imageChangedEvent: any = '';
    croppedImage: any = '';
    croppedImage1: any = ''
    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage = event.base64;
        this.croppedImage1 = this.croppedImage.replace("data:image/png;base64,", "")
        this._HelperService._Icon_Cropper_Data.Content = this.croppedImage1;
    }

    //promote deal code start---
    Form_Promote: FormGroup;
    Form_ManagePromote: FormGroup;

    Form_Promote_Show() {
        this._HelperService.Icon_Crop_Clear();
        this.InitImagePicker(this.InputFileComponent_Term);
        this._HelperService.OpenModal("_Icon_Cropper_Modal");
        // this._HelperService.OpenModal("_PreviewGeneral");



    }

    ManageStartDateRangeChange(value) {
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: value.start,
            minDate: value.start,
        };
        this._DealConfig.StartDate = value.start;
        this._DealConfig.EndDate = value.start;
        // this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_ManagePromote.patchValue(
            {
                StartDate: value.start.format('DD-MM-YYYY hh:mm a'),
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    ManageEndDateRangeChange(value) {
        this._DealConfig.EndDate = value.start;

        this.Form_ManagePromote.patchValue(
            {
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }


    Form_Promote_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_Promote = this._FormBuilder.group({
            OperationType: 'new',
            Task: 'savedealpromotion',
            TypeCode: [null, Validators.required],
            Url: [null, Validators.compose([Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.WebsiteUrl)])],
            TypeId: [null, Validators.required],
            Locations: 'Promotional Banner',
            ImageContent: null,
            StartDate: moment().format('DD-MM-YYYY hh:mm a'),
            EndDate: moment().add(1, 'days').endOf("day").format('DD-MM-YYYY hh:mm a'),
        });
    }
    ScheduleStartDateRangeChange(value) {
        this._DealConfig.StartDate = value.start;
        this.Form_Promote.patchValue(
            {
                StartDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }


    ScheduleEndDateRangeChange(value) {
        this._DealConfig.EndDate = value.start;
        this.Form_Promote.patchValue(
            {
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    Form_Promote_Process(_FormValue?: any) {
        if (this.SelectedCategories.length < 1) {
            this._HelperService.NotifyError("Please select visibility location");
        }
        else {
            var _tLoc = '';
            for (let index = 0; index < this.SelectedCategories.length; index++) {
                const element = this.SelectedCategories[index];
                if (index == 0) {
                    _tLoc = element;
                }
                else {
                    _tLoc = _tLoc + "," + element;
                }
            }
            this.Form_Promote.controls.Locations.setValue(_tLoc);

            var _FormValue = this.Form_Promote.value;
            this._HelperService.IsFormProcessing = true;
            // debugger;
            var Request = this.ReFormat_RequestBody1();

            if (_FormValue.Url == undefined || _FormValue.Url == null || _FormValue.Url == "") {
                this._HelperService.NotifyError("Enter url to save promotion");
            }

            // else if (_FormValue.StartDate == undefined) {
            //     this.Form_Promote.patchValue(
            //         {
            //             StartDate: moment(),
            //         }

            //     );
            // }

            // else if (_FormValue.EndDate == undefined) {
            //     this.Form_Promote.patchValue(
            //         {
            //             EndDate: moment(),
            //         }

            //     );
            // }


            else {
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(
                    this._HelperService.AppConfig.NetworkLocation.Console.V3.Promote,
                    Request
                );
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            // this._HelperService.FlashSwalSuccess("New pos terminal has been added successfully",
                            //   "You have successfully created new POS terminal");
                            // this._HelperService.ObjectCreated.next(true);

                            this._HelperService.NotifySuccess(_Response.Message);

                            this.TUTr_Setup();
                            this._DealConfig =
                            {
                                DefaultStartDate: null,
                                DefaultEndDate: null,
                                // SelectedDealCodeHours: 12,

                                StartDate: null,
                                EndDate: null,
                                DealImages: [],

                                Images: [],
                                StartDateConfig: {
                                },
                                EndDateConfig: {
                                },

                            }
                            this._ImageManager =
                            {
                                TCroppedImage: null,
                                ActiveImage: null,
                                ActiveImageName: null,
                                ActiveImageSize: null,
                                Option: {
                                    MaintainAspectRatio: "true",
                                    MinimumWidth: 800,
                                    MinimumHeight: 400,
                                    MaximumWidth: 800,
                                    MaximumHeight: 400,
                                    ResizeToWidth: 800,
                                    ResizeToHeight: 400,
                                    Format: "jpg",
                                }
                            }
                            this.ngOnInit();
                            this._DealConfig.DealImages = [];
                            this.removeImage();
                            this.Form_Promote_Clear();

                            //  this.DealsList_Setup();
                            this._HelperService.CloseModal('PromoteDeal');
                            this._HelperService.IsFormProcessing = false;


                            this.Form_Promote_Clear();
                            this.ResetFilterUI();
                            this._HelperService.Icon_Crop_Clear();

                            this.Form_Promote_Close();
                            if (_FormValue.OperationType == "close") {
                                this.Form_Promote_Close();
                            }
                        } else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        }
    }

    Form_Promote_Clear() {
        this.Form_Promote.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_Promote_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }

    Form_Promote_Close() {
        this._HelperService.CloseModal("PromoteDeal");
        this._HelperService.CloseAllModal();

    }


    ReFormat_RequestBody1(): void {
        var formValue: any = cloneDeep(this.Form_Promote.value);
        var formRequest: any = {
            'OperationType': 'new',
            'Task': 'savedealpromotion',
            "Url": formValue.Url,
            //StartDate: moment(this._DealConfig.StartDate).format('YYYY-MM-DD HH:mm'),
            StartDate: this._HelperService.HCXConvertDate(this._DealConfig.StartDate),
            EndDate: this._HelperService.HCXConvertDate(this._DealConfig.EndDate),
            "TypeCode": 'home',
            "TypeId": 758,
            "Status": "default.active",
            "ImageContent": this._DealConfig.DealImages[0],
            // Locations: 'Promotional Banner'
            Locations: this.Form_Promote.controls.Locations.value
        };
        return formRequest;
    }

    CurrentImagesCount: number = 0;
    private InitImagePicker(InputFileComponent: InputFileComponent) {
        if (InputFileComponent != undefined) {
            this.CurrentImagesCount = 0;
            this._HelperService._InputFileComponent = InputFileComponent;
            InputFileComponent.onChange = (files: Array<InputFile>): void => {
                if (files.length >= this.CurrentImagesCount) {
                    this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
                }
                this.CurrentImagesCount = files.length;
            };
        }
    }

    //#region Image Manager
    onImageAccept(value) {
        setTimeout(() => {
            this.Form_Promote.patchValue(
                {
                    ImageContent: null,
                }
            );
        }, 300);
        this._ImageManager.ActiveImage = value;
        this._ImageManager.ActiveImageName = value.file.name;
        this._ImageManager.ActiveImageName = value.file.size;
    }

    onImageAccept1(value) {
        setTimeout(() => {
            this.Form_ManagePromote.patchValue(
                {
                    ImageContent: null,
                }
            );
        }, 300);
        this._ImageManager.ActiveImage = value;
        this._ImageManager.ActiveImageName = value.file.name;
        this._ImageManager.ActiveImageName = value.file.size;
    }
    Icon_B64Cropped(base64: string) {
        this._ImageManager.TCroppedImage = base64;
    }
    Icon_B64CroppedDone() {
        var ImageDetails = this._HelperService.GetImageDetails(this._ImageManager.TCroppedImage);
        var ImageContent =
        {
            //OriginalContent: this._ImageManager.TCroppedImage,
            Name: this._ImageManager.ActiveImageName,
            Size: this._ImageManager.ActiveImageSize,
            Extension: ImageDetails.Extension,
            Content: ImageDetails.Content
        };
        // if (this._DealConfig.DealImages.length == 0) {
        //     this._DealConfig.DealImages.push(
        //         {
        //             //ImageContent: ImageItem,
        //             ImageContent,
        //             IsDefault: 1,
        //         }
        //     );
        // }
        // else {
        //     this._DealConfig.DealImages.push(
        //         {
        //           //  ImageContent: ImageItem,
        //           ImageContent,
        //             IsDefault: 0,
        //         }
        //     );
        // }


        if (this._DealConfig.DealImages.length == 0) {
            this._DealConfig.DealImages.push(
                {
                    //ImageContent: ImageItem,
                    Name: this._ImageManager.ActiveImageName,
                    Size: this._ImageManager.ActiveImageSize,
                    Extension: ImageDetails.Extension,
                    Content: ImageDetails.Content,
                    IsDefault: 1,
                }
            );
        }
        else {
            this._DealConfig.DealImages.push(
                {
                    //  ImageContent: ImageItem,
                    Name: this._ImageManager.ActiveImageName,
                    Size: this._ImageManager.ActiveImageSize,
                    Extension: ImageDetails.Extension,
                    Content: ImageDetails.Content,
                    IsDefault: 0,
                }
            );
        }


        this._ImageManager.TCroppedImage = null;
        this._ImageManager.ActiveImage = null;
        this._ImageManager.ActiveImageName = null;
        this._ImageManager.ActiveImageSize = null;
    }
    Icon_Crop_Clear() {
        this._ImageManager.TCroppedImage = null;
        this._ImageManager.ActiveImage = null;
        this._ImageManager.ActiveImageName = null;
        this._ImageManager.ActiveImageSize = null;
        this._HelperService.CloseModal('_Icon_Cropper_Modal');
    }
    RemoveImage(Item) {
        this._DealConfig.DealImages = this._DealConfig.DealImages.filter(x => x != Item);
    }

    removeImage(): void {
        //this.CurrentImagesCount = 0;
        this._DealConfig.DealImages = []
    }

    selectCat: any;
    //Edit promoted deal--
    //  Form_ManagePromote:FormGroup;
    ManagePromoteDeal: any = {};
    SelectedDealStartDatePromote: any;
    SelectedDealEndDatePromote: any;
    Selectedurl: any;
    DealTitle: any;
    SelectedDealStartDatePromote1: any;
    SelectedDealEndDatePromote1: any;
    Selectedimage: any;
    ManageAsPromote(ReferenceData: any): void {
        this.ManagePromoteDeal = {};
        this.ManagePromoteDeal.ReferenceKey = ReferenceData.ReferenceKey;
        this.ManagePromoteDeal.ReferenceId = ReferenceData.ReferenceId;
        this.SelectedDealStartDatePromote = ReferenceData.StartDatePart.Date;//this._HelperService.GetDateS(ReferenceData.StartDate);
        this.SelectedDealEndDatePromote = ReferenceData.EndDatePart.Date;// this._HelperService.GetDateS(ReferenceData.EndDate);
        this.Selectedurl = ReferenceData.Url,
            this.DealTitle = ReferenceData.DealTitle,
            this.Selectedimage = ReferenceData.ImageUrl;
        if (!isNull(ReferenceData.Locations) && !isUndefined(ReferenceData.Locations)) {
            this.SelectedCategories = ReferenceData.Locations
            this.selectedCategoriesArray = ReferenceData.Locations.split(",");
        }

        this.SelectedDealStartDatePromote1 = ReferenceData.StartDate;
        this.SelectedDealEndDatePromote1 = ReferenceData.EndDate;
        this.Form_ManagePromote.controls['StartDate'].setValue(ReferenceData.StartDatePart.DateTime);
        this.Form_ManagePromote.controls['EndDate'].setValue(ReferenceData.EndDatePart.DateTime);
        this._HelperService.OpenModal('ManagePromoteDeal');

        //  this.createCatArray(this.selectedCategoriesArray);
    }


    removeCategory(cat: any): void {
        this.selectedCategoriesArray.splice(cat, 1);
        let myVar1 = this.selectedCategoriesArray.join();
        this.SelectedCategories = myVar1;

    }

    Form_ManagePromote_Process(_FormValue?: any) {
        let checkIsArray = Array.isArray(this.SelectedCategories);
        // if (this.SelectedCategories.length < 1) {

        //     this._HelperService.NotifyError("Please select visibility location");
        //     this.Form_ManagePromote.controls.Locations = null;
        // } 
        // else
        if (checkIsArray == false) {
            this.Form_ManagePromote.controls.Locations.setValue(this.SelectedCategories);
        } else if (checkIsArray == true && this.SelectedCategories.length >= 1) {
            var _tLoc = '';
            for (let index = 0; index < this.SelectedCategories.length; index++) {
                const element = this.SelectedCategories[index];
                if (index == 0) {
                    _tLoc = element;
                }
                else {
                    _tLoc = _tLoc + "," + element;
                }
            }
            this.Form_ManagePromote.controls.Locations.setValue(_tLoc);
        }

        if (this.Form_ManagePromote.controls.Locations) {
            var _FormValue = this.Form_ManagePromote.value;

            this._HelperService.IsFormProcessing = true;
            // var Request = this.CreateCashierRequest(_FormValue);
            var Request = this.ReFormat_RequestBody2();
            if (_FormValue.StartDate == this.SelectedDealStartDatePromote) {
                this.Form_ManagePromote.patchValue(
                    {
                        StartDate: this.SelectedDealStartDatePromote1
                    }

                );
            }

            if (_FormValue.EndDate == this.SelectedDealEndDatePromote) {
                this.Form_ManagePromote.patchValue(
                    {
                        EndDate: this.SelectedDealEndDatePromote1
                    }

                );
            }
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(
                this._HelperService.AppConfig.NetworkLocation.Console.V3.Promote,
                Request
            );
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        // this._HelperService.FlashSwalSuccess("New pos terminal has been added successfully",
                        //   "You have successfully created new POS terminal");
                        // this._HelperService.ObjectCreated.next(true);

                        this._HelperService.NotifySuccess(_Response.Message);
                        this.TUTr_Setup();
                        this._HelperService.CloseModal('ManagePromoteDeal');
                        this._HelperService.IsFormProcessing = false;

                        this._DealConfig.DealImages = [];
                        this.Form_ManagePromote_Clear();
                        this.ResetFilterUI();
                        this._HelperService.Icon_Crop_Clear();

                        this.Form_ManagePromote_Close();
                        if (_FormValue.OperationType == "close") {
                            this.Form_ManagePromote_Close();
                        }
                    } else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                }
            );
        }
    }

    Form_ManagePromote_Clear() {
        this.Form_ManagePromote.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_ManagePromote_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }

    Form_ManagePromote_Close() {
        this._HelperService.CloseModal("ManagePromoteDeal");
        this._HelperService.CloseAllModal();

    }

    Form_ManagePromote_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_ManagePromote = this._FormBuilder.group({
            OperationType: 'new',
            Task: 'updatedealpromotion',
            Url: [null, Validators.required],
            DealTitle: [null, Validators.required],
            TypeCode: [null, Validators.required],
            TypeId: [null, Validators.required],
            // CategoryKey: [null, Validators.required],
            Locations: null,
            ImageContent: null,
            StartDate: moment().format('DD-MM-YYYY hh:mm'),
            EndDate: moment().add(1, 'days').endOf("day").format('DD-MM-YYYY hh:mm'),
        });
    }

    urldata: any; dealtitledata: any;
    ReFormat_RequestBody2(): void {
        var formValue: any = cloneDeep(this.Form_ManagePromote.value);
        if (formValue.Url != undefined) {
            this.urldata = formValue.Url
        }
        else {
            this.dealtitledata = formValue.DealTitle
        }
        var formRequest: any = {
            'OperationType': 'new',
            'Task': 'updatedealpromotion',

            "ReferenceId": this.ManagePromoteDeal.ReferenceId,
            "ReferenceKey": this.ManagePromoteDeal.ReferenceKey,
            //   "DealId": this._UserAccount.ReferenceId,
            //   "DealKey": this._UserAccount.ReferenceKey,

            "Url": formValue.Url,
            "DealTitle": formValue.DealTitle,
            StartDate: this._HelperService.HCXConvertDate(this._DealConfig.StartDate),
            EndDate: this._HelperService.HCXConvertDate(this._DealConfig.EndDate),

            "TypeCode": 'home',
            "TypeId": 758,
            "Status": "default.active",
            //"ImageContent": this._HelperService._Icon_Cropper_Data,
            "ImageContent": this._DealConfig.DealImages[0],
            Locations: this.Form_ManagePromote.controls.Locations.value

        };


        return formRequest;

    }

    ScheduleManageStartDateRangeChange(value) {
        this.Form_ManagePromote.patchValue(
            {
                StartDate: value.start,

            }
        );
    }

    ScheduleManageEndDateRangeChange(value) {
        this.Form_ManagePromote.patchValue(
            {
                EndDate: value.start,

            }
        );
    }
    //#endregion

    //stop promotion
    PromoteDeal: any = {};
    detetepromoteId: any;
    detetepromoteKey: any;
    UnMarkAsPromote(ReferenceData: any): void {
        this.PromoteDeal = {};
        this.detetepromoteId = ReferenceData.ReferenceId;
        this.detetepromoteKey = ReferenceData.ReferenceKey;

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.RemovePromoteTitle,
            text: this._HelperService.AppConfig.CommonResource.RemovePromoteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {

                this._HelperService.IsFormProcessing = true;
                var PData =
                {
                    Task: this._HelperService.AppConfig.Api.ThankUCash.deletedealpromotion,
                    ReferenceId: this.detetepromoteId,
                    ReferenceKey: this.detetepromoteKey,
                    // StatusCode: this.selectedStatusItem.statusCode,

                }
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promote, PData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Deal Promotion Deleted Successfully. It will take upto 5 minutes to update changes.");
                            this.TUTr_Setup();
                            // this._HelperService.CloseModal('FlashDeal');
                            this._HelperService.IsFormProcessing = false;

                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    }
                    ,
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                        this._HelperService.ToggleField = false;
                    });

            }
        });

    }


    StopAsPromote(ReferenceData: any): void {

        // this.GetAccountDetails(ReferenceData)
        this.PromoteDeal = {};
        this.detetepromoteId = ReferenceData.ReferenceId;
        this.detetepromoteKey = ReferenceData.ReferenceKey;

        swal({
            position: "center",
            title: "Stop this Promotion?",
            text: "Click on continue button to stop.",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                var _EtLoc = '';
                for (let index = 0; index < this.ESelectedBusinessCategories.length; index++) {
                    const element = this.ESelectedBusinessCategories[index];
                    if (index == 0) {
                        _EtLoc = element;
                    }
                    else {
                        _EtLoc = _EtLoc + "," + element;
                    }
                }


                this._HelperService.IsFormProcessing = true;
                var PData =
                {
                    Task: this._HelperService.AppConfig.Api.ThankUCash.updatedealpromotion,
                    "ReferenceId": ReferenceData.ReferenceId,
                    "ReferenceKey": ReferenceData.ReferenceKey,
                    //"Url": formValue.Url,
                    Locations: _EtLoc,
                    // StartDate: moment(this._DealConfig.StartDate).format('YYYY-MM-DD HH:mm'),
                    //  EndDate: moment(this._DealConfig.EndDate).format('YYYY-MM-DD HH:mm'),
                    StartDate: ReferenceData.StartDate,
                    EndDate: ReferenceData.EndDate,
                    "TypeCode": 'home',
                    "TypeId": 758,
                    "StatusCode": "default.inactive",
                    //"ImageContent": this._HelperService._Icon_Cropper_Data,
                    "ImageContent": this._DealConfig.DealImages[0]
                    // StatusCode: this.selectedStatusItem.statusCode,
                }


                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promote, PData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Deal Promotion Stopped Successfully. It will take upto 5 minutes to update changes.");
                            this.TUTr_Setup();
                            // this._HelperService.CloseModal('FlashDeal');
                            this._HelperService.IsFormProcessing = false;
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    }
                    ,
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                        this._HelperService.ToggleField = false;
                    });

            }
        });

    }

    StartAsPromote(ReferenceData: any): void {
        // this.GetAccountDetails(ReferenceData)
        this.PromoteDeal = {};
        this.detetepromoteId = ReferenceData.ReferenceId;
        this.detetepromoteKey = ReferenceData.ReferenceKey;

        swal({
            position: "center",
            title: "Start this Deal to Promote?",
            text: "Click Continue to Start Promote this Deal",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {


                var _EtLoc = '';
                for (let index = 0; index < this.ESelectedBusinessCategories.length; index++) {
                    const element = this.ESelectedBusinessCategories[index];
                    if (index == 0) {
                        _EtLoc = element;
                    }
                    else {
                        _EtLoc = _EtLoc + "," + element;
                    }
                }


                this._HelperService.IsFormProcessing = true;
                var PData =
                {
                    Task: this._HelperService.AppConfig.Api.ThankUCash.updatedealpromotion,
                    "ReferenceId": ReferenceData.ReferenceId,
                    "ReferenceKey": ReferenceData.ReferenceKey,
                    //"Url": formValue.Url,
                    Locations: _EtLoc,
                    StartDate: moment(this._DealConfig.StartDate).format('YYYY-MM-DD HH:mm'),
                    EndDate: moment(this._DealConfig.EndDate).format('YYYY-MM-DD HH:mm'),

                    "TypeCode": 'home',
                    "TypeId": 758,
                    "StatusCode": "default.active",
                    //"ImageContent": this._HelperService._Icon_Cropper_Data,
                    "ImageContent": this._DealConfig.DealImages[0]
                    // StatusCode: this.selectedStatusItem.statusCode,

                }


                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promote, PData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Deal Promotion Started Successfully. It will take upto 5 minutes to update changes.");
                            this.TUTr_Setup();
                            // this._HelperService.CloseModal('FlashDeal');
                            this._HelperService.IsFormProcessing = false;

                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    }
                    ,
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                        this._HelperService.ToggleField = false;
                    });

            }
        });

    }


    RouteDeal(ReferenceData) {
        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveDeal,
            {
                ReferenceKey: ReferenceData.DealKey,
                ReferenceId: ReferenceData.DealId,
                AccountKey: ReferenceData.AccountKey,
                AccountId: ReferenceData.AccountId,
                DisplayName: ReferenceData.DisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
            }
        );

        this._HelperService.AppConfig.ActiveReferenceKey =
            ReferenceData.DealKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.DealId;
        this._HelperService.AppConfig.ActiveAccountKey =
            ReferenceData.DealKey;
        this._HelperService.AppConfig.ActiveAccountId = ReferenceData.DealId;
        // this._Router.navigate([
        //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Store
        //     .Dashboard,
        //   ReferenceData.ReferenceKey,
        //   ReferenceData.ReferenceId,
        // ]);

        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Analytics,
            ReferenceData.DealKey,
            ReferenceData.DealId,
            ReferenceData.AccountId,
            ReferenceData.AccountKey,

        ]);
    }

    UrlClick(Url) {
        // if (Url.startsWith("234")) {
        //     this._SubAccountDetails.MobileNumber = this._SubAccountDetails.MobileNumber.substring(3, this._SubAccountDetails.length);
        // }

        let url: string = '';
        if (!/^http[s]?:\/\//.test(Url)) {
            url += 'http://';
        }

        url += Url;
        window.open(url, '_blank');


        // window.open(Url, '_blank').focus();
    }


    checkpromoteimage: boolean = false;
    public ESelectedBusinessCategories = [];
    ShowCategorySelector = true;
    GetAccountDetails(ReferenceData: any) {
        this.ESelectedBusinessCategories = [];
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetDeal,
            ReferenceId: ReferenceData.DealId,
            ReferenceKey: ReferenceData.DealKey,
            AccountId: ReferenceData.AccountId,
            AccountKey: ReferenceData.AccountKey
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.IsFormProcessing = false;
                    this._UserAccount = _Response.Result;

                    this.Selectedimage = _Response.Result.DealPromotionImageUrl;

                    if (_Response.Result.DealPromotionImageUrl == _Response.Result.ImageUrl) {
                        this.checkpromoteimage = true;
                    }
                    else {
                        this.checkpromoteimage = false;
                    }






                    this.ShowCategorySelector = false;
                    if (_Response.Result.DealPromotionLocations != undefined && _Response.Result.DealPromotionLocations != null && _Response.Result.DealPromotionLocations != '') {
                        var ter = _Response.Result.DealPromotionLocations.toString();
                        var array = ter.split(",");

                        //code added by me
                        for (var i = 0; i < array.length; i++) {
                            this.ESelectedBusinessCategories.push(array[i]);
                        }
                        //end of the code


                        //code added by harshal sir
                        // if (ter.indexOf(',').length > -1) {
                        //   var Items = ter.split(',');
                        //   if (Items.length > 0) {
                        //     this.ESelectedBusinessCategories.push(Items);
                        //   }
                        // }
                        // else {
                        //   this.ESelectedBusinessCategories.push(ter);
                        // }
                        // end of the code


                        setTimeout(() => {
                            this.ShowCategorySelector = true;
                        }, 300);

                    }



                    this.Form_ManagePromote.controls['StartDate'].setValue(moment(_Response.Result.DealPromotionStartDate).format('DD-MM-YYYY hh:mm a'));
                    this.Form_ManagePromote.controls['EndDate'].setValue(moment(_Response.Result.DealPrmotionEndDate).format('DD-MM-YYYY hh:mm a'));
                    var tStartDate = moment(_Response.Result.DealPromotionStartDate);
                    var tEndDate = moment(_Response.Result.DealPrmotionEndDate);


                    this._DealConfig.StartDate = tStartDate;
                    this._DealConfig.EndDate = tEndDate;


                }
                else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    public _UserAccount: any =
        {
            MerchantDisplayName: null,
            SecondaryEmailAddress: null,
            BankDisplayName: null,
            BankKey: null,
            OwnerName: null,
            SubOwnerAddress: null,
            SubOwnerLatitude: null,
            SubOwnerDisplayName: null,
            SubOwnerKey: null,
            SubOwnerLongitude: null,
            AccessPin: null,
            LastLoginDateS: null,
            AppKey: null,
            AppName: null,
            AppVersionKey: null,
            CreateDate: null,
            CreateDateS: null,
            CreatedByDisplayName: null,
            CreatedByIconUrl: null,
            CreatedByKey: null,
            Description: null,
            IconUrl: null,
            ModifyByDisplayName: null,
            ModifyByIconUrl: null,
            ModifyByKey: null,
            ModifyDate: null,
            ModifyDateS: null,
            PosterUrl: null,
            ReferenceKey: null,
            StatusCode: null,
            StatusI: null,
            StatusId: null,
            StatusName: null,
            AccountCode: null,
            AccountOperationTypeCode: null,
            AccountOperationTypeName: null,
            AccountTypeCode: null,
            AccountTypeName: null,
            Address: null,
            AppVersionName: null,
            ApplicationStatusCode: null,
            ApplicationStatusName: null,
            AverageValue: null,
            CityAreaKey: null,
            CityAreaName: null,
            CityKey: null,
            CityName: null,
            ContactNumber: null,
            CountValue: null,
            CountryKey: null,
            CountryName: null,
            DateOfBirth: null,
            DisplayName: null,
            EmailAddress: null,
            EmailVerificationStatus: null,
            EmailVerificationStatusDate: null,
            FirstName: null,
            GenderCode: null,
            GenderName: null,
            LastLoginDate: null,
            LastName: null,
            Latitude: null,
            Longitude: null,
            MobileNumber: null,
            Name: null,
            NumberVerificationStatus: null,
            NumberVerificationStatusDate: null,
            OwnerDisplayName: null,
            OwnerKey: null,
            Password: null,
            Reference: null,
            ReferralCode: null,
            ReferralUrl: null,
            RegionAreaKey: null,
            RegionAreaName: null,
            RegionKey: null,
            RegionName: null,
            RegistrationSourceCode: null,
            RegistrationSourceName: null,
            RequestKey: null,
            RoleKey: null,
            RoleName: null,
            SecondaryPassword: null,
            SystemPassword: null,
            UserName: null,
            WebsiteUrl: null,

            StateKey: null,
            StateName: null,


            DealPromotionId: null,
            DealPromotionKey: null,
            DealPromotionStartDate: null,
            DealPrmotionEndDate: null,

        }
}
export class OAccountOverview {
    public TotalTransactions?: number;
    public TotalSale?: number;
    public DeadTerminals?: number;
    public IdleTerminals?: number;
    public TerminalStatus?: number;
    public Total?: number;
    public Idle?: number;
    public Active?: number;
    public Dead?: number;
    public Inactive?: number;
    public UnusedTerminals?: number;
    public Merchants: number;
    public ActiveMerchants: number;
    public ActiveMerchantsDiff: number;
    public Terminals: number;
    public ActiveTerminals: number;
    public ActiveTerminalsDiff: number;
    public Transactions: number;
    public TransactionsDiff: number;
    public PurchaseAmount: number;
    public PurchaseAmountDiff: number;
    public CashRewardPurchaseAmount: number;
    public CashRewardPurchaseAmountDiff: number;
    public CardRewardPurchaseAmount: number;
    public CardRewardPurchaseAmountDiff: number;
}

export class OCoreUsage {

    public UserRewardAmount: number;
    public BankAccountNumber: number;
    public Charge: number;
    public TotalAmount: number;
    public MerchantAmount: number;
    public ToAccountId: number;
    public ToAccountMobileNumber: number;
    public AccountDisplayName: string;
    public ToAccountDisplayName: string;
    public Title: string;
    public BankName: string;
    public ReferenceNumber: string;
    public CreatedByDisplayName: string;
    public BankAccountName: string;
    public ModifyByDisplayName: string;
    public SystemComment: string;
    public ProductCategoryName: string;
    public ProductItemName: string;
    public RewardAmount: number;
    public Amount: number;
    public CommisonAmount: number;
    public AccountMobileNumber: number;
    public PaymentReference: string;
    public StatusB: string;
    Coordinates?: any;
    public ReferenceId: number;
    public AccountId: number;
    public AccountKey: string;
    public Reference: string;
    public ReferenceKey: string;
    public AppOwnerKey: string;
    public AppOwnerName: string;
    public AppKey: string;
    public AppName: string;
    public ApiKey: string;
    public ApiName: string;
    public AppVersionKey: string;
    public AppVersionName: string;
    public FeatureKey: string;
    public FeatureName: string;
    public UserAccountKey: string;
    public UserAccountDisplayName: string;
    public UserAccountIconUrl: string;
    public UserAccountTypeCode: string;
    public UserAccountTypeName: string;
    public SessionId: string;
    public SessionKey: string;
    public IpAddress: string;
    public Latitude: string;
    public Longitude: string;
    public Request: string;
    public Response: string;
    public RequestTime: Date;
    public StartDate: Date;
    public ModifyDate: Date;
    public EndDate: Date;
    public CreateDate: Date;
    public ResponseTime: Date;
    public ProcessingTime: string;
    public StatusId: number;
    public StatusCode: string;
    public StatusName: string;
}






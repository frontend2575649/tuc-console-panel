import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { Select2Module } from 'ng2-select2';
import { TimepickerModule } from 'ngx-bootstrap';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
// import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ImageCropperModule } from 'ngx-image-cropper';
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { NgxPaginationModule } from 'ngx-pagination';
import { DuplicateDealComponent } from './tuduplicatedeal.component';


const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 1,
};
const routes: Routes = [
  { path: '', component: DuplicateDealComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUDuplicateDealRoutingModule { }
@NgModule({
  declarations: [DuplicateDealComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    GooglePlaceModule,
    ImageCropperModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
    }),
    TUDuplicateDealRoutingModule,
    InputFileModule.forRoot(config),
    TimepickerModule.forRoot()
    // LeafletModule
  ]
})
export class DuplicateDealModule { }

import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as cloneDeep from 'lodash/cloneDeep';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { InputFile, InputFileComponent } from 'ngx-input-file';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OResponse, OSelect } from "../../../../service/service";
;
declare var $: any;
declare var google: any;
declare var moment: any;

@Component({
    selector: 'app-tuduplicatedeal',
    templateUrl: './tuduplicatedeal.component.html',
    styleUrls: []
})
export class DuplicateDealComponent implements OnInit {
    public SaveAccountRequest: any;
    public value: any;

    public showPickers: boolean = true;
    public merchantPlaceholder: string = "Select Merchant";
    public categoryPlaceholder: string = "Select Category";

    public NiraOrPercent: any = {
        DiscountAmount: "1",
        CommissionAmount: "1"
    }

    DealTimings: any = {
        Start: new Date(),
        End: new Date()
    }

    ValidTimings: any = {
        Start: new Date(),
        End: new Date()
    }

    Expiry: any = {
        Hours: 0,
        Days: 0,
        Date: new Date()
    }

    public WeekDays: any[] = [
        { name: 'Monday', selected: true },
        { name: 'Tuesday', selected: true },
        { name: 'Wednesday', selected: true },
        { name: 'Thursday', selected: true },
        { name: 'Friday', selected: true },
        { name: 'Saturday', selected: true },
        { name: 'Sunday', selected: true }
    ]

    @ViewChild(InputFileComponent)
    private InputFileComponent_Term: InputFileComponent;

    CurrentImagesCount: number = 0;

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {
    }


    ngOnInit() {

        $('[data-toggle="tooltip"]').tooltip();

        if (this.InputFileComponent_Term != undefined) {
            this.CurrentImagesCount = 0;
            this._HelperService._InputFileComponent = this.InputFileComponent_Term;
            this.InputFileComponent_Term.onChange = (files: Array<InputFile>): void => {
                if (files.length >= this.CurrentImagesCount) {
                    this._HelperService._SetFirstImageOrNone(this.InputFileComponent_Term.files);
                }
                this.CurrentImagesCount = files.length;
            };
        }

        this.Form_EditUser_Load();
        this.GetRoles_List();
        this.GetCategories_List();
        this.GetMerchants_List();
        this.GetStores_List();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];

            this._HelperService.AppConfig.ActiveAccountKey = params["accountkey"];
            this._HelperService.AppConfig.ActiveAccountId = params["accountid"];

            this.GetDealDetails();
        });
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    public GetRoles_Option: Select2Options;
    public GetRoles_Transport: any;
    GetRoles_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreParameters,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'Name',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
        this.GetRoles_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetRoles_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetRoles_Transport,
            multiple: false,
        };
    }
    GetRoles_ListChange(event: any) {
        this.Form_EditUser.patchValue(
            {
                RoleKey: event.value
            }
        );
    }

    public GetMerchants_Option: Select2Options;
    public GetMerchants_Transport: any;
    public SelectedMerchant: any = {};
    GetMerchants_List() {
        var PlaceHolder = this.merchantPlaceholder;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ]
        }

        this.GetMerchants_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetMerchants_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetMerchants_Transport,
            multiple: false,
        };
    }
    GetMerchants_ListChange(event: any) {

        this.SelectedMerchant = event.data[0];
        this.Form_EditUser.patchValue(
            {
                AccountKey: event.data[0].ReferenceKey,
                AccountId: event.data[0].ReferenceId
            }
        );



    }

    public GetCategories_Option: Select2Options;
    public GetCategories_Transport: any;
    GetCategories_List() {
        var PlaceHolder = this.categoryPlaceholder;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommons,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceKey",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: 'StatusCode',
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: '=',
                //     SearchValue: this._HelperService.AppConfig.Status.Active,
                // }
            ]
        }


        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'TypeCode', this._HelperService.AppConfig.DataType.Text,
            [
                this._HelperService.AppConfig.HelperTypes.MerchantCategories,

            ]
            , '=');
        this.GetCategories_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetCategories_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetCategories_Transport,
            multiple: false,
        };


    }
    GetCategories_ListChange(event: any) {
        this.Form_EditUser.patchValue(
            {
                CategoryKey: event.data[0].ReferenceKey

            }
        );
    }

    public GetStores_Option: Select2Options;
    public GetStores_Transport: any;
    public StoresList: any = []
    GetStores_List() {
        var PlaceHolder = "Select Stores";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: 'StatusCode',
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: '=',
                //     SearchValue: this._HelperService.AppConfig.Status.Active,
                // }
            ]
        }


        this.GetStores_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetStores_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetStores_Transport,
            multiple: true,
        };
    }
    GetStores_ListChange(event: any) {
        this.StoresList = event.data
    }

    ScheduleStartDateRangeChange(value) {

        this.Form_EditUser.patchValue(
            {
                StartDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
            }
        );
        this.Form_EditUser.patchValue(
            {
                StartDate: value.start,

            }
        );
    }

    ScheduleEndDateRangeChange(value) {

        this.Form_EditUser.patchValue(
            {
                EndDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
            }
        );
        this.Form_EditUser.patchValue(
            {
                EndDate: value.start,
            }
        );

    }

    _CurrentAddress: any = {};
    Form_EditUser: FormGroup;
    Form_EditUser_Address: string = null;
    Form_EditUser_Latitude: number = 0;
    Form_EditUser_Longitude: number = 0;
    @ViewChild('places') places: GooglePlaceDirective;
    Form_EditUser_PlaceMarkerClick(event) {
        this.Form_EditUser_Latitude = event.coords.lat;
        this.Form_EditUser_Longitude = event.coords.lng;
    }
    public Form_EditUser_AddressChange(address: Address) {
        this.Form_EditUser_Latitude = address.geometry.location.lat();
        this.Form_EditUser_Longitude = address.geometry.location.lng();
        this.Form_EditUser_Address = address.formatted_address;
        this.Form_EditUser.controls['Latitude'].setValue(this.Form_EditUser_Latitude);
        this.Form_EditUser.controls['Longitude'].setValue(this.Form_EditUser_Longitude);
        this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
        this.Form_EditUser.controls['Address'].setValue(address.name + ',' + address.formatted_address);
    }
    Form_EditUser_Show() {
    }
    Form_EditUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deals]);
    }
    Form_EditUser_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_EditUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: 'savedeal',
            TypeCode: 'deal',
            MaximumUnitSale: [null, Validators.required],
            EndDate: null,
            StartDate: null,
            CouponValidity: null
        });
    }
    Form_EditUser_Clear() {
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 300);
        this.Form_EditUser_Latitude = 0;
        this.Form_EditUser_Longitude = 0;
        this.Form_EditUser.reset();
        this._HelperService.Icon_Crop_Clear();
        this.Form_EditUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_DuplicateUser_Process(_FormValue: any, IsDraft: boolean) {
        this._HelperService.IsFormProcessing = true;

        this.SaveAccountRequest = this.ReFormat_RequestBody();
        this.SaveAccountRequest.StatusCode = 'deal.published';

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.SaveAccountRequest);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess('Deal duplication successful.');
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    //#region DealDetails 

    public _UserAccount: any = {
        ContactNumber: null,
        SecondaryEmailAddress: null,
        ReferenceId: null,
        BankDisplayName: null,
        BankKey: null,
        SubOwnerAddress: null,
        SubOwnerLatitude: null,
        SubOwnerDisplayName: null,
        SubOwnerKey: null,
        SubOwnerLongitude: null,
        AccessPin: null,
        LastLoginDateS: null,
        AppKey: null,
        AppName: null,
        AppVersionKey: null,
        CreateDate: null,
        CreateDateS: null,
        CreatedByDisplayName: null,
        CreatedByIconUrl: null,
        CreatedByKey: null,
        Description: null,
        IconUrl: null,
        ModifyByDisplayName: null,
        ModifyByIconUrl: null,
        ModifyByKey: null,
        ModifyDate: null,
        ModifyDateS: null,
        PosterUrl: null,
        ReferenceKey: null,
        StatusCode: null,
        StatusI: null,
        StatusId: null,
        StatusName: null,
        AccountCode: null,
        AccountOperationTypeCode: null,
        AccountOperationTypeName: null,
        AccountTypeCode: null,
        AccountTypeName: null,
        Address: null,
        AppVersionName: null,
        ApplicationStatusCode: null,
        ApplicationStatusName: null,
        AverageValue: null,
        CityAreaKey: null,
        CityAreaName: null,
        CityKey: null,
        CityName: null,
        CountValue: null,
        CountryKey: null,
        CountryName: null,
        DateOfBirth: null,
        DisplayName: null,
        EmailAddress: null,
        EmailVerificationStatus: null,
        EmailVerificationStatusDate: null,
        FirstName: null,
        GenderCode: null,
        GenderName: null,
        LastLoginDate: null,
        LastName: null,
        Latitude: null,
        Longitude: null,
        MobileNumber: null,
        Name: null,
        NumberVerificationStatus: null,
        NumberVerificationStatusDate: null,
        OwnerDisplayName: null,
        OwnerKey: null,
        Password: null,
        Reference: null,
        ReferralCode: null,
        ReferralUrl: null,
        RegionAreaKey: null,
        RegionAreaName: null,
        RegionKey: null,
        RegionName: null,
        RegistrationSourceCode: null,
        RegistrationSourceName: null,
        RequestKey: null,
        RoleKey: null,
        RoleName: null,
        SecondaryPassword: null,
        SystemPassword: null,
        UserName: null,
        WebsiteUrl: null
    };

    public _DealDetails: any =
        {
        }

    GetDealDetails() {
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetDeal,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveAccountId,
            AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.IsFormProcessing = false;
                    this._DealDetails = _Response.Result;

                    // this._DealDetails.StartDate = this._HelperService.GetDateTimeS(
                    //     this._DealDetails.StartDate
                    //   );

                    // this._DealDetails.EndDate = this._HelperService.GetDateTimeS(
                    //     this._DealDetails.EndDate
                    //   );

                    this._DealDetails.StartDate = this._HelperService.GetDateS(
                        this._DealDetails.StartDate
                    );

                    this._DealDetails.EndDate = this._HelperService.GetDateS(
                        this._DealDetails.EndDate
                    );

                    this.merchantPlaceholder = this._DealDetails.AccountDisplayName;
                    this.categoryPlaceholder = this._DealDetails.CategoryName;

                    this.showPickers = false;
                    this._ChangeDetectorRef.detectChanges();

                    this.GetCategories_List();
                    this.GetMerchants_List();

                    this.showPickers = true;
                    this._ChangeDetectorRef.detectChanges();

                }
                else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    //#endregion

    printform(): void {
        this.ReFormat_RequestBody();
    }

    ReFormat_RequestBody(): void {
        var formValue: any = cloneDeep(this._DealDetails);
        var formRequest: any = {
            'OperationType': 'new',
            'Task': 'savedeal',
            "AccountId": formValue.AccountId,
            "AccountKey": formValue.AccountKey,
            "TypeCode": "deal",
            "Title": formValue.Title,
            "Description": formValue.Description,
            "Terms": formValue.Terms,
            "StartDate": formValue.StartDate,
            "EndDate": formValue.EndDate,

            CodeValidityDays: "10",
            CodeValidityEndDate: "2020-11-14 00:00:00",
            UsageTypeCode: "hour",

            "ActualPrice": formValue.ActualPrice,
            "SellingPrice": formValue.Amount,
            "DiscountAmount": formValue.DiscountAmount,
            'SettlementTypeCode': formValue.SettlementTypeCode,

            "DiscountPercentage": 0.0,

            "Amount": formValue.Amount,
            "Charge": 0.0,
            "CommissionAmount": formValue.CommissionAmount,
            "TotalAmount": 0.0,
            "DiscountTypeCode": this.NiraOrPercent.DiscountAmount == '0' ? "percent" : "amount",
            "MaximumUnitSale": formValue.MaximumUnitSale,
            "MaximumUnitSalePerDay": formValue.MaximumUnitSalePerDay,
            "MaximumUnitSalePerPerson": formValue.MaximumUnitSalePerPerson,
            "CategoryKey": formValue.CategoryKey,
            "Schedule": [
            ]
        };

        //#region Set Redeem Schedule Timing 

        for (let index = 0; index < formRequest.Schedule.length; index++) {

            if (this.IsValidAllTime) {
                formRequest.Schedule[index].StartHour = '00:00';
                formRequest.Schedule[index].EndHour = '23:59';
                formRequest.Schedule[index].Type = 'dealredeemshedule';
            } else {
                formRequest.Schedule[index].StartHour = moment(this.ValidTimings.Start).format('hh:mm');
                formRequest.Schedule[index].EndHour = moment(this.ValidTimings.End).format('hh:mm');
                formRequest.Schedule[index].Type = 'dealredeemshedule';
            }
        }

        //#endregion

        //#region Set Schedule 

        for (let index = 0; index < 7; index++) {

            if (this.IsRunAllTime) {
                formRequest.Schedule.push({
                    DayOfWeek: index,
                    StartHour: '00:00',
                    EndHour: '23:59',
                    Type: 'dealshedule'
                });
            } else {
                formRequest.Schedule.push({
                    DayOfWeek: index,
                    StartHour: moment(this.DealTimings.Start).format('hh:mm'),
                    EndHour: moment(this.DealTimings.End).format('hh:mm'),
                    Type: 'dealshedule'
                });

            }
        }

        //#endregion


        return formRequest;

    }

    //#region Run Time 

    IsRunAllTime: boolean = true;
    RunAllTimeToogle(): void {
        this.IsRunAllTime = !(this.IsRunAllTime);
    }

    //#endregion

    //#region Redeem Time 

    IsValidAllTime: boolean = true;
    ValidAllTimeToogle(): void {
        this.IsValidAllTime = !(this.IsValidAllTime);
    }

    //#endregion

    //#region All Stores 

    IsAllStores: boolean = true;
    AllStoresToogle(): void {
        this.IsAllStores = !(this.IsAllStores);
    }

    //#endregion

    toogleWeekDay(index: number): void {
        this.WeekDays[index].selected = !this.WeekDays[index].selected;
    }

    ChangeExpiryDate(event): void {
        this.Expiry.Date = event.end;
    }

    removeImage(): void {
        this.CurrentImagesCount = 0;
        this._HelperService.Icon_Crop_Clear();
    }


}

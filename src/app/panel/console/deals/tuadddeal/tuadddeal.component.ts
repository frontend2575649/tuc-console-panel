import { Component, OnInit, ViewChild, ChangeDetectorRef, AfterContentChecked, OnDestroy, ViewRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from "../../../../service/service";;
import swal from 'sweetalert2';
declare var $: any;
import * as Feather from 'feather-icons';
declare var google: any;
declare var moment: any;
import * as cloneDeep from 'lodash/cloneDeep';
import { InputFileComponent, InputFile } from 'ngx-input-file';
import * as _ from 'lodash';
import { Select2OptionData } from 'ng2-select2';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { MatDialog } from '@angular/material';
import { FileManagerComponent } from '../../../../../app/modules/file-manager/file-manager.component';

import { CdkDragDrop, CdkDragEnter, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
    selector: 'app-tuadddeal',
    templateUrl: './tuadddeal.component.html'
})


export class AddDealComponent implements OnInit {
    SelectedImage:any;
    dealType;
    isStorePickup = true;
    isDeliverToAddress = false;
    Type_Data = ['box', "envelope", "soft-packaging"];
    packageData: any = {};
    Package;

    public ckConfig =
        {
            toolbar: ['Bold', 'NumberedList', 'BulletedList'],
            height: 300
        }
    public Editor = ClassicEditor;

    _ImageManager =
        {
            TCroppedImage: null,
            ActiveImage: null,
            ActiveImageName: null,
            ActiveImageSize: null,
            Option: {
                MaintainAspectRatio: "false",
                MinimumWidth: 600,
                MinimumHeight: 500,
                MaximumWidth: 600,
                MaximumHeight: 500,
                ResizeToWidth: 600,
                ResizeToHeight: 500,
                Format: "jpeg",
            }
        }
    public Hours = [];
    public Days = [];



    _DealConfig =
        {
            DefaultStartDate: null,
            DefaultEndDate: null,
            SelectedDealCodeHours: 12,
            SelectedDealCodeDays: 90,
            SelectedDealCodeEndDate: null,
            DealCodeValidityTypeCode: 'daysafterpurchase',
            StartDate: null,
            EndDate: null,
            DealImages: [],
            SelectedTitle: 3,
            Images: [],
            StartDateConfig: {
            },
            EndDateConfig: {
            },
            DealCodeEndDateConfig: {
            }
        }
    Form_AddDeal: FormGroup;
    Form_AddPackage: FormGroup;

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _dialog: MatDialog,
    ) {
    }
    ngOnDestroy(): void {
        setTimeout(() => {
            if (this._ChangeDetectorRef && !(this._ChangeDetectorRef as ViewRef).destroyed) {
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }
    ngAfterContentChecked(): void {
        this._ChangeDetectorRef.detectChanges();
    }

    ngOnInit(): void {
        for (let index = 1; index < 24; index++) {
            this.Hours.push(index);
        }
        for (let index = 1; index < 366; index++) {
            this.Days.push(index);
        }
        this._DealConfig.DefaultStartDate = moment();
        this._DealConfig.DefaultEndDate = moment().add(3, 'weeks').endOf("day");
        this._DealConfig.StartDate = this._DealConfig.DefaultStartDate;
        this._DealConfig.EndDate = this._DealConfig.DefaultEndDate;
        this._DealConfig.StartDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            parentEl: '#adddeal_startdate',
            startDate: this._HelperService.DateInUTC(moment()),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            parentEl: '#adddeal_enddate',
            startDate: this._HelperService.DateInUTC(moment()),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        this._DealConfig.DealCodeEndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            parentEl:'#dealexpireCode_date',
            startDate: this._DealConfig.EndDate,
            minDate: this._DealConfig.EndDate,
        };
        this.Form_AddUser_Load();
        this.GetDealCategories_List();
        this.GetMerchants_List();
        this.getCategorieslist();
        this.Form_AddDeal_Load();
        this.Form_AddPackage_Load();
    }

    openDialog() {
        this._HelperService.OpenModal('Form_Add_Deal');
    }

    Form_AddDeal_Close() {
        this._HelperService.CloseModal("Form_AddDeal");
    }
    Form_AddDeal_Load() {

        this.Form_AddDeal = this._FormBuilder.group({
            Title: [
                null,
            ],
            Description: [
                null,
                Validators.compose([
                    Validators.required
                ])
            ],
            Type: [
                null
            ],
            Height: [
                null
            ],
            Weight: [
                null,
                Validators.compose([
                    Validators.required,
                ])
            ],
            Length: [
                null,
            ],
            Width: [
                null
            ],
        });
    }

    Form_AddPackage_Load() {
        this.Form_AddPackage = this._FormBuilder.group({
            Title: [null,],
            Type: [null, Validators.compose([Validators.required])],
            Height: [null, Validators.compose([Validators.required, Validators.min(1)])],
            Weight: [null, Validators.compose([Validators.required, Validators.min(0)])],
            Length: [null, Validators.compose([Validators.required, Validators.min(1)])],
            Width: [null, Validators.compose([Validators.required, Validators.min(1)])],
        });
    }

    public packagingData: any = {};

    Form_AddDeal_Save() {
        const data = this.Form_AddDeal.value;
        this.packagingData = data;
        this._HelperService.CloseModal("Form_Add_Deal");
    }

    //#region  Form Manager
    Form_AddUser_Images = [];
    Form_AddUser: FormGroup;
    Form_AddUser_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_AddUser = this._FormBuilder.group({
            SubCategoryKey: [null, Validators.required],
            TitleTypeId: ["3", Validators.required], //transient
            TitleContent: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            Description: [null, Validators.compose([Validators.required, Validators.minLength(80), Validators.maxLength(5048)])],
            Terms: [null],
            MaximumUnitSale: [null, Validators.compose([Validators.required, Validators.min(1)])],
            MaximumUnitSalePerPerson: [null, Validators.compose([Validators.required, Validators.min(0)])],
            ActualPrice: [null, Validators.compose([Validators.required, Validators.min(1)])],
            SellingPrice: [null, Validators.compose([Validators.required, Validators.min(1)])],
            TUCPercentage: null,
            StartDate: this._DealConfig.DefaultStartDate.format('DD-MM-YYYY hh:mm a'),
            EndDate: this._DealConfig.DefaultEndDate.format('DD-MM-YYYY hh:mm a'),
            CodeUsageTypeCode: "daysafterpurchase", //transient
            TImage: null,
            CodeValidityEndDate: this._DealConfig.DefaultEndDate.format('DD-MM-YYYY hh:mm a'),
        });
    }
    isSaveinDraft:boolean=false;
    isPublishDeal:boolean=false;

    Form_AddUser_Process(_FormValue: any, IsDraft: boolean) {
        if (this.dealType == undefined || this.dealType == null) {
            this._HelperService.NotifyError("Select Deal type");
            return;
        }
        if (this.isDeliverToAddress && this.Form_AddDeal.value.Weight == undefined && this.Form_AddDeal.value.Weight < 0.1) {
            this._HelperService.NotifyError("Please enter weight");
            return;
        }
        if (this.isDeliverToAddress && this.Form_AddDeal.value.Description == undefined && this.Form_AddDeal.value.Description == "") {
            this._HelperService.NotifyError("Please enter package description");
            return;
        }
        if (this.SelectedMerchant == undefined || this.SelectedMerchant == null) {
            this._HelperService.NotifyError("Select deal merchant");
            return;
        }
        else if (this._SelectedSubCategory == undefined || this._SelectedSubCategory == null || this._SelectedSubCategory.ReferenceId == undefined || this._SelectedSubCategory.ReferenceId == null || this._SelectedSubCategory.ReferenceId == 0) {
            this._HelperService.NotifyError("Select deal category");
            return;
        }
        else if (_FormValue.ActualPrice == undefined || _FormValue.ActualPrice == null || _FormValue.ActualPrice == undefined || _FormValue.ActualPrice == 0) {
            this._HelperService.NotifyError("Enter original price");
            return;
        }
        else if (_FormValue.SellingPrice == undefined || _FormValue.SellingPrice == null || _FormValue.SellingPrice == undefined || _FormValue.SellingPrice == 0) {
            this._HelperService.NotifyError("Enter selling price");
            return;
        }
        else if (!this.isStorePickup && !this.isDeliverToAddress) {
            this._HelperService.NotifyError("Select at least one Delivery Option");
            return;
        }
        else if (_FormValue.SellingPrice <= 0) {
            this._HelperService.NotifyError("Actual price must be greater than 0");
            return;
        }
        else if (_FormValue.ActualPrice <= 0) {
            this._HelperService.NotifyError("Selling price must be greater than or equal 0");
            return;
        }
        else if (_FormValue.MaximumUnitSalePerPerson > _FormValue.MaximumUnitSale) {
            this._HelperService.NotifyError("Maximum purchase per person must be less than maximum purchase");
            return;
        }
        else if (parseFloat(_FormValue.SellingPrice) > parseFloat(_FormValue.ActualPrice)) {
            this._HelperService.NotifyError("Selling price must be less than or equal to actual price");
            return;
        }
        else if (this._DealConfig.DealImages == undefined || this._DealConfig.DealImages == null || this._DealConfig.DealImages.length == 0) {
            this._HelperService.NotifyError("Atleast 1 image is required for adding deal");
            return;
        }
        else if (this._DealConfig.StartDate == undefined || this._DealConfig.StartDate == null) {
            this._HelperService.NotifyError("Deal start date required");
            return;
        }
        else if (this._DealConfig.EndDate == undefined || this._DealConfig.EndDate == null) {
            this._HelperService.NotifyError("Deal end date required");
            return;
        }
        else {
            var Title = this._Titles.Title3;
            if (this._DealConfig.SelectedTitle == 1) {
                Title = this._Titles.Title1;
            }
            else if (this._DealConfig.SelectedTitle == 2) {
                Title = this._Titles.Title2;
            }
            else {
                Title = this._Titles.Title3;
            }

            var _PostData: any =
            {
                Task: "savedeal",
                AccountId: this.SelectedMerchant.ReferenceId,
                AccountKey: this.SelectedMerchant.ReferenceKey,
                Title: Title,
                TitleContent: _FormValue.TitleContent,
                TitleTypeId: _FormValue.TitleTypeId,
                Description: _FormValue.Description,
                Terms: _FormValue.Terms,

                StartDate: this._HelperService.HCXConvertDate(this._DealConfig.StartDate),
                EndDate: this._HelperService.HCXConvertDate(this._DealConfig.EndDate),
                CodeUsageTypeCode: _FormValue.CodeUsageTypeCode,
                CodeValidityDays: this._DealConfig.SelectedDealCodeDays,
                CodeValidityHours: this._DealConfig.SelectedDealCodeHours,
                CodeValidityEndDate: null,
                ActualPrice: _FormValue.ActualPrice,
                SellingPrice: _FormValue.SellingPrice,
                MaximumUnitSale: _FormValue.MaximumUnitSale,
                MaximumUnitSalePerPerson: _FormValue.MaximumUnitSalePerPerson,
                SubCategoryKey: this._SelectedSubCategory.ReferenceKey,
                StatusCode: 'deal.draft',
                SendNotification: false,
                Images: [],
                Locations: [],
                Shedule: [],
                DealTypeCode: this.dealType
            };
            if (this.dealType === 'dealtype.product') {

                if (this.isStorePickup && this.isDeliverToAddress) {
                    _PostData.DeliveryTypeCode = 'deliverytype.instoreanddelivery'
                }
                if (this.isDeliverToAddress && !this.isStorePickup) {
                    _PostData.DeliveryTypeCode = 'deliverytype.delivery'
                }
                if (this.isStorePickup && !this.isDeliverToAddress) {
                    _PostData.DeliveryTypeCode = 'deliverytype.instore'
                }
            }
            if (this.dealType === 'dealtype.product' && this.isDeliverToAddress) {
                _PostData.Packaging = {};
                _PostData.Packaging.Name = this.Form_AddDeal.value.Description;
                _PostData.Packaging.ParcelDescription = this.Form_AddDeal.value.Description;
                // _PostData.Packaging.Type = this.Form_AddDeal.value.Type;
                // _PostData.Packaging.Height = this.Form_AddDeal.value.Height || 1;
                _PostData.Packaging.Weight = this.Form_AddDeal.value.Weight;
                // _PostData.Packaging.Length = this.Form_AddDeal.value.Length || 1;
                // _PostData.Packaging.Width = this.Form_AddDeal.value.Width || 1;
                // _PostData.Packaging.Size_unit = 'cm';
                // _PostData.Packaging.Weight_unit = 'kg';
                // if (this.packagingData.selectedProduct) {
                //     _PostData.Packaging.PackagingId = this.Package;
                //     _PostData.Packaging.ParcelDescription = this.Form_AddDeal.value.Description;
                // }
                // if (this.subCategoryData.length > 0) {
                //     _PostData.Packaging.ReferenceId = this.selectedSubCategory.ReferenceId;
                //     _PostData.Packaging.ReferenceKey = this.selectedSubCategory.ReferenceKey;
                // } else {
                //     _PostData.Packaging.ReferenceId = this.packagingData.selectedProduct.ReferenceId;
                //     _PostData.Packaging.ReferenceKey = this.packagingData.selectedProduct.ReferenceKey;
                // }
            }
            if (this._AmountDistribution.TUCPercentage != this._AmountDistribution.OrignalTUCPercentage) {
                _PostData.TUCPercentage = this._AmountDistribution.TUCPercentage;
            }
            if (_PostData.CodeUsageTypeCode == 'dealenddate') {
                _PostData.CodeValidityEndDate = this._DealConfig.EndDate;
            }
            if (_PostData.CodeUsageTypeCode == 'date') {
                _PostData.CodeValidityEndDate = this._HelperService.HCXConvertDate(this._DealConfig.SelectedDealCodeEndDate);
            }
            //dealenddate
            // if (_FormValue.CodeValidityEndDate == _FormValue.EndDate) {
            //    // moment(this._DealConfig.EndDate).format('YYYY-MM-DD HH:mm');
            // }
            // else {
            //     _PostData.CodeValidityEndDate = this._HelperService.HCXConvertDate(this._DealConfig.SelectedDealCodeEndDate) ;// moment(this._DealConfig.SelectedDealCodeEndDate).format('YYYY-MM-DD HH:mm')
            // }

            this._DealConfig.DealImages.forEach(element => {
                // element.OriginalContent = null;
                element.ImageContent.OriginalContent = null;
                _PostData.Images.push(element);
            });
            if (IsDraft) {
                _PostData.StatusCode = 'deal.draft';
            } else {
                _PostData.StatusCode = 'deal.published';
            }
            this._HelperService.IsFormProcessing = true;

            if (IsDraft){
                this.isSaveinDraft=true;
                this.isPublishDeal=false;
            }
            else
            {
                this.isPublishDeal=true;
                this.isSaveinDraft=false;
            }
            
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, _PostData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        if (IsDraft) {
                            this._HelperService.NotifySuccess('Deal has been saved as draft.');
                        } else {
                            this._HelperService.NotifySuccess('Deal created successfully. It will take upto 5 minutes to update changes.');
                        }
                        this.Form_AddUser.reset();
                        this._DealConfig =
                        {
                            DefaultStartDate: null,
                            DefaultEndDate: null,
                            SelectedDealCodeHours: 12,
                            SelectedDealCodeDays: 30,
                            SelectedDealCodeEndDate: 0,
                            DealCodeValidityTypeCode: 'dealenddate',
                            StartDate: null,
                            EndDate: null,
                            DealImages: [],
                            SelectedTitle: 3,
                            Images: [],
                            StartDateConfig: {
                            },
                            EndDateConfig: {
                            },
                            DealCodeEndDateConfig: {
                            }
                        }
                        this._ImageManager =
                        {
                            TCroppedImage: null,
                            ActiveImage: null,
                            ActiveImageName: null,
                            ActiveImageSize: null,
                            Option: {
                                MaintainAspectRatio: "false",
                                MinimumWidth: 800,
                                MinimumHeight: 500,
                                MaximumWidth: 800,
                                MaximumHeight: 500,
                                ResizeToWidth: 800,
                                ResizeToHeight: 500,
                                Format: "jpg",
                            }
                        }
                        this.ngOnInit();
                        this.Form_AddUser_Close();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    Form_AddUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deals]);
    }
    //#endregion
    //#region Amount Distribution
    _AmountDistribution =
        {
            ActualPrice: null,
            SellingPrice: null,
            SellingPricePercentage: null,
            SellingPriceDifference: null,
            TUCPercentage: null,
            OrignalTUCPercentage: null,
            TUCAmount: null,
            MerchantPercentage: null,
            MerchantAmount: null,
        };
    ProcessAmounts() {
        var tAmount = this._AmountDistribution;
        var SellingPrice = parseFloat(this._AmountDistribution.SellingPrice);
        var ActualPrice = parseFloat(this._AmountDistribution.ActualPrice);
        if (ActualPrice != undefined && ActualPrice != null && isNaN(ActualPrice) == false && ActualPrice >= 0) {
            if (SellingPrice != undefined && SellingPrice != null && isNaN(SellingPrice) == false && SellingPrice >= 0) {
                if (ActualPrice >= SellingPrice) {
                    tAmount.SellingPriceDifference = this._HelperService.GetFixedDecimalNumber(ActualPrice - SellingPrice);
                    tAmount.SellingPricePercentage = this._HelperService.GetPercentageFromNumber(tAmount.SellingPriceDifference, ActualPrice);
                    tAmount.TUCAmount = this._HelperService.GetAmountFromPercentage(SellingPrice, tAmount.TUCPercentage);
                    tAmount.MerchantAmount = this._HelperService.GetFixedDecimalNumber(SellingPrice - tAmount.TUCAmount);
                    tAmount.MerchantPercentage = this._HelperService.GetPercentageFromNumber(tAmount.MerchantAmount, SellingPrice);
                }
                else {
                    if (SellingPrice > ActualPrice) {
                        setTimeout(() => {
                            this._AmountDistribution =
                            {
                                ActualPrice: this._AmountDistribution.ActualPrice,
                                SellingPrice: null,
                                SellingPricePercentage: null,
                                SellingPriceDifference: null,
                                OrignalTUCPercentage: this._AmountDistribution.OrignalTUCPercentage,
                                TUCPercentage: this._AmountDistribution.TUCPercentage,
                                TUCAmount: null,
                                MerchantPercentage: null,
                                MerchantAmount: null,
                            };
                            tAmount.SellingPriceDifference = this._HelperService.GetFixedDecimalNumber(ActualPrice - 0);
                            tAmount.SellingPricePercentage = this._HelperService.GetPercentageFromNumber(tAmount.SellingPriceDifference, ActualPrice);
                            tAmount.TUCAmount = this._HelperService.GetAmountFromPercentage(0, tAmount.TUCPercentage);
                            tAmount.MerchantAmount = this._HelperService.GetFixedDecimalNumber(0 - tAmount.TUCAmount);
                            tAmount.MerchantPercentage = this._HelperService.GetPercentageFromNumber(tAmount.MerchantAmount, 0);
                        }, 200);
                    }
                }
            }
        }
        this.ProcessTitle();
    }
    //#endregion

    //#region Title Manager
    _Titles =
        {
            OriginalTitle: "",
            Title1: this._HelperService.UserCountry.CurrencyNotation + " X deal title for " + this._HelperService.UserCountry.CurrencyNotation + " Y",
            Title2: this._HelperService.UserCountry.CurrencyNotation + " Y off on deal title",
            Title3: "x% off on deal title"
        }
    ProcessTitle() {
        this._Titles.Title1 = this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.ActualPrice) + " " + this._Titles.OriginalTitle + " for " + this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.SellingPrice);
        this._Titles.Title2 = this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.SellingPriceDifference) + " off on " + this._Titles.OriginalTitle;
        this._Titles.Title3 = Math.round(this._AmountDistribution.SellingPricePercentage) + "% off on " + this._Titles.OriginalTitle;
        // this.Form_AddDeal.patchValue({
        //     Description: `${this._SelectedSubCategory.Name} , ${this.Form_AddDeal.value.Title} , ${this._Titles.OriginalTitle}`,
        // })
        let description = '';
        if (this._SelectedSubCategory.Name != undefined && this._SelectedSubCategory.Name != null && this._SelectedSubCategory.Name != '') {
            description = this._SelectedSubCategory.Name;

        }

        if (this._Titles.OriginalTitle != undefined && this._Titles.OriginalTitle != null && this._Titles.OriginalTitle != '') {
            description += ' ' + this._Titles.OriginalTitle;
        }
        // if (this._SelectedSubCategory.Name) {
        //     description = `${description} ${this._SelectedSubCategory.Name}`
        // }
        if (this.Form_AddDeal.value.Weight != undefined && this.Form_AddDeal.value.Weight != null && this.Form_AddDeal.value.Weight > 0) {
            // description = (this._SelectedSubCategory.Name) ? `${description} , ${this.Form_AddDeal.value.Title}` : `${this.Form_AddDeal.value.Title}`
            description += ' (' + this.Form_AddDeal.value.Weight + 'kg)';
        }
        // if (this.Form_AddDeal.value.Title) {
        //     description = (this._SelectedSubCategory.Name) ? `${description} , ${this.Form_AddDeal.value.Title}` : `${this.Form_AddDeal.value.Title}`
        // }
        // if (this._Titles.OriginalTitle) {
        //     description = (this.Form_AddDeal.value.Title) ? `${description} , ${this._Titles.OriginalTitle}` : `${this._Titles.OriginalTitle}`
        // }
        if (description != undefined && description != null && description != '') {
            this.Form_AddDeal.patchValue({
                Description: description
            })
        }
    }
    //#endregion
    //#region Deal Shedule Manager 
    ScheduleStartDateRangeChange(value) {
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            parentEl: '#adddeal_enddate',
            startDate: value.start,
            minDate: value.start,
        };
        this._DealConfig.StartDate = value.start;
        this._DealConfig.EndDate = value.start;
        this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                StartDate: value.start.format('DD-MM-YYYY hh:mm a'),
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    ScheduleEndDateRangeChange(value) {
        this._DealConfig.DealCodeEndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            parentEl:'#dealexpireCode_date',
            startDate: value.start,
            minDate: value.start,
        };
        this._DealConfig.EndDate = value.start;
        this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    ScheduleCodeEndDateRangeChange(value) {
        this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    //#endregion

    //#region Image Manager
    onImageAccept(value) {
        setTimeout(() => {
            this.Form_AddUser.patchValue(
                {
                    TImage: null,
                }
            );
        }, 300);
        this._ImageManager.ActiveImage = value;
        this._ImageManager.ActiveImageName = value.file.name;
        this._ImageManager.ActiveImageName = value.file.size;
    }
    Icon_B64Cropped(base64: string) {
        this._ImageManager.TCroppedImage = base64;
    }
    Icon_B64CroppedDone() {
        var ImageDetails = this._HelperService.GetImageDetails(this._ImageManager.TCroppedImage);
        var ImageItem =
        {
            OriginalContent: this._ImageManager.TCroppedImage,
            Name: this._ImageManager.ActiveImageName,
            Size: this._ImageManager.ActiveImageSize,
            Extension: ImageDetails.Extension,
            Content: ImageDetails.Content
        };
        if (this._DealConfig.DealImages.length == 0) {
            this._DealConfig.DealImages.push(
                {
                    ImageContent: ImageItem,
                    IsDefault: 1,
                }
            );
        }
        else {
            this._DealConfig.DealImages.push(
                {
                    ImageContent: ImageItem,
                    IsDefault: 0,
                }
            );
        }
        this._ImageManager.TCroppedImage = null;
        this._ImageManager.ActiveImage = null;
        this._ImageManager.ActiveImageName = null;
        this._ImageManager.ActiveImageSize = null;
    }
    Icon_Crop_Clear() {
        this._ImageManager.TCroppedImage = null;
        this._ImageManager.ActiveImage = null;
        this._ImageManager.ActiveImageName = null;
        this._ImageManager.ActiveImageSize = null;
        this._HelperService.CloseModal('_Icon_Cropper_Modal');
    }
    RemoveImage(Item) {
        this._DealConfig.DealImages = this._DealConfig.DealImages.filter(x => x != Item);
    }

    openFilemanager() {
        if (!this.SelectedMerchant.ReferenceKey) {
            this._HelperService.NotifyError("Select merchant before selecting image");
        } else {
            let dialogRef = this._dialog.open(FileManagerComponent, {
                width: '1200px',
                height: '750px',
                maxWidth: '1200px',
                maxHeight: '800px',
                disableClose: true,
                data: {
                    merchantId: this.SelectedMerchant.ReferenceKey
                }
            });
            dialogRef.afterClosed().subscribe(res => {
                if (res.data) {
                    this._DealConfig.DealImages.push(
                        {
                            reference: res.data.reference,
                            url: res.data.url,
                        }
                    );
                }
            });
        }
    }

    drop(event: CdkDragDrop<any>): void {
        moveItemInArray(
            this._DealConfig.DealImages,
            event.previousContainer.data,
            event.container.data
        );
    }
    //#endregion

    //#region   List Helpers
    public _S2Categories_Data: Array<Select2OptionData>;
    GetDealCategories_List() {
        var PlaceHolder = "Select Merchant";
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.getallcategories,
            Offset: 0,
            Limit: 1000,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System2, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    var _Data = _Response.Result.Data as any[];
                    if (_Data.length > 0) {
                        var parentCategories = [];
                        _Data.forEach(element => {
                            var itemIndex = parentCategories.findIndex(x => x.ParentCategoryId == element.ParentCategoryId);
                            if (itemIndex == -1) {
                                var categories = _Data.filter(x => x.ParentCategoryId == element.ParentCategoryId);
                                parentCategories.push(
                                    {
                                        ParentCategoryId: element.ParentCategoryId,
                                        ParentCategoryKey: element.ParentCategoryKey,
                                        ParentCategoryName: element.ParentCategoryName,
                                        Categories: categories,
                                    }
                                );
                            }
                        });
                        var finalCat = [];
                        finalCat.push(
                            {
                                id: "0",
                                text: "Select category",
                                // disabled: true,
                            }
                        )

                        parentCategories.forEach(element => {
                            var Item = {
                                id: element.ParentCategoryId,
                                text: element.ParentCategoryName,
                                children: [],
                                additional: element,
                            };
                            element.Categories.forEach(CategoryItem => {
                                var cItem = {
                                    id: CategoryItem.ReferenceId,
                                    text: CategoryItem.Name + " (" + CategoryItem.Fees + "%)",
                                    additional: CategoryItem,
                                };
                                Item.children.push(cItem);
                            });
                            finalCat.push(Item);
                        });
                        this._S2Categories_Data = finalCat;
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    _SelectedSubCategory =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Fee: 5,
            Name: null,
            IconUrl: null,
            ParentCategoryId: 0,
            ParentCategoryKey: 0,
            ParentCategoryName: 0
        };
    GetDealCategories_Selected(event: any) {
        
        if (event.value != "0") {
            this._SelectedSubCategory.ReferenceId = event.data[0].additional.ReferenceId;
            this._SelectedSubCategory.ReferenceKey = event.data[0].additional.ReferenceKey;
            this._SelectedSubCategory.Name = event.data[0].additional.Name;
            this._SelectedSubCategory.IconUrl = event.data[0].additional.IconUrl;
            this._SelectedSubCategory.Fee = event.data[0].additional.Fees;
            this._AmountDistribution.TUCPercentage = event.data[0].additional.Fees;
            this._AmountDistribution.OrignalTUCPercentage = event.data[0].additional.Fees;
            this._SelectedSubCategory.ParentCategoryId = event.data[0].additional.ParentCategoryId;
            this._SelectedSubCategory.ParentCategoryKey = event.data[0].additional.ParentCategoryKey;
            this._SelectedSubCategory.ParentCategoryName = event.data[0].additional.ParentCategoryName;
            this.Form_AddUser.patchValue(
                {
                    SubCategoryKey: this._SelectedSubCategory.ParentCategoryKey
                }
            );
            this.ProcessAmounts();
        }
        else{
            this._SelectedSubCategory.ReferenceId=0;
        }
    }

    public GetMerchants_Option: Select2Options;
    public GetMerchants_Transport: any;
    public SelectedMerchant: any = {};
    GetMerchants_List() {
        var PlaceHolder = "Select Merchant";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetDealMerchants,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals,
            Limit:this._HelperService.AppConfig.ListLimit,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ]
        }
        this.GetMerchants_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetMerchants_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetMerchants_Transport,
            multiple: false,
        };
    }
    GetMerchants_ListChange(event: any) {
        this.SelectedMerchant = event.data[0];
        this.Form_AddUser.patchValue(
            {
                AccountKey: event.data[0].ReferenceKey,
                AccountId: event.data[0].ReferenceId
            }
        );
        this.getCategorieslist();
        this.GetStores_List();
    }
    GetStores_List() {
        if (this.SelectedMerchant != undefined && this.SelectedMerchant != null && this.SelectedMerchant.ReferenceId > 0) {
            var pData = {
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
                Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
                Offset: 0,
                Limit: 1000,
                SearchCondition: this._HelperService.GetSearchCondition("", "MerchantReferenceId", "number", this.SelectedMerchant.ReferenceId),
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                }
            );
        }

    }

    //#endregion
    public GetProductCategory_option: Select2Options;
    public GetProductCategory_Transport: any;
    public selectedProductCategory: any = {};
    public productData: any = [{ id: 0, text: 'Select Package' }];

    async getCategorieslist(isFromSave?) {
        // if (this.SelectedMerchant != undefined && this.SelectedMerchant != null && this.SelectedMerchant.ReferenceId > 0) {

        const pData = {
            "Task": "getsystempackagings",
            "RefreshCount": true,
            "TotalRecords": 0,
            "Offset": 0,
            "Limit": 1000,
            "SortExpression": "ReferenceId desc",
            SearchCondition: ''
        };
        if (this.SelectedMerchant != undefined && this.SelectedMerchant != null && this.SelectedMerchant.ReferenceId > 0) {
            pData.SearchCondition = this._HelperService.GetSearchCondition("", "AccountId", "number", this.SelectedMerchant.ReferenceId);
            pData.SearchCondition = `${pData.SearchCondition} || AccountId == null`
        } else {
            pData.SearchCondition = `AccountId == null`
        }


        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.systemPackaging, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.productData = [{ id: 0, text: 'Select Package' }];
                    if (_Response.Result.Data && _Response.Result.Data.length > 0) {
                        _Response.Result.Data.forEach(element => {
                            this.productData.push({
                                id: element.PackagingId,
                                text: element.Name || element.CategoryName,
                                additional: element
                            })
                        });
                        if (isFromSave) {
                            this.Package = this.productData[1].id;
                        }
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }


    subCategoryData = [];
    GetProductcategory_ListChange(event) {
        if (event.data && event.data.length > 0) {
            this.packagingData.selectedProduct = event.data[0].additional;
            if (this.packagingData.selectedProduct) {
                this.Package = event.value;
                this.Form_AddDeal.patchValue({
                    Title: `${this.packagingData.selectedProduct.Type}_${this.packagingData.selectedProduct.Height}H_${this.packagingData.selectedProduct.Width}W_${this.packagingData.selectedProduct.Length}L`,
                    Height: this.packagingData.selectedProduct.Height || 0,
                    Width: this.packagingData.selectedProduct.Width || 0,
                    Length: this.packagingData.selectedProduct.Length || 0,
                    Weight: this.packagingData.selectedProduct.Weight,
                    Type: this.packagingData.selectedProduct.Type
                });

                let description = '';
                if (this._SelectedSubCategory.Name) {
                    description = `${description} ${this._SelectedSubCategory.Name}`
                } if (this.Form_AddDeal.value.Title) {
                    description = (this._SelectedSubCategory.Name) ? `${description} , ${this.Form_AddDeal.value.Title}` : `${this.Form_AddDeal.value.Title}`
                }
                if (this._Titles.OriginalTitle) {
                    description = (this.Form_AddDeal.value.Title) ? `${description} , ${this._Titles.OriginalTitle}` : `${this._Titles.OriginalTitle}`
                }

                this.Form_AddDeal.patchValue({
                    Description: description
                })
            }
        }
    }

    isNew = true;

    openDeliveryDialog(type) {
        this.Form_AddPackage.reset();
        this._HelperService.OpenModal('delivery');
        if (type === 'duplicate') {
            this.isNew = false;
            this.Form_AddPackage.controls.Title.disable();
            this.Form_AddPackage.controls.Height.disable();
            this.Form_AddPackage.controls.Width.disable();
            this.Form_AddPackage.controls.Length.disable();
            this.Form_AddPackage.controls.Type.disable();

            this.Form_AddPackage.patchValue({
                Title: this.Form_AddDeal.value.Title,
                Height: this.Form_AddDeal.value.Height,
                Width: this.Form_AddDeal.value.Width,
                Length: this.Form_AddDeal.value.Length,
                Weight: this.Form_AddDeal.value.Weight,
                Type: this.Form_AddDeal.value.Type,
            });

        } else {
            this.isNew = true;

            this.Form_AddPackage.controls.Title.disable();
            this.Form_AddPackage.controls.Height.enable();
            this.Form_AddPackage.controls.Width.enable();
            this.Form_AddPackage.controls.Length.enable();
            this.Form_AddPackage.controls.Type.enable();
        }
    }

    savePackage() {

        const pData = {
            Task: "savepackaging",
            AccountId: this.SelectedMerchant.ReferenceId,
            AccountKey: this.SelectedMerchant.ReferenceKey,
            Height: this.Form_AddPackage.controls.Height.value,
            Length: this.Form_AddPackage.controls.Length.value,
            Width: this.Form_AddPackage.controls.Width.value,
            Weight: this.Form_AddPackage.controls.Weight.value,
            Type: this.Form_AddPackage.controls.Type.value,
            Name: this.Form_AddPackage.controls.Title.value,
            // ParcelDescription: this.Form_AddPackage.controls.Description.value,
            Size_unit: 'cm',
            Weight_unit: 'kg'
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.systemPackaging, pData);
        _OResponse.subscribe(
            async _Response => {
                await this.getCategorieslist(true);
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.CloseModal('delivery');
                    this.Form_AddDeal.patchValue({
                        Title: _Response.Result.Name,
                        Height: _Response.Result.Height,
                        Width: _Response.Result.Width,
                        Length: _Response.Result.Length,
                        Weight: _Response.Result.Weight,
                        Type: _Response.Result.Type,
                        Description: _Response.Result.Description
                    })
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );


    }

    onchangeofValue() {
        let title = '';
        if (this.Form_AddPackage.value.Type) {
            title = `${this.Form_AddPackage.value.Type}`;
        }
        if (this.Form_AddPackage.value.Height) {
            title = `${title}_${this.Form_AddPackage.value.Height}H`;
        }
        if (this.Form_AddPackage.value.Width) {
            title = `${title}_${this.Form_AddPackage.value.Width}W`;
        }
        if (this.Form_AddPackage.value.Length) {
            title = `${title}_${this.Form_AddPackage.value.Length}L`;
        }
        if (this.Form_AddPackage.value.Weight) {
            title = `${title}_${this.Form_AddPackage.value.Weight}WE`;
        }
        this.Form_AddPackage.patchValue({
            Title: `${title}`,
        });
    }

    close() {
        this._HelperService.CloseModal("delivery");
    }

}


export class IAmountDistribution {
    ActualPrice: number;
    SellingPrice: number;
    SellingPricePercentage: number;
    SellingPriceDifference: number;
    TUCPercentage: number;
    OrignalTUCPercentage: number;
    TUCAmount: number;
    MerchantPercentage: number;
    MerchantAmount: number;
}
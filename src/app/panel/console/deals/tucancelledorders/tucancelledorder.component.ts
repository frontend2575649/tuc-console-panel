import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs';

import swal from 'sweetalert2';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
declare var moment: any;
declare var $: any;


@Component({
    selector: 'tu-tucancelledorder',
    templateUrl: './tucancelledorder.component.html',
})
export class TUCancelledComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _FilterHelperService: FilterHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
    ) {

    }
    @ViewChild("offCanvas") divView: ElementRef;
    public ResetFilterControls: boolean = true;
    public AcquirerId = 0;

    ngOnInit() {
        Feather.replace();
        this._HelperService.StopClickPropogation();

        this.TUTr_Setup();

    }

    TUTr_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
    TUTr_InvoiceRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;


    public TUTr_Config: OList;
    public TuTr_Columns =
        {
            Status: true,
            Date: true,
            Account: true,
            AccountName: true,
            Mob: true,
            Biller: true,
            Amount: true,
            Source: true,
            PaymentRef: true
        }
    public TuTr_Filters_List =
        {
            Search: false,
            Date: false,
            Sort: false,
            Status: false,
            Biller: false
        }
    TUTr_Setup() {

        this.TUTr_Config = {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetShipments,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment,
            Title: "Cancelled Orders",
            StatusType: "default",
            DefaultSortExpression: "CreateDate desc",
            SearchBaseCondition: this.TuTr_Setup_SearchCondition(),
            TableFields: [
                {
                    DisplayName: "Orde ID",
                    SystemName: "ReferenceId",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Cancelled by",
                    SystemName: "ModifyDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date text-right",
                    Show: true,
                    IsDateSearchField: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
                {
                    DisplayName: "Placed On",
                    SystemName: "CreateDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date text-right",
                    Show: true,
                    IsDateSearchField: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },

                {
                    DisplayName: "Customer Name",
                    SystemName: "CustomerDisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },


                {
                    DisplayName: "Mobile Number",
                    SystemName: "CustomerMobileNumber",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "td-date text-right",
                    Show: true,
                    Sort: false,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
                {
                    DisplayName: "Items",
                    SystemName: "Items",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "text-center",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    DefaultValue: "ThankUCash",
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Total Amount",
                    SystemName: "Total Amount",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "text-center",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    DefaultValue: "ThankUCash",
                    NavigateField: "ReferenceKey"
                },
            ]
        };

        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this._HelperService.Active_FilterInit(this._HelperService.AppConfig.FilterTypeOption.GetShipment, this.TUTr_Config);
        this.TUTr_GetData();
    }

    TUTr_ToggleOption_Date(event: any, Type: any) {
        this.TUTr_ToggleOption(event, Type);
    }

    Data: any = {};
    TUTr_ToggleOption(event: any, Type: any) {
        if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
            event.data = {
                SalesMin: this.TUTr_InvoiceRangeMinAmount,
                SalesMax: this.TUTr_InvoiceRangeMaxAmount,
            }
        }
        if (Type == "date") {
            this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
            this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
        }
        this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

        this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
        this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

        this.TodayStartTime = this.TUTr_Config.StartTime;
        this.TodayEndTime = this.TUTr_Config.EndTime;
        if (event != null) {
            for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                const element = this.TUTr_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }
            }
        }
        if (
            (this.TUTr_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.TUTr_GetData();
        }
    }

    timeout = null;
    TUTr_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {
            if (Type == 'date') {
                event.start = this._HelperService.DateInUTC(event.start);
                event.end = this._HelperService.DateInUTC(event.end);
            }

            this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

            this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
            this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

            this.TodayStartTime = this.TUTr_Config.StartTime;
            this.TodayEndTime = this.TUTr_Config.EndTime;
            if (event != null) {
                for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                    const element = this.TUTr_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {
                        element.SystemActive = true;
                    }
                    else {
                        element.SystemActive = false;
                    }
                }
            }

            if (
                (this.TUTr_Config.RefreshData == true)
                && this._HelperService.DataReloadEligibility(Type)
            ) {
                this.TUTr_GetData();
            }
        }, this._HelperService.AppConfig.SearchInputDelay);

    }

    public OverviewData: any = {
        Transactions: 0,
        InvoiceAmount: 0.0
    };

    TUTr_GetData() {
        this.GetOverviews(this.TUTr_Config, "getordersovrview");
        var TConfig = this._DataHelperService.List_GetDataTur(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    }


    TuTr_Setup_SearchCondition() {
        var SearchCondition = "";
        SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, ['orderstatus.cancelledbyuser', 'orderstatus.cancelledbyseller', 'orderstatus.cancelledbysystem'], "==")
        // SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'orderstatus.cancelledbyseller', '='),
        // SearchCondition= this._HelperService.GetSearchConditionStrict(SearchCondition, 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'orderstatus.cancelledbysystem', '=')
        return SearchCondition;
    }


    SetOtherFilters(): void {
        this.TUTr_Config.SearchBaseConditions = [];
        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Store));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_Store_Selected = 0;
            this.StoresEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }



    public TUTr_Filter_Store_Option: Select2Options;
    public TUTr_Filter_Store_Toggle = false;
    public TUTr_Filter_Store_Selected = 0;
    TUTr_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Store_Option = {
            placeholder: 'Search by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Stores_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Store
        );
        this.StoresEventProcessing(event);
    }

    StoresEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }


    TodayStartTime = null;
    TodayEndTime = null;

    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();
        this.TUTr_Filter_Stores_Load();
        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();
    }

    SetSearchRanges(): void {
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalAmount', this.TUTr_Config.SearchBaseConditions);
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }
    }


    GetOverviews(ListOptions: any, Task: string): any {
        this._HelperService.IsFormProcessing = true;

        ListOptions.SearchCondition = '';
        ListOptions = this._DataHelperService.List_GetSearchConditionTur(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;
        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }
        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }


        var pData = {
            Task: Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            AccountId: ListOptions.AccountId,
            AccountKey: ListOptions.AccountKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.OverviewData = _Response.Result as any;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }


    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
        this.SetOtherFilters();
        this.TUTr_GetData();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_Store(Type, index);
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
        if (Type == 'Time') {
            this._HelperService.AppConfig.DateRangeOptions.startDate = new Date(2017, 0, 1, 0, 0, 0, 0);
            this._HelperService.AppConfig.DateRangeOptions.endDate = moment().endOf("day");
        }
        this.SetOtherFilters();
        this.TUTr_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            // input: "text",
            html:
                '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
                '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
                '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
            focusConfirm: false,
            preConfirm: () => {
                return {
                    filter: document.getElementById('swal-input1')['value'],
                    private: document.getElementById('swal-input2')['checked']
                }
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {

                if (result.value.filter.length < 5) {
                    this._HelperService.NotifyError('Enter filter name length greater than 4');
                    return;
                }

                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

                var AccessType: number = result.value.private ? 0 : 1;
                this._HelperService.Save_NewFilter(this._HelperService.AppConfig.FilterTypeOption.SoldHistory, AccessType);
                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();
                this._HelperService.Delete_Filter(this._HelperService.AppConfig.FilterTypeOption.SoldHistory);
                this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
                this.TUTr_GetData();
                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        if (this.TUTr_InvoiceRangeMaxAmount == null || this.TUTr_InvoiceRangeMaxAmount || undefined ||
            this.TUTr_InvoiceRangeMinAmount == null || this.TUTr_InvoiceRangeMinAmount || undefined) {
            this._HelperService.NotifyError("Value not be null or undefined")
        }
        else if (this.TUTr_InvoiceRangeMinAmount > this.TUTr_InvoiceRangeMaxAmount) {
            this._HelperService.NotifyError("Minimum  Amount should be less than Maximum  Amount");
        }
        else {

            this.SetSearchRanges();
            this._HelperService.MakeFilterSnapPermanent();
            this.TUTr_GetData();
            this.ResetFilterUI(); this._HelperService.StopClickPropogation();
            if (ButtonType == 'Sort') {
                $("#TUTr_sdropdown").dropdown('toggle');
            } else if (ButtonType == 'Other') {
                $("#TUTr_fdropdown").dropdown('toggle');
            }
        }
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
        this.SetOtherFilters();
        this.SetSalesRanges();
        this.TUTr_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }
    SetSalesRanges(): void {
        this.TUTr_InvoiceRangeMinAmount = this.TUTr_Config.SalesRange.SalesMin;
        this.TUTr_InvoiceRangeMaxAmount = this.TUTr_Config.SalesRange.SalesMax;
    }

}


export class OCoreUsage {

    public UserRewardAmount: number;
    public BankAccountNumber: number;
    public Charge: number;
    public TotalAmount: number;
    public MerchantAmount: number;
    public ToAccountId: number;
    public ToAccountMobileNumber: number;
    public AccountDisplayName: string;
    public ToAccountDisplayName: string;
    public Title: string;
    public BankName: string;
    public ReferenceNumber: string;
    public CreatedByDisplayName: string;
    public BankAccountName: string;
    public ModifyByDisplayName: string;
    public SystemComment: string;
    public ProductCategoryName: string;
    public ProductItemName: string;
    public RewardAmount: number;
    public Amount: number;
    public CommisonAmount: number;
    public AccountMobileNumber: number;
    public PaymentReference: string;
    public StatusB: string;
    public Coordinates?: any;
    public ReferenceId: number;
    public AccountId: number;
    public AccountKey: string;
    public Reference: string;
    public ReferenceKey: string;
    public AppOwnerKey: string;
    public AppOwnerName: string;
    public AppKey: string;
    public AppName: string;
    public ApiKey: string;
    public ApiName: string;
    public AppVersionKey: string;
    public AppVersionName: string;
    public FeatureKey: string;
    public FeatureName: string;
    public UserAccountKey: string;
    public UserAccountDisplayName: string;
    public UserAccountIconUrl: string;
    public UserAccountTypeCode: string;
    public UserAccountTypeName: string;
    public SessionId: string;
    public SessionKey: string;
    public IpAddress: string;
    public Latitude: string;
    public Longitude: string;
    public Request: string;
    public Response: string;
    public RequestTime: Date;
    public StartDate: Date;
    public ModifyDate: Date;
    public EndDate: Date;
    public CreateDate: Date;
    public ResponseTime: Date;
    public ProcessingTime: string;
    public StatusId: number;
    public StatusCode: string;
    public StatusName: string;
}
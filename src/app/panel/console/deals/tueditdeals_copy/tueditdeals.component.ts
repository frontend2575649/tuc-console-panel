import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from "../../../../service/service";;
import swal from 'sweetalert2';
declare var $: any;
import * as Feather from 'feather-icons';
declare var google: any;
declare var moment: any;
import * as cloneDeep from 'lodash/cloneDeep';
import { InputFileComponent, InputFile } from 'ngx-input-file';

@Component({
    selector: 'app-tueditdeals',
    templateUrl: './tueditdeals.component.html',
    styleUrls: ['./tueditdeals.component.css']

})
export class EditDealsComponent implements OnInit {
    public SaveAccountRequest: any;
    public value: any;

    public showPickers: boolean = true;
    public merchantPlaceholder: string = "Select Merchant";
    public categoryPlaceholder: string = "Select Category";

    public NiraOrPercent: any = {
        DiscountAmount: "1",
        CommissionAmount: "1"
    }

    DealTimings: any = {
        Start: new Date(),
        End: new Date()
    }

    ValidTimings: any = {
        Start: new Date(),
        End: new Date()
    }

    Expiry: any = {
        Hours: 0,
        Days: 0,
        Date: new Date()
    }

    public WeekDays: any[] = [
        { name: 'Monday', selected: true },
        { name: 'Tuesday', selected: true },
        { name: 'Wednesday', selected: true },
        { name: 'Thursday', selected: true },
        { name: 'Friday', selected: true },
        { name: 'Saturday', selected: true },
        { name: 'Sunday', selected: true }
    ]

    @ViewChild(InputFileComponent)
    private InputFileComponent_Term: InputFileComponent;

    CurrentImagesCount: number = 0;

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {
    }


    ngOnInit() {

        $('[data-toggle="tooltip"]').tooltip();

        if (this.InputFileComponent_Term != undefined) {
            this.CurrentImagesCount = 0;
            this._HelperService._InputFileComponent = this.InputFileComponent_Term;
            this.InputFileComponent_Term.onChange = (files: Array<InputFile>): void => {
                if (files.length >= this.CurrentImagesCount) {
                    this._HelperService._SetFirstImageOrNone(this.InputFileComponent_Term.files);
                }
                this.CurrentImagesCount = files.length;
            };
        }

        this.Form_EditUser_Load();
        this.GetRoles_List();
        this.GetCategories_List();
        this.GetMerchants_List();
        this.GetStores_List();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];

            this._HelperService.AppConfig.ActiveAccountKey = params["accountkey"];
            this._HelperService.AppConfig.ActiveAccountId = params["accountid"];

            this.GetDealDetails();
        });
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    public GetRoles_Option: Select2Options;
    public GetRoles_Transport: any;
    GetRoles_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreParameters,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'Name',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
        this.GetRoles_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetRoles_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetRoles_Transport,
            multiple: false,
        };
    }
    GetRoles_ListChange(event: any) {
        this.Form_EditUser.patchValue(
            {
                RoleKey: event.value
            }
        );
    }

    public GetMerchants_Option: Select2Options;
    public GetMerchants_Transport: any;
    public SelectedMerchant: any = {};
    GetMerchants_List() {
        var PlaceHolder = this.merchantPlaceholder;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ]
        }

        this.GetMerchants_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetMerchants_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetMerchants_Transport,
            multiple: false,
        };
    }
    GetMerchants_ListChange(event: any) {

        this.SelectedMerchant = event.data[0];
        this.Form_EditUser.patchValue(
            {
                AccountKey: event.data[0].ReferenceKey,
                AccountId: event.data[0].ReferenceId
            }
        );



    }

    public GetCategories_Option: Select2Options;
    public GetCategories_Transport: any;
    GetCategories_List() {
        var PlaceHolder = this.categoryPlaceholder;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommons,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceKey",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: 'StatusCode',
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: '=',
                //     SearchValue: this._HelperService.AppConfig.Status.Active,
                // }
            ]
        }


        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'TypeCode', this._HelperService.AppConfig.DataType.Text,
            [
                this._HelperService.AppConfig.HelperTypes.MerchantCategories,

            ]
            , '=');
        this.GetCategories_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetCategories_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetCategories_Transport,
            multiple: false,
        };


    }
    GetCategories_ListChange(event: any) {
        this.Form_EditUser.patchValue(
            {
                CategoryKey: event.data[0].ReferenceKey

            }
        );
    }

    public GetStores_Option: Select2Options;
    public GetStores_Transport: any;
    public StoresList: any = []
    GetStores_List() {
        var PlaceHolder = "Select Stores";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: 'StatusCode',
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: '=',
                //     SearchValue: this._HelperService.AppConfig.Status.Active,
                // }
            ]
        }


        this.GetStores_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetStores_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetStores_Transport,
            multiple: true,
        };
    }
    GetStores_ListChange(event: any) {
        this.StoresList = event.data
    }

    ScheduleStartDateRangeChange(value) {

        this.Form_EditUser.patchValue(
            {
                StartDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
            }
        );
        this.Form_EditUser.patchValue(
            {
                StartDate: value.start,

            }
        );
    }

    ScheduleEndDateRangeChange(value) {

        this.Form_EditUser.patchValue(
            {
                EndDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
            }
        );
        this.Form_EditUser.patchValue(
            {
                EndDate: value.start,
            }
        );

    }

    _CurrentAddress: any = {};
    Form_EditUser: FormGroup;
    Form_EditUser_Address: string = null;
    Form_EditUser_Latitude: number = 0;
    Form_EditUser_Longitude: number = 0;
    @ViewChild('places') places: GooglePlaceDirective;
    Form_EditUser_PlaceMarkerClick(event) {
        this.Form_EditUser_Latitude = event.coords.lat;
        this.Form_EditUser_Longitude = event.coords.lng;
    }
    public Form_EditUser_AddressChange(address: Address) {
        this.Form_EditUser_Latitude = address.geometry.location.lat();
        this.Form_EditUser_Longitude = address.geometry.location.lng();
        this.Form_EditUser_Address = address.formatted_address;
        this.Form_EditUser.controls['Latitude'].setValue(this.Form_EditUser_Latitude);
        this.Form_EditUser.controls['Longitude'].setValue(this.Form_EditUser_Longitude);
        this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
        this.Form_EditUser.controls['Address'].setValue(address.name + ',' + address.formatted_address);
    }
    Form_EditUser_Show() {
    }
    Form_EditUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deals]);
    }
    Form_EditUser_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_EditUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: 'updatedeal',
            TypeCode: 'deal',
            CategoryKey: null,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            AccountId: [null, Validators.required],
            AccountKey: [null, Validators.required],
            Title: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
            Description: null,
            Terms: [null, Validators.required],
            CommissionAmount: [null, Validators.required],
            DiscountAmount: [null, Validators.required],
            MaximumUnitSalePerDay: [null, Validators.required],
            MaximumUnitSale: [null, Validators.required],
            MaximumUnitSalePerPerson: [null, Validators.required],
            IconContent: this._HelperService._Icon_Cropper_Data,
            //SellingPrice: [null, Validators.required],
            Amount: [null, Validators.required],
            ActualPrice: [null, Validators.required],
            EndDate: null,
            StartDate: null,
            DealTitle: null, //transient
            CouponValidity: null //transient
        });
    }
    Form_EditUser_Clear() {
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 300);
        this.Form_EditUser_Latitude = 0;
        this.Form_EditUser_Longitude = 0;
        this.Form_EditUser.reset();
        this._HelperService.Icon_Crop_Clear();
        this.Form_EditUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_EditUser_Process(_FormValue: any, IsDraft: boolean) {
        this._HelperService.IsFormProcessing = true;
        _FormValue.Latitude = this.Form_EditUser_Latitude;
        _FormValue.Longitude = this.Form_EditUser_Longitude;
        this.SaveAccountRequest = this.ReFormat_RequestBody();

        if (IsDraft) {
            this.SaveAccountRequest.StatusCode = 'deal.draft';
        } else {
            this.SaveAccountRequest.StatusCode = 'deal.published';
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.SaveAccountRequest);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess('Deal updated successfully. It will take upto 5 minutes to update changes.');
                    this.InputFileComponent_Term.files.pop();
                    this.Form_EditUser_Clear();
                    if (_FormValue.OperationType == 'edit') {
                    }
                    else if (_FormValue.OperationType == 'close') {
                        this.Form_EditUser_Close();
                    }
                    this.Form_EditUser_Close();

                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    //#region DealDetails 

    public _UserAccount: any = {
        ContactNumber: null,
        SecondaryEmailAddress: null,
        ReferenceId: null,
        BankDisplayName: null,
        BankKey: null,
        SubOwnerAddress: null,
        SubOwnerLatitude: null,
        SubOwnerDisplayName: null,
        SubOwnerKey: null,
        SubOwnerLongitude: null,
        AccessPin: null,
        LastLoginDateS: null,
        AppKey: null,
        AppName: null,
        AppVersionKey: null,
        CreateDate: null,
        CreateDateS: null,
        CreatedByDisplayName: null,
        CreatedByIconUrl: null,
        CreatedByKey: null,
        Description: null,
        IconUrl: null,
        ModifyByDisplayName: null,
        ModifyByIconUrl: null,
        ModifyByKey: null,
        ModifyDate: null,
        ModifyDateS: null,
        PosterUrl: null,
        ReferenceKey: null,
        StatusCode: null,
        StatusI: null,
        StatusId: null,
        StatusName: null,
        AccountCode: null,
        AccountOperationTypeCode: null,
        AccountOperationTypeName: null,
        AccountTypeCode: null,
        AccountTypeName: null,
        Address: null,
        AppVersionName: null,
        ApplicationStatusCode: null,
        ApplicationStatusName: null,
        AverageValue: null,
        CityAreaKey: null,
        CityAreaName: null,
        CityKey: null,
        CityName: null,
        CountValue: null,
        CountryKey: null,
        CountryName: null,
        DateOfBirth: null,
        DisplayName: null,
        EmailAddress: null,
        EmailVerificationStatus: null,
        EmailVerificationStatusDate: null,
        FirstName: null,
        GenderCode: null,
        GenderName: null,
        LastLoginDate: null,
        LastName: null,
        Latitude: null,
        Longitude: null,
        MobileNumber: null,
        Name: null,
        NumberVerificationStatus: null,
        NumberVerificationStatusDate: null,
        OwnerDisplayName: null,
        OwnerKey: null,
        Password: null,
        Reference: null,
        ReferralCode: null,
        ReferralUrl: null,
        RegionAreaKey: null,
        RegionAreaName: null,
        RegionKey: null,
        RegionName: null,
        RegistrationSourceCode: null,
        RegistrationSourceName: null,
        RequestKey: null,
        RoleKey: null,
        RoleName: null,
        SecondaryPassword: null,
        SystemPassword: null,
        UserName: null,
        WebsiteUrl: null
    };

    public _DealDetails: any =
        {
        }

    GetDealDetails() {
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetDeal,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveAccountId,
            AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.IsFormProcessing = false;
                    this._DealDetails = _Response.Result;

                    this.Form_EditUser.controls['AccountKey'].setValue(this._DealDetails.AccountKey);
                    this.Form_EditUser.controls['AccountId'].setValue(this._DealDetails.AccountId);
                    this.Form_EditUser.controls['CategoryKey'].setValue(this._DealDetails.CategoryKey);

                    this.merchantPlaceholder = this._DealDetails.AccountDisplayName;
                    this.categoryPlaceholder = this._DealDetails.CategoryName;

                    this.showPickers = false;
                    this._ChangeDetectorRef.detectChanges();

                    this.GetCategories_List();
                    this.GetMerchants_List();

                    this.showPickers = true;
                    this._ChangeDetectorRef.detectChanges();

                }
                else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    //#endregion

    printform(): void {
        this.ReFormat_RequestBody();
    }

    ReFormat_RequestBody(): void {
        var formValue: any = cloneDeep(this.Form_EditUser.value);
        var formRequest: any = {
            'OperationType': 'new',
            'Task': 'updatedeal',
            "AccountId": formValue.AccountId,
            "AccountKey": formValue.AccountKey,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            "TypeCode": "deal",
            "Title": formValue.Title,
            "Description": formValue.Description,
            "Terms": formValue.Terms,
            "StartDate": formValue.StartDate,
            "EndDate": formValue.EndDate,

            CodeValidityDays: "10",
            // CodeValidityStartDate: "2020-11-04 00:00:00",
            CodeValidityEndDate: "2020-11-14 00:00:00",
            UsageTypeCode: "hour",

            "ActualPrice": formValue.ActualPrice,
            "SellingPrice": formValue.Amount,
            "DiscountAmount": formValue.DiscountAmount,

            "DiscountPercentage": 0,

            "Amount": formValue.Amount,
            "Charge": 0.0,
            "CommissionAmount": formValue.CommissionAmount,
            "TotalAmount": 0.0,

            "MaximumUnitSale": formValue.MaximumUnitSale,
            "MaximumUnitSalePerDay": formValue.MaximumUnitSalePerDay,
            "MaximumUnitSalePerPerson": formValue.MaximumUnitSalePerPerson,
            "CategoryKey": formValue.CategoryKey,
            "Schedule": [

            ]
        };

        if (this.NiraOrPercent.DiscountAmount == '0') {
            formRequest.DiscountPercentage = formValue.DiscountAmount;
            formRequest.DiscountAmount = (formValue.DiscountAmount / 100) * formValue.ActualPrice;
        } else if (this.NiraOrPercent.DiscountAmount == '1' && formValue.ActualPrice != 0.0) {
            formRequest.DiscountPercentage = (formValue.DiscountAmount / formValue.ActualPrice) * 100;
        }

        if (this.NiraOrPercent.CommissionAmount == '0') {
            formRequest.CommissionAmount = (formValue.CommissionAmount / 100) * formValue.ActualPrice;
        }

        //#region set image 

        if (this._HelperService._Icon_Cropper_Data.Content != null) {
            formRequest.Images = [
                {
                    "ImageContent": this._HelperService._Icon_Cropper_Data,
                    "IsDefault": 1
                }
            ];
        }

        //#endregion

        //#region Set Title (not required)

        // switch (formValue.DealTitle) {
        //     case '0': {
        //         formRequest.Title =
        //             formValue.ActualPrice + " " + formRequest.Title + ' for ' +
        //             formValue.SellingPrice + ' at ' + this.SelectedMerchant.DisplayName;
        //     }

        //         break;
        //     case '1': {
        //         formRequest.Title = formValue.DiscountAmount + ' off on ' + formRequest.Title;
        //     }

        //         break;
        //     case '2': {
        //         formRequest.Title = ((formValue.DiscountAmount / formValue.ActualPrice) * 100) + '% off on ' + formRequest.Title;
        //     }

        //         break;

        //     default:
        //         break;
        // }

        //#endregion 

        //#region Add store locations 

        if (!this.IsAllStores) {

            formRequest.Locations = [
            ];

            for (let index = 0; index < this.StoresList.length; index++) {
                const element = this.StoresList[index];

                formRequest.Locations.push({
                    ReferenceId: element.ReferenceId,
                    ReferenceKey: element.ReferenceKey
                });
            }
        }

        //#endregion

        //#region Add Redeem schedule days 

        for (let index = 0; index < this.WeekDays.length; index++) {
            const element = this.WeekDays[index];

            if (element.selected) {
                // formRequest.Schedule[0].DayOfWeek.push(element.name);
                formRequest.Schedule.push({ DayOfWeek: index });
            }

        }

        //#endregion

        //#region Set Redeem Schedule Timing 

        for (let index = 0; index < formRequest.Schedule.length; index++) {

            if (this.IsValidAllTime) {
                formRequest.Schedule[index].StartHour = '00:00';
                formRequest.Schedule[index].EndHour = '23:59';
                formRequest.Schedule[index].Type = 'dealredeemshedule';
            } else {
                formRequest.Schedule[index].StartHour = moment(this.ValidTimings.Start).format('hh:mm');
                formRequest.Schedule[index].EndHour = moment(this.ValidTimings.End).format('hh:mm');
                formRequest.Schedule[index].Type = 'dealredeemshedule';
            }
        }

        //#endregion

        //#region Set Schedule 

        for (let index = 0; index < 7; index++) {

            if (this.IsRunAllTime) {
                formRequest.Schedule.push({
                    DayOfWeek: index,
                    StartHour: '00:00',
                    EndHour: '23:59',
                    Type: 'dealshedule'
                });
            } else {
                formRequest.Schedule.push({
                    DayOfWeek: index,
                    StartHour: moment(this.DealTimings.Start).format('hh:mm'),
                    EndHour: moment(this.DealTimings.End).format('hh:mm'),
                    Type: 'dealshedule'
                });

            }
        }

        //#endregion

        //#region Set All Stores 



        //#endregion

        //#region Set Coupon Validity 

        switch (formValue.CouponValidity) {
            case '0': {
                formRequest.CodeValidityDays = this.Expiry.Days * 24;
                formRequest.UsageTypeCode = 'hour'
            }

                break;
            case '1': {
                formRequest.CodeValidityDays = this.Expiry.Hours;
                formRequest.UsageTypeCode = 'hour'
            }

                break;
            case '2': {
                formRequest.CodeValidityEndDate = moment(formRequest.EndDate);
                formRequest.UsageTypeCode = 'date';
            }

                break;
            case '3': {
                formRequest.CodeValidityEndDate = moment(this.Expiry.Date);
                formRequest.UsageTypeCode = 'date';
            }

                break;

            default:
                break;
        }

        //#endregion

        return formRequest;

    }


    //#region Run Time 

    IsRunAllTime: boolean = true;
    RunAllTimeToogle(): void {
        this.IsRunAllTime = !(this.IsRunAllTime);
    }

    //#endregion

    //#region Redeem Time 

    IsValidAllTime: boolean = true;
    ValidAllTimeToogle(): void {
        this.IsValidAllTime = !(this.IsValidAllTime);
    }

    //#endregion

    //#region All Stores 

    IsAllStores: boolean = true;
    AllStoresToogle(): void {
        this.IsAllStores = !(this.IsAllStores);
    }

    //#endregion

    toogleWeekDay(index: number): void {
        this.WeekDays[index].selected = !this.WeekDays[index].selected;
    }

    ChangeExpiryDate(event): void {
        this.Expiry.Date = event.end;
    }

    removeImage(): void {
        this.CurrentImagesCount = 0;
        this._HelperService.Icon_Crop_Clear();
    }

}

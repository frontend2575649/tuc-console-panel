import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from "../../../../service/service";;
import swal from 'sweetalert2';
declare var $: any;
import * as Feather from 'feather-icons';
declare var google: any;
declare var moment: any;
import * as cloneDeep from 'lodash/cloneDeep';
import { InputFileComponent, InputFile } from 'ngx-input-file';
import * as _ from 'lodash';
import { Select2OptionData } from 'ng2-select2';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { MatDialog } from '@angular/material';
import { FileManagerComponent } from 'src/app/modules/file-manager/file-manager.component';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { T } from '@angular/cdk/keycodes';
// import { ImageCroppedEvent } from "ngx-image-cropper”;

@Component({
    selector: 'app-tueditdeal',
    templateUrl: './tueditdeal.component.html'
})
export class EditDealComponent implements OnInit {
    dealType;
    isStorePickup = false;
    isDeliverToAddress = false;
    Package;
    Form_AddPackage: FormGroup;
    SelectedImage: any;

    public ckConfig =
        {
            toolbar: ['Bold', 'Italic', 'NumberedList', 'BulletedList'],
            height: 300
        }
    public Editor = ClassicEditor;
    public data = "";
    _ImageManager =
        {
            TCroppedImage: null,
            ActiveImage: null,
            ActiveImageName: null,
            ActiveImageSize: null,
            Option: {
                MaintainAspectRatio: "false",
                MinimumWidth: 600,
                MinimumHeight: 500,
                MaximumWidth: 600,
                MaximumHeight: 500,
                ResizeToWidth: 600,
                ResizeToHeight: 500,
                Format: "jpeg",
            }
        }
    public Hours = [];
    public Days = [];

    Type_Data = ['box', "envelope", "soft-packaging"]

    _DealConfig =
        {
            DefaultStartDate: null,
            DefaultEndDate: null,
            SelectedDealCodeHours: 12,
            SelectedDealCodeDays: 30,
            SelectedDealCodeEndDate: 0,
            DealCodeValidityTypeCode: 'daysafterpurchase',
            StartDate: null,
            EndDate: null,
            DealImages: [],
            SavedDealImages: [],
            SelectedTitle: 3,
            Images: [],
            StartDateConfig: {
            },
            EndDateConfig: {
            },
            DealCodeEndDateConfig: {
            }
        };

    Form_AddDeal: FormGroup;

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _dialog: MatDialog,
    ) {
    }



    ngOnInit(): void {
        Feather.replace();
        for (let index = 1; index < 24; index++) {
            this.Hours.push(index);
        }
        for (let index = 1; index < 366; index++) {
            this.Days.push(index);
        }
        this.GetMerchants_List();
        // this.getCategorieslist();
        this._DealConfig.DefaultStartDate = moment().startOf('day');
        this._DealConfig.DefaultEndDate = moment().add(1, 'weeks').endOf("day");
        this._DealConfig.StartDate = this._DealConfig.DefaultStartDate;
        this._DealConfig.EndDate = this._DealConfig.DefaultEndDate;
        this._DealConfig.StartDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            parentEl: '#adddeal_startdate',
            startDate: this._HelperService.DateInUTC(moment().startOf("day")),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            parentEl: '#adddeal_enddate',
            startDate: this._HelperService.DateInUTC(moment().startOf("day")),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        this._DealConfig.DealCodeEndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: this._DealConfig.EndDate,
            minDate: this._DealConfig.EndDate,
        };
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];

            this._HelperService.AppConfig.ActiveAccountKey = params["accountkey"];
            this._HelperService.AppConfig.ActiveAccountId = params["accountid"];

        });
        this.Form_AddUser_Load();
        this.GetDealCategories_List();
        this.Form_AddDeal_Load();
        this.Form_AddPackage_Load();
    }

    Form_AddPackage_Load() {
        this.Form_AddPackage = this._FormBuilder.group({
            Title: [null,],
            Type: [null, Validators.compose([])],
            Height: [null, Validators.compose([Validators.min(1)])],
            Weight: [null, Validators.compose([Validators.min(0)])],
            Length: [null, Validators.compose([Validators.min(1)])],
            Width: [null, Validators.compose([Validators.required, Validators.min(1)])],
        });
    }
    openDialog() {
        this._HelperService.OpenModal('Form_Add_Deal');
    }

    Form_AddDeal_Close() {
        this._HelperService.CloseModal("Form_AddDeal");
    }
    Form_AddDeal_Load() {

        this.Form_AddDeal = this._FormBuilder.group({
            Title: [null],
            Description: [null, Validators.compose([Validators.required])],
            Type: [null],
            Height: [null],
            Weight: [null, Validators.compose([Validators.required,])],
            Length: [null],
            Width: [null],
        });
    }

    public packagingData: any = {};

    Form_AddDeal_Save() {
        const data = this.Form_AddDeal.value;
        this.packagingData = data;
        this._HelperService.CloseModal("Form_Add_Deal");
    }


    IsDealLoaded = false;
    public _DealDetails: any = {};
    GetDeal() {
        this.IsDealLoaded = false;
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetDeal,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveAccountId,
            AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.IsDealLoaded = true;
                    this._HelperService.IsFormProcessing = false;
                    // this._DealDetails = _Response.Result;
                    this._DealDetails = this._HelperService.HCXGetDateComponent(_Response.Result);
                    this.Form_AddUser.controls['TitleContent'].patchValue(this._DealDetails.TitleContent);
                    this._Titles.OriginalTitle = this._DealDetails.TitleContent;
                    this.Form_AddUser.controls['AccountKey'].setValue(this._DealDetails.AccountKey);
                    this.Form_AddUser.controls['AccountId'].setValue(this._DealDetails.AccountId);
                    this.Form_AddUser.controls['SubCategoryKey'].setValue(this._DealDetails.SubCategoryKey);
                    this.MerchantPlaceHolder = this._DealDetails.AccountDisplayName;
                    this.GetMerchants_List();
                    this.SelectedMerchant.ReferenceId = this._DealDetails.AccountId;
                    this.getCategorieslist();
                    this.Form_AddUser.controls['TitleTypeId'].setValue(this._DealDetails.TitleTypeId);
                    this.Form_AddUser.controls['Description'].setValue(this._DealDetails.Description);
                    this.Form_AddUser.controls['Terms'].setValue(this._DealDetails.Terms);
                    this.Form_AddUser.controls['MaximumUnitSale'].setValue(this._DealDetails.MaximumUnitSale);
                    this.Form_AddUser.controls['MaximumUnitSalePerPerson'].setValue(this._DealDetails.MaximumUnitSalePerPerson);
                    this.Form_AddUser.controls['Terms'].setValue(this._DealDetails.Terms);
                    this.Form_AddUser.controls['ActualPrice'].setValue(this._DealDetails.ActualPrice);
                    this.Form_AddUser.controls['SellingPrice'].setValue(this._DealDetails.SellingPrice);
                    this.Form_AddUser.controls['TUCPercentage'].setValue(this._DealDetails.CommissionPercentage);
                    this.Form_AddUser.controls['StartDate'].setValue(this._DealDetails.StartDatePart.DateTime);
                    this.Form_AddUser.controls['EndDate'].setValue(this._DealDetails.EndDatePart.DateTime);
                    this.dealType = this._DealDetails.DealTypeCode;

                    if (this._DealDetails.DeliveryTypeCode === 'deliverytype.instoreanddelivery') {
                        this.isDeliverToAddress = true;
                        this.isStorePickup = true;
                    }
                    if (this._DealDetails.DeliveryTypeCode === 'deliverytype.instore') {
                        this.isStorePickup = true;
                    }

                    if (this._DealDetails.DeliveryTypeCode === 'deliverytype.delivery') {
                        this.isDeliverToAddress = true;
                    }

                    if (this._DealDetails.Packaging) {
                        this.Form_AddDeal.controls['Title'].setValue(this._DealDetails.Packaging.Name);
                        this.Form_AddDeal.controls['Description'].setValue(this._DealDetails.Packaging.ParcelDescription);
                        this.Form_AddDeal.controls['Type'].setValue(this._DealDetails.Packaging.Type);
                        this.Form_AddDeal.controls['Height'].setValue(this._DealDetails.Packaging.Height);
                        this.Form_AddDeal.controls['Weight'].setValue(this._DealDetails.Packaging.Weight);
                        this.Form_AddDeal.controls['Length'].setValue(this._DealDetails.Packaging.Length);
                        this.Form_AddDeal.controls['Width'].setValue(this._DealDetails.Packaging.Width);
                    }
                    if (this._DealDetails.PackagingCategory) {
                        this.Form_AddDeal.controls['Description'].setValue(this._DealDetails.PackagingCategory.ParcelDescription);
                    }
                    if (this._DealDetails.Images != undefined && this._DealDetails.Images != null && this._DealDetails.Images.length > 0) {
                        this._DealConfig.SavedDealImages = this._DealDetails.Images;
                    }
                    this.SelectedMerchant =
                    {
                        ReferenceId: this._DealDetails.AccountId,
                        ReferenceKey: this._DealDetails.AccountKey,
                        DisplayName: this._DealDetails.AccountDisplayName,
                    };

                    this._SelectedSubCategory =
                    {
                        ReferenceId: this._DealDetails.SubCategoryId,
                        ReferenceKey: this._DealDetails.SubCategoryKey,
                        Fee: this._DealDetails.SubCategoryFee,
                        Name: this._DealDetails.SubCategoryName,
                        IconUrl: null,

                        ParentCategoryId: this._DealDetails.CategoryId,
                        ParentCategoryKey: this._DealDetails.CategoryKey,
                        ParentCategoryName: this._DealDetails.CategoryName,
                    };
                    this._AmountDistribution =
                    {
                        ActualPrice: this._DealDetails.ActualPrice,
                        SellingPrice: this._DealDetails.SellingPrice,
                        SellingPricePercentage: this._DealDetails.DiscountPercentage,
                        SellingPriceDifference: this._DealDetails.DiscountAmount,
                        TUCPercentage: this._DealDetails.CommissionPercentage,
                        OrignalTUCPercentage: this._DealDetails.CommissionPercentage,
                        TUCAmount: this._DealDetails.CommissionAmount,
                        MerchantAmount: this._HelperService.GetFixedDecimalNumber((this._DealDetails.SellingPrice - this._DealDetails.CommissionAmount)),
                        MerchantPercentage: this._HelperService.GetPercentageFromNumber(this._DealDetails.MerchantAmount, this._DealDetails.SellingPrice),
                    };

                    this._DealConfig.SelectedTitle = this._DealDetails.TitleTypeId;

                    this._DealConfig.StartDate = this._DealDetails.StartDatePart.Object;
                    this._DealConfig.EndDate = this._DealDetails.EndDatePart.Object;
                    this._DealConfig.DefaultStartDate = this._DealConfig.StartDate;
                    this._DealConfig.DefaultEndDate = this._DealConfig.EndDate;

                    this._DealConfig.StartDateConfig = {
                        autoUpdateInput: false,
                        singleDatePicker: true,
                        timePicker: true,
                        locale: { format: "DD-MM-YYYY" },
                        alwaysShowCalendars: false,
                        showDropdowns: true,
                        parentEl: '#adddeal_startdate',
                        startDate: this._DealConfig.StartDate,
                        endDate: this._DealConfig.StartDate,
                        minDate: moment(),
                    };
                    this._DealConfig.EndDateConfig = {
                        autoUpdateInput: false,
                        singleDatePicker: true,
                        timePicker: true,
                        locale: { format: "DD-MM-YYYY" },
                        alwaysShowCalendars: false,
                        showDropdowns: true,
                        parentEl: '#adddeal_enddate',
                        startDate: this._DealConfig.EndDate,
                        endDate: this._DealConfig.EndDate,
                        minDate: moment(),
                    };

                    if (this._DealDetails.CodeValidityEndDate != undefined && this._DealDetails.CodeValidityEndDate != null) {
                        this._DealConfig.SelectedDealCodeEndDate = this._DealDetails.CodeValidityEndDatePart.Object;
                        this.Form_AddUser.controls['CodeValidityEndDate'].setValue(this._DealDetails.CodeValidityEndDatePart.DateTime);
                        this._DealConfig.DealCodeEndDateConfig = {
                            autoUpdateInput: false,
                            singleDatePicker: true,
                            timePicker: true,
                            locale: { format: "DD-MM-YYYY" },
                            alwaysShowCalendars: false,
                            showDropdowns: true,
                            parentEl: '#adddeal_enddate',
                            startDate: this._DealDetails.CodeValidityEndDatePart.Object,
                            minDate: this._DealDetails.CodeValidityEndDatePart.Object,
                        };
                    }
                    else {
                        this._DealConfig.DealCodeEndDateConfig = {
                            autoUpdateInput: false,
                            singleDatePicker: true,
                            timePicker: true,
                            locale: { format: "DD-MM-YYYY" },
                            alwaysShowCalendars: false,
                            showDropdowns: true,
                            startDate: this._DealConfig.EndDate,
                            minDate: this._DealConfig.EndDate,
                        };
                    }
                    this._DealConfig.DealCodeValidityTypeCode = this._DealDetails.UsageTypeCode;
                    this.Form_AddUser.controls['CodeUsageTypeCode'].setValue(this._DealDetails.UsageTypeCode);
                    if (this._DealConfig.DealCodeValidityTypeCode == "hour") {
                        this._DealConfig.SelectedDealCodeHours = this._DealDetails.CodeValidityDays;
                    }
                    if (this._DealConfig.DealCodeValidityTypeCode == "daysafterpurchase") {
                        this._DealConfig.SelectedDealCodeDays = Math.round(this._DealDetails.CodeValidityDays / 24);
                    }
                    this.ProcessAmounts();
                }
                else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    //#region  Form Manager
    Form_AddUser_Images = [];
    Form_AddUser: FormGroup;
    Form_AddUser_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_AddUser = this._FormBuilder.group({
            AccountId: [null, Validators.required],
            AccountKey: [null, Validators.required],
            SubCategoryKey: [null, Validators.required],
            TitleTypeId: [null, Validators.required], //transient
            TitleContent: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            Description: [null, Validators.compose([Validators.required, Validators.minLength(80)])],
            Terms: [null],
            MaximumUnitSale: [null, Validators.compose([Validators.required, Validators.min(1)])],
            MaximumUnitSalePerPerson: [null, Validators.compose([Validators.required, Validators.min(0)])],
            ActualPrice: [null, Validators.compose([Validators.required, Validators.min(1)])],
            SellingPrice: [null, Validators.compose([Validators.required, Validators.min(1)])],
            StartDate: this._DealConfig.DefaultStartDate.format('DD-MM-YYYY hh:mm a'),
            EndDate: this._DealConfig.DefaultEndDate.format('DD-MM-YYYY hh:mm a'),
            CodeUsageTypeCode: "daysafterpurchase", //transient
            TImage: null,
            TUCPercentage: null,
            CodeValidityEndDate: this._DealConfig.DefaultEndDate.format('DD-MM-YYYY hh:mm a'),
        });
    }

    Pushed = [];
    Form_AddUser_Process(_FormValue: any, IsPublish: string) {

        if (this.dealType == undefined || this.dealType == null) {
            this._HelperService.NotifyError("Select Deal type");
            return;
        }
        // if (this.isDeliverToAddress && this.packagingData.selectedProduct == undefined || this.packagingData.selectedProduct == null) {
        //     this._HelperService.NotifyError("Select Package");
        //     return;
        // }

        if (this.SelectedMerchant == undefined || this.SelectedMerchant == null) {
            this._HelperService.NotifyError("Select deal merchant");
            return;
        }
        else if (this._SelectedSubCategory == undefined || this._SelectedSubCategory == null || this._SelectedSubCategory.ReferenceId == undefined || this._SelectedSubCategory.ReferenceId == null || this._SelectedSubCategory.ReferenceId == 0) {
            this._HelperService.NotifyError("Select deal category");
            return;
        }
        else if (_FormValue.ActualPrice == undefined || _FormValue.ActualPrice == null || _FormValue.ActualPrice == undefined || _FormValue.ActualPrice == 0) {
            this._HelperService.NotifyError("Enter original price");
            return;
        }
        else if (_FormValue.SellingPrice == undefined || _FormValue.SellingPrice == null || _FormValue.SellingPrice == undefined || _FormValue.SellingPrice == 0) {
            this._HelperService.NotifyError("Enter selling price");
            return;
        }
        else if (_FormValue.SellingPrice <= 0) {
            this._HelperService.NotifyError("Actual price must be greater than 0");
            return;
        }
        else if (_FormValue.ActualPrice <= 0) {
            this._HelperService.NotifyError("Selling price must be greater than or equal 0");
            return;
        }
        else if (_FormValue.MaximumUnitSalePerPerson > _FormValue.MaximumUnitSale) {
            this._HelperService.NotifyError("Maximum purchase per person must be less than maximum purchase");
            return;
        }
        else if (_FormValue.SellingPrice > _FormValue.ActualPrice) {
            this._HelperService.NotifyError("Selling price must be less than or equal to actual price");
            return;
        }
        // else if (this._DealConfig.DealImages == undefined || this._DealConfig.DealImages == null || this._DealConfig.DealImages.length == 0) {
        //     this._HelperService.NotifyError("Atleast 1 image is required for adding deal");
        // }
        else if (this._DealConfig.StartDate == undefined || this._DealConfig.StartDate == null) {
            this._HelperService.NotifyError("Deal start date required");
            return;
        }
        else if (this._DealConfig.EndDate == undefined || this._DealConfig.EndDate == null) {
            this._HelperService.NotifyError("Deal end date required");
            return;
        }
        else if (this._DealConfig.EndDate < moment()) {
            this._HelperService.NotifyError("Deal end Date is greater than current date");
            return;
        }
        else {
            var Title = this._Titles.Title3;
            if (this._DealConfig.SelectedTitle == 1) {
                Title = this._Titles.Title1;
            }
            else if (this._DealConfig.SelectedTitle == 2) {
                Title = this._Titles.Title2;
            }
            else {
                Title = this._Titles.Title3;
            }
            var _PostData: any =
            {
                Task: "updatedeal",
                ReferenceId: this._DealDetails.ReferenceId,
                ReferenceKey: this._DealDetails.ReferenceKey,
                AccountId: this.SelectedMerchant.ReferenceId,
                AccountKey: this.SelectedMerchant.ReferenceKey,
                Title: Title,
                TitleContent: _FormValue.TitleContent,
                TitleTypeId: _FormValue.TitleTypeId,
                Description: _FormValue.Description,
                Terms: _FormValue.Terms,
                StartDate: this._HelperService.HCXConvertDate(this._DealConfig.StartDate),
                EndDate: this._HelperService.HCXConvertDate(this._DealConfig.EndDate),
                CodeUsageTypeCode: _FormValue.CodeUsageTypeCode,
                CodeValidityDays: this._DealConfig.SelectedDealCodeDays,
                CodeValidityHours: this._DealConfig.SelectedDealCodeHours,
                CodeValidityEndDate: null,
                ActualPrice: _FormValue.ActualPrice,
                SellingPrice: _FormValue.SellingPrice,
                MaximumUnitSale: _FormValue.MaximumUnitSale,
                MaximumUnitSalePerPerson: _FormValue.MaximumUnitSalePerPerson,
                SubCategoryKey: this._SelectedSubCategory.ReferenceKey,
                // StatusCode: 'deal.draft',
                SendNotification: false,
                Images: [],
                Locations: [],
                Shedule: [],
                DealTypeCode: this.dealType
            };
            // if (this.dealType === 'dealtype.product') {

            //     if (this.isStorePickup && this.isDeliverToAddress) {
            //         _PostData.DeliveryTypeCode = 'deliverytype.instoreanddelivery'
            //     }
            //     if (this.isDeliverToAddress) {
            //         _PostData.DeliveryTypeCode = 'deliverytype.delivery'
            //     }
            //     if (this.isStorePickup) {
            //         _PostData.DeliveryTypeCode = 'deliverytype.instore'
            //     }
            // }
            if (this.dealType === 'dealtype.product') {

                if (this.isStorePickup && this.isDeliverToAddress) {
                    _PostData.DeliveryTypeCode = 'deliverytype.instoreanddelivery'
                }
                if (this.isDeliverToAddress && !this.isStorePickup) {
                    _PostData.DeliveryTypeCode = 'deliverytype.delivery'
                }
                if (this.isStorePickup && !this.isDeliverToAddress) {
                    _PostData.DeliveryTypeCode = 'deliverytype.instore'
                }
            }
            if (this.dealType === 'dealtype.product' && this.isDeliverToAddress) {
                _PostData.Packaging = {};
                _PostData.Packaging.Name = this.Form_AddDeal.value.Title;
                _PostData.Packaging.ParcelDescription = this.Form_AddDeal.value.Description;
                _PostData.Packaging.Type = this.Form_AddDeal.value.Type;
                _PostData.Packaging.Height = this.Form_AddDeal.value.Height || 1;
                _PostData.Packaging.Weight = this.Form_AddDeal.value.Weight;
                _PostData.Packaging.Length = this.Form_AddDeal.value.Length || 1;
                _PostData.Packaging.Width = this.Form_AddDeal.value.Width || 1;
                _PostData.Packaging.Size_unit = 'cm';
                _PostData.Packaging.Weight_unit = 'kg';
                if (this.packagingData.selectedProduct) {
                    _PostData.Packaging.PackagingId = this.packagingData.selectedProduct.PackagingId;
                }
                // debugger;
                // if (this.subCategoryData.length > 0) {
                //     _PostData.Packaging.ReferenceId = this.selectedSubCategory.ReferenceId;
                //     _PostData.Packaging.ReferenceKey = this.selectedSubCategory.ReferenceKey;
                // } else {
                //     _PostData.Packaging.ReferenceId = this.packagingData.selectedProduct.ReferenceId;
                //     _PostData.Packaging.ReferenceKey = this.packagingData.selectedProduct.ReferenceKey;
                // }
            }
            if (_PostData.CodeUsageTypeCode == 'dealenddate') {
                _PostData.CodeValidityEndDate = this._DealConfig.EndDate;
            }
            if (_PostData.CodeUsageTypeCode == 'date') {
                _PostData.CodeValidityEndDate = this._HelperService.HCXConvertDate(this._DealConfig.SelectedDealCodeEndDate);
            }
            this._DealConfig.DealImages.forEach(element => {
                element.ImageContent.OriginalContent = null;
                _PostData.Images.push(element);
            });
            if (this._AmountDistribution.TUCPercentage != this._AmountDistribution.OrignalTUCPercentage) {
                _PostData.TUCPercentage = this._AmountDistribution.TUCPercentage;
            }
            if (IsPublish) {
                _PostData.StatusCode = 'deal.published';
            } else {
                if (this._DealDetails.StatusCode == 'deal.rejected') {
                    _PostData.StatusCode = 'deal.draft';
                }
            }
            this._HelperService.IsFormProcessing = true;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, _PostData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        // this._HelperService.NotifySuccess(_Response.Message);
                        if (IsPublish) {
                            this._HelperService.NotifySuccess("Deal Published Successfully. It will take upto 5 minutes to update changes.");
                            this._DealConfig.DealImages = []
                        }
                        else {
                            this._HelperService.NotifySuccess("Deal details updated successfully");
                            this._DealConfig.DealImages = []
                        }
                        this.ngOnInit()
                        // this.GetDeal();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    Form_AddUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deals]);
    }
    //#endregion
    //#region Amount Distribution
    _AmountDistribution =
        {
            ActualPrice: null,
            SellingPrice: null,
            SellingPricePercentage: null,
            SellingPriceDifference: null,
            TUCPercentage: null,
            OrignalTUCPercentage: null,
            TUCAmount: null,
            MerchantPercentage: null,
            MerchantAmount: null,
        };
    ProcessAmounts() {
        var tAmount = this._AmountDistribution;
        if (tAmount.ActualPrice != undefined && tAmount.ActualPrice != null && isNaN(tAmount.ActualPrice) == false && tAmount.ActualPrice >= 0) {
            if (tAmount.SellingPrice != undefined && tAmount.SellingPrice != null && isNaN(tAmount.SellingPrice) == false && tAmount.SellingPrice >= 0) {
                if (tAmount.ActualPrice >= tAmount.SellingPrice) {
                    tAmount.SellingPriceDifference = this._HelperService.GetFixedDecimalNumber(tAmount.ActualPrice - tAmount.SellingPrice);
                    tAmount.SellingPricePercentage = this._HelperService.GetPercentageFromNumber(tAmount.SellingPriceDifference, tAmount.ActualPrice);
                    tAmount.TUCAmount = this._HelperService.GetAmountFromPercentage(tAmount.SellingPrice, tAmount.TUCPercentage);
                    tAmount.MerchantAmount = this._HelperService.GetFixedDecimalNumber(tAmount.SellingPrice - tAmount.TUCAmount);
                    tAmount.MerchantPercentage = this._HelperService.GetPercentageFromNumber(tAmount.MerchantAmount, tAmount.SellingPrice);
                }
                else {
                    if (parseFloat(this._AmountDistribution.SellingPrice) >= parseFloat(this._AmountDistribution.ActualPrice)) {
                        setTimeout(() => {
                            this._AmountDistribution =
                            {
                                ActualPrice: this._AmountDistribution.ActualPrice,
                                SellingPrice: null,
                                SellingPricePercentage: null,
                                SellingPriceDifference: null,
                                TUCPercentage: this._AmountDistribution.TUCPercentage,
                                OrignalTUCPercentage: this._AmountDistribution.OrignalTUCPercentage,
                                TUCAmount: null,
                                MerchantPercentage: null,
                                MerchantAmount: null,
                            };
                            tAmount.SellingPriceDifference = this._HelperService.GetFixedDecimalNumber(tAmount.ActualPrice - 0);
                            tAmount.SellingPricePercentage = this._HelperService.GetPercentageFromNumber(tAmount.SellingPriceDifference, tAmount.ActualPrice);
                            tAmount.TUCAmount = this._HelperService.GetAmountFromPercentage(0, tAmount.TUCPercentage);
                            tAmount.MerchantAmount = this._HelperService.GetFixedDecimalNumber(0 - tAmount.TUCAmount);
                            tAmount.MerchantPercentage = this._HelperService.GetPercentageFromNumber(tAmount.MerchantAmount, 0);
                        }, 200);
                    }
                }
            }
        }
        this.ProcessTitle();
    }
    //#endregion

    //#region Title Manager
    _Titles =
        {
            OriginalTitle: "",
            Title1: this._HelperService.UserCountry.CurrencyNotation + " X deal title for " + this._HelperService.UserCountry.CurrencyNotation + " Y",
            Title2: this._HelperService.UserCountry.CurrencyNotation + " Y off on deal title",
            Title3: "x% off on deal title"
        }
    ProcessTitle() {
        this._Titles.Title1 = this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.ActualPrice) + " " + this._Titles.OriginalTitle + " for " + this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.SellingPrice);
        this._Titles.Title2 = this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.SellingPriceDifference) + " off on " + this._Titles.OriginalTitle;
        this._Titles.Title3 = Math.round(this._AmountDistribution.SellingPricePercentage) + "% off on " + this._Titles.OriginalTitle;
        // this.Form_AddDeal.patchValue({
        //     Description: `${this._SelectedSubCategory.Name} , ${this.Form_AddDeal.value.Title} , ${this._Titles.OriginalTitle}`,
        // })

        let description = '';
        if (this._SelectedSubCategory.Name) {
            description = `${description} ${this._SelectedSubCategory.Name}`
        } if (this.Form_AddDeal.value.Title) {
            description = (this._SelectedSubCategory.Name) ? `${description} , ${this.Form_AddDeal.value.Title}` : `${this.Form_AddDeal.value.Title}`
        }
        if (this._Titles.OriginalTitle) {
            description = (this.Form_AddDeal.value.Title) ? `${description} , ${this._Titles.OriginalTitle}` : `${this._Titles.OriginalTitle}`
        }

        this.Form_AddDeal.patchValue({
            Description: description
        })
    }
    //#endregion
    //#region Deal Shedule Manager 
    ScheduleStartDateRangeChange(value) {
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: value.start,
            minDate: value.start,
        };
        this._DealConfig.StartDate = value.start;
        this._DealConfig.EndDate = value.start;
        this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                StartDate: value.start.format('DD-MM-YYYY hh:mm a'),
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    ScheduleEndDateRangeChange(value) {
        this._DealConfig.DealCodeEndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: value.start,
            minDate: value.start,
        };
        this._DealConfig.EndDate = value.start;
        this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    ScheduleCodeEndDateRangeChange(value) {
        this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    //#endregion

    //#region Image Manager
    onImageAccept(value) {
        setTimeout(() => {
            this.Form_AddUser.patchValue(
                {
                    TImage: null,
                }
            );
        }, 300);
        this._ImageManager.ActiveImage = value;
        this._ImageManager.ActiveImageName = value.file.name;
        this._ImageManager.ActiveImageName = value.file.size;
    }
    Icon_B64Cropped(base64: string) {
        this._ImageManager.TCroppedImage = base64;
    }
    Icon_B64CroppedDone() {
        var ImageDetails = this._HelperService.GetImageDetails(this._ImageManager.TCroppedImage);
        var ImageItem =
        {
            OriginalContent: this._ImageManager.TCroppedImage,
            Name: this._ImageManager.ActiveImageName,
            Size: this._ImageManager.ActiveImageSize,
            Extension: ImageDetails.Extension,
            Content: ImageDetails.Content
        };
        if (this._DealConfig.DealImages.length == 0 && this._DealConfig.SavedDealImages.length == 0) {
            this._DealConfig.DealImages.push(
                {
                    ImageContent: ImageItem,
                    IsDefault: 1,
                }
            );
        }
        else {
            this._DealConfig.DealImages.push(
                {
                    ImageContent: ImageItem,
                    IsDefault: 0,
                }
            );
        }
        this._ImageManager.TCroppedImage = null;
        this._ImageManager.ActiveImage = null;
        this._ImageManager.ActiveImageName = null;
        this._ImageManager.ActiveImageSize = null;
    }
    Icon_Crop_Clear() {
        this._ImageManager.TCroppedImage = null;
        this._ImageManager.ActiveImage = null;
        this._ImageManager.ActiveImageName = null;
        this._ImageManager.ActiveImageSize = null;
        this._HelperService.CloseModal('_Icon_Cropper_Modal');
    }


    RemoveSecondaryImage(TItem) {
        this.ImageIconUrl = null;
        this.Icon_Crop_Clear();
    }
    RemoveImage(Item) {
        this._DealConfig.DealImages = this._DealConfig.DealImages.filter(x => x != Item);
    }

    openFilemanager() {
        let dialogRef = this._dialog.open(FileManagerComponent, {
            width: '1200px',
            height: '750px',
            maxWidth: '1200px',
            maxHeight: '800px',
            disableClose: true,
            data: {
                merchantId: this.SelectedMerchant.ReferenceKey
            }
        });
        dialogRef.afterClosed().subscribe(res => {
            if (res.data) {
                this._DealConfig.DealImages.push(
                    {
                        reference: res.data.reference,
                        url: res.data.url,
                    }
                );
            }
        });
    }

    RemoveDealImage(Item) {
        swal({
            position: "center",
            title: "Remove deal image?",
            text: "Image cannot be recovered once deleted. Do you want to continue?",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {

                this._HelperService.IsFormProcessing = true;
                var PData =
                {
                    Task: this._HelperService.AppConfig.Api.ThankUCash.deletedealimage,
                    ReferenceId: Item.ReferenceId,
                    ReferenceKey: Item.ReferenceKey,
                }

                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, PData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess(_Response.Message);
                            this._DealConfig.SavedDealImages = this._DealConfig.SavedDealImages.filter(x => x != Item);
                            // this._HelperService.CloseModal('FlashDeal');
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    }
                    ,
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                        this._HelperService.ToggleField = false;
                    });

            }
        });

    }
    //#endregion

    //#region   List Helpers
    public _S2Categories_Data: Array<Select2OptionData>;
    _SelectedSubCategory =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Fee: 5,
            Name: null,
            IconUrl: null,

            ParentCategoryId: 0,
            ParentCategoryKey: 0,
            ParentCategoryName: 0
        };
    GetDealCategories_List() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.getallcategories,
            Offset: 0,
            Limit: 1000,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System2, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    var _Data = _Response.Result.Data as any[];
                    if (_Data.length > 0) {
                        var parentCategories = [];
                        _Data.forEach(element => {
                            var itemIndex = parentCategories.findIndex(x => x.ParentCategoryId == element.ParentCategoryId);
                            if (itemIndex == -1) {
                                var categories = _Data.filter(x => x.ParentCategoryId == element.ParentCategoryId);
                                parentCategories.push(
                                    {
                                        ParentCategoryId: element.ParentCategoryId,
                                        ParentCategoryKey: element.ParentCategoryKey,
                                        ParentCategoryName: element.ParentCategoryName,
                                        Categories: categories,
                                    }
                                );
                            }
                        });
                        var finalCat = [];
                        parentCategories.forEach(element => {
                            var Item = {
                                id: element.ParentCategoryId,
                                text: element.ParentCategoryName,
                                children: [],
                                additional: element,
                            };
                            element.Categories.forEach(CategoryItem => {
                                var cItem = {
                                    id: CategoryItem.ReferenceId,
                                    text: CategoryItem.Name + " (" + CategoryItem.Fees + "%)",
                                    additional: CategoryItem,
                                };
                                Item.children.push(cItem);
                            });
                            finalCat.push(Item);
                        });
                        this._S2Categories_Data = finalCat;
                        this.GetDeal();
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    GetDealCategories_Selected(event: any) {
        if (event.value != "0") {
            this._SelectedSubCategory.ReferenceId = event.data[0].additional.ReferenceId;
            this._SelectedSubCategory.ReferenceKey = event.data[0].additional.ReferenceKey;
            this._SelectedSubCategory.Name = event.data[0].additional.Name;
            this._SelectedSubCategory.IconUrl = event.data[0].additional.IconUrl;
            this._SelectedSubCategory.Fee = event.data[0].additional.Fees;
            this._AmountDistribution.TUCPercentage = event.data[0].additional.Fees;
            this._AmountDistribution.OrignalTUCPercentage = event.data[0].additional.Fees;
            this._SelectedSubCategory.ParentCategoryId = event.data[0].additional.ParentCategoryId;
            this._SelectedSubCategory.ParentCategoryKey = event.data[0].additional.ParentCategoryKey;
            this._SelectedSubCategory.ParentCategoryName = event.data[0].additional.ParentCategoryName;
            this.Form_AddUser.patchValue(
                {
                    SubCategoryKey: this._SelectedSubCategory.ParentCategoryKey
                }
            );
            this.ProcessAmounts();
        }
    }

    public GetMerchants_Option: Select2Options;
    public GetMerchants_Transport: any;
    public SelectedMerchant: any = {};
    public MerchantPlaceHolder = "Select Merchant";
    GetMerchants_List() {
        var PlaceHolder = this.MerchantPlaceHolder;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ]
        }
        this.GetMerchants_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetMerchants_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetMerchants_Transport,
            multiple: false,
        };

    }

    GetMerchants_ListChange(event: any) {
        this.SelectedMerchant = event.data[0];
        this.Form_AddUser.patchValue(
            {
                AccountKey: event.data[0].ReferenceKey,
                AccountId: event.data[0].ReferenceId
            }
        );
        this.GetStores_List();
        this.getCategorieslist();
    }

    GetStores_List() {
        if (this.SelectedMerchant != undefined && this.SelectedMerchant != null && this.SelectedMerchant.ReferenceId > 0) {
            var pData = {
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
                Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
                Offset: 0,
                Limit: 1000,
                SearchCondition: this._HelperService.GetSearchCondition("", "MerchantReferenceId", "number", this.SelectedMerchant.ReferenceId),
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                }
            );
        }

    }
    //#endregion

    public GetProductCategory_option: Select2Options;
    public GetProductCategory_Transport: any;
    public selectedProductCategory: any = {};
    public productData: any = [{ id: 0, text: 'Select product' }];

    async getCategorieslist(isFromSave?) {
        // if (this.SelectedMerchant != undefined && this.SelectedMerchant != null && this.SelectedMerchant.ReferenceId > 0) {

        const pData = {
            "Task": "getsystempackagings",
            "RefreshCount": true,
            "TotalRecords": 0,
            "Offset": 0,
            "Limit": 1000,
            "SortExpression": "ReferenceId desc",
            SearchCondition: ''
        };
        if (this.SelectedMerchant != undefined && this.SelectedMerchant != null && this.SelectedMerchant.ReferenceId > 0) {
            pData.SearchCondition = this._HelperService.GetSearchCondition("", "AccountId", "number", this.SelectedMerchant.ReferenceId);
            pData.SearchCondition = `${pData.SearchCondition} || AccountId == null`
        } else {
            pData.SearchCondition = `AccountId == null`
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.systemPackaging, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.productData = [{ id: 0, text: 'Select Package' }];
                    if (_Response.Result.Data && _Response.Result.Data.length > 0) {
                        _Response.Result.Data.forEach(element => {
                            this.productData.push({
                                id: element.PackagingId,
                                text: element.Name || element.CategoryName,
                                additional: element
                            })
                        });
                        if (isFromSave) {
                            this.Package = this.productData[1].id;
                        }
                        if (this._DealDetails && this._DealDetails.Packaging && this._DealDetails.Packaging.PackagingId) {
                            this.Package = this._DealDetails.Packaging.PackagingId;
                        }
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
        // }

    }

    subCategoryData = [];
    GetProductcategory_ListChange(event) {
        if (event.data && event.data.length > 0) {
            this.packagingData.selectedProduct = event.data[0].additional;
            if (this.packagingData.selectedProduct) {
                this.Package = event.value;
                this.Form_AddDeal.patchValue({
                    Title: `${this.packagingData.selectedProduct.Type}_${this.packagingData.selectedProduct.Height}H_${this.packagingData.selectedProduct.Width}W_${this.packagingData.selectedProduct.Length}L`,
                    Height: this.packagingData.selectedProduct.Height || 0,
                    Width: this.packagingData.selectedProduct.Width || 0,
                    Length: this.packagingData.selectedProduct.Length || 0,
                    Weight: this.packagingData.selectedProduct.Weight,
                    Type: this.packagingData.selectedProduct.Type
                });

                let description = '';
                if (this._SelectedSubCategory.Name) {
                    description = `${description} ${this._SelectedSubCategory.Name}`
                } if (this.Form_AddDeal.value.Title) {
                    description = (this._SelectedSubCategory.Name) ? `${description} , ${this.Form_AddDeal.value.Title}` : `${this.Form_AddDeal.value.Title}`
                }
                if (this._Titles.OriginalTitle) {
                    description = (this.Form_AddDeal.value.Title) ? `${description} , ${this._Titles.OriginalTitle}` : `${this._Titles.OriginalTitle}`
                }

                this.Form_AddDeal.patchValue({
                    Description: description
                })
            }
        }
    }
    selectedIndex;
    selectedSubCategory;
    isNew = true;

    openDeliveryDialog(type) {
        this.Form_AddPackage.reset();
        this._HelperService.OpenModal('delivery');
        if (type === 'duplicate') {
            this.isNew = false;
            this.Form_AddPackage.controls.Title.disable();
            this.Form_AddPackage.controls.Height.disable();
            this.Form_AddPackage.controls.Width.disable();
            this.Form_AddPackage.controls.Length.disable();
            this.Form_AddPackage.controls.Type.disable();

            this.Form_AddPackage.patchValue({
                Title: this.Form_AddDeal.value.Title,
                Height: this.Form_AddDeal.value.Height,
                Width: this.Form_AddDeal.value.Width,
                Length: this.Form_AddDeal.value.Length,
                Weight: this.Form_AddDeal.value.Weight,
                Type: this.Form_AddDeal.value.Type,
            });

        } else {
            this.isNew = true;

            this.Form_AddPackage.controls.Title.disable();
            this.Form_AddPackage.controls.Height.enable();
            this.Form_AddPackage.controls.Width.enable();
            this.Form_AddPackage.controls.Length.enable();
            this.Form_AddPackage.controls.Type.enable();
        }
    }

    savePackage() {

        const pData = {
            Task: "savepackaging",
            AccountId: this.SelectedMerchant.ReferenceId,
            AccountKey: this.SelectedMerchant.ReferenceKey,
            Height: this.Form_AddPackage.controls.Height.value,
            Length: this.Form_AddPackage.controls.Length.value,
            Width: this.Form_AddPackage.controls.Width.value,
            Weight: this.Form_AddPackage.controls.Weight.value,
            Type: this.Form_AddPackage.controls.Type.value,
            Name: this.Form_AddPackage.controls.Title.value,
            // ParcelDescription: this.Form_AddPackage.controls.Description.value,
            Size_unit: 'cm',
            Weight_unit: 'kg'
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.systemPackaging, pData);
        _OResponse.subscribe(
            async _Response => {
                await this.getCategorieslist();
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.CloseModal('delivery');
                    this.Form_AddDeal.patchValue({
                        Title: _Response.Result.Name,
                        Height: _Response.Result.Height,
                        Width: _Response.Result.Width,
                        Length: _Response.Result.Length,
                        Weight: _Response.Result.Weight,
                        Type: _Response.Result.Type,
                        Description: _Response.Result.Description
                    })
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    onchangeofValue() {
        let title = '';
        if (this.Form_AddPackage.value.Type) {
            title = `${this.Form_AddPackage.value.Type}`;
        }
        if (this.Form_AddPackage.value.Height) {
            title = `${title}_${this.Form_AddPackage.value.Height}H`;
        }
        if (this.Form_AddPackage.value.Width) {
            title = `${title}_${this.Form_AddPackage.value.Width}W`;
        }
        if (this.Form_AddPackage.value.Length) {
            title = `${title}_${this.Form_AddPackage.value.Length}L`;
        }
        if (this.Form_AddPackage.value.Weight) {
            title = `${title}_${this.Form_AddPackage.value.Weight}WE`;
        }
        this.Form_AddPackage.patchValue({
            Title: `${title}`,
        });
    }

    close() {
        this._HelperService.CloseModal("delivery");
    }


    imageChangedEvent: any = '';
    croppedImage: any = '';
    croppedImage1: any = '';
    ImageIconUrl: any;
    jpeg: "jpeg";
    IconContent: OStorageContent = {
        Name: null,
        Content: null,
        Extension: null,
        TypeCode: null,
        Height: 400,
        Width: 800
    };
    fileChangeEvent(event: any) {
        this.imageChangedEvent = event;
        this._HelperService.OpenModal("imageCropperModule");
    }
    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage = event.base64;
        this.croppedImage1 = this.croppedImage.replace("data:image/png;base64,", "")
        this.IconContent.Content = this.croppedImage1;
        this.IconContent.Extension = event.file.type;
    }
    croppedImaged() {
        this.ImageIconUrl = this.croppedImage;
        this._HelperService.CloseModal("imageCropperModule");
    }
    croperReady() {
    }
    loadImageFailed() {
    }

}

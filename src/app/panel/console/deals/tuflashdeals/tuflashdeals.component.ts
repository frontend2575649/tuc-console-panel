import { ChangeDetectorRef, Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;
declare var moment: any;
import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: "tu-tuflashdeals",
  templateUrl: "./tuflashdeals.component.html",
})
export class TUFlashDealsComponent implements OnInit, OnDestroy {
  public ResetFilterControls: boolean = true;
  UpdateStatusArray: any = ['deal.draft', 'deal.approvalpending', 'deal.approved', 'deal.published', 'deal.paused', 'deal.expired',];
  public _ObjectSubscription: Subscription = null;
  public isUpdate:boolean=false;
  public isMarkUnMark :boolean=false;
  public isDelete:boolean=false;
  public isView:boolean=false;
  public isUpdateStatus:boolean=false;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = true;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;

  }


  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();
    this._HelperService.ValidateData();
    this.ListType = 1;
    this.isUpdate=this._HelperService.SystemName.includes("dealupdate");
    this.isUpdateStatus=this._HelperService.SystemName.includes("dealupdatestatus");
    this.isView=this._HelperService.SystemName.includes("dealsview");
    this.isMarkUnMark=this._HelperService.SystemName.includes("markunmarkflashdeal");
    this.isDelete=this._HelperService.SystemName.includes("dealdelete");
    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];

      this.DealsList_Setup();
      this.DealsList_Filter_Owners_Load();
      this.InitColConfig();

    });

    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.DealsList_GetData();
    // });
    // this._HelperService.StopClickPropogation();
  }

  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {
    }
  }
  MerchantRoute(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveMerchant,
      {
        ReferenceKey: ReferenceData.AccountKey,
        ReferenceId: ReferenceData.AccountId,
        DisplayName: ReferenceData.AccountDisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveMerchantReferenceKey = ReferenceData.AccountKey;
    this._HelperService.AppConfig.ActiveMerchantReferenceId = ReferenceData.AccountId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.RewardPercentage,
      ReferenceData.AccountKey,
      ReferenceData.AccountId,
    ]);

  }
  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  AddNewDeal() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.AddDeals,

    ]);
  }

  //#region merchantlist
  public ListType: number;

  public DealsList_Config: OList;
  DealsList_Setup() {
    this.DealsList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.FlashDeals,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals,
      Title: "Available Stores",
      StatusType: "default",
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [

        {
          DisplayName: " Merchant Name",
          SystemName: "AccountDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: " Title",
          SystemName: "Title",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Stores",
          SystemName: "Locations",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Description",
          SystemName: "Description",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Payable to merchant",
          SystemName: "MerchantAmount",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Maximum Unit Sale",
          SystemName: "MaximumUnitSale",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Deals Sold",
          SystemName: "TotalPurchase",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
          //   .PanelAcquirer.Merchant.Dashboard,
        }, {
          DisplayName: "Category Name",
          SystemName: "CategoryName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: 'Started On',
          SystemName: "StartDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: 'End On',
          SystemName: "EndDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]


    };
    this.DealsList_Config.ListType = this.ListType;
    this.DealsList_Config.SearchBaseCondition = "";

    if (this.DealsList_Config.ListType == 1) //  approvalpending
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.active', "=");
    }
    // else if (this.DealsList_Config.ListType == 2) //  approved
    // {
    //   this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.approved', "=");
    // }
    // else if (this.DealsList_Config.ListType == 3) // published
    // {
    //   this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.published', "=");
    // }
    // else if (this.DealsList_Config.ListType == 4) // paused
    // {
    //   this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.paused', "=");
    // }
    // else if (this.DealsList_Config.ListType == 5) // draft
    // {
    //   this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.draft', "=");
    // }
    // else if (this.DealsList_Config.ListType == 6) // expired
    // {
    //   this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.expired', "=");
    // }

    else {
      this.DealsList_Config.DefaultSortExpression = 'CreateDate desc';
    }





    this.DealsList_Config = this._DataHelperService.List_Initialize(
      this.DealsList_Config
    );

    // this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "DealerKey", 'text', this._HelperService.AppConfig.ActiveOwnerKey, "=");
    //this.DealsList_GetData();

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Stores,
      this.DealsList_Config
    );

    this.DealsList_GetData();
  }
  DealsList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.DealsList_Config.Sort.SortOptions.length; index++) {
        const element = this.DealsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }
    if (Type == "date") {
      this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
      this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
  }
    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.DealsList_Config
    );

    this.DealsList_Config = this._DataHelperService.List_Operations(
      this.DealsList_Config,
      event,
      Type
    );

    if (
      (this.DealsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.DealsList_GetData();
    }

  }
  timeout = null;
  DealsList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.DealsList_Config.Sort.SortOptions.length; index++) {
          const element = this.DealsList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.DealsList_Config
      );

      this.DealsList_Config = this._DataHelperService.List_Operations(
        this.DealsList_Config,
        event,
        Type
      );

      if (
        (this.DealsList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.DealsList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);
  }
  DealsList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.DealsList_Config
    );
    this.DealsList_Config = TConfig;
  }
  DealsList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveDeal,
      {
        ReferenceKey: ReferenceData.DealKey,
        ReferenceId: ReferenceData.DealId,
        AccountKey: ReferenceData.AccountKey,
        AccountId: ReferenceData.AccountId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.DealKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.DealId;
    this._HelperService.AppConfig.ActiveAccountKey =
      ReferenceData.DealKey;
    this._HelperService.AppConfig.ActiveAccountId = ReferenceData.DealId;
    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Store
    //     .Dashboard,
    //   ReferenceData.ReferenceKey,
    //   ReferenceData.ReferenceId,
    // ]);

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Analytics,
      ReferenceData.DealKey,
      ReferenceData.DealId,
      ReferenceData.AccountId,
      ReferenceData.AccountKey,
    ]);
  }

  //#endregion
  DealerList_ListTypeChange(Type) {
    this.ListType = Type;
    this.DealsList_Setup();
  }
  //#region OwnerFilter

  public DealsList_Filter_Owners_Option: Select2Options;
  public DealsList_Filter_Owners_Selected = null;
  DealsList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant,
    //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     this._HelperService.AppConfig.AccountType.PosAccount
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.DealsList_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  DealsList_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.DealsList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.DealsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.DealsList_Filter_Owners_Selected,
        "="
      );
      this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.DealsList_Config.SearchBaseConditions
      );
      this.DealsList_Filter_Owners_Selected = null;
    } else if (event.value != this.DealsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.DealsList_Filter_Owners_Selected,
        "="
      );
      this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.DealsList_Config.SearchBaseConditions
      );
      this.DealsList_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.DealsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.DealsList_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.DealsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.DealsList_Config.SearchBaseConditions = [];
    // this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.published', "=");;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.DealsList_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.DealsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.DealsList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.DealsList_Config);
    if (Type == 'Time') {
      this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
      this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
  }
    this.SetOtherFilters();

    this.DealsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      // inputAttributes: {
      //   autocapitalize: "off",
      //   autocorrect: "off",
      //   maxLength: "4",
      //   minLength: "4",
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Stores,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Stores
        );
        this._FilterHelperService.SetMerchantConfig(this.DealsList_Config);
        this.DealsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.DealsList_GetData();

    if (ButtonType == 'Sort') {
      $("#DealsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#DealsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.DealsList_Config);
    this.SetOtherFilters();

    this.DealsList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.DealsList_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  CloseRowModal(index: number): void {
    $("#SubAccountsList_rdropdown_" + index).dropdown('toggle');
  }


  Update_RowSelected(ReferenceData: any): void {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.EditDeal,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
      ReferenceData.AccountId,
      ReferenceData.AccountKey,
    ]);
  }


  DeleteDeal(ReferenceData): void {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PData =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.deletedeal,
          ReferenceId: ReferenceData.ReferenceId,
          ReferenceKey: ReferenceData.ReferenceKey,
          AccountId: ReferenceData.AccountId,
          AccountKey: ReferenceData.AccountKey
          // StatusCode: this.selectedStatusItem.statusCode,

        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, PData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Status Updated successfully. It will take upto 5 minutes to update changes.");
              this.DealsList_Setup();
              this._HelperService.IsFormProcessing = false;
              this._HelperService.CloseModal('exampleModal')
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });

      }
    });




  }

  FalshDeal: any = {};
  UnMarkAsFlash(ReferenceData: any): void {
    this.FalshDeal = {};
    this.FalshDeal.ReferenceKey = ReferenceData.DealKey;
    this.FalshDeal.ReferenceId = ReferenceData.DealId;

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.RemoveFlasTitle,
      text: this._HelperService.AppConfig.CommonResource.RemoveFlashHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PData =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.RemoveFlashDeal,
          ReferenceId: this.FalshDeal.ReferenceId,
          ReferenceKey: this.FalshDeal.ReferenceKey,
          // StatusCode: this.selectedStatusItem.statusCode,

        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, PData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess(_Response.Message);
              this.DealsList_Setup();
              this._HelperService.IsFormProcessing = false;

            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });

      }
    });

  }

  StatusUpdate(ReferenceData, i) {
    swal({
      title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      input: 'password',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },

    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var P1Data =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.updatedealstatus,
          ReferenceId: ReferenceData.ReferenceId,
          ReferenceKey: ReferenceData.ReferenceKey,
          AccountId: ReferenceData.AccountId,
          AccountKey: ReferenceData.AccountKey,
          StatusCode: this.UpdateStatusArray[i],

        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, P1Data);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Status Updated successfully. It will take upto 5 minutes to update changes.");
              this.DealsList_Setup();
              this._HelperService.IsFormProcessing = false;
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });

      }
    });



  }

}

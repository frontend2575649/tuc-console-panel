import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ChartDataSets } from 'chart.js';
import * as Feather from "feather-icons";
import * as cloneDeep from 'lodash/cloneDeep';
import { BaseChartDirective, Color, Label } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OList, OResponse, OSalesTrend, ODealData, OSalesTrendData, OSalesTrendDataHourly, OSelect } from '../../../../service/service';
declare var moment: any;
declare var $: any;
declare var d3: any;
import Highcharts from "highcharts";
import Exporting from "highcharts/modules/exporting";
import funnel from "highcharts/modules/funnel";
Exporting(Highcharts);
funnel(Highcharts);
@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUDashboardComponent implements OnInit {
  @ViewChild("container", { read: ElementRef }) container: ElementRef;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {

  }
  StartDateTime = null;
  EndDateTime = null;
  Last7StartDateTime = null;
  Last7EndDateTime = null;
  public TodayDate: any;
  public isDateandRefresh:boolean=false;
  ngOnInit() {
    // if(this._HelperService.LoginCheckVar){
    //   this._HelperService.OpenModal('exampleModal');
    //   this._HelperService.LoginCheckVar = false;

    // }
    this.TodayDate = this._HelperService.GetDateTime(new Date());
    Feather.replace();
    if(this._HelperService.SystemName.includes("dashboardmerchant") 
    || this._HelperService.SystemName.includes("dashboardcustomer")
    || this._HelperService.SystemName.includes("dashboardrewarddistributed")
    || this._HelperService.SystemName.includes("dashboardtransaction")
    || this._HelperService.SystemName.includes("dashboardsalesreport")
    || this._HelperService.SystemName.includes("dashboardbillersreport")
    ){
      this.isDateandRefresh=true;
    }
    this._HelperService.ValidateData();
    this.StartDateTime = moment().startOf('day');
    this.EndDateTime = moment().endOf('day');
    this.Last7StartDateTime = moment().subtract(6, "days"), moment()
    this.Last7EndDateTime = moment()
    this.LoadData();
    Highcharts.chart(this.container.nativeElement, {
      credits: {
        enabled: false
      },
      chart: {
        type: "funnel",
      },
      exporting: { enabled: false },
      title: {
        text: null,
        enabled: false
      },
      plotOptions: {
        xAxis: {
          labels: {
            rotation: 180,

          }
        },
        series: {
          dataLabels: {
            format: "<b>{point.name}</b> ({point.y:,.0f})",
            color:
              (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
              "black",
            softConnector: false,
            align: "center",
            allowOverlap: true,
            enabled: true,
            showInLegend: true,
            x: -250,
            y: -10
          },
          neckWidth: ["20%"],
          neckHeight: ["00%"],
          width: "30%",
          height: "100%",

        }
      },
      legend: {
        enabled: true
      },
      series: [
        {
          name: "Unique users",
          data: [
            ["Website visits", 40],
            ["Downloads", 20],
            ["Requested price", 21],
            ["Invoice sent", 24],
          ]
        }
      ]
    });
  }

  RefershOverview() {
    this.LoadData();
  }
  LoadData() {
    this.GetLoyalityOverview(this.StartDateTime, this.EndDateTime)
    this.GettodayOverviewSale(this.StartDateTime, this.EndDateTime)
    this.GetLoyalityOverviewSale(this.StartDateTime, this.EndDateTime);
    this.GetCustomerOverview(this.StartDateTime, this.EndDateTime);
    this.GetBillerOverview(this.StartDateTime, this.EndDateTime);
    this.TUTr_Setup(this.Last7StartDateTime, this.Last7EndDateTime);
    this.GetOverviews(this.StartDateTime, this.EndDateTime);
  }

  public LoyalityOverview: any =
    {

    }
  private LoyalityOvereview = {
    Task: 'getloyaltyoverview',
    StartDate: null,
    EndDate: null,
    // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };
  RouteBillers() {
    this._Router.navigate([
      "console/" + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AccountSettings.Payments,
    ]);
  }

  RouteTransaction() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.SalesHistory,
    ]);

  }

  MerchantRoute(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveMerchant,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveMerchantReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveMerchantReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.RewardPercentage,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);

  }

  RouteCustomer(ReferenceData) {
    //#region Save Current Merchant To Storage 
    this._HelperService.customerDisplayName=ReferenceData.UserDisplayName;
    this._HelperService.customerReferenceKey=ReferenceData.UserAccountKey,
    this._HelperService.customerReferenceId=ReferenceData.UserAccountId,
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveCustomer,
      {
        ReferenceKey: ReferenceData.UserAccountKey,
        ReferenceId: ReferenceData.UserAccountId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Customer,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.UserAccountKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.UserAccountId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.CustomerPanel.Accounts.WalletInfo,
      ReferenceData.UserAccountKey,
      ReferenceData.UserAccountId,
    ]);

    //#endregion

  }


  GetLoyalityOverview(StartDateTime, EndDateTime) {


    this._HelperService.IsFormProcessing = true;

    this.LoyalityOvereview.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.LoyalityOvereview.EndDate = this._HelperService.DateInUTC(EndDateTime);


    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analytics, this.LoyalityOvereview);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.LoyalityOverview = _Response.Result;


        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  public todayDataSalesOverview: any =
    {


    }

  public NewMerchants: any = []
  public LowBalance: any = []
  public NewPos: any = []
  private ptodayOverviewData = {
    Task: 'gettodaysoverview',
    StartDate: null,
    EndDate: null,
    // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };

  public LowWalletData: boolean = true;

  GettodayOverviewSale(StartDateTime, EndDateTime) {

    this._HelperService.IsFormProcessing = true;

    this.ptodayOverviewData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.ptodayOverviewData.EndDate = this._HelperService.DateInUTC(EndDateTime);


    // this.ptodayOverviewData.Type = this.Types.hour;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analytics, this.ptodayOverviewData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.todayDataSalesOverview = _Response.Result;
          this.NewMerchants = this.todayDataSalesOverview.NewMerchants;
          this.LowBalance = this.todayDataSalesOverview.LowBalancewMerchants;
          this.NewPos = this.todayDataSalesOverview.NewTerminals;
          this.LowWalletData = false;


        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  public SalesOverview: any =
    {

      ActiveMerchants: 0,
      TotalMerchants: 0,
      Customers: 0,
      Transactions: 0,
      InvoiceAmount: 0,
      CardTransaction: 0,
      CardInvoiceAmount: 0,
      CashTransaction: 0,
      CashInvoiceAmount: 0,
    }
  private pData = {
    Task: 'getsalesoverview',
    StartDate: null,
    EndDate: null,
    // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };
  GetLoyalityOverviewSale(StartDateTime, EndDateTime) {
    this._HelperService.IsFormProcessing = true;
    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);
    // this.pData.Type = this.Types.hour;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analytics, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.SalesOverview = _Response.Result;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public _GetoverviewSummary: any = {};
  public Counts: any = {}

  private pCustomerData = {
    Task: 'getcustomersoverview',
    StartDate: null,
    EndDate: null,
    Type: "Lite",
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetCustomerOverview(StartDateTime, EndDateTime) {

    this._HelperService.IsFormProcessing = true;

    this.pCustomerData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pCustomerData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analytics, this.pCustomerData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {

          //#region reset totals 

          //#endregion

          this._GetoverviewSummary = _Response.Result as any;
          this.Counts = this._GetoverviewSummary.Counts;




          //Percentage

          //ENd PErcentage

          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public _BillerSummary: any = {};

  private pBillerData = {
    Task: 'getvaspurchaseoverview',
    StartDate: null,
    EndDate: null,

    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetBillerOverview(StartDateTime, EndDateTime) {

    this._HelperService.IsFormProcessing = true;

    this.pBillerData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pBillerData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Vas, this.pBillerData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {

          //#region reset totals 

          //#endregion

          this._BillerSummary = _Response.Result as any;




          //Percentage

          //ENd PErcentage

          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }







  public TUTr_Config: OList;
  TUTr_Setup(Last7StartDateTime, Last7EndDateTime) {
    this.TUTr_Config =
    {
      Id: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.getsaletransactions,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore,
      Title: 'Sales History',
      StatusType: 'transaction',
      PageRecordLimit: 5,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
      Type: this._HelperService.AppConfig.ListType.All,
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'TransactionDate',
        SortName: null,
        SortColumn: 'InvoiceAmount',
        SortOrder: 'desc',
        SortOptions: [],
      },
      TableFields: [
        {
          DisplayName: '#',
          SystemName: 'ReferenceId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Date',
          SystemName: 'TransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Show: true,
          Search: true,
          Sort: true,
          IsDateSearchField: true,
        },
        {
          DisplayName: 'Transaction Status',
          SystemName: 'StatusName',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'User',
          SystemName: 'UserDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: true,
          Sort: true,
        },
        {
          DisplayName: 'User Mobile Number',
          SystemName: 'UserMobileNumber',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: true,
          Sort: false,
        },
        {
          DisplayName: 'Type',
          SystemName: 'TypeName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Sale Amount',
          SystemName: 'InvoiceAmount',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Show: true,
          Search: false,
          Sort: true,
        },
        {
          DisplayName: 'Reward Amount',
          SystemName: 'RewardAmount',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Class: 'text-grey',
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'User Reward Amount',
          SystemName: 'UserAmount',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Commission Amount',
          SystemName: 'CommissionAmount',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Show: false,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Card Number',
          SystemName: 'AccountNumber',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Card Type',
          SystemName: 'CardBrandName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Card Bank Provider',
          SystemName: 'CardBankName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'TUC Card Number',
          SystemName: 'TUCCardNumber',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Merchant',
          SystemName: 'ParentDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Store',
          SystemName: 'SubParentDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Acquirer / Bank',
          SystemName: 'AcquirerDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Terminal Provider',
          SystemName: 'ProviderDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Transaction Issuer (Done by)',
          SystemName: 'CreatedByDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Transaction Reference',
          SystemName: 'ReferenceNumber',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
        },



      ]
    }

    this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
    var daterangetoday = {
      start: this._HelperService.DateInUTC(Last7StartDateTime),
      end: this._HelperService.DateInUTC(Last7EndDateTime)
    };
    this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, daterangetoday, this._HelperService.AppConfig.ListToggleOption.Date);
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.MerchantSales,
      this.TUTr_Config
    );
    this.TUTr_GetData();
  }

  TUTr_GetData() {

    // this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getsaletransactionsoverview);
    var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
    this.TUTr_Config = TConfig;

  }

  public OverviewData: any = {
    Transactions: 0,
    InvoiceAmount: 0.0,
    FailedTransactions: 0.0

  };


  ColumnName: string = 'TransactionDate';

  GetOverviews(StartDateTime, EndDateTime): any {
    StartDateTime = moment(StartDateTime).subtract(1, "seconds");
    EndDateTime = moment(EndDateTime).add(1, "seconds");
    var FSd = new Date(StartDateTime);
    var TStartDateM = moment(FSd).utc().format("YYYY-MM-DD HH:mm:ss");
    var ESd = new Date(EndDateTime);
    var TEndTimeM = moment(ESd).utc().format("YYYY-MM-DD HH:mm:ss");
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getsaletransactionsoverview,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      SearchCondition: "( " +
        this.ColumnName +
        ' > "' +
        TStartDateM +
        '" AND ' +
        this.ColumnName +
        ' < "' +
        TEndTimeM +
        '")',
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.OverviewData = _Response.Result as any;

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });


  }

}

export class OAccountOverview {
  public TotalMerchants: any;
  public InvoiceAmount: number;
  public TotalTerminals: any;
  public CardTypeSale: any;
  public ActiveStores: any;
  public TotalStores: any;
  public Ptsp: any;
  public TotalSale: any;
  public TotalTransactions: any;
  public CashTransactionAmount: any;
  public CashTransactionsPerc: any;
  public CashTransactions: any;
  public CardTransactionsAmount: any;
  public CardTransactionsPerc: any;
  public CardTransactions: any;
  public Others: any;
  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public TerminalStatus?: number;
  public Total?: number;
  public Idle?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
  public Customers: number;
}
export class OAccountCustomerOverview {
  public TotalMerchants: any;
  public InvoiceAmount: number;
  public TotalTerminals: any;
  public CardTypeSale: any;
  public ActiveStores: any;
  public TotalStores: any;
  public Ptsp: any;
  public TotalSale: any;
  public TotalTransactions: any;
  public CashTransactionAmount: any;
  public CashTransactionsPerc: any;
  public CashTransactions: any;
  public CardTransactionsAmount: any;
  public CardTransactionsPerc: any;
  public CardTransactions: any;
  public Others: any;
  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public TerminalStatus?: number;
  public Total?: number;
  public Idle?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
  public AvgSpentVisit: any;
  public AvgVisitCustomer: any;
  public Transaction: any;
  public TransactionInvoiceAmount: any
  public AverageVisit: any
  public AverageInvoiceAmount: any

  public New?: any;
  public Repeat?: any;

}

export class OLoyalityAccountOverview {
  public RewardAmount: any;
  public RedeemAmount: any;
  public NewCustomers: any;
  public RepeatingCustomers: any;
  public TransactionInvoiceAmount: any;
  public Transaction: any;
  public RedeemTransaction: any;
  public RedeemInvoiceAmount: any;
  public AvgSpentVisit: any;
  public AvgVisitCustomer: any;
  public VisitsByRepeatingCustomers: any;
  public TotalCustomer: any;

}
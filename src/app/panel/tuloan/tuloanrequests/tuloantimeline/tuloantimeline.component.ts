import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OResponse } from "../../../../service/service";
import { BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-tuloantimeline',
  templateUrl: './tuloantimeline.component.html',
  styleUrls: ['./tuloantimeline.component.css'],
})
export class TuloantimelineComponent implements OnInit {

  timelineData: any = [];
  data: any = {};

  constructor(
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public bsModalRef: BsModalRef,
    public options: ModalOptions) {
    this.data = this.options.initialState;
  }

  ngOnInit() {
    this.getTimelineDetails();
  }

  getTimelineDetails() {
    const pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetLoanActivity,
      // ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      // ReferenceId: this._HelperService.AppConfig.ActiveReferenceId
      ReferenceKey: this.data.ReferenceKey,
      ReferenceId: this.data.ReferenceId
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.loans, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.timelineData = _Response.Result as any;
          if (this.timelineData && this.timelineData && this.timelineData.Data && this.timelineData.Data.length > 0) {
            this.timelineData.Data.forEach((element, i) => {
              if (i === 0 && element.FirstPartCreditAmount && element.FirstPartCreditAmount > 0) {
                element.otherDetails = `₦ ${element.FirstPartCreditAmount} credited to TUC as first part`;
              }
              if (i === 1) {
                element.otherDetails = `₦ ${element.LoanAmountCredited} credited in TUC account`;
              }
              if (i === 4) {
                element.Description = `TUC settled ₦ ${element.LoanAmountCredited} with merchant`;
              }
              if (i > 4) {
                element.Description = `${element.Description}- ₦ ${element.Amount} paid`;
              }
              element.date = element.StatusUpdatedTime || element.FirstPartCreditDate;
              element.date = this._HelperService.GetDateTimeS(element.date);

            });
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

}

import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";


import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Ng5SliderModule } from 'ng5-slider';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import { MainPipe } from '../../../service/main-pipe.module';
import { TUloanrequestsComponent } from "./tuloanrequests.component";
import { TuloantimelineComponent } from './tuloantimeline/tuloantimeline.component';

const routes: Routes = [{
  path: "",
  children: [{ path: "", redirectTo: 'loanrequests' },
  { path: "loanrequests", component: TUloanrequestsComponent },
  { path: "loantimeline", component: TuloantimelineComponent }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})

export class TULoadRequestsRoutingModule { }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    Ng5SliderModule,
    ModalModule.forRoot(),
    TULoadRequestsRoutingModule,
    MainPipe,
    ],
  providers: [BsModalService],
  declarations: [TUloanrequestsComponent, TuloantimelineComponent],
  entryComponents: [TuloantimelineComponent]
})

export class TuloanrequestsModule { }

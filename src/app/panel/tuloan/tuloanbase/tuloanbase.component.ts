import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, OResponse } from "../../../service/service";

@Component({
  selector: "tu-tuloanbase",
  templateUrl: "tuloanbase.component.html"
})
export class DealLoadBaseComponent implements OnInit {
  _ShowMap = false;
  listData: any;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) { }

  ngOnInit() {


  }

  downloadReport(): any {
    const pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.getrepayments,
      TotalRecords: 0,
      Offset: 0,
      Limit: 25,
      RefreshCount: true,
      SearchCondition: '',
      SortExpression: "LoanId desc",
      Type: null,
      ReferenceKey: null,
      ReferenceId: 0,
      SubReferenceId: 0,
      IsDownload: true,
      Status:748
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.repayments,pData);
    _OResponse.subscribe(
      (_Response) => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.listData = _Response.Result as any;
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      (_Error) => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
}
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { ChartsModule } from 'ng2-charts';

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';

import { TULoanDetailsComponent } from "./tuloandetails.component";
import { ImageCropperModule } from "ngx-image-cropper";
import { TuloantimelineComponent } from "../tuloanrequests/tuloantimeline/tuloantimeline.component";

import { TuloanrequestsModule } from '../tuloanrequests/tuloanrequests.module';

const routes: Routes = [{ path: "", component: TULoanDetailsComponent }];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TULoanDetailsRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        TULoanDetailsRoutingModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        ImageCropperModule,
        ModalModule.forRoot(),
        TuloanrequestsModule,
        ChartsModule
    ],
    declarations: [TULoanDetailsComponent],
    providers: [BsModalService],
    entryComponents: [TuloantimelineComponent]
})
export class TULoanDetailsModule { }

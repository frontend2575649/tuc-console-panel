import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import * as Feather from "feather-icons";
import { BsModalService } from 'ngx-bootstrap/modal';
// import { ChartType } from 'chart.js';
import { MultiDataSet, Label, Color } from 'ng2-charts';
import { ActivatedRoute, Router, Params } from "@angular/router";
import {
    OSelect,
    OList,
    DataHelperService,
    HelperService,
    OResponse,
    OStorageContent,
    OCoreParameter,
    OCoreCommon,
    OInvoiceDetails
} from "../../../service/service";
import swal from "sweetalert2";
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { TuloantimelineComponent } from '../tuloanrequests/tuloantimeline/tuloantimeline.component';

@Component({
    selector: "tu-tuloandetails",
    templateUrl: "./tuloandetails.component.html"
})
export class TULoanDetailsComponent implements OnInit {


    totalPrincipleAmount = 0;
    totalIntrest = 0;
    overallTotal = 0;
    tucTotal = 0;
    providerTotal = 0;
    public doughnutChartOptions: any = {


        legend: {
            display: false
        },
        plugins: {
            datalabels: {
                formatter: () => {
                    return null;
                },
            },
        },
        tooltips: { enabled: false }
    }
    // Doughnut
    public doughnutChartLabels: Label[] = [];
    public doughnutChartData = [];
    public doughnutChartType = 'doughnut';
    public datacalc = [];

    public doughnutChartColors: any[] = [{
        backgroundColor: []
    }];

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        private modalService: BsModalService,
        private cdr: ChangeDetectorRef

    ) { }
    public refrenceId = null;
    public refrenceKey = null;
    loanDetails: any = {};

    getLoanDetail() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetLoanDetails,
            ReferenceKey: this.refrenceKey,
            ReferenceId: this.refrenceId
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.loans, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.loanDetails = _Response.Result as any;
                    let data = [];
                    if (this.loanDetails) {
                        this.datacalc = this.loanDetails.RepaymentTimeLine;

                        if (this.loanDetails && this.loanDetails.LoanDisbursmentTimeLine) {
                            this.loanDetails.LoanDisbursmentTimeLine.ProcessingFees = this.loanDetails.LoanDisbursmentTimeLine.ProcessingFees ? this.loanDetails.LoanDisbursmentTimeLine.ProcessingFees : 0
                            this.loanDetails.LoanDisbursmentTimeLine.info = `When customer use Loan card at merchant, then TUC will settle loan amount (By charging ${this.loanDetails.LoanDisbursmentTimeLine.ProcessingFees}% processing fees)`;
                        }

                        if (this.loanDetails.RepaymentTimeLine && this.loanDetails.RepaymentTimeLine.length > 0) {
                            this.loanDetails.RepaymentTimeLine.forEach(element => {
                                element.ProcessingFees = element.ProcessingFees ? element.ProcessingFees : 0
                                element.info = `${element.ProcessingFees}% Processing Fees taken from merchant`;

                                this.totalPrincipleAmount = this.totalPrincipleAmount + element.PrincipalAmount;
                                this.totalIntrest = this.totalIntrest + element.Interest;
                                this.overallTotal = this.overallTotal + element.Total;
                                this.tucTotal = this.tucTotal + element.ToTUC;
                                this.providerTotal = this.providerTotal + element.ToProvider
                            });
                        }

                        if (this.loanDetails.PlanDetail && this.loanDetails.PlanDetail.TotalInstallments) {
                            for (let i = 0; i < this.loanDetails.PlanDetail.TotalInstallments; i++) {
                                this.doughnutChartData.push(1);
                                this.doughnutChartLabels.push("installment")
                            }
                            if (this.loanDetails.PlanDetail.PaidInstallments > 0) {
                                this.doughnutChartColors[0].backgroundColor.push("#6FD49B");
                            }
                            for (let i = 0; i < this.loanDetails.PlanDetail.PaidInstallments - 1; i++) {
                                this.doughnutChartColors[0].backgroundColor.push("#0168FA9A");
                            }
                            let remainingInstallments = this.loanDetails.PlanDetail.TotalInstallments - this.loanDetails.PlanDetail.PaidInstallments;
                            for (let i = 0; i < remainingInstallments; i++) {
                                this.doughnutChartColors[0].backgroundColor.push("#C0CCDA");
                            }
                        }
                    }
                    this.cdr.detectChanges();
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });

    }

    openDialog() {
        const initialState = {
            ReferenceKey: this.refrenceKey,
            ReferenceId: this.refrenceId
        };
        this.modalService.show(TuloantimelineComponent, { initialState });
    }

    openDetails(details) {
        this.refrenceId = details.ReferenceId;
        this.refrenceKey = details.ReferenceKey;
        this.getLoanDetail();
    }
    customerDetails(details) {
        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.CustomerPanel.LoanHistory,
            details.CustomerAccountKey,
            details.CustomerAccountId,
        ]);
    }
    ngOnInit() {
        Feather.replace();

        this._ActivatedRoute.params.subscribe((params: Params) => {
            this.refrenceId = params["referenceid"];
            this.refrenceKey = params['referencekey'];
            if (this.refrenceKey == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
            } else {
                this.getLoanDetail();
                // this.InvoiceList_Filter_TransactionTypes_Load();
                // this.InvoiceList_Setup();
                // this.Get_Invoice();
            }
        });


    }
}

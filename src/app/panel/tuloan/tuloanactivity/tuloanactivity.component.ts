import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, OResponse } from "../../../service/service";

@Component({
  selector: "tu-tuloanactivity",
  templateUrl: "tuloanactivity.component.html",
  styleUrls:['./tuloadactivity.component.css']
})
export class DealLoanActivityComponent implements OnInit {
  _ShowMap = false;
  isPropActive:boolean=false;
  public listData: any = {};
  public overviewData: any = {};
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) { }

  ngOnInit() {


  }

  getDefaultLoanOverview() {

    var pData = {
        Task: this._HelperService.AppConfig.Api.ThankUCash.GetLoanOverview,
        ReferenceKey: null,
        ReferenceId: 0
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.bnpl.loans, pData);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.overviewData = _Response.Result as any;
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);

        });

}



defaultClick(event){
  
  if(event==2){
    this.isPropActive=true;
  }
  else{
    this.isPropActive=false;
  }
}
}
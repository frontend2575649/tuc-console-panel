import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Ng5SliderModule } from 'ng5-slider';

import { TULoanProcessingComponent } from "./tuloanprocessing.component";
const routes: Routes = [{ path: "", component: TULoanProcessingComponent }];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TULoanProcessingRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng5SliderModule,
        Ng2FileInputModule,
        TULoanProcessingRoutingModule,
    ],
    declarations: [TULoanProcessingComponent]
})
export class TULoanProcessingeModule { }

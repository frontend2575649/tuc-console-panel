import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { AgmCoreModule } from '@agm/core';
import { DealLoanActivityComponent } from './tuloanactivity.component';
// import { LeafletModule } from '@asymmetrik/ngx-leaflet';
const routes: Routes = [
    {
        path: "",
        component: DealLoanActivityComponent,
        children: [

            //  { path: "", data: { permission: "getloan", menuoperations: "LoanOperations",  PageName: "System.Menu.LoanRequest", accounttypecode: "admin" }, loadChildren: "./tuloanrequests/tuloanrequests.module#TURequestModule" },
            { path: "creditloan", data: { permission: "getloan", menuoperations: "LoanOperations", PageName: "All loans", accounttypecode: "admin" }, loadChildren: "../tuloanactivity/tucreditloan/tucreditloan.module#TUCreditLoandueModule" },
            { path: "defaults", data: { permission: "getloan", menuoperations: "LoanOperations", PageName: "All loans", accounttypecode: "admin" }, loadChildren: "../tuloanactivity/tucreditdefault/tucreditdefault.module#TULoanDefaultModule" },
            { path: "rejected", data: { permission: "getloan", menuoperations: "LoanOperations", PageName: "All loans", accounttypecode: "admin" }, loadChildren: "../tuloanactivity/tuloanrejected/tuloanrejected.module#TULoanRejecteddueModule" },
            { path: "failed", data: { permission: "getloan", menuoperations: "LoanOperations", PageName: "All loans", accounttypecode: "admin" }, loadChildren: "../tuloanactivity/tuloanfailed/tuloanfailed.module#TULoanFailedeModule" },
            { path: "processing", data: { permission: "getloan", menuoperations: "LoanOperations", PageName: "All loans", accounttypecode: "admin" }, loadChildren: "../tuloanactivity/tuloanprocessing/tuloanprocessing.module#TULoanProcessingeModule" },
            { path: "loanactive", data: { permission: "getloan", menuoperations: "LoanOperations", PageName: "All loans", accounttypecode: "admin" }, loadChildren: "../tuloanactivity/tuloanactive/tuloanactive.module#TULoanActiveModule"},
            { path: "loanall", data: { permission: "getloan", menuoperations: "LoanOperations", PageName: "All loans", accounttypecode: "admin" }, loadChildren: "../tuloanactivity/tuloanall/tuloanall.module#TuloanallModule"}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DealLoanActivityRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        DealLoanActivityRoutingModule,
        // LeafletModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    declarations: [DealLoanActivityComponent]
})
export class DealLoanActivityModule { }

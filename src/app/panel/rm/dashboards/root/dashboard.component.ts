import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DaterangePickerComponent } from 'ng2-daterangepicker';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OResponse } from '../../../../service/service';
declare var moment: any;

@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.component.html',
    styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUDashboardComponent implements OnInit {
    @ViewChild(DaterangePickerComponent)
    private picker: DaterangePickerComponent;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {
    }
    public TodayStartTime = null;
    public TodayEndTime = null;

    Type = 5;
    StartTime = null;
    EndTime = null;
    CustomType = 1;
    ngOnInit() {
        this._HelperService.FullContainer = false;
        if (this.StartTime == undefined) {
            this.StartTime = moment().startOf('month');
            this.EndTime = moment().endOf('month');
        }

        if (this.TodayStartTime == undefined) {
            this.TodayStartTime = moment().startOf('day');
            this.TodayEndTime = moment().add(1, 'minutes');
        }
        this.LoadData();
    }

    UserAnalytics_DateChange(Type) {
        this.Type = Type;
        var SDate;
        if (Type == 1) {
            SDate =
                {
                    start: moment().startOf('day'),
                    end: moment().endOf('day'),
                }
        }
        else if (Type == 2) {
            SDate =
                {
                    start: moment().subtract(1, 'days').startOf('day'),
                    end: moment().subtract(1, 'days').endOf('day'),
                }
        }
        else if (Type == 3) {
            SDate =
                {
                    start: moment().startOf('isoWeek'),
                    end: moment().endOf('isoWeek'),
                }
        }
        else if (Type == 4) {
            SDate =
                {
                    start: moment().subtract(1, 'weeks').startOf('isoWeek'),
                    end: moment().subtract(1, 'weeks').endOf('isoWeek'),
                }
        }
        else if (Type == 5) {
            SDate =
                {
                    start: moment().startOf('month'),
                    end: moment().endOf('month'),
                }
        }
        else if (Type == 6) {
            SDate =
                {
                    start: moment().startOf('month').subtract(1, 'months'),
                    end: moment().startOf('month').subtract(1, 'days'),
                }
        }
        else if (Type == 7) {
            SDate =
                {
                    start: new Date(2017, 0, 1, 0, 0, 0, 0),
                    end: moment().endOf('day'),
                }
        }
        if (this.picker.datePicker != undefined) {
            this.picker.datePicker.setStartDate(SDate.start);
            this.picker.datePicker.setEndDate(SDate.end);
        }
        this.StartTime = SDate.start;
        this.EndTime = SDate.end;
        this.LoadData();
    }
    LoadData() {
        this._HelperService.GetAccountOverview(this._HelperService.AppConfig.ActiveOwnerKey, null, this.StartTime, this.EndTime);
        this.GetLiveTerminals();
    }

    public AllTerminals = [];
    public LiveTerminals = {
        Data: [],
    };
    public TotalTerminals = 0;
    public LiveTerminalCount = 0;
    public IdleTerminalCount = 0;
    public DeadTerminalCount = 0;
    public Providers: any[] = [];
    public Merchants: any[] = [];
    public Stores: any[] = [];
    public Acquirers: any[] = [];
    public SelectedMerchant = '0';
    public SelectedTerminalStatus = '0';
    public SelectedStore = '0';
    public SelectedProvider = '0';

    public ActiveTerminals: any[] = [];
    public IdleTerminals: any[] = [];
    public DeadTerminals: any[] = [];


    GetLiveTerminals() {
        this.AllTerminals = [];
        this.LiveTerminals = {
            Data: [],
        };

        this.TotalTerminals = 0;
        this.LiveTerminalCount = 0;
        this.IdleTerminalCount = 0;
        this.DeadTerminalCount = 0;
        this.Providers = [];
        this.Merchants = [];
        this.Stores = [];
        this.Acquirers = [];
        this.SelectedMerchant = '0';
        this.SelectedTerminalStatus = '0';
        this.LiveTerminals = {
            Data: [],
        };
        this._HelperService.IsFormProcessing = true;
        var Data = {
            Task: "getliveterminals",
            Type: "relationshipmanager",
            ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
            Data
        );
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.LiveTerminals = _Response.Result;
                    this.AllTerminals = this.LiveTerminals.Data;
                    this.RefreshTerminalsData();

                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            }
        );
    }

    public RefreshTerminalsData() {
        if (this.LiveTerminals != undefined) {
            this.TotalTerminals = this.LiveTerminals.Data.length;
            this.LiveTerminals.Data.forEach(element => {
                var TLive = 0;
                var TIdle = 0;
                var TDead = 0;
                var DifferenceInfo = this._HelperService.GetTimeDifference(element.LastTransactionDate, moment());
                if (DifferenceInfo.Days == 0) {
                    element.Type = 'live';
                    TLive = 1;
                    this.LiveTerminalCount = this.LiveTerminalCount + 1;
                    this.ActiveTerminals.push(element);
                }
                else if (DifferenceInfo.Days > 0 && DifferenceInfo.Days < 8) {
                    element.Type = 'idle';
                    TIdle = 1;
                    this.IdleTerminalCount = this.IdleTerminalCount + 1;
                    this.IdleTerminals.push(element);

                }
                else {

                    element.Type = 'dead';
                    TDead = 1;
                    this.DeadTerminalCount = this.DeadTerminalCount + 1;
                    this.DeadTerminals.push(element);
                }
                element.LastTransactionDateD = this._HelperService.GetDateS(element.LastTransactionDate);
                element.LastTransactionDateT = this._HelperService.GetTimeS(element.LastTransactionDate);
                if (element.LastTransactionDate != "0001-01-01 00:00:00.000000") {
                    element.LastTransactionDateDiffS = this._HelperService.GetTimeDifferenceS(element.LastTransactionDate, moment());
                    element.LastTransactionDateDiff = this._HelperService.GetTimeDifference(element.LastTransactionDate, moment());
                }
                element.LastTransactionDate = this._HelperService.GetDateTimeS(element.LastTransactionDate);

                var ProviderIndex = this.Providers.findIndex(x => x.Name == element.ProviderDisplayName);
                if (ProviderIndex != -1) {
                    this.Providers[ProviderIndex].TotalTerminals = this.Providers[ProviderIndex].TotalTerminals + 1;
                    this.Providers[ProviderIndex].LiveTerminals = this.Providers[ProviderIndex].LiveTerminals + TLive;
                    this.Providers[ProviderIndex].IdleTerminals = this.Providers[ProviderIndex].IdleTerminals + TIdle;
                    this.Providers[ProviderIndex].DeadTerminals = this.Providers[ProviderIndex].DeadTerminals + TDead;
                    this.Providers[ProviderIndex].LastTransactionDate = element.LastTransactionDate;
                    this.Providers[ProviderIndex].IconUrl = element.ProviderIconUrl;

                    this.Providers[ProviderIndex].LastTransactionDateD = element.LastTransactionDateD;
                    this.Providers[ProviderIndex].LastTransactionDateT = element.LastTransactionDateT;
                    this.Providers[ProviderIndex].LastTransactionDateDiffS = element.LastTransactionDateDiffS;
                    this.Providers[ProviderIndex].LastTransactionDateDiff = element.LastTransactionDateDiff;

                }
                else {
                    var PItem =
                    {
                        Name: element.ProviderDisplayName,
                        TotalTerminals: 1,
                        LiveTerminals: TLive,
                        IdleTerminals: TIdle,
                        DeadTerminals: TDead,
                        LastTransactionDate: element.LastTransactionDate,
                        LastTransactionDateD: element.LastTransactionDateD,
                        LastTransactionDateT: element.LastTransactionDateT,
                        LastTransactionDateDiffS: element.LastTransactionDateDiffS,
                        LastTransactionDateDiff: element.LastTransactionDateDiff,
                        IconUrl: element.ProviderIconUrl,
                    };
                    this.Providers.push(PItem);
                }
                var AcquirerIndex = this.Acquirers.findIndex(x => x.Name == element.AcquirerDisplayName);
                if (AcquirerIndex != -1) {
                    this.Acquirers[AcquirerIndex].TotalTerminals = this.Acquirers[AcquirerIndex].TotalTerminals + 1;
                    this.Acquirers[AcquirerIndex].LiveTerminals = this.Acquirers[AcquirerIndex].LiveTerminals + TLive;
                    this.Acquirers[AcquirerIndex].IdleTerminals = this.Acquirers[AcquirerIndex].IdleTerminals + TIdle;
                    this.Acquirers[AcquirerIndex].DeadTerminals = this.Acquirers[AcquirerIndex].DeadTerminals + TDead;
                    this.Acquirers[AcquirerIndex].LastTransactionDate = element.LastTransactionDate;
                    if (element.AcquirerIconUrl != undefined) {
                        this.Acquirers[AcquirerIndex].IconUrl = element.AcquirerIconUrl;
                    }
                    this.Acquirers[AcquirerIndex].LastTransactionDateD = element.LastTransactionDateD;
                    this.Acquirers[AcquirerIndex].LastTransactionDateT = element.LastTransactionDateT;
                    this.Acquirers[AcquirerIndex].LastTransactionDateDiffS = element.LastTransactionDateDiffS;
                    this.Acquirers[AcquirerIndex].LastTransactionDateDiff = element.LastTransactionDateDiff;
                }
                else {
                    var PItem =
                    {
                        Name: element.AcquirerDisplayName,
                        TotalTerminals: 1,
                        LiveTerminals: TLive,
                        IdleTerminals: TIdle,
                        DeadTerminals: TDead,
                        LastTransactionDate: element.LastTransactionDate,
                        LastTransactionDateD: element.LastTransactionDateD,
                        LastTransactionDateT: element.LastTransactionDateT,
                        LastTransactionDateDiffS: element.LastTransactionDateDiffS,
                        LastTransactionDateDiff: element.LastTransactionDateDiff,
                        IconUrl: element.AcquirerIconUrl,
                    };
                    this.Acquirers.push(PItem);
                }

                var MerchantIndex = this.Merchants.findIndex(x => x.Name == element.MerchantDisplayName);
                if (MerchantIndex != -1) {
                    this.Merchants[MerchantIndex].TotalTerminals = this.Merchants[MerchantIndex].TotalTerminals + 1;
                    this.Merchants[MerchantIndex].LiveTerminals = this.Merchants[MerchantIndex].LiveTerminals + TLive;
                    this.Merchants[MerchantIndex].IdleTerminals = this.Merchants[MerchantIndex].IdleTerminals + TIdle;
                    this.Merchants[MerchantIndex].DeadTerminals = this.Merchants[MerchantIndex].DeadTerminals + TDead;
                    this.Merchants[MerchantIndex].LastTransactionDate = element.LastTransactionDate;
                    this.Merchants[MerchantIndex].LastTransactionDateD = element.LastTransactionDateD;
                    this.Merchants[MerchantIndex].LastTransactionDateT = element.LastTransactionDateT;
                    this.Merchants[MerchantIndex].LastTransactionDateDiffS = element.LastTransactionDateDiffS;
                    this.Merchants[MerchantIndex].LastTransactionDateDiff = element.LastTransactionDateDiff;
                    this.Merchants[MerchantIndex].IconUrl = element.MerchantIconUrl;
                }
                else {
                    var PItem =
                    {
                        Name: element.MerchantDisplayName,
                        TotalTerminals: 1,
                        LiveTerminals: TLive,
                        IdleTerminals: TIdle,
                        DeadTerminals: TDead,
                        LastTransactionDate: element.LastTransactionDate,
                        LastTransactionDateD: element.LastTransactionDateD,
                        LastTransactionDateT: element.LastTransactionDateT,
                        LastTransactionDateDiffS: element.LastTransactionDateDiffS,
                        LastTransactionDateDiff: element.LastTransactionDateDiff,
                        IconUrl: element.MerchantIconUrl,
                    };
                    this.Merchants.push(PItem);
                }

                var StoreIndex = this.Stores.findIndex(x => x.Name == element.StoreDisplayName);
                if (StoreIndex != -1) {
                    this.Stores[StoreIndex].TotalTerminals = this.Stores[StoreIndex].TotalTerminals + 1;
                    this.Stores[StoreIndex].LiveTerminals = this.Stores[StoreIndex].LiveTerminals + TLive;
                    this.Stores[StoreIndex].IdleTerminals = this.Stores[StoreIndex].IdleTerminals + TIdle;
                    this.Stores[StoreIndex].DeadTerminals = this.Stores[StoreIndex].DeadTerminals + TDead;
                    this.Stores[StoreIndex].LastTransactionDate = element.LastTransactionDate;
                    this.Stores[StoreIndex].IconUrl = element.MerchantIconUrl;
                    this.Stores[StoreIndex].LastTransactionDateD = element.LastTransactionDateD;
                    this.Stores[StoreIndex].LastTransactionDateT = element.LastTransactionDateT;
                    this.Stores[StoreIndex].LastTransactionDateDiffS = element.LastTransactionDateDiffS;
                    this.Stores[StoreIndex].LastTransactionDateDiff = element.LastTransactionDateDiff;
                }
                else {
                    var PItem =
                    {
                        Name: element.StoreDisplayName,
                        TotalTerminals: 1,
                        LiveTerminals: TLive,
                        IdleTerminals: TIdle,
                        DeadTerminals: TDead,
                        LastTransactionDate: element.LastTransactionDate,
                        LastTransactionDateD: element.LastTransactionDateD,
                        LastTransactionDateT: element.LastTransactionDateT,
                        LastTransactionDateDiffS: element.LastTransactionDateDiffS,
                        LastTransactionDateDiff: element.LastTransactionDateDiff,
                        IconUrl: element.MerchantIconUrl,
                    };
                    this.Stores.push(PItem);
                }

            });
        }
    }
}


export class OAccountOverview {
    public Merchants: number;
    public ActiveMerchants: number;
    public ActiveMerchantsDiff: number;
    public Terminals: number;
    public ActiveTerminals: number;
    public ActiveTerminalsDiff: number;
    public Transactions: number;
    public TransactionsDiff: number;
    public PurchaseAmount: number;
    public PurchaseAmountDiff: number;
    public CashRewardPurchaseAmount: number;
    public CashRewardPurchaseAmountDiff: number;
    public CardRewardPurchaseAmount: number;
    public CardRewardPurchaseAmountDiff: number;
}
import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { DataHelperService, HelperService, OList, OSelect } from "../../../../service/service";

@Component({
    selector: "tu-terminals",
    templateUrl: "./terminals.component.html"
})
export class TerminalsComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService
    ) { }
    public AcquirerId = 0;
    ngOnInit() {

        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null && this._HelperService.AppConfig.ActiveReferenceId == null) {
                if (this._HelperService.AppConfig.ActiveOwnerId != undefined && this._HelperService.AppConfig.ActiveOwnerId != null) {
                    this.AcquirerId = this._HelperService.AppConfig.ActiveOwnerId;
                    this.TerminalsList_Filter_Merchants_Load();
                    this.TerminalsList_Filter_Stores_Load();
                    this.TerminalsList_Filter_Providers_Load();
                    this.TerminalsList_Setup();
                }
                else {
                    this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
                }
            } else {
                this.AcquirerId = this._HelperService.AppConfig.ActiveReferenceId;
                this._HelperService.Get_UserAccountDetails(true);
                this.TerminalsList_Filter_Merchants_Load();
                this.TerminalsList_Filter_Stores_Load();
                this.TerminalsList_Filter_Providers_Load();
                this.TerminalsList_Setup();
            }
        });


    }

    public TerminalsList_Config: OList;
    TerminalsList_Setup() {
        this.TerminalsList_Config = {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.Core.GetRMTerminals,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCRmManager,
            Title: "Available Terminals",
            StatusType: "default",
            Type: this._HelperService.AppConfig.ListType.All,
            DefaultSortExpression: 'DisplayName asc',
            ReferenceId:  this._HelperService.AppConfig.ActiveOwnerId,
            SubReferenceId: this._HelperService.UserOwner.AccountId,
            TableFields: [
                {
                    DisplayName: "TID",
                    SystemName: "DisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: "Location",
                    SystemName: "StoreDisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: "Address",
                    SystemName: "Address",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },

                {
                    DisplayName: "Merchant",
                    SystemName: "MerchantDisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: "Provider",
                    SystemName: "OwnerDisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null
                }
            ]
        };
        this.TerminalsList_Config = this._DataHelperService.List_Initialize(
            this.TerminalsList_Config
        );
        this.TerminalsList_GetData();
    }
    TerminalsList_ToggleOption(event: any, Type: any) {
        this.TerminalsList_Config = this._DataHelperService.List_Operations(
            this.TerminalsList_Config,
            event,
            Type
        );
        if (this.TerminalsList_Config.RefreshData == true) {
            this.TerminalsList_GetData();
        }
    }
    TerminalsList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(
            this.TerminalsList_Config
        );
        this.TerminalsList_Config = TConfig;
    }
    TerminalsList_RowSelected(ReferenceData) {
    }




    public TerminalsList_Filter_Merchant_Option: Select2Options;
    public TerminalsList_Filter_Merchant_Selected = 0;
    TerminalsList_Filter_Merchants_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Merchant
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TerminalsList_Filter_Merchant_Option = {
            placeholder: 'Filter by Merchant',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TerminalsList_Filter_Merchants_Change(event: any) {
        if (event.value == this.TerminalsList_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Merchant_Selected, '=');
            this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
            this.TerminalsList_Filter_Merchant_Selected = 0;
        }
        else if (event.value != this.TerminalsList_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Merchant_Selected, '=');
            this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
            this.TerminalsList_Filter_Merchant_Selected = event.value;
            this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Merchant_Selected, '='));
        }
        this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TerminalsList_Filter_Store_Option: Select2Options;
    public TerminalsList_Filter_Store_Selected = 0;
    TerminalsList_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Store
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TerminalsList_Filter_Store_Option = {
            placeholder: 'Filter by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TerminalsList_Filter_Stores_Change(event: any) {
        if (event.value == this.TerminalsList_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Store_Selected, '=');
            this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
            this.TerminalsList_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TerminalsList_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Store_Selected, '=');
            this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
            this.TerminalsList_Filter_Store_Selected = event.value;
            this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'StoreId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Store_Selected, '='));
        }
        this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }


    public TerminalsList_Filter_Provider_Option: Select2Options;
    public TerminalsList_Filter_Provider_Selected = 0;
    TerminalsList_Filter_Providers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TerminalsList_Filter_Provider_Option = {
            placeholder: 'Filter by PTSP',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TerminalsList_Filter_Providers_Change(event: any) {
        if (event.value == this.TerminalsList_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Provider_Selected, '=');
            this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
            this.TerminalsList_Filter_Provider_Selected = 0;
        }
        else if (event.value != this.TerminalsList_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Provider_Selected, '=');
            this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
            this.TerminalsList_Filter_Provider_Selected = event.value;
            this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Provider_Selected, '='));
        }
        this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }
} 

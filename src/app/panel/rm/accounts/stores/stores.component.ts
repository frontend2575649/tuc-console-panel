import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { DataHelperService, HelperService, OList, OSelect } from "../../../../service/service";

@Component({
    selector: "tu-stores",
    templateUrl: "./stores.component.html"
})
export class StoresComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService
    ) { }
    ngOnInit() {
        this.StoresList_Filter_Owners_Load();
        this.StoresList_Setup();
    }

    public StoresList_Config: OList;
    StoresList_Setup() {
        this.StoresList_Config = {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.Core.GetRMStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCRmManager,
            Title: "Available Stores",
            StatusType: "default",
            Type: this._HelperService.AppConfig.ListType.All,
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "RmKey", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveOwnerKey, "="),
            DefaultSortExpression: 'StartDate desc',
            TableFields: [
                {
                    DisplayName: 'Location',
                    SystemName: 'DisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Address',
                    SystemName: 'Address',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Contact No',
                    SystemName: 'ContactNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Merchant',
                    SystemName: 'MerchantDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Terminals',
                    SystemName: 'TerminalsCount',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Date',
                    SystemName: 'StartDate',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    IsDateSearchField: true,
                },
            ]
        };
        this.StoresList_Config = this._DataHelperService.List_Initialize(
            this.StoresList_Config
        );
        this.StoresList_GetData();
    }
    StoresList_ToggleOption(event: any, Type: any) {
        this.StoresList_Config = this._DataHelperService.List_Operations(
            this.StoresList_Config,
            event,
            Type
        );
        if (this.StoresList_Config.RefreshData == true) {
            this.StoresList_GetData();
        }
    }
    StoresList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(
            this.StoresList_Config
        );
        this.StoresList_Config = TConfig;
    }
    StoresList_RowSelected(ReferenceData) {

    }



    public StoresList_Filter_Owners_Option: Select2Options;
    public StoresList_Filter_Owners_Selected = 0;
    StoresList_Filter_Owners_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
            ]
        };
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
            [
                this._HelperService.AppConfig.AccountType.Merchant
            ]
            , '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.StoresList_Filter_Owners_Option = {
            placeholder: 'Filter by Merchant',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    StoresList_Filter_Owners_Change(event: any) {
        if (event.value == this.StoresList_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '=');
            this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
            this.StoresList_Filter_Owners_Selected = 0;
        }
        else if (event.value != this.StoresList_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '=');
            this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
            this.StoresList_Filter_Owners_Selected = event.value;
            this.StoresList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '='));
        }
        this.StoresList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

}

import { ChangeDetectorRef, Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
import * as cloneDeep from 'lodash/cloneDeep';
declare var $: any;
import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: "tu-tumerchantplan",
  templateUrl: "./tumerchantplan.component.html",
})
export class TUMerchantPlanComponent implements OnInit, OnDestroy {
  CurrentPlan: any = {
    Name: null,
    TypeName: null,
    SellingPrice: null,
    Policy: null,


  };
  FreePlan: any = {
    Name: null,
    TypeName: null,
    SellingPrice: null,
    Policy: null,


  };
  BasicPlan: any = {
    Name: null,
    TypeName: null,
    SellingPrice: null,
    Policy: null,
  };
  PremiumPlan: any = {
    Name: null,
    TypeName: null,
    SellingPrice: null,
    Policy: null,
  };


  DateRangeOptions: any;
  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = true;
    this._HelperService.showAddNewSubAccBtn = false;
    this.DateRangeOptions = cloneDeep(this._HelperService.AppConfig.DateRangeOptions);
    this.DateRangeOptions.opens = "left";

  }

  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();

    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.MerchantPlan);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;

    }


    this.GetAccountSubscription();

  }


  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }
  }

  public _GetAccountSubscription = {

    Data: []
  }

  public GetAccountSubscription() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: 'getaccountsubscriptions',
      AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,
      Offset: 0,
      Limit: 10,
      // Source: this._HelperService.AppConfig.TransactionSource.Merchant
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscriptions, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this._GetAccountSubscription = _Response.Result;

          for (let index = 0; index < this._GetAccountSubscription.Data.length; index++) {
            const element: any = this._GetAccountSubscription.Data[index];


            if (this._GetAccountSubscription.Data[0]) {
              this.CurrentPlan = this._GetAccountSubscription.Data[0]


            }


            // for (let index = 0; index < this._GetAccountSubscription.Data.length; index++) {
            //   const element = this._GetAccountSubscription.Data[index];


            // }


            for (let index = 0; index < element.Features.length; index++) {
              const features = element.Features[index];

            }


          }
          // this._AccountBalance.Balance = _Response.Result.Balance / 100;
          // this._AccountBalance.Credit = _Response.Result.Credit / 100;
          // this._AccountBalance.Debit = _Response.Result.Debit / 100;
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }



}

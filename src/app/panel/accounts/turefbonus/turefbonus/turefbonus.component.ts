import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ChangeContext } from 'ng5-slider';
import swal from 'sweetalert2';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
declare var moment: any;
declare var $: any;
import { Observable, Subscription } from 'rxjs';


@Component({
    selector: 'tu-turefbonus',
    templateUrl: './turefbonus.component.html',
})
export class TURefbonusComponent implements OnInit, OnDestroy {

    public _ObjectSubscription: Subscription = null;

    getUserData: any;
    User_EmailAddress: any;
    User_Account: any;
    User_RefCode: any;
    Form_Add: FormGroup;
    Form_Edit: FormGroup;

    ngOnInit() {
        this._HelperService.StopClickPropogation();
        Feather.replace();
        this.RefBonusList_Setup();
        this.Form_Add_Load();
        this.Form_Edit_Load();
    }
    ngOnDestroy(): void {
        try { this._ObjectSubscription.unsubscribe(); } catch (error) { }
    }

    bonustype;
    public ResetFilterControls: boolean = true;
    BackReferral(){
        this._Router.navigate(['console' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AccountSettings.Referralb]);
    }

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _FilterHelperService: FilterHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
    ) {
        this.getUserDataemail();
    }

    getUserDataemail() {
        this.getUserData = this._HelperService.GetStorage("hca");
        // console.log(this.getUserData, "getdata");
        if (this.getUserData) {
            this.User_EmailAddress = this.getUserData.User.EmailAddress;
            this.User_Account = this.getUserData.UserAccount.AccountId;
            this.User_RefCode = this.getUserData.UserAccount.ReferralCode;
        }
    }

    Form_Add_Show() {
        this._HelperService.OpenModal('Form_Add_Content')
    };
    Form_Add_Close() {
        this._HelperService.CloseModal('Form_Add_Content')
    };
    Form_Edit_Show() {
        this._HelperService.OpenModal('Form_Edit_Content')
    };
    Form_Edit_Close() {
        this._HelperService.CloseModal('Form_Edit_Content')
    };

    Form_Add_Clear() {
        this.Form_Add_Load();
    }

    Form_Add_Load() {
        this.Form_Add = this._FormBuilder.group({
            Task: this._HelperService.AppConfig.Api.ThankUCash.SetReferralAmount,
            AuthAccountId: this.User_Account,
            EmailAddress: this.User_EmailAddress,
            Amount: ['', Validators.required],
            Percentage: null,
            Reward: null,
        });
    }

    Form_Add_Process(_FormValue: any) {

        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, _FormValue);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                    this.Form_Add_Clear();
                    this.TURef_GetData();
                    if (_FormValue.OperationType == 'close') {
                        this.Form_Add_Close();
                    }
                    this.Form_Add_Close();
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    _Details =
        {
            AccountDisplayName: null,
            AccountEmailAddress: null,
            AccountId: null,
            AccountKey: null,
            AccountMobileNumber: null,
            Amount: null,
            Comment: null,
            CreateDate: null,
            CreateDateD: null,
            CreateDateT: null,
            CreatedByDisplayName: null,
            CreatedById: null,
            CreatedByKey: null,
            DateSet: null,
            DateSetD: null,
            EmailAddress: null,
            ReferenceId: null,
            ReferenceKey: null,
            ReferenceNumber: null,
            StatusBadge: null,
            StatusC: null,
            StatusCode: null,
            StatusName: null,
        }
    Selected(ReferenceData: any) {
        console.log(ReferenceData);
        this.Form_Edit_Show();
        this._Details = ReferenceData;
    }

    Form_Edit_Clear() {
        this.Form_Edit_Load();
    }

    Form_Edit_Load() {
        this.Form_Edit = this._FormBuilder.group({
            Task: this._HelperService.AppConfig.Api.ThankUCash.UpdateRefId,
            AuthAccountId: this.User_Account,
            EmailAddress: this.User_EmailAddress,
            ReferralCode: this.User_RefCode,
            Amount: ['', Validators.required],
            Percentage: null,
            Reward: null,
        });
    }

    Form_Edit_Process(_FormValue: any) {

        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, _FormValue);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                    this.Form_Edit_Clear();
                    this.TURef_GetData();
                    if (_FormValue.OperationType == 'close') {
                        this.Form_Edit_Close();
                    }
                    this.Form_Edit_Close();
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    public RefBonusList_Config: OList;

    RefBonusList_Setup() {

        this.RefBonusList_Config = {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetReferralAmount,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
            Title: "",
            StatusType: "default",
            AuthAccountId: this.User_Account,
            EmailAddress: this.User_EmailAddress,
            DefaultSortExpression: "CreateDate desc",
            TableFields: [
                {
                    DisplayName: "Bonus Amount",
                    SystemName: "Amount",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "text-center",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    DefaultValue: "ThankUCash",
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Create Date",
                    SystemName: "DateSet",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "",
                    Show: true,
                    IsDateSearchField: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
            ]
        };

        this.RefBonusList_Config = this._DataHelperService.List_Initialize(this.RefBonusList_Config);
        this._HelperService.Active_FilterInit(this._HelperService.AppConfig.FilterTypeOption.SoldHistory, this.RefBonusList_Config);
        this.TURef_GetData();
    }

    RefBonusList_ToggleOption(event: any, Type: any) {
        // debugger;
        if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
            event.data = {
                SalesMin: this.TURef_ReferralRangeMinAmount,
                SalesMax: this.TURef_ReferralRangeMaxAmount,
            }
        }
        if (Type == "date") {
            this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
            this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
        }
        this._HelperService.Update_CurrentFilterSnap(event, Type, this.RefBonusList_Config);

        this.RefBonusList_Config = this._DataHelperService.List_Operations(this.RefBonusList_Config, event, Type);
        this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

        this.TodayStartTime = this.RefBonusList_Config.StartTime;
        this.TodayEndTime = this.RefBonusList_Config.EndTime;
        if (event != null) {
            for (let index = 0; index < this.RefBonusList_Config.Sort.SortOptions.length; index++) {
                const element = this.RefBonusList_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }
            }
        }
        if (
            (this.RefBonusList_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.TURef_GetData();
        }
    }

    timeout = null;
    RefBonusList_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {
            // if (Type == 'date') {
            //     event.start = this._HelperService.DateInUTC(event.start);
            //     event.end = this._HelperService.DateInUTC(event.end);
            // }

            this._HelperService.Update_CurrentFilterSnap(event, Type, this.RefBonusList_Config);
            this.RefBonusList_Config = this._DataHelperService.List_Operations(this.RefBonusList_Config, event, Type);
            this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

            this.TodayStartTime = this.RefBonusList_Config.StartTime;
            this.TodayEndTime = this.RefBonusList_Config.EndTime;
            if (event != null) {
                for (let index = 0; index < this.RefBonusList_Config.Sort.SortOptions.length; index++) {
                    const element = this.RefBonusList_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {
                        element.SystemActive = true;
                    }
                    else {
                        element.SystemActive = false;
                    }
                }
            }
            if (
                (this.RefBonusList_Config.RefreshData == true)
                && this._HelperService.DataReloadEligibility(Type)
            ) {
                this.TURef_GetData();
            }
        }, this._HelperService.AppConfig.SearchInputDelay);
    }

    public OverviewData: any = {
        Transactions: 0,
        InvoiceAmount: 0.0
    };
    ReferenceData: any;
    TURef_GetData() {
        var TConfig = this._DataHelperService.List_GetDataRef(this.RefBonusList_Config);
        this.RefBonusList_Config = TConfig;
        this.RefBonusList_Config.Data = this.ReferenceData;
        // console.log(this.ReferenceData, "ref data");
    }


    public DeleteConfiguration(): void {
        // if (this._BonusDetails.ReferenceKey != ReferenceData.ReferenceKey) {
        //     this._BonusDetails = ReferenceData;
        //     // this.TList_GetDetails();
        // }
        this._HelperService.IsFormProcessing = true;
        this._HelperService.AppConfig.ShowHeader = true;
        var pData = {
            Task: "deleteconfiguration",
            ReferenceKey: this._BonusDetails.ReferenceKey,
            ReferenceId: this._BonusDetails.ReferenceId,
        };

        let _OResponse: Observable<OResponse>;

        swal({
            title: this._HelperService.AppConfig.CommonResource.DeleteDetails,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            showCancelButton: true,
            position: this._HelperService.AppConfig.Alert_Position,
            animation: this._HelperService.AppConfig.Alert_AllowAnimation,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
        }).then((result) => {
            if (result.value) {
                this._HelperService.OpenModal('Edit_Rolefeature_Content');
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Configuration, pData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.NotifySuccess("Record Deleted  Successfully ");
                            this.RefBonusList_Setup();
                        } else {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }

    public _BonusDetails = {
        ReferenceId: 0,
        ReferenceKey: null,
        DataTypeCode: null,
        DataTypeName: null,
        DataTypeId: null,
        Name: null,
        SystemName: null,
        Description: null,
        Value: null,
        CreateDate: null,
        StatusCode: null,
        StatusName: null,
        StatusI: null,
        StatusB: null,
        AccountTypeCode: null,
        ValueHelperCode: null,
    }

    SetOtherFilters(): void {
        this.RefBonusList_Config.SearchBaseConditions = [];
        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Store));
        if (CurrentIndex != -1) {
            this.TURef_Filter_Store_Selected = 0;
            this.StoresEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }


    TURef_ReferralRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
    TURef_ReferralRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
    TURef_InvoiceRange_OnChange(changeContext: ChangeContext): void {
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TURef_ReferralRangeMinAmount, this.TURef_ReferralRangeMaxAmount);
        this.RefBonusList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.RefBonusList_Config.SearchBaseConditions);
        this.TURef_ReferralRangeMinAmount = changeContext.value;
        this.TURef_ReferralRangeMaxAmount = changeContext.highValue;
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TURef_ReferralRangeMinAmount, this.TURef_ReferralRangeMaxAmount);
        if (this.TURef_ReferralRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TURef_ReferralRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.RefBonusList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.RefBonusList_Config.SearchBaseConditions);
        }
        else {
            this.RefBonusList_Config.SearchBaseConditions.push(SearchCase);
        }
        this.RefBonusList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }



    public TURef_Filter_Store_Option: Select2Options;
    public TURef_Filter_Store_Toggle = false;
    public TURef_Filter_Store_Selected = 0;
    TURef_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TURef_Filter_Store_Option = {
            placeholder: 'Search by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TURef_Filter_Stores_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.RefBonusList_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Store
        );
        this.StoresEventProcessing(event);
    }

    StoresEventProcessing(event: any): void {
        if (event.value == this.TURef_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TURef_Filter_Store_Selected, '=');
            this.RefBonusList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.RefBonusList_Config.SearchBaseConditions);
            this.TURef_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TURef_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TURef_Filter_Store_Selected, '=');
            this.RefBonusList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.RefBonusList_Config.SearchBaseConditions);
            this.TURef_Filter_Store_Selected = event.value;
            this.RefBonusList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TURef_Filter_Store_Selected, '='));
        }
        this.RefBonusList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }


    TodayStartTime = null;
    TodayEndTime = null;

    Data: any = {};

    SetSalesRanges(): void {
        this.TURef_ReferralRangeMinAmount = this.RefBonusList_Config.SalesRange.SalesMin;
        this.TURef_ReferralRangeMaxAmount = this.RefBonusList_Config.SalesRange.SalesMax;
    }

    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();
        this.TURef_Filter_Stores_Load();
        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();
    }

    SetSearchRanges(): void {
        this.RefBonusList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalAmount', this.RefBonusList_Config.SearchBaseConditions);
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalAmount', this.TURef_ReferralRangeMinAmount, this.TURef_ReferralRangeMaxAmount);
        if (this.TURef_ReferralRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TURef_ReferralRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.RefBonusList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.RefBonusList_Config.SearchBaseConditions);
        }
        else {
            this.RefBonusList_Config.SearchBaseConditions.push(SearchCase);
        }
    }


    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetStoreConfig(this.RefBonusList_Config);
        this.SetOtherFilters();
        this.TURef_GetData();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_Store(Type, index);
        this._FilterHelperService.SetStoreConfig(this.RefBonusList_Config);
        if (Type == 'Time') {
            this._HelperService.AppConfig.DateRangeOptions.startDate = new Date(2017, 0, 1, 0, 0, 0, 0);
            this._HelperService.AppConfig.DateRangeOptions.endDate = moment().endOf("day");
        }
        this.SetOtherFilters();
        this.TURef_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            // input: "text",
            html:
                '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
                '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
                '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
            focusConfirm: false,
            preConfirm: () => {
                return {
                    filter: document.getElementById('swal-input1')['value'],
                    private: document.getElementById('swal-input2')['checked']
                }
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {

                if (result.value.filter.length < 5) {
                    this._HelperService.NotifyError('Enter filter name length greater than 4');
                    return;
                }

                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();
                this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);
                var AccessType: number = result.value.private ? 0 : 1;
                this._HelperService.Save_NewFilter(this._HelperService.AppConfig.FilterTypeOption.SoldHistory, AccessType);
                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();
                this._HelperService.Delete_Filter(this._HelperService.AppConfig.FilterTypeOption.SoldHistory);
                this._FilterHelperService.SetStoreConfig(this.RefBonusList_Config);
                this.TURef_GetData();
                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }


    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        if (this.TURef_ReferralRangeMaxAmount == null || this.TURef_ReferralRangeMaxAmount == undefined ||
            this.TURef_ReferralRangeMinAmount == null || this.TURef_ReferralRangeMinAmount == undefined) {
            this._HelperService.NotifyError("Value not be null or undefined");
        }
        else if (this.TURef_ReferralRangeMinAmount > this.TURef_ReferralRangeMaxAmount) {
            this._HelperService.NotifyError("Minimum  Amount should be less than Maximum  Amount");
        }
        else {
            this.SetSearchRanges();
            this._HelperService.MakeFilterSnapPermanent();
            this.TURef_GetData();
            this.ResetFilterUI(); this._HelperService.StopClickPropogation();
            if (ButtonType == 'Sort') {
                $("#RefBonusList_sdropdown").dropdown('toggle');
            } else if (ButtonType == 'Other') {
                $("#RefBonusList_fdropdown").dropdown('toggle');
            }
        }
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetStoreConfig(this.RefBonusList_Config);
        this.SetOtherFilters();
        this.SetSalesRanges();
        this.TURef_GetData();
        this.ResetFilterUI();
        this._HelperService.StopClickPropogation();
    }


}
import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable, Subscription } from "rxjs";
import { DataHelperService, HelperService, OResponse, OSelect, OUserDetails, FilterHelperService } from "../../../../service/service";
import { InputFileComponent, InputFile } from 'ngx-input-file';
declare let $: any;

@Component({
  selector: "tu-tuvasproduct",
  templateUrl: "./tuvasproduct.component.html"
})
export class TUVasProductComponent implements OnInit, OnDestroy {
isLoaded :boolean = true;

  public _CashierAddress: any = {};
  ToggleStoreSelect: number;
  showStorePicker: boolean;

  @ViewChild("inputfile")
  private InputFileComponent: InputFileComponent;

  CurrentImagesCount: number;

  //#region subscriptions 

  subscription: Subscription;
  ReloadSubscription: Subscription;

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.ReloadSubscription.unsubscribe();
  }

  //#endregion

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {

  }

  //#region MapCorrection 

  slideOpen: any = false;
  changeSlide(): void {
    this.slideOpen = !this.slideOpen;
    if (this.slideOpen) {
    }
  }

  //#endregion

  //#region ToogleDetailView 

  HideStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-ShowStoreDetail");
  }

  ShowStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-ShowStoreDetail");
    element.classList.remove("Hm-HideDiv");
  }

  //#endregion

  BackDropInit(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  ngOnInit() {
    //#region UIInit 
    Feather.replace();
    setTimeout(() => {
      this._HelperService.ValidateData();
    }, 500);
    this._HelperService.ContainerHeight = window.innerHeight;
    this._HelperService.AppConfig.ShowHeader = true;
    this.BackDropInit();
    //#endregion
    //#region Subscriptions 
    this.subscription = this._HelperService.isprocessingtoogle
      .subscribe((item) => {
        this._ChangeDetectorRef.detectChanges();
      });
    this.ReloadSubscription = this._HelperService.ReloadEventEmit.subscribe((number) => {

    });
    //#endregion
    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.VasProduct);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.VasProductName = StorageDetails.VasProductName;
      this._HelperService.VasProductCategoryName = StorageDetails.VasProductCategoryName;
      this._HelperService.VasRewardPer = StorageDetails.VasRewardPer;
      this._HelperService.VasTucCommission = StorageDetails.VasTucCommission;
      this._HelperService.StatusName = StorageDetails.StatusName;
      this._HelperService.IconUrl = StorageDetails.IconUrl;
      this._HelperService.UserRewardPercentage =  StorageDetails.UserRewardPercentage;

    }
    //#region DropdownInit 
    // this.GetCashiersDetails();
    //#endregion
    this.Form_EditUser_Load();
    this.TUTr_Filter_Stores_Load();

    this.CurrentImagesCount = 0;
    this._HelperService.Icon_Crop_Clear();
    this._HelperService._InputFileComponent = this.InputFileComponent;
    this.InputFileComponent.onChange = (files: Array<InputFile>): void => {
      if (files.length >= this.CurrentImagesCount) {
        this._HelperService._SetFirstImageOrNone(this.InputFileComponent.files);
      }
      this.CurrentImagesCount = files.length;
    };
  }

  //#region EditUser 

  Form_EditUser: FormGroup;

  //#region EditUser_Address 

  _CurrentAddress: any = {};

  Form_EditUser_Address: string = null;
  Form_EditUser_Latitude: number = 0;
  Form_EditUser_Longitude: number = 0;

  @ViewChild('placesStore') placesStore: GooglePlaceDirective;

  Form_EditUser_PlaceMarkerClick(event) {
    this.Form_EditUser_Latitude = event.coords.lat;
    this.Form_EditUser_Longitude = event.coords.lng;
  }
  public Form_EditUser_AddressChange(address: Address) {
    this.Form_EditUser_Latitude = address.geometry.location.lat();
    this.Form_EditUser_Longitude = address.geometry.location.lng();
    this.Form_EditUser_Address = address.formatted_address;
    this.Form_EditUser.controls['Latitude'].setValue(this.Form_EditUser_Latitude);
    this.Form_EditUser.controls['Longitude'].setValue(this.Form_EditUser_Longitude);
    this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
  }

  //#endregion

  Form_EditUser_Show() {
    this._HelperService.OpenModal("Form_EditUser_Content");
  }
  Form_EditUser_Close() {
    // this._Router.navigate([
    //     this._HelperService.AppConfig.Pages.System.AdminUsers
    // ]);
    this._HelperService.OpenModal("Form_EditUser_Content");
  }
  Form_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;
    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;
    this.Form_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.UpdateVasProduct,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
     
      StatusCode: this._HelperService.AppConfig.Status.Active,
      Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      RewardPercentage: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])],
      UserRewardPercentage: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])],
      
    });
  }
  Form_EditUser_Clear() {
    //this.Form_EditUser.reset();
    //this.Form_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_EditUser_Process(_FormValue: any) {
  
    _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveReferenceKey;
    _FormValue.ReferenceId = this._HelperService.AppConfig.ActiveReferenceId;

    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.Console.V3.Vas,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Vas Product Updated successfully");
          this._HelperService.CloseModal('off-canvas')
          this.Form_EditUser_Clear();
          this.RemoveOffCanvas();
          // if (_FormValue.OperationType == "close") {
          //   this.Form_EditUser_Close();
          // }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region OffCanvasNBackdrop 
  @ViewChild("offCanvas") divView: ElementRef;

  ShowOffCanvas() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  RemoveOffCanvas() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }
  //#endregion


  //#region CashierDetails 

  public _CashierDetails: any =
    {
      ManagerName: null,
      BranchName: null,
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }
  GetCashiersDetails() {

    this._HelperService.IsFormProcessing = true;
  this.isLoaded = false;

    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetCashier,
      // AccountId: this._HelperService.UserAccount.AccountId,
      // AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountKey: this._HelperService.AppConfig.ActiveCashierReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveCashierReferenceId,


    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
           this.isLoaded = true;
          this._CashierDetails = _Response.Result;
          this._CashierDetails.CreateDate = this._HelperService.GetDateS(this._CashierDetails.CreateDate);
          this._CashierDetails.ModifyDate = this._HelperService.GetDateS(this._CashierDetails.ModifyDate);
          this._CashierDetails.StatusI = this._HelperService.GetStatusIcon(this._CashierDetails.StatusCode);
          this._CashierDetails.StatusB = this._HelperService.GetStatusBadge(this._CashierDetails.StatusCode);
          this._CashierDetails.StatusC = this._HelperService.GetStatusColor(this._CashierDetails.StatusCode);
          this._CashierAddress = this._CashierDetails.Address;

          this.showStorePicker = false;
          this._ChangeDetectorRef.detectChanges();

          this.TUTr_Filter_Store_Option.placeholder = this._CashierDetails.StoreDisplayName;
          this.TUTr_Filter_Stores_Load();

          this.showStorePicker = true;
          this._ChangeDetectorRef.detectChanges();
          //#region ResponseInit 
          this._CashierDetails.EndDateS = this._HelperService.GetDateS(
            this._CashierDetails.EndDate
          );
          this._CashierDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._CashierDetails.CreateDate
          );
          this._CashierDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._CashierDetails.ModifyDate
          );
          this._CashierDetails.StatusI = this._HelperService.GetStatusIcon(
            this._CashierDetails.StatusCode
          );
          this._CashierDetails.StatusB = this._HelperService.GetStatusBadge(
            this._CashierDetails.StatusCode
          );
          this._CashierDetails.StatusC = this._HelperService.GetStatusColor(
            this._CashierDetails.StatusCode
          );

          //#endregion

          //#region InitLocationParams 
          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {

            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;

            this.Form_EditUser_Latitude = _Response.Result.Latitude;
            this.Form_EditUser_Longitude = _Response.Result.Longitude;

            this.Form_EditUser.controls['Latitude'].setValue(_Response.Result.Latitude);
            this.Form_EditUser.controls['Longitude'].setValue(_Response.Result.Longitude);

          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }

          this.Form_EditUser.controls['StoreId'].setValue(_Response.Result.StoreReferenceId);
          this.Form_EditUser.controls['StoreKey'].setValue(_Response.Result.StoreReferenceKey);
          //#endregion

        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion
  Form_EditUser_Block() {

  }
  public TUTr_Filter_Store_Option: Select2Options;
  TUTr_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Store
        }
      ]
    };

    var OwnerKey = this._HelperService.UserAccount.AccountId;
    if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
      OwnerKey = this._HelperService.UserAccount.AccountId;
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, OwnerKey, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Store_Option = {
      placeholder: this._CashierDetails.StoreDisplayName,
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Stores_Change(event: any) {
    this.Form_EditUser.patchValue(
      {
        StoreId: event.data[0].ReferenceId,
        StoreKey: event.data[0].ReferenceKey,
      }


    );

  }

  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }
  BlockCashier() {
    this._HelperService.OpenModal("BlockCashier");
  }

  public Block(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "disablecashier",
      AccountId: this._CashierDetails.ReferenceId,
      AccountKey: this._CashierDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess("Cashier Disabled Successfully ");
          this._HelperService.CloseModal("BlockPos");
          this.GetCashiersDetails();

        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public UnBlock(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "enablecashier",
      AccountId: this._CashierDetails.ReferenceId,
      AccountKey: this._CashierDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess("Cashier Enabled Successfully ");
          this._HelperService.CloseModal("BlockPos");
          this.GetCashiersDetails();

        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

}

export class OAccountOverview {
  public Merchants: number;
  public Stores: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}

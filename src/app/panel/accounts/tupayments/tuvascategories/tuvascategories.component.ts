import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
declare var moment: any;
declare var $: any;


@Component({
    selector: 'tu-tuvascategories',
    templateUrl: './tuvascategories.component.html',
})
export class TUVasCategoriesComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _FilterHelperService: FilterHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
    ) {

    }

    public ResetFilterControls: boolean = true;
    public AcquirerId = 0;
    ngOnInit() {
        Feather.replace();
        this._HelperService.ValidateData();
        this._HelperService.StopClickPropogation();

        

    }
}

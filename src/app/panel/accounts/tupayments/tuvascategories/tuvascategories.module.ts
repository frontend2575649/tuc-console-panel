import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Ng5SliderModule } from 'ng5-slider';
import { MainPipe } from '../../../../service/main-pipe.module'
import { TUVasCategoriesComponent } from "./tuvascategories.component";
import { MerchantguardGuard } from "src/app/service/guard/merchantguard.guard";
const routes: Routes = [{ path: "", component: TUVasCategoriesComponent,
children: [
  { path: "", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../tuairtime/tuairtime.module#TUAirtimeModule" },
  { path: "airtime", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../tuairtime/tuairtime.module#TUAirtimeModule" },
  { path: "electricity", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../tuelectricity/tuelectricity.module#TUElectricityModule" },
  { path: "tollgate", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../tutollgate/tutollgate.module#TUTollGateModule" },
  { path: "tvrecharge", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../tutvrechanrge/tutvrechanrge.module#TUTvRechargeModule" },


]


}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUCategoriesRoutingModule { }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    Ng5SliderModule,
    TUCategoriesRoutingModule,
    MainPipe
  ],
  declarations: [TUVasCategoriesComponent]
})
export class TUVasCategoriesModule { }

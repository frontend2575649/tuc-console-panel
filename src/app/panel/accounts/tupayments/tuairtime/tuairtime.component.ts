import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
declare var moment: any;
declare var $: any;


@Component({
    selector: 'tu-airtime',
    templateUrl: './tuairtime.component.html',
})
export class TUAirtimeComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _FilterHelperService: FilterHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
    ) {

    }

    public ResetFilterControls: boolean = true;
    public AcquirerId = 0;
    public ResetSortingUi: boolean = true;
    ngOnInit() {
        Feather.replace();
        this._HelperService.StopClickPropogation();

        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null && this._HelperService.AppConfig.ActiveReferenceId == null) {
                if (this._HelperService.AppConfig.ActiveOwnerId != null) {
                    this.AcquirerId = this._HelperService.AppConfig.ActiveOwnerId;
                    this.AirT_Setup();
                    this.AirT_Filter_Merchants_Load();
                    this.AirT_Filter_UserAccounts_Load();
                    this.AirT_Filter_Stores_Load();
                    this.AirT_Filter_Providers_Load();
                    this.AirT_Filter_Issuers_Load();
                    this.AirT_Filter_CardBrands_Load();
                    this.AirT_Filter_TransactionTypes_Load();
                    this.AirT_Filter_CardBanks_Load();
                    this.AirT_Filter_Banks_Load();
                    this.AirT_Filter_Terminals_Load();
                    this.AirT_Filter_Cashiers_Load();

                }
                else {
                    this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
                }
            } else {
                this.AcquirerId = this._HelperService.AppConfig.ActiveReferenceId;

                // this._HelperService.Get_UserAccountDetails(true);
                this.AirT_Setup();
                this.AirT_Filter_Merchants_Load();
                this.AirT_Filter_UserAccounts_Load();
                this.AirT_Filter_Stores_Load();
                this.AirT_Filter_Providers_Load();
                this.AirT_Filter_Issuers_Load();
                this.AirT_Filter_CardBrands_Load();
                this.AirT_Filter_TransactionTypes_Load();
                this.AirT_Filter_CardBanks_Load();
                this.AirT_Filter_Banks_Load();
                this.AirT_Filter_Terminals_Load();
                this.AirT_Filter_Cashiers_Load();


            }
            this.TodayStartTime = new Date(2017, 0, 1, 0, 0, 0, 0), moment();
            this.TodayEndTime = moment();
            // this.GetAccountOverviewLite();
        });

    }


    // SortUIReferesh() {
    //     setTimeout(() => {
    //       this.ResetSortUI();
    //     }, 100)
    // }

    ResetSortUI(): void {
        this.ResetSortingUi = false;
        this._ChangeDetectorRef.detectChanges();
        this.ResetSortingUi = true;
        this._ChangeDetectorRef.detectChanges();
    }

    RouteCustomer(ReferenceData) {
        //#region Save Current Merchant To Storage 

        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveCustomer,
            {
                ReferenceKey: ReferenceData.UserAccountKey,
                ReferenceId: ReferenceData.UserAccountId,
                DisplayName: ReferenceData.DisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.Customer,
            }
        );

        //#endregion

        //#region Set Active Reference Key To Current Merchant 

        this._HelperService.AppConfig.ActiveReferenceKey =
            ReferenceData.UserAccountKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.UserAccountId;

        //#endregion

        //#region navigate 

        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.CustomerPanel.Accounts.WalletInfo,
            ReferenceData.UserAccountKey,
            ReferenceData.UserAccountId,
        ]);

        //#endregion

    }

    CashierRoute(ReferenceData) {
        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveCashier,
            {
                ReferenceKey: ReferenceData.CashierKey,
                ReferenceId: ReferenceData.CashierId,
                DisplayName: ReferenceData.DisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.Cashier,
            }
        );

        //#endregion

        //#region Set Active Reference Key To Current Merchant 

        this._HelperService.AppConfig.ActiveCashierReferenceKey =
            ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveCashierReferenceId = ReferenceData.ReferenceId;

        //#endregion

        //#region navigate 

        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.CashierDetails.Overview,
            ReferenceData.CashierKey,
            ReferenceData.CashierId,
        ]);
    }

    StoreRoute(ReferenceData) {

        this._HelperService.StoreDetailParent = this._HelperService.AppConfig.StoreDetailParents.Transactions;

        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveStore,
            {
                ReferenceKey: ReferenceData.SubParentKey,
                ReferenceId: ReferenceData.SubParentId,
                DisplayName: ReferenceData.DisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
            }
        );

        this._HelperService.AppConfig.ActiveStoreReferenceKey =
            ReferenceData.SubParentKey;
        this._HelperService.AppConfig.ActiveStoreReferenceId = ReferenceData.SubParentId;

        // this._Router.navigate([
        //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Store
        //     .Dashboard,
        //   ReferenceData.ReferenceKey,
        //   ReferenceData.ReferenceId,
        // ]);

        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Store
                .SalesHistory,
            ReferenceData.SubParentKey,
            ReferenceData.SubParentId,
        ]);

    }

    TerminalRoute(ReferenceData) {
        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveTerminal,
            {
                ReferenceKey: ReferenceData.CreatedByKey,
                ReferenceId: ReferenceData.CreatedById,
                DisplayName: ReferenceData.DisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
            }
        );

        //#endregion

        //#region Set Active Reference Key To Current Merchant 

        this._HelperService.AppConfig.ActiveTerminalReferenceKey =
            ReferenceData.CreatedByKey;
        this._HelperService.AppConfig.ActiveTerminalReferenceId = ReferenceData.CreatedById;

        //#endregion

        //#region navigate 

        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Terminal.SalesHistory,
            ReferenceData.CreatedByKey,
            ReferenceData.CreatedById,
        ]);
    }

    AirT_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
    AirT_InvoiceRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
    AirT_InvoiceRange_OnChange(changeContext: ChangeContext): void {
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalPurchaseAmount', this.AirT_InvoiceRangeMinAmount, this.AirT_InvoiceRangeMaxAmount);
        this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
        this.AirT_InvoiceRangeMinAmount = changeContext.value;
        this.AirT_InvoiceRangeMaxAmount = changeContext.highValue;
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalPurchaseAmount', this.AirT_InvoiceRangeMinAmount, this.AirT_InvoiceRangeMaxAmount);
        if (this.AirT_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.AirT_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
        }
        else {
            this.AirT_Config.SearchBaseConditions.push(SearchCase);
        }
        this.AirT_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#region transactions 

    public AirT_Config: OList;
    AirT_Setup() {
        this.AirT_Config =
        {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.Core.GetVasProducts,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Vas,
            Title: 'Sales History',
            StatusType: 'default',
            Status: this._HelperService.AppConfig.StatusList.defaultitem,
            Type: this._HelperService.AppConfig.ListType.All,
            DefaultSortExpression: 'CreateDate desc',
            //SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "CategoryKey", this._HelperService.AppConfig.DataType.Text, 'airtime', "="),
            TableFields: [
                {
                    DisplayName: '#',
                    SystemName: 'ReferenceId',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Show: true,
                    Search: false,
                    Sort: false,
                },

                // {
                //     DisplayName: "Added On",
                //     SystemName: 'CreateDate',
                //     DataType: this._HelperService.AppConfig.DataType.Date,
                //     Class: 'td-date',
                //     Show: true,
                //     Search: false,
                //     IsDateSearchField: true,
                //     Sort: true,
                //     ResourceId: null,
                // },


                {
                    DisplayName: 'Provider',
                    SystemName: 'Name',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: true,
                    Sort: true,
                },

                {
                    DisplayName: 'Packages',
                    SystemName: 'TotalItem',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Show: true,
                    Search: false,
                    Sort: true,
                },

                {
                    DisplayName: 'Total Sale',
                    SystemName: 'TotalPurchase',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Show: true,
                    Search: false,
                    Sort: true,
                },
                {
                    DisplayName: 'Sale Achieved',
                    SystemName: 'TotalPurchaseAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Show: true,
                    Search: false,
                    Sort: true,
                },

                {
                    DisplayName: 'Reward %',
                    SystemName: 'RewardPercentage',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Show: true,
                    Search: false,
                    Sort: true,
                },
                {
                    DisplayName: 'User Reward %',
                    SystemName: 'UserRewardPercentage',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Show: true,
                    Search: false,
                    Sort: true,
                },
                {
                    DisplayName: 'TUC Commission  %',
                    SystemName: 'CommissionPercentage',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Show: true,
                    Search: false,
                    Sort: true,
                },



            ]
        }

        this.AirT_Config = this._DataHelperService.List_Initialize(this.AirT_Config);

        // var daterangetoday = {
        //     start: this._HelperService.DateInUTC(new Date(2017, 0, 1, 0, 0, 0, 0)),
        //     end: this._HelperService.DateInUTC(moment().endOf("day"))
        // };
        // this.AirT_Config = this._DataHelperService.List_Operations(this.AirT_Config, daterangetoday, this._HelperService.AppConfig.ListToggleOption.Date);
        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.AirtimeSales,
            this.AirT_Config
        );
        this.AirT_GetData(true);
    }
    AirT_ToggleOption_Date(event: any, Type: any) {
        this.AirT_ToggleOption(event, Type);



    }
    AirT_ToggleOption(event: any, Type: any) {
        if (Type == 'date') {
            event.start = this._HelperService.DateInUTC(event.start);
            event.end = this._HelperService.DateInUTC(event.end);
        }

        if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
            event.data = {
                SalesMin: this.AirT_InvoiceRangeMinAmount,
                SalesMax: this.AirT_InvoiceRangeMaxAmount
            }
        }

        this._HelperService.Update_CurrentFilterSnap(event, Type, this.AirT_Config);

        this.AirT_Config = this._DataHelperService.List_Operations(this.AirT_Config, event, Type);
        this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

        this.TodayStartTime = this.AirT_Config.StartTime;
        this.TodayEndTime = this.AirT_Config.EndTime;
        if (event != null) {
            for (let index = 0; index < this.AirT_Config.Sort.SortOptions.length; index++) {
                const element = this.AirT_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {

                    element.SystemActive = true;



                }

                else {
                    element.SystemActive = false;

                }

            }
        }


        if (
            (this.AirT_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.AirT_GetData();
        }
    }
    timeout = null;
    AirT_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {
            if (Type == 'date') {
                event.start = this._HelperService.DateInUTC(event.start);
                event.end = this._HelperService.DateInUTC(event.end);
            }

            this._HelperService.Update_CurrentFilterSnap(event, Type, this.AirT_Config);

            this.AirT_Config = this._DataHelperService.List_Operations(this.AirT_Config, event, Type);
            this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

            this.TodayStartTime = this.AirT_Config.StartTime;
            this.TodayEndTime = this.AirT_Config.EndTime;
            if (event != null) {
                for (let index = 0; index < this.AirT_Config.Sort.SortOptions.length; index++) {
                    const element = this.AirT_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {

                        element.SystemActive = true;



                    }

                    else {
                        element.SystemActive = false;

                    }

                }
            }


            if (
                (this.AirT_Config.RefreshData == true)
                && this._HelperService.DataReloadEligibility(Type)
            ) {
                this.AirT_GetData(true);
            }
        }, this._HelperService.AppConfig.SearchInputDelay);


    }


    public OverviewData: any = {
        Transactions: 0,
        InvoiceAmount: 0.0
    };

    AirT_GetData(isFirstTime?: boolean) {
        // this.GetOverviews(this.AirT_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getsaletransactionsoverview);

        // var TConfig = this._DataHelperService.List_GetDataWithOutSort(this.AirT_Config, undefined, undefined, isFirstTime ? isFirstTime : false);
        // this.AirT_Config = TConfig;

        var TConfig = this._DataHelperService.List_GetData(
            this.AirT_Config
        );
        this.AirT_Config = TConfig;

        this._DataHelperService.statusOption(this.AirT_Config.StatusOptions);
    }
    AirT_RowSelected(ReferenceData) {
        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.VasProduct,
            {
                ReferenceKey: ReferenceData.ReferenceKey,
                ReferenceId: ReferenceData.ReferenceId,
                VasProductName: ReferenceData.Name,
                VasProductCategoryName: ReferenceData.CategoryName,
                VasRewardPer: ReferenceData.RewardPercentage,
                VasTucCommission: ReferenceData.CommissionPercentage,
                IconUrl: ReferenceData.IconUrl,
                StatusName: ReferenceData.StatusName,
                UserRewardPercentage: ReferenceData.UserRewardPercentage

            }
        );

        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
        this._HelperService.VasProductName = ReferenceData.Name;
        this._HelperService.VasProductCategoryName = ReferenceData.CategoryName;
        this._HelperService.VasRewardPer = ReferenceData.RewardPercentage;
        this._HelperService.VasTucCommission = ReferenceData.CommissionPercentage;
        this._HelperService.StatusName = ReferenceData.StatusName;
        this._HelperService.IconUrl = ReferenceData.IconUrl;
        this._HelperService.UserRewardPercentage = ReferenceData.UserRewardPercentage;


        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Payments
                .SaleHistory,
            ReferenceData.ReferenceKey,
            ReferenceData.ReferenceId,
        ]);


    }

    //#endregion

    SetOtherFilters(): void {
        this.AirT_Config.SearchBaseConditions = [];
        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.AirtimeSales.Merchant));
        if (CurrentIndex != -1) {
            this.AirT_Filter_Merchant_Selected = 0;
            this.MerchantEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.AirtimeSales.Provider));
        if (CurrentIndex != -1) {
            this.AirT_Filter_Provider_Selected = 0;
            this.ProvidersEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.AirtimeSales.Store));
        if (CurrentIndex != -1) {
            this.AirT_Filter_Store_Selected = 0;
            this.StoresEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.AirtimeSales.TransactionType));
        if (CurrentIndex != -1) {
            this.AirT_Filter_TransactionType_Selected = 0;
            this.TransTypeEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.AirtimeSales.CardBank));
        if (CurrentIndex != -1) {
            this.AirT_Filter_CardBank_Selected = 0;
            this.CardBankEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.AirtimeSales.CardBrand));
        if (CurrentIndex != -1) {
            this.AirT_Filter_CardBrand_Selected = 0;
            this.CardBrandEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }

    SetSalesRanges(): void {
        this.AirT_InvoiceRangeMinAmount = this.AirT_Config.SalesRange.SalesMin;
        this.AirT_InvoiceRangeMaxAmount = this.AirT_Config.SalesRange.SalesMax;
    }

    //#region dropdowns 

    //#region merchant 

    public AirT_Filter_Merchant_Option: Select2Options;
    public AirT_Filter_Merchant_Selected = 0;
    AirT_Filter_Merchants_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
            ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: "AccountTypeCode",
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: "=",
                //     SearchValue: this._HelperService.AppConfig.AccountType.Merchant
                // }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.AirT_Filter_Merchant_Option = {
            placeholder: 'Search By Merchant',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    AirT_Filter_Merchants_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.AirT_Config,
            this._HelperService.AppConfig.OtherFilters.AirtimeSales.Merchant
        );
        this.MerchantEventProcessing(event);
    }

    MerchantEventProcessing(event: any): void {
        if (event.value == this.AirT_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Merchant_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_Merchant_Selected = 0;
        }
        else if (event.value != this.AirT_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Merchant_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_Merchant_Selected = event.value;
            this.AirT_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Merchant_Selected, '='));
        }
        this.AirT_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.AirT_Filter_Store_Toggle = true;

        this.AirT_Filter_Stores_Load();
        this.AirT_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
            this.AirT_Filter_Store_Toggle = false;
        }, 500);
    }

    //#endregion

    //#region stores 

    public AirT_Filter_Store_Option: Select2Options;
    public AirT_Filter_Store_Toggle = false;
    public AirT_Filter_Store_Selected = 0;
    AirT_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
            AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            AccountId: this._HelperService.AppConfig.ActiveOwnerId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.AirT_Filter_Store_Option = {
            placeholder: 'Search by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    AirT_Filter_Stores_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.AirT_Config,
            this._HelperService.AppConfig.OtherFilters.AirtimeSales.Store
        );
        this.StoresEventProcessing(event);
    }

    StoresEventProcessing(event: any): void {
        if (event.value == this.AirT_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Store_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_Store_Selected = 0;
        }
        else if (event.value != this.AirT_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Store_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_Store_Selected = event.value;
            this.AirT_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Store_Selected, '='));
        }
        this.AirT_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.AirT_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion

    //#region Cashiers 

    public AirT_Filter_Cashier_Option: Select2Options;
    public AirT_Filter_Cashier_Toggle = false;
    public AirT_Filter_Cashier_Selected = 0;
    AirT_Filter_Cashiers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetCashiers,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            AccountId: this._HelperService.AppConfig.ActiveOwnerId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.AirT_Filter_Cashier_Option = {
            placeholder: 'Search by Cashier',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    AirT_Filter_Cashiers_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.AirT_Config,
            this._HelperService.AppConfig.OtherFilters.AirtimeSales.Cashier
        );
        this.CashiersEventProcessing(event);
    }

    CashiersEventProcessing(event: any): void {
        if (event.value == this.AirT_Filter_Cashier_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CashierId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Cashier_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_Cashier_Selected = 0;
        }
        else if (event.value != this.AirT_Filter_Cashier_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CashierId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Cashier_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_Cashier_Selected = event.value;
            this.AirT_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CashierId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Cashier_Selected, '='));
        }
        this.AirT_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.AirT_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion


    //#region Terminal Filter
    public AirT_Filter_Terminal_Option: Select2Options;
    public AirT_Filter_Terminal_Toggle = false;
    public AirT_Filter_Terminal_Selected = 0;
    AirT_Filter_Terminals_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminals,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
            AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            AccountId: this._HelperService.AppConfig.ActiveOwnerId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "TerminalId",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.AirT_Filter_Terminal_Option = {
            placeholder: 'Search by Terminal',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    AirT_Filter_Terminals_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.AirT_Config,
            this._HelperService.AppConfig.OtherFilters.AirtimeSales.Terminal
        );
        this.TerminalsEventProcessing(event);
    }

    TerminalsEventProcessing(event: any): void {
        if (event.value == this.AirT_Filter_Terminal_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Terminal_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_Terminal_Selected = 0;
        }
        else if (event.value != this.AirT_Filter_Terminal_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Terminal_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_Terminal_Selected = event.value;
            this.AirT_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Terminal_Selected, '='));
        }
        this.AirT_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.AirT_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }
    //#endregion End


    //#region useraccount 

    public AirT_Filter_UserAccount_Option: Select2Options;
    public AirT_Filter_UserAccount_Selected = 0;
    AirT_Filter_UserAccounts_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "MobileNumber",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.AppUser
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.AirT_Filter_UserAccount_Option = {
            placeholder: 'Search Customer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    AirT_Filter_UserAccounts_Change(event: any) {
        if (event.value == this.AirT_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_UserAccount_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_UserAccount_Selected = 0;
        }
        else if (event.value != this.AirT_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_UserAccount_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_UserAccount_Selected = event.value;
            this.AirT_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_UserAccount_Selected, '='));
        }
        this.AirT_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region bank 

    public AirT_Filter_Bank_Option: Select2Options;
    public AirT_Filter_Bank_Selected = 0;
    AirT_Filter_Banks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
                }]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.AirT_Filter_Bank_Option = {
            placeholder: 'Search By Bank / Acquirer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    AirT_Filter_Banks_Change(event: any) {

        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.AirT_Config,
            this._HelperService.AppConfig.OtherFilters.AirtimeSales.Bank
        );

        this.BanksEventProcessing(event);
    }

    BanksEventProcessing(event: any): void {
        if (event.value == this.AirT_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Bank_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_Bank_Selected = 0;
        }
        else if (event.value != this.AirT_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Bank_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_Bank_Selected = event.value;
            this.AirT_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Bank_Selected, '='));
        }
        this.AirT_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.AirT_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion

    //#region provider 

    public AirT_Filter_Provider_Option: Select2Options;
    public AirT_Filter_Provider_Selected = 0;
    AirT_Filter_Providers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.AirT_Filter_Provider_Option = {
            placeholder: 'Search By Provider',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    AirT_Filter_Providers_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.AirT_Config,
            this._HelperService.AppConfig.OtherFilters.AirtimeSales.Provider
        );

        this.ProvidersEventProcessing(event);
    }

    ProvidersEventProcessing(event: any): void {
        if (event.value == this.AirT_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Provider_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_Provider_Selected = 0;
        }
        else if (event.value != this.AirT_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Provider_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_Provider_Selected = event.value;
            this.AirT_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Provider_Selected, '='));
        }
        this.AirT_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.AirT_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion

    //#region issuers 

    public AirT_Filter_Issuer_Option: Select2Options;
    public AirT_Filter_Issuer_Selected = 0;
    AirT_Filter_Issuers_Load() {
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }]
        };


        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
            [
                this._HelperService.AppConfig.AccountType.PosTerminal,
                this._HelperService.AppConfig.AccountType.PGAccount,
                this._HelperService.AppConfig.AccountType.Cashier,
            ], "=");

        if (this.AirT_Filter_Provider_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Provider_Selected, '=');
        }
        if (this.AirT_Filter_Bank_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'BankId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Bank_Selected, '=');
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'SubOwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictOr(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.AirT_Filter_Issuer_Option = {
            placeholder: 'Search By Issuer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    AirT_Filter_Issuers_Change(event: any) {
        if (event.value == this.AirT_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Issuer_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_Issuer_Selected = 0;
        }
        else if (event.value != this.AirT_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Issuer_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_Issuer_Selected = event.value;
            this.AirT_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_Issuer_Selected, '='));
        }
        this.AirT_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region transactionType 

    public AirT_Filter_TransactionType_Option: Select2Options;
    public AirT_Filter_TransactionType_Selected = 0;
    AirT_Filter_TransactionTypes_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionType
                },
                {
                    SystemName: "SubParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionTypeReward
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.AirT_Filter_TransactionType_Option = {
            placeholder: 'Search By Tran. Type',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    AirT_Filter_TransactionTypes_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.AirT_Config,
            this._HelperService.AppConfig.OtherFilters.AirtimeSales.TransactionType
        );

        this.TransTypeEventProcessing(event);
    }

    TransTypeEventProcessing(event: any): void {
        if (event.value == this.AirT_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_TransactionType_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_TransactionType_Selected = 0;
        }
        else if (event.value != this.AirT_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_TransactionType_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_TransactionType_Selected = event.value;
            this.AirT_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_TransactionType_Selected, '='));
        }
        this.AirT_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region cardbrands 

    public AirT_Filter_CardBrand_Option: Select2Options;
    public AirT_Filter_CardBrand_Selected = 0;
    AirT_Filter_CardBrands_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "TypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.CardBrand
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.AirT_Filter_CardBrand_Option = {
            placeholder: 'Search By Card Brand',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    AirT_Filter_CardBrands_Change(event: any) {

        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.AirT_Config,
            this._HelperService.AppConfig.OtherFilters.AirtimeSales.CardBrand
        );
        this.CardBrandEventProcessing(event);
    }

    CardBrandEventProcessing(event: any): void {
        if (event.value == this.AirT_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_CardBrand_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_CardBrand_Selected = 0;
        }
        else if (event.value != this.AirT_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_CardBrand_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_CardBrand_Selected = event.value;
            this.AirT_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_CardBrand_Selected, '='));
        }
        this.AirT_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region cardbank 

    public AirT_Filter_CardBank_Option: Select2Options;
    public AirT_Filter_CardBank_Selected = 0;
    AirT_Filter_CardBanks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields:
                [
                    {
                        SystemName: "ReferenceId",
                        Type: this._HelperService.AppConfig.DataType.Number,
                        Id: true,
                        Text: false,
                    },
                    {
                        SystemName: "Name",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        Id: false,
                        Text: true
                    },
                    {
                        SystemName: "TypeCode",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        SearchCondition: "=",
                        SearchValue: this._HelperService.AppConfig.HelperTypes.CardBank
                    }
                ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.AirT_Filter_CardBank_Option = {
            placeholder: 'Search By Card Bank',
            ajax: _Transport,
            multiple: false,
            allowClear: true,

        };
    }

    AirT_Filter_CardBanks_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.AirT_Config,
            this._HelperService.AppConfig.OtherFilters.AirtimeSales.CardBank
        );

        this.CardBankEventProcessing(event);
    }

    CardBankEventProcessing(event: any): void {
        if (event.value == this.AirT_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_CardBank_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_CardBank_Selected = 0;
        }
        else if (event.value != this.AirT_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_CardBank_Selected, '=');
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
            this.AirT_Filter_CardBank_Selected = event.value;
            this.AirT_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.AirT_Filter_CardBank_Selected, '='));
        }
        this.AirT_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#endregion

    TodayStartTime = null;
    TodayEndTime = null;

    public _AccountOverview: OAccountOverview =
        {
            ActiveMerchants: 0,
            ActiveMerchantsDiff: 0,
            ActiveTerminals: 0,
            Idle: 0,
            Dead: 0,
            Active: 0,
            Inactive: 0,
            ActiveTerminalsDiff: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardPurchaseAmountDiff: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardPurchaseAmountDiff: 0,
            Merchants: 0,
            PurchaseAmount: 0,
            PurchaseAmountDiff: 0,
            Terminals: 0,
            Transactions: 0,
            TransactionsDiff: 0,
            UnusedTerminals: 0,
            IdleTerminals: 0,
            TerminalStatus: 0,
            DeadTerminals: 0,
            Total: 0,
            TotalTransactions: 0,
            TotalSale: 0,
        }

    Data: any = {};
    GetAccountOverviewLite(): void {
        this._HelperService.IsFormProcessing = true;
        this.Data = {
            Task: 'getaccountoverview',
            StartTime: this.TodayStartTime,
            EndTime: this.TodayEndTime,
            AccountKey: this._HelperService.UserAccount.AccountKey,
            AccountId: this._HelperService.UserAccount.AccountId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.Data);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._AccountOverview = _Response.Result as OAccountOverview;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();

        this.AirT_Filter_Merchants_Load();
        this.AirT_Filter_UserAccounts_Load();
        this.AirT_Filter_Stores_Load();
        this.AirT_Filter_Providers_Load();
        this.AirT_Filter_Issuers_Load();
        this.AirT_Filter_CardBrands_Load();
        this.AirT_Filter_TransactionTypes_Load();
        this.AirT_Filter_CardBanks_Load();

        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();

    }

    SetSearchRanges(): void {
        //#region Invoice 
        this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalPurchaseAmount', this.AirT_Config.SearchBaseConditions);
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalPurchaseAmount', this.AirT_InvoiceRangeMinAmount, this.AirT_InvoiceRangeMaxAmount);
        if (this.AirT_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.AirT_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.AirT_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.AirT_Config.SearchBaseConditions);
        }
        else {
            this.AirT_Config.SearchBaseConditions.push(SearchCase);
        }

        //#endregion      

    }

    //#region SalesOverview 


    GetOverviews(ListOptions: any, Task: string): any {
        this._HelperService.IsFormProcessing = true;

        ListOptions.SearchCondition = '';
        ListOptions = this._DataHelperService.List_GetSearchConditionTur(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }


        var pData = {
            Task: Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            AccountId: ListOptions.AccountId,
            AccountKey: ListOptions.AccountKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.OverviewData = _Response.Result as any;

                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);

            });


    }

    //#endregion

    //#region filterOperations

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetMerchantSalesConfig(this.AirT_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        this.SetSalesRanges();
        //#endregion

        this.AirT_GetData(true);
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_NoSort(Type, index);
        this._FilterHelperService.SetMerchantSalesConfig(this.AirT_Config);
        // if (Type == 'Time') {
        //     var daterangetoday = {
        //         start: this._HelperService.DateInUTC(new Date(2017, 0, 1, 0, 0, 0, 0)),
        //         end: this._HelperService.DateInUTC(moment().endOf("day"))
        //     };
        //     this.AirT_Config = this._DataHelperService.List_Operations(this.AirT_Config, daterangetoday, this._HelperService.AppConfig.ListToggleOption.Date);
        // }
        //#region setOtherFilters
        this.SetOtherFilters();
        this.SetSalesRanges();
        //#endregion

        this.AirT_GetData(true);
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            // input: "text",
            html:
                '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
                '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
                '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
            focusConfirm: false,
            preConfirm: () => {
                return {
                    filter: document.getElementById('swal-input1')['value'],
                    private: document.getElementById('swal-input2')['checked']
                }
            },
            // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            // inputAttributes: {
            //   autocapitalize: "off",
            //   autocorrect: "off",
            //   maxLength: "4",
            //   minLength: "4",
            // },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {

                if (result.value.filter.length < 4) {
                    this._HelperService.NotifyError('Enter filter name length greater than 4');
                    return;
                }

                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

                var AccessType: number = result.value.private ? 0 : 1;
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.AirtimeSales,
                    AccessType
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._HelperService.Delete_Filter(
                    this._HelperService.AppConfig.FilterTypeOption.AirtimeSales
                );
                this._FilterHelperService.SetMerchantSalesConfig(this.AirT_Config);
                this.AirT_GetData(true);

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        if (this.AirT_InvoiceRangeMaxAmount == null || this.AirT_InvoiceRangeMinAmount == null ||
            this.AirT_InvoiceRangeMaxAmount == undefined || this.AirT_InvoiceRangeMinAmount == undefined) {
            this._HelperService.NotifyError("Value not be null or undefined");
        }
        else if (this.AirT_InvoiceRangeMinAmount > this.AirT_InvoiceRangeMaxAmount) {
            this._HelperService.NotifyError("Minimum  Amount should be less than Maximum  Amount");
        }
        else {

            this.SetSearchRanges();
            this._HelperService.MakeFilterSnapPermanent();
            this.AirT_GetData();
            this.ResetFilterUI(); 
            this._HelperService.StopClickPropogation();

            if (ButtonType == 'Sort') {
                $("#AirT_sdropdown").dropdown('toggle');
            } else if (ButtonType == 'Other') {
                $("#AirT_fdropdown").dropdown('toggle');
            }
        }
    }
    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetMerchantSalesConfig(this.AirT_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        this.SetSalesRanges();
        //#endregion
        this.AirT_GetData(true);

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    //#endregion

}

export class OAccountOverview {
    public TotalTransactions?: number;
    public TotalSale?: number;

    public DeadTerminals?: number;
    public IdleTerminals?: number;
    public TerminalStatus?: number;
    public Total?: number;
    public Idle?: number;
    public Active?: number;
    public Dead?: number;
    public Inactive?: number;
    public UnusedTerminals?: number;
    public Merchants: number;
    public ActiveMerchants: number;
    public ActiveMerchantsDiff: number;
    public Terminals: number;
    public ActiveTerminals: number;
    public ActiveTerminalsDiff: number;
    public Transactions: number;
    public TransactionsDiff: number;
    public PurchaseAmount: number;
    public PurchaseAmountDiff: number;
    public CashRewardPurchaseAmount: number;
    public CashRewardPurchaseAmountDiff: number;
    public CardRewardPurchaseAmount: number;
    public CardRewardPurchaseAmountDiff: number;
}
import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import Highcharts from "highcharts";
import Exporting from "highcharts/modules/exporting";
import funnel from "highcharts/modules/funnel";
import { BaseChartDirective } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, ODealData, OResponse, OSalesTrend, OSalesTrendData, OSalesTrendDataHourly } from '../../../../service/service';
declare var moment: any;
declare var $: any;
import { ChartDataSets } from 'chart.js';
import * as cloneDeep from 'lodash/cloneDeep';

Exporting(Highcharts);
funnel(Highcharts);

@Component({
  selector: "tuvasanalytics",
  templateUrl: "./tuvasanalytics.component.html",
  styles: [
    `
      agm-map {
        height: 300px;
      }
    `
  ]
})
export class TuVasAnalyticsComponent implements OnInit {
  @ViewChild("container", { read: ElementRef }) container: ElementRef;

  public PostHorizontalData = [
    { data: [80], label: 'Active' },
    { data: [72], label: 'In-Active' },
    { data: [65], label: 'Dead ' },
    { data: [75], label: 'Idle' },
  ];

  lastdaytext = "LAST DAY";
  lastweektext = "LAST WEEK";
  lastweekCustom = "LAST WEEK";
  lastmonthtext = "LAST MONTH";
  lastyeartext = "LAST YEAR";
  _SaleByVas = [
    { data: [0], label: 'Toll gate' },
    { data: [0], label: 'Airtime' },
    { data: [0], label: 'Tv ' },
    { data: [0], label: 'Electricity' },
  ];
  _CustomerUsingVas = [
    { data: [0], label: 'Toll gate' },
    { data: [0], label: 'Airtime' },
    { data: [0], label: 'Tv ' },
    { data: [0], label: 'Electricity' },
  ];
  Types: any = {
    year: 'year',
    month: 'month',
    week: 'week',
    day: 'day',
    hour: 'hour'
  }

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];
  public TodayDate: any;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this.TodayDate = this._HelperService.GetDateTime(new Date());
  }
  StartDateTime = null;

  EndDateTime = null;
  //#endregion
  ngOnInit() {

    this._HelperService.FullContainer = false;

    this.StartDateTime = moment().startOf('day');
    this.EndDateTime = moment().endOf('day');

    this.LoadData();

    Highcharts.chart(this.container.nativeElement, {
      credits: {
        enabled: false
      },
      chart: {
        type: "funnel",
      },
      exporting: { enabled: false },
      title: {
        text: null,
        enabled: false
      },
      plotOptions: {
        xAxis: {
          labels: {
            rotation: 180,

          }
        },
        series: {
          dataLabels: {
            format: "<b>{point.name}</b> ({point.y:,.0f})",
            color:
              (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
              "black",
            softConnector: false,
            align: "center",
            allowOverlap: true,
            enabled: true,
            showInLegend: true,
            x: -250,
            y: -10
          },
          neckWidth: ["20%"],
          neckHeight: ["00%"],
          width: "30%",
          height: "100%",

        }
      },
      legend: {
        enabled: true
      },
      series: [
        {
          name: "Unique users",
          data: [
            ["Website visits", 40],
            ["Downloads", 20],
            ["Requested price", 21],
            ["Invoice sent", 24],
          ]
        }
      ]
    });

    Feather.replace();

  }

  LoadData() {
    this.GetLoyalityOverview(this.StartDateTime, this.EndDateTime)
    this.GetSalesVasOverview(this.StartDateTime, this.EndDateTime);

    this.RefreshTerminalStatusCount();


  }

  RefreshTerminalStatusCount() {
    this._HelperService.GetTerminalStatusCount(this._HelperService.UserAccount.AccountId, this._HelperService.AppConfig.AccountType.MerchantId, 0, 0);
  }
  public DateSelected = moment().startOf("day");
  DateChanged(event: any, Type: any): void {

    var ev: any = cloneDeep(event);

    this.DateSelected = moment(ev.start).startOf("day");
    this.StartDateTime = moment(ev.start).startOf('day');
    this.EndDateTime = moment(ev.end).endOf('day');
    this.GetLoyalityOverview(this.StartDateTime, this.EndDateTime);
    this.GetSalesVasOverview(this.StartDateTime, this.EndDateTime);



  }


  MerchantList() {
    this._Router.navigate([
      'console/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AccountSettings.Merchants,

    ]);
  }

  public LoyalityOverview: any =
    {


    }
  private LoyalityOvereview = {
    Task: 'getvaspurchaseoverview',
    StartDate: null,
    EndDate: null,
    // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };

  GetLoyalityOverview(StartDateTime, EndDateTime) {

    this._HelperService.IsFormProcessing = true;
    this.LoyalityOvereview.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.LoyalityOvereview.EndDate = this._HelperService.DateInUTC(EndDateTime);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Vas, this.LoyalityOvereview);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.LoyalityOverview = _Response.Result;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  public Lcctopup: any = {  }
  public Airtime: any = {  }
  public Tv: any = {  }
  public Electricity: any = {  }
  SalesVasOverview = [
    { data: [0], label: 'Toll gate' },
    { data: [0], label: 'Airtime' },
    { data: [0], label: 'Airtime ' },
    { data: [0], label: 'Electricity' },
  ];

  public SalesVasOverview1: any =[]
private SalesVasOvereview = {
  Task: 'getvaspurchasecategoryoverview',
  StartDate: null,
  EndDate: null,
  StoreReferenceId: 0,
  StoreReferenceKey: null,
  Type: null
};
public StoreLabel = [];
public StoreData = [];
public TPlotDataSet = [];

StoreLabel1: [
  {
    label: "Toll",
  },
  {
    label: "Airtime",
  },

  {
    label: "Airtime",
  },
  {
    label: "Electricity",
  }
  
] 

GetSalesVasOverview(StartDateTime, EndDateTime) {

  this._HelperService.IsFormProcessing = true;
  this.SalesVasOvereview.StartDate = this._HelperService.DateInUTC(StartDateTime);
  this.SalesVasOvereview.EndDate = this._HelperService.DateInUTC(EndDateTime);
  let _OResponse: Observable<OResponse>;
  _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Vas, this.SalesVasOvereview);
  _OResponse.subscribe(
    _Response => {
      this._HelperService.IsFormProcessing = false;
      if (_Response.Status == this._HelperService.StatusSuccess) {
        this.SalesVasOverview1 = _Response.Result;
        this.Lcctopup =  this.SalesVasOverview1[0].Overview;
        this.Airtime =  this.SalesVasOverview1[1].Overview;
        this.Tv =  this.SalesVasOverview1[2].Overview;
        this.Electricity =  this.SalesVasOverview1[3].Overview;
        this._SaleByVas = [
          { data: [ this.Lcctopup.InvoiceAmount], label: 'Toll gate' },
          { data: [this.Airtime.InvoiceAmount], label: 'Airtime' },
          { data: [this.Tv.InvoiceAmount], label: 'Tv ' },
          { data: [this.Electricity.InvoiceAmount], label: 'Electricity' },          
        ];

      
        this._CustomerUsingVas = [
          { data: [this.Lcctopup.Customers], label: 'Toll gate' },
          { data: [this.Airtime.Customers], label: 'Airtime' },
          { data: [this.Tv.Customers], label: 'Tv ' },
          { data: [this.Electricity.Customers], label: 'Electricity' },
        ];
 

      }
      else {
        this._HelperService.NotifyError(_Response.Message);
      }
    },
    _Error => {
      this._HelperService.IsFormProcessing = false;
      this._HelperService.HandleException(_Error);
    });
}



  





  MerchantRoute(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveMerchant,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveMerchantReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveMerchantReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.RewardPercentage,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);

  }

  TerminalRoute(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveTerminal,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveTerminalReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveTerminalReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Terminal.SalesHistory,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);
  }



  CloseRowModal(): void { }







}


export class OAccountOverview {
  public TerminalStatus?: any;
  public TotalTerminals?: number;
  public TotalSale?: number;
  public AverageTransactionAmount?: number;
  public TotalTransactions?: number;
  public ActiveTerminalsPerc?: number;
  public IdleTerminalsPerc?: number;
  public DeadTerminalsPerc?: number;
  public UnusedTerminalsPerc?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public Idle?: number;
  public Total?: number;

  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
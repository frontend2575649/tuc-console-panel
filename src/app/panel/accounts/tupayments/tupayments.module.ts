import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Ng5SliderModule } from 'ng5-slider';
import { MainPipe } from '../../../service/main-pipe.module'
import { TUPaymentsComponent } from "./tupayments.component";
import { MerchantguardGuard } from "src/app/service/guard/merchantguard.guard";
const routes: Routes = [{ path: "",canActivate:[MerchantguardGuard], component: TUPaymentsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUPayementsRoutingModule { }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    Ng5SliderModule,
    TUPayementsRoutingModule,
    MainPipe
  ],
  declarations: [TUPaymentsComponent]
})
export class TUPaymentsModule { }

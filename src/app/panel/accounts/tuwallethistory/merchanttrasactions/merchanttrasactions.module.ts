import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MerchanttrasactionsComponent } from './merchanttrasactions.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Ng5SliderModule } from 'ng5-slider';
import { MainPipe } from '../../../../service/main-pipe.module'
import { MerchantguardGuard } from "src/app/service/guard/merchantguard.guard";
const routes: Routes = [{ path: "", component: MerchanttrasactionsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MerchanttrasactionsRoutingModule { }

@NgModule({
  declarations: [MerchanttrasactionsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    Ng5SliderModule,
    MerchanttrasactionsRoutingModule,
    MainPipe  ]
})
export class MerchanttrasactionsModule { }

import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
declare var moment: any;
declare var $: any;

@Component({
    selector: 'app-merchanttrasactions',
    templateUrl: './merchanttrasactions.component.html',
})
export class MerchanttrasactionsComponent implements OnInit {

    public ListType: number;
    public displayCommentBox: boolean = false
    public disableAction: boolean = false
    public comment = ""

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _FilterHelperService: FilterHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
    ) {
    }
    @ViewChild("offCanvas") divView: ElementRef;
    @ViewChild("scrollTocomment") scrollTocomment: ElementRef;

    public ResetFilterControls: boolean = true;
    public AcquirerId = 0;
    public MerchantData:any 
    ngOnInit() {
        window.addEventListener('scroll', this.scroll, true);
        this._HelperService.ValidateData();
        this.MerchantData = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveMerchant)
        Feather.replace();
        this._HelperService.StopClickPropogation();
        this.ListType = 1;
        this.InitBackDropClickEvent();
        this.TUTr_Setup();


        this._ActivatedRoute.params.subscribe((params: Params) => {
            var referenceid = params['referenceid'];
            if (referenceid != undefined && referenceid != null) {
                this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
                this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
                this._HelperService.AppConfig.ActiveAccountId = params['accountid'];
                this._HelperService.AppConfig.ActiveAccountKey = params['accountkey'];
                //   this.ListAppUsage_GetDetails();
            }

        });

    }
    ngOnDestroy() {
        window.removeEventListener('scroll', this.scroll, true);
    }

    scroll = (event): void => {
        $(".daterangepicker").hide();

        $(".form-daterangepicker").blur();
    };
    openDatepicker(event) {
        // alert("test");
        $(".daterangepicker").show();
    }

    InitBackDropClickEvent(): void {
        var backdrop: HTMLElement = document.getElementById("backdrop");

        backdrop.onclick = () => {
            $(this.divView.nativeElement).removeClass('show');
            backdrop.classList.remove("show");
        };
    }

    TUTr_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
    TUTr_InvoiceRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
    TUTr_InvoiceRange_OnChange(changeContext: ChangeContext): void {
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        this.TUTr_InvoiceRangeMinAmount = changeContext.value;
        this.TUTr_InvoiceRangeMaxAmount = changeContext.highValue;
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#region transactions 

    public TUTr_Config: OList;
    public TuTr_Columns =
        {
            Status: true,
            Date: true,
            Account: true,
            AccountName: true,
            Mob: true,
            Biller: true,
            Amount: true,
            Source: true,
            PaymentRef: true
        }
    public TuTr_Filters_List =
        {
            Search: false,
            Date: false,
            Sort: false,
            Status: false,
            Biller: false
        }

    TUTr_Setup() {
        this.TUTr_Config =
        {
            Id: null,
            Task: "gettransactions",
            Location: this._HelperService.AppConfig.NetworkLocation.V3.TransationReport,
            Title: 'Wallet activity',
            SearchBaseCondition: this.TUTrGPointsDebit_Setup_SearchCondition(),
            Type: null,
            ReferenceKey: null,
            ReferenceId: 0,
            SubReferenceId: 0,
            IsDownload: false,
            RefreshCount: true,
            Sort:
            {
                SortDefaultName: null,
                SortDefaultColumn: 'TransactionDate',
                SortName: null,
                SortColumn: null,
                SortOrder: 'desc',
                SortOptions: [],
            },
            TableFields: [
                {
                    DisplayName: 'Reference',
                    SystemName: 'ReferenceId',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Date',
                    SystemName: 'TransactionDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Show: true,
                    Search: true,
                    Sort: true,
                    IsDateSearchField: true,
                },
                {
                    DisplayName: 'Transaction Type',
                    SystemName: 'TypeCategory',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Amount',
                    SystemName: 'Amount',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Balance',
                    SystemName: 'Amount',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
            ]
        }

        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.MerchantSales,
            this.TUTr_Config
        );
        this.TUTr_GetData();
    }

    TUTrGPointsDebit_Setup_SearchCondition() {

        var SearchCondition = "";
        //SearchCondition = this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'transaction.success', '='),
            SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'ParentId', this._HelperService.AppConfig.DataType.Number, this.MerchantData.ReferenceId, '=');
        return SearchCondition;
    }

    TUTr_ToggleOption_Date(event: any, Type: any) {
        this.TUTr_ToggleOption(event, Type);
    }

    TUTr_ToggleOption(event: any, Type: any) {

        // if (Type == 'date') {
        //     event.start = this._HelperService.DateInUTC(event.start);
        //     event.end = this._HelperService.DateInUTC(event.end);
        // }
        if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
            event.data = {
                SalesMin: this.TUTr_InvoiceRangeMinAmount,
                SalesMax: this.TUTr_InvoiceRangeMaxAmount
            }
        }

        this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

        this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
        this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

        this.TodayStartTime = this.TUTr_Config.StartTime;
        this.TodayEndTime = this.TUTr_Config.EndTime;
        if (event != null) {
            for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                const element = this.TUTr_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }

            }
        }
        if (
            (this.TUTr_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.TUTr_GetData();
        }
    }

    timeout = null;
    TUTr_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {
            if (Type == 'date') {
                event.start = this._HelperService.DateInUTC(event.start);
                event.end = this._HelperService.DateInUTC(event.end);
            }

            this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

            this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
            this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

            this.TodayStartTime = this.TUTr_Config.StartTime;
            this.TodayEndTime = this.TUTr_Config.EndTime;
            if (event != null) {
                for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                    const element = this.TUTr_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {
                        element.SystemActive = true;
                    }
                    else {
                        element.SystemActive = false;
                    }

                }
            }


            if (
                (this.TUTr_Config.RefreshData == true)
                && this._HelperService.DataReloadEligibility(Type)
            ) {
                this.TUTr_GetData();
            }
        }, this._HelperService.AppConfig.SearchInputDelay);


    }

    public OverviewData: any = {
        InvoiceAmount: 0.0,
        RewardAmount: 0,
        RedeemAmount: 0,
        Cashouts: 0
    };

    TUTr_GetData() {
        this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.Core.getwallethistoryoverview);
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.TUTr_Config = TConfig;
        console.log("TUTr_Config",this.TUTr_Config);
        
        this._DataHelperService.statusOption(this.TUTr_Config.StatusOptions);
    }

    //   ListAppUsage_GetDetails() {
    //       var pData = {
    //           Task: this._HelperService.AppConfig.Api.Core.GetCashOut,
    //           ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
    //           ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
    //           AccountId: this._HelperService.AppConfig.ActiveAccountId,
    //           AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
    //           // Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this.CurrentRequest_Key, '='),
    //       };
    //       let _OResponse: Observable<OResponse>;
    //       _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Co, pData);
    //       _OResponse.subscribe(
    //           _Response => {
    //               if (_Response.Status == this._HelperService.StatusSuccess) {
    //                   this._CoreUsage = _Response.Result as OCoreUsage;
    //                   this._CoreUsage.StartDate = this._HelperService.GetDateTimeS(this._CoreUsage.StartDate).split(" ")[0];
    //                   this._CoreUsage.CreateDate = this._HelperService.GetDateTimeS(this._CoreUsage.CreateDate);
    //                   this._CoreUsage.EndDate = this._HelperService.GetDateTimeS(this._CoreUsage.EndDate);
    //                   this._CoreUsage.ModifyDate = this._CoreUsage.ModifyDate ? this._HelperService.GetDateTimeS(this._CoreUsage.ModifyDate).split(" ")[0] : null;
    //                   this._CoreUsage.ResponseTime = this._HelperService.GetDateTimeS(this._CoreUsage.ResponseTime);
    //                   this._CoreUsage.StatusB = this._HelperService.GetStatusBadge(this._CoreUsage.StatusCode);
    //                   this.disableAction = this._CoreUsage.StatusName == "Approved" || this._CoreUsage.StatusName == "Rejected"
    //                   this.clicked()
    //               }
    //               else {
    //                   this._HelperService.NotifyError(_Response.Message);
    //               }
    //           },
    //           _Error => {
    //               this._HelperService.HandleException(_Error);
    //           });
    //   }


    //#endregion

    SetOtherFilters(): void {
        this.TUTr_Config.SearchBaseConditions = [];

    }

    SetSalesRanges(): void {
        this.TUTr_InvoiceRangeMinAmount = this.TUTr_Config.SalesRange.SalesMin;
        this.TUTr_InvoiceRangeMaxAmount = this.TUTr_Config.SalesRange.SalesMax;
    }


    TodayStartTime = null;
    TodayEndTime = null;

    public _AccountOverview: OAccountOverview =
        {
            ActiveMerchants: 0,
            ActiveMerchantsDiff: 0,
            ActiveTerminals: 0,
            Idle: 0,
            Dead: 0,
            Active: 0,
            Inactive: 0,
            ActiveTerminalsDiff: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardPurchaseAmountDiff: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardPurchaseAmountDiff: 0,
            Merchants: 0,
            PurchaseAmount: 0,
            PurchaseAmountDiff: 0,
            Terminals: 0,
            Transactions: 0,
            TransactionsDiff: 0,
            UnusedTerminals: 0,
            IdleTerminals: 0,
            TerminalStatus: 0,
            DeadTerminals: 0,
            Total: 0,
            TotalTransactions: 0,
            TotalSale: 0,
        }

    Data: any = {};
    GetAccountOverviewLite(): void {
        this._HelperService.IsFormProcessing = true;
        this.Data = {
            Task: 'getaccountoverview',
            StartTime: this.TodayStartTime,
            EndTime: this.TodayEndTime,
            AccountKey: this._HelperService.UserAccount.AccountKey,
            AccountId: this._HelperService.UserAccount.AccountId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.Data);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._AccountOverview = _Response.Result as OAccountOverview;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();

        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();

    }

    SetSearchRanges(): void {
        //#region Invoice 
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('Amount', this.TUTr_Config.SearchBaseConditions);
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }

        //#endregion      

    }

    //#region SalesOverview 

    GetOverviews(ListOptions: any, Task: string): any {
        this._HelperService.IsFormProcessing = true;

        ListOptions.SearchCondition = '';
        ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }


        var pData = {
            Task: Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            AccountId: ListOptions.AccountId,
            AccountKey: ListOptions.AccountKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.TransationReport, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.OverviewData = _Response.Result as any;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);

            });


    }

    //#endregion

    //#region filterOperations

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        this.SetSalesRanges();
        //#endregion

        this.TUTr_GetData();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_NoSort(Type, index);
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        this.SetSalesRanges();
        //#endregion

        this.TUTr_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            // input: "text",
            html:
                '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
                '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
                '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
            focusConfirm: false,
            preConfirm: () => {
                return {
                    filter: document.getElementById('swal-input1')['value'],
                    private: document.getElementById('swal-input2')['checked']
                }
            },
            // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            // inputAttributes: {
            //   autocapitalize: "off",
            //   autocorrect: "off",
            //   maxLength: "4",
            //   minLength: "4",
            // },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {

                if (result.value.filter.length < 5) {
                    this._HelperService.NotifyError('Enter filter name length greater than 4');
                    return;
                }

                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

                var AccessType: number = result.value.private ? 0 : 1;
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.Payments,
                    AccessType
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._HelperService.Delete_Filter(
                    this._HelperService.AppConfig.FilterTypeOption.Payments
                );
                this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
                this.TUTr_GetData();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        this.TUTr_Config.ActivePage = 1
        if (this.TUTr_InvoiceRangeMinAmount && this.TUTr_InvoiceRangeMaxAmount && this.TUTr_InvoiceRangeMinAmount > this.TUTr_InvoiceRangeMaxAmount) {
            this._HelperService.NotifyError("Minimum Sale Amount should be less than Maximum Sale Amount");
            return;
        }
        if (this.TUTr_InvoiceRangeMinAmount < 0 || this.TUTr_InvoiceRangeMaxAmount < 0) {
            this._HelperService.NotifyError("Sale Amount should not be negative");
            return;
        }
        this.SetSearchRanges();

        this._HelperService.MakeFilterSnapPermanent();
        this.TUTr_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();

        if (ButtonType == 'Sort') {
            $("#TUTr_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#TUTr_fdropdown").dropdown('toggle');
        }
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        this.SetSalesRanges();
        //#endregion
        this.TUTr_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    //#endregion

    public _CoreUsage: OCoreUsage =
        {
            AccountDisplayName: null,
            BankName: null,
            BankAccountNumber: null,
            BankAccountName: null,
            SourceName: null,
            Charge: null,
            TotalAmount: null,
            ReferenceNumber: null,
            CreatedByDisplayName: null,
            ModifyByDisplayName: null,
            SystemComment: null,

            StatusB: null,
            ProductCategoryName: null,
            PaymentReference: null,
            ProductItemName: null,
            UserRewardAmount: null,
            Amount: null,
            RewardAmount: null,
            Coordinates: null,
            CommisonAmount: null,
            StartDate: null,
            EndDate: null,
            ModifyDate: null,
            CreateDate: null,



            AccountMobileNumber: null,
            AccountKey: null, AccountId: null,
            RedeemFromName: null,
            ReferenceKey: null, ReferenceId: null, Reference: null, StatusCode: null, StatusId: null, StatusName: null, UserAccountKey: null, UserAccountIconUrl: null, UserAccountDisplayName: null, ApiKey: null, ApiName: null, AppKey: null, AppName: null, AppOwnerKey: null, AppOwnerName: null, AppVersionKey: null, AppVersionName: null, FeatureKey: null, FeatureName: null, IpAddress: null, Latitude: null, Longitude: null, ProcessingTime: null, Request: null, RequestTime: null, Response: null, ResponseTime: null, SessionId: null, SessionKey: null, UserAccountTypeCode: null, UserAccountTypeName: null,
        }
}

export class OAccountOverview {
    public TotalTransactions?: number;
    public TotalSale?: number;
    public DeadTerminals?: number;
    public IdleTerminals?: number;
    public TerminalStatus?: number;
    public Total?: number;
    public Idle?: number;
    public Active?: number;
    public Dead?: number;
    public Inactive?: number;
    public UnusedTerminals?: number;
    public Merchants: number;
    public ActiveMerchants: number;
    public ActiveMerchantsDiff: number;
    public Terminals: number;
    public ActiveTerminals: number;
    public ActiveTerminalsDiff: number;
    public Transactions: number;
    public TransactionsDiff: number;
    public PurchaseAmount: number;
    public PurchaseAmountDiff: number;
    public CashRewardPurchaseAmount: number;
    public CashRewardPurchaseAmountDiff: number;
    public CardRewardPurchaseAmount: number;
    public CardRewardPurchaseAmountDiff: number;
}
export class OCoreUsage {

    public UserRewardAmount: number;
    public BankAccountNumber: number;
    public Charge: number;
    public TotalAmount: number;
    public AccountDisplayName: string;
    public BankName: string;
    public ReferenceNumber: string;
    public CreatedByDisplayName: string;
    public BankAccountName: string;
    public ModifyByDisplayName: string;
    public SystemComment: string;
    public ProductCategoryName: string;
    public ProductItemName: string;
    public RewardAmount: number;
    public Amount: number;
    public CommisonAmount: number;
    public AccountMobileNumber: number;
    public PaymentReference: string;
    public StatusB: string;
    Coordinates?: any;
    public ReferenceId: number;
    public AccountId: number;
    public AccountKey: string;
    public Reference: string;
    public ReferenceKey: string;
    public AppOwnerKey: string;
    public AppOwnerName: string;
    public AppKey: string;
    public AppName: string;
    public ApiKey: string;
    public ApiName: string;
    public AppVersionKey: string;
    public AppVersionName: string;
    public FeatureKey: string;
    public FeatureName: string;
    public UserAccountKey: string;
    public UserAccountDisplayName: string;
    public UserAccountIconUrl: string;
    public UserAccountTypeCode: string;
    public UserAccountTypeName: string;
    public SessionId: string;
    public SessionKey: string;
    public IpAddress: string;
    public Latitude: string;
    public Longitude: string;
    public Request: string;
    public Response: string;
    public RequestTime: Date;
    public StartDate: Date;
    public ModifyDate: Date;
    public EndDate: Date;
    public CreateDate: Date;
    public ResponseTime: Date;
    public ProcessingTime: string;
    public StatusId: number;
    public StatusCode: string;
    public StatusName: string;
    public SourceName: any;
    public RedeemFromName: any
}

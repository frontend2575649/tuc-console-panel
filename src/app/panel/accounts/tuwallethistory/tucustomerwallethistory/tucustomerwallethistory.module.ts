import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TucustomerwallethistoryComponent } from './tucustomerwallethistory.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { Ng5SliderModule } from 'ng5-slider';
import { MerchantguardGuard } from 'src/app/service/guard/merchantguard.guard';
const routes: Routes = [
    { path: '',canActivate:[MerchantguardGuard] ,component: TucustomerwallethistoryComponent },
    { path: "details/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.Dashboard" }, loadChildren: "../customertrasactions/customertrasactions.module#CustomertrasactionsModule" },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUCustomerWalletHistoryRoutingModule { }

@NgModule({
  declarations: [TucustomerwallethistoryComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    Ng5SliderModule,
    TUCustomerWalletHistoryRoutingModule,
  ]
})
export class TucustomerwallethistoryModule { }

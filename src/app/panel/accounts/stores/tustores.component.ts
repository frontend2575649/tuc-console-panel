import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;
declare var moment:any;
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../service/service";
import { Observable, Subscription } from 'rxjs';
import { HCoreXAddress, HCXAddressConfig, locationType } from '../../../component/hcxaddressmanager/hcxaddressmanager.component';
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { Placeholder } from "@angular/compiler/src/i18n/i18n_ast";
import { Select2OptionData } from "ng2-select2";


@Component({
  selector: "tu-tustores",
  templateUrl: "./tustores.component.html",
})
export class TUStoresComponent implements OnInit, OnDestroy {
  public SaveAccountRequest: any;
  public ResetFilterControls: boolean = true;
  public ShowCategorySelector: boolean = true;
  public _ObjectSubscription: Subscription = null;
  isData: boolean = true;
  ShowManager = false;
  public StoreManager_Data: Array<Select2OptionData>;
  public S2StoreManager_Option: Select2Options;
  public mapheight = '287px'
  
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = true;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;

  }


  ngOnInit() {
    window.addEventListener('scroll', this.scroll, true);
    this._HelperService.StopClickPropogation();
    Feather.replace();
    this._HelperService.ValidateData();
    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];

      this.StoresList_Setup();
      this.GetStateCategories();
      this.StoresList_Filter_Owners_Load();
      this.InitColConfig();
      this.Form_AddStore_Load();
      this.TUTr_Filter_Merchant_Load();
      this.GetBusinessCategories();
    });

    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.StoresList_GetData();
    // });
    // this._HelperService.StopClickPropogation();
  }


  scroll = (event): void => {
    $(".daterangepicker").hide();
    // if (!this.hideDatePicker) {
    //   this.isDateRangepicker = false;
    //   setTimeout(() => {
    //     this.isDateRangepicker = true;
    //   }, 1)
    // }
    $(".form-daterangepicker").blur();
  };
  // isDateRangepicker: boolean = true
  openDatepicker(event) {

    $(".daterangepicker").show();
  }

  public ToggleMerchantSelect: boolean = false;
  public TUTr_Filter_Merchant_Option: Select2Options;
  public merchantId: number = null;
  public StoreTogglefield: boolean = false;
  TUTr_Filter_Merchant_Load() {

    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },

      ]
    };

    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Merchant_Option = {
      placeholder: 'Select Merchant',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Merchant_Change(event: any) {

    this.Form_AddStore.patchValue(
      {
        AccountId: event.data[0].ReferenceId,
        AccountKey: event.data[0].ReferenceKey,
        MerchantName: event.data[0].DisplayName,
      }


    );
    this.GetStoreManagerList(event.data[0].ReferenceId,event.data[0].ReferenceKey)



    // if(this.merchantId  != null){
    //   this.TUTr_Filter_Stores_Load();
    //   this.ToggleStoreSelect = true;
    //   this._ChangeDetectorRef.detectChanges();
    // }

  }

  public SelectedStoreManager = ''
  GetStoreManagerList(accId, accKey) {
    this.ShowManager = false;
    var PData =
    {
        Task: 'getstoremanagers',
        AccountId: accId,
        AccountKey: accKey,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, PData);
    _OResponse.subscribe(
        _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
                if (_Response.Result.Data != undefined) {
                    var _Data = _Response.Result.Data as any[];
                    if (_Data.length > 0) {
                        var finalCat = [];
                        this.ShowManager = false;
                        _Data.forEach(element => {
                            var Item = {
                                id: element.ReferenceId,
                                key: element.ReferenceKey,
                                text: element.DisplayName,
                                additional: element,
                            };
                            finalCat.push(Item);
                        });
                        this.S2StoreManager_Option = {
                          placeholder: "Select Store Manager",
                          multiple: false,
                          allowClear: false,
                      };
                        this.StoreManager_Data = finalCat;
                        this.ShowManager = true;
                      
                    }
                    else {
                        this.S2StoreManager_Option = {
                            placeholder: "Select Store Manager",
                            multiple: false,
                            allowClear: false,
                        };
                        setTimeout(() => {
                            this.ShowManager = true;
                        }, 500);
                    }
                }
                else {
                    this.S2StoreManager_Option = {
                        placeholder: "Select Store Manager",
                        multiple: false,
                        allowClear: false,
                    };
                    setTimeout(() => {
                        this.ShowManager = true;
                    }, 500);
                }
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;

        });
  }

  StoreManagerSelected(Items) {
    this.Form_AddStore.controls["StoreManagerId"].patchValue(Items.data[0].additional.ReferenceId)
    this.Form_AddStore.controls["MEmailAddress"].patchValue(Items.data[0].additional.EmailAddress)
    this.Form_AddStore.controls["MobileNumber"].patchValue(Items.data[0].additional.ContactNumber)
}

  ngOnDestroy(): void {
    window.removeEventListener('scroll', this.scroll, true);
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {
    }
  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion
  public ListType: number;
  StoreList_ListTypeChange(Type) {
    this.ListType = Type;
    this.StoresList_Setup();
  }
  //#region Storelist

  public StoresList_Config: OList;
  StoresList_Setup() {
    this.StoresList_Config = {
      Id: null,
      // Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      Title: "Available Stores",
      StatusType: "default",
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      // AccountKey: this._HelperService.UserAccount.AccountKey,
      // AccountId: this._HelperService.UserAccount.AccountId,
      DefaultSortExpression: "CreateDate desc",
      IsDownload: true,
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'CreateDate',
        SortName: null,
        SortColumn: '',
        SortOrder: 'desc',
        SortOptions: [],
      },
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: " Store Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Contact No",
          SystemName: "ContactNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Total POS",
          SystemName: "TotalTerminals",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Active POS",
          SystemName: "ActiveTerminals",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Cashiers",
          SystemName: "Cashiers",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        }, {
          DisplayName: "RM",
          SystemName: "RmDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };


    this.StoresList_Config = this._DataHelperService.List_Initialize(
      this.StoresList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Stores,
      this.StoresList_Config
    );

    this.StoresList_GetData();
  }
  // hideDatePicker: boolean = false;
  StoresList_ToggleOption(event: any, Type: any) {

    // if (Type == "date") {
    //   this.hideDatePicker = true;
    // } else {
    //   this.hideDatePicker = false;
    // }
    if (Type == "date") {
      this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
      this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
  }
    if (event != null) {
      for (let index = 0; index < this.StoresList_Config.Sort.SortOptions.length; index++) {
        const element = this.StoresList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.StoresList_Config
    );

    this.StoresList_Config = this._DataHelperService.List_Operations(
      this.StoresList_Config,
      event,
      Type
    );

    if (
      (this.StoresList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.StoresList_GetData();
    }

  }

  timeout = null;
  StoresList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.StoresList_Config.Sort.SortOptions.length; index++) {
          const element = this.StoresList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.StoresList_Config
      );

      this.StoresList_Config = this._DataHelperService.List_Operations(
        this.StoresList_Config,
        event,
        Type
      );

      if (
        (this.StoresList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.StoresList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);
  }

  StoresList_GetData() {

    this.StoresList_Config.ListType = this.ListType;
    this.StoresList_Config.SearchBaseCondition = "";

    if (this.StoresList_Config.ListType == 1) // Active
    {
      this.StoresList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.StoresList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.active', "=");
    }
    else if (this.StoresList_Config.ListType == 2) // Suspend
    {
      this.StoresList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.StoresList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.suspended', "=");
    }
    else if (this.StoresList_Config.ListType == 3) // Blocked
    {
      this.StoresList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.StoresList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.blocked', "=");
    }

    else if (this.StoresList_Config.ListType == 4) {  // InActive
      this.StoresList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.StoresList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.inactive', "=");
    }
    else if (this.StoresList_Config.ListType == 5) {  // All
      this.StoresList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrictFromArray(this.StoresList_Config.SearchBaseCondition, "StatusCode", 'number', [], "=");
    }
    else {
      this.StoresList_Config.DefaultSortExpression = 'CreateDate desc';
    }

    this.GetOverviews(this.StoresList_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.Getstoreoverview);

    var TConfig = this._DataHelperService.List_GetData(
      this.StoresList_Config
    );
    this.StoresList_Config = TConfig;
    this._DataHelperService.statusOption(this.StoresList_Config.StatusOptions);

  }

  StoresList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveStore,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        MerchantId: ReferenceData.MerchantReferenceId,
        MerchantKey: ReferenceData.MerchantReferenceKey,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
      }
    );

    this._HelperService.AppConfig.ActiveStoreReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveStoreReferenceId = ReferenceData.ReferenceId;
    this._HelperService.AppConfig.ActiveMerchantReferenceKey =
      ReferenceData.MerchantReferenceKey;
    this._HelperService.AppConfig.ActiveMerchantReferenceId = ReferenceData.MerchantReferenceId;
    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Store
    //     .Dashboard,
    //   ReferenceData.ReferenceKey,
    //   ReferenceData.ReferenceId,
    // ]);

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Store.SalesHistory,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
      ReferenceData.MerchantReferenceKey,
      ReferenceData.MerchantReferenceId
    ]);


  }

  //#endregion


  //#region OwnerFilter

  public StoresList_Filter_Owners_Option: Select2Options;
  public StoresList_Filter_Owners_Selected = null;
  StoresList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
      // ReferenceKey: this._HelperService.UserAccount.AccountKey,
      // ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant,
    //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     this._HelperService.AppConfig.AccountType.PosAccount
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.StoresList_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  StoresList_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.StoresList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.StoresList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "MerchantReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.StoresList_Filter_Owners_Selected,
        "="
      );
      this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.StoresList_Config.SearchBaseConditions
      );
      this.StoresList_Filter_Owners_Selected = null;
    } else if (event.value != this.StoresList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "MerchantReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.StoresList_Filter_Owners_Selected,
        "="
      );
      this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.StoresList_Config.SearchBaseConditions
      );
      this.StoresList_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.StoresList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "MerchantReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.StoresList_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.StoresList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.StoresList_Config.SearchBaseConditions = [];
    this.StoresList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.StoresList_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.StoresList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);
    if (Type == 'Time') {
      this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
      this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
  }

    this.SetOtherFilters();

    this.StoresList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      // inputAttributes: {
      //   autocapitalize: "off",
      //   autocorrect: "off",
      //   maxLength: "4",
      //   minLength: "4",
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Stores,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Stores
        );
        this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);
        this.StoresList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.StoresList_GetData();

    if (ButtonType == 'Sort') {
      $("#StoresList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#StoresList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);
    this.SetOtherFilters();

    this.StoresList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.StoresList_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }
  _CurrentAddress: any = {};
  Form_AddStore_Address: string = null;
  Form_AddStore_Latitude: number = 0;
  Form_AddStore_Longitude: number = 0;
  @ViewChild('places') places: GooglePlaceDirective;
  Form_AddStore_PlaceMarkerClick(event) {
    this.Form_AddStore_Latitude = event.coords.lat;
    this.Form_AddStore_Longitude = event.coords.lng;
  }
  public Form_AddStore_AddressChange(address: Address) {

    this.Form_AddStore_Latitude = address.geometry.location.lat();
    this.Form_AddStore_Longitude = address.geometry.location.lng();
    this.Form_AddStore_Address = address.formatted_address;

    this.Form_AddStore.controls['Latitude'].setValue(this.Form_AddStore_Latitude);
    this.Form_AddStore.controls['Longitude'].setValue(this.Form_AddStore_Longitude);
    this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
    if (this._CurrentAddress.country != this._HelperService.UserCountrycode) {
      this._HelperService.NotifyError('Currently we’re not serving in this area, please add locality within ' + this._HelperService.UserCountrycode);
      this.reset();

    }
    else {
      // this.Form_AddStore.controls['Address'].setValue(address.formatted_address);
      this.Form_AddStore.controls['MapAddress'].setValue(address.formatted_address);
    }

  }
  reset() {
    //this.Form_AddStore.controls['Address'].reset()
    this.Form_AddStore.controls['MapAddress'].reset()
    this.Form_AddStore.controls['CityName'].reset()
    this.Form_AddStore.controls['StateName'].reset()
    this.Form_AddStore.controls['CountryName'].reset()

  }

  _ShowGeneralPreview() {
    // if (this.statename == null) {
    // this._HelperService.NotifyError('Select State');
    // }

    // else {
    this._HelperService.CloseModal("Form_AddStore_Content");
    this._HelperService.OpenModal('_PreviewStore');
    // }
  }

  Form_AddStore: FormGroup;
  Form_AddStore_Show() {
    this._HelperService.OpenModal("Form_AddStore_Content");
  }
  Form_AddStore_Close() {
    // this._HelperService.CloseModal("Form_AddStore_Content");
    this._HelperService.CloseModal('_PreviewStore');
    this._HelperService.CloseAllModal();

  }
  Form_AddStore_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddStore = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.savestore,
      AccountId: [null, Validators.required],
      AccountKey: [null, Validators.required],
      DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      StoreManagerId: [null],
      // FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128), Validators.pattern("^[a-zA-Z _-]*$")])],
      // LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128), Validators.pattern("^[a-zA-Z _-]*$")])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      MobileNumber: [null],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      MEmailAddress: [null],
      StatusCode: this._HelperService.AppConfig.Status.Active,
      MerchantName: [null],
    });
  }
  Form_AddStore_Clear() {
    this.SelectedBusinessCategories = [];
    this.Form_AddStore.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddStore_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
    this._Address = {};

  }






  public _Address: HCoreXAddress = {};
  public _AddressConfig: HCXAddressConfig =
    {
      locationType: locationType.form
    }
  AddressChange(Address) {
    this._Address = Address;
  }



  ShowMerchantSelection = true;
  Form_AddStore_Process(_FormValue?: any) {
    this.isData = false;
    if (!this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "") {
      this._HelperService.NotifyError('Please select business location');
    }
    else {
      var _FormValue = this.Form_AddStore.value;
      if (this.SelectedBusinessCategories != undefined && this.SelectedBusinessCategories.length > 0) {
        _FormValue.Categories = []
        this.SelectedBusinessCategories.forEach(element => {
          _FormValue.Categories.push(
            {
              ReferenceId: element
            }
          )
        });
      }

      this._HelperService.IsFormProcessing = true;
      this.SaveAccountRequest = this.ReFormat_RequestBody();
      let _OResponse: Observable<OResponse>;
      var Request = this.CreateStoreRequest(_FormValue);
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, Request);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.FlashSwalSuccess("Store Added successfully", "Done! New Store Has Been Added");
            this._HelperService.ObjectCreated.next(true);
            this.StoresList_GetData();
            this.Form_AddStore_Clear();
            this.Form_AddStore_Close();
            this.ShowMerchantSelection = false;
            this.ShowCategorySelector = false;
            setTimeout(() => {
              this.isData = true;
              this.ShowMerchantSelection = true;
              this.ShowCategorySelector = true;
            }, 3000);
            if (_FormValue.OperationType == "close") {
              this.Form_AddStore_Close();
            }
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );

    }

  }



  public BusinessCategories = [];
  public S2BusinessCategories = [];
  GetBusinessCategories() {

    this._HelperService.ToggleField = true;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getcategories,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      SortExpression: 'Name asc',
      Offset: 0,
      Limit: 1000,
    }
    PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
      PData.SearchCondition,
      "TypeCode",
      this._HelperService.AppConfig.DataType.Text,
      this._HelperService.AppConfig.HelperTypes.MerchantCategories,
      "="
    );
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Op, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            this.BusinessCategories = _Response.Result.Data;

            this.ShowCategorySelector = false;
            this._ChangeDetectorRef.detectChanges();
            for (let index = 0; index < this.BusinessCategories.length; index++) {
              const element = this.BusinessCategories[index];
              this.S2BusinessCategories.push(
                {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name
                }
              );
            }

            this.ShowCategorySelector = true;
            this._ChangeDetectorRef.detectChanges();

            this._HelperService.ToggleField = false;

          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });
  }
  public SelectedBusinessCategories = [];
  CategoriesSelected(Items) {
    if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
      this.SelectedBusinessCategories = Items.value;
    }
    else {
      this.SelectedBusinessCategories = [];
    }


  }
  SaveMerchantBusinessCategory(item) {
    if (item != '0') {
      var Setup =
      {
        Task: this._HelperService.AppConfig.Api.Core.SaveUserParameter,
        TypeCode: this._HelperService.AppConfig.HelperTypes.MerchantCategories,
        // UserAccountKey: this._UserAccount.ReferenceKey,
        CommonKey: item,
        StatusCode: 'default.active'
      };
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, Setup);
      _OResponse.subscribe(
        _Response => {
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess('Business category assigned to merchant');
            // this.BusinessCategories = [];
            // this.GetMerchantBusinessCategories();
          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }
  }


  public citykey: any; public cityid: any; cityname: any;
  public cityareaid: any;
  public cityareacode: any;
  public cityareaname: any;
  // public countryname: any;
  // public countryid: any;
  // public countrycode: any;

  CreateStoreRequest(_FormValue: any): void {
    _FormValue.ContactPerson = {
      // FirstName: _FormValue.FirstName,
      // LastName: _FormValue.LastName,
      MobileNumber: _FormValue.MobileNumber,
      EmailAddress: _FormValue.MEmailAddress
    }
    // //Location Manager - Start
    _FormValue.Address = this._Address.Address;
    _FormValue.AddressComponent = this._Address;
    // //Location Manager - End

    _FormValue.FirstName = undefined;
    _FormValue.LastName = undefined;
    _FormValue.MobileNumber = undefined;
    // _FormValue.EmailAddress = undefined;

    _FormValue.Latitude = undefined;
    _FormValue.Longitude = undefined;
    _FormValue.MapAddress = undefined;

    return _FormValue;
  }


  //#region SalesOverview 
  public OverviewData = { Total: 0, New: 0, Suspended: 0, Blocked: 0, Active: 0 };
  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetData(ListOptions);
    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: SortExpression,
      Type: ListOptions.Type,
      ReferenceKey: ListOptions.ReferenceKey,

      ReferenceId: ListOptions.ReferenceId,
      SubReferenceId: ListOptions.SubReferenceId,

      IsDownload: false,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.accounts, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.OverviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });


  }
  //#endregion


  //state
  public StateCategories = [];
  public S2StateCategories = [];
  public stateerrro: boolean = false;
  public ShowstateSelector: boolean = true;
  public ShowcitySelector: boolean = true;
  GetStateCategories() {
    if (this.SelectedCityCategories != undefined && this.SelectedCityCategories.length > 0) {
      this.S2CityCategories = [];
    }
    this._HelperService.ToggleField = true;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getstates,
      ReferenceKey: this._HelperService.UserCountrykey,
      ReferenceId: this._HelperService.UserCountryId,
      //SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      // SortExpression: 'Name asc',
      Offset: 0,
      Limit: 1000,
    }
    // PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
    //     PData.SearchCondition,
    //     "TypeCode",
    //     this._HelperService.AppConfig.DataType.Text,
    //     this._HelperService.AppConfig.HelperTypes.MerchantCategories,
    //     "="
    // );
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.State, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            this.StateCategories = _Response.Result.Data;

            this.ShowstateSelector = false;
            this._ChangeDetectorRef.detectChanges();
            this.S2StateCategories.push(
              {
                id: 0,
                key: "0",
                text: "Select State"
              }
            );


            for (let index = 0; index < this.StateCategories.length; index++) {

              const element = this.StateCategories[index];
              this.S2StateCategories.push(
                {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name
                }
              );
            }
            this.ShowstateSelector = true;
            this._ChangeDetectorRef.detectChanges();

            this._HelperService.ToggleField = false;

          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });
  }
  public SelectedStateCategories = [];
  public selectedstate: boolean = false;
  statekey: any; stateid: any; statename: any;
  StateSelected(Items) {
    if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
      this.SelectedStateCategories = Items.value;
      this.statekey = Items.data[0].key;
      this.stateid = Items.data[0].id;
      this.statename = Items.data[0].text;
      this.selectedstate = true;
      this.GetCityCategories()
      // this.Form_AddStore.controls['StateCode'].patchValue(this.statekey);
      // this.Form_AddStore.controls['StateId'].patchValue(this.stateid);
      // this.Form_AddStore.controls['StateName'].patchValue(this.statename);
    }
    else {
      this.SelectedStateCategories = [];
    }
  }


  //City
  public CityCategories = [];
  public S2CityCategories = [];

  GetCityCategories() {
    this._HelperService.ToggleField = true;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getcities,
      ReferenceKey: this.statekey,
      ReferenceId: this.stateid,
      //SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      // SortExpression: 'Name asc',
      Offset: 0,
      Limit: 1000,
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.City, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            this.CityCategories = _Response.Result.Data;
            this.ShowcitySelector = false;
            this.S2CityCategories = [];
            this._ChangeDetectorRef.detectChanges();
            for (let index = 0; index < this.CityCategories.length; index++) {
              const element = this.CityCategories[index];
              this.S2CityCategories.push(
                {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name
                }
              );
            }
            this.ShowcitySelector = true;
            this._ChangeDetectorRef.detectChanges();

            this._HelperService.ToggleField = false;

          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });



  }
  public SelectedCityCategories = [];

  // public citykey: any; public cityid: any; cityname: any;
  // public cityareaid: any;
  // public cityareacode: any;
  // public cityareaname: any;

  CitySelected(Items) {
    if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
      this.SelectedCityCategories = Items.value;
      this.citykey = Items.data[0].key;
      this.cityid = Items.data[0].id
      this.cityname = Items.data[0].text
      // this.Form_AddStore.controls['CityCode'].patchValue(this.citykey);
      // this.Form_AddStore.controls['CityId'].patchValue(this.cityid);
      // this.Form_AddStore.controls['CityName'].patchValue(this.cityname);
    }
    else {
      this.SelectedCityCategories = [];
    }
  }




  ReFormat_RequestBody(): void {
    var formValue: any = this.Form_AddStore.value;
    var formRequest: any = {
      OperationType: 'new',
      Task: formValue.Task,
      DisplayName: formValue.DisplayName,
      Name: formValue.Name,
      ContactNumber: formValue.ContactNumber,
      EmailAddress: formValue.EmailAddress,
      RewardPercentage: formValue.RewardPercentage,
      WebsiteUrl: formValue.WebsiteUrl,
      ReferralCode: formValue.ReferralCode,
      Description: formValue.Description,
      UserName: formValue.UserName,
      Password: formValue.Password,
      StatusCode: formValue.StatusCode,
      ContactPerson: {
        FirstName: formValue.FirstName,
        LastName: formValue.LastName,
        MobileNumber: formValue.MobileNumber,
        EmailAddress: formValue.EmailAddress
      },
      IconContent: formValue.IconContent
    };
    return formRequest;

  }



}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TunotaccessComponent } from './tunotaccess.component';

describe('TunotaccessComponent', () => {
  let component: TunotaccessComponent;
  let fixture: ComponentFixture<TunotaccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TunotaccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TunotaccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ChangeContext } from 'ng5-slider';
import swal from 'sweetalert2';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
declare var moment: any;
declare var $: any;
import { Observable, Subscription } from 'rxjs';


@Component({
    selector: 'tu-turef',
    templateUrl: './turef.component.html',
})
export class TURefComponent implements OnInit {

    public ResetFilterControls: boolean = true;
    public _ObjectSubscription: Subscription = null;

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _FilterHelperService: FilterHelperService
    ) {
    }

    ngOnInit() {
        this._HelperService.StopClickPropogation();
        Feather.replace();
        this.RefList_Setup();
    }


    ngOnDestroy(): void {
        try { this._ObjectSubscription.unsubscribe(); } catch (error) { }
    }

    GoToRefBonus() {
        this._Router.navigate(['console' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AccountSettings.Refbonus]);

    }
    public RefList_Config: OList;
    RefList_Setup() {
        this.RefList_Config = {
            Id: null,
            Task: 'getreferrals',
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Referals,
            Title: "Available referral",
            StatusType: "default",
            DefaultSortExpression: "",
            Sort:
            {
                SortDefaultName: null,
                SortDefaultColumn: 'ReferenceId',
                SortName: null,
                SortColumn: '',
                SortOrder: 'desc',
                SortOptions: [],
            },
            TableFields: [
                {
                    DisplayName: "User Name",
                    SystemName: "DisplayName",
                    DataType: this._HelperService.AppConfig.DataType.String,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Referred By",
                    SystemName: "OwnerDisplayName",
                    DataType: this._HelperService.AppConfig.DataType.String,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: 'Date Used',
                    SystemName: "ReferralUsedDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date text-right",
                    Show: true,
                    IsDateSearchField: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
                {
                    DisplayName: "Amount",
                    SystemName: "ReferralAmount",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
            ]
        };

        this.RefList_Config.SearchBaseCondition = "";
        this.RefList_Config = this._DataHelperService.List_Initialize(this.RefList_Config);
        this._HelperService.Active_FilterInit(this._HelperService.AppConfig.FilterTypeOption.Stores, this.RefList_Config);
        this.RefList_GetData();
    }

    TodayStartTime = null;
    TodayEndTime = null;
    
    RefList_ToggleOption(event: any, Type: any) {

        if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
            event.data = {
                SalesMin: this.TURef_ReferralRangeMinAmount,
                SalesMax: this.TURef_ReferralRangeMaxAmount
            }
        }
        if (event != null) {
            for (let index = 0; index < this.RefList_Config.Sort.SortOptions.length; index++) {
                const element = this.RefList_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }
            }
        }
        if (Type == "date") {
            this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
            this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
        }
        this._HelperService.Update_CurrentFilterSnap(event, Type, this.RefList_Config);
        this.RefList_Config = this._DataHelperService.List_Operations(this.RefList_Config, event, Type);
        this.TodayStartTime = this.RefList_Config.StartTime;
        this.TodayEndTime = this.RefList_Config.EndTime;
        if (event != null) {
            for (let index = 0; index < this.RefList_Config.Sort.SortOptions.length; index++) {
                const element = this.RefList_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }

            }
        }


        if ((this.RefList_Config.RefreshData == true) && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.RefList_GetData();
        }

    }

    timeout = null;

    RefList_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {
            if (event != null) {
                for (let index = 0; index < this.RefList_Config.Sort.SortOptions.length; index++) {
                    const element = this.RefList_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {
                        element.SystemActive = true;
                    }
                    else {
                        element.SystemActive = false;
                    }
                }
            }

            this._HelperService.Update_CurrentFilterSnap(event, Type, this.RefList_Config);
            this.RefList_Config = this._DataHelperService.List_Operations(this.RefList_Config, event, Type);
            if ((this.RefList_Config.RefreshData == true) && this._HelperService.DataReloadEligibility(Type)) {
                this.RefList_GetData();
            }
        }, this._HelperService.AppConfig.SearchInputDelay);
    }

    TURef_ReferralRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
    TURef_ReferralRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;

    SetSalesRanges(): void {
        this.TURef_ReferralRangeMinAmount = this.RefList_Config.SalesRange.SalesMin;
        this.TURef_ReferralRangeMaxAmount = this.RefList_Config.SalesRange.SalesMax;
    }

    SetSearchRanges(): void {
        this.RefList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('ReferralAmount', this.RefList_Config.SearchBaseConditions);
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'ReferralAmount', this.TURef_ReferralRangeMinAmount, this.TURef_ReferralRangeMaxAmount);
        if (this.TURef_ReferralRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TURef_ReferralRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.RefList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.RefList_Config.SearchBaseConditions);
        }
        else {
            this.RefList_Config.SearchBaseConditions.push(SearchCase);
        }
    }

    RefList_GetData() {
        this.GetOverviews(this.RefList_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.Getreferralsoverview);
        var TConfig = this._DataHelperService.List_GetOverviewRef(this.RefList_Config);
        this.RefList_Config = TConfig;
    }

    SetOtherFilters(): void {
        this.RefList_Config.SearchBaseConditions = [];
        this.RefList_Config.SearchBaseCondition = null;
        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
        if (CurrentIndex != -1) {
            this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetMerchantConfig(this.RefList_Config);
        this.SetOtherFilters();
        this.RefList_GetData();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_DealMerchant(Type, index);
        this._FilterHelperService.SetMerchantConfig(this.RefList_Config);
        if (Type == 'Time') {
            this._HelperService.AppConfig.DateRangeOptions.startDate = new Date(2017, 0, 1, 0, 0, 0, 0);
            this._HelperService.AppConfig.DateRangeOptions.endDate = moment().endOf("day");
        }
        this.SetOtherFilters();
        this.RefList_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            html:
                '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
                '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
                '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
            focusConfirm: false,
            preConfirm: () => {
                return {
                    filter: document.getElementById('swal-input1')['value'],
                    private: document.getElementById('swal-input2')['checked']
                }
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                if (result.value.filter.length < 5) {
                    this._HelperService.NotifyError('Enter filter name length greater than 4');
                    return;
                }
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();
                this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);
                var AccessType: number = result.value.private ? 0 : 1;
                this._HelperService.Save_NewFilter(this._HelperService.AppConfig.FilterTypeOption.Stores, AccessType);
                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();
                this._HelperService.Delete_Filter(this._HelperService.AppConfig.FilterTypeOption.Stores);
                this._FilterHelperService.SetMerchantConfig(this.RefList_Config);
                this.RefList_GetData();
                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        if (this.TURef_ReferralRangeMaxAmount == null || this.TURef_ReferralRangeMaxAmount == undefined ||
            this.TURef_ReferralRangeMinAmount == null || this.TURef_ReferralRangeMinAmount == undefined) {
            this._HelperService.NotifyError("Value not be null or undefined");
        }
        else if (this.TURef_ReferralRangeMinAmount > this.TURef_ReferralRangeMaxAmount) {
            this._HelperService.NotifyError("Minimum Amount should be less than Maximum  Amount");
        }
        else {
            this.SetSearchRanges();
            this._HelperService.MakeFilterSnapPermanent();
            this.RefList_GetData();
            this.ResetFilterUI(); this._HelperService.StopClickPropogation();
            if (ButtonType == 'Sort') {
                $("#RefList_sdropdown").dropdown('toggle');
            } else if (ButtonType == 'Other') {
                $("#RefList_fdropdown").dropdown('toggle');
            }
        }
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetMerchantConfig(this.RefList_Config);
        this.SetOtherFilters();
        this.SetSalesRanges();
        this.RefList_GetData();
        this.ResetFilterUI();
        this._HelperService.StopClickPropogation();
    }

    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();
        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();
    }

    public OverviewData = { TotalReferralAmount: 0, TotalReferrals: 0};
    GetOverviews(ListOptions: any, Task: string): any {
        this._HelperService.IsFormProcessing = true;
        ListOptions.SearchCondition = '';
        ListOptions = this._DataHelperService.List_GetData(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;
        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }
        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }

        var pData = {
            Task: Task,
            TotalRecords: ListOptions.TotalRecords,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Referals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.OverviewData = _Response.Result as any;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    OwnerEventProcessing(event: any): void {
        this.RefList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }


}
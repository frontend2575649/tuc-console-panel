import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { MainPipe } from '../../../service/main-pipe.module'
import { ImageCropperModule } from 'ngx-image-cropper';

import { TUCashiersComponent } from "./tucashiers.component";
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { MerchantguardGuard } from "src/app/service/guard/merchantguard.guard";

const routes: Routes = [{ path: "",canActivate:[MerchantguardGuard], component: TUCashiersComponent }];

const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 1,
};

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUCashiersRoutingModule {}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TUCashiersRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    ImageCropperModule,
    MainPipe,
    InputFileModule.forRoot(config),
  ],
  declarations: [TUCashiersComponent]
})
export class TUCashiersModule {}

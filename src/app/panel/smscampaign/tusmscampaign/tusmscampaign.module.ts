import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { TUSMSCampaignComponent } from "./tusmscampaign.component";
import { Angular4PaystackModule } from 'angular4-paystack'
import { DynamicRoutesguardGuard } from "src/app/service/guard/dynamicroutes.guard";

const routes: Routes = [
    {
        path: "",
        component: TUSMSCampaignComponent,
        // canActivate:[DynamicRoutesguardGuard],
        // data:{accessName:['campaignsview']},
        children: [
           
            { path: "analytics/:accountkey/:accountid/:referencekey/:referenceid", data: { permission: "customer", menuoperations: "ManageMerchant", PageName: 'System.Menu.Campaign', accounttypecode: "customer" }, loadChildren: "../tusmscampaign/smsoverview/smsoverview.module#TUOverviewModule" },
            { path: "customerscampaign/:accountkey/:accountid/:referencekey/:referenceid", data: { permission: "customer", menuoperations: "ManageMerchant", PageName: 'System.Menu.Campaign', accounttypecode: "customer" }, loadChildren: "./../tusmscampaign/allcutomers/allcutomers.module#TUAllCustomersModule" },
            
       
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUSmscampaignRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUSmscampaignRoutingModule,
        GooglePlaceModule,
        Angular4PaystackModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    declarations: [TUSMSCampaignComponent]
})
export class TUSmscampaignDModule { }

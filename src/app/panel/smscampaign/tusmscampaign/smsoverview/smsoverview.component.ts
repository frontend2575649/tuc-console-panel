import { ChangeDetectorRef, Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BaseChartDirective, Label, Color } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OList, OResponse, OSelect, OSalesTrend, OSalesTrendData, OSalesTrendDataHourly } from '../../../../service/service';
declare var moment: any;
declare var $: any;
declare var d3: any;
import * as cloneDeep from 'lodash/cloneDeep';
import * as lodash from 'lodash';
import * as pluginEmptyOverlay from "chartjs-plugin-empty-overlay";
import * as Feather from "feather-icons";
import { ChartDataSets } from 'chart.js';
import { timeInterval } from 'rxjs/operators';
import { MerchantguardGuard } from 'src/app/service/guard/merchantguard.guard';
declare var introJs: any;
declare const window: any;

import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet } from 'ng2-charts';


@Component({
  selector: "tu-smsoverview",
  templateUrl: "./smsoverview.component.html",
})



export class TUSmsOverviewcomonent implements OnInit {

  //chart code start---
  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,

    legend: {
      display: false
    },
    tooltips: {
      enabled: false
    },
    plugins: {
      datalabels: {
        display: false
      }
    },


  };
  public pieChartLabels: Label[] = [['Total SMS'], ['Delivered SMS'], ['Failed SMS'], ['Pending SMS']];
  public pieChartData = [1000, 800, 200, 100];
  // public pieChartData= [];
  //  public pieChartData1=[]
  //   pieChartData:any = [
  //     { 
  //         data: []
  //     }
  // ];
  public pieChartType: ChartType = 'doughnut';
  //public pieChartLegend = false;
  public pieChartPlugins = [];

  public donutColors = [
    {
      backgroundColor: [
        '#7987a1',
        '#0168FA',
        '#DC3545',
        '#FFC20A',

      ]
    }
  ];

  //chart code end---

  lastdaytext = "LAST DAY";
  lastweektext = "LAST WEEK";
  lastweekCustom = "LAST WEEK";
  lastmonthtext = "LAST MONTH";

  Types: any = {
    year: 'year',
    month: 'month',
    week: 'week',
    day: 'day',
    hour: 'hour'
  }

  DatePickerOptions: any;

  public lineChartData: ChartDataSets[] = [
    { data: [87, 110, 22, 80, 77, 100, 222], label: 'Series B' },

  ];
  public lineChartData2: ChartDataSets[] = [
    { data: [87, 110, 22, 80, 77, 100, 150], label: 'Series B' },
    { data: [54, 110, 98, 44, 23, 64, 120], label: 'Series A' },
  ];
  public lineChartData3: ChartDataSets[] = [
    { data: [87, 110, 22, 80, 77, 100, 150], label: 'Series B' },
    { data: [54, 110, 98, 44, 23, 64, 120], label: 'Series A' },
  ];

  public lineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: "#0168fa"

    },

  ]
  public lineCompareChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: "rgba(1, 104, 250, 0.10)"

    },
    {
      borderColor: '#dc3545',
      backgroundColor: "rgba(220, 56, 72, 0.1)"

    },

  ]
  public lineChartColors2: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ]
  public TodayDate: any;

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this.DatePickerOptions = cloneDeep(this._HelperService.AppConfig.DatePickerOptions);
    this.DatePickerOptions.isAlwaysOpen = true;

    //  monkeyPatchChartJsTooltip();
    //  monkeyPatchChartJsLegend();
  }

  ngOnInit() {
    // this.StartIntro()
    this.initializeDatePicker('dailydate', this._HelperService.AppConfig.DatePickerTypes.hour, 'TodaySalesTrend_dropdown');
    this.initializeDatePicker('weeklydate', this._HelperService.AppConfig.DatePickerTypes.week, 'WeekSalesTrend_dropdown');
    this.initializeDatePicker('monthlydate', this._HelperService.AppConfig.DatePickerTypes.month, 'MonthSalesTrend_dropdown');
    Feather.replace();
    this._HelperService.FullContainer = false;
    this.TodayDate = this._HelperService.GetDateTime(new Date());
    //start time and end time for overview
    this.TodayStartTime = moment().startOf('day');
    this.TodayEndTime = moment().endOf('day');
    // this.GetSalesOverviewLite();
    this.Form_AddUser_Load();
    this.TUTr_Filter_Stores_Load();
    this.InitializeDates();
    // this.RewardList_GetData();
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.CustomerData.next(params);
      this._HelperService.AppConfig.ActiveAccountKey = params["accountkey"];
      this._HelperService.AppConfig.ActiveAccountId = params['accountid'];
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];

      if (this._HelperService.AppConfig.ActiveAccountKey == null || this._HelperService.AppConfig.ActiveAccountId == null) {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
      } else {
        //#region DropdownInit 
        this.GetAccountOverviewLite(this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday);

      }
    });



    // this.GetCampagineOverviewLite(this.TodayStartTime, this.TodayEndTime);

  }




  public _InterSwitchAmount = 0;
  public PaymentId = 0;
  public nameValue2: any = 0;
  public RandomNumber: number = null;

  interswitch() {
    // this.RandomNumber = this._HelperService.Get6DigitRandomNumber();
    this._InterSwitchAmount = null;
    this.PaymentId = this._HelperService.GetRandomNumber();
    this._HelperService.OpenModal('Form_AddUser_InterSwitch');
  }
  timeout = null;





  StartIntro() {
    // introJs().start()
    introJs().setOptions({
      steps: [
        {
          intro: 'Let\'s take a tour to dashboard '

        },
        {
          element: '#TopUp',
          intro: 'Allows to add balance to your account, also shows your remaining balance'
        },
        {
          element: '#SaleAmount',
          intro: 'Shows your Total Sale for the day '
        },
        {
          element: '#Transactions',
          intro: 'Shows Total Number of Transactions done for the day'
        },
        {
          element: '#CustomerVisits',
          intro: 'Shows the total number of Customers visited your store in a day'
        },
        {
          element: '#RewardPercentage',
          intro: 'Shows what percentage of reward you have given to your customers.'
        },
        {
          element: '#CustomerHourly',
          intro: 'Helps to know highest and lowest customer visits at different hours of the day'
        },
        {
          element: '#Highlights',
          intro: 'Shows total customers, New customers, Loyal customers, Avg Spending and Avg Visits of  customers for a Month'
        },
        {
          element: '#CurrentDay',
          intro: 'Compares Total sale of Last Day with Present day'
        },
        {
          element: '#CurrentWeek',
          intro: 'Compares total sale of Last week with current week'
        },
        {
          element: '#CurrentMonth',
          intro: 'Compares total sale of Last month with Current month'
        }
      ]
    }).start();

  }

  InitializeDates(): void {

    //#region Daily Dates 

    this._Daily.ActualStartDate = moment().startOf('day');
    this._Daily.ActualEndDate = moment().endOf('day');

    this._Daily.CompareStartDate = moment().subtract(1, 'day').startOf('day');
    this._Daily.CompareEndDate = moment().subtract(1, 'day').endOf('day');

    //#endregion

    //#region Weekly Dates 

    this._Weekly.ActualStartDate = moment().startOf('week').startOf('day');
    this._Weekly.ActualEndDate = moment().endOf('day');

    this._Weekly.CompareStartDate = moment().subtract(1, 'week').startOf('week').startOf('day');
    this._Weekly.CompareEndDate = moment().subtract(1, 'week').endOf('week').endOf('day');

    //#endregion

    //#region Monthly Dates 

    this._Monthly.ActualStartDate = moment().startOf('month').startOf('day');
    this._Monthly.ActualEndDate = moment().endOf('month').endOf('day');

    this._Monthly.CompareStartDate = moment().subtract(1, 'months').startOf('month').startOf('day');
    this._Monthly.CompareEndDate = moment().subtract(1, 'months').endOf('month').endOf('day');

    this.MonthlybarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Monthly.ActualStartDate), moment(this._Monthly.ActualEndDate));

    //#endregion

  }

  LoadData() {
    this._WeeklySalesReportGetActualData();
    this._DailySalesReportGetActualData();
    this._MonthlySalesReportGetActualData();
    this._VisitsSalesReportGetActualData();
  }


  smsoverview() {

    this._HelperService.IsFormProcessing = true;
    var Data = {
      // Task: 'getsalesoverview',
      StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
      EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,



    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData("https://testwebconnect.thankucash.com/api/v3/campaigns/sms/getsmsoverview", Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverview = _Response.Result as OAccountOverview;
          //#region calculatetype 
          this._AccountOverview['CardTransactionsPerc'] = (this._HelperService.DivideTwoNumbers(this._AccountOverview['CardTransactions'], this._AccountOverview['TotalTransactions'])) * 100;
          this._AccountOverview['CashTransactionsPerc'] = (this._HelperService.DivideTwoNumbers(this._AccountOverview['CashTransactions'], this._AccountOverview['TotalTransactions'])) * 100;
          this.datapieType.datasets[0].data[1] = this._AccountOverview['CardTransactions'];
          this.datapieType.datasets[0].data[0] = this._AccountOverview['CashTransactions'];
          //#endregion
          //#region calculatetermperc 

          if (this._AccountOverview.TerminalStatus && this._AccountOverview.TerminalStatus['Total'] != 0) {
            var _TempVal = this._HelperService.DivideTwoNumbers(100, this._AccountOverview.TerminalStatus['Total']);
            this._AccountOverview["IdleTerminalsPerc"] = this._AccountOverview.TerminalStatus['Idle'] * _TempVal;
            this._AccountOverview["ActiveTerminalsPerc"] = this._AccountOverview.TerminalStatus['Active'] * _TempVal;
            this._AccountOverview["DeadTerminalsPerc"] = this._AccountOverview.TerminalStatus['Dead'] * _TempVal;
            this._AccountOverview["UnusedTerminalsPerc"] = this._AccountOverview.TerminalStatus['Inactive'] * _TempVal;
          } else {
            this._AccountOverview["IdleTerminalsPerc"] = 0;
            this._AccountOverview["ActiveTerminalsPerc"] = 0;
            this._AccountOverview["DeadTerminalsPerc"] = 0;
            this._AccountOverview["UnusedTerminalsPerc"] = 0;
          }

          //#endregion
          //#region calculateothers 

          var other: any = {
            Name: "Other",
            Transactions: 0,
            Amount: 0.0,
            TransactionPerc: 0.0
          }


          if (this._AccountOverview['CardTypeSale']) {
            for (let index = 0; index < this._AccountOverview['CardTypeSale'].length; index++) {
              let element = this._AccountOverview['CardTypeSale'][index];

              other.Transactions += element['Transactions'];
              other.Amount += element['Amount'];

              this.datapieCard.datasets[0].data.push(element['Transactions']);
              //#region CardTypePerc 

              this._AccountOverview['CardTypeSale'][index].TransactionPer = this._HelperService.
                DivideTwoNumbers(element.Transactions, this._AccountOverview["TotalTransactions"]) * 100;
              this._AccountOverview['CardTypeSale'][index].AmountPer = this._HelperService.
                DivideTwoNumbers(element.Amount, this._AccountOverview["TotalSale"]) * 100;

              //#endregion
            }
          }

          other.Transactions = this._AccountOverview["TotalTransactions"] - other['Transactions'];
          this.datapieCard.datasets[0].data.push(other.Transactions);

          other.Amount = this._AccountOverview["TotalSale"] - other['Amount'];

          other.TransactionPer = this._HelperService.
            DivideTwoNumbers(other['Transactions'], this._AccountOverview["TotalTransactions"]) * 100;
          other.AmountPer = this._HelperService.
            DivideTwoNumbers(other['Amount'], this._AccountOverview["TotalSale"]) * 100;

          this._AccountOverview['Others'] = other;

          //#endregion
          this.components.forEach(a => {
            try {
              if (a.chart) a.chart.update();
            } catch (error) {

            }
          });

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });

  }

  totalsms: number = 0;
  deliveredsms: number = 0;
  failedsms: number = 0;
  pendingsms: number = 0;
  SmsOverview: any = {}
  SmsOverviewPer: any = {
    Delivered: null,
    Failed: null,
    Pending: null,

  }
  public showSMSChart: boolean = true

  GetAccountOverviewLite(startdate, endDate) {
    this._HelperService.IsFormProcessing = true;
    var PData = {
      Task: 'getsmsoverview',
      StartDate: startdate,
      EndDate: endDate,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
      AccountId: this._HelperService.AppConfig.ActiveAccountId,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, PData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == "Success") {
          this.SmsOverview = _Response.Result;

          this.showSMSChart = false;
          this._ChangeDetectorRef.detectChanges();
          this.datapieType.datasets[0].data[0] = this.SmsOverview.Total;
          this.datapieType.datasets[0].data[1] = this.SmsOverview.Delivered;
          this.datapieType.datasets[0].data[2] = this.SmsOverview.Failed;
          this.datapieType.datasets[0].data[3] = this.SmsOverview.Pending;
          this.showSMSChart = true;
          this._ChangeDetectorRef.detectChanges();
          this.totalsms = _Response.Result.Total;
          this.deliveredsms = _Response.Result.Delivered
          this.failedsms = _Response.Result.Failed
          this.pendingsms = _Response.Result.Pending
          this.SmsOverviewPer.Delivered = Math.round((this._HelperService.DivideTwoNumbers(this.SmsOverview.Delivered, this.SmsOverview.Total)) * 100);
          this.SmsOverviewPer.Failed = Math.round((this._HelperService.DivideTwoNumbers(this.SmsOverview.Failed, this.SmsOverview.Total)) * 100);
          this.SmsOverviewPer.Pending = Math.round((this._HelperService.DivideTwoNumbers(this.SmsOverview.Pending, this.SmsOverview.Total)) * 100);
        }
      });

  }

  CampaignOverview: any = {}
  GetCampagineOverviewLite(startdate, endDate) {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getcampaignoverview',
      StartDate: startdate,
      EndDate: endDate,
      // AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      // AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,

    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData("api/v3/campaigns/sms/", Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == "Success") {
          this.CampaignOverview = _Response.Result




        }
      });

  }




  //#region 7daychartconfig 

  public BarChartOptions: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      },
    },
    emptyOverlay: {
      fillStyle: 'rgba(255,0,0,0.4)',
      fontColor: 'rgba(255,255,255,1.0)',
      fontStrokeWidth: 0,
      enabled: true
    }
  }

  public barChartLabels: Label[] = [];
  public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#F10875', '#F10875', '#F10875', '#F10875', '#F10875', '#F10875', '#F10875'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Active' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Idle' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Dead' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Inactive' }
  ];


  //#endregion

  //#region last7day 

  public _LastSevenDaysData: any = {};

  //#endregion

  //#region dougnutconfig 

  // public pieChartOptions = {
  //   maintainAspectRatio: false,
  //   responsive: true,
  //   legend: {
  //     display: false,
  //   },
  //   animation: {
  //     animateScale: true,
  //     animateRotate: true
  //   }
  // };

  public doughnutcard = [{ backgroundColor: ['#FFC20A', '#00cccc', '#f10075', '#0168fa'] }];
  public datapieCard = {
    labels: ['Master Card', 'Visa', 'Verve', 'Other Cards'],
    datasets: [{
      data: [],
    }],

  };

  public doughnuttype = [{ backgroundColor: ['#00cccc', '#f10075'] }];
  public datapieType = {
    labels: ['Cash', 'Card',],
    datasets: [{
      data: [0, 0, 0, 0],
      backgroundColor: ['#66a4fb', '#4cebb5']
    }]
  };

  //#endregion

  //#region accountoverview 

  TodayStartTime = null;
  TodayEndTime = null;

  GetSalesOverviewLite() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getsalesoverview',
      StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
      EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      AmountDistribution: true

    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverview = _Response.Result as OAccountOverview;
          //#region calculatetype 
          this._AccountOverview['CardTransactionsPerc'] = (this._HelperService.DivideTwoNumbers(this._AccountOverview['CardTransactions'], this._AccountOverview['TotalTransactions'])) * 100;
          this._AccountOverview['CashTransactionsPerc'] = (this._HelperService.DivideTwoNumbers(this._AccountOverview['CashTransactions'], this._AccountOverview['TotalTransactions'])) * 100;
          this.datapieType.datasets[0].data[1] = this._AccountOverview['CardTransactions'];
          this.datapieType.datasets[0].data[0] = this._AccountOverview['CashTransactions'];
          //#endregion
          //#region calculatetermperc 

          if (this._AccountOverview.TerminalStatus && this._AccountOverview.TerminalStatus['Total'] != 0) {
            var _TempVal = this._HelperService.DivideTwoNumbers(100, this._AccountOverview.TerminalStatus['Total']);
            this._AccountOverview["IdleTerminalsPerc"] = this._AccountOverview.TerminalStatus['Idle'] * _TempVal;
            this._AccountOverview["ActiveTerminalsPerc"] = this._AccountOverview.TerminalStatus['Active'] * _TempVal;
            this._AccountOverview["DeadTerminalsPerc"] = this._AccountOverview.TerminalStatus['Dead'] * _TempVal;
            this._AccountOverview["UnusedTerminalsPerc"] = this._AccountOverview.TerminalStatus['Inactive'] * _TempVal;
          } else {
            this._AccountOverview["IdleTerminalsPerc"] = 0;
            this._AccountOverview["ActiveTerminalsPerc"] = 0;
            this._AccountOverview["DeadTerminalsPerc"] = 0;
            this._AccountOverview["UnusedTerminalsPerc"] = 0;
          }

          //#endregion
          //#region calculateothers 

          var other: any = {
            Name: "Other",
            Transactions: 0,
            Amount: 0.0,
            TransactionPerc: 0.0
          }


          if (this._AccountOverview['CardTypeSale']) {
            for (let index = 0; index < this._AccountOverview['CardTypeSale'].length; index++) {
              let element = this._AccountOverview['CardTypeSale'][index];

              other.Transactions += element['Transactions'];
              other.Amount += element['Amount'];

              this.datapieCard.datasets[0].data.push(element['Transactions']);
              //#region CardTypePerc 

              this._AccountOverview['CardTypeSale'][index].TransactionPer = this._HelperService.
                DivideTwoNumbers(element.Transactions, this._AccountOverview["TotalTransactions"]) * 100;
              this._AccountOverview['CardTypeSale'][index].AmountPer = this._HelperService.
                DivideTwoNumbers(element.Amount, this._AccountOverview["TotalSale"]) * 100;

              //#endregion
            }
          }

          other.Transactions = this._AccountOverview["TotalTransactions"] - other['Transactions'];
          this.datapieCard.datasets[0].data.push(other.Transactions);

          other.Amount = this._AccountOverview["TotalSale"] - other['Amount'];

          other.TransactionPer = this._HelperService.
            DivideTwoNumbers(other['Transactions'], this._AccountOverview["TotalTransactions"]) * 100;
          other.AmountPer = this._HelperService.
            DivideTwoNumbers(other['Amount'], this._AccountOverview["TotalSale"]) * 100;

          this._AccountOverview['Others'] = other;

          //#endregion
          this.components.forEach(a => {
            try {
              if (a.chart) a.chart.update();
            } catch (error) {

            }
          });

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }




  //click event handler for Card Dougnout and TypeDougnut
  onChartClick(event: any): void {

  }

  //#endregion

  //#region terminal 

  public _AccountCustomerOverview: OAccountCustomerOverview =
    {
      CardTypeSale: 0,
      ActiveMerchants: 0,
      ActiveMerchantsDiff: 0,
      ActiveTerminals: 0,
      Idle: 0,
      Dead: 0,
      Active: 0,
      Inactive: 0,
      ActiveTerminalsDiff: 0,
      CardRewardPurchaseAmount: 0,
      CardRewardPurchaseAmountDiff: 0,
      CashRewardPurchaseAmount: 0,
      CashRewardPurchaseAmountDiff: 0,
      Merchants: 0,
      PurchaseAmount: 0,
      PurchaseAmountDiff: 0,
      Terminals: 0,
      Transactions: 0,
      TransactionsDiff: 0,
      UnusedTerminals: 0,
      IdleTerminals: 0,
      TerminalStatus: 0,
      DeadTerminals: 0,
      Total: 0,
      TotalMerchants: 0,
      TotalTerminals: 0,
      ActiveStores: 0,
      TotalStores: 0,
      Ptsp: 0,
      TotalSale: 0,
      TotalTransactions: 0,
      CashTransactionAmount: 0,
      CashTransactionsPerc: 0,
      CashTransactions: 0,
      CardTransactionsAmount: 0,
      CardTransactionsPerc: 0,
      CardTransactions: 0,
      Others: {},
      InvoiceAmount: 0,
      AvgSpentVisit: 0,
      AvgVisitCustomer: 0,
      Transaction: 0,
      TransactionInvoiceAmount: 0,
      AverageVisit: 0,
      AverageInvoiceAmount: 0,
    }

  public _AccountCustomerOverview_SingleDay: OAccountCustomerOverview =
    {
      CardTypeSale: 0,
      ActiveMerchants: 0,
      ActiveMerchantsDiff: 0,
      ActiveTerminals: 0,
      Idle: 0,
      Dead: 0,
      Active: 0,
      Inactive: 0,
      ActiveTerminalsDiff: 0,
      CardRewardPurchaseAmount: 0,
      CardRewardPurchaseAmountDiff: 0,
      CashRewardPurchaseAmount: 0,
      CashRewardPurchaseAmountDiff: 0,
      Merchants: 0,
      PurchaseAmount: 0,
      PurchaseAmountDiff: 0,
      Terminals: 0,
      Transactions: 0,
      TransactionsDiff: 0,
      UnusedTerminals: 0,
      IdleTerminals: 0,
      TerminalStatus: 0,
      DeadTerminals: 0,
      Total: 0,
      TotalMerchants: 0,
      TotalTerminals: 0,
      ActiveStores: 0,
      TotalStores: 0,
      Ptsp: 0,
      TotalSale: 0,
      TotalTransactions: 0,
      CashTransactionAmount: 0,
      CashTransactionsPerc: 0,
      CashTransactions: 0,
      CardTransactionsAmount: 0,
      CardTransactionsPerc: 0,
      CardTransactions: 0,
      Others: {},
      InvoiceAmount: 0,
      AvgSpentVisit: 0,
      AvgVisitCustomer: 0,
      Transaction: 0,
      TransactionInvoiceAmount: 0,
      AverageVisit: 0,
      AverageInvoiceAmount: 0,
    }

  public _AccountOverview: OAccountOverview =
    {
      CardTypeSale: 0,
      ActiveMerchants: 0,
      ActiveMerchantsDiff: 0,
      ActiveTerminals: 0,
      Idle: 0,
      Dead: 0,
      Active: 0,
      Inactive: 0,
      ActiveTerminalsDiff: 0,
      CardRewardPurchaseAmount: 0,
      CardRewardPurchaseAmountDiff: 0,
      CashRewardPurchaseAmount: 0,
      CashRewardPurchaseAmountDiff: 0,
      Merchants: 0,
      PurchaseAmount: 0,
      PurchaseAmountDiff: 0,
      Terminals: 0,
      Transactions: 0,
      TransactionsDiff: 0,
      UnusedTerminals: 0,
      IdleTerminals: 0,
      TerminalStatus: 0,
      DeadTerminals: 0,
      Total: 0,
      TotalMerchants: 0,
      TotalTerminals: 0,
      ActiveStores: 0,
      TotalStores: 0,
      Ptsp: 0,
      TotalSale: 0,
      TotalTransactions: 0,
      CashTransactionAmount: 0,
      CashTransactionsPerc: 0,
      CashTransactions: 0,
      CardTransactionsAmount: 0,
      CardTransactionsPerc: 0,
      CardTransactions: 0,
      Others: {},
      InvoiceAmount: 0,
      Customers: 0
    }

  TerminalsList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveTerminal,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Terminal
    //     .Dashboard,
    //   ReferenceData.ReferenceKey,
    //   ReferenceData.ReferenceId,
    // ]);
  }
  //#endregion

  Form_AddUser: FormGroup;
  Form_AddUser_Show() {
    this._HelperService.OpenModal("Form_AddUser_Content");
  }
  Form_AddUser_Close() {
    // this._Router.navigate([
    //     this._HelperService.AppConfig.Pages.System.AdminUsers
    // ]);
    this._HelperService.CloseModal("Form_AddUser_Content");
  }
  Form_AddUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.saveconfiguration,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      StatusCode: this._HelperService.AppConfig.Status.Active,
      ConfigurationKey: this._HelperService.AppConfig.ConfigurationKey,
      Value: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])],
      Comment: [null, Validators.required],


    });
  }
  Form_AddUser_Clear() {
    this.Form_AddUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddUser_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    if (this._HelperService.AppConfig.RewardPercentagePermission) {
      this._HelperService.IsFormProcessing = false;
      if (_FormValue.Value >= this._HelperService.AppConfig.RewardPercentagePermission.MinimumValue && _FormValue.Value <= this._HelperService.AppConfig.RewardPercentagePermission.MaximumValue) {
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(
          this._HelperService.AppConfig.NetworkLocation.V3.Operations,
          _FormValue
        );
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Reward Percentage Updated successfully");
              this.Form_AddUser_Clear();
              this.Form_AddUser_Close();
              if (_FormValue.OperationType == "close") {
                this.Form_AddUser_Close();
              }
            } else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      } else {
        this._HelperService.NotifyError('Upgrade Your Subscription')
        this.Form_AddUser_Close();
        // this._Router.navigate(['m' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Upgrade]);
        return;
      }

    } else {
      this._HelperService.IsFormProcessing = false;
      this._HelperService.NotifyError('Reward percentage is disabled');
      return;

    }


  }


  // ViewMore() {
  //   this._HelperService._Router.navigate([
  //     this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.MyBuisness.SalesTrends
  //   ])
  // }

  DateSelected(event: any, date: any) {

  }
  public DateTypeModal
  ChooseDateModal(Type: string): void {
    this._HelperService.OpenModal('Form_DatePicker_Content');
    this.DateTypeModal = Type
  }

  showdatepicker_1: boolean = false;
  ToogleDatePicker(): void {
    this.showdatepicker_1 = !this.showdatepicker_1;
  }

  ApplyFilters() {
    $("#TodaySalesTrend_dropdown").dropdown('toggle');
  }

  public RewardHistoryList_Config: OList;
  RewardHistoryList_Setup() {
    this.RewardHistoryList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.getconfigurationhistory,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Operations,
      Title: "Available History",
      StatusType: "default",
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
      SubReferenceKey: "rewardpercentage",
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: "TerminalId",
          SystemName: "TerminalId",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "SerialNumber",
          SystemName: "SerialNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Provider",
          SystemName: "ProviderDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "text-right",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.RewardHistoryList_Config = this._DataHelperService.List_InitializeRewPerc(
      this.RewardHistoryList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Merchant,
      this.RewardHistoryList_Config
    );

    this.RewardHistoryList_GetData();
  }
  RewardHistoryList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.RewardHistoryList_Config.Sort.SortOptions.length; index++) {
        const element = this.RewardHistoryList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.RewardHistoryList_Config


    );

    this.RewardHistoryList_Config = this._DataHelperService.List_Operations(
      this.RewardHistoryList_Config,
      event,
      Type
    );

    if (
      (this.RewardHistoryList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.RewardHistoryList_GetData();
    }

  }
  RewardHistoryList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.RewardHistoryList_Config
    );
    this.RewardHistoryList_Config = TConfig;
  }

  //#region New Reward Perentage 

  RewardPerc: any = null;


  //#endregion

  //#region DateChangeHandler
  DateChanged(event: any, Type: any): void {

    var ev: any = cloneDeep(event);

    //#region Daily 

    this._Daily.ActualStartDate = moment(ev.start).startOf("day");
    this._Daily.ActualEndDate = moment(ev.end).endOf("day");

    this._Daily.CompareStartDate = moment(ev.start).subtract(1, 'day').startOf("day");
    this._Daily.CompareEndDate = moment(ev.end).subtract(1, 'day').endOf("day");

    //#endregion
    //#region Week 

    this._Weekly.ActualStartDate = moment(ev.start).startOf('week').startOf('day');
    this._Weekly.ActualEndDate = moment(ev.end).endOf('day');

    this._Weekly.CompareStartDate = moment(ev.start).subtract(1, 'week').startOf('week').startOf('day');
    this._Weekly.CompareEndDate = moment(ev.end).subtract(1, 'week').endOf('week').endOf('day');

    //#endregion
    //#region Monthly 

    this._Monthly.ActualStartDate = moment(ev.start).startOf('month').startOf('day');
    this._Monthly.ActualEndDate = moment(ev.end).endOf('month').endOf('day');

    this._Monthly.CompareStartDate = moment(ev.start).subtract(1, 'months').startOf('month').startOf('day');
    this._Monthly.CompareEndDate = moment(ev.end).subtract(1, 'months').endOf('month').endOf('day');

    this.MonthlybarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Monthly.ActualStartDate), moment(this._Monthly.ActualEndDate).endOf('month').endOf('day'));
    //#endregion

    // this._DailySalesReportGetActualData();
    // this._WeeklySalesReportGetActualData();
    // this._MonthlySalesReportGetActualData();

    this.GetAccountOverviewLite(this._Daily.ActualStartDate, this._Daily.ActualEndDate);
    this.GetCampagineOverviewLite(this._Daily.ActualStartDate, this._Daily.ActualEndDate);

  }
  //#endregion

  //#region MonthlyBarChartConfig 
  public MonthlyOptions: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      }
    }
  }
  public MonthlybarChartLabels = [];
  // public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public MonthlybarChartColors = [{ backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }];
  public MonthlybarChartType = 'bar';
  public MonthlybarChartLegend = true;
  public MonthlybarChartData = [
    { data: [200], label: 'Remote' },
    { data: [22], label: 'Remote' },
    { data: [22], label: 'Visit' },
    { data: [22], label: 'Visit' },
  ];


  public MonthlylineChartData: ChartDataSets[] = [
    { data: [], label: 'Current Month' },
    { data: [], label: 'Last Month' },
  ];

  public MonthlylineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ]

  //#endregion

  //#region Hourly Visits Report 

  public VisitslineChartData: ChartDataSets[] = [
    { data: [], label: 'Visits' }
  ];
  public VisitslineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',

    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];
  public VisitslineChartLabels = ['00:00', 'O1:00', '02:00', 'O3:00', '04:00', 'O5:00', '06:00', 'O7:00', '08:00', '09:00', '10:00', '11:00',
    '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
  showVisitsChart = true;

  public _Visits: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0,

    HeigestSales: {},
    LowestSales: {}
  }

  public _VisitsSalesReportGetActualData(): void {
    this.VisitslineChartData[0].data = [];
    this.GetSalesReportDaily(this._Daily.ActualStartDate, this._Daily.ActualEndDate, this._Daily.ActualData, 'visit', 'actual');
  }
  //#endregion

  //#region Daily Sales Report 

  public DailylineChartData: ChartDataSets[] = [
    { data: [87, 110, 22, 80, 77, 100, 150], label: 'Today' },
    { data: [87, 110, 22, 80, 77, 100, 150], label: 'Last Day' },
  ];
  public DailylineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];
  public DailylineChartLabels = ['00:00', 'O1:00', '02:00', 'O3:00', '04:00', 'O5:00', '06:00', 'O7:00', '08:00', '09:00', '10:00', '11:00',
    '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
  showDailyChart = true;

  public _Daily: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0,
    HeigestSales: 0,
    LowestSales: 0,
  }

  DailyDateChanged(event: any, Type: any): void {
    var ev: any = cloneDeep(event);
    this._Daily.CompareStartDate = moment(ev.start).startOf("day");
    this._Daily.CompareEndDate = moment(ev.end).endOf("day");

    this.lastdaytext = this._HelperService.GetDateS(this._Daily.CompareStartDate);
    this.DailylineChartData[1].label = this.lastdaytext;

    this._DailySalesReportGetActualData();

    this.hideDailyPicker = true;
    this._HelperService.CloseModal('Form_DatePicker_Content');
  }

  public _DailySalesReportReset(): void {


  }

  public _DailySalesReportGetActualData(): void {

    this.DailylineChartData[0].data = [];
    this.DailylineChartData[1].data = [];
    this.GetSalesReportDaily(this._Daily.ActualStartDate, this._Daily.ActualEndDate, this._Daily.ActualData, this.Types.hour, 'actual');
    this.GetSalesReportDaily(this._Daily.CompareStartDate, this._Daily.CompareEndDate, this._Daily.CompareData, this.Types.hour, 'compare');

  }
  //#endregion

  //#region Weekly Sales Report 

  public WeeklylineChartData: ChartDataSets[] = [
    { data: [], label: 'Current Week' },
    { data: [], label: 'Last Week' },
  ];
  public WeeklylineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];
  public WeeklylineChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
  showWeeklyChart = true;

  public _Weekly: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }

  WeelyDateChanged(event: any, Type: any): void {

    var ev: any = cloneDeep(event);
    this._Weekly.CompareStartDate = moment(ev.start).startOf("week").startOf('day');
    this._Weekly.CompareEndDate = moment(ev.end).endOf("week").endOf('day');


    this.lastweektext = this._HelperService.GetDateSByFormat(this._Weekly.CompareStartDate, 'DD MMM YY')
      + "-" + this._HelperService.GetDateSByFormat(this._Weekly.CompareEndDate, 'DD MMM YY');
    this.WeeklylineChartData[1].label = this._HelperService.GetDateSByFormat(this._Weekly.CompareStartDate, 'DD MMM YY');
    this.lastweekCustom = this._HelperService.GetDateSByFormat(this._Weekly.CompareStartDate, 'DD MMM YYYY')
    this.lastweekCustom = 'Custom Week';
    this._WeeklySalesReportGetActualData();

    this.hideWeeklyPicker = true;
    this._HelperService.CloseModal('Form_DatePicker_Content');
  }

  public _WeeklySalesReportReset(): void {


  }

  public _WeeklySalesReportGetActualData(): void {

    this.WeeklylineChartData[0].data = [];
    this.WeeklylineChartData[1].data = [];

    this.GetSalesReportWeekly(this._Weekly.ActualStartDate, this._Weekly.ActualEndDate, this._Weekly.ActualData, this.Types.day, 'actual');
    this.GetSalesReportWeekly(this._Weekly.CompareStartDate, this._Weekly.CompareEndDate, this._Weekly.CompareData, this.Types.day, 'compare');

  }
  //#endregion

  //#region Monthly Sales Report 

  showMonthlyChart = true;

  public _Monthly: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0,

  }

  MonthlyDateChanged(event: any, Type: any): void {
    var ev: any = cloneDeep(event);
    this._Monthly.CompareStartDate = moment(ev.start).startOf("month").startOf("day");
    this._Monthly.CompareEndDate = moment(ev.end).endOf("month").endOf("day");

    this.lastmonthtext = this._HelperService.GetDateSByFormat(this._Monthly.CompareStartDate, 'MMM YYYY');
    this.MonthlylineChartData[1].label = this._HelperService.GetDateSByFormat(this._Monthly.CompareStartDate, 'MMM YY');

    this.MonthlybarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Monthly.ActualStartDate), moment(this._Monthly.ActualEndDate).endOf('month').endOf('day'));

    this._MonthlySalesReportGetActualData();

    this.hideMonthlyPicker = true;
    this._HelperService.CloseModal('Form_DatePicker_Content');
  }

  public _MonthlySalesReportReset(): void {


  }

  public _MonthlySalesReportGetActualData(): void {

    this.MonthlylineChartData[0].data = [];
    this.MonthlylineChartData[1].data = [];

    this._Monthly.ActualData = this.GetSalesReport(this._Monthly.ActualStartDate, this._Monthly.ActualEndDate, this._Monthly.ActualData, this.Types.month, 'actual');
    this._Monthly.CompareData = this.GetSalesReport(this._Monthly.CompareStartDate, this._Monthly.CompareEndDate, this._Monthly.CompareData, this.Types.month, 'compare');

  }
  //#endregion

  //#region Sales History General Method 

  private pData = {
    Task: 'getsaleshistory',
    StartDate: null,
    EndDate: null,
    AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };


  GetSalesReport(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);
    this.pData.Type = Type;

    if (Type == this.Types.month) {
      this.pData.Type = this.Types.day;
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OSalesTrendData[];

          var TempArray = [];
          var SalesAmount = 0;
          var Heigest = {
            Date: null,
            TotalTransaction: 0.0,
            TotalInvoiceAmount: 0.0
          };

          var Lowest = {
            Date: null,
            TotalTransaction: 0.0,
            TotalInvoiceAmount: 0.0
          };

          if (Type == this._HelperService.AppConfig.GraphTypes.month) {
            var MonthAllDays: any[] = this._HelperService.CalculateIntermediateDate(cloneDeep(StartDateTime), cloneDeep(EndDateTime));
            for (let index = 0; index < MonthAllDays.length; index++) {
              const element = MonthAllDays[index];
              var RData: OSalesTrendData = Data.find(x => moment(x.Date, 'DD-MM-YYYY').format('DD MMM') == element);
              if (RData) {
                TempArray[index] = RData.TotalInvoiceAmount;
                SalesAmount = SalesAmount + RData.TotalInvoiceAmount;
              } else {
                TempArray[index] = 0;
              }
            }
          } else {
            for (let index = 0; index < Data.length; index++) {
              const element: OSalesTrendData = Data[index];
              TempArray.push(element.TotalInvoiceAmount);

              if (Heigest.TotalInvoiceAmount < element.TotalInvoiceAmount) {
                Heigest = element;
              }

              if (Lowest.TotalInvoiceAmount > element.TotalInvoiceAmount) {
                Lowest = element;
              }

              SalesAmount = SalesAmount + element.TotalInvoiceAmount;
            }
          }


          if (Type == this._HelperService.AppConfig.GraphTypes.month) {

            this._Monthly

            this.showMonthlyChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.MonthlylineChartData[0].data = TempArray;
              this._Monthly.ActualSalesAmount = SalesAmount;
            } else if (LineType == 'compare') {
              this.MonthlylineChartData[1].data = TempArray;
              this._Monthly.CompareSalesAmount = SalesAmount;
            }

            this.showMonthlyChart = true;
            this._ChangeDetectorRef.detectChanges();
          }
          else if (Type == this._HelperService.AppConfig.GraphTypes.day) {
            this.showWeeklyChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.WeeklylineChartData[0].data = TempArray;
              this._Weekly.ActualSalesAmount = SalesAmount;
            } else if (LineType == 'compare') {
              this.WeeklylineChartData[1].data = TempArray;
              this._Weekly.CompareSalesAmount = SalesAmount;
            }

            this.showWeeklyChart = true;
            this._ChangeDetectorRef.detectChanges();
          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  GetSalesReportDaily(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    this.pData.Type = this.Types.hour;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OSalesTrendData[];

          var TempArray = [];
          var SalesAmount = 0;
          var Heigest: OSalesTrendDataHourly = {
            Hour: null,
            HourAmPm: '0:00 AM',
            HourAmPmNext: '1:00 AM',
            TotalTransaction: 0.0,
            TotalInvoiceAmount: 0.0,
            TotalCustomer: 0.0
          };

          var Lowest: OSalesTrendDataHourly = {
            Hour: null,
            HourAmPm: null,
            HourAmPmNext: null,
            TotalTransaction: 0.0,
            TotalInvoiceAmount: 0.0,
            TotalCustomer: 0.0
          };

          var DataHourly = _Response.Result as OSalesTrendDataHourly[];
          for (let index = 0; index < 24; index++) {
            var RData: OSalesTrendDataHourly = DataHourly.find(x => x.Hour == index);
            if (RData != undefined && RData != null) {

              if (Type == 'visit') {
                TempArray.push(RData.TotalCustomer);
              } else {
                TempArray.push(RData.TotalInvoiceAmount);
              }

              const element: OSalesTrendDataHourly = RData;
              if (Heigest.TotalCustomer < element.TotalCustomer) {

                Heigest = element;

                //#region current 

                let dd = " AM";
                let h = index;
                if (h >= 12) {
                  h = index - 12;
                  dd = " PM";
                }
                if (h == 0) {
                  h = 12;
                }
                let Hour = h + ":00" + dd;
                Heigest.HourAmPm = Hour;

                //#endregion

                //#region next 

                let dd2 = " AM";
                let h2 = index + 1;
                if (h2 == 24) {
                  h2 = 1;
                }
                else if (h2 >= 12) {
                  h2 = (index + 1) - 12;
                  dd2 = " PM";
                }
                if (h2 == 0) {
                  h2 = 12;
                }
                let Hour2 = Math.abs(h2) + ":00" + dd2;
                Heigest.HourAmPmNext = Hour2;

                //#endregion
              }

              if (Lowest.HourAmPm == null) {
                Lowest = element;

                //#region current 

                let dd3 = " AM";
                let h3 = index;
                if (h3 >= 12) {
                  h3 = index - 12;
                  dd3 = " PM";
                }
                if (h3 == 0) {
                  h3 = 12;
                }
                let Hour3 = h3 + ':00' + dd3;
                Lowest.HourAmPm = Hour3;

                //#endregion

                //#region next 

                let dd4 = " AM";
                let h4 = index + 1;
                if (h4 == 24) {
                  h4 = 1;
                } else if (h4 >= 12) {
                  h4 = (index + 1) - 12;
                  dd4 = " PM";
                }
                if (h4 == 0) {
                  h4 = 12;
                }
                let Hour4 = Math.abs(h4) + ":00" + dd4;
                Lowest.HourAmPmNext = Hour4;

                //#endregion
              }

              if (Lowest.TotalCustomer > element.TotalCustomer && element.TotalCustomer != 0) {
                Lowest = element;

                //#region current 

                let dd3 = " AM";
                let h3 = index;
                if (h3 >= 12) {
                  h3 = index - 12;
                  dd3 = " PM";
                }
                if (h3 == 0) {
                  h3 = 12;
                }
                let Hour3 = h3 + ':00' + dd3;
                Lowest.HourAmPm = Hour3;

                //#endregion

                //#region next 

                let dd4 = " AM";
                let h4 = index + 1;
                if (h4 == 24) {
                  h4 = 1;
                } else if (h4 >= 12) {
                  h4 = (index + 1) - 12;
                  dd4 = " PM";
                }
                if (h4 == 0) {
                  h4 = 12;
                }
                let Hour4 = Math.abs(h4) + ":00" + dd4;
                Lowest.HourAmPmNext = Hour4;

                //#endregion
              }

              SalesAmount = SalesAmount + element.TotalInvoiceAmount;
            }
            else {
              TempArray.push(0);
            }

          }



          if (Type == this._HelperService.AppConfig.GraphTypes.hour) {

            this.showDailyChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.DailylineChartData[0].data = TempArray;
              this._Daily.ActualSalesAmount = SalesAmount;
            } else if (LineType == 'compare') {
              this.DailylineChartData[1].data = TempArray;
              this._Daily.CompareSalesAmount = SalesAmount;
            }

            this.showDailyChart = true;
            this._ChangeDetectorRef.detectChanges();
          } else if (Type == 'visit') {
            this._Visits.HeigestSales = Heigest;
            this._Visits.LowestSales = Lowest;

            this.showVisitsChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.VisitslineChartData[0].data = TempArray;
              this._Visits.ActualSalesAmount = SalesAmount;
            } else if (LineType == 'compare') {
              this.VisitslineChartData[1].data = TempArray;
              this._Visits.CompareSalesAmount = SalesAmount;
            }

            this.showVisitsChart = true;
            this._ChangeDetectorRef.detectChanges();
          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  GetSalesReportWeekly(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    this.pData.Type = this.Types.week;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OSalesTrendData[];




          var TempArray = [];
          var SalesAmount = 0;

          var DataDaily = _Response.Result as OSalesTrendData[];
          for (let index = 0; index < 7; index++) {

            var weekday = '';
            switch (index) {
              case 0: weekday = 'Sunday';
                break;
              case 1: weekday = 'Monday';

                break;
              case 2: weekday = 'Tuesday';

                break;
              case 3: weekday = 'Wednesday';

                break;
              case 4: weekday = 'Thursday';

                break;
              case 5: weekday = 'Friday';

                break;
              case 6: weekday = 'Saturday';
                break;

              default:
                break;
            }

            var RData: OSalesTrendData = DataDaily.find(x => x['WeekDay'] == weekday);

            if (RData != undefined && RData != null) {

              TempArray.push(RData.TotalInvoiceAmount);

              const element: OSalesTrendData = RData;

              SalesAmount = SalesAmount + element.TotalInvoiceAmount;
            }
            else {
              TempArray.push(0);
            }

          }

          if (Type == this._HelperService.AppConfig.GraphTypes.day) {
            this.showWeeklyChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.WeeklylineChartData[0].data = TempArray;
              this._Weekly.ActualSalesAmount = SalesAmount;
            } else if (LineType == 'compare') {
              this.WeeklylineChartData[1].data = TempArray;
              this._Weekly.CompareSalesAmount = SalesAmount;
            }

            this.showWeeklyChart = true;
            this._ChangeDetectorRef.detectChanges();
          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  GetSalesReportMonthly(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);
    this.pData.Type = Type;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OSalesTrendData[];

          var TempArray = [];
          var SalesAmount = 0;

          var DateRange = this._HelperService.CalculateIntermediateDate(moment(this.pData.StartDate).startOf('month'), moment(this.pData.EndDate).endOf('month'));

          for (let index = 0; index < DateRange.length; index++) {
            var RData: OSalesTrendData = Data.find(x => this._HelperService.GetDateTimeSChart(x.Date) == DateRange[index]);
            const element = DateRange[index];
            if (RData) {
            } else {
            }

          }

          for (let index = 0; index < Data.length; index++) {
            const element: OSalesTrendData = Data[index];
            TempArray.push(element.TotalInvoiceAmount);

            SalesAmount = SalesAmount + element.TotalInvoiceAmount;
          }

          if (Type == this._HelperService.AppConfig.GraphTypes.month) {
            this.showMonthlyChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.MonthlylineChartData[0].data = TempArray;
              this._Monthly.ActualSalesAmount = SalesAmount;
            } else if (LineType == 'compare') {
              this.MonthlylineChartData[1].data = TempArray;
              this._Monthly.CompareSalesAmount = SalesAmount;
            }

            this.showMonthlyChart = true;
            this._ChangeDetectorRef.detectChanges();
          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  //#endregion

  //#region Store Filter
  public TUTr_Filter_Store_Selected: any;
  public ToggleStoreSelect: boolean = false;
  public TUTr_Filter_Store_Option: Select2Options;
  TUTr_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Store
        }
      ]
    };

    var OwnerKey = this._HelperService.UserAccount.AccountId;
    if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
      OwnerKey = this._HelperService.UserOwner.AccountId;
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, OwnerKey, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Store_Option = {
      placeholder: 'Select Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Stores_Change(event: any) {
    if (event.value == this.TUTr_Filter_Store_Selected) {
      this.pData.StoreReferenceId = 0;
      this.pData.StoreReferenceKey = null;
      this.TUTr_Filter_Store_Selected = 0;
    }
    else if (event.value != this.TUTr_Filter_Store_Selected) {
      this.pData.StoreReferenceId = event.data[0].ReferenceId;
      this.pData.StoreReferenceKey = event.data[0].ReferenceKey;

      this.TUTr_Filter_Store_Selected = event.value;
    }
    this.LoadData();
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 500);
  }
  //#endregion

  computePerc(num: OSalesTrend): any {

    if (num.CompareSalesAmount == 0) {
      return '100 %';
    }

    if (num.ActualSalesAmount > num.CompareSalesAmount) {
      return Math.round(((num.ActualSalesAmount - num.CompareSalesAmount) / num.ActualSalesAmount) * 100) + ' %';
    } else if (num.ActualSalesAmount < num.CompareSalesAmount) {
      return Math.round(((num.CompareSalesAmount - num.ActualSalesAmount) / num.CompareSalesAmount) * 100) + ' %';
    } else {
      return '0 %';
    }
  }

  computeFlag(num: OSalesTrend): number {
    if (num.ActualSalesAmount > num.CompareSalesAmount) {
      return 1;
    } else if (num.ActualSalesAmount < num.CompareSalesAmount) {
      return -1;
    } else {
      return 0;
    }
  }


  GetLoyaltyOverviewLite() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getloyaltyoverview',
      StartDate: this.TodayStartTime,
      EndDate: this.TodayEndTime,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,

    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._OLoyalityAccountOverview = _Response.Result as OLoyalityAccountOverview;

          this._OLoyalityAccountOverview.AvgSpentVisit = this._OLoyalityAccountOverview.TransactionInvoiceAmount / this._OLoyalityAccountOverview.Transaction;
          this._OLoyalityAccountOverview.AvgVisitCustomer = (this._OLoyalityAccountOverview.Transaction) / (this._OLoyalityAccountOverview.TotalCustomer);



          this.components.forEach(a => {
            try {
              if (a.chart) a.chart.update();
            } catch (error) {

            }
          });

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public _OLoyalityAccountOverview: OLoyalityAccountOverview =
    {

      RewardAmount: 0,
      RedeemAmount: 0,
      NewCustomers: 0,
      RepeatingCustomers: 0,
      TransactionInvoiceAmount: 0,
      Transaction: 0,
      RedeemTransaction: 0,
      RedeemInvoiceAmount: 0,
      AvgSpentVisit: 0,
      AvgVisitCustomer: 0,
      VisitsByRepeatingCustomers: 0,
      TotalCustomer: 0

    }

  CloseRowModal(index: number): void {
    switch (index) {
      case 0: {
        var ev: any = {
          start: moment().subtract(1, 'week').startOf('day'),
          end: moment().subtract(1, 'week').endOf('day')
        };
        this.DailyDateChanged(ev, '');
        $("#TodaySalesTrend_dropdown").dropdown('toggle');
      }

        break;
      case 1: {
        var ev: any = {
          start: moment().subtract(1, 'month').startOf('week').startOf('day'),
          end: moment().subtract(1, 'month').endOf('week').endOf('day')
        };
        this.WeelyDateChanged(ev, '');
        $("#WeekSalesTrend_dropdown").dropdown('toggle');
      }

        break;
      case 2: {
        var ev: any = {
          start: moment().subtract(1, 'year').startOf('month').startOf('day'),
          end: moment().subtract(1, 'year').endOf('month').endOf('day')
        };
        this.MonthlyDateChanged(ev, '');
        $("#MonthSalesTrend_dropdown").dropdown('toggle');
      }

        break;

      default:
        break;
    }
  }

  initializeDatePicker(pickerId: string, type: string, dropdownId: string): void {
    var i = '#' + pickerId;
    var picker = "#" + dropdownId;


    if (type == this._HelperService.AppConfig.DatePickerTypes.month) {
      $(i).datepicker(
        {
          viewMode: "months",
          minViewMode: "months"
        }
      );
    } else {
      $(i).datepicker();
    }

    $(i).on('changeDate', () => {
      switch (type) {
        case this._HelperService.AppConfig.DatePickerTypes.hour:
          {
            this.DailyDateChanged({
              start: $(i).datepicker("getDate"),
              end: $(i).datepicker("getDate")
            }, '');
            $(picker).dropdown('toggle');
          }
          break;

        case this._HelperService.AppConfig.DatePickerTypes.week:
          {
            this.WeelyDateChanged({
              start: $(i).datepicker("getDate"),
              end: $(i).datepicker("getDate")
            }, '');
            $(picker).dropdown('toggle');
          }
          break;

        case this._HelperService.AppConfig.DatePickerTypes.month:
          {
            this.MonthlyDateChanged({
              start: $(i).datepicker("getDate"),
              end: $(i).datepicker("getDate")
            }, '');
            $(picker).dropdown('toggle');
          }
          break;

        default:
          break;
      }
    });
  }

  hideDailyPicker: boolean = true;
  hideWeeklyPicker: boolean = true;
  hideMonthlyPicker: boolean = true;

  ShowHideCalendar(type: string) {
    switch (type) {
      case this._HelperService.AppConfig.DatePickerTypes.hour: {
        this.hideDailyPicker = !this.hideDailyPicker;
      }

        break;
      case this._HelperService.AppConfig.DatePickerTypes.week: {
        this.hideWeeklyPicker = !this.hideWeeklyPicker;
      }

        break;
      case this._HelperService.AppConfig.DatePickerTypes.month: {
        this.hideMonthlyPicker = !this.hideMonthlyPicker;
      }

        break;

      default:
        break;
    }
  }

  _ShowGauge: boolean = true;


}

export class OAccountOverview {
  public TotalMerchants: any;
  public InvoiceAmount: number;
  public TotalTerminals: any;
  public CardTypeSale: any;
  public ActiveStores: any;
  public TotalStores: any;
  public Ptsp: any;
  public TotalSale: any;
  public TotalTransactions: any;
  public CashTransactionAmount: any;
  public CashTransactionsPerc: any;
  public CashTransactions: any;
  public CardTransactionsAmount: any;
  public CardTransactionsPerc: any;
  public CardTransactions: any;
  public Others: any;
  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public TerminalStatus?: number;
  public Total?: number;
  public Idle?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
  public Customers: number;
}
export class OAccountCustomerOverview {
  public TotalMerchants: any;
  public InvoiceAmount: number;
  public TotalTerminals: any;
  public CardTypeSale: any;
  public ActiveStores: any;
  public TotalStores: any;
  public Ptsp: any;
  public TotalSale: any;
  public TotalTransactions: any;
  public CashTransactionAmount: any;
  public CashTransactionsPerc: any;
  public CashTransactions: any;
  public CardTransactionsAmount: any;
  public CardTransactionsPerc: any;
  public CardTransactions: any;
  public Others: any;
  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public TerminalStatus?: number;
  public Total?: number;
  public Idle?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
  public AvgSpentVisit: any;
  public AvgVisitCustomer: any;
  public Transaction: any;
  public TransactionInvoiceAmount: any
  public AverageVisit: any
  public AverageInvoiceAmount: any

  public New?: any;
  public Repeat?: any;

}

export class OLoyalityAccountOverview {
  public RewardAmount: any;
  public RedeemAmount: any;
  public NewCustomers: any;
  public RepeatingCustomers: any;
  public TransactionInvoiceAmount: any;
  public Transaction: any;
  public RedeemTransaction: any;
  public RedeemInvoiceAmount: any;
  public AvgSpentVisit: any;
  public AvgVisitCustomer: any;
  public VisitsByRepeatingCustomers: any;
  public TotalCustomer: any;

}
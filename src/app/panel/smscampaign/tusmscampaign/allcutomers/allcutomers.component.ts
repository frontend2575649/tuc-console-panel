import { ChangeDetectorRef, Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: "tu-allcutomers",
  templateUrl: "./allcutomers.component.html",
})
export class TUAllCustomersComponent implements OnInit, OnDestroy {
  public ResetFilterControls: boolean = true;

  public _ObjectSubscription: Subscription = null;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
    $('[data-toggle="tooltip"]').tooltip({
      placement: 'top'
    });
  }
  GroupId: any = null
  GroupKey: any = null
  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();


    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.CustomerData.next(params);
      this._HelperService.AppConfig.ActiveAccountKey = params["accountkey"];
      this._HelperService.AppConfig.ActiveAccountId = params['accountid'];
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
      // this.GroupKey= params["groupkey"];
      // this.GroupId= params['groupid'];
      // this._HelperService.AppConfig.ActiveOwnerDisplayName = params['title'];

      if (this._HelperService.AppConfig.ActiveAccountKey == null || this._HelperService.AppConfig.ActiveAccountId == null) {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
      } else {
        //#region DropdownInit 

        this.CustomersList_Setup();

      }
    });
    this.GroupId = this._HelperService.AppConfig.ActiveReferenceId;
    this.GroupKey = this._HelperService.AppConfig.ActiveReferenceKey;
    this.CustomersList_Filter_Owners_Load();
    this.InitColConfig();


    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.CustomersList_GetData();
    // });
    // this._HelperService.StopClickPropogation();

    $('[data-toggle="tooltip"]').tooltip({
      placement: 'top'
    });

  }

  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {
    }
  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region merchantlist
  IsDeleteAll: boolean = false

  AllDelete() {
    // for (let index = 0; index < this.CustomersList_Config.Data.length; index++) {
    //   const element = this.CustomersList_Config.Data[index];
    // }

  }


  checkAll(checked) { // pass true or false to check or uncheck all

    this.IsDeleteAll = !this.IsDeleteAll;


    if (this.IsDeleteAll == true) {
      var inputs = document.getElementsByTagName("input");
      for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].type == "checkbox") {
          inputs[i].checked = checked;
          // This way it won't flip flop them and will set them all to the same value which is passed into the function
        }
      }
    }
    else {
      var inputs = document.getElementsByTagName("input");
      for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].type == "checkbox") {
          inputs[i].checked = false;
          // This way it won't flip flop them and will set them all to the same value which is passed into the function
        }
      }
    }

  }


  public GroupItemIdA = []
  CheckedCustomer: boolean = false;
  CHange(RefData) {
    this.CheckedCustomer = !this.CheckedCustomer
    this.GroupItemIdA.push({
      GroupItemId: RefData.GroupItemId,
    })
  }

  PushCustomer(value) {
    if ((<HTMLInputElement>document.getElementById(value)).checked) {
      this.GroupItemIdA.push({
        GroupItemId: value,
      });
    } else {
      let indexx = this.GroupItemIdA.indexOf(value);
      this.GroupItemIdA.splice(indexx, 1);
    }


  }

  public DeleteConfiguration(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;

    var pData = {
      Task: "deletegroupitems",
      GroupId: this.GroupId,
      GroupKey: this.GroupKey,
      IsDeleteAll: false,
      Items: []
    };
    let _OResponse: Observable<OResponse>;
    swal({
      title: "Delete Customer?",
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      // input: 'password',
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      // inputAttributes: {
      //   autocapitalize: 'off',
      //   autocorrect: 'off',
      //   maxLength: "4",
      //   minLength: "4"
      // },
    }).then((result) => {
      if (result.value) {
        if (this.IsDeleteAll) {
          pData.IsDeleteAll = true

        }
        else {
          pData.Items = this.GroupItemIdA;
          pData.IsDeleteAll = false


        }

        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, pData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.IsFormProcessing = false;
              this.GroupItemIdA = [];
              this._HelperService.NotifySuccess("Group Customer Deleted  Successfully ");
              this.CustomersList_Setup();
            } else {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });




  }

  public CustomersList_Config: OList;
  CustomersList_Setup() {
    this.CustomersList_Config = {
      Id: null,
      // Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetCampaignItems,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.SMS,
      Title: "All Customers",
      StatusType: "default",
      IsDownload: true,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'SendDate',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },
      TableFields: [
        {
          DisplayName: "Name",
          SystemName: "Name",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Mobile Number",
          SystemName: "MobileNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Group Name",
          SystemName: "GroupName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "SendDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.CustomersList_Config = this._DataHelperService.List_Initialize(
      this.CustomersList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Stores,
      this.CustomersList_Config
    );

    this.CustomersList_GetData();
  }
  CustomersList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.CustomersList_Config.Sort.SortOptions.length; index++) {
        const element = this.CustomersList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.CustomersList_Config
    );

    this.CustomersList_Config = this._DataHelperService.List_Operations(
      this.CustomersList_Config,
      event,
      Type
    );

    if (
      (this.CustomersList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.CustomersList_GetData();
    }

  }
  CustomersList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.CustomersList_Config
    );
    this.CustomersList_Config = TConfig;


  }
  CustomersList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveStore,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store
    //     .SalesHistory,
    //   ReferenceData.ReferenceKey,
    //   ReferenceData.ReferenceId,
    // ]);

  }

  //#endregion

  //#region OwnerFilter

  public CustomersList_Filter_Owners_Option: Select2Options;
  public CustomersList_Filter_Owners_Selected = null;
  CustomersList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant,
    //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     this._HelperService.AppConfig.AccountType.PosAccount
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.CustomersList_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  CustomersList_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.CustomersList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.CustomersList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.CustomersList_Filter_Owners_Selected,
        "="
      );
      this.CustomersList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.CustomersList_Config.SearchBaseConditions
      );
      this.CustomersList_Filter_Owners_Selected = null;
    } else if (event.value != this.CustomersList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.CustomersList_Filter_Owners_Selected,
        "="
      );
      this.CustomersList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.CustomersList_Config.SearchBaseConditions
      );
      this.CustomersList_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.CustomersList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.CustomersList_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.CustomersList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.CustomersList_Config.SearchBaseConditions = [];
    this.CustomersList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.CustomersList_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.CustomersList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.CustomersList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.CustomersList_Config);

    this.SetOtherFilters();

    this.CustomersList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        maxLength: "32",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Stores
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Stores
        );
        this._FilterHelperService.SetMerchantConfig(this.CustomersList_Config);
        this.CustomersList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.CustomersList_GetData();

    if (ButtonType == 'Sort') {
      $("#CustomersList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#CustomersList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.CustomersList_Config);
    this.SetOtherFilters();

    this.CustomersList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.CustomersList_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

}

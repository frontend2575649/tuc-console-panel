import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { DataHelperService, FilterHelperService, HelperService, OResponse, OSelect, OUserDetails } from "../../../service/service";
import { Observable, Subscription } from 'rxjs';
declare let $: any;
declare var d3: any;
import swal from 'sweetalert2';
import * as Feather from 'feather-icons';

@Component({
  selector: "tu-tusmscampaign",
  templateUrl: "./tusmscampaign.component.html"
})
export class TUSMSCampaignComponent implements OnInit {
  VarAccordion: boolean = false;
  VarAccordion1: boolean = false;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
  }
  public _ObjectSubscription1: Subscription = null;
  public _CampaignDataSubscription: Subscription = null;

  ngOnInit() {
    this._HelperService.ValidateData();
    Feather.replace();
    // this._ActivatedRoute.params.subscribe((params: Params) => {

    //   this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
    //   this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];

    //   this.GetMerchantDetails();
    //   this.Get_UserAccountDetails();

    // });
    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveCampaign);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveAccountKey = StorageDetails.AccountKey;
      this._HelperService.AppConfig.ActiveAccountId = StorageDetails.AccountId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
      this.TList_GetDetails();

    }
    else {
      this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound])
      this._HelperService.DeleteStorage('templocmerchant');
    }

    // if(StorageDetails ==  null || StorageDetails !=  undefined){
    //   this._CampaignDataSubscription = this._HelperService.CustomerData.subscribe(params => {
    //     this._HelperService.AppConfig.ActiveAccountKey = params["accountkey"];
    //     this._HelperService.AppConfig.ActiveAccountId = params['accountid'];
    //     this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
    //     this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
    //     if (this._HelperService.AppConfig.ActiveReferenceId != undefined && this._HelperService.AppConfig.ActiveReferenceId != null) {
    //       this.TList_GetDetails();
  
    //     }
    //     else {
    //       this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound])
    //       this._HelperService.DeleteStorage('templocmerchant');
    //     }
    //   });
    // }

    
  }
  ngOnDestroy(): void {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
    this._HelperService.showAddNewCashierBtn = false;

    try {
      this._ObjectSubscription1.unsubscribe();
    } catch (error) {

    }
  }

  ShowAllLocations(): void {
    this._HelperService.OpenModal("AllGroups");
  }

  CampaignList() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelSMS.AllSmsCampaign])
  }

  public _Configuration: any =
    {
      Title: null,
      Message: null,
      SendDate: null,
      TotalItem: null,
      SenderIdName: null,
      RecipientSubTypeCode: null,
      RecipientTypeCode: null,
      StatusCode: null,
      SendDateFormate: null
    }
  SenderName: boolean = true;
  public Groups: any = [];
  public GroupsList: any = [];

  TList_GetDetails() {
    var pData = {
      Task: 'getcampaign',
      AccountId: this._HelperService.AppConfig.ActiveAccountId,
      AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._Configuration = _Response.Result;
          this._Configuration.SendDateFormate = this._HelperService.GetDateTimeS(this._Configuration.SendDate);
          this.GroupsList = this._Configuration.Groups;
          this._Configuration.StatusB = this._HelperService.GetStatusBadge(
            this._Configuration.StatusCode
          );
          this._ChangeDetectorRef.detectChanges();
          this.SenderName = false;

          this._ChangeDetectorRef.detectChanges();


        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      });
  }

}
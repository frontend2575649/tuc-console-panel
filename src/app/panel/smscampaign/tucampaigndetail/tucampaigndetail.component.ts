import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from "../../../service/service";;
import swal from 'sweetalert2';
declare var $: any;
import * as Feather from 'feather-icons';
declare var google: any;
declare var moment: any;
import * as cloneDeep from 'lodash/cloneDeep';
import { InputFileComponent, InputFile } from 'ngx-input-file';
import { ThrowStmt } from '@angular/compiler';
import { template } from '@angular/core/src/render3';
import * as XLSX from 'xlsx';

@Component({
    selector: 'app-tucampaigndetail',
    templateUrl: './tucampaigndetail.component.html',
    styleUrls: ['./tucampaigndetail.component.css'],
})
export class CampaignDetailsComponent implements OnInit {
    public SaveAccountRequest: any;
    public value: any;
    CurrencySymbol = ""

    @ViewChild(InputFileComponent)
    private InputFileComponent_Term: InputFileComponent;

    CurrentImagesCount: number = 0;
    public ScheduleC: any = null;
    public group: any = null;

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {

        // if(this._HelperService.UserCountry){
        //     if(this._HelperService.UserCountry.CountryIso == 'ng'){

        //         this.CurrencySymbol = "₦"

        //     }

        //     else{
        //         this.CurrencySymbol = "₵"

        //     }
        // }

        // else{
        //     this.CurrencySymbol = ""

        // }
        this.ScheduleC = 9;
        this.group = 0

    }

    CampaignId: any = null
    CampaignKey: any = null
    ngOnInit() {
        //   this.CurrencySymbol   = btoa(unescape(encodeURIComponent(this._HelperService.AppConfig.CurrencySymbolText)))  
        this.ScheduleC = 9;
        this.group = 0
        this.GroupTypeCustomer = "group";
        $('[data-toggle="tooltip"]').tooltip();
        if (this.InputFileComponent_Term != undefined) {
            this.CurrentImagesCount = 0;
            this._HelperService._InputFileComponent = this.InputFileComponent_Term;
            this.InputFileComponent_Term.onChange = (files: Array<InputFile>): void => {
                if (files.length >= this.CurrentImagesCount) {
                    this._HelperService._SetFirstImageOrNone(this.InputFileComponent_Term.files);
                }
                this.CurrentImagesCount = files.length;
            };
        }
        this.SelectedMerchant.DisplayName = this._HelperService.AppConfig.ActiveOwnerDisplayName;
        this.GetRoles_List();
        this.GetCategories_List();
        this.GetMerchants_List();
        this.GetGroups_List();
        this.GetOwner_List();
        this.GetCustomer_List();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveAccountKey = params["accountkey"];
            this._HelperService.AppConfig.ActiveAccountId = params['accountid'];
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
            } else {
                //#region DropdownInit 

                this.TList_GetDetails();
                this.Form_EditUser_Load();


            }
        });
        this.CampaignId = this._HelperService.AppConfig.ActiveReferenceId;
        this.CampaignKey = this._HelperService.AppConfig.ActiveReferenceKey;
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }


    public CustomTypeData: string = null;
    public SelectType: boolean = true;
    CustomType(value: any) {
        this.CustomTypeData = value
        if (this.CustomTypeData == 'List') {
            // this.Form_EditUser.controls['TotalItem'].setValue(this.Customers_Config.TotalRecords);
            // this.ResetFilterUI();

        }
        else {
            // this.Form_EditUser.controls['TotalItem'].setValue('0');
        }
        setTimeout(() => {
            this.SelectType = false
            this.GetCustomer_List();
            this.SelectType = true
        }, 500);



    }

    public GroupTypeCustomer = null
    SelectGroupType(value) {
        this.RecipientSubtype = value

        if (value == 'group') {

            this.group = 'group'

        }
        if (value == 'tuc') {

            this.group = 'tuc'

        }
        if (value == 'external') {

            this.group = 'external'

        }
        if (value == 'manual') {

            this.group = 'manual'

        }



    }


    ReturnCampaignList() {
        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelSMS.AllSmsCampaign])
    }


    SendNotification: boolean = true;
    SendNotificationToggle(): void {
        this.SendNotification = !(this.SendNotification);
    }

    public GetRoles_Option: Select2Options;
    public GetRoles_Transport: any;
    GetRoles_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreParameters,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'Name',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
        this.GetRoles_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetRoles_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetRoles_Transport,
            multiple: false,
        };
    }
    GetRoles_ListChange(event: any) {
        this.Form_EditUser.patchValue(
            {
                RoleKey: event.value
            }
        );
    }

    public GetMerchants_Option: Select2Options;
    public GetMerchants_Transport: any;
    public SelectedMerchant: any = {};

    GetMerchants_List() {
        // var PlaceHolder = "Select Sender";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetSenderIds,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.SMS,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        }

        this.GetMerchants_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetMerchants_Option = {
            placeholder: 'Select Sender',
            ajax: this.GetMerchants_Transport,
            multiple: false,
        };
    }
    GetMerchants_ListChange(event: any) {
        this.SelectedMerchant = event.data[0];
        this.Form_EditUser.patchValue(
            {
                SenderId: event.data[0].ReferenceId,

            },


        );

    }

    public GetCategories_Option: Select2Options;
    public GetCategories_Transport: any;
    GetCategories_List() {
        var PlaceHolder = "Select Sender";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetSenderIds,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.SMS,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceKey",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },

            ]
        }



        this.GetCategories_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetCategories_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetCategories_Transport,
            multiple: false,
        };


    }
    GetCategories_ListChange(event: any) {
        this.Form_EditUser.patchValue(
            {
                SenderId: event.data[0].ReferenceId

            }


        );

    }

    public GetGroups_Option: Select2Options;
    public GetGroups_Transport: any;
    public GroupsList: any = []
    GetGroups_List() {
        var PlaceHolder = "Select Groups";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetGroups,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.SMS,
            AccountId: this._Configuration.AccountId,
            AccountKey: this._Configuration.AccountKey,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "GroupId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "Title",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: 'StatusCode',
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: '=',
                //     SearchValue: this._HelperService.AppConfig.Status.Active,
                // }
            ]
        }


        this.GetGroups_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetGroups_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetGroups_Transport,
            multiple: true,
        };
    }
    GetGroups_ListChange(event: any) {

        this.GroupsList = event.data

    }


    public GetSuggestedGroups_Option: Select2Options;
    public GetSuggestedGroups_Transport: any;
    public SuggestedCustomersList: any = []
    GetSuggestedGroups_List() {
        var PlaceHolder = "Select SuggestedGroups";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.ThankUCash.getcustomers,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.SMS,
            AccountId: this._HelperService.AppConfig.ActiveOwnerId,
            AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "GroupId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "Title",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: 'StatusCode',
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: '=',
                //     SearchValue: this._HelperService.AppConfig.Status.Active,
                // }
            ]
        }


        this.GetSuggestedGroups_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetSuggestedGroups_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetSuggestedGroups_Transport,
            multiple: true,
        };
    }
    GetSuggestedGroups_ListChange(event: any) {

        this.SuggestedCustomersList = event.data

    }



    public CampaignSendDate: any = null
    ScheduleStartDateRangeChange(value) {


        // this.Form_EditUser.patchValue(
        //     {
        //         SendDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
        //     }
        // );
        this._Configuration.SendDateFormate = value.start;
        this._Configuration.SendDate = value.start;

        this.Form_EditUser.patchValue(
            {
                SendDate: value.start,

            }
        );


    }

    EndDatevalue: any = null;
    EndDatevalueF: any = null;

    // Formatedate(){
    //     this.EndDatevalueF = this._HelperService.GetDateS(this.EndDatevalue)

    // }
    ShowAllLocations(): void {
        this._HelperService.OpenModal("AllGroups");
    }


    ScheduleEndDateRangeChange(value) {

        this.IsStartDateAfterEnd = moment(moment(value.start)).isBefore(this.Form_EditUser.controls['StartDate'].value);
        this.EndDatevalue = moment(value.start).format('DD-MM-YYYY'),
            this.Form_EditUser.patchValue(
                {
                    EndDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),

                }
            );
        this.Form_EditUser.patchValue(
            {
                EndDate: moment(value.start),
            }
        );

    }

    _CurrentAddress: any = {};
    Form_EditUser: FormGroup;
    Form_EditUser_Address: string = null;
    Form_EditUser_Latitude: number = 0;
    Form_EditUser_Longitude: number = 0;
    IsStartDateAfterEnd: boolean = false;
    DuplicateDealKey: string;
    DuplicateDealId: number;
    DuplicateAccountId: number;
    DuplicateAccountKey: number;

    public _DealDetails: any =
        {
        }
    @ViewChild('places') places: GooglePlaceDirective;
    Form_EditUser_PlaceMarkerClick(event) {
        this.Form_EditUser_Latitude = event.coords.lat;
        this.Form_EditUser_Longitude = event.coords.lng;
    }
    public Form_EditUser_AddressChange(address: Address) {
        this.Form_EditUser_Latitude = address.geometry.location.lat();
        this.Form_EditUser_Longitude = address.geometry.location.lng();
        this.Form_EditUser_Address = address.formatted_address;
        this.Form_EditUser.controls['Latitude'].setValue(this.Form_EditUser_Latitude);
        this.Form_EditUser.controls['Longitude'].setValue(this.Form_EditUser_Longitude);
        this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
        this.Form_EditUser.controls['Address'].setValue(address.name + ',' + address.formatted_address);
    }
    Form_EditUser_Show() {
    }
    Form_EditUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deals]);
    }
    Form_EditUser_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_EditUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: 'updatecampaign',
            AccountId: this._HelperService.AppConfig.ActiveAccountId,
            AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
            ReferenceId: this.CampaignId,
            ReferenceKey: this.CampaignKey,
            Title: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            Message: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(750)])],
            SendDate: moment(),
            RecipientTypeCode: ['group'],
            SenderId: [null, Validators.required],
            TotalItem: null,
            RecipientSubTypeCode: null

        });
    }
    Form_EditUser_Clear() {
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 300);
        this.Form_EditUser.reset();
        this._HelperService.Icon_Crop_Clear();
        this.Form_EditUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_EditUser_Process(_FormValue: any) {
        this._HelperService.IsFormProcessing = true;
        this.SaveAccountRequest = this.ReFormat_RequestBody();
        // this.SaveAccountRequest.Groups = this.GroupsList;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, this.SaveAccountRequest);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess('Campaign created successfully.');
                    this.ReturnCampaignList()
                    this.Form_EditUser_Clear();
                    if (_FormValue.OperationType == 'edit') {
                    }
                    else if (_FormValue.OperationType == 'close') {
                        // this.Form_EditUser_Close();
                    }
                    // this.Form_EditUser_Close();


                }

                else if (_Response.Message == 'HCP0142: Deal title already exists. Please update existing deal or use new title for deal') {
                    // this._DealDetails = _Response.Result;
                    // this._HelperService.OpenModal('exampleModal');
                    return;

                }
                else {
                    this._HelperService.NotifyError(_Response.Message);

                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }


    Newdate: any = null
    ReFormat_RequestBody(): void {
        var formValue: any = cloneDeep(this.Form_EditUser.value);
        if (this.ActualMessageLength != formValue.Message.length || this.ActualSendDate != formValue.SendDate || this.ActualTotalItem != formValue.TotalItem) {

            var formRequest: any = {
                'OperationType': 'new',
                'Task': 'updatecampaign',
                "AccountId": this._Configuration.AccountId,
                "AccountKey": this._Configuration.AccountKey,
                "ReferenceId": this.CampaignId,
                "ReferenceKey": this.CampaignKey,
                "Title": formValue.Title,
                "Message": formValue.Message,
                "SendDate": formValue.SendDate,
                'RecipientTypeCode': this.GroupTypeCustomer,
                "RecipientSubTypeCode": this.RecipientSubtype,
                "SenderId": formValue.SenderId,
                "StatusCode": 'published',
                "TotalItem": formValue.TotalItem,
                "IsItemUpdated": true

                //  Groups:this.GroupsList

            };

            if (this.GroupTypeCustomer == "group") {
                formRequest.RecipientTypeCode = this.RecipientSubtype,
                    formRequest.RecipientSubTypeCode = this.GroupTypeCustomer,
                    formRequest.Groups = [
                    ];
                for (let index = 0; index < this.GroupsList.length; index++) {
                    const element = this.GroupsList[index];

                    formRequest.Groups.push({
                        GroupId: element.GroupId,
                    });
                }

            }
            else if (this.GroupTypeCustomer == "custom") {

                if (this.CustomTypeData == 'List') {
                    formRequest.RecipientTypeCode = this.RecipientSubtype,
                        formRequest.RecipientSubTypeCode = this.GroupTypeCustomer,
                        formRequest.Items = [
                        ];
                    for (let index = 0; index < this.CustomerList.length; index++) {
                        const element = this.CustomerList[index];

                        formRequest.Items.push({
                            Name: element.DisplayName,
                            MobileNumber: element.MobileNumber
                        });
                    }


                }
                else {
                    formRequest.RecipientTypeCode = this.RecipientSubtype,
                        formRequest.RecipientSubTypeCode = 'tuc',
                        formRequest.Conditions = [
                        ];
                    formRequest.Conditions = this._HelperService.FiltersArrayFinal;
                    //   formRequest.Condition = this.FormSeacrchCondition;
                }
            }
            else if (this.GroupTypeCustomer == "manual") {
                formRequest.RecipientTypeCode = this.RecipientSubtype,
                    formRequest.RecipientSubTypeCode = this.GroupTypeCustomer,
                    formRequest.Items = [
                    ];
                for (let index = 0; index < this.CustomerList.length; index++) {
                    const element = this.CustomerList[index];

                    formRequest.Items.push({
                        Name: element.DisplayName,
                        MobileNumber: element.MobileNumber
                    });
                }

            }
            else {
                formRequest.RecipientTypeCode = this.RecipientSubtype,
                    formRequest.RecipientSubTypeCode = this.GroupTypeCustomer,
                    formRequest.Items = [
                    ];
                formRequest.Items = this.filelist;
                formRequest.FileName = this.NameFile;

            }

            return formRequest;
        }
        else {
            var formRequest: any = {
                'OperationType': 'new',
                'Task': 'updatecampaign',
                "AccountId": this._Configuration.AccountId,
                "AccountKey": this._Configuration.AccountKey,
                "ReferenceId": this.CampaignId,
                "ReferenceKey": this.CampaignKey,
                "Title": formValue.Title,
                "Message": formValue.Message,
                "SendDate": formValue.SendDate,
                'RecipientTypeCode': this.GroupTypeCustomer,
                "RecipientSubTypeCode": this.RecipientSubtype,
                "SenderId": formValue.SenderId,
                "StatusCode": 'published',
                "TotalItem": formValue.TotalItem,
                "IsItemUpdated": false

                //  Groups:this.GroupsList

            };

            // if (formValue.SendDate == this.ActualSendDate) {

            //     formRequest.SendDate = this._Configuration.SendDate

            // }
            // else {
            //     formRequest.SendDate = formValue.SendDate
            // }

            if (this.GroupTypeCustomer == "group") {
                formRequest.RecipientTypeCode = this.RecipientSubtype,
                    formRequest.RecipientSubTypeCode = this.GroupTypeCustomer,
                    formRequest.Groups = [
                    ];
                for (let index = 0; index < this.GroupsList.length; index++) {
                    const element = this.GroupsList[index];

                    formRequest.Groups.push({
                        GroupId: element.GroupId,
                    });
                }

            }
            else if (this.GroupTypeCustomer == "custom") {

                if (this.CustomTypeData == 'List') {
                    formRequest.RecipientTypeCode = this.RecipientSubtype,
                        formRequest.RecipientSubTypeCode = this.GroupTypeCustomer,
                        formRequest.Items = [
                        ];
                    for (let index = 0; index < this.CustomerList.length; index++) {
                        const element = this.CustomerList[index];

                        formRequest.Items.push({
                            Name: element.DisplayName,
                            MobileNumber: element.MobileNumber
                        });
                    }


                }
                else {
                    formRequest.RecipientTypeCode = this.RecipientSubtype,
                        formRequest.RecipientSubTypeCode = 'tuc',
                        formRequest.Conditions = [
                        ];
                    formRequest.Conditions = this._HelperService.FiltersArrayFinal;
                    //   formRequest.Condition = this.FormSeacrchCondition;
                }
            }
            else if (this.GroupTypeCustomer == "manual") {
                formRequest.RecipientTypeCode = this.RecipientSubtype,
                    formRequest.RecipientSubTypeCode = this.GroupTypeCustomer,
                    formRequest.Items = [
                    ];
                for (let index = 0; index < this.CustomerList.length; index++) {
                    const element = this.CustomerList[index];

                    formRequest.Items.push({
                        Name: element.DisplayName,
                        MobileNumber: element.MobileNumber
                    });
                }

            }
            else {
                formRequest.RecipientTypeCode = this.RecipientSubtype,
                    formRequest.RecipientSubTypeCode = this.GroupTypeCustomer,
                    formRequest.Items = [
                    ];
                formRequest.Items = this.filelist;
                formRequest.FileName = this.NameFile;

            }

            return formRequest;
        }
    }

    title = 'XlsRead'
    file: File;
    arrayBuffer: any;
    filelist: any;
    public NameFile: string = null;
    IsUploading = false;
    UploadCount = 0;

    addfile(event) {

        this.file = event.target.files[0];
        this.NameFile = this.file.name;
        let fileReader = new FileReader();
        fileReader.readAsArrayBuffer(this.file);
        fileReader.onload = (e) => {
            this.arrayBuffer = fileReader.result;
            var data = new Uint8Array(this.arrayBuffer);
            var arr = new Array();
            for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
            var bstr = arr.join("");
            var workbook = XLSX.read(bstr, { type: "binary" });
            var first_sheet_name = workbook.SheetNames[0];
            var worksheet = workbook.Sheets[first_sheet_name];
            this.filelist = XLSX.utils.sheet_to_json(worksheet, { raw: true });
        }
    }

    //#endregion
    public _Configuration: any =
        {
            Title: null,
            Message: null,
            SendDate: null,
            TotalItem: 0,
            SenderIdName: null,
            RecipientSubTypeCode: null,
            RecipientTypeCode: null,
            StatusCode: null,
            SendDateFormate: null,

        }
    SenderName: boolean = true;
    ActualMessageLength: string = null
    ActualTotalItem: string = null
    ActualSendDate: string = null

    public Groups: any = []
    TList_GetDetails() {
        var pData = {
            Task: 'getcampaign',
            AccountId: this._HelperService.AppConfig.ActiveAccountId,
            AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._Configuration = _Response.Result;
                    this.MerchantId = this._Configuration.AccountId;
                    this.Merchantkey = this._Configuration.AccountKey;
                    this.RecipientSubtype = this._Configuration.RecipientTypeCode;
                    this.GroupTypeCustomer = this._Configuration.RecipientSubTypeCode;
                    this.ActualMessageLength = this._Configuration.Message.length;
                    this.ActualTotalItem = this._Configuration.TotalItem;
                    this.ActualSendDate = this._Configuration.SendDate;
                    // this._Configuration.SendDateFormate = this._HelperService.GetDateTimeS(this._Configuration.SendDate);
                    // this.Form_EditUser.controls['SendDate'].setValue(this._Configuration.SendDate)
                    this._Configuration.SendDateFormate = moment(this._Configuration.SendDate).format('DD-MM-YYYY HH:mm');
                    this.GroupsList = this._Configuration.Groups;
                    this._ChangeDetectorRef.detectChanges();
                    this.SenderName = false;
                    this.GroupTypeCustomer = this._Configuration.RecipientSubTypeCode;
                    this.GetMerchants_Option.placeholder = this._Configuration.SenderIdName;
                    this.GetOwners_Option.placeholder = this._Configuration.AccountDisplayName;
                    this.Form_EditUser.controls['SenderId'].setValue(this._Configuration.SenderId)
                    this._ChangeDetectorRef.detectChanges();
                    this.SenderName = true;
                    this.GetGroups_List();
                    this.GetCustomer_List();
                    this.countmessagesize();

                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    showMainContent: Boolean = false;
    typeCustomer: any = null;
    RecipientSubtype: any = null;
    AllCustomers: number = 0;

    ShowHideButton(element) {
        this.GroupTypeCustomer = element;
        if (this.GroupTypeCustomer == "group") {
            this.RecipientSubtype = "group"
        }
        else {
            this.RecipientSubtype = "individual"
        }

    }
    public GetCustomers_Option: Select2Options;
    public GetCustomer_Transport: any;
    GetCustomer_List() {
        var PlaceHolder = "Select Customers";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetCustomers,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.SMS,
            AccountId: this._Configuration.AccountId,
            AccountKey: this._Configuration.AccountKey,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },

                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false
                },
                {
                    SystemName: "MobileNumber",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'CreateDate',
                    Type: this._HelperService.AppConfig.DataType.Date,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ]
        }


        this.GetCustomer_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetCustomers_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetCustomer_Transport,
            multiple: true,
        };
    }
    public CustomerList: any = []
    GetCustomer_ListChange(event: any) {
        this.CustomerList = event.data;
        var ItemCount = 0
        for (let index = 0; index < this.CustomerList.length; index++) {
            const element = this.CustomerList[index];
            ItemCount = element.ItemCount + 1;

        }
        this.AllCustomers = this.CustomerList.length
        this.Form_EditUser.controls['TotalItem'].setValue(this.AllCustomers);


    }

    SmsCount: number = 0
    SmsPricing: number = 0
    MessageLength: string = null
    countmessagesize() {
        this.SmsCount = Math.ceil(this.Form_EditUser.controls['Message'].value.length / 140);
        this.SmsPricing = this.SmsCount * 4;
        this.MessageLength = this.Form_EditUser.controls['Message'].value.length;
    }


    public GetOwners_Option: Select2Options;
    public GetOwners_Transport: any;
    public SelectedOwner: any = {};
    GetOwner_List() {
        var PlaceHolder = "Select Merchant";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ]
        }

        this.GetOwners_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetOwners_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetOwners_Transport,
            multiple: false,
        };
    }
    MerchantId: number = null;
    Merchantkey: string = null;
    public ToggleMerchantSelect: boolean = true;

    GetOwners_ListChange(event: any) {
        this.SelectedMerchant = event.data[0];
        this.Form_EditUser.patchValue(
            {
                AccountKey: event.data[0].ReferenceKey,
                AccountId: event.data[0].ReferenceId
            }
        );

        this.ToggleMerchantSelect = false;
        this.GetGroups_List();

        this.MerchantId = event.data[0].ReferenceId;
        this.Merchantkey = event.data[0].ReferenceKey;
        setTimeout(() => {
            this.ToggleMerchantSelect = true;
            this.GetGroups_List();
            this.GetCustomer_List();
            // this.Customers_GetData();
        }, 500);




    }

}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AgmCoreModule } from '@agm/core';
import { CampaignDetailsComponent } from './tucampaigndetail.component';
// import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ImageCropperModule } from 'ngx-image-cropper';
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { TimepickerModule } from 'ngx-bootstrap';

const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 3,
};
const routes: Routes = [
  { path: '', component: CampaignDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUCampaignDetailRoutingModule { }
@NgModule({
  declarations: [CampaignDetailsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    GooglePlaceModule,
    ImageCropperModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
    }),
    TUCampaignDetailRoutingModule,
    InputFileModule.forRoot(config),
    TimepickerModule.forRoot()
    // LeafletModule
  ]
})
export class CampaignDetailModule { }

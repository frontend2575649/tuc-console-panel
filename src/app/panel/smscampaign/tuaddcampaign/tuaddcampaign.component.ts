import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, FilterHelperService } from "../../../service/service";;
import swal from 'sweetalert2';
declare var $: any;
import * as Feather from 'feather-icons';
declare var google: any;
declare var moment: any;
import * as cloneDeep from 'lodash/cloneDeep';
import { InputFileComponent, InputFile } from 'ngx-input-file';
import { ThrowStmt } from '@angular/compiler';
import { template } from '@angular/core/src/render3';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-tuaddcampaign',
  templateUrl: './tuaddcampaign.component.html',
  styleUrls: ['./tuaddcampaign.component.css'],
})
export class AddCampaignComponent implements OnInit {
  public SaveAccountRequest: any;
  public value: any;
  CurrencySymbol = ""

  @ViewChild(InputFileComponent)
  private InputFileComponent_Term: InputFileComponent;

  CurrentImagesCount: number = 0;
  public ScheduleC: any = null;
  public group: any = null;
  public CustomersAll = "All Customers"
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService

  ) {

    // if(this._HelperService.UserCountry){
    //     if(this._HelperService.UserCountry.CountryIso == 'ng'){

    //         this.CurrencySymbol = "₦"

    //     }

    //     else{
    //         this.CurrencySymbol = "₵"

    //     }
    // }

    // else{
    //     this.CurrencySymbol = ""

    // }
    this.ScheduleC = 9;
    this.group = 0

  }

  Newdate: any = null
  ngOnInit() {

    //   this.CurrencySymbol   = btoa(unescape(encodeURIComponent(this._HelperService.AppConfig.CurrencySymbolText)))  
    this.ScheduleC = 9;
    this.group = 0
    this.GroupTypeCustomer = "group";
    this.RecipientSubtype = "group";
    this.CustomTypeData = 'List';
    this.Newdate = new Date();
    $('[data-toggle="tooltip"]').tooltip();
    if (this.InputFileComponent_Term != undefined) {
      this.CurrentImagesCount = 0;
      this._HelperService._InputFileComponent = this.InputFileComponent_Term;
      this.InputFileComponent_Term.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(this.InputFileComponent_Term.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
    this.SelectedMerchant.DisplayName = this._HelperService.AppConfig.ActiveOwnerDisplayName;
    this.Form_AddUser_Load();
    // this.GetRoles_List();
    this.GetOwner_List();
    this.GetCategories_List();
    this.GetMerchants_List();
    this.GetGroups_List();
    this.GetCustomer_List();
    this.Customers_Setup()

    // this.GetBalance();
    this.checkBalence();
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
    });
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }

  CustomerCount: any = null;
  name: string = ""
  public errorText: boolean = false
  paybleAmount: number = 0;
  keyup(name) {
    this.CustomerCount = name * 4;
    if (this.CustomerCount > this._AccountBalance.Balance) {
      this.errorText = true;
    } else {
      this.errorText = false;
    }
    this.paybleAmount = this.CustomerCount - this._AccountBalance.Balance
    this.checkBalence();
  }

  checkBalence() {
    var Ref = this.generateRandomNumber();
    this._TransactionReference = 'Tp' + '_' + this._HelperService.UserAccount.AccountId + '_' + Ref;
  }

  activatePayment: boolean = false;

  paymentDone(ref: any) {
    this.TransactionId = ref.trans
    if (ref != undefined && ref != null && ref != '') {
      if (ref.status == 'success') {
        this.CreditAmount();
      }
      else {
        this._HelperService.NotifyError('Payment failed');
      }
    }
    else {
      this._HelperService.NotifyError('Payment failed');
    }
    // this.title = 'Payment successfull';
  }
  public TransactionId
  public _AccountBalanceCreditAmount = 0;
  public CreditAmount() {
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "topupwallet",
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      Amount: this.paybleAmount,
      PaymentReference: this._TransactionReference,
      TransactionId: this.TransactionId,
      PaymentSource: 'wallet'
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, PostData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountBalanceCreditAmount = 0;
          this._TransactionReference = null;
          this._HelperService.NotifySuccess("Account credited");
          this.GetBalance();
          this.errorText = false;


        } else {
          this._HelperService.NotifySuccess(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  paymentCancel() {
  }

  public _TransactionReference = null;

  generateRandomNumber() {
    var TrRef = Math.floor(100000 + Math.random() * (999999 + 1 - 100000));
    return TrRef;
  }

  public _AccountBalance =
    {
      Credit: 0,
      Debit: 0,
      Balance: 0,
      Sms: 0
    }
  totalBalence: any;
  public GetBalance() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: 'getbalance',
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      Source: this._HelperService.AppConfig.TransactionSource.Merchant
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          // this.checkBalence(this.AllCustomers);
          this._AccountBalance = _Response.Result;
          this.totalBalence = this._AccountBalance.Balance
          this._AccountBalance.Sms = Math.round(this._AccountBalance.Balance / 4)


          // this._AccountBalance.Balance = _Response.Result.Balance / 100;
          // this._AccountBalance.Credit = _Response.Result.Credit / 100;
          // this._AccountBalance.Debit = _Response.Result.Debit / 100;
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  IsSendNow: boolean = false
  IsSendNowF() {
    this.IsSendNow = true;
  }

  public GroupTypeCustomer = null
  SelectGroupType(value) {
    this.RecipientSubtype = value
    // this.Form_AddUser.controls['TotalItem'].setValue(0);
    this.AllCustomers = 0;
  }


  ReturnCampaignList() {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelSMS.AllSmsCampaign])
  }


  SendNotification: boolean = true;
  SendNotificationToggle(): void {
    this.SendNotification = !(this.SendNotification);
  }

  public GetMerchants_Option: Select2Options;
  public GetMerchants_Transport: any;
  public SelectedMerchant: any = {};

  GetMerchants_List() {
    var PlaceHolder = "Select Sender";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetSenderIds,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.SMS,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },

        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }

      ]
    }

    this.GetMerchants_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetMerchants_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetMerchants_Transport,
      multiple: false,
    };
  }
  SenderName: string = null;
  GetSenders_ListChange(event: any) {
    this.SenderName = event.data[0].Name
    this.Form_AddUser.patchValue(
      {
        SenderId: event.data[0].ReferenceId,

      },


    );

  }

  public GetCategories_Option: Select2Options;
  public GetCategories_Transport: any;
  GetCategories_List() {
    var PlaceHolder = "Select Sender";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetSenderIds,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.SMS,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },

        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },

      ]
    }



    this.GetCategories_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetCategories_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetCategories_Transport,
      multiple: false,
    };


  }
  GetCategories_ListChange(event: any) {
    this.Form_AddUser.patchValue(
      {
        SenderId: event.data[0].ReferenceId

      }
    );
  }

  public GetGroups_Option: Select2Options;
  public GetGroups_Transport: any;
  public GroupsList: any = []
  GetGroups_List() {
    var PlaceHolder = "Select Groups";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetGroups,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.SMS,
      AccountId: this.MerchantId,
      AccountKey: this.Merchantkey,
      SortCondition: [],
      Fields: [
        {
          SystemName: "GroupId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },

        {
          SystemName: "Title",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "ItemCount",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: false,
          Text: true,
        },
        // {
        //     SystemName: 'StatusCode',
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: '=',
        //     SearchValue: this._HelperService.AppConfig.Status.Active,
        // }
      ]
    }


    this.GetGroups_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetGroups_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetGroups_Transport,
      multiple: true,
    };
  }
  AllCustomers: number = 0;
  public SelectCustomer: boolean = false;
  GetGroups_ListChange(event: any) {
    this.GroupsList = event.data
    var ItemCount = 0
    for (let index = 0; index < this.GroupsList.length; index++) {
      const element = this.GroupsList[index];
      ItemCount = ItemCount + element.ItemCount;

    }
    if (this.AllCustomers == 0 && this.AllCustomers == undefined) {
      this.SelectCustomer = false;

    }
    else {
      this.SelectCustomer = true;

    }

    this.AllCustomers = ItemCount

    this.Form_AddUser.controls['TotalItem'].setValue(this.AllCustomers);

    this.SMSPRICINGData = this.AllCustomers * 4
    this.paybleAmount = this.SMSPRICINGData - this._AccountBalance.Balance

    if (this.SMSPRICINGData > this._AccountBalance.Balance) {


      this.errorText = true
    } else {
      this.errorText = false;
    }
  }
  public CampaignSendDate: any = this._HelperService.GetDateTimeS(moment());
  ScheduleStartDateRangeChange(value) {
// console.log("CampaignSendDate",this.CampaignSendDate);

    // this.Form_AddUser.patchValue(
    //     {
    //         SendDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
    //     }
    // );
    this.CampaignSendDate = this._HelperService.GetDateTimeS(value.start)
    this.Form_AddUser.patchValue(
      {
        SendDate: value.start,

      }
    );


  }

  EndDatevalue: any = null;
  EndDatevalueF: any = null;

  ScheduleEndDateRangeChange(value) {

    this.IsStartDateAfterEnd = moment(moment(value.start)).isBefore(this.Form_AddUser.controls['StartDate'].value);
    this.EndDatevalue = moment(value.start).format('DD-MM-YYYY'),
      this.Form_AddUser.patchValue(
        {
          EndDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),

        }
      );
    this.Form_AddUser.patchValue(
      {
        EndDate: moment(value.start),
      }
    );

  }
  showMainContent: Boolean = false;
  typeCustomer: any = null;
  RecipientSubtype: any = null;
  SMSPRICINGData: any = null
  ShowHideButton(element) {
    this.GroupTypeCustomer = element;
    this.Form_AddUser.controls['TotalItem'].setValue(null);
    if (this.GroupTypeCustomer == "group") {
      this.RecipientSubtype = "group";
    }
    else {
      this.RecipientSubtype = "individual";
    }
    // this.showMainContent = this.showMainContent ? false : true;
  }
  public GetCustomers_Option: Select2Options;
  public GetCustomer_Transport: any;

  GetCustomer_List() {
    if (this.CustomTypeData == 'List') {
      var PlaceHolder = "Select Customers";
      var _Select: OSelect =
      {
        Task: this._HelperService.AppConfig.Api.ThankUCash.GetCustomers,
        Location: this._HelperService.AppConfig.NetworkLocation.V3.SMS,
        AccountId: this.MerchantId,
        AccountKey: this.Merchantkey,
        SortCondition: [],
        Fields: [
          {
            SystemName: "DisplayName",
            Type: this._HelperService.AppConfig.DataType.Text,
            Id: false,
            Text: true,
          },

          {
            SystemName: "ReferenceId",
            Type: this._HelperService.AppConfig.DataType.Number,
            Id: true,
            Text: false
          },
          {
            SystemName: "MobileNumber",
            Type: this._HelperService.AppConfig.DataType.Text,
            Id: false,
            Text: true,
          },
          {
            SystemName: 'CreateDate',
            Type: this._HelperService.AppConfig.DataType.Date,
            SearchCondition: '=',
            SearchValue: this._HelperService.AppConfig.Status.Active,
          }
        ]
      }
      this.GetCustomer_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
      this.GetCustomers_Option = {
        placeholder: PlaceHolder,
        ajax: this.GetCustomer_Transport,
        multiple: true,
        disabled: false
      };
    }
    else {
      var PlaceHolder = "Select Customers";
      var _Select: OSelect =
      {
        Task: this._HelperService.AppConfig.Api.ThankUCash.GetCustomers,
        Location: this._HelperService.AppConfig.NetworkLocation.V3.SMS,
        AccountId: this._HelperService.AppConfig.ActiveOwnerId,
        AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
        SortCondition: [],
        Fields: [
          {
            SystemName: "DisplayName",
            Type: this._HelperService.AppConfig.DataType.Text,
            Id: false,
            Text: true,
          },

          {
            SystemName: "ReferenceId",
            Type: this._HelperService.AppConfig.DataType.Number,
            Id: true,
            Text: false
          },
          {
            SystemName: "MobileNumber",
            Type: this._HelperService.AppConfig.DataType.Text,
            Id: false,
            Text: true,
          },
          {
            SystemName: 'CreateDate',
            Type: this._HelperService.AppConfig.DataType.Date,
            SearchCondition: '=',
            SearchValue: this._HelperService.AppConfig.Status.Active,
          }
        ]
      }
      this.GetCustomer_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
      this.GetCustomers_Option = {
        placeholder: PlaceHolder,
        ajax: this.GetCustomer_Transport,
        multiple: true,
        disabled: true
      };
    }

  }
  public CustomerList: any = []
  GetCustomer_ListChange(event: any) {

    this.CustomerList = event.data;
    var ItemCount = 0
    for (let index = 0; index < this.CustomerList.length; index++) {
      const element = this.CustomerList[index];
      ItemCount = element.ItemCount + 1;

    }
    this.AllCustomers = this.CustomerList.length
    if (this.AllCustomers == 0 && this.AllCustomers == undefined) {
      this.SelectCustomer = false;

    }
    else {
      this.SelectCustomer = true;

    }

    this.Form_AddUser.controls['TotalItem'].setValue(this.AllCustomers);
    this.SMSPRICINGData = this.AllCustomers * 4
    this.paybleAmount = this.SMSPRICINGData - this._AccountBalance.Balance

    if (this.SMSPRICINGData > this._AccountBalance.Balance) {


      this.errorText = true
    } else {
      this.errorText = false;
    }

  }

  _CurrentAddress: any = {};
  Form_AddUser: FormGroup;
  Form_AddUser_Address: string = null;
  Form_AddUser_Latitude: number = 0;
  Form_AddUser_Longitude: number = 0;
  IsStartDateAfterEnd: boolean = false;
  DuplicateDealKey: string;
  DuplicateDealId: number;
  DuplicateAccountId: number;
  DuplicateAccountKey: number;

  public _DealDetails: any =
    {
    }
  @ViewChild('places') places: GooglePlaceDirective;
  Form_AddUser_PlaceMarkerClick(event) {
    this.Form_AddUser_Latitude = event.coords.lat;
    this.Form_AddUser_Longitude = event.coords.lng;
  }
  public Form_AddUser_AddressChange(address: Address) {
    this.Form_AddUser_Latitude = address.geometry.location.lat();
    this.Form_AddUser_Longitude = address.geometry.location.lng();
    this.Form_AddUser_Address = address.formatted_address;
    this.Form_AddUser.controls['Latitude'].setValue(this.Form_AddUser_Latitude);
    this.Form_AddUser.controls['Longitude'].setValue(this.Form_AddUser_Longitude);
    this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
    this.Form_AddUser.controls['Address'].setValue(address.name + ',' + address.formatted_address);
  }
  Form_AddUser_Show() {
  }
  Form_AddUser_Close() {
    this._Router.navigate(["m" + "/" + this._HelperService.AppConfig.Pages.ThankUCash.PanelSMS.AddCampaign]);
  }
  Form_AddUser_Load() {
    this._HelperService._Icon_Cropper_Data.Width = 128;
    this._HelperService._Icon_Cropper_Data.Height = 128;
    this._HelperService.IsFormProcessing = false;

    this.Form_AddUser = this._FormBuilder.group({
      OperationType: 'new',
      Task: 'savecampaign',
      AccountId: this.MerchantId,
      AccountKey: this.Merchantkey,
      Title: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
      Message: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(510)])],
      SendDate: null,
      RecipientTypeCode: [null],
      SenderId: [null, Validators.required],
      TotalItem: [null, Validators.required]

    });
  }
  Form_AddUser_Clear() {
    this._HelperService.ToggleField = true;
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 300);
    this.Form_AddUser.reset();
    this._HelperService.Icon_Crop_Clear();
    this.Form_AddUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddUser_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    this.SaveAccountRequest = this.ReFormat_RequestBody();
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, this.SaveAccountRequest);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess('Campaign created successfully.');
          this.ReturnCampaignList();
          this.Form_AddUser_Clear();
          if (_FormValue.OperationType == 'edit') {
          }
          else if (_FormValue.OperationType == 'close') {
            this.Form_AddUser_Close();
          }
          // this.Form_AddUser_Close();


        }

        else if (_Response.Message == 'HCP0142: Deal title already exists. Please update existing deal or use new title for deal') {
          this._DealDetails = _Response.Result;
          this._HelperService.OpenModal('exampleModal');
          return;

        }
        else {
          this._HelperService.NotifyError(_Response.Message);

        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  public CustomTypeData: string = null;
  public SelectType: boolean = true;
  CustomType(value: any) {
    this.CustomTypeData = value
    if (this.CustomTypeData == 'List') {
      // this.Form_AddUser.controls['TotalItem'].setValue(this.Customers_Config.TotalRecords);
      this.ResetFilterUI();

    }
    else {
      // this.Form_AddUser.controls['TotalItem'].setValue('0');
    }
    setTimeout(() => {
      this.SelectType = false
      this.GetCustomer_List();
      this.SelectType = true
    }, 500);



  }
  SmsCount: number = 0
  SmsPricing: number = 0
  MessageLength: string = null
  countmessagesize() {
    this.SmsCount = Math.ceil(this.Form_AddUser.controls['Message'].value.length / 140);
    this.SmsPricing = this.SmsCount * 4;
    this.MessageLength = this.Form_AddUser.controls['Message'].value.length;

    if (this.SmsPricing > this._AccountBalance.Balance) {


      this.errorText = true
    } else {
      this.errorText = false;
    }
  }
  ShowAllLocations(): void {
    this._HelperService.OpenModal("AllGroups");
  }



  ReFormat_RequestBody(): void {
    var formValue: any = cloneDeep(this.Form_AddUser.value);
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'savecampaign',
      'AccountId': this.MerchantId,
      'AccountKey': this.Merchantkey,
      "Title": formValue.Title,
      "Message": formValue.Message,
      // "SendDate": formValue.SendDate,
      "SenderId": formValue.SenderId,
      "StatusCode": 'published',
      "TotalItem": formValue.TotalItem,
      "IsSendNow": this.IsSendNow
      //  Groups:this.GroupsList

    };

    if (this.IsSendNow == true) {

      formRequest.SendDate = moment()
    }
    else {
      formRequest.SendDate = formValue.SendDate

    }

    if (this.GroupTypeCustomer == "group") {
      formRequest.RecipientTypeCode = this.RecipientSubtype,
        formRequest.RecipientSubTypeCode = this.GroupTypeCustomer,
        formRequest.Groups = [
        ];
      for (let index = 0; index < this.GroupsList.length; index++) {
        const element = this.GroupsList[index];

        formRequest.Groups.push({
          GroupId: element.GroupId,
        });
      }

    }
    else if (this.GroupTypeCustomer == "custom") {

      if (this.CustomTypeData == 'List') {
        formRequest.RecipientTypeCode = this.RecipientSubtype,
          formRequest.RecipientSubTypeCode = this.GroupTypeCustomer,
          formRequest.Items = [
          ];
        for (let index = 0; index < this.CustomerList.length; index++) {
          const element = this.CustomerList[index];

          formRequest.Items.push({
            Name: element.DisplayName,
            MobileNumber: element.MobileNumber
          });
        }


      }
      else {
        formRequest.RecipientTypeCode = this.RecipientSubtype,
          formRequest.RecipientSubTypeCode = 'tuc',
          formRequest.Conditions = [
          ];
        formRequest.Conditions = this._HelperService.FiltersArrayFinal;
        formRequest.Condition = this.FormSeacrchCondition;
      }




    }
    else if (this.GroupTypeCustomer == "manual") {
      formRequest.RecipientTypeCode = this.RecipientSubtype,
        formRequest.RecipientSubTypeCode = this.GroupTypeCustomer,
        formRequest.Items = [
        ];
      for (let index = 0; index < this.CustomerList.length; index++) {
        const element = this.CustomerList[index];

        formRequest.Items.push({
          Name: element.DisplayName,
          MobileNumber: element.MobileNumber
        });
      }

    }
    else {
      formRequest.RecipientTypeCode = this.RecipientSubtype,
        formRequest.RecipientSubTypeCode = this.GroupTypeCustomer,
        formRequest.Items = [
        ];
      formRequest.Items = this.filelist;
      formRequest.FileName = this.NameFile;

    }

    return formRequest;

  }

  title = 'XlsRead'
  file: File;
  arrayBuffer: any;
  filelist: any;
  public NameFile: string = null;
  IsUploading = false;
  UploadCount = 0;
  FileLength: number = null
  addfile(event) {
    this.Form_AddUser.controls['TotalItem'].setValue(0);
    this.file = event.target.files[0];
    this.NameFile = this.file.name;
    this.FileLength = event.length
    let fileReader = new FileReader();
    fileReader.readAsArrayBuffer(this.file);
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      var data = new Uint8Array(this.arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      this.Form_AddUser.controls['TotalItem'].setValue(XLSX.utils.sheet_to_json(worksheet, { raw: true }).length);
      this.filelist = XLSX.utils.sheet_to_json(worksheet, { raw: true });
      if (this.Form_AddUser.controls['TotalItem'].value == 0 && this.Form_AddUser.controls['TotalItem'].value == undefined) {
        this.SelectCustomer = false;

      }
      else {
        this.SelectCustomer = true;
      }

      this.SMSPRICINGData = this.Form_AddUser.controls['TotalItem'].value * 4
      this.paybleAmount = this.SMSPRICINGData - this._AccountBalance.Balance

      if (this.SMSPRICINGData > this._AccountBalance.Balance) {


        this.errorText = true
      } else {
        this.errorText = false;
      }

    }
  }
  //#endregion

  //CustomerData
  TUTr_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
  TUTr_InvoiceRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
  TUTr_RedeemRangeMinAmount: number = this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit;
  TUTr_RedeemRangeMaxAmount: number = this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit;
  TUTr_RewardRangeMinAmount: number = this._HelperService.AppConfig.RangeRewardAmountMinimumLimit;
  TUTr_RewardRangeMaxAmount: number = this._HelperService.AppConfig.RangeRewardAmountMaximumLimit;
  public FilterData: boolean = true;
  public ResetFilterControls: boolean = true;

  public Customers_Config: OList;
  Customers_Setup() {
    this.Customers_Config = {
      Id: null,
      Sort: null,
      Task: "getcustomercount",
      Location: this._HelperService.AppConfig.NetworkLocation.V3.SMS,

      Title: "All Customers",
      StatusType: "default",
      IsDownload: false,
      Type: this._HelperService.AppConfig.CustomerTypes.all,
      AccountKey: null,
      AccountId: null,
      SubReferenceId: null,
      SubReferenceKey: null,
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: " Customer Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Mobile No",
          SystemName: "MobileNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Total Spent",
          SystemName: "TotalInvoiceAmount",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Reward Earned",
          SystemName: "TucRewardAmount",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          DefaultValue: '0',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Tuc Plus Reward",
          SystemName: "TucPlusRewardAmount",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: false,
          Search: false,
          Sort: false,
          DefaultValue: '0',
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Redeem Amount",
          SystemName: "RedeemAmount",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Visits",
          SystemName: "TotalTransaction",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: 'Last Transaction',
          SystemName: "LastTransactionDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
          NavigateField: "ReferenceKey",
        }
      ]
    };
    this.Customers_Config = this._DataHelperService.List_Initialize(
      this.Customers_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Customer,
      this.Customers_Config
    );

    this.Customers_GetData();
  }

  Customers_Filter(event: any, Type: any) {




    if (event != null) {
      for (let index = 0; index < this.Customers_Config.Sort.SortOptions.length; index++) {
        const element = this.Customers_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.Customers_Config


    );

    this.Customers_Config = this._DataHelperService.List_Operations(
      this.Customers_Config,
      event,
      Type
    );
    this.Customers_GetData();



  }

  Customers_ToggleOption(event: any, Type: any) {
    if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {

      event.data = {
        SalesMin: this.TUTr_InvoiceRangeMinAmount,
        SalesMax: this.TUTr_InvoiceRangeMaxAmount,
        RewardMin: this.TUTr_RewardRangeMinAmount,
        RewardMax: this.TUTr_RewardRangeMaxAmount,
        RedeemMin: this.TUTr_RedeemRangeMinAmount,
        RedeemMax: this.TUTr_RedeemRangeMaxAmount
      }

      if (this.TUTr_InvoiceRangeMinAmount == null || this.TUTr_InvoiceRangeMaxAmount == null || this.TUTr_RewardRangeMinAmount == null || this.TUTr_RewardRangeMaxAmount == null || this.TUTr_RedeemRangeMinAmount == null || this.TUTr_RedeemRangeMaxAmount == null) {
        this.FilterData = false;
      } else {
        this.FilterData = true;

      }
    }

    if (event != null) {
      for (let index = 0; index < this.Customers_Config.Sort.SortOptions.length; index++) {
        const element = this.Customers_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.Customers_Config


    );

    this.Customers_Config = this._DataHelperService.List_Operations(
      this.Customers_Config,
      event,
      Type
    );


    if (
      (this.Customers_Config.RefreshData == true) && this._HelperService.DataReloadEligibility(Type)) {
      this.Customers_GetData();
    }


  }

  timeout = null;
  Customers_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.Customers_Config.Sort.SortOptions.length; index++) {
          const element = this.Customers_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.Customers_Config


      );

      this.Customers_Config = this._DataHelperService.List_Operations(
        this.Customers_Config,
        event,
        Type
      );

      if (
        (this.Customers_Config.RefreshData == true) && this._HelperService.DataReloadEligibility(Type)) {
        this.Customers_GetData();
      }
      else {
        // this._HelperService.MakeFilterSnapPermanent();

      }
    }, this._HelperService.AppConfig.SearchInputDelay);

  }
  public FormSeacrchCondition: any = null
  public TotalRecordData: any = null;
  public TotalItemV: boolean = true
  Customers_GetData() {
    if (this.SelectedMerchant) {
      this.Customers_Config.AccountId = this.SelectedMerchant.ReferenceId,
        this.Customers_Config.AccountKey = this.SelectedMerchant.ReferenceKey

    }
    var TConfig = this._DataHelperService.List_GetData(
      this.Customers_Config
    );
    this.Customers_Config = TConfig;
    console.log("this.Customers_Config", this.Customers_Config);
    this.FormSeacrchCondition = this.Customers_Config.SearchCondition;
    // this.AllCustomers = this.Customers_Config.TotalRecords
    // this.Form_AddUser.controls['TotalItem'].setValue(this.AllCustomers);
    // this.SMSPRICINGData = this.AllCustomers * 4

  }


  SetOtherFilters(): void {
    this.Customers_Config.SearchBaseConditions = [];
    // this.Configurations_Config.SearchBaseCondition = null;
    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));

  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.Customers_Config);
    //#region setOtherFilters
    this.SetOtherFilters();
    this.SetSalesRanges();
    //#endregion
    this.Customers_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.Customers_Config);
    this.SetOtherFilters();
    this.SetSalesRanges();
    this.Customers_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Customer
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Customer
        );
        this._FilterHelperService.SetMerchantConfig(this.Customers_Config);
        this.Customers_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }
  SetSalesRanges(): void {
    this.TUTr_InvoiceRangeMinAmount = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
    this.TUTr_InvoiceRangeMaxAmount = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
    this.TUTr_RewardRangeMinAmount = this._HelperService.AppConfig.RangeRewardAmountMinimumLimit;
    this.TUTr_RewardRangeMaxAmount = this._HelperService.AppConfig.RangeRewardAmountMaximumLimit;
    this.TUTr_RedeemRangeMinAmount = this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit;
    this.TUTr_RedeemRangeMaxAmount = this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit;
  }
  SetSearchRanges(): void {
    //#region Invoice 
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalInvoiceAmount', this.Customers_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalInvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
    if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }

    //#endregion      

    //#region reward 
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalRewardAmount', this.Customers_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalRewardAmount', this.TUTr_RewardRangeMinAmount, this.TUTr_RewardRangeMaxAmount);
    if (this.TUTr_RewardRangeMinAmount == this._HelperService.AppConfig.RangeRewardAmountMinimumLimit && this.TUTr_RewardRangeMaxAmount == this._HelperService.AppConfig.RangeRewardAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }

    //#endregion

    //Redeem
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalRedeemAmount', this.Customers_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalRedeemAmount', this.TUTr_RedeemRangeMinAmount, this.TUTr_RedeemRangeMaxAmount);
    if (this.TUTr_RedeemRangeMinAmount == this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit && this.TUTr_RedeemRangeMaxAmount == this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }
    //endregion
  }

  // FiltersArray = []
  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this.SetSearchRanges();
    this._HelperService.MakeFilterSnapPermanent();
    this.Customers_GetData();


    if (ButtonType == 'Sort') {
      $("#Customers_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#Customers_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.Customers_Config);
    this.SetOtherFilters();
    this.SetSalesRanges();
    this.Customers_GetData();
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    this.Customers_GetData();
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }
  public GetOwners_Option: Select2Options;
  public GetOwners_Transport: any;
  public SelectedOwner: any = {};
  GetOwner_List() {
    var PlaceHolder = "Select Merchant";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },

        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: 'StatusCode',
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: '=',
          SearchValue: this._HelperService.AppConfig.Status.Active,
        }
      ]
    }

    this.GetOwners_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetOwners_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetOwners_Transport,
      multiple: false,
    };
  }
  MerchantId: number = null;
  Merchantkey: string = null;
  public ToggleMerchantSelect: boolean = true;

  GetOwners_ListChange(event: any) {
    this.SelectedMerchant = event.data[0];
    this.Form_AddUser.patchValue(
      {
        AccountKey: event.data[0].ReferenceKey,
        AccountId: event.data[0].ReferenceId
      }
    );

    this.ToggleMerchantSelect = false;
    this.GetGroups_List();

    this.MerchantId = event.data[0].ReferenceId;
    this.Merchantkey = event.data[0].ReferenceKey;
    setTimeout(() => {
      this.ToggleMerchantSelect = true;
      this.GetGroups_List();
      this.GetCustomer_List();
      this.Customers_GetData();
    }, 500);




  }




}

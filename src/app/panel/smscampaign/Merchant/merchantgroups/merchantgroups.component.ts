import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;
import * as XLSX from 'xlsx';

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Subscription, Observable } from 'rxjs';
import { ChangeContext } from "ng5-slider";

@Component({
  selector: "tu-merchantgroups",
  templateUrl: "./merchantgroups.component.html",
})
export class TUMerchantGroupsComponent implements OnInit, OnDestroy {
  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;
  public Customer = "Customers";

  public type: any = null
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
    this.type = 1;
  }
  @ViewChild("offCanvas") divView: ElementRef;
  CustomerList: any = null;
  ngOnInit() {
    this._HelperService.StopClickPropogation();
    this.type = 1;
    this.source = "tuc";
    this.CustomerList = "All Customers";
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];
      // this._HelperService.AppConfig.ActiveOwnerDisplayName = params['title'];
      if (this._HelperService.AppConfig.ActiveMerchantReferenceKey == null || this._HelperService.AppConfig.ActiveMerchantReferenceId == null) {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
      } else {
        //#region DropdownInit 
        this.Configurations_Setup();
        // this.Customers_Setup();


      }
    });


    Feather.replace();
    this.InitBackDropClickEvent();
    this.TUTr_Filter_Customer_Load();
    this.InitColConfig();
    this.Form_Add_Load();
    this.Form_Edit_Load();
    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.Configurations_GetData();
    // });

  }
  title = 'XlsRead'
  file: File;
  arrayBuffer: any;
  filelist: any;
  public NameFile: string = null;
  IsUploading = false;
  UploadCount = 0;
  addfile(event) {

    this.file = event.target.files[0];
    this.NameFile = this.file.name;
    let fileReader = new FileReader();
    fileReader.readAsArrayBuffer(this.file);
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      var data = new Uint8Array(this.arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      this.filelist = XLSX.utils.sheet_to_json(worksheet, { raw: true });
    }
  }

  StartMerchantUpload() {
    this.ResetFilterUI();
    if (this.filelist.length > 0 && this.filelist.length <= 1000) {

      this.IsUploading = true;

      // this.filelist[index].Status = "processing";
      // this.filelist[index].Message = "sending data";
      var PostItem = {
        OperationType: 'new',
        Task: this._HelperService.AppConfig.Api.Core.OnBoardCustomers,
        AccountId: null,
        AccountKey: null,
        FileName: this.NameFile,
        Data: this.filelist,
      };
      this._HelperService.IsFormProcessing = true;
      PostItem.FileName = this.NameFile;
      PostItem.AccountId = this._HelperService.AppConfig.ActiveMerchantReferenceId,
        PostItem.AccountKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.OnBoard, PostItem);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          this.UploadCount = this.UploadCount + 1;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this.filelist.Status = "success";
            this.filelist.Message = "Customer added";
            this._HelperService.NotifySuccess('Customer import successfull');
            this._HelperService.CloseModal('SampleSheet');
            // this.MerchantsList_Setup();
          }
          else {
            this.filelist.Status = "failed";
            this.filelist.Message = "Customer already present";
          }
          if (this.filelist == (this.filelist.length - 1)) {
            this.IsUploading = false;
            this._HelperService.NotifySuccess('Customer import successfull');
          }


        },
        _Error => {
          this.UploadCount = this.UploadCount + 1;
          if (this.filelist == (this.filelist.length - 1)) {
            this.IsUploading = false;
            this._HelperService.NotifySuccess('Customer import successfull');
          }
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }
    else {
      this._HelperService.NotifyError('Select file to upload merchants');
    }
  }
  AllCutomers(ReferenceData) {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelSMS.AllCustomers,
      this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      this._HelperService.AppConfig.ActiveMerchantReferenceId,
      ReferenceData.GroupKey,
      ReferenceData.GroupId,
      ReferenceData.Title,


    ]);
  }

  TUTr_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
  TUTr_InvoiceRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
  TUTr_InvoiceRange_OnChange(changeContext: ChangeContext): void {
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalInvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    this.TUTr_InvoiceRangeMinAmount = changeContext.value;
    this.TUTr_InvoiceRangeMaxAmount = changeContext.highValue;
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalInvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
    if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }
    this.Customers_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  TUTr_RedeemRangeMinAmount: number = this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit;
  TUTr_RedeemRangeMaxAmount: number = this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit;
  TUTr_RedeemRange_OnChange(changeContext: ChangeContext): void {
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'RedeemAmount', this.TUTr_RedeemRangeMinAmount, this.TUTr_RedeemRangeMaxAmount);
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    this.TUTr_RedeemRangeMinAmount = changeContext.value;
    this.TUTr_RedeemRangeMaxAmount = changeContext.highValue;
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'RedeemAmount', this.TUTr_RedeemRangeMinAmount, this.TUTr_RedeemRangeMaxAmount);
    if (this.TUTr_RedeemRangeMinAmount == this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit && this.TUTr_RedeemRangeMaxAmount == this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }
    this.Customers_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }


  TUTr_RewardRangeMinAmount: number = this._HelperService.AppConfig.RangeRewardAmountMinimumLimit;
  TUTr_RewardRangeMaxAmount: number = this._HelperService.AppConfig.RangeRewardAmountMaximumLimit;
  TUTr_RewardRange_OnChange(changeContext: ChangeContext
  ): void {
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalRewardAmount', this.TUTr_RewardRangeMinAmount, this.TUTr_RewardRangeMaxAmount);
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    this.TUTr_RewardRangeMinAmount = changeContext.value;
    this.TUTr_RewardRangeMaxAmount = changeContext.highValue;
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalRewardAmount', this.TUTr_RewardRangeMinAmount, this.TUTr_RewardRangeMaxAmount);
    if (this.TUTr_RewardRangeMinAmount == this._HelperService.AppConfig.RangeRewardAmountMinimumLimit && this.TUTr_RewardRangeMaxAmount == this._HelperService.AppConfig.RangeRewardAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }
    this.Customers_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  public Customers_Config: OList;
  Customers_Setup() {
    this.Customers_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetCustomerCount,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.SMS,

      Title: "All Customers",
      StatusType: "default",
      IsDownload: false,
      Type: this._HelperService.AppConfig.CustomerTypes.all,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      SubReferenceId: null,
      SubReferenceKey: null,
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: " Customer Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Mobile No",
          SystemName: "MobileNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Total Spent",
          SystemName: "TotalInvoiceAmount",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Reward Earned",
          SystemName: "TucRewardAmount",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          DefaultValue: '0',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Tuc Plus Reward",
          SystemName: "TucPlusRewardAmount",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: false,
          Search: false,
          Sort: false,
          DefaultValue: '0',
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Redeem Amount",
          SystemName: "RedeemAmount",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Visits",
          SystemName: "TotalTransaction",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: 'Last Transaction',
          SystemName: "LastTransactionDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
          NavigateField: "ReferenceKey",
        }
      ]
    };
    this.Customers_Config = this._DataHelperService.List_Initialize(
      this.Customers_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Customer,
      this.Customers_Config
    );

    this.Customers_GetData();
  }

  Customers_Filter(event: any, Type: any) {




    if (event != null) {
      for (let index = 0; index < this.Customers_Config.Sort.SortOptions.length; index++) {
        const element = this.Customers_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.Customers_Config


    );

    this.Customers_Config = this._DataHelperService.List_Operations(
      this.Customers_Config,
      event,
      Type
    );
    this.Customers_GetData();



  }

  Customers_ToggleOption(event: any, Type: any) {
    if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {

      event.data = {
        SalesMin: this.TUTr_InvoiceRangeMinAmount,
        SalesMax: this.TUTr_InvoiceRangeMaxAmount,
        RewardMin: this.TUTr_RewardRangeMinAmount,
        RewardMax: this.TUTr_RewardRangeMaxAmount,
        RedeemMin: this.TUTr_RedeemRangeMinAmount,
        RedeemMax: this.TUTr_RedeemRangeMaxAmount
      }
    }

    if (event != null) {
      for (let index = 0; index < this.Customers_Config.Sort.SortOptions.length; index++) {
        const element = this.Customers_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.Customers_Config


    );

    this.Customers_Config = this._DataHelperService.List_Operations(
      this.Customers_Config,
      event,
      Type
    );

    if (
      (this.Customers_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.Customers_GetData();
    }

  }

  timeout = null;
  Customers_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.Customers_Config.Sort.SortOptions.length; index++) {
          const element = this.Customers_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.Customers_Config


      );

      this.Customers_Config = this._DataHelperService.List_Operations(
        this.Customers_Config,
        event,
        Type
      );

      if (
        (this.Customers_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.Customers_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);

  }
  public FormSeacrchCondition: any = null
  public TotalRecordData: any = null;
  Customers_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.Customers_Config
    );
    this.Customers_Config = TConfig;
    this.FormSeacrchCondition = this.Customers_Config.SearchCondition;
    this.TotalRecordData = this.Customers_Config.TotalRecords + " " + "Customers"

  }
  public TUTr_Filter_Customer_Option: Select2Options;
  public TUTr_Filter_Customer_Toggle = false;
  public TUTr_Filter_Customer_Selected = 0;
  TUTr_Filter_Customer_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.getcustomers,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }

      ]
    };


    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Customer_Option = {
      placeholder: 'All Customer',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
      minimumResultsForSearch: -1
    };
  }

  TUTr_Filter_Customer_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.Configurations_Config,
      this._HelperService.AppConfig.OtherFilters.MerchantSales.Store
    );
    this.CustomerEventProcessing(event);
  }

  CustomerEventProcessing(event: any): void {
    if (event.value == this.TUTr_Filter_Customer_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Customer_Selected, '=');
      this.Configurations_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Configurations_Config.SearchBaseConditions);
      this.TUTr_Filter_Customer_Selected = 0;
    }
    else if (event.value != this.TUTr_Filter_Customer_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Customer_Selected, '=');
      this.Configurations_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Configurations_Config.SearchBaseConditions);
      this.TUTr_Filter_Customer_Selected = event.value;
      this.Configurations_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Customer_Selected, '='));
    }
    this.Configurations_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    this._HelperService.ToggleField = true;

    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 500);
  }


  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }
  }
  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  ChangeValue(ReferenceData) {

    if (this._Configuration.ReferenceKey != ReferenceData.ReferenceKey) {
      this._Configuration = ReferenceData;
    }
    this._HelperService.OpenModal('Change_Value_Content');
  }

  OpenHistory(ReferenceData) {
    if (this._Configuration.ReferenceKey != ReferenceData.ReferenceKey) {
      this._Configuration = ReferenceData;
    }

    this._HelperService.OpenModal('History_Content');
    setTimeout(() => {
      this._HelperService.StopClickPropogation();
    }, 200);

  }

  closeHistory() {
    this._HelperService.CloseModal('History_Content');

  }

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  public _Configuration: any =
    {
      Title: null,
      FileName: null,
      GroupKey: null,
      GroupId: null,
      Condition: null,
      ItemCount: null
    }

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region Configurations
  public CurrentRequest_Key: string;

  public Configurations_Config: OList;
  Configurations_Setup() {
    this.Configurations_Config = {
      Id: null,
      Sort: null,
      Task: 'getgroups',
      Location: this._HelperService.AppConfig.NetworkLocation.V3.SMS,
      Title: 'Groups',
      StatusType: 'default',
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      PageRecordLimit: 9,
      Type: this._HelperService.AppConfig.ListType.All,
      DefaultSortExpression: 'CreateDate desc',
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.HelperTypes.MerchantCategories, "="),
      TableFields: [
        {
          DisplayName: 'Name',
          SystemName: 'Name',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        }
      ]

    };
    this.Configurations_Config = this._DataHelperService.List_Initialize(
      this.Configurations_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Cashier,
      this.Configurations_Config
    );

    this.Configurations_GetData();
  }
  Configurations_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.Configurations_Config.Sort.SortOptions.length; index++) {
        const element = this.Configurations_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.Configurations_Config


    );

    this.Configurations_Config = this._DataHelperService.List_Operations(
      this.Configurations_Config,
      event,
      Type
    );

    if (
      (this.Configurations_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.Configurations_GetData();
    }

  }
  Configurations_GetData() {

    var TConfig = this._DataHelperService.List_GetData(
      this.Configurations_Config
    );
    this.Configurations_Config = TConfig;
  }
  Configurations_RowSelected(ReferenceData) {

    if (this._Configuration.ReferenceKey != ReferenceData.ReferenceKey) {
      this._Configuration = ReferenceData;
    }
    // this._HelperService.OpenModal('Form_Edit_Content');


  }
  public FilterConditions = []
  public ITEMS = []

  TList_GetDetails(ReferenceData) {
    var pData = {
      Task: 'getgroup',
      GroupKey: ReferenceData.GroupKey,
      GroupId: ReferenceData.GroupId,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._Configuration = _Response.Result;
          if (this._Configuration.Conditions) {
            this.FilterConditions = this._Configuration.Conditions;

          }
          if (this._Configuration.Items) {
            this.ITEMS = this._Configuration.Items;

          }
          this._HelperService.OpenModal('Form_Edit_Content');

          // this.TListVChange_Setup();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      });
  }

  SetOtherFilters(): void {
    this.Configurations_Config.SearchBaseConditions = [];
    this.Configurations_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));

  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.Customers_Config);
    //#region setOtherFilters
    this.SetOtherFilters();
    this.SetSalesRanges();
    //#endregion
    this.Customers_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.Customers_Config);
    this.SetOtherFilters();
    this.SetSalesRanges();
    this.Customers_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Customer
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Customer
        );
        this._FilterHelperService.SetMerchantConfig(this.Customers_Config);
        this.Customers_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }
  SetSalesRanges(): void {
    this.TUTr_InvoiceRangeMinAmount = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
    this.TUTr_InvoiceRangeMaxAmount = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
    this.TUTr_RewardRangeMinAmount = this._HelperService.AppConfig.RangeRewardAmountMinimumLimit;
    this.TUTr_RewardRangeMaxAmount = this._HelperService.AppConfig.RangeRewardAmountMaximumLimit;
    this.TUTr_RedeemRangeMinAmount = this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit;
    this.TUTr_RedeemRangeMaxAmount = this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit;
  }
  SetSearchRanges(): void {
    //#region Invoice 
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalInvoiceAmount', this.Customers_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalInvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
    if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }

    //#endregion      

    //#region reward 
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalRewardAmount', this.Customers_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalRewardAmount', this.TUTr_RewardRangeMinAmount, this.TUTr_RewardRangeMaxAmount);
    if (this.TUTr_RewardRangeMinAmount == this._HelperService.AppConfig.RangeRewardAmountMinimumLimit && this.TUTr_RewardRangeMaxAmount == this._HelperService.AppConfig.RangeRewardAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }

    //#endregion

    //Redeem
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalRedeemAmount', this.Customers_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalRedeemAmount', this.TUTr_RedeemRangeMinAmount, this.TUTr_RedeemRangeMaxAmount);
    if (this.TUTr_RedeemRangeMinAmount == this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit && this.TUTr_RedeemRangeMaxAmount == this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }
    //endregion
  }

  // FiltersArray = []
  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this.SetSearchRanges();
    this._HelperService.MakeFilterSnapPermanent();
    this.Customers_GetData();
    if (ButtonType == 'Sort') {
      $("#Customers_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#Customers_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.Customers_Config);
    this.SetOtherFilters();
    this.SetSalesRanges();
    this.Customers_GetData();
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  clicked() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }
  public source: string = 'tuc';

  CheckType(value) {

    this.source = value


  }

  Form_Add: FormGroup;
  Form_Add_Show() {
    // this.TListVChange_GetData();
    this._HelperService.OpenModal('Form_Add_Content');
  }
  Form_Add_Close() {
    // this.TListVChange_GetData();
    this._HelperService.CloseModal('Form_Add_Content');
  }
  Form_Add_Load() {
    this.Form_Add = this._FormBuilder.group({
      Task: 'savegroup',
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      SourceCode: null,
      Title: [null, Validators.required],
    });
  }
  Form_Add_Clear() {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_Add_Load();
  }
  Form_Add_Process(_FormValue: any) {
    this.ResetFilterUI();
    if (this.source == "tuc") {
      _FormValue.Conditions = this._HelperService.FiltersArrayFinal;
      _FormValue.Condition = this.FormSeacrchCondition;
    }
    else {
      _FormValue.Items = this.filelist;
      _FormValue.FileName = this.NameFile;
    }
    _FormValue.SourceCode = this.source;
    this._HelperService.IsFormProcessing = true;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, _FormValue);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.FiltersArrayFinal = [];
          this.FormSeacrchCondition = null;
          // this.source = null;
          _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
          this._HelperService.NotifySuccess(_Response.Message);
          this._HelperService._FileSelect_Icon_Reset();
          this.Form_Add_Clear();
          this.Configurations_GetData();
          this.Customers_Setup();
          if (_FormValue.OperationType == 'close') {
            this.Form_Add_Close();
          }
          this.Form_Add_Close();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          this._HelperService.FiltersArrayFinal = [];

        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

  Form_Edit: FormGroup;
  Form_Edit_Show() {
    // this.TListVChangeForm_Edit_GetData();
    this._HelperService.OpenModal('Form_Edit_Content');
  }
  Form_Edit_Close() {
    // this.TListVChange_GetData();
    this._HelperService.CloseModal('Form_Edit_Content');
  }
  Form_Edit_Load() {
    this.Form_Edit = this._FormBuilder.group({
      Task: 'updategroup',
      GroupKey: this._Configuration.GroupKey,
      GroupId: this._Configuration.GroupId,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      SourceCode: this._Configuration.SourceCode,
      Title: [null, Validators.required],
    });
  }
  Form_Edit_Clear() {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_Edit_Load();
    this._HelperService.CloseModal('Form_Edit_Content');
  }
  Form_Edit_Process(_FormValue: any) {

    if (this._Configuration.SourceCode == "tuc") {
      _FormValue.Conditions = this.FilterConditions;
      _FormValue.Condition = this._Configuration.Condition;
    }
    else {
      _FormValue.Items = this.ITEMS;
      _FormValue.FileName = this._Configuration.FileName;
    }

    _FormValue.GroupId = this._Configuration.GroupId;
    _FormValue.GroupKey = this._Configuration.GroupKey;
    _FormValue.SourceCode = this._Configuration.SourceCode;

    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, _FormValue);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.FiltersArrayFinal = [];
          this.FormSeacrchCondition = null;
          // this.source = null;
          _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
          this._HelperService.NotifySuccess(_Response.Message);
          this._HelperService._FileSelect_Icon_Reset();
          this.Form_Edit_Clear();
          this.Configurations_Setup();
          // this.TList_GetDetails();
          if (_FormValue.OperationType == 'close') {
            this.Form_Edit_Close();
          }
          this.Form_Edit_Close();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          this._HelperService.FiltersArrayFinal = [];

        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }


  public DeleteConfiguration(ReferenceData): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "deletegroup",
      GroupKey: ReferenceData.GroupKey,
      GroupId: ReferenceData.GroupId,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
    };
    let _OResponse: Observable<OResponse>;
    swal({
      title: "Delete Group ?",
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      // input: 'password',
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      // inputAttributes: {
      //   autocapitalize: 'off',
      //   autocorrect: 'off',
      //   maxLength: "4",
      //   minLength: "4"
      // },
    }).then((result) => {
      if (result.value) {
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, pData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.NotifySuccess("Group Deleted  Successfully ");
              this.Configurations_Setup();
            } else {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });




  }

}


export class OCoreUsage {
  public Reference: string;
  public ReferenceKey: string;
  public ReferenceId: string;
  public SystemName: string;
  public UserAccountKey: string;
  public UserAccountDisplayName: string;
  public UserAccountIconUrl: string;
  public TypeCode: string;
  public TypeName: string;
  public HelperCode: string;
  public HelperName: string;
  public ParentKey: string;
  public ParentCode: string;
  public ParentName: string;
  public SubParentKey: string;
  public SubParentCode: string;
  public SubParentName: string;
  public PlatformCharge?: any;
  public Name: string;
  public Value: string;
  public SubValue: string;
  public Description: string;
  public Data: string;
  public Sequence: number = 0;
  public Count: number = 0;
  public SubItemsCount: number = 0;
  public IconUrl: string;
  public PosterUrl: string;
  public CreateDate: Date;
  public CreatedByKey: string;
  public CreatedByDisplayName: string;
  public CreatedByIconUrl: string;
  public ModifyDate: Date;
  public ModifyByKey: string;
  public ModifyByDisplayName: string;
  public ModifyByIconUrl: string;
  public StatusId: number = 0;
  public StatusCode: string;
  public StatusName: string;
  public StatusI: string;
  public CreateDateS: string;
  public ModifyDateS: string;
}
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { MainPipe } from '../../../../service/main-pipe.module'
import { TimepickerModule } from 'ngx-bootstrap';

import {TUAllSmsCampaignComponent   } from "./tuallsmscampaign.component";
import { DynamicRoutesguardGuard } from "src/app/service/guard/dynamicroutes.guard";

const routes: Routes = [{ path: "",canActivateChild:[DynamicRoutesguardGuard],data:{accessName:['maodsmscampaign','smsmerchantallcampaigns']} , component: TUAllSmsCampaignComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUAllSMSCampaignRoutingModule { }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TUAllSMSCampaignRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    MainPipe,
    TimepickerModule.forRoot()
  ],
  declarations: [TUAllSmsCampaignComponent]
})
export class TUAllSMSCampaignModule { }

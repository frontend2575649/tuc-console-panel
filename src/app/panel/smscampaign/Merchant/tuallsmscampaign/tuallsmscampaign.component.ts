import { ChangeDetectorRef, Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;
declare var moment: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Observable, Subscription } from 'rxjs';
import { ChangeContext } from 'ng5-slider';
declare var moment: any;

@Component({
  selector: "tu-tuallsmscampaign",
  templateUrl: "./tuallsmscampaign.component.html",
})
export class TUAllSmsCampaignComponent implements OnInit, OnDestroy {

  FlashTimings: any = {
    Start: new Date(),
    End: new Date()
  }
  DealTimings: any = {
    Start: new Date(),
    End: new Date()
  }

  public showFlashDeals: boolean = true;
  public ResetFilterControls: boolean = true;
  UpdateStatusArray: any = ['deal.draft', 'deal.approvalpending', 'deal.approved', 'deal.published', 'deal.paused', 'deal.expired',];
  public _ObjectSubscription: Subscription = null;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;

  }

  MerchantId = null;
  MerchantKey = null;

  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();
    this.ListType = 3;
    this.TodayStartTime = this._HelperService.AppConfig.DefaultStartTimeAll;
    this.TodayEndTime = this._HelperService.AppConfig.DefaultEndTimeToday;
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];
      this.MerchantId = this._HelperService.AppConfig.ActiveMerchantReferenceId;
      this.MerchantKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;
      this.CampaignList_Setup();
      this.CampaignList_Filter_Owners_Load();
      this.InitColConfig();
      this.GetSalesOverview();

    });

    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.CampaignList_GetData();
    // });
    // this._HelperService.StopClickPropogation();
  }

  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {
    }
  }


  public _GetoverviewSummary: any = {};
  public TodayStartTime = null;
  public TodayEndTime = null;
  private pData = {
    Task: 'getcampaignoverview',
    StartDate: this._HelperService.DateInUTC(this._HelperService.AppConfig.DefaultStartTimeAll),
    EndDate: this._HelperService.DateInUTC(this._HelperService.AppConfig.DefaultEndTime),
    AccountId: this.MerchantId,
    AccountKey: this.MerchantKey,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetSalesOverview() {

    this._HelperService.IsFormProcessing = true;
    this.pData.AccountId = this.MerchantId
    this.pData.AccountKey = this.MerchantKey
    this.pData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetoverviewSummary = _Response.Result as any;
          // this.Piedata[0] = this._GetoverviewSummary.Total;

          this._ChangeDetectorRef.detectChanges();


          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  AddNewDeal() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelSMS.AddCampaign]);
  }

  //#endregion

  //#region Deallist
  public ListType: number;
  public CampaignList_Config: OList;
  CampaignList_Setup() {
    this.CampaignList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetCampaigns,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.SMS,
      Title: "Available Campaigns",
      StatusType: "Campaign",
      DefaultSortExpression: "CreateDate desc",
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'AccountId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '=='),
      TableFields: [


        {
          DisplayName: "SMS Bugget",
          SystemName: "ItemCount",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Message",
          SystemName: "Message",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: 'Send Date',
          SystemName: "SendDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },


        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.CampaignList_Config.ListType = this.ListType;
    // this.CampaignList_Config.SearchBaseCondition = "";

    if (this.CampaignList_Config.ListType == 1) //  draft
    {
      this.CampaignList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.CampaignList_Config.SearchBaseCondition, "StatusId", 'number', '712', "=");
    }
    else if (this.CampaignList_Config.ListType == 2) //  sent
    {
      this.CampaignList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.CampaignList_Config.SearchBaseCondition, "StatusId", 'number', '722', "=");
    }
    else if (this.CampaignList_Config.ListType == 3) // running
    {
      this.CampaignList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.CampaignList_Config.SearchBaseCondition, "StatusId", 'number', '718', "=");
    }
    else if (this.CampaignList_Config.ListType == 4) // upcoming
    {
      this.CampaignList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.CampaignList_Config.SearchBaseCondition, "StatusId", 'number', '717', "=");
    }
    else if (this.CampaignList_Config.ListType == 5) // approval
    {
      this.CampaignList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.CampaignList_Config.SearchBaseCondition, "StatusId", 'number', '713', "=");
    }
    else if (this.CampaignList_Config.ListType == 6) // rejected
    {
      this.CampaignList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.CampaignList_Config.SearchBaseCondition, "StatusId", 'number', '716', "=");
    }
    else {
      this.CampaignList_Config.DefaultSortExpression = 'CreateDate desc';
    }

    this.CampaignList_Config = this._DataHelperService.List_Initialize(
      this.CampaignList_Config
    );

    // this.CampaignList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.CampaignList_Config.SearchBaseCondition, "DealerKey", 'text', this._HelperService.AppConfig.ActiveOwnerKey, "=");
    this.CampaignList_GetData();

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Deals,
      this.CampaignList_Config
    );

    this.CampaignList_GetData();
  }
  CampaignList_ToggleOption(event: any, Type: any) {
    if (Type == "date") {
      this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
      this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
  }
    if (event != null) {
      for (let index = 0; index < this.CampaignList_Config.Sort.SortOptions.length; index++) {
        const element = this.CampaignList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.CampaignList_Config
    );

    this.CampaignList_Config = this._DataHelperService.List_Operations(
      this.CampaignList_Config,
      event,
      Type
    );

    if (
      (this.CampaignList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.CampaignList_GetData();
    }

  }
  timeout = null;
  CampaignList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.CampaignList_Config.Sort.SortOptions.length; index++) {
          const element = this.CampaignList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.CampaignList_Config
      );

      this.CampaignList_Config = this._DataHelperService.List_Operations(
        this.CampaignList_Config,
        event,
        Type
      );

      if (
        (this.CampaignList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.CampaignList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);
  }
  CampaignList_GetData() {
    var TConfig = this._DataHelperService.List_GetDataTur(
      this.CampaignList_Config
    );
    this.CampaignList_Config = TConfig;
  }
  CampaignList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveDeal,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,

      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
    this._HelperService.AppConfig.ActiveAccountKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;
    this._HelperService.AppConfig.ActiveAccountId = this._HelperService.AppConfig.ActiveMerchantReferenceId;


    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelSMS.CampaignDetail,
      this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      this._HelperService.AppConfig.ActiveMerchantReferenceId,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,

    ]);


  }

  CampaignList_RowDetail(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveCampaign,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
        AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
        DisplayName: ReferenceData.DisplayName,

      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
    this._HelperService.AppConfig.ActiveAccountKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;
    this._HelperService.AppConfig.ActiveAccountId = this._HelperService.AppConfig.ActiveMerchantReferenceId;


    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelSMS.SmsCampaign,
      this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      this._HelperService.AppConfig.ActiveMerchantReferenceId,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,

    ]);


  }



  //#endregion
  SelectStatusApprovalpending: boolean = false
  SelectStatusDraft: boolean = false
  SelectStatusRunning: boolean = true
  SelectStatusPaused: boolean = false
  SelectStatusUpcoming: boolean = false
  SelectStatusExpired: boolean = false


  DealerList_ListTypeChange(Type) {
    this.ListType = Type;
    this.CampaignList_Config.ListType = this.ListType;
    this.CampaignList_Config.SearchBaseCondition = "";
    if (this.CampaignList_Config.ListType == 1) //  draft
    {
      this.CampaignList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.CampaignList_Config.SearchBaseCondition, "StatusId", 'number', '712', "=");
      this.SelectStatusApprovalpending = true;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;


    }
    else if (this.CampaignList_Config.ListType == 2) //  sent
    {
      this.CampaignList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.CampaignList_Config.SearchBaseCondition, "StatusId", 'number', '722', "=");
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = true;
      this.SelectStatusExpired = false;


    }
    else if (this.CampaignList_Config.ListType == 3) // running
    {
      this.CampaignList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.CampaignList_Config.SearchBaseCondition, "StatusId", 'number', '718', "=");
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = true;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;

    }
    else if (this.CampaignList_Config.ListType == 4) // Upcoming
    {
      this.CampaignList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.CampaignList_Config.SearchBaseCondition, "StatusId", 'number', '717', "=");
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = true;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }
    else if (this.CampaignList_Config.ListType == 5) // draft
    {
      this.CampaignList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.CampaignList_Config.SearchBaseCondition, "StatusId", 'number', '713', "=");
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = true;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }
    else if (this.CampaignList_Config.ListType == 6) // expired
    {
      this.CampaignList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.CampaignList_Config.SearchBaseCondition, "StatusId", 'number', '716', "=");
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = true;
    }
    else {
      this.CampaignList_Config.DefaultSortExpression = 'CreateDate desc';
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }
    this.CampaignList_Setup();
  }
  //#region OwnerFilter

  public CampaignList_Filter_Owners_Option: Select2Options;
  public CampaignList_Filter_Owners_Selected = null;
  CampaignList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant,
    //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     this._HelperService.AppConfig.AccountType.PosAccount
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.CampaignList_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  CampaignList_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.CampaignList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.CampaignList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.CampaignList_Filter_Owners_Selected,
        "="
      );
      this.CampaignList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.CampaignList_Config.SearchBaseConditions
      );
      this.CampaignList_Filter_Owners_Selected = null;
    } else if (event.value != this.CampaignList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.CampaignList_Filter_Owners_Selected,
        "="
      );
      this.CampaignList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.CampaignList_Config.SearchBaseConditions
      );
      this.CampaignList_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.CampaignList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.CampaignList_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.CampaignList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  // SetSalesRanges(): void {
  //   this.Deal_AvailableRangeMinAmount = this._HelperService.AppConfig.DealMinimumLimit;
  //   this.Deal_AvailableRangeMaxAmount = this._HelperService.AppConfig.DealMaximumLimit;
  //   this.Deal_SoldRangeMinAmount = this._HelperService.AppConfig.DealPurchaseMinimumLimit;
  //   this.Deal_SoldRangeMaxAmount = this._HelperService.AppConfig.DealPurchaseMaximumLimit;
  // }


  SetOtherFilters(): void {
    this.CampaignList_Config.SearchBaseConditions = [];
    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.CampaignList_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.CampaignList_Config);
    //#region setOtherFilters
    this.SetOtherFilters();
    // this.SetSalesRanges();
    //#endregion
    this.CampaignList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.CampaignList_Config);

    if (Type == 'Time') {
      this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
      this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
  }
    this.SetOtherFilters();
    this.CampaignList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();
        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Deals
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }
  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Deals
        );
        this._FilterHelperService.SetMerchantConfig(this.CampaignList_Config);
        this.CampaignList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }
  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    // this.SetSearchRanges();
    this._HelperService.MakeFilterSnapPermanent();
    this.CampaignList_GetData();

    if (ButtonType == 'Sort') {
      $("#CampaignList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#CampaignList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }
  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.CampaignList_Config);
    this.SetOtherFilters();
    // this.SetSalesRanges();
    this.CampaignList_GetData();
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#region range selectors 





  //#endregion

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.CampaignList_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  CloseRowModal(index: number): void {
    $("#SubAccountsList_rdropdown_" + index).dropdown('toggle');
  }


  flashOptionCurrent: string = 'false';
  FlashOptionSelected: any;
  FlashOptions: any = [
    {
      id: 0,
      text: 'All',
      code: 'all'
    },
    {
      id: 1,
      text: 'Flash',
      code: 'flash'
    }
  ];
  FlashList_ToggleOption(): void {
    var ev: any = {
      target: 'IsFlashDeal',
      value: this.flashOptionCurrent
    };
    this.DealList_Filter_Flash_Change(ev);
  }

  DealList_Filter_Flash_Change(event: any) {
    if (event.value == this.FlashOptionSelected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'IsFlashDeal', this._HelperService.AppConfig.DataType.Text, this.FlashOptionSelected, '=');
      this.CampaignList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.CampaignList_Config.SearchBaseConditions);
      this.FlashOptionSelected = 0;
    }
    else if (event.value != this.FlashOptionSelected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'IsFlashDeal', this._HelperService.AppConfig.DataType.Text, this.FlashOptionSelected, '=');
      this.CampaignList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.CampaignList_Config.SearchBaseConditions);
      this.FlashOptionSelected = event.value;
      this.CampaignList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'IsFlashDeal', this._HelperService.AppConfig.DataType.Text, this.FlashOptionSelected, '='));
    }
    this.CampaignList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }
  public _UserAccount: any =
    {
      MerchantDisplayName: null,
      SecondaryEmailAddress: null,
      BankDisplayName: null,
      BankKey: null,
      OwnerName: null,
      SubOwnerAddress: null,
      SubOwnerLatitude: null,
      SubOwnerDisplayName: null,
      SubOwnerKey: null,
      SubOwnerLongitude: null,
      AccessPin: null,
      LastLoginDateS: null,
      AppKey: null,
      AppName: null,
      AppVersionKey: null,
      CreateDate: null,
      CreateDateS: null,
      CreatedByDisplayName: null,
      CreatedByIconUrl: null,
      CreatedByKey: null,
      Description: null,
      IconUrl: null,
      ModifyByDisplayName: null,
      ModifyByIconUrl: null,
      ModifyByKey: null,
      ModifyDate: null,
      ModifyDateS: null,
      PosterUrl: null,
      ReferenceKey: null,
      StatusCode: null,
      StatusI: null,
      StatusId: null,
      StatusName: null,
      AccountCode: null,
      AccountOperationTypeCode: null,
      AccountOperationTypeName: null,
      AccountTypeCode: null,
      AccountTypeName: null,
      Address: null,
      AppVersionName: null,
      ApplicationStatusCode: null,
      ApplicationStatusName: null,
      AverageValue: null,
      CityAreaKey: null,
      CityAreaName: null,
      CityKey: null,
      CityName: null,
      ContactNumber: null,
      CountValue: null,
      CountryKey: null,
      CountryName: null,
      DateOfBirth: null,
      DisplayName: null,
      EmailAddress: null,
      EmailVerificationStatus: null,
      EmailVerificationStatusDate: null,
      FirstName: null,
      GenderCode: null,
      GenderName: null,
      LastLoginDate: null,
      LastName: null,
      Latitude: null,
      Longitude: null,
      MobileNumber: null,
      Name: null,
      NumberVerificationStatus: null,
      NumberVerificationStatusDate: null,
      OwnerDisplayName: null,
      OwnerKey: null,
      Password: null,
      Reference: null,
      ReferralCode: null,
      ReferralUrl: null,
      RegionAreaKey: null,
      RegionAreaName: null,
      RegionKey: null,
      RegionName: null,
      RegistrationSourceCode: null,
      RegistrationSourceName: null,
      RequestKey: null,
      RoleKey: null,
      RoleName: null,
      SecondaryPassword: null,
      SystemPassword: null,
      UserName: null,
      WebsiteUrl: null,

      StateKey: null,
      StateName: null

    }


  //#region Run Time 



  //#endregion



  //#endregion

  //#region Stock Update 


  //#endregion

  DuplicateCampaign(ReferenceData): void {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DuplicateCampaign,
      // text: this._HelperService.AppConfig.CommonResource.DeleteDealHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PData =
        {
          Task: 'DuplicateCampaign',
          ReferenceId: ReferenceData.ReferenceId,
          ReferenceKey: ReferenceData.ReferenceKey,
          AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
          AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
          // StatusCode: this.selectedStatusItem.statusCode,

        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, PData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Campaign Created successfully");
              this.CampaignList_Setup();
              this.GetSalesOverview();
              this._HelperService.IsFormProcessing = false;
              this._HelperService.CloseModal('exampleModal')
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });

      }
    });




  }
  DeleteCampaign(ReferenceData): void {
    swal({
      position: "center",
      title: "Delete Campaign ?",
      text: "Click continue to delete campaign",
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PData =
        {
          Task: 'deletecampaign',
          ReferenceId: ReferenceData.ReferenceId,
          ReferenceKey: ReferenceData.ReferenceKey,
          AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
          AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
          // StatusCode: this.selectedStatusItem.statusCode,
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, PData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Campaign Deleted successfully");
              this.CampaignList_Setup();
              this.GetSalesOverview();
              this._HelperService.IsFormProcessing = false;
              this._HelperService.CloseModal('exampleModal')
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });

      }
    });




  }
  StatusUpdate(ReferenceData, status, message): void {
    swal({
      position: "center",
      title: message,
      text: this._HelperService.AppConfig.CommonResource.UpdateTitle,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PData =
        {
          Task: 'updatecampaignstatus',
          ReferenceId: ReferenceData.ReferenceId,
          ReferenceKey: ReferenceData.ReferenceKey,
          AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
          AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
          StatusCode: status,
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, PData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Campaign Deleted successfully");
              this.CampaignList_Setup();
              this.GetSalesOverview();
              this._HelperService.IsFormProcessing = false;
              this._HelperService.CloseModal('exampleModal')
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });

      }
    });




  }

}


// CampaignList_Config
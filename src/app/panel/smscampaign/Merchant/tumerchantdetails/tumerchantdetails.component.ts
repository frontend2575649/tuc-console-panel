import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { DataHelperService, FilterHelperService, HelperService, OResponse, OSelect, OUserDetails } from "../../../../service/service";
import { Observable, Subscription } from 'rxjs';
declare let $: any;
declare var d3: any;
import swal from 'sweetalert2';
import * as Feather from 'feather-icons';

@Component({
  selector: "tu-merchantdetails",
  templateUrl: "./tumerchantdetails.component.html"
})
export class TUMerchantDetailsComponent implements OnInit {
  VarAccordion: boolean = false;
  VarAccordion1: boolean = false;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
  }
  public _ObjectSubscription1: Subscription = null;
  public _Roles = [
    {
      'id': 0,
      'text': 'Select Role',
      'apival': 'Select Role'
    },
    {
      'id': 10,
      'text': 'Admin',
      'apival': 'merchantadmin'
    },
    {
      'id': 11,
      'text': 'Store Manager',
      'apival': 'merchantstoremanager'
    },

  ];
  ngOnInit() {
  
    Feather.replace();
    this._HelperService.ValidateData();
    // this._ActivatedRoute.params.subscribe((params: Params) => {

    //   this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
    //   this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];

    //   this.GetMerchantDetails();
    //   this.Get_UserAccountDetails();

    // });
    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveMerchant);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveMerchantReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    this.GetMerchantDetails();
      this.Get_UserAccountDetails();
    }
    else{ 
      this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound])
      this._HelperService.DeleteStorage('templocmerchant');

    }
  }
  ngOnDestroy(): void {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
    this._HelperService.showAddNewCashierBtn = false;

    try {
      this._ObjectSubscription1.unsubscribe();
    } catch (error) {

    }
  }

  public Get_UserAccountDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccount,
      Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveMerchantReferenceKey, '='),
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService._UserAccount = _Response.Result as OUserDetails;
          this._HelperService._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(this._HelperService._UserAccount.CreateDate);
          this._HelperService._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(this._HelperService._UserAccount.ModifyDate);
          this._HelperService._UserAccount.StatusI = this._HelperService.GetStatusIcon(this._HelperService._UserAccount.StatusCode);
          this._HelperService.IsFormProcessing = false;
          //#endregion
        }
        else {
          this._HelperService.IsFormProcessing = false;

          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {

        this._HelperService.HandleException(_Error);
      });
  }

  public _MerchantDetails: any =
    {
      "ReferenceId": null,
      "ReferenceKey": null,
      "DisplayName": null,
      "EmailAddress": null,
      "IconUrl": null,
      "AccountCode": null,
      "WebsiteUrl": null,
      "Address": {
        "Address": null,
        "Latitude": null,
        "Longitude": null
      },
      "ContactPerson": {
        "MobileNumber": null,
        "EmailAddress": null
      },
      "Categories": [
        {
          "ReferenceId": null,
          "ReferenceKey": null,
          "Name": null,
          "IconUrl": null
        }
      ],
      "Stores": null,
      "Terminals": null,
      "Cashiers": null,
      "CreateDate": null,
      "StatusCode": null,
      "StatusName": null

    }
  public _UserAccount: OUserDetails = {
    ContactNumber: null,
    SecondaryEmailAddress: null,
    ReferenceId: null,
    BankDisplayName: null,
    BankKey: null,
    SubOwnerAddress: null,
    SubOwnerLatitude: null,
    SubOwnerDisplayName: null,
    SubOwnerKey: null,
    SubOwnerLongitude: null,
    AccessPin: null,
    LastLoginDateS: null,
    AppKey: null,
    AppName: null,
    AppVersionKey: null,
    CreateDate: null,
    CreateDateS: null,
    CreatedByDisplayName: null,
    CreatedByIconUrl: null,
    CreatedByKey: null,
    Description: null,
    IconUrl: null,
    ModifyByDisplayName: null,
    ModifyByIconUrl: null,
    ModifyByKey: null,
    ModifyDate: null,
    ModifyDateS: null,
    PosterUrl: null,
    ReferenceKey: null,
    StatusCode: null,
    StatusI: null,
    StatusId: null,
    StatusName: null,
    AccountCode: null,
    AccountOperationTypeCode: null,
    AccountOperationTypeName: null,
    AccountTypeCode: null,
    AccountTypeName: null,
    Address: null,
    AppVersionName: null,
    ApplicationStatusCode: null,
    ApplicationStatusName: null,
    AverageValue: null,
    CityAreaKey: null,
    CityAreaName: null,
    CityKey: null,
    CityName: null,
    CountValue: null,
    CountryKey: null,
    CountryName: null,
    DateOfBirth: null,
    DisplayName: null,
    EmailAddress: null,
    EmailVerificationStatus: null,
    EmailVerificationStatusDate: null,
    FirstName: null,
    GenderCode: null,
    GenderName: null,
    LastLoginDate: null,
    LastName: null,
    Latitude: null,
    Longitude: null,
    MobileNumber: null,
    Name: null,
    NumberVerificationStatus: null,
    NumberVerificationStatusDate: null,
    OwnerDisplayName: null,
    OwnerKey: null,
    Password: null,
    Reference: null,
    ReferralCode: null,
    ReferralUrl: null,
    RegionAreaKey: null,
    RegionAreaName: null,
    RegionKey: null,
    RegionName: null,
    RegistrationSourceCode: null,
    RegistrationSourceName: null,
    RequestKey: null,
    RoleKey: null,
    RoleName: null,
    SecondaryPassword: null,
    SystemPassword: null,
    UserName: null,
    WebsiteUrl: null
  };
  public _Address: any = {};
  public _ContactPerson: any = {};
  public _Overview: any = {};
  GetMerchantDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchant,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._MerchantDetails = _Response.Result;
          this._Address = this._MerchantDetails.Address;
          this._ContactPerson = this._MerchantDetails.ContactPerson;
          this._MerchantDetails['ContactPerson'] = {};
          //#region RelocateMarker 

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          // this.FormC_EditUser_Latitude = this._MerchantDetails.Latitude;
          // this.FormC_EditUser_Longitude = this._MerchantDetails.Longitude;
          // this.FormC_EditUser_Latitude = _Response.Result.Latitude;
          // this.FormC_EditUser_Longitude = _Response.Result.Longitude;

          //#endregion

          //#region DatesAndStatusInit 

          this._MerchantDetails.StartDateS = this._HelperService.GetDateS(
            this._MerchantDetails.StartDate
          );
          this._MerchantDetails.EndDateS = this._HelperService.GetDateS(
            this._MerchantDetails.EndDate
          );
          this._MerchantDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._MerchantDetails.CreateDate
          );
          this._MerchantDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._MerchantDetails.ModifyDate
          );
          this._MerchantDetails.StatusI = this._HelperService.GetStatusIcon(
            this._MerchantDetails.StatusCode
          );
          this._MerchantDetails.StatusB = this._HelperService.GetStatusBadge(
            this._MerchantDetails.StatusCode
          );
          this._MerchantDetails.StatusC = this._HelperService.GetStatusColor(
            this._MerchantDetails.StatusCode
          );


          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  CashierRoute() {

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.Cashiers,
      this._MerchantDetails.ReferenceKey,
      this._MerchantDetails.ReferenceId

    ]);
  }

  StoreRoute() {

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Merchant.Stores,
      this._MerchantDetails.ReferenceKey,
      this._MerchantDetails.ReferenceId


    ]);

  }

  TerminalRoute() {

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Merchant.Terminals,
      this._MerchantDetails.ReferenceKey,
      this._MerchantDetails.ReferenceId

    ]);
  }
  EditMerchant(): void {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Profile, this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      this._HelperService.AppConfig.ActiveMerchantReferenceId
    ]);
    this._HelperService.CloseAllModal();

  }
  VarAccordion1Business1: boolean = false;

  VarAccordion1Business() {
    this.VarAccordion1Business1 = !this.VarAccordion1Business1;

  }
}
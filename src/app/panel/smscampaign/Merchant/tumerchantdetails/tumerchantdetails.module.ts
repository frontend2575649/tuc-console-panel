import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { TUMerchantDetailsComponent } from "./tumerchantdetails.component";
import { Angular4PaystackModule } from 'angular4-paystack'

const routes: Routes = [
    {
        path: "",
        component: TUMerchantDetailsComponent,
        children: [
            // { path: "", data: { permission: "customer", menuoperations: "ManageMerchant", accounttypecode: "customer" }, loadChildren: "./../merchantcredits/merchantcredits.module#TUAllCreditsModule" },
            { path: "merchantcredits/:referencekey/:referenceid", data: { permission: "customer", menuoperations: "ManageMerchant", PageName: 'System.Menu.Merchant', accounttypecode: "customer" }, loadChildren: "./../merchantcredits/merchantcredits.module#TUAllCreditsModule" },
            { path: "merchantgroups/:referencekey/:referenceid", data: { permission: "customer", menuoperations: "ManageMerchant", PageName: 'System.Menu.Merchant', accounttypecode: "customer" }, loadChildren: "./../merchantgroups/merchantgroups.module#TUMGroupModule" },
            
       
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUMerchantDRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUMerchantDRoutingModule,
        GooglePlaceModule,
        Angular4PaystackModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    declarations: [TUMerchantDetailsComponent]
})
export class TUMerchantDModule { }

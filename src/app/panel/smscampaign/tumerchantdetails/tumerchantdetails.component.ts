import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { DataHelperService, FilterHelperService, HelperService, OResponse, OSelect, OUserDetails } from "../../../service/service";
import { Observable, Subscription } from 'rxjs';
declare let $: any;
declare var d3: any;
import swal from 'sweetalert2';
import * as Feather from 'feather-icons';

@Component({
  selector: "tu-merchantdetails",
  templateUrl: "./tumerchantdetails.component.html"
})
export class TUMerchantDetailsComponent implements OnInit {
  VarAccordion: boolean = false;
  VarAccordion1: boolean = false;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
  }
  public _ObjectSubscription1: Subscription = null;
  public _Roles = [
    {
        'id': 0,
        'text': 'Select Role',
        'apival': 'Select Role'
    },
    {
        'id': 10,
        'text': 'Admin',
        'apival': 'merchantadmin'
    },
    {
      'id': 11,
      'text': 'Store Manager',
      'apival': 'merchantstoremanager'
  },
   
];
  ngOnInit() {
    
    this._HelperService.ValidateData();
    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveMerchant);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveMerchantReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
 
    }
    else{ 
      this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound])
      this._HelperService.DeleteStorage('templocmerchant');

    }
  }
  


 
}
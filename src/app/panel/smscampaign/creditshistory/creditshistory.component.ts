import { ChangeDetectorRef, Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../service/service";
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: "tu-creditshistory",
  templateUrl: "./creditshistory.component.html",
})
export class TUCreditsHistoryComponent implements OnInit, OnDestroy {
  public ResetFilterControls: boolean = true;

  public _ObjectSubscription: Subscription = null;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {


  }

  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();
    this.CreditsList_Setup();
    // this.GetBalance();

    this.CreditsList_Filter_Owners_Load();
  }

  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {
    }
  }


  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }




  public CreditsList_Config: OList;
  CreditsList_Setup() {
    this.CreditsList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetTopupHistory,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.SMS,
      Title: "Credit History",
      StatusType: "transaction",
      IsDownload: true,
      // AccountKey:  this._HelperService.AppConfig.ActiveMerchantReferenceKey ,
      // AccountId:  this._HelperService.AppConfig.ActiveMerchantReferenceId,

      DefaultSortExpression: "CreateDate desc",
      TableFields: [
        {
          DisplayName: "Name",
          SystemName: "AccountDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },



        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.CreditsList_Config = this._DataHelperService.List_Initialize(
      this.CreditsList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Stores,
      this.CreditsList_Config
    );

    this.CreditsList_GetData();
  }
  CreditsList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.CreditsList_Config.Sort.SortOptions.length; index++) {
        const element = this.CreditsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.CreditsList_Config
    );

    this.CreditsList_Config = this._DataHelperService.List_Operations(
      this.CreditsList_Config,
      event,
      Type
    );

    if (
      (this.CreditsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.CreditsList_GetData();
    }

  }
  CreditsList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.CreditsList_Config
    );
    this.CreditsList_Config = TConfig;


  }
  CreditsList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveStore,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store
    //     .SalesHistory,
    //   ReferenceData.ReferenceKey,
    //   ReferenceData.ReferenceId,
    // ]);

  }

  //#endregion

  //#region OwnerFilter

  public CreditsList_Filter_Owners_Option: Select2Options;
  public CreditsList_Filter_Owners_Selected = null;
  CreditsList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant,
    //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     this._HelperService.AppConfig.AccountType.PosAccount
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.CreditsList_Filter_Owners_Option = {
      placeholder: "Filter by Merchant",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  CreditsList_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.CreditsList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.CreditsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "AccountKey",
        this._HelperService.AppConfig.DataType.Text,
        this.CreditsList_Filter_Owners_Selected,
        "="
      );
      this.CreditsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.CreditsList_Config.SearchBaseConditions
      );
      this.CreditsList_Filter_Owners_Selected = null;
    } else if (event.value != this.CreditsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "AccountKey",
        this._HelperService.AppConfig.DataType.Text,
        this.CreditsList_Filter_Owners_Selected,
        "="
      );
      this.CreditsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.CreditsList_Config.SearchBaseConditions
      );
      this.CreditsList_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.CreditsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "AccountKey",
          this._HelperService.AppConfig.DataType.Text,
          this.CreditsList_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.CreditsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.CreditsList_Config.SearchBaseConditions = [];
    this.CreditsList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.CreditsList_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.CreditsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.CreditsList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.CreditsList_Config);

    this.SetOtherFilters();

    this.CreditsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        maxLength: "32",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Stores
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Stores
        );
        this._FilterHelperService.SetMerchantConfig(this.CreditsList_Config);
        this.CreditsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.CreditsList_GetData();

    if (ButtonType == 'Sort') {
      $("#CreditsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#CreditsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.CreditsList_Config);
    this.SetOtherFilters();

    this.CreditsList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.CreditsList_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }


  public _AccountBalance =
    {
      Credit: 0,
      Debit: 0,
      Balance: 0,
      Sms: 0
    }

  public GetBalance() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: 'getbalance',
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      Source: this._HelperService.AppConfig.TransactionSource.Merchant
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountBalance = _Response.Result;
          this._AccountBalance.Sms = Math.round(this._AccountBalance.Balance / 4)


          // this._AccountBalance.Balance = _Response.Result.Balance / 100;
          // this._AccountBalance.Credit = _Response.Result.Credit / 100;
          // this._AccountBalance.Debit = _Response.Result.Debit / 100;
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );

  }

  IsUseWallet: boolean = false;
  SmsValue: boolean = false;

  UseWallet() {
    this.IsUseWallet = !this.IsUseWallet;
  }
  Smspricing(value) {
    this._AccountBalanceCreditAmount = value * 4
    if (value.length != 0 && value != 0 && value != null && value != undefined) {
      this.SmsValue = true;
    }
    else {
      this.SmsValue = false;

    }

  }

  public _TransactionReference = null;
  public _AccountBalanceCreditAmount = 0;
  public SmsCount = 0;

  generateRandomNumber() {
    var TrRef = Math.floor(100000 + Math.random() * (999999 + 1 - 100000));
    return TrRef;
  }
  OpenPaymentOptions() {
    this._AccountBalanceCreditAmount = 0;
    var Ref = this.generateRandomNumber();
    this._TransactionReference = 'Sms' + '_' + this._HelperService.AppConfig.ActiveMerchantReferenceId + '_' + Ref;
    this._HelperService.OpenModal('Form_AddUser_Content');
  }
  paymentDone(ref: any) {
    this.TransactionId = ref.trans
    if (ref != undefined && ref != null && ref != '') {
      if (ref.status == 'success') {
        this.CreditAmount();
      }
      else {
        this._HelperService.NotifyError('Payment failed');
      }
    }
    else {
      this._HelperService.NotifyError('Payment failed');
    }
    // this.title = 'Payment successfull';
  }
  public TransactionId
  public CreditAmount() {
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "topupwallet",
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      Amount: this._AccountBalanceCreditAmount,
      PaymentReference: this._TransactionReference,
      TransactionId: this.TransactionId,
      PaymentSource: 'wallet'
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SMS, PostData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountBalanceCreditAmount = 0;
          this._TransactionReference = null;
          this._HelperService.NotifySuccess("Account credited");
          this.GetBalance();


        } else {
          this._HelperService.NotifySuccess(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  paymentCancel() {
  }

}

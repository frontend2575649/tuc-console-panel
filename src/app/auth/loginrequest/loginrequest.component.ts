import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { HelperService, OResponse } from '../../service/service';

@Component({
    selector: 'loginrequest',
    templateUrl: './loginrequest.component.html',
})
export class LoginRequestComponent implements OnInit {
    ShowPassword: boolean = true;
    _Form_Login_Processing = false;
    _Form_Login: FormGroup;
    constructor(
        public _FormBuilder: FormBuilder,
        public _TranslateService: TranslateService,
        public _Router: Router,
        public _HelperService: HelperService,
    ) {
        this._Form_Login = _FormBuilder.group({
            'username': ['', Validators.required],
            'password': ['', Validators.required],
        });

    }
    ngOnInit() {
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Account);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Device);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Location);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.OReqH);
        this._HelperService.SetNetwork();
    }
    _Form_Login_Process(value: any) {
        this._Form_Login_Processing = true;
        var pData = {
            Task: 'sysloginrequest',
            UserName: value.username,
            Password: value.password,
            PlatformCode: 'web',
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
        _OResponse.subscribe(
            _Response => {
                this._Form_Login_Processing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.LoginCheckVar = false;
                    this._HelperService.SaveStorage("acctemp", _Response.Result);
                    this._Form_Login.reset();
                    window.location.href = this._HelperService.AppConfig.Pages.ThankUCash.LoginConfirm;
                }
                else {
                    //this._HelperService.NotifyError(_Response.Message);
                    this._HelperService.NotifyError("Enter Valid UserName Or Password");
                }
            },
            _Error => {
                this._Form_Login_Processing = false;
                this._HelperService.HandleException(_Error);
            });
    }


    ToogleShowHidePassword(): void {
        this.ShowPassword = !this.ShowPassword;
    }
}

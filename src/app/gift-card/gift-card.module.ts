import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: "", redirectTo: 'overview' },
  {
    path: "overview", data: { permission: "giftcardshome", PageName: "Gift card overview" },
    loadChildren: "../merchant/giftcard/overview/tugiftcard.module#TuGiftCardModule"
  },
  {





    
    path: "issued", data: {
      permission: "giftcardstransaction", PageName: "Gift card issued",
      loadChildren: '../merchant/giftcard/giftcards/listgiftcards.module#ListgiftcardsModule'
    },
  },
  {
    path: "transactions", data: { permission: "giftcardstransaction", PageName: "Gift card transactions" },
    children: [
      { path: "", redirectTo: 'history' },
      // {
      //   path: 'rewardhistory', data: { 'permission': 'giftcardstransaction', PageName: 'Gift card reward history' },
      //   loadChildren: '../merchant/giftcard/Reward/tusale.module#TUSaleModule'
      // },
      // {
      //   path: 'redeemhistory2', data: { 'permission': 'giftcardstransaction', PageName: 'Gift card redeem history' },
      //   loadChildren: '../merchant/giftcard/Redeemed/tusale.module#TUSaleModule'
      // },
      // {
      //   path: 'balancehistory', data: { 'permission': 'giftcardstransaction', PageName: 'Gift card balance history' },
      //   loadChildren: '../merchant/giftcard/Balance/tusale.module#TUSaleModule'
      // },
      {
        path: "issuedhistory", data: {
          permission: "giftcardstransaction", PageName: "Gift card issued",
        },
        loadChildren: './giftcards/listgiftcards.module#ListgiftcardsModule'
      },
      {
        path: "history", data: {
          permission: "giftcardstransaction", PageName: "Gift card transaction history",
        },
        loadChildren: './transactions/tucgctransactions.module#TucgctransactionsModule'

      },
      {
        path: "redeemhistory", data: {
          permission: "giftcardstransaction", PageName: "Gift card transaction history",
        },
        loadChildren: './redeemindividual/tucredeemhistory.module#TucredeemhistoryModule'
      },
    ]
  },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class GiftCardModule {
}

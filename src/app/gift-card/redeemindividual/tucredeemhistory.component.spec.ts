import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TucredeemhistoryComponent } from './tucredeemhistory.component';

describe('TucredeemhistoryComponent', () => {
  let component: TucredeemhistoryComponent;
  let fixture: ComponentFixture<TucredeemhistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TucredeemhistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TucredeemhistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

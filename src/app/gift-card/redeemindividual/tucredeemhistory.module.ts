import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TucredeemhistoryComponent } from './tucredeemhistory.component';
import { DynamicRoutesguardGuard } from 'src/app/service/guard/dynamicroutes.guard';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { Select2Module } from 'ng2-select2';
import { Ng5SliderModule } from 'ng5-slider';
import { NgxPaginationModule } from 'ngx-pagination';
import { MainPipe } from 'src/app/service/main-pipe.module';

const routes: Routes = [{ path: "", canActivateChild: [DynamicRoutesguardGuard], data: { accessName: ['maodgiftcardtransaction'] }, component: TucredeemhistoryComponent }];

@NgModule({
  declarations: [TucredeemhistoryComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    Ng5SliderModule,
    MainPipe,
    RouterModule.forChild(routes)
  ]
})
export class TucredeemhistoryModule { }

import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs';
import { DataHelperService } from 'src/app/service/datahelper.service';
import { FilterHelperService } from 'src/app/service/filterhelper.service';
import { HelperService } from 'src/app/service/helper.service';
import { OList, OSelect, OResponse } from 'src/app/service/object.service';
import swal from 'sweetalert2';
declare var moment: any;
declare var $: any;

@Component({
  selector: 'app-tucredeemhistory',
  templateUrl: './tucredeemhistory.component.html',
  styleUrls: ['./tucredeemhistory.component.css']
})
export class TucredeemhistoryComponent implements OnInit {

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService: FilterHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
  ) {
    this.isMerchantGC = !this._Router.url.includes('/console/giftcards/transactions');
  }

  public ResetFilterControls: boolean = true;
  public AcquirerId = 0;
  public isMerchantGC: boolean;
  ngOnInit() {
    Feather.replace();
    this._HelperService.StopClickPropogation();
    this.TUTr_Filter_Banks_Load();
    this.TUTr_Setup();
    this.TodayStartTime = new Date(2017, 0, 1, 0, 0, 0, 0), moment();
    this.TodayEndTime = moment();
  }

  SetSearchRanges(): void {
    //#region Invoice 
    this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('Amount', this.TUTr_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
    if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
      this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
    }
    else {
      this.TUTr_Config.SearchBaseConditions.push(SearchCase);
    }

    //#endregion      

  }

  TUTr_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
  TUTr_InvoiceRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
  TUTr_InvoiceRange_OnChange(changeContext: ChangeContext): void {
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
    this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
    this.TUTr_InvoiceRangeMinAmount = changeContext.value;
    this.TUTr_InvoiceRangeMaxAmount = changeContext.highValue;
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
    if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
      this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
    }
    else {
      this.TUTr_Config.SearchBaseConditions.push(SearchCase);
    }
    this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }
  TUTr_RedeemRangeMinAmount: number = this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit;
  TUTr_RedeemRangeMaxAmount: number = this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit;
  TUTr_RedeemRange_OnChange(changeContext: ChangeContext): void {
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalAmount', this.TUTr_RedeemRangeMinAmount, this.TUTr_RedeemRangeMaxAmount);
    this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
    this.TUTr_RedeemRangeMinAmount = changeContext.value;
    this.TUTr_RedeemRangeMaxAmount = changeContext.highValue;
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalAmount', this.TUTr_RedeemRangeMinAmount, this.TUTr_RedeemRangeMaxAmount);
    if (this.TUTr_RedeemRangeMinAmount == this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit && this.TUTr_RedeemRangeMaxAmount == this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit) {
      this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
    }
    else {
      this.TUTr_Config.SearchBaseConditions.push(SearchCase);
    }
    this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }
  //#region transactions 

  public TUTr_Config: OList;
  public TuTr_Columns =
    {
      Date: true,
      User: true,
      Type: true,
      Invoice: true,
      Redeem: true,
      Merchant: true,
      DoneBy: true,
      Cashier: true,
    }
  public TuTr_Filters_List =
    {
      Customer: false,
      Type: false,
      Merchant: false,
      Date: false,
      Sort: false,
      Status: false,
      Store: false,
      Acquirer: false,
      Provider: false,
      Issuer: false,
      InvoiceAmountRange: false,
      RedeemAmountRange: false,
      Cashier: false,
    }
  TUTr_Setup() {
    this.TUTr_Config =
    {
      Id: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetReedemHistory,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.GC,
      Title: 'Redeem History',
      StatusType: 'transaction',
      // AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      // AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      // SearchBaseCondition: this.TuTr_Setup_SearchCondition(),
      Status: this._HelperService.AppConfig.StatusList.defaultitem,
      Type: this._HelperService.AppConfig.ListType.All,
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'IssueDate',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },
      TableFields: [
        {
          DisplayName: 'Date',
          SystemName: 'IssueDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Show: true,
          Search: true,
          Sort: true,
          IsDateSearchField: true,
        },
        {
          DisplayName: 'Gift Card',
          SystemName: 'CardNo',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: true,
          Sort: false,
        },
        {
          DisplayName: 'Customer',
          SystemName: 'ReceiverName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: true,
          Sort: true,
        },
        {
          DisplayName: 'Merchant Name',
          SystemName: 'MerchantName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: true,
          Sort: false,
        },
        {
          DisplayName: 'Store Name',
          SystemName: 'StoreName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: true,
          Sort: false,
        },
        {
          DisplayName: 'Amount',
          SystemName: 'Amount',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Show: true,
          Search: false,
          Sort: true,
        },
      ]
    }

    this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.GiftCardRedeems,
      this.TUTr_Config
    );
    this.TUTr_GetData();
  }

  TuTr_Setup_SearchCondition() {
    this.TuTr_Columns =
    {
      Date: true,
      User: true,
      Type: true,
      Invoice: true,
      Redeem: true,
      Merchant: true,
      DoneBy: true,
      Cashier: true,
    }
    var SearchCondition = "";
    this.TuTr_Filters_List =
    {
      Customer: true,
      Type: true,
      Merchant: false,
      Date: true,
      Sort: true,
      Status: true,
      Store: false,
      Acquirer: false,
      Provider: false,
      Issuer: false,
      InvoiceAmountRange: true,
      RedeemAmountRange: true,
      Cashier: true,
    }

    // if (this.isMerchantGC) {
    //   SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '!=');
    //   SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'ParentId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '=');
    // }

    // SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'SourceKey', this._HelperService.AppConfig.DataType.Text, 'transaction.source.giftcards', '=');
    // SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'TypeKey', this._HelperService.AppConfig.DataType.Text, 'transaction.type.giftcardredeem', '=');
    // SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'ModeKey', this._HelperService.AppConfig.DataType.Text, 'transaction.mode.debit', '=');
    return SearchCondition;
  }
  TUTr_ToggleOption_Date(event: any, Type: any) {
    this.TUTr_ToggleOption(event, Type);

  }
  TUTr_ToggleOption(event: any, Type: any) {

    if (Type == 'date') {
      event.start = this._HelperService.DateInUTC(event.start);
      event.end = this._HelperService.DateInUTC(event.end);
    }


    this.TodayStartTime = this.TUTr_Config.StartTime;
    this.TodayEndTime = this.TUTr_Config.EndTime;
    if (event != null) {
      for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
        const element = this.TUTr_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }


    if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
      event.data = {
        SalesMin: this.TUTr_InvoiceRangeMinAmount,
        SalesMax: this.TUTr_InvoiceRangeMaxAmount,
        RedeemMin: this.TUTr_RedeemRangeMinAmount,
        RedeemMax: this.TUTr_RedeemRangeMaxAmount
      }
    }

    this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);
    this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
    this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

    if (
      (this.TUTr_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TUTr_GetData();
    }
  }

  public OverviewData: any = {
    Transactions: 0,
    InvoiceAmount: 0.0
  };

  TUTr_GetData() {
    this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.GetReedemOverview);
    var TConfig = this._DataHelperService.List_GetDataTur(this.TUTr_Config);
    this.TUTr_Config = TConfig;
  }
  TUTr_RowSelected(ReferenceData) {
    var ReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
  }

  //#endregion

  TodayStartTime = null;
  TodayEndTime = null;

  public _AccountOverview: OAccountOverview =
    {
      ActiveMerchants: 0,
      ActiveMerchantsDiff: 0,
      ActiveTerminals: 0,
      Idle: 0,
      Dead: 0,
      Active: 0,
      Inactive: 0,
      ActiveTerminalsDiff: 0,
      CardRewardPurchaseAmount: 0,
      CardRewardPurchaseAmountDiff: 0,
      CashRewardPurchaseAmount: 0,
      CashRewardPurchaseAmountDiff: 0,
      Merchants: 0,
      PurchaseAmount: 0,
      PurchaseAmountDiff: 0,
      Terminals: 0,
      Transactions: 0,
      TransactionsDiff: 0,
      UnusedTerminals: 0,
      IdleTerminals: 0,
      TerminalStatus: 0,
      DeadTerminals: 0,
      Total: 0,
      TotalTransactions: 0,
      TotalSale: 0,
    }

  Data: any = {};
  GetAccountOverviewLite(): void {
    this._HelperService.IsFormProcessing = true;
    this.Data = {
      Task: 'getaccountoverview',
      StartTime: this.TodayStartTime,
      EndTime: this.TodayEndTime,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverview = _Response.Result as OAccountOverview;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();

  }

  //#region SalesOverview 


  public TUTr_Filter_Bank_Option: Select2Options;
    public TUTr_Filter_Bank_Selected = 0;




    TUTr_Filter_Banks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
                }]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Bank_Option = {
            placeholder: 'Search By Bank / Acquirer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }



    TUTr_Filter_Banks_Change(event: any) {

        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Bank
        );

        this.BanksEventProcessing(event);
    }



    BanksEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Bank_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Bank_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        
    }


  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetSearchConditionTur(ListOptions);
    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: SortExpression,
      Type: ListOptions.Type,
      ReferenceKey: ListOptions.ReferenceKey,
      StartDate: ListOptions.StartDate,
      EndDate: ListOptions.EndDate,
      ReferenceId: ListOptions.ReferenceId,
      SubReferenceId: ListOptions.SubReferenceId,
      SubReferenceKey: ListOptions.SubReferenceKey,
      AccountId: ListOptions.AccountId,
      AccountKey: ListOptions.AccountKey,
      ListType: ListOptions.ListType,
      IsDownload: false,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.GC, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.OverviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });


  }

  //#endregion

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantSalesConfig(this.TUTr_Config);
    this.TUTr_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_IssuedGiftCards(Type, index);
    this._FilterHelperService.SetMerchantSalesConfig(this.TUTr_Config);
    this.TUTr_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_MerchantSales(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.GiftCardRedeems
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.GiftCardRedeems
        );
        this._FilterHelperService.SetMerchantSalesConfig(this.TUTr_Config);
        this.TUTr_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    if (this.TUTr_InvoiceRangeMaxAmount == null || this.TUTr_InvoiceRangeMaxAmount == undefined ||
      this.TUTr_InvoiceRangeMinAmount == null || this.TUTr_InvoiceRangeMinAmount == undefined) {
      this._HelperService.NotifyError("value not be null or undefined");
    }
    else if (this.TUTr_InvoiceRangeMaxAmount < this.TUTr_InvoiceRangeMinAmount) {
      this._HelperService.NotifyError("Minimum invoice Amount should be less than maximum invoice Amount");
    }
    else {
      this._HelperService.MakeFilterSnapPermanent();
      this.SetSearchRanges();
      this.TUTr_GetData();
      this.ResetFilterUI();
      this._HelperService.StopClickPropogation();

      if (ButtonType == 'Sort') {
        $("#TUTr_sdropdown").dropdown('toggle');
      } else if (ButtonType == 'Other') {
        $("#TUTr_fdropdown").dropdown('toggle');
      }
    }
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantSalesConfig(this.TUTr_Config);
    this.SetSalesRanges();
    this.TUTr_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  SetSalesRanges(): void {
    this.TUTr_InvoiceRangeMinAmount = this.TUTr_Config.SalesRange.SalesMin;
    this.TUTr_InvoiceRangeMaxAmount = this.TUTr_Config.SalesRange.SalesMax;
  }

  //#endregion

}

export class OAccountOverview {
  public TotalTransactions?: number;
  public TotalSale?: number;

  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public TerminalStatus?: number;
  public Total?: number;
  public Idle?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
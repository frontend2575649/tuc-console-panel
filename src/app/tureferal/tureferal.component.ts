import { ChangeDetectorRef, Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../service/service";
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: "tu-tureferal",
  templateUrl: "./tureferal.component.html",
})
export class TUReferalComponent implements OnInit, OnDestroy {
  public ResetFilterControls: boolean = true;

  public _ObjectSubscription: Subscription = null;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = true;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;

  }


  ngOnInit() {
    // window.addEventListener('scroll', this.scroll, true);
    this._HelperService.StopClickPropogation();
    Feather.replace();
    this._HelperService.ValidateData();


    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];

      this.AcquirerList_Setup();
      this.AcquirerList_Filter_Owners_Load();
      this.RMList_Filter_Owners_Load();
      this.InitColConfig();

    });

    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.AcquirerList_GetData();
    // });
    // this._HelperService.StopClickPropogation();
  }
  // scroll = (event): void => {
  //   $(".daterangepicker").hide();
  //   $(".form-daterangepicker").blur();
  // };
  // openDatepicker(event) {
    
  //   $(".daterangepicker").show();
  // }


  ngOnDestroy(): void {
    // window.removeEventListener('scroll', this.scroll, true);
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {
    }
  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  GotoAddBank() {


    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.UserOnboarding.Referal
    ]);
  }


  //#endregion

  //#region merchantlist

  public AcquirerList_Config: OList;
  AcquirerList_Setup() {

    this.AcquirerList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.getpromocodes,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Promocode,
      Title: "Available Promocodes",
      StatusType: "PromocodeStatus",
      DefaultSortExpression: "CreateDate desc",
      IsDownload: true,
      TableFields: [
        {
          DisplayName: "Campaign",
          SystemName: "Title",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Description",
          SystemName: "Description",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Promo Code",
          SystemName: "PromoCode",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "PromoCodeValue",
          SystemName: "PromoCodeValue",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: "End Date",
          SystemName: "EndDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: "Start Date",
          SystemName: "StartDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };







    this.AcquirerList_Config = this._DataHelperService.List_Initialize(
      this.AcquirerList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Acquirer,
      this.AcquirerList_Config
    );

    this.AcquirerList_GetData();
  }
  // AcquirerList_ToggleOption(event: any, Type: any) {

  //   if (event != null) {
  //     for (let index = 0; index < this.AcquirerList_Config.Sort.SortOptions.length; index++) {
  //       const element = this.AcquirerList_Config.Sort.SortOptions[index];
  //       if (event.SystemName == element.SystemName) {
  //         element.SystemActive = true;
  //       }
  //       else {
  //         element.SystemActive = false;
  //       }
  //     }
  //   }

  //   this._HelperService.Update_CurrentFilterSnap(
  //     event,
  //     Type,
  //     this.AcquirerList_Config
  //   );

  //   this.AcquirerList_Config = this._DataHelperService.List_Operations(
  //     this.AcquirerList_Config,
  //     event,
  //     Type
  //   );

  //   if (
  //     (this.AcquirerList_Config.RefreshData == true)
  //     && this._HelperService.DataReloadEligibility(Type)
  //   ) {
  //     this.AcquirerList_GetData();
  //   }

  // }


  AcquirerList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.AcquirerList_Config.Sort.SortOptions.length; index++) {
        const element = this.AcquirerList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.AcquirerList_Config
    );

    this.AcquirerList_Config = this._DataHelperService.List_Operations(
      this.AcquirerList_Config,
      event,
      Type
    );

    if (
      (this.AcquirerList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.AcquirerList_GetData();
    }

  }


  timeout = null;
  AcquirerList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.AcquirerList_Config.Sort.SortOptions.length; index++) {
          const element = this.AcquirerList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.AcquirerList_Config
      );

      this.AcquirerList_Config = this._DataHelperService.List_Operations(
        this.AcquirerList_Config,
        event,
        Type
      );

      if (
        (this.AcquirerList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.AcquirerList_GetData();
      }

    }, this._HelperService.AppConfig.SearchInputDelay);

  }
  AcquirerList_GetData() {

    this.AcquirerList_Config.ListType = this.ListType;
    this.AcquirerList_Config.SearchBaseCondition = "";

    if (this.AcquirerList_Config.ListType == 1) // Active
    {
      this.AcquirerList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.AcquirerList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.active', "=");
    }
    // else if (this.AcquirerList_Config.ListType == 2) // Suspend
    // {
    //     this.AcquirerList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.AcquirerList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.suspended', "=");
    // }
    // else if (this.AcquirerList_Config.ListType == 3) // Blocked
    // {
    //     this.AcquirerList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.AcquirerList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.blocked', "=");
    // }

    else if (this.AcquirerList_Config.ListType == 4) {  // InActive
      this.AcquirerList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.AcquirerList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.inactive', "=");
    }
    else if (this.AcquirerList_Config.ListType == 5) {  // All
      this.AcquirerList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrictFromArray(this.AcquirerList_Config.SearchBaseCondition, "StatusCode", 'number', [], "=");
    }
    else {
      this.AcquirerList_Config.DefaultSortExpression = 'CreateDate desc';
    }




    this.GetOverviews(this.AcquirerList_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getpromocodesoverview);
    var TConfig = this._DataHelperService.List_GetData(
      this.AcquirerList_Config
    );
    this.AcquirerList_Config = TConfig;
  }
  AcquirerList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveAcquirer,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Acquirer,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Store
    //     .Dashboard,
    //   ReferenceData.ReferenceKey,
    //   ReferenceData.ReferenceId,
    // ]);

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AcquirerPanel.ReferalAccount,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);


  }



  //#endregion

  //#region OwnerFilter

  public AcquirerList_Filter_Owners_Option: Select2Options;
  public AcquirerList_Filter_Owners_Selected = null;
  AcquirerList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant,
    //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     this._HelperService.AppConfig.AccountType.PosAccount
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.AcquirerList_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  AcquirerList_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.AcquirerList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.AcquirerList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.AcquirerList_Filter_Owners_Selected,
        "="
      );
      this.AcquirerList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.AcquirerList_Config.SearchBaseConditions
      );
      this.AcquirerList_Filter_Owners_Selected = null;
    } else if (event.value != this.AcquirerList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.AcquirerList_Filter_Owners_Selected,
        "="
      );
      this.AcquirerList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.AcquirerList_Config.SearchBaseConditions
      );
      this.AcquirerList_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.AcquirerList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.AcquirerList_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.AcquirerList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  //#region RMFilter

  public RMList_Filter_Owners_Option: Select2Options;
  public RMList_Filter_Owners_Selected = null;
  RMList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetSubAccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.SubAccounts,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ],
    };

    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'RoleId', this._HelperService.AppConfig.DataType.Number, 7, '=');

    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.RMList_Filter_Owners_Option = {
      placeholder: "Sort by Rm",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  RMList_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.AcquirerList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  RMEventProcessing(event: any): void {
    if (event.value == this.AcquirerList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "RmId",
        this._HelperService.AppConfig.DataType.Text,
        this.AcquirerList_Filter_Owners_Selected,
        "="
      );
      this.AcquirerList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.AcquirerList_Config.SearchBaseConditions
      );
      this.AcquirerList_Filter_Owners_Selected = null;
    } else if (event.value != this.AcquirerList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "RmId",
        this._HelperService.AppConfig.DataType.Text,
        this.AcquirerList_Filter_Owners_Selected,
        "="
      );
      this.AcquirerList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.AcquirerList_Config.SearchBaseConditions
      );
      this.AcquirerList_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.AcquirerList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "RmId",
          this._HelperService.AppConfig.DataType.Text,
          this.AcquirerList_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.AcquirerList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion


  SetOtherFilters(): void {
    this.AcquirerList_Config.SearchBaseConditions = [];
    this.AcquirerList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.AcquirerList_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.AcquirerList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.AcquirerList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.AcquirerList_Config);

    this.SetOtherFilters();

    this.AcquirerList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      // inputAttributes: {
      //   autocapitalize: "off",
      //   autocorrect: "off",
      //   maxLength: "4",
      //   minLength: "4",
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Acquirer,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Acquirer
        );
        this._FilterHelperService.SetMerchantConfig(this.AcquirerList_Config);
        this.AcquirerList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.AcquirerList_GetData();

    if (ButtonType == 'Sort') {
      $("#AcquirerList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#AcquirerList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.AcquirerList_Config);
    this.SetOtherFilters();

    this.AcquirerList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.AcquirerList_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  public OverviewData: any = {
    Total: 0,
    Active: 0,
    Expired: 0,

  };

  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);
    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: SortExpression,
      Type: ListOptions.Type,
      ReferenceKey: ListOptions.ReferenceKey,
      StartDate: ListOptions.StartDate,
      EndDate: ListOptions.EndDate,
      ReferenceId: ListOptions.ReferenceId,
      SubReferenceId: ListOptions.SubReferenceId,
      SubReferenceKey: ListOptions.SubReferenceKey,
      AccountId: ListOptions.AccountId,
      AccountKey: ListOptions.AccountKey,
      //OwnerId: ListOptions.OwnerId,
      //OwnerKey: ListOptions.OwnerKey,

      ListType: ListOptions.ListType,
      IsDownload: false,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Promocode, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        // console.log("_Response",_Response)
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.OverviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });


  }


  public ListType: number;
  MerchantsList_ListTypeChange(Type) {

    this.ListType = Type;
    this.AcquirerList_Setup();
  }


}

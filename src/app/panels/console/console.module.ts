import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { TUConsoleComponent } from './console.component';
import { TUMerchantRoutingModule } from './console.routing.module';
import { BnplModule } from 'src/app/bnpl/bnpl.module';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
    declarations: [
        TUConsoleComponent,
    ],
    imports: [
        TUMerchantRoutingModule,
        TranslateModule,
        CommonModule,
        ReactiveFormsModule,
        BnplModule
    ],
    providers: [],
    bootstrap: [TUConsoleComponent]
})
export class TUConsoleModule { }

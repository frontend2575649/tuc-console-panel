import { AfterContentChecked, ChangeDetectorRef, Component, OnDestroy, OnInit, Renderer2, ViewRef } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import * as Cookies from '../../../assets/js/js.cookie.js';
import { DataHelperService, HelperService, OResponse } from '../../service/service';
import { MerchantguardGuard } from 'src/app/service/guard/merchantguard.guard';
import { filter } from 'rxjs/operators';

declare var $: any;

@Component({
    selector: 'console-root',
    templateUrl: './console.component.html',
})
export class TUConsoleComponent implements OnInit, OnDestroy, AfterContentChecked {
    imagecontent: any;
    LogoImageUrlContent:any;
    localLogoImageUrl:any;
    public _Store_Option: Select2Options;
    public isSystemAdmin: boolean = false;
    // private _ChangeDetectorRef: any;
    public isAllMerchants:boolean = false;
    public isLoyaltyMerchants:boolean = false;
    public isViewMerchants:boolean = false;
    public isMerchantManagementRoute:boolean = false;

    constructor(
        public _DataHelperService: DataHelperService,
        public _Router: Router,
        public _HelperService: HelperService,
        public _MerchantguardGuard: MerchantguardGuard,
        private renderer2: Renderer2,
        public _ChangeDetectorRef: ChangeDetectorRef,
    ) {
        this.localLogoImageUrl = '../../assets/img/dealday-logo.jpg'
        this.LogoImageUrlContent=this._HelperService.GetStorage("logoimageurl");
        if(this.LogoImageUrlContent != "" || this.LogoImageUrlContent === undefined || this.LogoImageUrlContent === null){
            this._HelperService.logoImageUrl = this.LogoImageUrlContent;
        }
        this.Logo_GetData()
        _Router.events.pipe(
            filter(event => event instanceof NavigationStart)  
          ).subscribe((event: NavigationEnd) => {
            if(event.url != "/console/merchants")
            {
                this.isAllMerchants = false;
                this.isLoyaltyMerchants = false;
                this.isViewMerchants = false;
            }
            if(event.url == '/console/stores' || event.url == '/console/terminals' || event.url == '/console/cashiers'){
                this.isMerchantManagementRoute = true;
            }
          });
    }
    ngAfterContentChecked(): void {
        this._ChangeDetectorRef.detectChanges();
    }
    ngOnDestroy(): void {
        setTimeout(() => {
            if (this._ChangeDetectorRef && !(this._ChangeDetectorRef as ViewRef).destroyed) {
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    ngAfterViewInit(): void {
    }

    ngOnInit() {
        this._HelperService.ValidateData();
        this._HelperService.getPendingRequestCount()
        this._HelperService.darkStyle = Cookies.get("mapColor");
        if (this._HelperService.SystemName.includes('teamsandroles')
            || this._HelperService.SystemName.includes('generalsettings')
            || this._HelperService.SystemName.includes('systemadministration')
            || this._HelperService.SystemName.includes('viewdownloads')) {
            this.isSystemAdmin = true;
        }
        // Chart.defaults.global.defaultFontColor = 'white';
        // Chart.defaults.global.defaultFontSize = '16';

        $("li").click(() => {

            var backdrop: HTMLElement = document.getElementById("backdrop");
            backdrop.classList.remove("show");

            var modal_backdrop: HTMLCollectionOf<Element> = document.getElementsByClassName("modal");
            if (modal_backdrop.length > 0) {
                const element = modal_backdrop.item(0);
                this._HelperService.CloseModal(element.id);
            }

            var modal_backdrop: HTMLCollectionOf<Element> = document.getElementsByClassName("modal-backdrop");
            if (modal_backdrop.length > 0) {
                const element = modal_backdrop.item(0);
                element.classList.remove("modal-backdrop");
            }

        });

        $('body').on('click', '.df-mode', (e) => {
            e.preventDefault();

            for (let index = 0; index < $('.df-mode').length; index++) {
                const element = $('.df-mode')[index];
                setTimeout(() => {
                    if (element.classList.length == 3) {
                        if (element.getAttribute('data-title') === 'dark') {
                            this._HelperService.CheckMode = 'dark';
                            this._HelperService.darkStyle = [
                                {
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#242f3e"
                                        }
                                    ]
                                },
                                {
                                    elementType: "labels.text.stroke",
                                    stylers: [
                                        {
                                            color: "#242f3e"
                                        }
                                    ]
                                },
                                {
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#746855"
                                        }
                                    ]
                                },
                                {
                                    featureType: "administrative.locality",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#d59563"
                                        }
                                    ]
                                },
                                {
                                    featureType: "poi",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#d59563"
                                        }
                                    ]
                                },
                                {
                                    featureType: "poi.park",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#263c3f"
                                        }
                                    ]
                                },
                                {
                                    featureType: "poi.park",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#6b9a76"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#38414e"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road",
                                    elementType: "geometry.stroke",
                                    stylers: [
                                        {
                                            color: "#212a37"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#9ca5b3"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road.highway",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#746855"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road.highway",
                                    elementType: "geometry.stroke",
                                    stylers: [
                                        {
                                            color: "#1f2835"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road.highway",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#f3d19c"
                                        }
                                    ]
                                },
                                {
                                    featureType: "transit",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#2f3948"
                                        }
                                    ]
                                },
                                {
                                    featureType: "transit.station",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#d59563"
                                        }
                                    ]
                                },
                                {
                                    featureType: "water",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#17263c"
                                        }
                                    ]
                                },
                                {
                                    featureType: "water",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#515c6d"
                                        }
                                    ]
                                },
                                {
                                    featureType: "water",
                                    elementType: "labels.text.stroke",
                                    stylers: [
                                        {
                                            color: "#17263c"
                                        }
                                    ]
                                }
                            ];


                            Cookies.set('mapColor', this._HelperService.darkStyle);
                        } else {
                            this._HelperService.CheckMode = 'light';
                            this._HelperService.darkStyle = [];
                            Cookies.set('mapColor', this._HelperService.darkStyle);
                        }
                    }
                }, 50);
            }

        });

        setTimeout(() => {
            var hasMode = Cookies.get('df-mode');

            this._HelperService.CheckMode = (hasMode === 'dark') ? 'dark' : 'light';
            if (hasMode == 'dark') {
                this._HelperService.darkStyle = [
                    {
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#242f3e"
                            }
                        ]
                    },
                    {
                        elementType: "labels.text.stroke",
                        stylers: [
                            {
                                color: "#242f3e"
                            }
                        ]
                    },
                    {
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#746855"
                            }
                        ]
                    },
                    {
                        featureType: "administrative.locality",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#d59563"
                            }
                        ]
                    },
                    {
                        featureType: "poi",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#d59563"
                            }
                        ]
                    },
                    {
                        featureType: "poi.park",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#263c3f"
                            }
                        ]
                    },
                    {
                        featureType: "poi.park",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#6b9a76"
                            }
                        ]
                    },
                    {
                        featureType: "road",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#38414e"
                            }
                        ]
                    },
                    {
                        featureType: "road",
                        elementType: "geometry.stroke",
                        stylers: [
                            {
                                color: "#212a37"
                            }
                        ]
                    },
                    {
                        featureType: "road",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#9ca5b3"
                            }
                        ]
                    },
                    {
                        featureType: "road.highway",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#746855"
                            }
                        ]
                    },
                    {
                        featureType: "road.highway",
                        elementType: "geometry.stroke",
                        stylers: [
                            {
                                color: "#1f2835"
                            }
                        ]
                    },
                    {
                        featureType: "road.highway",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#f3d19c"
                            }
                        ]
                    },
                    {
                        featureType: "transit",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#2f3948"
                            }
                        ]
                    },
                    {
                        featureType: "transit.station",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#d59563"
                            }
                        ]
                    },
                    {
                        featureType: "water",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#17263c"
                            }
                        ]
                    },
                    {
                        featureType: "water",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#515c6d"
                            }
                        ]
                    },
                    {
                        featureType: "water",
                        elementType: "labels.text.stroke",
                        stylers: [
                            {
                                color: "#17263c"
                            }
                        ]
                    }
                ];
                Cookies.set('mapColor', this._HelperService.darkStyle);
            }
            else {
                this._HelperService.darkStyle = [];
                Cookies.set('mapColor', this._HelperService.darkStyle);
            }
        }, 50);
        const s = this.renderer2.createElement('script');
        s.type = 'text/javascript';
        s.src = '../../../assets/js/dashforge.aside.js';
        s.text = ``;
        this.renderer2.appendChild(document.body, s);

        const s2 = this.renderer2.createElement('script');
        s2.type = 'text/javascript';
        s2.src = '../../../../assets/js/dashforge.settings.js';
        s2.text = ``;
        this.renderer2.appendChild(document.body, s2);
        Feather.replace();
        // console.log(" this.imagecontent ", this.imagecontent )
        // console.log(" this.imagecontent ", this.imagecontent )
        // setTimeout(()=>{
        //     console.log(" this.imagecontent11 ", this.imagecontent );
        // },500)
    }

    private pData={
        Task: this._HelperService.AppConfig.Api.Core.getapplogo,
        Panel:"WebConnect",
        ImageContent: {
          Reference: "",
          Name: "Logo",
          Extension: "png",
          Tags: ""
          }
      }
        Logo_GetData() {
          this._HelperService.IsFormProcessing = true;
          let _OResponse: Observable<OResponse>;
              _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin, this.pData);
      
              _OResponse.subscribe(
                _Response => {
                  this._HelperService.IsFormProcessing = false;
                  if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.logoImageUrl =_Response.Result.ImageUrl;
                    this._HelperService.SaveStorage("logoimageurl", _Response.Result.ImageUrl);

                  }
                  else {
                    this._HelperService.NotifyError(_Response.Message);
                }
                  
      
                })
        } 
       
    // isInsideArray(){
    //     return false;
    // }

    clicked(e)
    {
        if(e == 'allMerchants')
        {
            this.isAllMerchants = true;
            this.isLoyaltyMerchants = false;
            this.isViewMerchants = false;
            this.isMerchantManagementRoute = false;

        }
        if(e == 'loyaltyMerchants')
        {
            this.isAllMerchants = false;
            this.isLoyaltyMerchants = true;
            this.isViewMerchants = false;
            this.isMerchantManagementRoute = true;

        }
        if(e == 'viewMerchant')
        {
            this.isAllMerchants = false;
            this.isLoyaltyMerchants = false;
            this.isViewMerchants = true;
            this.isMerchantManagementRoute = false;

        }

    }

    ProcessLogout() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.LogoutTitle,
            text: "Click on Logout button to confirm",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: 'Logout',
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                var pData = {
                    Task: this._HelperService.AppConfig.Api.Logout
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess(_Response.Message);
                            this._HelperService.DeleteStorage('temploc');

                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                    });
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Account);
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Location);
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.OReqH);
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Permissions);
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.IconImage);
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.Login]);
            }
            else { }
        });



    }


}
import { NgModule } from "@angular/core";
import { Routes, RouterModule, CanActivateChild } from "@angular/router";
import { TUConsoleComponent } from "./console.component";
import { HelperService } from "../../service/helper.service";
import { MerchantguardGuard } from 'src/app/service/guard/merchantguard.guard';

const routes: Routes = [
  {
    path: "console",
    component: TUConsoleComponent,
    // canActivate:[MerchantguardGuard],
    canActivateChild:[HelperService],
    children: [
      { path: 'dashboard',data: { permission: 'credentials', PageName: 'DASHBOARD' },loadChildren: "../../panel/console/dashboards/root/dashboard.module#TUDashboardModule" },
      { path: 'settings/addroles', data: { 'permission': 'credentials', PageName: 'System.Menu.Settings' }, loadChildren: '../../../app/modules/acountsettings/tusettings/tuaddroles/tuaddroles.module#TURolesModule' },
      { path: 'settings/editroles/:referencekey/:referenceid', data: { 'permission': 'credentials', PageName: 'System.Menu.Settings' }, loadChildren: '../../../app/modules/acountsettings/tusettings/tueditroles/tueditroles.module#TUEditRolesModule' },
      { path: "acquirer/branchdetails/editbranch/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../Acquirer/BranchDivision/EditBranch/EditBranch.module#EditBranchModule" },


      //Account Settings

      { path: 'settings/adminuser', data: { 'permission': 'roles', PageName: 'System.Menu.Settings' }, loadChildren: '../../modules/acountsettings/tusettings/adminuser/adminuser.module#TUAdminUserModule' },
      { path: "sampletable", data: { permission: "account", PageName: "System.Menu.Merchant" }, loadChildren: "../../panel/accounts/samlecolapsetable/samlecolapsetable.module#TUSampleTableModule" },

      //{ path: "trial", data: { permission: "account", PageName: "System.Menu.Merchants" }, loadChildren: "../../modules/accounts/tuconsole/cashiers/tucashiers.module#TUCashiersModule" },
      //End 
      { path: "plans", data: { permission: "account", PageName: "System.Menu.MerchantPlans" }, loadChildren: "../../panel/accounts/tumerchantplans/tumerchantplans.module#TUMerchantPlansModule" },
      { path: "plans/plan/:referencekey/:referenceid", data: { permission: "account", PageName: "System.Menu.MerchantPlan" }, loadChildren: "../../panel/accounts/tumerchantplans/tumerchantplan/tumerchantplan.module#TUMerchantPlanModule" },

      { path: "cashiers", data: { permission: "account", PageName: "System.Menu.Cashiers" }, loadChildren: "../../panel/accounts/cashiers/tucashiers.module#TUCashiersModule" },
      { path: "stores", data: { permission: "account", PageName: "System.Menu.Stores" }, loadChildren: "../../panel/accounts/stores/tustores.module#TUStoresModule" },

      { path: "terminals", data: { permission: "account", PageName: "System.Menu.Terminals" }, loadChildren: "../../panel/accounts/tuterminals/tuterminals.module#TUTerminalsModule" },
      { path: "payments", data: { permission: "account", PageName: "System.Menu.Payments" }, loadChildren: "../../panel/accounts/tupayments/tupayments.module#TUPaymentsModule" },
      { path: "vas", data: { permission: "account", PageName: "System.Menu.VasServices" }, loadChildren: "../../panel/accounts/tupayments/tuvascategories/tuvascategories.module#TUVasCategoriesModule" },
      { path: "vasproduct", data: { permission: "account", PageName: "System.Menu.VasServices" }, loadChildren: "../../panel/accounts/tupayments/tuvasproduct/tuvasproduct.module#TUVasProductModule" },
      { path: "vasanalytics", data: { permission: "account", PageName: "System.Menu.VasServices" }, loadChildren: "../../panel/accounts/tupayments/tuvasanalytics/tuvasanalytics.module#TuVasAnalyticsModule" },

      { path: "invoices", data: { permission: "account", PageName: "System.Menu.Invoices" }, loadChildren: "../../panel/accounts/tuinvoices/tuinvoices.module#TUInvoicesModule" },
      { path: "invoices/invoice/:referencekey/:useraccountkey", data: { permission: "account", PageName: "System.Menu.Invoices" }, loadChildren: "../../panel/accounts/tuinvoices/invoicedetails/invoicedetails.module#TUInvoiceDetailsModule" },

      { path: "userprofile", data: { permission: "account", PageName: "System.Menu.Profile" }, loadChildren: "../../modules/acountsettings/tusettings/userprofile/tuuserprofile.module#TUUserProfileModule" },
      { path: "settings", data: { permission: "account", PageName: "System.Menu.AccountSettings" }, loadChildren: "../../modules/accountdetails/tuconsole/tusettings/tusettings.module#TUSettingsModule" },
      { path: "dashboard", data: { permission: "dashboard", PageName: "System.Menu.Dashboard" }, loadChildren: "./../../panel/console/dashboards/root/dashboard.module#TUDashboardModule" },
      { path: "store", data: { permission: "account", PageName: "System.Menu.Store" }, loadChildren: "../../modules/accountdetails/tuconsole/tustore/tustore.module#TUStoreModule" },
      { path: "terminal", data: { permission: "account", PageName: "System.Menu.Terminal" }, loadChildren: "../../modules/accountdetails/tuconsole/tuterminal/tuterminal.module#TUTerminalModule" },
      { path: "cashier", data: { permission: "account", PageName: "System.Menu.Cashier" }, loadChildren: "../../modules/accountdetails/tuconsole/tucashier/tucashier.module#TUCashierModule" },
      { path: "subaccount", data: { permission: "account", PageName: "System.Menu.Cashier" }, loadChildren: "../../modules/accountdetails/tuconsole/tusubaccount/tusubaccount.module#TUSubAccountModule" },
      { path: "bank", data: { permission: "account", PageName: "System.Menu.BankReconcialiation" }, loadChildren: "../../modules/accounts/tuconsole/bankreconciliation/bank.module#TUBankModule" },
      { path: "bulkterminals", data: { permission: "account", PageName: "System.Menu.BulkTerminal" }, loadChildren: "../../modules/accounts/tuconsole/bulkterminalupload/tuterminals.module#TUBulkTerminalsModule" },
      { path: "bulkterminalslist", data: { permission: "account", PageName: "System.Menu.BulkTerminal" }, loadChildren: "../../modules/accounts/tuconsole/bulkterminallist/tuterminals.module#TUBulkTerminalsModule" },

      // { path: "customers", data: { permission: "account", PageName: "System.Menu.Customer" }, loadChildren: "../../modules/dashboards/loyality/customers/tucustomers.module#TUCustomersModule" },
      //{ path: "customer", data: { permission: "account", PageName: "System.Menu.Customer" }, loadChildren: "../../modules/accoutdetails/tuconsole/tucustomer/tucustomer.module#TUCustomerModule" },
      { path: "campaign", data: { permission: "account", PageName: "System.Menu.Campaign" }, loadChildren: "../../modules/accountdetails/tuconsole/tucampaign/tucampaign.module#TUCampaignModule" },
      { path: 'cashiers', data: { 'permission': 'cashiers', PageName: 'System.Menu.Cashier' }, canActivate: [MerchantguardGuard], loadChildren: '../../modules/accounts/tuconsole/cashiers/tucashiers.module#TUCashiersModule' },

      // Merchant Details
      { path: 'merchants', data: { 'permission': 'merchants', PageName: 'System.Menu.Merchant' }, loadChildren: '../../modules/accounts/tuconsole/merchants/tumerchants.module#TUMerchantsModule' },
      { path: 'merchant', data: { 'permission': 'merchants', PageName: 'System.Menu.Merchant' }, loadChildren: '../../modules/accountdetails/tuconsole/tumerchant/tumerchant.module#TUMerchantModule' },

      { path: "profile/:referencekey/:referenceid", data: { permission: "customers", PageName: "System.Menu.Profile" }, loadChildren: "./../../pages/hcmerchantedit/hcmerchantedit.module#HCMerchantEditModule" },
      {
        path: "terminallive",
        data: { permission: "dashboard", PageName: "System.Menu.Dashboard" },
        loadChildren:
          "./../../pages/dashboard/tumerchantterminallive/tumerchantterminallive.module#TUMerchantTerminalsLiveModule"
      },
      {
        path: "merchantupload",
        data: { permission: "stores", PageName: "System.Menu.BulkUplaod" },
        loadChildren:
          "./../../pages/useronboarding/bulkmerchant/merchantupload.module#TUMerchantUploadModule"
      },

      { path: 'onboarding/merchant', data: { 'permission': 'merchants', PageName: 'System.Menu.MerchantOnboarding' }, loadChildren: '../../pages/useronboarding/merchantonboarding/merchantonboarding.module#TUMerchantOnboardingModule' },

      //TUCMALl
      { path: "ongoingorders", data: { permission: "account", PageName: "System.Menu.Orders" }, loadChildren: "../../panel/console/tucmall/tuongoingorders/tuorders.module#TUOrdersModule" },
      { path: "orders", data: { permission: "account", PageName: "System.Menu.Orders" }, loadChildren: "../../panel/console/tucmall/tuorders/tuorders.module#TUOrdersModule" },
      { path: "tucorders", data: { permission: "account", PageName: "System.Menu.Orders" }, loadChildren: "../../panel/console/tucmall/tucorders/tucorders.module#TUCOrderssModule" },

      { path: "orderhistory/:referenceid/:referencekey", data: { permission: "account", PageName: "System.Menu.OrderHistory" }, loadChildren: "../../panel/console/tucmall/tuorder/tuorder.module#TUOrderModule" },
      { path: "invoicehistory/:referenceid/:referencekey", data: { permission: "account", PageName: "System.Menu.InvoiceHistory" }, loadChildren: "../../panel/console/tucmall/tuinvoice/tuinvoice.module#TUInvoiceModule" },
      // { path: "cancelledorderhistory", data: { permission: "account", PageName: "System.Menu.OrderHistory" }, loadChildren: "../../panel/console/tucmall/tucancelledorders/tucancelledorders.component#TUCancelledOrderModule" },
      { path: "products", data: { permission: "account", PageName: "System.Menu.Products" }, loadChildren: "../../panel/console/tucmall/tuproducts/tuproducts.module#TUProductsModule" },
      { path: "ambassadors", data: { permission: "account", PageName: "System.Menu.Ambassadors" }, loadChildren: "../../panel/console/tucmall/tuambassadors/tuambassadors.module#TUAmbassadorsModule" },
      { path: "product", data: { permission: "account", PageName: "System.Menu.Products" }, loadChildren: "../../modules/accountdetails/tuconsole/tuproduct/tuproduct.module#TUProductModule" },
      { path: "ambassador", data: { permission: "account", PageName: "System.Menu.Ambassador" }, loadChildren: "../../modules/accountdetails/tuconsole/tucambassador/tucambassador.module#TUCAmbassadorModule" },
      { path: "ambassador/:referencekey/:referenceid", data: { permission: "account", PageName: "System.Menu.Ambassador" }, loadChildren: "../../modules/accountdetails/tuconsole/tucambassador/tucambassador.module#TUCAmbassadorModule" },
      { path: "addproduct", data: { permission: "account", PageName: "System.Menu.AddProduct" }, loadChildren: "../../panel/console/tucmall/tuaddproduct/tuaddproduct.module#TUAddProductsModule" },
      { path: "addambassador", data: { permission: "account", PageName: "System.Menu.AddAmbassador" }, loadChildren: "../../panel/console/tucmall/tuaddambassador/tuaddambassador.module#TUAddAmbassadorModule" },
      { path: "editproduct", data: { permission: "account", PageName: "System.Menu.EditProduct" }, loadChildren: "../../panel/console/tucmall/tueditproduct/tueditproduct.module#TUEditProductsModule" },

      { path: "allorderhistory", data: { PageName: "Order History" }, loadChildren: "../../panel/console/tucmall/orderhistory/orderhistory.module#OrderhistoryModule" },
      { path: "deliveryoverview", data: { PageName: "Overview" }, loadChildren: "../../panel/console/tucmall/deliveryoverview/deliveryoverview.module#DeliveryoverviewModule" },

      { path: "redeemedorders", data: { permission: "deals", PageName: "System.Menu.Deals" }, loadChildren: "../../panel/console/deals/turedeemedorder/turedeemedorder.module#TURedeemedOrderModule" },
      { path: "cancelledorders", data: { permission: "deals", PageName: "System.Menu.Deals" }, loadChildren: "../../panel/console/deals/tucancelledorders/tucancelledorder.module#TUCancelledOrderModule" },


      //My Buissness 
      { path: "sales/saleshistory", data: { permission: "dashboard", PageName: "System.Menu.SalesHistory" }, loadChildren: "../../modules/transactions/tusale/merchant/tusale.module#TUSaleModule" },
      { path: "salestrend", data: { permission: "dashboard", PageName: "System.Menu.SalesTrend" }, loadChildren: "../../modules/dashboards/mybuisness/tusalestrends/tusalestrends.module#TuSalesTrendModule" },
      { path: "salesummary", data: { permission: "dashboard", PageName: "System.Menu.SaleSummary" }, loadChildren: "../../modules/dashboards/mybuisness/tusalesummary/tusalesummary.module#TUSaleSummaryModule" },


      //Reward N Redeem
      { path: "saleshistory", data: { permission: "dashboard", PageName: "System.Menu.SalesHistory" }, canActivate: [MerchantguardGuard], loadChildren: "../../modules/transactions/tusale/tusale.module#TUSaleModule" },
      { path: "rewardhistory", canActivate: [MerchantguardGuard], data: { permission: "dashboard", PageName: "System.Menu.RewardHistory" }, loadChildren: "../../modules/transactions/tusale/rewardhistory/tusale.module#TUSaleModule" },
      { path: "pendingrewardhistory", data: { permission: "dashboard", PageName: "System.Menu.PendingRewardHistory" }, loadChildren: "../../modules/transactions/tusale/pendingrewardhistory/tusale.module#TUSaleModule" },

      { path: "redeemhistory", data: { permission: "dashboard", PageName: "System.Menu.RedeemHistory" }, canActivate: [MerchantguardGuard], loadChildren: "../../modules/transactions/tusale/redeemhistory/tusale.module#TUSaleModule" },
      { path: "rewardclaim", data: { permission: "dashboard", PageName: "System.Menu.RewardClaim" }, canActivate: [MerchantguardGuard], loadChildren: "../../modules/transactions/tusale/turewardclaim/turewardclaim.module#TURewardsClaimModule" },
      // { path: "saledetail/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.SalesHistory" }, loadChildren: "../../modules/transactions/tusale/tusaledetail/tusale.module#TUSaleModule" },

      //Daily Report
      { path: "dailyreport/saleshistory", data: { permission: "dashboard", PageName: "System.Menu.SalesHistory" }, loadChildren: "../../modules/transactions/tusale/dailyreport/tusale.module#TUSaleModule" },
      { path: "dailyreport/rewardhistory", data: { permission: "dashboard", PageName: "System.Menu.RewardHistory" }, loadChildren: "../../modules/transactions/tusale/rewardhistory/dailyreport/tusale.module#TUSaleModule" },
      { path: "dailyreport/redeemhistory", data: { permission: "dashboard", PageName: "System.Menu.RedeemHistory" }, loadChildren: "../../modules/transactions/tusale/redeemhistory/dailyreport/tusale.module#TUSaleModule" },
      { path: "dailyreport/rewardclaim", data: { permission: "dashboard", PageName: "System.Menu.RewardClaim" }, loadChildren: "../../modules/transactions/tusale/turewardclaim/dailyreport/turewardclaim.module#TURewardsClaimModule" },

      //Loyality
      { path: "overview", data: { permission: "dashboard", PageName: "System.Menu.Overview" }, loadChildren: "../../modules/dashboards/loyality/overview/dashboard.module#TUDashboardModule" },
      { path: "visits", data: { permission: "dashboard", PageName: "System.Menu.Visits" }, loadChildren: "../../modules/dashboards/loyality/visits/dashboard.module#TUDashboardModule" },
      { path: "campaigns", data: { permission: "dashboard", PageName: "System.Menu.Campaigns" }, loadChildren: "../../modules/dashboards/loyality/campaigns/tucampaigns.module#TUCampaignsModule" },

      //Acquirer
      { path: 'referal', data: { 'permission': 'referal', PageName: 'System.Menu.Referal' }, loadChildren: '../../tureferal/tureferal.module#TUReferalModule' },


      { path: 'acquirers', data: { 'permission': 'acquirer', PageName: 'System.Menu.Acquirers' }, loadChildren: '../../Acquirer/account/tuacquirers/tuacquirers.module#TUAcquirersModule' },
      { path: 'acquirer', data: { 'permission': 'acquirer', PageName: 'System.Menu.Acquirer' }, loadChildren: '../../Acquirer/accountdetails/tuacquirer/tuacquirer.module#TUAcquirerModule' },

      { path: 'refer', data: { 'permission': 'refer', PageName: 'System.Menu.Referal' }, loadChildren: '../../Acquirer/accountdetails/turefer/turefer.module#TUReferModule' },


      { path: 'onboarding/bank', data: { 'permission': 'acquirer', PageName: 'System.Menu.AcquirerOnboarding' }, loadChildren: '../../Acquirer/accountcreation/tuaddacquirer/tuaddacquirer.module#AddAcquirerModule' },

      { path: 'onboarding/referal', data: { 'permission': 'referal', PageName: 'System.Menu.CampaignReferal' }, loadChildren: '../../Acquirer/accountcreation/tuaddreferal/tuaddreferal.module#AddReferalModule' },

      //Ptsp

      { path: 'ptsps', data: { 'permission': 'acquirer', PageName: 'System.Menu.Ptsps' }, loadChildren: '../../ptsp/accounts/tuptsps/tuptsps.module#TUPtspsModule' },
      { path: 'ptsp', data: { 'permission': 'acquirer', PageName: 'System.Menu.Ptsps' }, loadChildren: '../../ptsp/accountdetails/tuptsp/tuptsp.module#TUPtspModule' },
      { path: 'onboarding/terminalprovider', data: { 'permission': 'acquirer', PageName: 'System.Menu.PtspOnboarding' }, loadChildren: '../../ptsp/accountcreation/tuaddptsp/tuaddptsp.module#AddPtspModule' },

      //pssp

      { path: 'gatewayproviders', data: { 'permission': 'acquirer', PageName: 'System.Menu.PgAccounts' }, loadChildren: '../../pssp/accounts/tupssps/tupssps.module#TUPsspsModule' },
      { path: 'gatewayprovider', data: { 'permission': 'acquirer', PageName: 'System.Menu.PgAccount' }, loadChildren: '../../pssp/accountdetails/tupssp/tupssp.module#TUPsspModule' },
      { path: 'onboarding/gatewayprovider', data: { 'permission': 'acquirer', PageName: 'System.Menu.PsspOnboarding' }, loadChildren: '../../pssp/accountcreation/tuaddpssp/tuaddpssp.module#AddPsspModule' },

      //partner
      { path: 'partners', data: { 'permission': 'acquirer', PageName: 'System.Menu.Partners' }, loadChildren: '../../partner/account/tupartners/tupartners.module#TUPartnersModule' },
      { path: 'partner', data: { 'permission': 'acquirer', PageName: 'System.Menu.Partner' }, loadChildren: '../../partner/accountdetails/tupartner/tupartner.module#TUPartnerModule' },
      { path: 'onboarding/partner', data: { 'permission': 'acquirer', PageName: 'System.Menu.PartnerOnboarding' }, loadChildren: '../../partner/accountcreation/tuaddpartner/tuaddpartner.module#AddPartnerModule' },

      //Customers
      { path: 'customers', data: { 'permission': 'customers', PageName: 'System.Menu.Customers' }, loadChildren: '../../Customer/modules/accounts/tucustomer/customers/tucustomers.module#TUCustomersModule' },
      { path: 'customer', data: { 'permission': 'customers', PageName: 'System.Menu.Customers' }, loadChildren: '../../Customer/modules/accountdetails/tucustomer/tucustomer.module#TUCustomerModule' },

      //Account Settings  
      { path: 'settings', data: { 'permission': 'admin', PageName: 'System.Menu.Admin' }, loadChildren: '../../modules/acountsettings/tusettings/tusettings.module#TUSettingsModule' },

      //Administration
      { path: 'generalsettinghome', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/tugeneralsettings/tuhome.module#TUHomeModule' },
      { path: 'administrationhome', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/tuhome/tuhome.module#TUHomeModule' },
      { path: 'requesthistory', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/turequesthistory/turequesthistory.module#TURequestHistoryModule' },



      { path: 'verificationrequest', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/tuverficationrequest/tuverficationrequest.module#TUVerificatiobRequestModule' },
      { path: 'usersession', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/tuusersession/tuusersession.module#TUUserSessionModule' },
      { path: 'adminfeatures', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/tuadminfeatures/tuadminfeatures.module#TUAdminFeaturesModule' },
      { path: 'pushnotification', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/tupushnotification/tupushnotification.module#TUPushNotificationModule' },

      { path: 'configurations', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/tuconfiguration/tuconfiguration.module#TUConfigurationsModule' },
      { path: 'city', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/tucity/tucity.module#TUCityModule' },
      { path: 'detailscity', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/tudetailscity/tudetailscity.module#TUDetailsCityModule' },
      { path: 'getcity/:referencekey/:referenceid', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/tugetcity/tugetcity.module#TUGetCityModule' },
      { path: 'getcityarea/:referencekey/:referenceid', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/tugetcityarea/tugetcityarea.module#TUGetCityAreaModule' },


      // lof feature routing start

      { path: 'logfeature', data: { 'permission': 'administration', PageName: 'System.Menu.Log' }, loadChildren: '../../panel/administration/tulogfeature/tulogfeature.module#TULogFeatureModule' },

      // lof feature routing end



      { path: 'systemlog', data: { 'permission': 'administration', PageName: 'System.Menu.Log' }, loadChildren: '../../panel/administration/tusystemlog/tusystemlog.module#TUSystemlogModule' },
      { path: 'responsecode', data: { 'permission': 'administration', PageName: 'System.Menu.Log' }, loadChildren: '../../panel/administration/turesponsemessages/turesponsemessages.module#TUResponseMessagesModule' },
      { path: 'appslider', data: { 'permission': 'administration', PageName: 'System.Menu.Log' }, loadChildren: '../../panel/administration/tuappslider/tuappslider.module#TUAppSliderModule' },
      { path: 'imagegallery', data: { 'permission': 'administration', PageName: 'System.Menu.ImageGallery' }, loadChildren: '../../panel/administration/tuimagegallery/tuimagesgallery/tuimagesgallery.module#TUImageGalleryModule' },

      { path: 'apps', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/hcapps/tuapplist/tuapplist.module#TUAppListModule' },
      { path: 'newapis', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/hcapps/tunewconfig/tunewconfig.module#TUNewConfigModule' },

      { path: 'app', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/hcapps/hcapps.module#HCAppsModule' },
      { path: 'merchantcategories', data: { 'permission': 'administration', PageName: 'Merchant Categories' }, loadChildren: '../../panel/administration/tumerchantcategories/tumerchantcategories.module#TUMerchantCategoriesModule' },
      { path: 'merchantsubcategories/:referencekey/:referenceid', data: { 'permission': 'administration', PageName: 'Merchant Sub Categories' }, loadChildren: '../../panel/administration/tumerchantcategories/merchantsubcategories/merchantsubcategories.module#MerchantSubCategoriesModule' },
      { path: 'dealcategories', data: { 'permission': 'administration', PageName: 'Deal Categories' }, loadChildren: '../../panel/administration/tudealcategories/tudealcategories.module#TUDealCategoriesModule' },
      { path: 'dealsubcategories/:referencekey/:referenceid', data: { 'permission': 'administration', PageName: 'Deal Sub Categories' }, loadChildren: '../../panel/administration/tudealcategories/tudealsubcategories/tudealsubcategories.module#TUDealSubCategoriesModule' },
      { path: 'rootcategories', data: { 'permission': 'administration', PageName: 'Root Categories' }, loadChildren: '../../panel/administration/turootcategory/turootcategory.module#TurootcategoryModule' },
      { path: 'subcategories/:referencekey/:referenceid', data: { 'permission': 'administration', PageName: 'Subcategories' }, loadChildren: '../../panel/administration/tusubcategories/tusubcategories.module#TusubcategoryModule' },
      { path: 'faqcategories', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/tufaqcategories/tufaqcategories.module#TUFAQCategoriesModule' },
      { path: 'faqs/:referencekey/:referenceid', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/tufaqs/tufaqs.module#TUFAQsModule' },
      { path: 'dealrequest/:referencekey/:referenceid', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/tudealrequest/tudealrequest.module#TUDealRequestModule' },
      { path: 'logo', data: { 'permission': 'administration', PageName: 'Logo' }, loadChildren: '../../panel/administration/tuadminlogomanagement/tulogomanagement.module#TULogoManagementModule' },
      { path: 'adddeal', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/console/deals/tuadddeal/tuadddeal.module#AddDealModule' },
      { path: 'editdeal/:referencekey/:referenceid/:accountid/:accountkey', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/console/deals/tueditdeal/tueditdeal.module#EditDealModule' },
      { path: 'adddeals', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/console/deals/tuadddeals/tuadddeals.module#AddDealsModule' },
      { path: 'deals', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/console/deals/tudeals/tudeals.module#TUDealsModule' },
      { path: 'dealsold', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/console/deals/tudealsold/tudealsold.module#TUDealsOldModule' },
      { path: 'flashdeals', data: { 'permission': 'deals', PageName: 'System.Menu.FlashDeals' }, loadChildren: '../../panel/console/deals/tuflashdeals/tuflashdeals.module#TUFlashDealsModule' },
      { path: 'dealpromotion', data: { 'permission': 'deals', PageName: 'System.Menu.DealPromotion' }, loadChildren: '../../panel/console/deals/tudealpromotion/tudealpromotion.module#TUDealPromotionModule' },

      { path: "allpickup", data: { permission: "deals", PageName: "System.Menu.Deals" }, loadChildren: "../../panel/console/deals/tupickups/rootpickup.module#RootPickupRoutingModule" },

      { path: 'deal', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/console/deals/tudeal/tudeal.module#TUDealModule' },
      { path: 'editdeals/:referencekey/:referenceid/:accountid/:accountkey', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/console/deals/tueditdeals/tueditdeals.module#EditDealsModule' },
      { path: 'dealsoverview', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/console/deals/tudealspurchaseoverview/tuoverview.module#TuOverviewTrendModule' },
      { path: 'externalurl', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/console/deals/tuexternalurl/tuexternalurl.module#TUExternalUrlModule' },
      { path: 'apppromotion', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/console/deals/tuapppromotion/tuapppromotion.module#TUApppromotionModule' },

      { path: 'dealsmerchant', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/console/deals/tudealsmerchant/tudealsmerchant.module#TUDealsMerchantModule' },
      { path: 'dealscustomer', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/console/deals/tudealscustomer/tudealscustomer.module#TUDealsCustomerModule' },
      { path: 'soldhistory', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/console/deals/tusoldhistory/tusoldhistory.module#TUSoldHistoryModule' },
      { path: 'dealredeemhistory', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/console/deals/turedeemhistory/turedeemhistory.module#TURedeemHistoryModule' },
      { path: 'duplicatedeal/:referencekey/:referenceid/:accountid/:accountkey', data: { 'permission': 'deals', PageName: 'System.Menu.DealDetails' }, loadChildren: '../../panel/console/deals/tuduplicatedeal/tuduplicatedeal.module#DuplicateDealModule' },
      { path: 'editduplicatedeal/:referencekey/:referenceid/:accountid/:accountkey', data: { 'permission': 'deals', PageName: 'System.Menu.DealDetails' }, loadChildren: '../../panel/console/deals/tueditduplicatedeals/tueditduplicatedeals.module#EditDuplicateDealsModule' },
      //Reports
      { path: 'dailyreport/tusalereports', data: { 'permission': 'deals', PageName: 'System.Menu.SaleReports' }, loadChildren: '../../panel/reports/tusale/tusale.module#TUSaleModule' },

      //Analytics

      //  { path: 'promoanalytics', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../dashboards/tuanalyticsreferal/tuanalyticsreferal.module#TUAnalyticsReferalModule' },

      { path: 'todayoverviews', data: { 'permission': 'deals', PageName: 'System.Menu.TodaysOverview' }, loadChildren: '../../panel/console/analytics/tutodayoverviews/tutodayoverviews.module#TuTodayOverviewModule' },
      { path: 'growthreveneue', data: { 'permission': 'deals', PageName: 'System.Menu.GrowthRevenue' }, loadChildren: '../../panel/console/analytics/tugrowthrevenue/tugrowthrevenue.module#TuGrowthRevenueModule' },
      { path: 'merchantanalysis', data: { 'permission': 'deals', PageName: 'System.Menu.MerchantAnalytics' }, loadChildren: '../../panel/console/analytics/tumerchantanalysis/tumerchantanalysis.module#TuMerchantAnalysisModule' },
      { path: 'customeranalysis', data: { 'permission': 'deals', PageName: 'System.Menu.CustomerAnalytics' }, loadChildren: '../../panel/console/analytics/tucustomeranalysis/tucustomeranalysis.module#TuCustomerAnalysisModule' },
      { path: 'analyticoverview', data: { 'permission': 'deals', PageName: 'System.Menu.Overview' }, loadChildren: '../../panel/console/analytics/tuoverview/tuoverview.module#TuOverviewModule' },

      // canActivate: [MerchantguardGuard],
      //Wallets
      { path: 'customerwallets', data: { 'permission': 'deals', PageName: 'System.Menu.CustomerWallets' }, loadChildren: '../../panel/accounts/tuwallets/tucustomerwallets/tucustomerwallets.module#TUCustomerWalletsModule' },
      { path: 'merchantwallets', data: { 'permission': 'deals', PageName: 'System.Menu.MerchantWallets' }, loadChildren: '../../panel/accounts/tuwallets/tumerchantwallets/tumerchantwallets.module#TUMerchantWalletsModule' },

      { path: 'customerwallethistory', data: { 'permission': 'deals', PageName: 'System.Menu.CustomerWallets' }, loadChildren: '../../panel/accounts/tuwallethistory/tucustomerwallethistory/tucustomerwallethistory.module#TucustomerwallethistoryModule' },
      { path: 'merchantwallethistory', data: { 'permission': 'deals', PageName: 'System.Menu.MerchantWallets' }, loadChildren: '../../panel/accounts/tuwallethistory/tumerchantwallethistory/tumerchantwallethistory.module#TumerchantwallethistoryModule' },

      //Cashouts

      { path: 'cashouts', data: { 'permission': 'deals', PageName: 'System.Menu.Cashouts' }, loadChildren: '../../panel/accounts/tucashouts/tucashoutsale/tupayments.module#TUPaymentsModule' },
      { path: 'cashouts/:referencekey/:referenceid/:accountid/:accountkey', data: { 'permission': 'deals', PageName: 'System.Menu.Cashouts' }, loadChildren: '../../panel/accounts/tucashouts/tucashoutsale/tupayments.module#TUPaymentsModule' },

      { path: 'merchantcashouts', data: { 'permission': 'deals', PageName: 'System.Menu.Cashouts' }, loadChildren: '../../panel/accounts/tucashouts/tumerchantcashouts/tumerchantcashouts.module#TUMerchantCashoutsModule' },
      { path: 'merchantcashouts/:referencekey/:referenceid/:accountid/:accountkey', data: { 'permission': 'deals', PageName: 'System.Menu.Cashouts' }, loadChildren: '../../panel/accounts/tucashouts/tumerchantcashouts/tumerchantcashouts.module#TUMerchantCashoutsModule' },

      //Refunds
      { path: 'supercash', data: { 'permission': 'refunds', PageName: 'System.Menu.SuperCash' }, loadChildren: '../../panel/accounts/tucsupercash/tucsupercash/tucsupercash.module#TUSuperCashModule' },
      { path: 'refunds', data: { 'permission': 'refunds', PageName: 'System.Menu.Refunds' }, loadChildren: '../../panel/accounts/turefunds/turefunds/turefunds.module#TURefundsModule' },
      { path: "downloads", data: { permission: "account", PageName: "System.Menu.Downloads" }, loadChildren: "../../panel/accounts/tudownloads/tudownloads.module#TUDownloadsModule" },
      { path: 'refbonus', data: { 'permission': 'refbonus', PageName: 'REFERRAL BONUS' }, loadChildren: '../../panel/accounts/turefbonus/turefbonus/turefbonus.module#TURefbonusModule' },
      { path: 'referralb', data: { 'permission': 'referralb', PageName: 'REFERRAL ' }, loadChildren: '../../panel/accounts/turef/turef/turef.module#TURefModule' },

      
      // SmsNotification
      { path: 'smsnotification', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../panel/administration/tusmsnotifications/tusmsnotifications.module#TUSmsNotificationModule' },

      // Loan
      { path: 'loanrequests', data: { 'permission': 'administration', PageName: 'Loan Requests' }, loadChildren: '../../panel/tuloan/tuloanrequests/tuloanrequests.module#TuloanrequestsModule' },
      { path: 'loan', data: { 'permission': 'administration', PageName: 'Repayments' }, loadChildren: '../../panel/tuloan/tuloanbase/tuloanbase.module#DealLoanBaseModule' },

      // { path: 'loanactive', data: { 'permission': 'administration', PageName: 'System.Menu.Loan' }, loadChildren: '../../panel/tuloan/tuloanactive/tuloanactive.module#TULoanActiveModule' },
      { path: 'loanactivity', data: { 'permission': 'administration', PageName: 'All loans' }, loadChildren: '../../panel/tuloan/tuloanactivity/tuloanactivity.module#DealLoanActivityModule' },
      { path: 'loandetail/:referencekey/:referenceid', data: { 'permission': 'administration', PageName: 'Loan' }, loadChildren: '../../panel/tuloan/tuloandetails/tuloandetails.module#TULoanDetailsModule' },

      //

      { path: 'conpendingtransaction', data: { 'permission': 'administration', PageName: 'System.Menu.PendingTransaction' }, loadChildren: '../../modules/transactions/tusale/Pendingtransaction/console/pendingtransaction/pendingtransaction.module#TUPendingTransactionModule' },

      // SMS
      { path: "smsmerchantdetail", data: { permission: "dashboard", PageName: "System.Menu.Merchant" }, loadChildren: "../../panel/smscampaign/Merchant/tumerchantdetails/tumerchantdetails.module#TUMerchantDModule" },
      { path: "smsparentmerchantdetail", data: { permission: "dashboard", PageName: "System.Menu.Merchant" }, loadChildren: "../../panel/smscampaign/tumerchantdetails/tumerchantdetails.module#TUMerchantDetailsModule" },
      { path: "smscampaign", data: { permission: "dashboard", PageName: "System.Menu.Merchant" }, loadChildren: "../../panel/smscampaign/tusmscampaign/tusmscampaign.module#TUSmscampaignDModule" },

      { path: "merchantsms", data: { permission: "dashboard", PageName: "System.Menu.Merchant" }, loadChildren: "../../panel/smscampaign/Merchant/merchantsms/merchantsms.module#TUMerchantSMSModule" },
      { path: "smscampaigns", data: { permission: "dashboard", PageName: "System.SMS.Campaigns" }, loadChildren: "../../panel/smscampaign/tuallsmscampaign/tuallsmscampaign.module#TUAllSMSCampaignModule" },
      { path: "addcampaigns", data: { permission: "dashboard", PageName: "System.SMS.AddCampaign" }, loadChildren: "../../panel/smscampaign/tuaddcampaign/tuaddcampaign.module#AddCampaignModule" },
      { path: "smsoverview", data: { permission: "dashboard", PageName: "System.Menu.Dashboard" }, loadChildren: "../../panel/smscampaign/smsoverview/smsoverview.module#TUOverviewModule" },
      { path: "campaigndetail/:accountkey/:accountid/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.SMS.Campaign" }, loadChildren: "../../panel/smscampaign/tucampaigndetail/tucampaigndetail.module#CampaignDetailModule" },
      { path: "credithistory", data: { permission: "dashboard", PageName: "System.Menu.Merchant" }, loadChildren: "../../panel/smscampaign/creditshistory/creditshistory.module#TUCreditsHistoryModule" },

      //BNPL
      { path: 'bnpl', data: { permission: 'administration' }, loadChildren: '../../bnpl/bnpl.module#BnplModule' },

      // new merchantOnboarding

      { path: 'merchantOnboardingBusiness', data: { 'permission': 'merchants', PageName: 'System.Menu.merchantOnboardingBusiness' }, loadChildren: '../../pages/merchantOnboarding/businessDetail/businessDetail.module#businessDetailModule' },
      { path: 'merchantOnboardingAccount', data: { 'permission': 'merchants', PageName: 'System.Menu.merchantOnboardingAccount' }, loadChildren: '../../pages/merchantOnboarding/accountDetail/accountDetail.module#accountDetailModule' },
      //BNPL
      { path: 'bnpl', data: { permission: 'administration' }, loadChildren: '../../bnpl/bnpl.module#BnplModule' },

      // // Gift Card
      // { path: "giftcards", data: { permission: "customer", PageName: 'System.Menu.Merchant', accounttypecode: "customer" }, loadChildren: "../../merchant/giftcard/overview/tugiftcard.module#TuGiftCardModule" },
      // { path: "gcrewardhistory", data: { permission: "giftcard", PageName: 'System.Menu.GiftCard' }, loadChildren: '../../merchant/giftcard/Reward/tusale.module#TUSaleModule' },
      // { path: 'gcredeemhistory', data: { permission: 'giftcard', PageName: 'System.Menu.GiftCard' }, loadChildren: '../../merchant/giftcard/Redeemed/tusale.module#TUSaleModule' },

      //GIFT CARD
      { path: 'giftcards', data: { permission: 'giftcards' }, loadChildren: '../../gift-card/gift-card.module#GiftCardModule' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUMerchantRoutingModule { }

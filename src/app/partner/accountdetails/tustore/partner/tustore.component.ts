import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable, Subscription } from "rxjs";
import { DataHelperService, HelperService, OResponse, OSelect, OUserDetails, FilterHelperService } from "../../../../service/service";
declare let $: any;
import { HCoreXAddress, HCXAddressConfig, locationType } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';



@Component({
  selector: "tu-store",
  templateUrl: "./tustore.component.html"
})
export class TUStoreComponent implements OnInit, OnDestroy {

  public _isAddressLoaded = false;

  public _Address: HCoreXAddress = {};
  public _AddressStore: HCoreXAddress = {};

  public _AddressConfig: HCXAddressConfig =
    {
      locationType: locationType.form,
    }

  AddressChange(Address) {
    this._Address = Address;
  }
  subscription: Subscription;
  ReloadSubscription: Subscription;
  merchant_DisplayName:any;

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.ReloadSubscription.unsubscribe();
  }

  //#endregion

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) { }

  //#region MapCorrection 

  slideOpen: any = false;
  changeSlide(): void {
    this.slideOpen = !this.slideOpen;
    if (this.slideOpen) {
      // this._HelperService._MapCorrection();
    }
  }

  //#endregion

  //#region ToogleDetailView 

  HideStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-ShowStoreDetail");
  }

  ShowStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-ShowStoreDetail");
    element.classList.remove("Hm-HideDiv");
  }

  //#endregion

  BackDropInit(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }
  public OwnerDisplayName: string = null
  ngOnInit() {

    //#region UIInit 

    Feather.replace();
    this._HelperService.ContainerHeight = window.innerHeight;
    this._HelperService.AppConfig.ShowHeader = true;

    this.BackDropInit();
    // this._HelperService._InitMap();

    //#endregion

    //#region Subscriptions 

    this.subscription = this._HelperService.isprocessingtoogle
      .subscribe((item) => {
        this._ChangeDetectorRef.detectChanges();
      });

    this.ReloadSubscription = this._HelperService.ReloadEventEmit.subscribe((number) => {

    });

    //#endregion

    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveStore);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.Partner.ActiveStoreReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.Partner.ActiveStoreReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
      this.OwnerDisplayName = StorageDetails.OwnerDisplayName;
      this.merchant_DisplayName=StorageDetails.merchantDisplayName

    }
    console.log("StorageDetails",StorageDetails);

    this._HelperService.Get_UserAccountDetails(false);

    //#region DropdownInit 

    this.GetSoresDetails();
    this.GetBranches_List();
    this.GetMangers_List();

    //#endregion

    this.Form_EditUser_Load();
  }

  //#region EditUser 

  Form_EditUser: FormGroup;

  //#region EditUser_Address 

  _CurrentAddress: any = {};

  Form_EditUser_Address: string = null;
  Form_EditUser_Latitude: number = 0;
  Form_EditUser_Longitude: number = 0;

  @ViewChild('placesStore') placesStore: GooglePlaceDirective;

  Form_EditUser_PlaceMarkerClick(event) {
    this.Form_EditUser_Latitude = event.coords.lat;
    this.Form_EditUser_Longitude = event.coords.lng;
  }
  public Form_EditUser_AddressChange(address: Address) {
    this.Form_EditUser_Latitude = address.geometry.location.lat();
    this.Form_EditUser_Longitude = address.geometry.location.lng();
    this.Form_EditUser_Address = address.formatted_address;
    this.Form_EditUser.controls['Latitude'].setValue(this.Form_EditUser_Latitude);
    this.Form_EditUser.controls['Longitude'].setValue(this.Form_EditUser_Longitude);
    this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
  }

  //#endregion

  Form_EditUser_Show() {
    this._HelperService.OpenModal("Form_EditUser_Content");
  }
  Form_EditUser_Close() {
    // this._Router.navigate([
    //     this._HelperService.AppConfig.Pages.System.AdminUsers
    // ]);
    this._HelperService.OpenModal("Form_EditUser_Content");
  }
  Form_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;
    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;
    this.Form_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.UpdateStore,
      AccountKey: this._HelperService.AppConfig.Partner.ActiveStoreReferenceKey,
      AccountId: this._HelperService.AppConfig.Partner.ActiveStoreReferenceId,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      BranchId: [null],
      BranchKey: [null],
      RmId: [null],
      RmKey: [null],
      DisplayName: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      Name: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      ContactNumber: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: ['', Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],

      // Address: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      // CityName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      // StateName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      // CountryName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      // Latitude: [null, Validators.compose([Validators.required])],

      Address: [null],
      CityName: [null],
      StateName: [null],
      CountryName: [null],
      Latitude: 0,
      Longitude: 0,
      StatusCode: this._HelperService.AppConfig.Status.Active,
    });
  }
  Form_EditUser_Clear() {
    //this.Form_EditUser.reset();
    //this.Form_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_EditUser_Process(_FormValue: any) {
    // _FormValue.DisplayName = _FormValue.FirstName;
    // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;

    if (this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "") {
      this._HelperService.NotifyError("Please select address");
    }
    else {

      // // _FormValue.Latitude = this.Form_EditUser_Latitude;
      // // _FormValue.Longitude = this.Form_EditUser_Longitude;
      // _FormValue.Location = this._Address;
      _FormValue.Address = this._Address.Address;
      _FormValue.AddressComponent = this._Address;
      _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveReferenceKey;
      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(
        this._HelperService.AppConfig.NetworkLocation.V3.Account,
        _FormValue
      );
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess("Account created successfully");
            this._HelperService.CloseModal('off-canvas')
            this.Form_EditUser_Clear();
            this.RemoveOffCanvas();
            this.GetSoresDetails();
            // if (_FormValue.OperationType == "close") {
            //   this.Form_EditUser_Close();
            // }
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );
    }
  }

  //#endregion

  //#region OffCanvasNBackdrop 
  @ViewChild("offCanvas") divView: ElementRef;

  ShowOffCanvas() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  RemoveOffCanvas() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }
  //#endregion

  //#region Dropdowns 

  //#region Branches 

  public GetBranches_Option: Select2Options;
  public GetBranches_Transport: any;
  GetBranches_List() {
    var PlaceHolder = "Select Branch";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetBranches,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },

        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: 'StatusCode',
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: '=',
        //     SearchValue: this._HelperService.AppConfig.Status.Active,
        // }
      ]
    }
    this.GetBranches_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetBranches_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetBranches_Transport,
      multiple: false,
    };
  }


  GetBranches_ListChange(event: any) {
    // alert(event);\
    this.Form_EditUser.patchValue(
      {
        BranchKey: event.data[0].ReferenceKey,
        BranchId: event.data[0].ReferenceId
      }
    );

  }
  //#endregion

  //#region Managers 

  public GetMangers_Option: Select2Options;
  public GetMangers_Transport: any;
  GetMangers_List() {

    var PlaceHolder = "Select RM";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetManagers,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },

      ]
    }

    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'RoleId', this._HelperService.AppConfig.DataType.Text, '8', '=');
    this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetMangers_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetMangers_Transport,
      multiple: false,
    };
  }
  GetMangers_ListChange(event: any) {
    // alert(event);
    this.Form_EditUser.patchValue(
      {
        RmKey: event.data[0].ReferenceKey,
        RmId: event.data[0].ReferenceId
      }
    );
  }

  //#endregion

  //#endregion

  //#region StoreDetails 

  public _StoreDetails: any =
    {
      ManagerName: null,
      BranchName: null,
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,

      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
      // 
      Address: null,
      AddressComponent:
      {
        Latitude: 0,
        Longitude: 0,
      },
      // 
    }
  GetSoresDetails() {

    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStore,
      ReferenceKey: this._HelperService.AppConfig.Partner.ActiveStoreReferenceKey,
      ReferenceId: this._HelperService.AppConfig.Partner.ActiveStoreReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Partner.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._StoreDetails = _Response.Result;
          // debugger;

          this._isAddressLoaded = true;
          // console.log("this._StoreDetails.AddressComponent;", this._StoreDetails.AddressComponent);
          this._Address = this._StoreDetails.AddressComponent;
          setTimeout(() => {
            this._isAddressLoaded = true;
          }, 300);

          this._ChangeDetectorRef.detectChanges();
          //#region ResponseInit 
          this.GetBranches_Option.placeholder = this._StoreDetails.BranchName;
          this.GetMangers_Option.placeholder = this._StoreDetails.ManagerName;

          this._StoreDetails.EndDateS = this._HelperService.GetDateS(this._StoreDetails.EndDate);
          this._StoreDetails.CreateDateS = this._HelperService.GetDateTimeS(this._StoreDetails.CreateDate);
          this._StoreDetails.ModifyDateS = this._HelperService.GetDateTimeS(this._StoreDetails.ModifyDate);
          this._StoreDetails.StatusI = this._HelperService.GetStatusIcon(this._StoreDetails.StatusCode);
          this._StoreDetails.StatusB = this._HelperService.GetStatusBadge(this._StoreDetails.StatusCode);
          this._StoreDetails.StatusC = this._HelperService.GetStatusColor(this._StoreDetails.StatusCode);

          //#endregion

          //#region InitLocationParams 
          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {

            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;

            this.Form_EditUser_Latitude = _Response.Result.Latitude;
            this.Form_EditUser_Longitude = _Response.Result.Longitude;

            this.Form_EditUser.controls['Latitude'].setValue(_Response.Result.Latitude);
            this.Form_EditUser.controls['Longitude'].setValue(_Response.Result.Longitude);

          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }

          // this._HelperService._ReLocate();
          //#endregion

        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion
  Form_EditUser_Block() {

  }
  BlockStore() {
    this._HelperService.OpenModal("BlockPos");
    this.RemoveOffCanvas();
  }
  public Block(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "disablestore",
      AccountId: this._StoreDetails.ReferenceId,
      AccountKey: this._StoreDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess("Store Disabled Successfully ");
          this._HelperService.CloseModal("BlockPos");
          this.GetSoresDetails();
          this.ShowOffCanvas();

        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public UnBlock(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "enablestore",
      AccountId: this._StoreDetails.ReferenceId,
      AccountKey: this._StoreDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess("Store Enabled Successfully ");
          this._HelperService.CloseModal("BlockPos");
          this.GetSoresDetails();
          this.ShowOffCanvas();


        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

}

export class OAccountOverview {
  public Merchants: number;
  public Stores: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}

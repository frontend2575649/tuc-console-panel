import { AgmCoreModule } from '@agm/core';
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Select2Module } from "ng2-select2";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgxPaginationModule } from "ngx-pagination";
import { DynamicRoutesguardGuard } from 'src/app/service/guard/dynamicroutes.guard';
import { MainPipe } from '../../../service/main-pipe.module';
import { TUPartnerComponent } from "./tupartner.component";



const routes: Routes = [
    {
        path: "",
        component: TUPartnerComponent,
        canActivate:[DynamicRoutesguardGuard],
        data:{accessName:['viewpartner']},
        children: [
            { path: ":referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../partner/dashboards/tudetailpartner/dashboard.module#TUDashboardModule" },
            { path: "partneraccount/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../partner/dashboards/tudetailpartner/dashboard.module#TUDashboardModule" },
            { path: "dashboard/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../partner/dashboards/root/dashboard.module#TUDashboardModule" },
            { path: "merchants/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../partner/account/tumerchants/tumerchants.module#TUMerchantsModule" },
            { path: "stores/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../partner/account/tustores/partner/tustores.module#TUStoresModule" },
            { path: "terminals/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../partner/account/tuterminals/partner/tuterminals.module#TUTerminalsModule" },
            { path: "subaccounts/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../partner/account/subadmins/subadmins.module#SubadminsModule" },
            
            { path: 'activity/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.Terminal' }, loadChildren: '../../../partner/dashboards/activity/tuactivity.module#TUActivityModule' },
            { path: 'saleshistory/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.Terminal' }, loadChildren: '../../../partner/transactions/tusale/partner/tusale.module#TUSaleModule' },
            { path: 'bulkmerchant/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../partner/account/bulkmerchant/merchantupload.module#TUMerchantUploadModule' },

            { path: "merchant", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../partner/accountdetails/tumerchant/partner/tumerchant.module#TUMerchantModule" },
            { path: "store", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../partner/accountdetails/tustore/partner/tustore.module#TUStoreModule" },
            { path: "terminal", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../partner/accountdetails/tuterminal/partner/tuterminal.module#TUTerminalModule" },

            { path: 'merchantonboarding/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.AddMerchant' }, loadChildren: '../../../partner/accountcreation/merchantonboarding/merchantonboarding.module#TUMerchantOnboardingModule' },
            { path: "bulkcustomers/:referencekey/:referenceid", data: { permission: "terminals", PageName: "System.Menu.Customers" }, loadChildren: "../../../partner/account/bulkcustomer/customerupload.module#TUCustomerUploadModule" },
            { path: "allcustomers/:referencekey/:referenceid", data: { permission: "terminals", PageName: "System.Menu.Customers" }, loadChildren: "../../account/tucustomers/tucustomers.module#TUCustomersModule" },
            {
              path: "bulkcustomerupload/:referencekey/:referenceid",
              data: { permission: "acquirer", PageName: "System.Menu.CustomerUpload" },
              loadChildren:
                "../../account/bulkcustomerfiles/customerupload.module#TUCustomerUploadModule"
            },

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUPartnerRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUPartnerRoutingModule,
        GooglePlaceModule,
        MainPipe,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),

    ],
    declarations: [TUPartnerComponent]
})
export class TUPartnerModule { }

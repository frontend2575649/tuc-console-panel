import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, FilterHelperService } from '../../../service/service';
import swal from 'sweetalert2';
import * as XLSX from 'xlsx';
declare var $: any;
declare var moment:any;
type AOA = any[][];
import * as Feather from "feather-icons";
@Component({
  selector: 'hc-tucustomers',
  templateUrl: './tucustomers.component.html',
})
export class TUCustomersComponent implements OnInit {
  public ResetFilterControls: boolean = true;

  title = 'XlsRead'
  file: File;
  arrayBuffer: any;
  filelist: any;
  public PostItem = {

    OperationType: 'new',
    Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,
    AccountId: this._HelperService.UserAccount.AccountId,
    AccountKey: this._HelperService.UserAccount.AccountKey,
    Data: [],
    StatusCode: this._HelperService.AppConfig.Status.Inactive,


  };

  public SelectedDay = null;
  public SelectedMonth = null;
  public SelectedYear = null;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {

  }
  @ViewChild("offCanvas") divView: ElementRef;


  ngOnInit() {


    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveCustomer);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
    }

    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];

      Feather.replace();
      this.InitBackDropClickEvent();
      this.MerchantsList_Setup();
      this.MerchantsList_Filter_Owners_Load();
      this.Form_AddCustomer_Load();


    });



  }
  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  clicked() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

  RouteCustomer(ReferenceData) {
    //#region Save Current Merchant To Storage 

    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveCustomer,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Customer,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.CustomerPanel.Accounts.WalletInfo,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);

    //#endregion

  }


  IsUploading = false;
  UploadCount = 0;
  TotalCount = 0;
  CustomersList: CustomerImport[] = [];
  data: AOA = [];
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';
  onFileChange(evt: any) {
    this.UploadCount = 0;
    this.TotalCount = 0;
    this.CustomersList = [];
    this.IsUploading = false;
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
      for (let index = 1; index < this.data.length; index++) {
        const CustomerInfo = this.data[index];
        var DisplayName = CustomerInfo[0];
        var CompanyName = CustomerInfo[1];
        var BusinessContactNumber = CustomerInfo[2];
        var BusinessEmailAddress = CustomerInfo[2];
        var BusinessType = CustomerInfo[2];
        var BusinessAddress = CustomerInfo[2];
        var ContactFirstName = CustomerInfo[2];
        var ContactLastName = CustomerInfo[2];
        var ContactMobileNumber = CustomerInfo[2];
        var RewardPercentage = CustomerInfo[2];
        if (BusinessType == "online") {
          BusinessType = this._HelperService.AppConfig.AccountOperationType.Online
        }
        else if (BusinessType == "offline") {
          BusinessType = this._HelperService.AppConfig.AccountOperationType.Offline
        }
        else {
          BusinessType = this._HelperService.AppConfig.AccountOperationType.OnlineAndOffline
        }
        if (DisplayName != undefined && DisplayName != undefined) {
          var AppUserInfo: CustomerImport =
          {
            BusinessAddress: BusinessAddress,
            BusinessContactNumber: BusinessContactNumber,
            BusinessEmailAddress: BusinessEmailAddress,
            CompanyName: CompanyName,
            BusinessType: BusinessType,
            ContactFirstName: ContactFirstName,
            ContactLastName: ContactLastName,
            ContactMobileNumber: ContactMobileNumber,
            DisplayName: DisplayName,
            RewardPercentage: RewardPercentage,
            Status: 'pending',
            Message: '',
          };
          this.CustomersList.push(AppUserInfo);
          this.TotalCount = this.TotalCount + 1;
        }
      }
    };
    reader.readAsBinaryString(target.files[0]);
  }




  export(): void {
    var Cus = this.CustomersList as any;
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(Cus);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'ImportReport');
    XLSX.writeFile(wb, this.fileName);
  }
  onUpload() {
    this.StartMerchantUpload();

  }

  // StartCustomerUpload() {
  //     if (this.CustomersList.length > 0) {
  //         this.IsUploading = true;
  //         for (let index = 0; index < this.CustomersList.length; index++) {
  //             const element = this.CustomersList[index];
  //             this.CustomersList[index].Status = "processing";
  //             this.CustomersList[index].Message = "sending data";
  //             var PostItem = {
  //                 OperationType: 'new',
  //                 Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,
  //                 AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
  //                 AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
  //                 RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
  //                 OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
  //                 DisplayName: element.DisplayName,
  //                 Name: element.CompanyName,
  //                 FirstName: element.ContactFirstName,
  //                 LastName: element.ContactLastName,
  //                 MobileNumber: element.ContactMobileNumber,
  //                 ContactNumber: element.BusinessContactNumber,
  //                 EmailAddress: element.BusinessEmailAddress,
  //                 Address: element.BusinessAddress,
  //                 Latitude: 0,
  //                 Longitude: 0,
  //                 RegionKey: '',
  //                 RegionAreaKey: '',
  //                 CityKey: '',
  //                 CityAreaKey: '',
  //                 PostalCodeKey: '',
  //                 CountValue: 0,
  //                 AverageValue: 0,
  //                 StatusCode: this._HelperService.AppConfig.Status.Inactive,
  //                 Configuration: [{
  //                     SystemName: 'rewardpercentage',
  //                     Value: element.RewardPercentage,
  //                 }],
  //             };
  //             this._HelperService.IsFormProcessing = true;
  //             let _OResponse: Observable<OResponse>;
  //             _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, PostItem);
  //             _OResponse.subscribe(
  //                 _Response => {
  //                     this._HelperService.IsFormProcessing = false;
  //                     this.UploadCount = this.UploadCount + 1;
  //                     if (_Response.Status == this._HelperService.StatusSuccess) {
  //                         this.CustomersList[index].Status = "success";
  //                         this.CustomersList[index].Message = "merchant added";
  //                     }
  //                     else {
  //                         this.CustomersList[index].Status = "failed";
  //                         this.CustomersList[index].Message = "merchant already present";
  //                     }
  //                     if (index == (this.CustomersList.length - 1)) {
  //                         this.IsUploading = false;
  //                         this._HelperService.NotifySuccess('Merchants import successfull');
  //                     }


  //                 },
  //                 _Error => {
  //                     this.UploadCount = this.UploadCount + 1;
  //                     if (index == (this.CustomersList.length - 1)) {
  //                         this.IsUploading = false;
  //                         this._HelperService.NotifySuccess('Merchants import successfull');
  //                     }
  //                     this._HelperService.IsFormProcessing = false;
  //                     this._HelperService.HandleException(_Error);
  //                 });
  //         }
  //     }
  //     else {
  //         this._HelperService.NotifyError('Select file to upload merchants');
  //     }

  // }

  public NameFile: string = null;
  addfile(event) {

    this.file = event.target.files[0];
    this.NameFile = this.file.name;
    let fileReader = new FileReader();
    fileReader.readAsArrayBuffer(this.file);
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      var data = new Uint8Array(this.arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      this.filelist = XLSX.utils.sheet_to_json(worksheet, { raw: true });
    }
  }

  StartMerchantUpload() {
    if (this.filelist.length > 0 && this.filelist.length <= 1000) {

      this.IsUploading = true;

      // this.filelist[index].Status = "processing";
      // this.filelist[index].Message = "sending data";
      var PostItem = {
        OperationType: 'new',
        Task: this._HelperService.AppConfig.Api.Core.OnBoardCustomers,
        AccountId: this._HelperService.UserAccount.AccountId,
        AccountKey: this._HelperService.UserAccount.AccountKey,
        FileName: this.NameFile,
        Data: this.filelist,
      };
      this._HelperService.IsFormProcessing = true;
      PostItem.FileName = this.NameFile;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.OnBoard, PostItem);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          this.UploadCount = this.UploadCount + 1;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this.filelist.Status = "success";
            this.filelist.Message = "Customer added";
            this._HelperService.NotifySuccess('Customer import successfull');
            this._HelperService.CloseModal('SampleSheet');
            this.MerchantsList_Setup();
          }
          else {
            this.filelist.Status = "failed";
            this.filelist.Message = "Customer already present";
          }
          if (this.filelist == (this.filelist.length - 1)) {
            this.IsUploading = false;
            this._HelperService.NotifySuccess('Customer import successfull');
          }


        },
        _Error => {
          this.UploadCount = this.UploadCount + 1;
          if (this.filelist == (this.filelist.length - 1)) {
            this.IsUploading = false;
            this._HelperService.NotifySuccess('Customer import successfull');
          }
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }
    else {
      this._HelperService.NotifyError('Select file to upload merchants');
    }
  }


  public MerchantsList_Config: OList;
  public CurrentRequest_Key: string;
  public CurrentRequest_Id?: any;
  public CustomerData: any = {
  };
  MerchantsList_Setup() {
    this.MerchantsList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.getcustomers,
      Location: this._HelperService.AppConfig.NetworkLocation.Partner.V3.Account,
      Title: "Customers",
      StatusType: "default",
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      // SubReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: "Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard,
        },
        {
          DisplayName: "Mobile No",
          SystemName: "MobileNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard,
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard,
        },
        {
          DisplayName: "ReferenceNumber",
          SystemName: "ReferenceNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard,
        },



        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard,
        },
      ]
    };
    this.MerchantsList_Config = this._DataHelperService.List_Initialize(
      this.MerchantsList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.PartnerCustomers,
      this.MerchantsList_Config
    );

    this.MerchantsList_GetData();
  }
  MerchantsList_ToggleOption(event: any, Type: any) {
    if (Type == "date") {
      this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
      this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
  }
    if (event != null) {
      for (let index = 0; index < this.MerchantsList_Config.Sort.SortOptions.length; index++) {
        const element = this.MerchantsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.MerchantsList_Config


    );

    this.MerchantsList_Config = this._DataHelperService.List_Operations(
      this.MerchantsList_Config,
      event,
      Type
    );

    if (
      (this.MerchantsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.MerchantsList_GetData();
    }

  }
  MerchantsList_GetData() {
    var TConfig = this._DataHelperService.List_GetDataTerm(
      this.MerchantsList_Config
    );
    this.MerchantsList_Config = TConfig;
  }

  MerchantsList_RowSelected(referenceData) {
    this.CurrentRequest_Key = referenceData.ReferenceKey;
    this.CurrentRequest_Id = referenceData.ReferenceId;
    this.CustomerData = referenceData;
    this.ListAppUsage_GetDetails();
  }


  ListAppUsage_GetDetails() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.getcustomer,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,
      AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceKey: this.CurrentRequest_Key,
      ReferenceId: this.CurrentRequest_Id,

      // Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this.CurrentRequest_Key, '='),
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.CustomerData = _Response.Result;
          this.CustomerData.CreateDate = this._HelperService.GetDateTimeS(this.CustomerData.CreateDate);
          this.CustomerData.ModifyDateS = this._HelperService.GetDateTimeS(this.CustomerData.ModifyDate);
          this.CustomerData.StatusB = this._HelperService.GetStatusBadge(this.CustomerData.StatusCode);

          this.CustomerData.StatusI = this._HelperService.GetStatusIcon(this.CustomerData.StatusCode);
          this.clicked()
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      });
  }
  public MerchantsList_Filter_Owners_Option: Select2Options;
  public MerchantsList_Filter_Owners_Selected = null;
  MerchantsList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant,
    //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     this._HelperService.AppConfig.AccountType.PosAccount
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.MerchantsList_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  MerchantsList_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.MerchantsList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.MerchantsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.MerchantsList_Filter_Owners_Selected,
        "="
      );
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.MerchantsList_Config.SearchBaseConditions
      );
      this.MerchantsList_Filter_Owners_Selected = null;
    } else if (event.value != this.MerchantsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.MerchantsList_Filter_Owners_Selected,
        "="
      );
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.MerchantsList_Config.SearchBaseConditions
      );
      this.MerchantsList_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.MerchantsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.MerchantsList_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.MerchantsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }
  //#endregion

  SetOtherFilters(): void {
    this.MerchantsList_Config.SearchBaseConditions = [];
    this.MerchantsList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.MerchantsList_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }
  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.MerchantsList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
    if (Type == 'Time') {
      this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
      this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
  }
    this.SetOtherFilters();

    this.MerchantsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.PartnerCustomers
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.PartnerCustomers
        );
        this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
        this.MerchantsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.MerchantsList_GetData();

    if (ButtonType == 'Sort') {
      $("#MerchantsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#MerchantsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
    this.SetOtherFilters();

    this.MerchantsList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.MerchantsList_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }
  CreateCashierRequest(_FormValue: any): void {

    // _FormValue.Address = {
    //   Latitude: this.Form_AddStore_Latitude,
    //   Longitude: this.Form_AddStore_Longitude,
    //   Address: this.Form_AddStore_Address,
    //   CityName: this._CurrentAddress.sublocality_level_2,
    //   StateName: this._CurrentAddress.sublocality_level_1,
    //   CountryName: this._CurrentAddress.country
    // }

    // _FormValue.Latitude = undefined;
    // _FormValue.Longitude = undefined;
    // _FormValue.MapAddress = undefined;

    var IconContent: any = undefined;

    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      IconContent = this._HelperService._Icon_Cropper_Data;
    }

    _FormValue.IconContent = IconContent;

    return _FormValue;
  }

  Form_AddCustomer: FormGroup;
  Form_AddCustomer_Show() {
    this._HelperService.Icon_Crop_Clear();
    this._HelperService.OpenModal("Form_AddCustomer_Content");
  }
  Form_AddCustomer_Close() {
    this._HelperService.CloseModal("Form_AddCustomer_Content");
  }
  Form_AddCustomer_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddCustomer = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.SaveCampaign,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
      StoreId: [null, Validators.required],
      StoreKey: [null, Validators.required],
      StatusCode: this._HelperService.AppConfig.Status.Active,
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      EmployeeCode: [null],
      GenderCode: this._HelperService.AppConfig.Gender.Male,
    });
  }
  Form_AddCustomer_Clear() {
    this.Form_AddCustomer.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddCustomer_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddCustomer_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;

    var Request = this.CreateCashierRequest(_FormValue);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.Account,
      Request
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.FlashSwalSuccess("New cashier has been added successfully",
            "Done! You have successfully created new Cashier");
          this._HelperService.ObjectCreated.next(true);
          this.Form_AddCustomer_Clear();
          this.Form_AddCustomer_Close();
          if (_FormValue.OperationType == "close") {
            this.Form_AddCustomer_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

}

export class CustomerImport {
  public DisplayName: string | null;
  public CompanyName: string | null;
  public BusinessContactNumber: string | null;
  public BusinessEmailAddress: string | null;
  public BusinessAddress: string | null;
  public BusinessType: string | null;
  public ContactFirstName: string | null;
  public ContactLastName: string | null;
  public ContactMobileNumber: string | null;
  public RewardPercentage: string | null;
  public Status: string | null;
  public Message: string | null;
}

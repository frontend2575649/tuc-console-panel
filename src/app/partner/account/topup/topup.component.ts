import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";

declare var $: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../service/service";
import { Observable } from 'rxjs';

@Component({
  selector: "tu-topup",
  templateUrl: "./topup.component.html",
})
export class TUTopupComponent implements OnInit {
  public ResetFilterControls: boolean = true;
  CurrentPlan: any = {
    Name: null,
    TypeName: null,
    SellingPrice: null,
    Policy: null,
    EndDate: null


  };
  FreePlan: any = {
    Name: null,
    TypeName: null,
    SellingPrice: null,
    Policy: null,


  };
  BasicPlan: any = {
    Name: null,
    TypeName: null,
    SellingPrice: null,
    Policy: null,
  };
  PremiumPlan: any = {
    Name: null,
    TypeName: null,
    SellingPrice: null,
    Policy: null,
  };


  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {



  }

  ngOnInit() {
    Feather.replace();
    this.TopUpHistoryList_Setup();
    this.GetBalance();
    // this.GetSubscription();

  }

  public SubscriptionId: number;
  public SubscriptionKey: string;
  selectPlan(plan: string) {
    if (plan == 'basic') {
      this.SubscriptionId = this.BasicPlan.ReferenceId
      this.SubscriptionKey = this.BasicPlan.ReferenceKey
    }
    if (plan == 'premium') {
      this.SubscriptionId = this.PremiumPlan.ReferenceId
      this.SubscriptionKey = this.PremiumPlan.ReferenceKey
    }
    if (plan == 'free') {
      this.SubscriptionId = this.FreePlan.ReferenceId
      this.SubscriptionKey = this.FreePlan.ReferenceKey
    }
    this._HelperService.CloseModal('Plans');
  }

  closePlanDetail() {
    this._HelperService.OpenModal('Plans');
  }
  closePremiumPlanDetail() {
    this._HelperService.OpenModal('Plans');
  }



  public _TransactionReference = null;
  public _AccountBalanceCreditAmount = 0;
  generateRandomNumber() {
    var TrRef = Math.floor(100000 + Math.random() * (999999 + 1 - 100000));
    return TrRef;
  }
  OpenPaymentOptions() {
    this._AccountBalanceCreditAmount = 0;
    var Ref = this.generateRandomNumber();
    this._TransactionReference = 'Tp' + '_' + this._HelperService.UserAccount.AccountId + '_' + Ref;
    this._HelperService.OpenModal('Form_AddUser_Content');
  }
  paymentDone(ref: any) {
    this.TransactionId = ref.trans
    if (ref != undefined && ref != null && ref != '') {
      if (ref.status == 'success') {
        this.CreditAmount();
      }
      else {
        this._HelperService.NotifyError('Payment failed');
      }
    }
    else {
      this._HelperService.NotifyError('Payment failed');
    }
    // this.title = 'Payment successfull';
  }
  public TransactionId
  public CreditAmount() {
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "topupaccount",
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      Amount: this._AccountBalanceCreditAmount,
      PaymentReference: this._TransactionReference,
      TransactionId: this.TransactionId
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscription, PostData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountBalanceCreditAmount = 0;
          this._TransactionReference = null;

          this._HelperService.NotifySuccess("Account credited");
          this.GetBalance();


        } else {
          this._HelperService.NotifySuccess(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  paymentCancel() {
  }

  //SUbscription Payment






  //end Subscription


  public _AccountBalance =
    {
      Credit: 0,
      Debit: 0,
      Balance: 0
    }
  public GetBalance() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: 'getaccountbalance',
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      Source: this._HelperService.AppConfig.TransactionSource.Merchant
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscription, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountBalance = _Response.Result;
          // this._AccountBalance.Balance = _Response.Result.Balance / 100;
          // this._AccountBalance.Credit = _Response.Result.Credit / 100;
          // this._AccountBalance.Debit = _Response.Result.Debit / 100;
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  OpenTopUpHistory() {
    this._HelperService.OpenModal('TopupHistory')
    this.TopUpHistoryList_Setup();
  }
  public TopUpHistoryList_Config: OList;
  TopUpHistoryList_Setup() {
    this.TopUpHistoryList_Config = {
      Id: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetTopupHistory,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Subscription,
      Title: "Available Stores",
      StatusType: "default",
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'TransactionDate',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },

      TableFields: [
        {
          DisplayName: "Reference Number",
          SystemName: "PaymentReference",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },


        {
          DisplayName: "Amount",
          SystemName: "Amount",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Cashiers",
          SystemName: "Cashiers",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",

        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "TransactionDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.TopUpHistoryList_Config = this._DataHelperService.List_Initialize(
      this.TopUpHistoryList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Merchant,
      this.TopUpHistoryList_Config
    );

    this.TopUpHistoryList_GetData();
  }
  TopUpHistoryList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.TopUpHistoryList_Config.Sort.SortOptions.length; index++) {
        const element = this.TopUpHistoryList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.TopUpHistoryList_Config


    );

    this.TopUpHistoryList_Config = this._DataHelperService.List_Operations(
      this.TopUpHistoryList_Config,
      event,
      Type
    );

    if (
      (this.TopUpHistoryList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TopUpHistoryList_GetData();
    }

  }
  TopUpHistoryList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.TopUpHistoryList_Config
    );
    this.TopUpHistoryList_Config = TConfig;
  }
  TopUpHistoryList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveStore,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;




  }
  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.TopUpHistoryList_Setup();

    if (ButtonType == 'Sort') {
      $("#TopUpHistoryList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#TopUpHistoryList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.TopUpHistoryList_Config);

    this.TopUpHistoryList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }




}

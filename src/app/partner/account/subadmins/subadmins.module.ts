import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { ImageCropperModule } from 'ngx-image-cropper';
import { SubadminsComponent } from './subadmins.component';
import { MainPipe } from '../../../service/main-pipe.module'
import { DynamicRoutesguardGuard } from 'src/app/service/guard/dynamicroutes.guard';

import { Ng5SliderModule } from 'ng5-slider';
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
const routes: Routes = [
  { path: '', canActivateChild: [DynamicRoutesguardGuard], data: { accessName: ['viewpartnersubaccounts', 'partnersubaccounts'] }, component: SubadminsComponent }
];
const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 1,
};
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubAdminsRoutingModule { }


@NgModule({
  declarations: [SubadminsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    SubAdminsRoutingModule,
    ImageCropperModule,
    MainPipe,
    Ng5SliderModule,
    InputFileModule.forRoot(config),

  ]
})
export class SubadminsModule { }

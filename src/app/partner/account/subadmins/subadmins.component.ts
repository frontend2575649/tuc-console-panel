import { Component, ElementRef, ViewChild, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from 'feather-icons';
declare var $: any;
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { InputFileComponent, InputFile } from 'ngx-input-file';
import {
  OSelect,
  OList,
  DataHelperService,
  HelperService,
  OResponse,
  FilterHelperService
} from "../../../service/service";
import swal from "sweetalert2";
import * as cloneDeep from 'lodash/cloneDeep';
declare var moment: any;

@Component({
  selector: 'app-subadmins',
  templateUrl: './subadmins.component.html',
})
export class SubadminsComponent implements OnInit {
  _imageConfig =
    {

      Images: [],
      profileImages: [],
            StartDateConfig: {
            },
            EndDateConfig: {
            },
            DefaultStartDate: null,
            DefaultEndDate: null,
            StartDate: null,
            EndDate: null,
    }

  private InputFileComponent_Term: InputFileComponent;
  _ImageManager =
    {
      TCroppedImage: null,
      ActiveImage: null,
      ActiveImageName: null,
      ActiveImageSize: null,
      Option: {
        MaintainAspectRatio: "true",
        MinimumWidth: 800,
        MinimumHeight: 400,
        MaximumWidth: 800,
        MaximumHeight: 400,
        ResizeToWidth: 800,
        ResizeToHeight: 400,
        Format: "jpg",
      }
    }
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService: FilterHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,


  ) { }
  @ViewChild("offCanvas") divView: ElementRef;

  ngOnInit() {
    Feather.replace();
    this._HelperService.ValidateData();
    this.InitBackDropClickEvent();
    this._HelperService.StopClickPropogation();

    this.Form_AddUser_Load();
    this.Form_EditUser_Load();


    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
      if (
        this._HelperService.AppConfig.ActiveReferenceKey == null ||
        this._HelperService.AppConfig.ActiveReferenceId == null
      ) {
        this.SubAccountList_Setup();
        this.SubAccountList_Filter_Owners_Load();
        // this.GetSubAccountDetails();
      } else {
        this.SubAccountList_Setup();
        this.SubAccountList_Filter_Owners_Load();

      }
    });


  }

  clicked() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }
  Form_AddUser_Open() {
    this._HelperService.OpenModal('Form_AddUser_Content');
  }

  public SubAccountList_Config: OList;
  SubAccountList_Setup() {
    this.SubAccountList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.Core.GetSubAccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.Partner.V3.SubAccounts,
      Title: "Sub Accounts",
      StatusType: "default",
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      // DefaultSortExpression: 'CreateDate desc',
      TableFields: [
        {
          DisplayName: 'Name',
          SystemName: 'Name',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: 'Mobile Number',
          SystemName: 'MobileNumber',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          DefaultValue: 'ThankUCash',
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: 'Email Address',
          SystemName: 'EmailAddress',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: 'Role Name',
          SystemName: 'RoleName',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: 'text-right',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: 'Terminals',
          SystemName: 'Terminals',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: 'text-right',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date text-right',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
      ]
    };
    this.SubAccountList_Config = this._DataHelperService.List_Initialize(
      this.SubAccountList_Config
    );
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Cashier,
      this.SubAccountList_Config
    );
    this.SubAccountList_GetData();
  }
  SubAccountList_ToggleOption(event: any, Type: any) {
    if (event != null) {
      for (let index = 0; index < this.SubAccountList_Config.Sort.SortOptions.length; index++) {
        const element = this.SubAccountList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }
    if (Type == "date") {
      this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
      this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
  }
    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.SubAccountList_Config


    );

    this.SubAccountList_Config = this._DataHelperService.List_Operations(
      this.SubAccountList_Config,
      event,
      Type
    );

    if (
      (this.SubAccountList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.SubAccountList_GetData();
    }
  }
  SubAccountList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.SubAccountList_Config
    );
    this.SubAccountList_Config = TConfig;
  }
  public CurrentKey: string = null
  public CurrentId: number = null
  SubAccountList_RowSelected(ReferenceData) {

    this.CurrentKey = ReferenceData.ReferenceKey;
    this.CurrentId = ReferenceData.ReferenceId;
    // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.AccountDetails.SubAdminDetails, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
    this.GetSubAccountDetails();


  }


  public SubAccountList_Filter_Owners_Option: Select2Options;
  public SubAccountList_Filter_Owners_Selected = 0;
  SubAccountList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
      ]
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
      [
        this._HelperService.AppConfig.AccountType.Merchant,
        this._HelperService.AppConfig.AccountType.Acquirer,
        this._HelperService.AppConfig.AccountType.PGAccount,
        this._HelperService.AppConfig.AccountType.PosAccount
      ]
      , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.SubAccountList_Filter_Owners_Option = {
      placeholder: 'Sort by Referrer',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  SubAccountList_Filter_Owners_Change(event: any) {
    if (event.value == this.SubAccountList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.SubAccountList_Filter_Owners_Selected, '=');
      this.SubAccountList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.SubAccountList_Config.SearchBaseConditions);
      this.SubAccountList_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.SubAccountList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.SubAccountList_Filter_Owners_Selected, '=');
      this.SubAccountList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.SubAccountList_Config.SearchBaseConditions);
      this.SubAccountList_Filter_Owners_Selected = event.value;
      this.SubAccountList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.SubAccountList_Filter_Owners_Selected, '='));
    }
    this.SubAccountList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }
  Form_AddUser: FormGroup;
  Form_AddUser_Show() {
    this._HelperService.OpenModal("Form_AddUser_Content");
  }
  Form_AddUser_Close() {
    // this._Router.navigate([
    //     this._HelperService.AppConfig.Pages.System.AdminUsers
    // ]);
    this._imageConfig.profileImages = [];
    this._HelperService.CloseModal("Form_AddUser_Content");
  }
  Form_AddUser_Load() {
    this._HelperService._Icon_Cropper_Data.Width = 128;
    this._HelperService._Icon_Cropper_Data.Height = 128;
    this.Form_AddUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.Core.SaveSubAccount,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,
      AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
      RoleId: [7, Validators.required],
      RoleKey: ['admin', Validators.required],
      ImageContent: null,

      Name: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(128)
        ])
      ],
      MobileNumber: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(14)
        ])
      ],
      EmailAddress: [
        null,
        Validators.compose([
          Validators.required,
          Validators.email,
          Validators.minLength(2)
        ])
      ],
      StatusCode: this._HelperService.AppConfig.Status.Active,

    });
  }
  Form_AddUser_Clear() {
    this.Form_AddUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }

  ReFormat_RequestBody(): void {
    var formValue: any = cloneDeep(this.Form_AddUser.value);
    var formRequest: any = {

      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.Core.SaveSubAccount,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,
      AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
      StatusCode: formValue.StatusCode,
      "ImageContent": this._imageConfig.profileImages[0],
       RoleId: formValue.RoleId,
       RoleKey: formValue.RoleKey,
      "Name": formValue.Name,
      "MobileNumber": formValue.MobileNumber,
      "EmailAddress": formValue.EmailAddress,
        
    };
    return formRequest;
}
  Form_AddUser_Process(_FormValue: any) {
    // _FormValue.DisplayName = _FormValue.FirstName;
    // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
    let requestBody = this.ReFormat_RequestBody();
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.Partner.V3.SubAccounts,
      requestBody
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account created successfully");
          this.Form_AddUser_Clear();
          this._HelperService.CloseModal('Form_AddUser_Content');
          this.SubAccountList_Setup();
          this._imageConfig =
          {
              DefaultStartDate: null,
              DefaultEndDate: null,
              // SelectedDealCodeHours: 12,

              StartDate: null,
              EndDate: null,
              profileImages: [],

              Images: [],
              StartDateConfig: {
              },
              EndDateConfig: {
              },

          }
          this._ImageManager =
          {
              TCroppedImage: null,
              ActiveImage: null,
              ActiveImageName: null,
              ActiveImageSize: null,
              Option: {
                  MaintainAspectRatio: "true",
                  MinimumWidth: 800,
                  MinimumHeight: 400,
                  MaximumWidth: 800,
                  MaximumHeight: 400,
                  ResizeToWidth: 800,
                  ResizeToHeight: 400,
                  Format: "jpg",
              }
          }
          this._imageConfig.profileImages = [];
          this.removeImage();
          if (_FormValue.OperationType == "close") {
            this.Form_AddUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public _SubAccountDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      RoleName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,

      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }

  GetSubAccountDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetSubAccount,
      ReferenceKey: this.CurrentKey,
      ReferenceId: this.CurrentId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Partner.V3.SubAccounts, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._SubAccountDetails = _Response.Result;

          if (this._SubAccountDetails != undefined && this._SubAccountDetails.MobileNumber != undefined && this._SubAccountDetails.MobileNumber != null) {
            if (this._SubAccountDetails.MobileNumber.startsWith("234") || this._SubAccountDetails.MobileNumber.startsWith("233") || this._SubAccountDetails.MobileNumber.startsWith("254")) {
              this._SubAccountDetails.MobileNumber = this._SubAccountDetails.MobileNumber.substring(3, this._SubAccountDetails.length);
            }
          }

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }

          this._SubAccountDetails.StartDateS = this._HelperService.GetDateS(
            this._SubAccountDetails.StartDate
          );
          this._SubAccountDetails.EndDateS = this._HelperService.GetDateS(
            this._SubAccountDetails.EndDate
          );
          this._SubAccountDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._SubAccountDetails.CreateDate
          );
          this._SubAccountDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._SubAccountDetails.ModifyDate
          );
          this._SubAccountDetails.StatusI = this._HelperService.GetStatusIcon(
            this._SubAccountDetails.StatusCode
          );
          this._SubAccountDetails.StatusB = this._HelperService.GetStatusBadge(
            this._SubAccountDetails.StatusCode
          );
          this._SubAccountDetails.StatusC = this._HelperService.GetStatusColor(
            this._SubAccountDetails.StatusCode
          );

          this.clicked()



        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


  Form_EditUser: FormGroup;
  Form_EditUser_Show() {
    this.unclick();
    this._HelperService.OpenModal("Form_EditUser_Content");
  }
  Form_EditUser_Close() {
    // this._Router.navigate([
    //     this._HelperService.AppConfig.Pages.System.AdminUsers
    // ]);
    this._HelperService.CloseModal("Form_EditUser_Content");
    this.clicked();
  }
  Form_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_EditUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.Core.UpdateSubAccount,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,
      AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: null,
      ReferenceKey: null,
      RoleId: [7, Validators.required],
      RoleKey: ['admin', Validators.required],
      ImageContent: null,

      Name: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(128)
        ])
      ],
      MobileNumber: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(14)
        ])
      ],
      EmailAddress: [
        null,
        Validators.compose([
          Validators.required,
          Validators.email,
          Validators.minLength(2)
        ])
      ],
      StatusCode: this._HelperService.AppConfig.Status.Active,

    });
  }
  Form_EditUser_Clear() {
    this.Form_EditUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_EditUser_Process(_FormValue: any) {
    _FormValue.ReferenceId = this.CurrentId;
    _FormValue.ReferenceKey = this.CurrentKey;

    // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.Partner.V3.SubAccounts,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account created successfully");
          // this.Form_EditUser_Clear();
          this._HelperService.CloseModal('Form_EditUser_Content');
          this.clicked();
          this.SubAccountList_Setup();
          if (_FormValue.OperationType == "close") {
            this.Form_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  SetOtherFilters(): void {
    this.SubAccountList_Config.SearchBaseConditions = [];
    // this.SubAccountList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    // if (CurrentIndex != -1) {
    //   this.MerchantsList_Filter_Stores_Selected = null;
    //   this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    // }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {

    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.SubAccountList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.SubAccountList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.SubAccountList_Config);

    this.SetOtherFilters();
    if (Type == 'Time') {
      this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
      this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
  }
    this.SubAccountList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      // inputAttributes: {
      //   autocapitalize: "off",
      //   autocorrect: "off",
      //   maxLength: "4",
      //   minLength: "4",
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Cashier,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Cashier
        );
        this._FilterHelperService.SetMerchantConfig(this.SubAccountList_Config);
        this.SubAccountList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.SubAccountList_GetData();

    if (ButtonType == 'Sort') {
      $("#MerchantsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#MerchantsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.SubAccountList_Config);
    this.SetOtherFilters();

    this.SubAccountList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion
  public ResetFilterControls: boolean = true;

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    // this.MerchantsList_Filter_Stores_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }


  Form_Promote_Show() {
    this._HelperService.Icon_Crop_Clear();
    this.InitImagePicker(this.InputFileComponent_Term);
    this._HelperService.OpenModal("_Icon_Cropper_Modal");
    // this._HelperService.OpenModal("_PreviewGeneral");



  }

  CurrentImagesCount: number = 0;
  private InitImagePicker(InputFileComponent: InputFileComponent) {
    if (InputFileComponent != undefined) {
      this.CurrentImagesCount = 0;
      this._HelperService._InputFileComponent = InputFileComponent;
      InputFileComponent.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
  }
  Form_ManagePromote: FormGroup;

  onImageAccept(value) {
    setTimeout(() => {
        this.Form_AddUser.patchValue(
            {
                ImageContent: null,
            }
        );
    }, 300);
    this._ImageManager.ActiveImage = value;
    this._ImageManager.ActiveImageName = value.file.name;
    this._ImageManager.ActiveImageName = value.file.size;
}

  onImageAccept1(value) {
    this._ImageManager.ActiveImage = value;
    this._ImageManager.ActiveImageName = value.file.name;
    this._ImageManager.ActiveImageName = value.file.size;
  }
  Icon_B64Cropped(base64: string) {
    this._ImageManager.TCroppedImage = base64;
  }
  Icon_B64CroppedDone() {
    var ImageDetails = this._HelperService.GetImageDetails(this._ImageManager.TCroppedImage);
    var ImageContent =
    {
      //OriginalContent: this._ImageManager.TCroppedImage,
      Name: this._ImageManager.ActiveImageName,
      Size: this._ImageManager.ActiveImageSize,
      Extension: ImageDetails.Extension,
      Content: ImageDetails.Content
    };
   
    if (this._imageConfig.profileImages.length == 0) {
      this._imageConfig.profileImages.push(
        {
          //ImageContent: ImageItem,
          Name: this._ImageManager.ActiveImageName,
          Size: this._ImageManager.ActiveImageSize,
          Extension: ImageDetails.Extension,
          Content: ImageDetails.Content,
          IsDefault: 1,
        }
      );
    }
    else {
      this._imageConfig.profileImages.push(
        {
          //  ImageContent: ImageItem,
          Name: this._ImageManager.ActiveImageName,
          Size: this._ImageManager.ActiveImageSize,
          Extension: ImageDetails.Extension,
          Content: ImageDetails.Content,
          IsDefault: 0,
        }
      );
    }
    this._ImageManager.TCroppedImage = null;
    this._ImageManager.ActiveImage = null;
    this._ImageManager.ActiveImageName = null;
    this._ImageManager.ActiveImageSize = null;
  }
  Icon_Crop_Clear() {
    this._ImageManager.TCroppedImage = null;
    this._ImageManager.ActiveImage = null;
    this._ImageManager.ActiveImageName = null;
    this._ImageManager.ActiveImageSize = null;
    this._HelperService.CloseModal('_Icon_Cropper_Modal');
  }
  RemoveImage(Item) {
    this._imageConfig.profileImages = this._imageConfig.profileImages.filter(x => x != Item);
  }

  removeImage(): void {
    //this.CurrentImagesCount = 0;
    this._imageConfig.profileImages = []
  }

}
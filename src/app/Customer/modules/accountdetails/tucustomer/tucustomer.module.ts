import { AgmCoreModule } from '@agm/core';
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Select2Module } from "ng2-select2";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgxPaginationModule } from "ngx-pagination";
import { DynamicRoutesguardGuard } from 'src/app/service/guard/dynamicroutes.guard';
import { MainPipe } from '../../../../service/main-pipe.module';
import { TUCustomerComponent } from "./tucustomer.component";



const routes: Routes = [
    {
        path: "",
        component: TUCustomerComponent,
        canActivate: [DynamicRoutesguardGuard],
        data: { accessName: ['viewcustomer'] },
        children: [
            { path: "", data: { permission: "getcustomer", menuoperations: "ManageCustomer", accounttypecode: "customer" }, loadChildren: "../tucustomer/tucustomerdetails/tucustomerdetails.module#TUCustomerDetailsModule" },
            { path: "details", data: { permission: "getcustomer", menuoperations: "ManageCustomer", accounttypecode: "customer" }, loadChildren: "../tucustomer/tucustomerdetails/tucustomerdetails.module#TUCustomerDetailsModule" },

            { path: 'configurations/:referencekey/:referenceid', data: { 'permission': 'dashboard', PageName: 'System.Menu.Configuration' }, loadChildren: '../../../../modules/accounts/tuconsole/merchants/tuconfigurations/tuconfigurations.module#TUConfigurationManagerModule' },
            { path: "dashboard/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.Dashboard" }, loadChildren: "../../../../modules/dashboards/merchant/dashboard.module#TUDashboardModule" },
            { path: 'salesummary/:referencekey/:referenceid', data: { 'permission': 'salesummary', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../modules/dashboards/mybuisness/tusalesummary/tusalesummary.module#TUSaleSummaryModule' },

            { path: "purchasehistory/:referencekey/:referenceid", canActivateChild: [DynamicRoutesguardGuard], data: { accessName: ['customerdtlspurchasehistory'], permission: "dashboard", PageName: "System.Menu.PurchaseHistory" }, loadChildren: "../../transactions/tusale/tusale.module#TUSaleModule" },

            { path: "vashistory/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.VasHistory" }, loadChildren: "../../transactions/vashistory/tusale.module#TUSaleModule" },


            { path: "rewardhistory/:referencekey/:referenceid", canActivateChild: [DynamicRoutesguardGuard], data: { accessName: ['customerdtlsrewardhistory'], permission: "dashboard", PageName: "System.Menu.RewardHistory" }, loadChildren: "../../transactions/tusale/rewardhistory/tusale.module#TUSaleModule" },
            { path: "redeemhistory/:referencekey/:referenceid", canActivateChild: [DynamicRoutesguardGuard], data: { accessName: ['customerdtlsredeemhistory'], permission: "dashboard", PageName: "System.Menu.RedeemHistory" }, loadChildren: "../../transactions/tusale/redeemhistory/tusale.module#TUSaleModule" },
            { path: "rewardclaimhistory/:referencekey/:referenceid", canActivateChild: [DynamicRoutesguardGuard], data: { accessName: ['customerdtlsclaimhistory'], permission: "dashboard", PageName: "System.Menu.RewardClaim" }, loadChildren: "../../transactions/tusale/turewardclaim/turewardclaim.module#TURewardsClaimModule" },

            { path: "paymentshistory/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.RedeemHistory" }, loadChildren: "../../transactions/tusale/tupayments/tupayments.module#TUPaymentsModule" },

            // { path: "giftpointshistory/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.GiftCard" }, loadChildren: "../../transactions/tusale/tugiftpoints/tugiftpoints.module#TUGiftPointsModule" },
            //  { path: "giftcardshistory/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.GiftPoint" }, loadChildren: "../../transactions/tusale/tugiftcards/tugiftcards.module#TUGiftCardsModule" },
            { path: "giftpointshistory/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.GiftPoint" }, loadChildren: "../../transactions/tusale/tugiftpoints/tugiftpoints.module#TUGiftPointsModule" },
            { path: "giftcardshistory/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.GiftCard" }, loadChildren: "../../transactions/tusale/tugiftcards/tugiftcards.module#TUGiftCardsModule" },
            { path: "buypointshistory/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.BuyPoints" }, loadChildren: "../../transactions/tusale/tubuypoints/tubuypoints.module#TUBuyPointsModule" },

            // { path: 'terminals/:referencekey/:referenceid', data: { 'permission': 'terminals', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../modules/accounts/tuterminals/merchant/tuterminals.module#TUTerminalsModule' },
            // { path: 'stores/:referencekey/:referenceid', data: { 'permission': 'stores', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../modules/accounts/tustores/merchant/tustores.module#TUStoresModule' }
            { path: "dealhistory", data: { permission: "getcustomer", menuoperations: "ManageCustomer", accounttypecode: "customer" }, loadChildren: "../../../../../app/Customer/modules/transactions/tusale/tudealhistory/dealtransaction/dealtransaction.module#DealTransactionModule" },
            { path: "loanhistory/:referencekey/:referenceid", data: { permission: "getcustomer", menuoperations: "ManageCustomer", accounttypecode: "customer" }, loadChildren: "../../../../../app/Customer/modules/transactions/tusale/loanhistory/loanhistory.module#LoanHistoryModule" },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUCustomerRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUCustomerRoutingModule,
        GooglePlaceModule,
        MainPipe,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),

    ],
    declarations: [TUCustomerComponent]
})
export class TUCustomerModule { }

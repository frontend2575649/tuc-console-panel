import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { DataHelperService, FilterHelperService, HelperService, OResponse, OUserDetails } from "../../../../../service/service";
import { Observable, Subscription } from 'rxjs';
declare let $: any;
declare var d3: any;
import swal from 'sweetalert2';

@Component({
  selector: "tu-customerdetails",
  templateUrl: "./tucustomerdetails.component.html"
})
export class TUCustomerDetailsComponent implements OnInit {

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {

  }
  public _CustomerDataSubscription: Subscription = null;

  ngOnInit() {

    setTimeout(() => {
      this._HelperService.ValidateData();
    }, 500);

    const customerData = this._HelperService.GetStorage("hcactivecustomer");
    if (customerData.ReferenceId != undefined && customerData.ReferenceId != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = customerData.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = customerData.ReferenceId;

      this.GetCustomerDetails();

    }
    // this._CustomerDataSubscription = this._HelperService.CustomerData.subscribe(params => {
    //   this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
    //   this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
    //   if (this._HelperService.AppConfig.ActiveReferenceId != undefined && this._HelperService.AppConfig.ActiveReferenceId != null) {
    //     this.GetCustomerDetails();

    //   }
    //   else {
    //   }
    // });

    this.Form_EditUser_Load();

  }


  // ngOnDestroy(): void {
  //   this._CustomerDataSubscription.unsubscribe();
  // }



  BlockAccount() {
    swal({
      title: this._HelperService.AppConfig.CommonResource.BlockCustomerTitle,
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
        ' <label> Do you really want to block this customer?  </label>' +
        '<input type="text"  placeholder="Enter Comment"  id="swal-input1" class="swal2-input">' +
        '<input style="-webkit-text-security: disc;" type="text" placeholder="Enter Pin" maxlength="4"  id="swal-input2" class="swal2-input">',


      focusConfirm: false,
      inputClass: 'swalText',
      preConfirm: () => {
        return [
          document.getElementById('swal-input1')['value'],
          document.getElementById('swal-input2')['value']
        ]
      },
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocomplete: 'off',
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updateaccountstatus",
          AccountId: this._CustomerDetails.ReferenceId,
          AccountKey: this._CustomerDetails.ReferenceKey,
          StatusCode: "default.blocked",
          AuthPin: result.value[1],
          Comment: result.value[0],
          AccountTypeCode: this._HelperService.AppConfig.AccountType.Customer

        };

        let _OResponse: Observable<OResponse>;
        if (PostData.AuthPin != null && PostData.AuthPin != undefined && PostData.AuthPin != "") {
          _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, PostData);
          _OResponse.subscribe(
            _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.NotifySuccess("Customer Blocked Successfully");
                this.GetCustomerDetails();
              } else {
                this._HelperService.NotifySuccess(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
            }
          );
        }
        else {
          this._HelperService.NotifyError('Please Enter Valid Pin');
        }



      }
    });


  }
  UnblockBlockAccount() {
    swal({
      title: this._HelperService.AppConfig.CommonResource.UnBlockCustomerTitle,
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
        ' <label> Do you really want to make active this customer?  </label>' +
        '<input  type="text" placeholder="Enter Comment"  id="swal-input1" class="swal2-input">' +
        '<input style="-webkit-text-security: disc;" type="text" placeholder="Enter Pin" id="swal-input2" maxlength="4" class="swal2-input">',
      focusConfirm: false,
      inputClass: 'swalText',
      preConfirm: () => {
        return [
          document.getElementById('swal-input1')['value'],
          document.getElementById('swal-input2')['value']
        ]
      },
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocomplete: 'off',
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updateaccountstatus",
          AccountId: this._CustomerDetails.ReferenceId,
          AccountKey: this._CustomerDetails.ReferenceKey,
          StatusCode: "default.active",
          AuthPin: result.value[1],
          Comment: result.value[0],
          AccountTypeCode: this._HelperService.AppConfig.AccountType.Customer

        };

        let _OResponse: Observable<OResponse>;
        if (PostData.AuthPin != null && PostData.AuthPin != undefined && PostData.AuthPin != "") {
          _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, PostData);
          _OResponse.subscribe(
            _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.NotifySuccess("Customer Active Successfully");
                this.GetCustomerDetails();
              } else {
                this._HelperService.NotifySuccess(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
            }
          );
        }
        else {
          this._HelperService.NotifyError('Please Enter Valid Pin');
        }


      }
    });


  }

  SusspendAccount() {
    swal({
      title: this._HelperService.AppConfig.CommonResource.SuspendCustomerTitle,
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
        ' <label> Do you really want to suspend this customer?  </label>' +
        '<input   type="text" placeholder="Enter Comment" autocomplete="off" id="swal-input1" class="swal2-input">' +
        '<input style="-webkit-text-security: disc;" type="text" placeholder="Enter Pin"  maxlength="4" autocomplete="off" id="swal-input2" class="swal2-input">',


      focusConfirm: false,
      preConfirm: () => {
        return [
          document.getElementById('swal-input1')['value'],
          document.getElementById('swal-input2')['value']
        ]
      },
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocomplete: 'off',
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4",

      },
    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updateaccountstatus",
          AccountId: this._CustomerDetails.ReferenceId,
          AccountKey: this._CustomerDetails.ReferenceKey,
          StatusCode: "default.suspended",
          AuthPin: result.value[1],
          Comment: result.value[0],
          AccountTypeCode: this._HelperService.AppConfig.AccountType.Customer

        };
        let _OResponse: Observable<OResponse>;
        if (PostData.AuthPin != null && PostData.AuthPin != undefined && PostData.AuthPin != "") {
          _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, PostData);
          _OResponse.subscribe(
            _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.NotifySuccess("Customer Suspended Successfully");
                this.GetCustomerDetails();
              } else {
                this._HelperService.NotifySuccess(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
            }
          );
        } else {
          this._HelperService.NotifyError('Please Enter Valid Pin');
        }

      }
    });


  }




  Form_EditUser: FormGroup;
  Form_EditUser_Show() {
    this._HelperService.OpenModal("Account_Edit_Content");
  }
  Form_EditUser_Close() {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService.CloseModal("Account_Edit_Content");
  }
  Form_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_EditUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.Core.UpdateCustomer,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      GenderCode: this._HelperService.AppConfig.Gender.Male,
      Address: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      StatusCode: this._HelperService.AppConfig.Status.Inactive,
    });
  }
  Form_EditUser_Clear() {
    this.Form_EditUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_EditUser_Process(_FormValue: any) {
    _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveReferenceKey;
    _FormValue.ReferenceId = this._HelperService.AppConfig.ActiveReferenceId;

    // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;


    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account Updated Successfully");
          // this.Form_EditUser_Clear();
          this._HelperService.CloseModal('Account_Edit_Content');
          this.GetCustomerDetails();

          if (_FormValue.OperationType == "close") {
            this.Form_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }




  public _CustomerDetails: any =
    {
      "LastTransactionDate": null,
      "ReferenceId": null,
      "ReferenceKey": null,
      "AccountTypeCode": null,
      "AccountTypeName": null,
      "AccountOperationTypeCode": null,
      "AccountOperationTypeName": null,
      "OwnerKey": null,
      "OwnerDisplayName": null,
      "OwnerIconUrl": null,
      "DisplayName": null,
      "AccessPin": null,
      "AccountCode": null,
      "IconUrl": null,
      "PosterUrl": null,
      "ReferralCode": null,
      "RegistrationSourceCode": null,
      "RegistrationSourceName": null,
      "AppKey": null,
      "AppName": null,
      "AppVersionKey": null,
      "AppVersionName": null,
      "RequestKey": null,
      "CreateDate": null,
      "CreatedByKey": null,
      "CreatedByDisplayName": null,
      "CreatedByIconUrl": null,
      "ModifyByIconUrl": null,
      "StatusId": null,
      "StatusCode": null,
      "StatusName": null,
      "UserName": null,
      "Password": null,
      "SecondaryPassword": null,
      "SystemPassword": null,
      "MobileNumber": null,
      "ContactNumber": null,
      "Latitude": null,
      "Longitude": null,
      "EmailVerificationStatus": null,
      "EmailVerificationStatusDate": null,
      "NumberVerificationStatus": null,
      "NumberVerificationStatusDate": null,
      "DateOfBirth": null,
      "DateOfBirthS": null

    }

  _ShowGauge: boolean = true;
  //#region Balance Data 
  public _TransactionReference = null;
  public _AccountBalanceCreditAmount = 0;
  generateRandomNumber() {
    var TrRef = Math.floor(100000 + Math.random() * (999999 + 1 - 100000));
    return TrRef;
  }
  OpenPaymentOptions() {
    this._AccountBalanceCreditAmount = 0;
    var Ref = this.generateRandomNumber();
    this._TransactionReference = this._HelperService.AppConfig.ActiveReferenceId + '_' + this._HelperService.UserAccount.ReferralCode + '_' + Ref;
    this._HelperService.OpenModal('Form_TopUp_Content');
  }
  paymentDone(ref: any) {
    this.TransactionId = ref.trans
    if (ref != undefined && ref != null && ref != '') {
      if (ref.status == 'success') {
        this.CreditAmount();
      }
      else {
        this._HelperService.NotifyError('Payment failed');
      }
    }
    else {
      this._HelperService.NotifyError('Payment failed');
    }
    // this.title = 'Payment successfull';
  }

  public TransactionId
  public CreditAmount() {
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "topupaccount",
      AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,
      Amount: this._AccountBalanceCreditAmount,
      PaymentReference: this._TransactionReference,
      TransactionId: this.TransactionId
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscription, PostData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountBalanceCreditAmount = 0;
          this._TransactionReference = null;
          this._HelperService.NotifySuccess("Account credited");

        } else {
          this._HelperService.NotifySuccess(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  paymentCancel() {
  }




  public _AccountBalance: any = {
    Credit: 0.0,
    Debit: 0.0,
    Balance: 0.0,
    LastTransactionAmount: 0.0,
    LastTopupBalance: 0.0,
    LastTransactionDate: null,
    Ratio: 0,
    IsLoaded: false,
  };

  //#endregion

  //#region CustomerDetails 

  public _UserAccount: OUserDetails = {
    ContactNumber: null,
    SecondaryEmailAddress: null,
    ReferenceId: null,
    BankDisplayName: null,
    BankKey: null,
    SubOwnerAddress: null,
    SubOwnerLatitude: null,
    SubOwnerDisplayName: null,
    SubOwnerKey: null,
    SubOwnerLongitude: null,
    AccessPin: null,
    LastLoginDateS: null,
    AppKey: null,
    AppName: null,
    AppVersionKey: null,
    CreateDate: null,
    CreateDateS: null,
    CreatedByDisplayName: null,
    CreatedByIconUrl: null,
    CreatedByKey: null,
    Description: null,
    IconUrl: null,
    ModifyByDisplayName: null,
    ModifyByIconUrl: null,
    ModifyByKey: null,
    ModifyDate: null,
    ModifyDateS: null,
    PosterUrl: null,
    ReferenceKey: null,
    StatusCode: null,
    StatusI: null,
    StatusId: null,
    StatusName: null,
    AccountCode: null,
    AccountOperationTypeCode: null,
    AccountOperationTypeName: null,
    AccountTypeCode: null,
    AccountTypeName: null,
    Address: null,
    AppVersionName: null,
    ApplicationStatusCode: null,
    ApplicationStatusName: null,
    AverageValue: null,
    CityAreaKey: null,
    CityAreaName: null,
    CityKey: null,
    CityName: null,
    CountValue: null,
    CountryKey: null,
    CountryName: null,
    DateOfBirth: null,
    DisplayName: null,
    EmailAddress: null,
    EmailVerificationStatus: null,
    EmailVerificationStatusDate: null,
    FirstName: null,
    GenderCode: null,
    GenderName: null,
    LastLoginDate: null,
    LastName: null,
    Latitude: null,
    Longitude: null,
    MobileNumber: null,
    Name: null,
    NumberVerificationStatus: null,
    NumberVerificationStatusDate: null,
    OwnerDisplayName: null,
    OwnerKey: null,
    Password: null,
    Reference: null,
    ReferralCode: null,
    ReferralUrl: null,
    RegionAreaKey: null,
    RegionAreaName: null,
    RegionKey: null,
    RegionName: null,
    RegistrationSourceCode: null,
    RegistrationSourceName: null,
    RequestKey: null,
    RoleKey: null,
    RoleName: null,
    SecondaryPassword: null,
    SystemPassword: null,
    UserName: null,
    WebsiteUrl: null
  };

  GetCustomerDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccount,
      Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveReferenceKey, '='),
    }
    console.log("padat..........", pData);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._CustomerDetails = _Response.Result;
          console.log("this._CustomerDetails........", this._CustomerDetails);

          this._CustomerDetails.LastTransactionDate = this._HelperService.GetDateTimeS(this._CustomerDetails.LastTransactionDate);
          this._CustomerDetails['ContactPerson'] = {};
          //#region RelocateMarker 

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          // this.FormC_EditUser_Latitude = this._CustomerDetails.Latitude;
          // this.FormC_EditUser_Longitude = this._CustomerDetails.Longitude;
          // this.FormC_EditUser_Latitude = _Response.Result.Latitude;
          // this.FormC_EditUser_Longitude = _Response.Result.Longitude;

          //#endregion

          //#region DatesAndStatusInit 

          this._CustomerDetails.StartDateS = this._HelperService.GetDateS(
            this._CustomerDetails.StartDate
          );
          this._CustomerDetails.DateOfBirthS = this._HelperService.GetDateS(
            this._CustomerDetails.DateOfBirth
          );
          this._CustomerDetails.EndDateS = this._HelperService.GetDateS(
            this._CustomerDetails.EndDate
          );
          this._CustomerDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._CustomerDetails.CreateDate
          );
          this._CustomerDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._CustomerDetails.ModifyDate
          );
          this._CustomerDetails.StatusI = this._HelperService.GetStatusIcon(
            this._CustomerDetails.StatusCode
          );
          this._CustomerDetails.StatusB = this._HelperService.GetStatusBadge(
            this._CustomerDetails.StatusCode
          );
          this._CustomerDetails.StatusC = this._HelperService.GetStatusColor(
            this._CustomerDetails.StatusCode
          );


          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  EmptyFunctions(): void {

  }

  EditCustomer(): void {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Profile
    ]);
  }

  //#endregion

  // code
  tabSwitched(arg1: any): void { }
  ngAfterContentChecked(): void {
    this._ChangeDetectorRef.detectChanges();
  }

  merchantDeatils(ReferenceData) {
    console.log(ReferenceData);
    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.RewardPercentage,
    //   ReferenceData.OwnerKey,
    //   ReferenceData.OwnerId,
    // ]);
  }
}
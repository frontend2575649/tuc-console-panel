import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { TUCustomerDetailsComponent } from "./tucustomerdetails.component";
import { Angular4PaystackModule } from 'angular4-paystack'
import { DynamicRoutesguardGuard } from "src/app/service/guard/dynamicroutes.guard";

const routes: Routes = [
    {
        path: "",
        component: TUCustomerDetailsComponent,
    //    canActivateChild:[DynamicRoutesguardGuard],
    //    data:{accessName:['viewcustomer']},
        children: [
            { path: "", data: { permission: "customer", menuoperations: "ManageMerchant", accounttypecode: "customer" }, loadChildren: "../../../../../modules/accounts/tuconsole/topup/topup.module#TUTopUpModule" },
            { path: "moredetail/:referencekey/:referenceid", data: { permission: "customer", menuoperations: "ManageMerchant", PageName: 'System.Menu.Customer', accounttypecode: "customer" }, loadChildren: "../../../../../modules/accounts/tuconsole/topup/topup.module#TUTopUpModule" },
            // { path: 'sales/salehistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Customer' }, loadChildren: '../../../../modules/transactions/tusale/customer/merchant/tusale.module#TUSaleModule' },
            // { path: 'sales/rewardhistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Customer' }, loadChildren: '../../../../modules/transactions/tusale/customer/rewardhistory/tusale.module#TUSaleModule' },
            // { path: 'sales/redeemhistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Customer' }, loadChildren: '../../../../modules/transactions/tusale/customer/redeemhistory/tusale.module#TUSaleModule' },
            // { path: 'sales/rewardclaimhistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Customer' }, loadChildren: '../../../../modules/transactions/tusale/customer/turewardclaim/turewardclaim.module#TURewardsClaimModule' },

            // { path: 'terminals/:referencekey/:referenceid', data: { 'permission': 'terminals', PageName: 'System.Menu.Store' }, loadChildren: '../../../../modules/accounts/tuterminals/store/tuterminals.module#TUTerminalsModule' },

            { path: 'moredetail/walletinfo/:referencekey/:referenceid', data: { 'permission': 'customer', PageName: 'System.Menu.Customer' }, loadChildren: '../../../dashboards/walletinfo/walletinfo.module#WalletInfoModule' },
            { path: 'moredetail/overview/:referencekey/:referenceid', data: { 'permission': 'customer', PageName: 'System.Menu.Customer' }, loadChildren: '../../../../modules/dashboards/Customer-Overview/overview.module#CustomerOverviewModule' },
            { path: 'moredetail/cstomer-rewards/:referencekey/:referenceid', data: { 'permission': 'customer', PageName: 'System.Menu.Customer' }, loadChildren: '../../../../modules/dashboards/Customer-rewards/rewards.module#CustomerrewardsModule' },
            { path: "referalhistory/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.ReferalHistory" }, loadChildren: "../../../../modules/transactions/tusale/referalhistory/tusale.module#TUSaleModule" },


            { path: 'moredetail/rewardnredeem/:referencekey/:referenceid', data: { 'permission': 'customer', PageName: 'System.Menu.Customer' }, loadChildren: '../../../dashboards/rewardnredeem/rewardnredeem.module#RewardNRedeemModule' },
            // { path: 'moredetail/subaccounts/:referencekey/:referenceid', data: { 'permission': 'cashiers', PageName: 'System.Menu.Settings' }, loadChildren: '../../../../accounts/tuconsole/subaccounts/tusubaccounts.module#TUSubAccountsModule' },
            // { path: 'moredetail/topup/:referencekey/:referenceid', data: { 'permission': 'topup', PageName: 'System.Menu.Settings' }, loadChildren: '../../../../accounts/tuconsole/topup/topup.module#TUTopUpModule' }

            { path: 'bnplconfiguration/:referencekey/:referenceid', data: { permission: "customer", PageName: "BNPL Configuration" }, loadChildren: '../cubnplconfiguration/cubnplconfiguration.module#CubnplconfigurationModule'},
            { path: "moredetail/cashout/:referencekey/:referenceid", data: { permission: "customer", PageName: "System.Menu.Cashouts" }, loadChildren: "../customercashout/customercashout.module#CustomerCashoutModule" },
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUCustomerRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUCustomerRoutingModule,
        GooglePlaceModule,
        Angular4PaystackModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    declarations: [TUCustomerDetailsComponent]
})
export class TUCustomerDetailsModule { }

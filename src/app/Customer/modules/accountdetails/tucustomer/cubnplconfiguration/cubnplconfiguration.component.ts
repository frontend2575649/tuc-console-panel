import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { Observable } from 'rxjs';

import { HelperService } from '../../../../../service/helper.service';
import { OResponse } from 'src/app/service/service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-cubnplconfiguration',
  templateUrl: './cubnplconfiguration.component.html'
})
export class CubnplconfigurationComponent implements OnInit {

  _Configurations: any = {};

  constructor(private _HelperService: HelperService,
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,) { }

  BNPLEnable_Disable() {
    swal({
      title: 'Do you want to Enable/Disable this Merchant for BNPL?',
      text: 'After settings, it takes upto 1 hours to reflect changes, Do you want to continue?',
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Blue,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      input: 'password',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;


        //Save account configuration for merchant
        var PostData = {
          Task: "saveaccountconfiguration",
          AuthPin: result.value,
          TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
          UserAccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
          ConfigurationKey: "bynowpaylater",
          Value: this._Configurations.BNPL_Config,
          StatusCode: this._HelperService.AppConfig.Status.Active,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              // this._HelperService.NotifySuccess("BNPL Configuration Updated Successfully");
              this._HelperService.NotifySuccess(_Response.Message);
            } else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });
  }

  GetBnplConfigurations() {
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetUserParameters,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      SortExpression: 'Name asc',
      Offset: 0,
      Limit: 1000,
    }
    PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
      PData.SearchCondition,
      "TypeCode",
      this._HelperService.AppConfig.DataType.Text,
      this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
      "="
    );
    PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
      PData.SearchCondition,
      "UserAccountKey",
      this._HelperService.AppConfig.DataType.Text,
      this._HelperService.AppConfig.ActiveReferenceKey,
      "="
    );
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            var ResposeData = _Response.Result.Data;
            const bnpldata = ResposeData.find(el => el.CommonSystemName == "bynowpaylater")
            if (bnpldata) {
              this._Configurations.BNPL_Config = bnpldata.Value;
            }
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  ngOnInit() {
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
      if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Customer]);
      } else {
        this._HelperService.Get_UserAccountDetails(true);
        this.GetBnplConfigurations();
      }
    });
  }

}

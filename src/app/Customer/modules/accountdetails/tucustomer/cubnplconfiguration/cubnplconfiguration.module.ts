import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CubnplconfigurationComponent } from './cubnplconfiguration.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';


const routes: Routes = [
  {
    path: "",
    component: CubnplconfigurationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CubnplconfigurationRoutingModule { }

@NgModule({
  declarations: [CubnplconfigurationComponent],
  imports: [
    CommonModule,
    FormsModule,
    CubnplconfigurationRoutingModule
  ]
})
export class CubnplconfigurationModule { }

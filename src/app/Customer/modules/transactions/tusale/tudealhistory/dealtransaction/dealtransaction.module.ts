import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { AgmCoreModule } from '@agm/core';
import { DealTransactionComponent } from './dealtransaction.component';
import { DynamicRoutesguardGuard } from "src/app/service/guard/dynamicroutes.guard";
// import { LeafletModule } from '@asymmetrik/ngx-leaflet';
const routes: Routes = [
    {
        path: "",
        component: DealTransactionComponent,
        canActivate:[DynamicRoutesguardGuard],
        data:{accessName:['customerdtlsdealhistory']},
        children: [
            { path: "/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant",  PageName: "System.Menu.DealHistory", accounttypecode: "merchant" }, loadChildren: "../../../../../../../app/Customer/modules/transactions/tusale/tudealhistory/tupayments/tupayments.module#TUPaymentsModule" },

            { path: "purchasehistory/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant",  PageName: "System.Menu.DealHistory", accounttypecode: "merchant" }, loadChildren: "../../../../../../../app/Customer/modules/transactions/tusale/tudealhistory/tupayments/tupayments.module#TUPaymentsModule" },

            { path: "redeemhistory/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", PageName: "System.Menu.DealHistory",accounttypecode: "merchant" }, loadChildren: "../../../../../../../app/Customer/modules/transactions/tusale/tudealhistory/redeemhistory/tusale.module#TUSaleModule" },

            
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class  DealTransactionRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        DealTransactionRoutingModule,
        // LeafletModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    declarations: [DealTransactionComponent]
})
export class DealTransactionModule { }

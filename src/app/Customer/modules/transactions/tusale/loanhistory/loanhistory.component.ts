import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../../service/service';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
declare var $: any;


@Component({
  selector: 'app-loanhistory',
  templateUrl: './loanhistory.component.html',
  styleUrls: ['./loanhistory.component.css']
})
export class LoanhistoryComponent implements OnInit {

  public listData: any = {};
  public overviewData: any = {};
  providerList = this._HelperService.AppConfig.PROVIDERS;
  loanAmountMin: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
  loanAmountMax: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
  public TUTr_Config: OList;
  public ResetFilterControls: boolean = true;
  public AcquirerId = 0;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService: FilterHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
  ) {
  }


  public TuTr_Columns =
    {
      Status: true,
      Date: true,
      Account: true,
      AccountName: true,
      Mob: true,
      Biller: true,
      Amount: true,
      Source: true,
      PaymentRef: true
    }
  public TuTr_Filters_List =
    {
      Date: false,
      Sort: false,
      Status: false,
      Biller: false
    }
  TUTr_Setup() {
    this.TUTr_Config =
    {
      Id: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetAccountLoans,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.accountloans,
      Status: 0,
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'ReferenceId',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },
      Title: "Closed loans",
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,
      AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
      StatusType: null,
      SearchBaseCondition: this.TuTr_Setup_SearchCondition(),
      TableFields: this.TuTr_Setup_Fields(),
      DefaultSortExpression: "ReferenceId desc",
    }

    this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.LoanHistory,
      this.TUTr_Config
    );
    this.TUTr_GetData();
  }

  TuTr_Setup_SearchCondition() {
    this.TuTr_Columns =
    {
      Status: true,
      Date: true,
      Account: true,
      AccountName: true,
      Mob: true,
      Biller: true,
      Amount: true,
      Source: true,
      PaymentRef: true
    }
    var SearchCondition = '';
    this.TuTr_Filters_List =
    {
      Date: true,
      Sort: true,
      Status: true,
      Biller: true
    }

    return SearchCondition;
  }
  TuTr_Setup_Fields() {
    var TableFields = [];

    TableFields = [
      {
        DisplayName: 'Reference ID',
        SystemName: 'ReferenceId',
        DataType: this._HelperService.AppConfig.DataType.Number,
        Class: '',
        Show: true,
        Search: false,
        Sort: true,
      },
      {
        DisplayName: 'Customer Details',
        SystemName: 'AccountName',
        DataType: this._HelperService.AppConfig.DataType.Text,
        Class: '',
        Show: true,
        Search: true,
        Sort: true,
      },
      {
        DisplayName: 'Customer mobile number',
        SystemName: 'AccountMobileNumber',
        DataType: this._HelperService.AppConfig.DataType.Text,
        Class: '',
        Show: true,
        Search: true,
        Sort: false,
      },
      {
        DisplayName: 'Merchant',
        SystemName: 'MerchantDisplayName',
        DataType: this._HelperService.AppConfig.DataType.Text,
        Class: '',
        Show: true,
        Search: true,
        Sort: true,
      },
      {
        DisplayName: 'Provider',
        SystemName: 'ProviderName',
        DataType: this._HelperService.AppConfig.DataType.Text,
        Class: '',
        Show: true,
        Search: true,
        Sort: true,
        ResourceId: null,
      },
      {
        DisplayName: 'Loan',
        SystemName: 'Amount',
        DataType: this._HelperService.AppConfig.DataType.Decimal,
        Class: '',
        Show: true,
        Search: true,
        Sort: true,
        ResourceId: null,
      },
      {
        DisplayName: 'int',
        SystemName: 'TotalInterest',
        DataType: this._HelperService.AppConfig.DataType.Decimal,
        Class: '',
        Show: true,
        Search: false,
        Sort: true,
        ResourceId: null,
      },
      {
        DisplayName: 'Repay',
        SystemName: 'TotalPaidAmount',
        DataType: this._HelperService.AppConfig.DataType.Decimal,
        Class: '',
        Show: true,
        Search: false,
        Sort: true,
        ResourceId: null,
      },
      {
        DisplayName: 'Start date',
        SystemName: 'StartDate',
        DataType: this._HelperService.AppConfig.DataType.Date,
        Class: '',
        Show: true,
        Search: false,
        Sort: true,
        ResourceId: null,
        IsDateSearchField: true,
      },
      {
        DisplayName: 'End date',
        SystemName: 'EndDate',
        DataType: this._HelperService.AppConfig.DataType.Date,
        Class: '',
        Show: true,
        Search: false,
        Sort: true,
        ResourceId: null,
        IsDateSearchField: true,
      }
    ]

    return TableFields;
  }


  TUTr_GetData() {
    this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.ThankUCash.GetAccountLoans);
    var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config, null, true);
    this.TUTr_Config = TConfig;
  }
  TUTr_RowSelected(ReferenceData) {
    const ReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Loans.LoanDetail,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);
  }

  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions, true);
    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: SortExpression,
      Type: ListOptions.Type,
      ReferenceKey: ListOptions.ReferenceKey,
      StartDate: ListOptions.StartDate,
      EndDate: ListOptions.EndDate,
      ReferenceId: ListOptions.ReferenceId,
      SubReferenceId: ListOptions.SubReferenceId,
      SubReferenceKey: ListOptions.SubReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,
      AccountKey: ListOptions.AccountKey,
      ListType: ListOptions.ListType,
      IsDownload: false,
      Status: ListOptions.Status
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.accountloans, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.listData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });


  }

  SetOtherFilters(): void {
    this.TUTr_Config.SearchBaseConditions = [];

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant));
    if (CurrentIndex != -1) {
      this.TUTr_Filter_Merchant_Selected = 0;
      this.MerchantEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Provider));
    if (CurrentIndex != -1) {
      this.TUTr_Filter_Provider_Selected = 0;
      this.ProvidersEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region dropdowns 


  public TUTr_Filter_Provider_Option: Select2Options;
  public TUTr_Filter_Provider_Selected = 0;

  TUTr_Filter_Providers_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.getloanproviders,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.accountloans,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "ProviderName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Provider_Option = {
      placeholder: 'Search By Provider',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }

  TUTr_Filter_Providers_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TUTr_Config,
      this._HelperService.AppConfig.OtherFilters.MerchantSales.Provider
    );

    this.ProvidersEventProcessing(event);
  }

  ProvidersEventProcessing(event: any): void {
    if (event.value == this.TUTr_Filter_Provider_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
      this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
      this.TUTr_Filter_Provider_Selected = 0;
    }
    else if (event.value != this.TUTr_Filter_Provider_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
      this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
      this.TUTr_Filter_Provider_Selected = event.value;
      this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '='));
    }
    this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    this._HelperService.ToggleField = true;

    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 500);
  }

  //#region merchant 

  public TUTr_Filter_Merchant_Option: Select2Options;
  public TUTr_Filter_Merchant_Selected = 0;
  TUTr_Filter_Merchants_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      // ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      // ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: "AccountTypeCode",
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: "=",
        //     SearchValue: this._HelperService.AppConfig.AccountType.Merchant
        // }
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Merchant_Option = {
      placeholder: 'Search By Merchant',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Merchants_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TUTr_Config,
      this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant
    );
    this.MerchantEventProcessing(event);
  }

  MerchantEventProcessing(event: any): void {
    if (event.value == this.TUTr_Filter_Merchant_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
      this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
      this.TUTr_Filter_Merchant_Selected = 0;
    }
    else if (event.value != this.TUTr_Filter_Merchant_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
      this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
      this.TUTr_Filter_Merchant_Selected = event.value;
      this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '='));
    }
    this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    this._HelperService.ToggleField = true;
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 500);
  }

  SetSearchRanges(): void {
    this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('Amount', this.TUTr_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.loanAmountMin, this.loanAmountMax);
    if (this.loanAmountMin == this._HelperService.AppConfig.loanAmountMin && this.loanAmountMax == this._HelperService.AppConfig.loanAmountMax) {
      this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
    }
    else {
      this.TUTr_Config.SearchBaseConditions.push(SearchCase);
    }
  }


  TUTr_ToggleOption(event: any, Type: any) {

    if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
      event.data = {
        SalesMin: this.loanAmountMin,
        SalesMax: this.loanAmountMax,
      }
    }

    this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

    this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);


    if (event != null) {
      for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
        const element = this.TUTr_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }


    if (
      (this.TUTr_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TUTr_GetData();
    }
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_MerchantSales(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.LoanHistory
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.LoanHistory
        );
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
        this.TUTr_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this.SetSearchRanges();
    this._HelperService.MakeFilterSnapPermanent();
    this.TUTr_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();

    if (ButtonType == 'Sort') {
      $("#TUTr_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#TUTr_fdropdown").dropdown('toggle');
    }
  }

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.TUTr_Filter_Merchants_Load();
    this.TUTr_Filter_Providers_Load();
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }


  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
    this.SetSalesRanges();

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion
    this.TUTr_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }


  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
    this.SetSalesRanges();

    // //#region setOtherFilters
    // this.SetOtherFilters();
    // //#endregion

    this.TUTr_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
    this.SetSalesRanges();

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.TUTr_GetData();
  }


  SetSalesRanges(): void {
    this.loanAmountMin = this.TUTr_Config.SalesRange.SalesMin;
    this.loanAmountMax = this.TUTr_Config.SalesRange.SalesMax;
  }

  getAllLoanOverview() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetAccountLoansOverview,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.accountloans, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.overviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });

  }

  ngOnInit() {
    Feather.replace();
    this._HelperService.StopClickPropogation();

    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
      // console.log("this._HelperService.AppConfig.ActiveReferenceId.......",this._HelperService.AppConfig.ActiveReferenceId);
      if (this._HelperService.AppConfig.ActiveReferenceKey == null && this._HelperService.AppConfig.ActiveReferenceId == null) {
        if (this._HelperService.AppConfig.ActiveOwnerId != null) {
          this.AcquirerId = this._HelperService.AppConfig.ActiveOwnerId;
          this.getAllLoanOverview();
          this.TUTr_Setup();
          this.TUTr_Filter_Merchants_Load();
          this.TUTr_Filter_Providers_Load();
        }
        else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
        }
      } else {
        this.AcquirerId = this._HelperService.AppConfig.ActiveReferenceId;

        this._HelperService.Get_UserAccountDetails(true);
        this.getAllLoanOverview();
        this.TUTr_Setup();
        this.TUTr_Filter_Merchants_Load();
        this.TUTr_Filter_Providers_Load();
      }
      // this.TodayStartTime = new Date(2017, 0, 1, 0, 0, 0, 0), moment();
      // this.TodayEndTime = moment();
      // this.GetAccountOverviewLite();
    });

  }

}

import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { MainPipe } from '../../../../../service/main-pipe.module'

import { TUCustomersComponent } from "./tucustomers.component";
import { MerchantguardGuard } from "src/app/service/guard/merchantguard.guard";

const routes: Routes = [
  {
    path: "",
    component: TUCustomersComponent,
    children: [
      { path: "", data: { permission: "customer", menuoperations: "ManageCustomer", accounttypecode: "customer" }, loadChildren: "../customers/tuallcustomers/tuallcustomers.module#TUAllCustomersModule" },
      { path: 'allcustomers', data: { 'permission': 'customer', PageName: 'System.Menu.AllCustomers' }, loadChildren: '../customers/tuallcustomers/tuallcustomers.module#TUAllCustomersModule' },
      { path: 'merchantcustomers', data: { 'permission': 'customer', PageName: 'System.Menu.MerchantCustomers' }, loadChildren: '../customers/tumerchantcutomers/tumerchantcutomers.module#TUMerchantCustomersModule' },
      { path: 'tuccustomers', data: { 'permission': 'customer', PageName: 'System.Menu.TUCCustomers' }, loadChildren: '../customers/tutuccutomers/tutuccutomers.module#TUTucCustomersModule' },
      { path: 'tucappdownloadcustomers', data: { 'permission': 'customer', PageName: 'System.Menu.TUCAppDownloads' }, loadChildren: '../customers/tuappdownloadscustomers/tuappdownloadscustomers.module#TUAppDownloadsCustomersModule' }
   
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUCustomersRoutingModule { }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TUCustomersRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    MainPipe
  ],
  declarations: [TUCustomersComponent]
})
export class TUCustomersModule { }

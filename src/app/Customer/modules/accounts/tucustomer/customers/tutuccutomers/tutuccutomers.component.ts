import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs';
import swal from "sweetalert2";
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from "../../../../../../service/service";
declare var $: any;
declare var moment: any;

@Component({
  selector: "tu-tutuccustomers",
  templateUrl: "./tutuccutomers.component.html",
})
export class TuTucCustomersComponent implements OnInit {
  public ResetFilterControls: boolean = true;
  public ListType: number;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
    this._HelperService.CustomerAnalytics = true;
    this._HelperService.DownloadAnalytics = false;

  }

  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();

    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];

      this.Customers_Setup();
      this.Customers_Filter_Owners_Load();
      this.InitColConfig();

    });

    // this._HelperService.StopClickPropogation();
  }

  
  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }

  ToogleType(type: string): void {
    this.Customers_Config.Type = type;
    this.Customers_GetData();
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region merchantlist

  public Customers_Config: OList;
  Customers_Setup() {
    this.Customers_Config = {
      Id: null,
      Sort: null,
      //Task: this._HelperService.AppConfig.Api.ThankUCash.GetAppUsers,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetCustomers,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      // Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
      Title: "Available Customers",
      StatusType: "defaultcustomer",
      Type: this._HelperService.AppConfig.CustomerTypes.thankucash,
      //Type: this._HelperService.AppConfig.CustomerTypes.all,
      // ListType: 1,
      AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,
      DefaultSortExpression: "CreateDate desc",
      IsDownload: true,

      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: "Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Mobile No",
          SystemName: "MobileNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Transaction",
          SystemName: "TotalTransaction",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "EmailAddress ",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "TUC Plus Reward",
          SystemName: "TucPlusRewardInvoiceAmount",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Tuc Plus Balance",
          SystemName: "TucPlusBalance",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Rewards ",
          SystemName: "TucRewardAmount",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Redeemed",
          SystemName: "RedeemAmount",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "TUC Balance",
          SystemName: "TucBalance",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: 'Last Transaction Date',
          SystemName: "LastTransactionDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };


    this.Customers_Config.ListType = this.ListType;
    this.Customers_Config.SearchBaseCondition = "";

    if (this.Customers_Config.ListType == 1) // Active
    {
        this.Customers_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.Customers_Config.SearchBaseCondition, "StatusCode", 'number', 'default.active', "=");
    }
    else if (this.Customers_Config.ListType == 2) // Suspend
    {
        this.Customers_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.Customers_Config.SearchBaseCondition, "StatusCode", 'number', 'default.suspended', "=");
    }
    else if (this.Customers_Config.ListType == 3) // Blocked
    {
        this.Customers_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.Customers_Config.SearchBaseCondition, "StatusCode", 'number', 'default.blocked', "=");
    }
    
    else if (this.Customers_Config.ListType == 4) {  // InActive
        this.Customers_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.Customers_Config.SearchBaseCondition, "StatusCode", 'number', 'default.inactive', "=");
    }
    else if (this.Customers_Config.ListType == 5) {  // All
        this.Customers_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrictFromArray(this.Customers_Config.SearchBaseCondition, "StatusCode", 'number', [], "=");
    }
    else {
        this.Customers_Config.DefaultSortExpression = 'CreateDate desc';
    }



    this.Customers_Config = this._DataHelperService.List_Initialize(
      this.Customers_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Customer,
      this.Customers_Config
    );

    this.Customers_GetData();
  }
  Customers_ToggleOption(event: any, Type: any) {
    if (Type == "date") {
      this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
      this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
  }
    if (event != null) {
      for (let index = 0; index < this.Customers_Config.Sort.SortOptions.length; index++) {
        const element = this.Customers_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    //newly added code filter start
    if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
      event.data = {
        SalesMin: this.TUTr_InvoiceRangeMinAmount,
        SalesMax: this.TUTr_InvoiceRangeMaxAmount,
        RewardMin: this.TUTr_RewardRangeMinAmount,
        RewardMax: this.TUTr_RewardRangeMaxAmount,
        RedeemMin: this.TUTr_RedeemRangeMinAmount,
        RedeemMax: this.TUTr_RedeemRangeMaxAmount
      }
    }
    //newly added code filter end


    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.Customers_Config


    );

    this.Customers_Config = this._DataHelperService.List_Operations(
      this.Customers_Config,
      event,
      Type
    );

    if (
      (this.Customers_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.Customers_GetData();
    }

  }
  timeout = null;
  Customers_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.Customers_Config.Sort.SortOptions.length; index++) {
          const element = this.Customers_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.Customers_Config


      );

      this.Customers_Config = this._DataHelperService.List_Operations(
        this.Customers_Config,
        event,
        Type
      );

      if (
        (this.Customers_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.Customers_GetData();
      }

    }, this._HelperService.AppConfig.SearchInputDelay);

  }

  Customers_GetData() {
    this.GetOverviews(this.Customers_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getcustomersstatusoverview);
    var TConfig = this._DataHelperService.List_GetData(
      this.Customers_Config
    );
    this.Customers_Config = TConfig;
    this._DataHelperService.statusOption(this.Customers_Config.StatusOptions);

  }

  MerchantsList_ListTypeChange(Type) {
    this.ListType = Type;
    this.Customers_Setup();
}
  Customers_RowSelected(ReferenceData) {
    //#region Save Current Merchant To Storage 
    this._HelperService.customerDisplayName=ReferenceData.DisplayName;
    this._HelperService.customerReferenceKey=ReferenceData.ReferenceKey,
    this._HelperService.customerReferenceId=ReferenceData.ReferenceId,
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveCustomer,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Customer,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.CustomerPanel.Accounts.CustomerOverview,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);

    //#endregion





  }

  //#endregion
  TUTr_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
  TUTr_InvoiceRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
  TUTr_InvoiceRange_OnChange(changeContext: ChangeContext): void {
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalInvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    this.TUTr_InvoiceRangeMinAmount = changeContext.value;
    this.TUTr_InvoiceRangeMaxAmount = changeContext.highValue;
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalInvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
    if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }
    this.Customers_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  TUTr_RedeemRangeMinAmount: number = this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit;
  TUTr_RedeemRangeMaxAmount: number = this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit;
  TUTr_RedeemRange_OnChange(changeContext: ChangeContext): void {
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'RedeemAmount', this.TUTr_RedeemRangeMinAmount, this.TUTr_RedeemRangeMaxAmount);
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    this.TUTr_RedeemRangeMinAmount = changeContext.value;
    this.TUTr_RedeemRangeMaxAmount = changeContext.highValue;
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'RedeemAmount', this.TUTr_RedeemRangeMinAmount, this.TUTr_RedeemRangeMaxAmount);
    if (this.TUTr_RedeemRangeMinAmount == this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit && this.TUTr_RedeemRangeMaxAmount == this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }
    this.Customers_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }


  TUTr_RewardRangeMinAmount: number = this._HelperService.AppConfig.RangeRewardAmountMinimumLimit;
  TUTr_RewardRangeMaxAmount: number = this._HelperService.AppConfig.RangeRewardAmountMaximumLimit;
  TUTr_RewardRange_OnChange(changeContext: ChangeContext
  ): void {
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TucRewardAmount', this.TUTr_RewardRangeMinAmount, this.TUTr_RewardRangeMaxAmount);
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    this.TUTr_RewardRangeMinAmount = changeContext.value;
    this.TUTr_RewardRangeMaxAmount = changeContext.highValue;
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TucRewardAmount', this.TUTr_RewardRangeMinAmount, this.TUTr_RewardRangeMaxAmount);
    if (this.TUTr_RewardRangeMinAmount == this._HelperService.AppConfig.RangeRewardAmountMinimumLimit && this.TUTr_RewardRangeMaxAmount == this._HelperService.AppConfig.RangeRewardAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }
    this.Customers_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }


  //#region OwnerFilter

  public Customers_Filter_Owners_Option: Select2Options;
  public Customers_Filter_Owners_Selected = null;
  Customers_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetAccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,

      SearchCondition: "",
      SortCondition: ["DisplayName asc"],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
      [
        this._HelperService.AppConfig.AccountType.Merchant,
        this._HelperService.AppConfig.AccountType.Acquirer,
        this._HelperService.AppConfig.AccountType.PGAccount,
        this._HelperService.AppConfig.AccountType.PosAccount
      ]
      , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.Customers_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  Customers_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.Customers_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.Customers_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "OwnerId",
        this._HelperService.AppConfig.DataType.Text,
        this.Customers_Filter_Owners_Selected,
        "="
      );
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.Customers_Config.SearchBaseConditions
      );
      this.Customers_Filter_Owners_Selected = null;
    } else if (event.value != this.Customers_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "OwnerId",
        this._HelperService.AppConfig.DataType.Text,
        this.Customers_Filter_Owners_Selected,
        "="
      );
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.Customers_Config.SearchBaseConditions
      );
      this.Customers_Filter_Owners_Selected = event.data[0].ReferenceId;
      this.Customers_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "OwnerId",
          this._HelperService.AppConfig.DataType.Text,
          this.Customers_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.Customers_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.Customers_Config.SearchBaseConditions = [];
    this.Customers_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.Customers_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }




  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.Customers_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion
    this.SetSalesRanges();

    this.Customers_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.Customers_Config);
    if (Type == 'Time') {
      this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
      this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
  }
    this.SetOtherFilters();
    this.SetSalesRanges();

    this.Customers_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      // inputAttributes: {
      //   autocapitalize: "off",
      //   autocorrect: "off",
      //   maxLength: "4",
      //   minLength: "4",
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Customer,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Customer
        );
        this._FilterHelperService.SetMerchantConfig(this.Customers_Config);
        this.Customers_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    if (this.TUTr_InvoiceRangeMinAmount < 0 || this.TUTr_InvoiceRangeMaxAmount < 0 || this.TUTr_RewardRangeMinAmount < 0 || this.TUTr_RewardRangeMaxAmount < 0 || this.TUTr_RedeemRangeMinAmount < 0 || this.TUTr_RedeemRangeMaxAmount < 0) {
      this._HelperService.NotifyError("Negative Numbers not allowed ");
      return;
    }else if (this.TUTr_InvoiceRangeMinAmount && this.TUTr_InvoiceRangeMaxAmount && this.TUTr_InvoiceRangeMinAmount > this.TUTr_InvoiceRangeMaxAmount) {
      this._HelperService.NotifyError("Minimum TUC Balance should be less than Maximum TUC Balance");
      return;
    } else if (this.TUTr_RewardRangeMinAmount && this.TUTr_RewardRangeMaxAmount && this.TUTr_RewardRangeMinAmount > this.TUTr_RewardRangeMaxAmount) {
      this._HelperService.NotifyError("Minimum Reward Amount should be less than Maximum Reward Amount");
      return;
    } else if (this.TUTr_RedeemRangeMinAmount && this.TUTr_RedeemRangeMaxAmount && this.TUTr_RedeemRangeMinAmount > this.TUTr_RedeemRangeMaxAmount) {
      this._HelperService.NotifyError("Minimum Redeem Amount should be less than Maximum Redeem Amount");
      return;
    }
    else if (this.TUTr_InvoiceRangeMinAmount == null || this.TUTr_InvoiceRangeMinAmount == undefined
      || this.TUTr_RewardRangeMaxAmount == null || this.TUTr_RewardRangeMaxAmount == undefined) {
        this._HelperService.NotifyError("Value not be null or undefined");
      return;
    }
    this.SetSearchRanges();
    this._HelperService.MakeFilterSnapPermanent();
    this.Customers_GetData();

    if (ButtonType == 'Sort') {
      $("#Customers_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#Customers_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }


  SetSalesRanges(): void {
    this.TUTr_InvoiceRangeMinAmount = this.Customers_Config.SalesRange.SalesMin;
    this.TUTr_InvoiceRangeMaxAmount = this.Customers_Config.SalesRange.SalesMax;
    this.TUTr_RewardRangeMinAmount = this.Customers_Config.SalesRange.RewardMin;
    this.TUTr_RewardRangeMaxAmount = this.Customers_Config.SalesRange.RewardMax;
    this.TUTr_RedeemRangeMinAmount = this.Customers_Config.SalesRange.RedeemMin;
    this.TUTr_RedeemRangeMaxAmount = this.Customers_Config.SalesRange.RedeemMax;
  }


  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.Customers_Config);
    this.SetOtherFilters();
    this.SetSalesRanges();
    this.Customers_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.Customers_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }


  //#region SalesOverview 
  // public OverviewData = { Total: 0, New: 0, Suspended: 0, Blocked: 0 , Active: 0};
  public OverviewData = { Total: 0, New: 0, Idle: 0, Dead: 0, Active: 0 };
  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);
    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: SortExpression,
      Type: ListOptions.Type,
      ReferenceKey: ListOptions.ReferenceKey,
      StartDate: ListOptions.StartDate,
      EndDate: ListOptions.EndDate,
      ReferenceId: ListOptions.ReferenceId,
      SubReferenceId: ListOptions.SubReferenceId,
      SubReferenceKey: ListOptions.SubReferenceKey,
      AccountId: ListOptions.AccountId,
      AccountKey: ListOptions.AccountKey,
      ListType: ListOptions.ListType,
      IsDownload: false,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Analytics, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.OverviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });


  }
  //#endregion

  SetSearchRanges(): void {

    //#region Invoice 
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('MainBalance', this.Customers_Config.SearchBaseConditions);

    var SearchCase = this._HelperService.GetSearchConditionRange('', 'MainBalance', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
    if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }

    //#endregion

    //#region reward 
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TucRewardAmount', this.Customers_Config.SearchBaseConditions);

    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TucRewardAmount', this.TUTr_RewardRangeMinAmount, this.TUTr_RewardRangeMaxAmount);
    if (this.TUTr_RewardRangeMinAmount == this._HelperService.AppConfig.RangeRewardAmountMinimumLimit && this.TUTr_RewardRangeMaxAmount == this._HelperService.AppConfig.RangeRewardAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }

    //#endregion

    //#region Redeem 
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('RedeemAmount', this.Customers_Config.SearchBaseConditions);

    var SearchCase = this._HelperService.GetSearchConditionRange('', 'RedeemAmount', this.TUTr_RedeemRangeMinAmount, this.TUTr_RedeemRangeMaxAmount);
    if (this.TUTr_RedeemRangeMinAmount == this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit && this.TUTr_RedeemRangeMaxAmount == this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }

    //#endregion


  }

}

import { ChangeDetectorRef, Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ChartDataSets } from 'chart.js';
import * as Feather from 'feather-icons';
import * as cloneDeep from 'lodash/cloneDeep';
import { BaseChartDirective, Color } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OResponse, OSalesTrend, OSalesTrendData, OSalesTrendDataHourly, OSelect, OList } from '../../../../service/service';
declare var moment: any;
declare var $: any;


@Component({
  selector: "walletinfo",
  templateUrl: "./walletinfo.component.html",
  styleUrls: ['./walletinfo.component.css'],
  styles: [
    `
      agm-map {
        height: 300px;
      },
     
    `
  ]
})
export class WalletInfoComponent implements OnInit {

  lastdaytext = "LAST DAY";
  lastweektext = "LAST WEEK";
  lastweekCustom = "LAST WEEK";
  lastmonthtext = "LAST MONTH";

  Types: any = {
    year: 'year',
    month: 'month',
    week: 'week',
    day: 'day',
    hour: 'hour'
  }

  public showGraph: boolean = true;
  public lineChartData: ChartDataSets[] = [
    { data: [87, 110, 22, 80, 77, 100, 150], type: 'bar', label: 'Series B', hidden: false },
    { data: [54, 110, 98, 44, 23, 64, 120], type: 'line', label: 'Series A', hidden: false },
  ];
  public lineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    },
    {
      backgroundColor: 'rgba(1, 104, 250, 00)',
    },
    {
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ]
  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {

  }

  //#endregion

  ngOnInit() {
    this._HelperService.FullContainer = false;
    Feather.replace();


    //start time and end time for overview


    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.CustomerData.next(params);
      this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];

      this.pDataSales.AccountId = this._HelperService.AppConfig.ActiveReferenceId;
      this.pDataSales.AccountKey = this._HelperService.AppConfig.ActiveReferenceKey;


      this.LoadData();

    });


  }

  LoadData() {
    this.GetSalesReport();
    // this.GetTransationReport();
    this.TUTr_Setup();
  }

  //#region Sales History General Method 

  private pDataSales = {
    Task: 'getbalance',
    AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
    AccountId: this._HelperService.AppConfig.ActiveReferenceId,
    Type: null
  };

  BalanceData: any = {
    "ThankUCash": {
      "Credit": 0.0,
      "Debit": 0.0,
      "Balance": 0.0,
      "LastTransactionDate": "0001-01-01T00:00:00"
    },
    "ThankUCashPlus": {
      "Credit": 0.0,
      "Debit": 0.0,
      "Balance": 0.0,
      "LastTransactionDate": "0001-01-01T00:00:00"
    },
    "GiftCard": {
      "Credit": 0.0,
      "Debit": 0.0,
      "Balance": 0.0,
      "LastTransactionDate": "0001-01-01T00:00:00"
    },
    "GiftPoint": {
      "Credit": 0.0,
      "Debit": 0.0,
      "Balance": 0.0,
      "LastTransactionDate": "0001-01-01T00:00:00"
    }
  };

  GetSalesReport() {

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Customer, this.pDataSales);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.BalanceData = _Response.Result as OSalesTrendData[];
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  pTransationData = {
    Task: "gettransactions",
    TotalRecords: 0,
    Offset: 0,
    Limit: 10,
    RefreshCount: true,
    // AccountId: this._HelperService.AppConfig.ActiveReferenceId,
    SearchCondition: "StatusCode = \"transaction.success\"  AND UserAccountId = \"6\"",
    SortExpression: "TransactionDate desc",
    Type: null,
    ReferenceKey: null,
    ReferenceId: 0,
    SubReferenceId: 0,
    IsDownload: false
  }
  TUTr_Config: any;
  GetTransationReport() {

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.TransationReport, this.pTransationData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.TUTr_Config = _Response.Result as OSalesTrendData[];
          // console.log('this.TUTr_Config', this.TUTr_Config);
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }



  //#endregion
  public TUTr_Config_test: OList;
  TUTr_Setup() {
    this.TUTr_Config =
    {
      Id: null,
      Task: "gettransactions",
      Location: this._HelperService.AppConfig.NetworkLocation.V3.TransationReport,
      Title: 'Wallet activity',
      // StatusType: 'giftcard',

      SearchBaseCondition: this.TUTrGPointsDebit_Setup_SearchCondition(),
      // Status: this._HelperService.AppConfig.StatusList.defaultitem,

      Type: null,
      ReferenceKey: null,
      ReferenceId: 0,
      SubReferenceId: 0,
      IsDownload: false,
      RefreshCount: true,
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'TransactionDate',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },
      TableFields: [
        {
          DisplayName: '#',
          SystemName: 'ReferenceId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Date',
          SystemName: 'TransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Show: true,
          Search: true,
          Sort: true,
          IsDateSearchField: true,
        },
        {
          DisplayName: 'Transaction Status',
          SystemName: 'StatusName',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Show: true,
          Search: false,
          Sort: false,
        },



      ]
    }

    this.TUTr_Config_test = this._DataHelperService.List_Initialize(this.TUTr_Config);
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.MerchantSales,
      this.TUTr_Config_test
    );
    this.TUTr_GetData();
  }

  TUTrGPointsDebit_Setup_SearchCondition() {

    var SearchCondition = "";

    SearchCondition = this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'transaction.success', '='),
      SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');


    return SearchCondition;
  }

  TUTr_GetData() {
    // this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.gettransactionsoverview);
    var TConfig = this._DataHelperService.List_GetDataTur(this.TUTr_Config);
    this.TUTr_Config_test = TConfig;
    // console.log("TUTr_Config_test",this.TUTr_Config_test);
  }

  TUTr_ToggleOption(event: any, Type: any) {

  
    this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config_test);

    this.TUTr_Config_test = this._DataHelperService.List_Operations(this.TUTr_Config_test, event, Type);

  

    
    if (event != null) {
        for (let index = 0; index < this.TUTr_Config_test.Sort.SortOptions.length; index++) {
            const element = this.TUTr_Config_test.Sort.SortOptions[index];
            if (event.SystemName == element.SystemName) {
                element.SystemActive = true;
            }
            else {
                element.SystemActive = false;
            }
        }
    }
    this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.TUTr_Config_test
    );
    this.TUTr_Config_test = this._DataHelperService.List_Operations(
        this.TUTr_Config_test,
        event,
        Type
    );
    if (
        (this.TUTr_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
    ) {
        this.TUTr_GetData();
    }
}





  // hideDailyPicker: boolean = true;
  hideWeeklyPicker: boolean = true;
  // hideMonthlyPicker: boolean = true;

  ShowHideCalendar(type: string) {
    switch (type) {
      // case this._HelperService.AppConfig.DatePickerTypes.hour: {
      //   this.hideDailyPicker = !this.hideDailyPicker;
      // }

      //   break;
      case this._HelperService.AppConfig.DatePickerTypes.week: {
        this.hideWeeklyPicker = !this.hideWeeklyPicker;
      }

        break;
      // case this._HelperService.AppConfig.DatePickerTypes.month: {
      //   this.hideMonthlyPicker = !this.hideMonthlyPicker;
      // }

      //   break;

      default:
        break;
    }
  }

  walletActivity = [
    {
      Credit: 87.33,
      Source: "revard",
      Usages: "trebet Superstor",
      Date: "12-06-2022"
    },
    {
      Credit: 87.33,
      Source: "revard",
      Usages: "trebet Superstor",
      Date: "12-06-2022"
    },
    {
      Credit: 87.33,
      Source: "revard",
      Usages: "trebet Superstor",
      Date: "12-06-2022"
    },
    {
      Credit: 87.33,
      Source: "revard",
      Usages: "trebet Superstor",
      Date: "12-06-2022"
    },
    {
      Credit: 87.33,
      Source: "revard",
      Usages: "trebet Superstor",
      Date: "12-06-2022"
    },
    {
      Credit: 87.33,
      Source: "revard",
      Usages: "trebet Superstor",
      Date: "12-06-2022"
    },
    {
      Credit: 87.33,
      Source: "revard",
      Usages: "trebet Superstor",
      Date: "12-06-2022"
    }
  ]

}
export class OAccountOverview {
  public TerminalStatus?: any;
  public TotalTerminals?: number;
  public TotalSale?: number;
  public AverageTransactionAmount?: number;
  public TotalTransactions?: number;
  public ActiveTerminalsPerc?: number;
  public IdleTerminalsPerc?: number;
  public DeadTerminalsPerc?: number;
  public UnusedTerminalsPerc?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public Idle?: number;
  public Total?: number;

  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;

  public CardInvoiceAmount: number;
  public CardTransaction: number;
  public CashInvoiceAmount: number;
  public CashTransaction: number;
  public InvoiceAmount: number;
}


// Task: 'getbalance',
// Location: this._HelperService.AppConfig.NetworkLocation.V3.Customer,
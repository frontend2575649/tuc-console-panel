import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { Ng5SliderModule } from 'ng5-slider';
import { CustomerOverviewComponent } from "./overview.component";
import { ChartsModule } from 'ng2-charts';
import { AgmCoreModule } from '@agm/core';
import { AgmOverlays } from 'agm-overlays';




const routes: Routes = [{ path: "", component: CustomerOverviewComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OverviewRoutingModule { }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    OverviewRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    
    Ng5SliderModule,
    ChartsModule,
    AgmOverlays,
    AgmCoreModule.forRoot({
        apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
    }),
  ],
  declarations: [CustomerOverviewComponent]
})
export class CustomerOverviewModule { }

import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs';
import { DataHelperService } from "src/app/service/datahelper.service";
import { HelperService } from "src/app/service/helper.service";
import { OResponse } from "src/app/service/object.service";
import swal from "sweetalert2";

declare var $: any;
declare var moment: any;

@Component({
  selector: "tuc-customerOverview",
  templateUrl: "./overview.component.html",
  styles: [
    `
      agm-map {
        height: 300px;
      }
    `
  ]
})

export class CustomerOverviewComponent implements OnInit {

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.CustomerData.next(params);
      this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];

    });
    this.accountKey = this._HelperService.AppConfig.ActiveReferenceId;
  }
  accountKey: any;
  public CustomerTUC_Class = [
    { data: [0], label: 'Food' },
    { data: [0], label: 'Electronics' },
    { data: [0], label: 'Fashion' },
    { data: [0], label: 'Bar' },
  ]
  ngOnInit(): void {

    //  console.log(" this._HelperService.AppConfig.ActiveReferenceId", this._HelperService.AppConfig.ActiveReferenceId)
    // this.Customer_TUC_Class();
    this.GetMostVisitedPlaces();
    this.getFrequentlyCategory();
    this.GetPaymentsModeOverview();


  }

  // TUC_class: any;
  // Customer_TUC_Class() {

  //   this.TUC_class = {
  //     FashionFood: 12,
  //     Electronics: 50,
  //     Fashion: 40,
  //     Bar: 57,

  //   }
  //   this.CustomerTUC_Class = [
  //     { data: [this.TUC_class.Food], label: "Food" },
  //     { data: [this.TUC_class.Electronics], label: "Electronics" },
  //     { data: [this.TUC_class.Fashion], label: "Fashion" },
  //     { data: [this.TUC_class.Bar], label: "Bar" },
  //   ];
  // }

  // visitedPlaces=[
  //   {
  //     Latitude: 9.043742,
  //     Longitude: 7.4921611,
  //     DisplayName: "SAHYADRI5",
  //     IconUrl: "https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/o/2022/7/3fa6f862df60443ebea2ff0ca5f05f47.png",
  //   },
  //   {
  //     Latitude: 9.1121146,
  //     Longitude: 7.377227299999999,
  //     DisplayName: "pune",
  //     IconUrl: "https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/defaults/defaulticon.png",
  //   },
  //   {
  //     Latitude: 9.0764785,
  //     Longitude: 7.398574,
  //     DisplayName: "SHIKSHA1",
  //     IconUrl: "https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/defaults/defaulticon.png",
  //   } ,
  //   {
  //     Latitude: 6.439834500000001,
  //     Longitude: 3.4342072,
  //     DisplayName: "SAHYADRI",
  //     IconUrl: "https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/o/2022/7/3fa6f862df60443ebea2ff0ca5f05f47.png",
  //   }
  // ]


  // Start Frequently Visited Places Data
  lat = 9.0820;
  long = 8.6753;
  zoom = 5;

  onMouseOver(infoWindow, $event: MouseEvent) {

    infoWindow.open();
  }

  onMouseOut(infoWindow, $event: MouseEvent) {
    infoWindow.close();
  }


  pDataMostVisitedPlaces = {};
  visitedPlaces: any;
  startDateToApply: any

  GetMostVisitedPlaces() {
    let startDateToApply = new Date(2017, 0, 1, 0, 0, 0, 0);
    let restructurestartDate = startDateToApply.toISOString();
    let endDate = moment().endOf("day");
    let restructureendDate = endDate.toISOString();

    this.pDataMostVisitedPlaces['Task'] = 'getfrequentlyvisitedplaces'
    this.pDataMostVisitedPlaces['StartDate'] = restructurestartDate;
    this.pDataMostVisitedPlaces['EndDate'] = restructureendDate;
    this.pDataMostVisitedPlaces['AccountId'] = this.accountKey;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.mostVisitedPlaces, this.pDataMostVisitedPlaces);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          // this.BalanceData = _Response.Result as OSalesTrendData[];
          if (_Response.Result.Data.length) {
            this.visitedPlaces = _Response.Result.Data;
          }

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  // Start Frequently Visited Places Data

  // Start  Frequently purchased  Category Data
  pfrequentlyCategory = {}
  TUC_class={
    Food: 0,
    Electronics: 0,
    Fashion: 0,
    Bar: 0,
  }
  getFrequentlyCategory() {
    console.log("test Data");
    let startDateToApply = new Date(2017, 0, 1, 0, 0, 0, 0);
    let formatStartDate = startDateToApply.toISOString();
    let endDate = moment().endOf("day");
    let formatEndDate = endDate.toISOString();

    this.pfrequentlyCategory['Task'] = 'getfrequentlypurchasedcategories'
    this.pfrequentlyCategory['StartDate'] = formatStartDate;
    this.pfrequentlyCategory['EndDate'] = formatEndDate;
    this.pfrequentlyCategory['AccountId'] = this.accountKey;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.frequentlyPurchasedCategory, this.pfrequentlyCategory);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data.length) {
            let _Category = _Response.Result.Data;
            this.TUC_class.Food = _Category.Food;
            this.TUC_class.Electronics = _Category.Electronics;
            this.TUC_class.Fashion = _Category.Fashion;
            this.TUC_class.Bar = _Category.Bar;
            this.CustomerTUC_Class = [
              { data: [this.TUC_class.Food], label: "Food" },
              { data: [this.TUC_class.Electronics], label: "Electronics" },
              { data: [this.TUC_class.Fashion], label: "Fashion" },
              { data: [this.TUC_class.Bar], label: "Bar" },
            ];
          }
          else  {
            // this.TUC_class['Food'] = this.no_CategoryData.Food;
            // this.TUC_class['Electronics'] = this.no_CategoryData.Electronics;
            // this.TUC_class['Fashion'] = this.no_CategoryData.Fashion;
            // this.TUC_class['Bar'] = this.no_CategoryData.Bar;
            // console.log("this.TUC_class",this.TUC_class);
            this.CustomerTUC_Class = [
              { data: [this.TUC_class.Food], label: "Food" },
              { data: [this.TUC_class.Electronics], label: "Electronics" },
              { data: [this.TUC_class.Fashion], label: "Fashion" },
              { data: [this.TUC_class.Bar], label: "Bar" },
            ];
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  // End Frequently purchased  Category Data
  // Start Card VS Cash Chart Data
  PaymentModes = ['0', '0'];
  public PaymentsModeStatus = ['0', '0'];
  public PaymentModelabel = ['Card', 'Cash'];

  PaymentsMode: any = {};
  pPaymenttsModeData = {};
  LoadingChart: boolean = true;
  showDailyChart: boolean = true;

  GetPaymentsModeOverview() {

    // this._HelperService.IsFormProcessing = true;
    let startDateToApply = new Date(2017, 0, 1, 0, 0, 0, 0);
    let formatStartDate = startDateToApply.toISOString();
    let endDate = moment().endOf("day");
    let formatEndDate = endDate.toISOString();
    this.pPaymenttsModeData['Task'] = 'getcustomerstransactions'
    this.pPaymenttsModeData['StartDate'] = formatStartDate;
    this.pPaymenttsModeData['EndDate'] = formatEndDate;
    this.pPaymenttsModeData['AccountId'] = this.accountKey;

    this.LoadingChart = true;


    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.customerCardCashTransaction, this.pPaymenttsModeData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.LoadingChart = false;
          //#endregion
          this.showDailyChart = false;
          this._ChangeDetectorRef.detectChanges();
          // console.log(_Response.Result,"payment modes response");


          this.PaymentsMode = _Response.Result as any;
          this.PaymentModes[0] = this.PaymentsMode.CardTransactions;
          this.PaymentModes[1] = this.PaymentsMode.CashTransactions;

          this.PaymentsModeStatus[0] = this.PaymentsMode.CardTransactions;
          this.PaymentsModeStatus[1] = this.PaymentsMode.CashTransactions;
          this.showDailyChart = true;
          this._ChangeDetectorRef.detectChanges();
          this.LoadingChart = false;




        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  // End Card VS Cash Chart Data
}
import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Chart from "chart.js";
import * as Feather from "feather-icons";
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs';
import { DataHelperService } from "src/app/service/datahelper.service";
import { HelperService } from "src/app/service/helper.service";
import swal from "sweetalert2";

declare var $: any;
declare var moment: any;

@Component({
  selector: "tuc-customerrewards",
  templateUrl: "./rewards.component.html",
  
})

export class CustomerrewardComponent implements OnInit {

  public chart: Chart;
public rewardsEarned=[
  {
    rewards: 10000,
    Transactions: 50,
  
},
{
  rewards: 15000,
  Transactions: 60,

},
{
  rewards: 17000,
  Transactions: 70,

},
{
  rewards: 20000,
  Transactions: 100,

},
]
constructor(
  public _Router: Router,
  public _ActivatedRoute: ActivatedRoute,
  public _FormBuilder: FormBuilder,
  public _HelperService: HelperService,
  public _DataHelperService: DataHelperService,
  public _ChangeDetectorRef: ChangeDetectorRef
){

}
  
    ngOnInit(): void {
       
    }

    ngAfterViewInit() {
      this.chart = new Chart('canvas', {
        type: 'bar',
        data: {
          labels: ['Jan', 'Feb', 'March', 'April', 'May', 'June',"july",'August','Sep','Oct','Nov','Dec'],
          datasets: [
            {
              label: 'Reward Trend ',
              data: [10, 12, 9, 1,15,17,10,20,19,9,10,20],
              backgroundColor: ["#00cccc","#00cccc","#00cccc","#00cccc","#00cccc","#00cccc","#00cccc","#00cccc","#00cccc","#00cccc","#00cccc","#00cccc"],
              borderColor:  ["#00cccc"],
              borderWidth: 1,
            },
          ],
        },
      });
    }
   
}
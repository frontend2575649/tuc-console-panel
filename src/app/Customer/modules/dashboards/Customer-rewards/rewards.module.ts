import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { Ng5SliderModule } from 'ng5-slider';

import { ChartsModule } from 'ng2-charts';
import { CustomerrewardComponent } from "./rewards.component";




const routes: Routes = [{ path: "", component: CustomerrewardComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRewardRoutingModule { }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    CustomerRewardRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    
    Ng5SliderModule,
    ChartsModule
  ],
  declarations: [CustomerrewardComponent]
})
export class CustomerrewardsModule { }

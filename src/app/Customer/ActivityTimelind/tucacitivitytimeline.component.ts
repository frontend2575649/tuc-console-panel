import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs';
import swal from "sweetalert2";

declare var $: any;
declare var moment: any;

@Component({
  selector: "tuc-activitytimeline",
  templateUrl: "./tucacitivitytimeline.component.html",
  styleUrls: ["./tucacitivitytimeline.component.css"]
})

export class TucAcitivityTimelineComponent implements OnInit {
  timeLine = [
    {
      // Title: "@twitter thanks for you appreciation and @google thanks for you appreciation",
      Title: "@twitter thanks for you appreciation",
      Date: "13 june 18",
      SubTitle: "Test1",
      TotalVisit:"1",
      imgsrc:"../../../assets/Aloo-Matar.jpeg",
      spentAmount:"123",
      tAmount:"123"

    },
    {
      Title: "Do you know how Google search works.",
      Date: "10 june 18",
      SubTitle: "Test1",
      TotalVisit:"1",
      imgsrc:"../../../assets/Aloo-Matar.jpeg",
      spentAmount:"123",
      tAmount:"123"

    },
    {
      Title: "for iphone 7",
      Date: "13 june 18",
      SubTitle: "Test1",
      TotalVisit:"1",
      imgsrc:"../../../assets/Aloo-Matar.jpeg",
      spentAmount:"123",
      tAmount:"123"

    },
    {
      Title: "you will get your products",
      Date: "11 june 18",
      SubTitle: "Test1",
      TotalVisit:"1",
      imgsrc:"../../../assets/Aloo-Matar.jpeg",
      spentAmount:"123",
      tAmount:"123"

    },
    {
      Title: "discover vital information about your readers.",
      Date: "10 june 18",
      SubTitle: "Test1",
      TotalVisit:"1",
      imgsrc:"../../../assets/Aloo-Matar.jpeg",
      spentAmount:"123",
      tAmount:"123"

    },
    

  ]
  favCategory=[
    {
      DisplayName:"Test1",
      SubTitle: "Test1",
      TotalVisit:"1",
      imgsrc:"../../../assets/Aloo-Matar.jpeg"
    },
    {
     
      DisplayName:"Test2",
      SubTitle: "Test1",
      TotalVisit:"1",
      imgsrc:"../../../assets/Aloo-Matar.jpeg"
    },
    {
      SubTitle: "Test1",
      DisplayName:"Test3",
      TotalVisit:"1",
      imgsrc:"../../../assets/Aloo-Matar.jpeg"
    },
    {
      DisplayName:"Test4",
      SubTitle: "Test1",
      TotalVisit:"1",
      imgsrc:"../../../assets/Aloo-Matar.jpeg"
    },
    {
      DisplayName:"Test5",
      SubTitle: "Test1",
      TotalVisit:"1",
      imgsrc:"../../../assets/Aloo-Matar.jpeg"
    },
    {
      DisplayName:"Test6",
      SubTitle: "Test1",
      TotalVisit:"1",
      imgsrc:"../../../assets/Aloo-Matar.jpeg"
    },
  ]
  transitionData=[
    {
      Title:"Airtime"
    },
    {
      Title:"Deals"
    },
    {
      Title:"TUC Pay"
    },
    {
      Title:"Airtime"
    },
    {
      Title:"Deals"
    },
    {
      Title:"TUC Pay"
    }
    
  ]
  constructor() { }

  ngOnInit(): void {

  }

}
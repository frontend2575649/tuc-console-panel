import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as Feather from 'feather-icons';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { OSelect, OList, DataHelperService, HelperService, OResponse, FilterHelperService } from '../../../service/service';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
declare var $: any;

@Component({
    selector: 'app-tuproducts',
    templateUrl: './tuproducts.component.html'
})
export class TuproductsComponent implements OnInit {

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _FilterHelperService: FilterHelperService



    ) {

        this._HelperService.ShowDateRange = false;

    }

    public MerchantKey;
    public MerchantId;
    public ProductList_Filter_Owners_Option: Select2Options;
    public ProductList_Filter_Owners_Selected = 0;
    public TUVariantList_Config: OList;
    public _VProductDetail: any = {};
    public _VariantDetail: any = {};
    public ReferenceId: any = {};
    public ReferenceKey: any = {};
    public ProductId: any = {};
    public ProductKey: any = {};
    public SelectedSKU: any = {};
    public ProductName: string;
    public ProductPrice: number;
    public RewardPercentage: number;

    ngOnInit() {
        this._HelperService.StopClickPropogation();

        this._ActivatedRoute.params.subscribe((params: Params) => {
            this.MerchantKey = params["referencekey"];
            this.MerchantId = params["referenceid"];
        })
        Feather.replace();
        this.Form_AddProduct_Load();
        this.GetProductCategories_List();
        this.ProductList_Filter_Owners_Load();
        this.TUVariantList_Setup();

        this.Form_Product_Load();
        this.GetProductSubCategories_List();

    }
    Form_AddProduct: FormGroup;
    Form_AddProduct_Clear() {
        this.Form_AddProduct.reset();
        this.Form_AddProduct_Load();
    }
    Form_AddProduct_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_AddProduct = this._FormBuilder.group({

            Task: this._HelperService.AppConfig.Api.ThankUCash.SaveTerminalProduct,
            OperationType: 'new',
            AccountId:  this._HelperService.AppConfig.PTSP.ActiveMerchantReferenceId,
            AccountKey:  this._HelperService.AppConfig.PTSP.ActiveMerchantReferenceKey,
            Sku: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(20)])],
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            SellingPrice: [null, Validators.compose([Validators.required, Validators.min(1), Validators.max(99999999)])],
            ActualPrice: [null],
            CategoryName: [null, Validators.required],
            SubCategoryName: [null, Validators.required],
            RewardPercentage: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])],
            StatusCode: this._HelperService.AppConfig.Status.Active,
            Description: [null],
            TotalStock: 0,
            AvailableStock: 0,
           // ReferenceNumber: [null],
            ReferenceNumber: this._HelperService.GenerateGuid(),  
            ShortDescription: [null],
            MaximumRewardAmount: 0,

        });
    }
    Form_AddProduct_Process(_FormValue) {
        _FormValue.ActualPrice = _FormValue.SellingPrice;
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Ptsp.V3.TUCPtspProduct, _FormValue);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_FormValue.OperationType == 'new') {
                        this._HelperService.NotifySuccess('Product account created successfully.');
                        this.Form_AddProduct_Clear();
                        this._HelperService.CloseModal('form_AddProduct_Content');
                        this.TUVariantList_Setup();
                    }

                }
                else {
                    this.Form_AddProduct_Clear();
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });

    }
    ProductList_Filter_Owners_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Product.getproducts,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.HCProduct,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            Fields: [
                {
                    SystemName: 'ReferenceId',
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
            ]
        };

        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.ProductList_Filter_Owners_Option = {
            placeholder: 'Filter by Products',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }


    TUVariantList_Setup() {
        this.TUVariantList_Config =
        {
            Id: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminalProducts,
            Location: this._HelperService.AppConfig.NetworkLocation.Ptsp.V3.TUCPtspProduct,
            Title: 'Product List',
            ReferenceId: this._HelperService.AppConfig.PTSP.ActiveMerchantReferenceId,
            ReferenceKey: this._HelperService.AppConfig.PTSP.ActiveMerchantReferenceKey,
            Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
            TableFields: this.TUVariantList_Setup_Fields(),
        }
        this.TUVariantList_Config = this._DataHelperService.List_Initialize(this.TUVariantList_Config);

        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.Merchant,
            this.TUVariantList_Config
        );
        this.TUVariantList_GetData();
    }

    TUVariantList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.TUVariantList_Config);
        this.TUVariantList_Config = TConfig;
    }
    TUVariantList_ToggleOption_Date(event: any, Type: any) {
        this.TUVariantList_ToggleOption(event, Type);
    }
    TUVariantList_ToggleOption(event: any, Type: any) {
        if (event != null) {
            for (let index = 0; index < this.TUVariantList_Config.Sort.SortOptions.length; index++) {
                const element = this.TUVariantList_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }
            }
        }

        this._HelperService.Update_CurrentFilterSnap(
            event,
            Type,
            this.TUVariantList_Config


        );

        this.TUVariantList_Config = this._DataHelperService.List_Operations(
            this.TUVariantList_Config,
            event,
            Type
        );

        if (
            (this.TUVariantList_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.TUVariantList_GetData();
        }
    }


    TUVariantList_Setup_Fields() {
        var TableFields = [];
        TableFields = [

            {
                DisplayName: 'Name',
                SystemName: 'Name',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: true,
            },
            {
                DisplayName: 'SKU',
                SystemName: 'Sku',
                DataType: this._HelperService.AppConfig.DataType.Decimal,
                Show: true,
                Search: true,
                Sort: false,
            },
            {
                DisplayName: 'Price',
                SystemName: 'SellingPrice',
                DataType: this._HelperService.AppConfig.DataType.Decimal,
                Show: true,
                Search: false,
                Sort: false,
            },
            {
                DisplayName: 'Reward %',
                SystemName: 'RewardPercentage',
                DataType: this._HelperService.AppConfig.DataType.Decimal,
                Show: true,
                Search: true,
                Sort: true,
            },
            {
                DisplayName: 'Stock',
                SystemName: 'TotalStock',
                DataType: this._HelperService.AppConfig.DataType.Decimal,
                Show: true,
                Search: false,
                Sort: false,
            },
            {
                DisplayName: 'Added On',
                SystemName: 'CreateDate',
                DataType: this._HelperService.AppConfig.DataType.Date,
                Show: true,
                Search: true,
                Sort: true,
                IsDateSearchField: true,
            },
        ];
        return TableFields;
    }


    form_AddProduct_Open() {
        this._HelperService.OpenModal('form_AddProduct_Content');
    }

    TUVariant_RowSelected(ReferenceData) {
        this.ProductId = ReferenceData.ReferenceId;
        this.ProductKey = ReferenceData.ReferenceKey;
        this.Get_ProductDetails();

    }

    Get_ProductDetails() {
        this._HelperService.AppConfig.ShowHeader = true;
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: "getterminalproduct",
            ReferenceId: this.ProductId,
            ReferenceKey: this.ProductKey
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Ptsp.V3.TUCPtspProduct, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.ProductId = _Response.Result.ReferenceId;
                    this.ProductKey = _Response.Result.ReferenceKey;
                    this.ProductName = _Response.Result.Name;
                    this.SelectedSKU = _Response.Result.Sku;
                    this.ProductPrice = _Response.Result.SellingPrice;
                    this.RewardPercentage = _Response.Result.RewardPercentage;
                    this.GetProductCategories_Option.placeholder = _Response.Result.CategoryName;
                    this.GetProductSubCategories_Option.placeholder = _Response.Result.SubCategoryName;
                    this.Form_Product.controls['CategoryName'].setValue(_Response.Result.CategoryName);
                    this.Form_Product.controls['SubCategoryName'].setValue(_Response.Result.SubCategoryName);
                    this._ChangeDetectorRef.detectChanges;

                    this._HelperService.OpenModal('form_EditProduct_Content');

                } else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    Form_Product: FormGroup;
    Form_Product_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_Product = this._FormBuilder.group({

            Task: "updateterminalproduct",
            OperationType: 'edit',
            ReferenceId: this.ProductId,
            ReferenceKey: this.ProductKey,

            Sku: [null],
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(30)])],
            SellingPrice: [null, Validators.compose([Validators.required, Validators.min(1), Validators.max(99999999)])],
            ActualPrice: [null],
            RewardPercentage: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])],
            StatusCode: this._HelperService.AppConfig.Status.Active,
            Description: [null],
            CategoryName: [null, Validators.required],
            SubCategoryName: [null, Validators.required],
            TotalStock: 0,
            AvailableStock: 0,
            ReferenceNumber: [null],
            ShortDescription: [null],
            MaximumRewardAmount: 0,

        });

    }

    Form_Product_Process(_FormValue: any) {

        _FormValue.ReferenceKey = this.ProductKey,
            _FormValue.ReferenceId = this.ProductId;
        _FormValue.ActualPrice = _FormValue.SellingPrice;
        if (this._HelperService._Icon_Cropper_Data.Content != null) {
            _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
        }
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Ptsp.V3.TUCPtspProduct, _FormValue);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess('Product account updated successfully.');
                    this._HelperService.CloseModal('form_EditProduct_Content');
                    this.TUVariantList_Setup();
                    this._HelperService.IsFormProcessing = false;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });

    }

    public GetProductCategories_Option: Select2Options;

    GetProductCategories_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: "getcategories",
            Location: this._HelperService.AppConfig.NetworkLocation.V2.HCProduct,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'Name',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetProductCategories_Option = {
            placeholder: PlaceHolder,
            ajax: _Transport,
            multiple: false
        };
    }

    GetProductCategories_ListChange(event: any) {
        this.Form_AddProduct.patchValue({
            CategoryName: event.data[0].Name

        })


        this.Form_Product.patchValue(
            {
                CategoryName: event.data[0].Name

            }
        );

    }

    public GetProductSubCategories_Option: Select2Options;

    GetProductSubCategories_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Product.getsubcategories,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.HCProduct,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'Name',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        // _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'ReferenceId', this._HelperService.AppConfig.DataType.Number, '0', '>');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;

        this.GetProductSubCategories_Option = {
            placeholder: PlaceHolder,
            ajax: _Transport,
            multiple: false,
        };
    }

    GetProductSubCategories_ListChange(event: any) {
        this.Form_AddProduct.patchValue(
            {
                SubCategoryName: event.data[0].Name
            }
        );

        this.Form_Product.patchValue(
            {
                SubCategoryName: event.data[0].Name

            }
        );
    }
    Delete_Confirm() {
        swal({
            position: 'top',
            title: this._HelperService.AppConfig.CommonResource.DeleteProductDetails,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var pData = {
                    Task: "deleteterminalproduct",
                    ReferenceId: this.ProductId,
                    ReferenceKey: this.ProductKey,
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Ptsp.V3.TUCPtspProduct, pData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess(_Response.Message);
                            this._HelperService.CloseModal('ModalDetails');
                            this.TUVariantList_Setup();
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {

                        this._HelperService.HandleException(_Error);
                    });
            }
        });
    }
    public SelectedProductItem =
        {
            ReferenceId: null,
            ReferenceKey: null,
            AccountId: null,
            AccountKey: null,
            AccountDisplayName: null,
            CategoryKey: null,
            CategoryName: null,
            Name: null,
            SystemName: null,
            Description: null,
            IconUrl: null,
            PosterUrl: null,
            MinimumAmount: null,
            MaximumAmount: null,
            ActualPrice: null,
            SellingPrice: null,
            AllowMultiple: null,
            RewardPercentage: null,
            MaximumRewardAmount: null,
            TotalStock: null,
            CreateDate: null,
            CreatedByDisplayName: null,
            CreatedByKey: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
        };




    SetOtherFilters(): void {
        this.TUVariantList_Config.SearchBaseConditions = [];
        this.TUVariantList_Config.SearchBaseCondition = null;

        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
        if (CurrentIndex != -1) {
            //   this.MerchantsList_Filter_Owners_Selected = null;
            //   this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }

    //#region filterOperations

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetMerchantConfig(this.TUVariantList_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion

        this.TUVariantList_GetData();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_Merchant(Type, index);
        this._FilterHelperService.SetMerchantConfig(this.TUVariantList_Config);

        this.SetOtherFilters();

        this.TUVariantList_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            input: "text",
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            inputAttributes: {
                autocapitalize: "off",
                autocorrect: "off",
                maxLength: "32",
                minLength: "4",
            },

            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_Merchant(result.value);
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.Merchant
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }


        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._HelperService.Delete_Filter(
                    this._HelperService.AppConfig.FilterTypeOption.Merchant
                );
                this._FilterHelperService.SetMerchantConfig(this.TUVariantList_Config);
                this.TUVariantList_GetData();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });

    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        this._HelperService.MakeFilterSnapPermanent();
        this.TUVariantList_GetData();
        this.ResetFilterUI();

        if (ButtonType == 'Sort') {
            $("#MerchantsList_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#MerchantsList_fdropdown").dropdown('toggle');
        }

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetMerchantConfig(this.TUVariantList_Config);
        this.SetOtherFilters();

        this.TUVariantList_GetData();
        this.ResetFilterUI();
        this._HelperService.StopClickPropogation();
    }



    ResetFilterControls: boolean = true;
    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();

        // this.MerchantsList_Filter_Owners_Load();

        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();
    }



}

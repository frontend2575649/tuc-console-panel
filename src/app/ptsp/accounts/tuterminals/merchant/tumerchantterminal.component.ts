import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { HelperService, DataHelperService, OList, OSelect, FilterHelperService } from 'src/app/service/service';
import * as Feather from 'feather-icons';
declare var moment: any;
import swal from "sweetalert2";
declare var $: any;
@Component({
  selector: 'app-tumerchantterminal',
  templateUrl: './tumerchantterminal.component.html'
})
export class TumerchantterminalComponent implements OnInit {

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService,

  ) {

    this._HelperService.ShowDateRange = false;

  }
  public merchantId = 0;
  public PtspId = 0;
  public TodayStartTime = null;
  public TodayEndTime = null;
  Type = 5;
  StartTime = null;
  EndTime = null;
  ngOnInit() {
    Feather.replace();
    this._HelperService.StopClickPropogation();

    this.TerminalsList_Filter_Stores_Load();
    if (this.StartTime == undefined) {
      this.StartTime = moment().startOf('month');
      this.EndTime = moment().endOf('month');
    }

    if (this.TodayStartTime == undefined) {

      this.TodayStartTime = moment().startOf('day');
      this.TodayEndTime = moment().add(1, 'minutes');
    }
    this._ActivatedRoute.params.subscribe((params: Params) => {
      // this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      // this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
      if (this._HelperService.AppConfig.PTSP.ActiveMerchantReferenceKey == null && this._HelperService.AppConfig.PTSP.ActiveMerchantReferenceId == null) {
        if (this._HelperService.AppConfig.ActiveReferenceId != undefined && this._HelperService.AppConfig.ActiveReferenceId != null) {
          this.merchantId = this._HelperService.AppConfig.PTSP.ActiveMerchantReferenceId;
          this.PtspId = this._HelperService.AppConfig.ActiveReferenceId;
          this.TerminalsList_Setup();
          this.TerminalsList_Filter_Banks_Load();
        }
        else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
        }
      } else {
        this.merchantId = this._HelperService.AppConfig.PTSP.ActiveMerchantReferenceId;

        this.TerminalsList_Setup();
        this.TerminalsList_Filter_Banks_Load();
      }
    });
    this._HelperService.GetAccountOverviewSub(this._HelperService.AppConfig.ActiveReferenceId, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.PTSP.ActiveMerchantReferenceId,
      this._HelperService.AppConfig.PTSP.ActiveMerchantReferenceKey, this.StartTime, this.EndTime);
  }
  public TerminalsList_Config: OList;

  TerminalsList_Setup() {
    this.TerminalsList_Config = {
      Id: "MerchantsList",
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchantsTerminals,
      Location: this._HelperService.AppConfig.NetworkLocation.Ptsp.V3.TUCPtspAccount,
      Title: "All Terminals",
      StatusType: "default",

      Type: this._HelperService.AppConfig.ListType.All,
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.PTSP.ActiveMerchantReferenceId, '=='),
      Sort:
      {
        SortDefaultName: 'Added on',
        SortDefaultColumn: 'CreateDate',
        SortDefaultOrder: 'desc'
      },
      ListType: 1,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      // SubReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      // SubReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      TableFields: [
        {
          DisplayName: 'TID',
          SystemName: 'TerminalId',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: 'tx-normal',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Acquirer',
          SystemName: 'AcquirerName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: 'tx-normal',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Store',
          SystemName: 'StoreName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: 'tx-normal',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },

        {
          DisplayName: 'Last Tr',
          SystemName: 'LastTransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'tx-normal',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: false,
        },
        {
          DisplayName: 'StatusID',
          SystemName: 'StatusId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: false,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Status',
          SystemName: 'StatusCode',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: false,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Status',
          SystemName: 'StatusName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: 'tx-normal',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: "Added On",
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'tx-bold',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },

      ]
    };
    this.TerminalsList_Config = this._DataHelperService.List_Initialize(
      this.TerminalsList_Config
    );
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Terminal,
      this.TerminalsList_Config
    );
    this.TerminalsList_GetData();
  }
  TerminalsList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.TerminalsList_Config
    );
    this.TerminalsList_Config = TConfig;
  }
  TerminalsList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.ActiveTerminal, {
      "ReferenceKey": ReferenceData.ReferenceKey,
      "ReferenceId": ReferenceData.ReferenceId,
      "DisplayName": ReferenceData.DisplayName,
      "AccountTypeCode": this._HelperService.AppConfig.AccountType.PosTerminal
    });

    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.PTSPPanel.Terminal.Dashboard, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
  }
  TerminalsList_ToggleOption(event: any, Type: any) {
    if (event != null) {
      for (let index = 0; index < this.TerminalsList_Config.Sort.SortOptions.length; index++) {
        const element = this.TerminalsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.TerminalsList_Config


    );

    this.TerminalsList_Config = this._DataHelperService.List_Operations(
      this.TerminalsList_Config,
      event,
      Type
    );

    if (
      (this.TerminalsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TerminalsList_GetData();
    }
  }
  public TerminalsList_Filter_Bank_Option: Select2Options;
  public TerminalsList_Filter_Bank_Selected = 0;
  TerminalsList_Filter_Banks_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
        }
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Bank_Option = {
      placeholder: 'Sort by Bank',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Banks_Change(event: any) {
    if (event.value == this.TerminalsList_Filter_Bank_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Bank_Selected = 0;
    }
    else if (event.value != this.TerminalsList_Filter_Bank_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Bank_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '='));
    }
    this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }


  //#region filterOperations

  public TerminalsList_Filter_Store_Option: Select2Options;
  public TerminalsList_Filter_Store_Selected = null;
  TerminalsList_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
            SystemName: "ReferenceId",
            Type: this._HelperService.AppConfig.DataType.Number,
            Id: true,
            Text: false,
        },
        {
            SystemName: "DisplayName",
            Type: this._HelperService.AppConfig.DataType.Text,
            Id: false,
            Text: true
        },
        {
            SystemName: "AccountTypeCode",
            Type: this._HelperService.AppConfig.DataType.Text,
            SearchCondition: "=",
            SearchValue: this._HelperService.AppConfig.AccountType.Store
        }

    ]
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.Partner.ActiveMerchantReferenceId, '=');
    if (this.TerminalsList_Filter_Store_Selected != 0) {
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Store_Selected, '=');
    }
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Store_Option = {
      placeholder: 'Filter by Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Stores_Change(event: any) {

    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TerminalsList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Store
    );

    this.StoreEventProcessing(event);

  }

  StoreEventProcessing(event: any): void {
    if (event.value == this.TerminalsList_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Store_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Store_Selected = 0;
    } else if (event.value != this.TerminalsList_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Store_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Store_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "StoreId",
          this._HelperService.AppConfig.DataType.Number,
          this.TerminalsList_Filter_Store_Selected,
          "="
        )
      );
    }
    this.TerminalsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  SetOtherFilters(): void {
    this.TerminalsList_Config.SearchBaseConditions = [];
    this.TerminalsList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Terminal.Merchant));
    if (CurrentIndex != -1) {
      // this.TerminalsList_Filter_Merchant_Selected = null;
      // this.MerchantEventProcess(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }

    CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Terminal.Provider));
    if (CurrentIndex != -1) {
      this.TerminalsList_Filter_Bank_Selected = null;
      // this.BanksEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }

    CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Terminal.Store));
    if (CurrentIndex != -1) {
      this.TerminalsList_Filter_Store_Selected = null;
      this.StoreEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }
  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetPOSConfig(this.TerminalsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.TerminalsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        maxLength: "32",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_POS(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Terminal
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Terminal
        );
        this._FilterHelperService.SetPOSConfig(this.TerminalsList_Config);
        this.TerminalsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.TerminalsList_GetData();
    this.ResetFilterUI();

    if (ButtonType == 'Sort') {
      $("#TerminalsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#TerminalsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();

  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetPOSConfig(this.TerminalsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.TerminalsList_GetData();
    this.ResetFilterUI();
    this._HelperService.StopClickPropogation();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_POS(Type, index);
    this._FilterHelperService.SetPOSConfig(this.TerminalsList_Config);

    //#region setOtherFilters

    this.SetOtherFilters();

    //#endregion

    this.TerminalsList_GetData();
  }

  //#endregion

  ResetFilterControls: boolean = true;
  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    // this.TerminalsList_Filter_Merchants_Load();
    this.TerminalsList_Filter_Stores_Load();
    this.TerminalsList_Filter_Banks_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }


}

import { Component, OnInit, ViewChild, ElementRef, ViewChildren } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
declare var moment: any;
import swal from "sweetalert2";
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon, OOverview } from '../../../service/service';
import { DaterangePickerComponent } from 'ng2-daterangepicker';
import { ChangeContext } from 'ng5-slider';
import { BaseChartDirective } from 'ng2-charts';
import * as Feather from 'feather-icons';
import { NumberValueAccessor } from '@angular/forms/src/directives';

@Component({
  selector: 'app-tuposactivity',
  templateUrl: './tuposactivity.component.html',

})
export class TuposactivityComponent implements OnInit {


  public Options: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      }
    }
  }
  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
  ) {

  }
  public barChartLabels = ['day 1 ', 'day 2', 'day 3', 'day 4', 'day 5', "day 6", "day 7"];
  public barChartColors = [{ backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }];

  // public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [], label: 'Remote' },
    { data: [], label: 'Remote' },
    { data: [], label: 'Visit' },
    { data: [], label: 'Visit' },
  ];

  Type = 5;
  StartTime = null;
  StartTimeS = null;
  EndTime = null;
  EndTimeS = null;
  CustomType = 1;
  public TodayStartTime = null;
  public TodayEndTime = null;
  ngOnInit() {
    this._HelperService.FullContainer = false;
    Feather.replace();
    //start time and end time for overview
    this.TodayStartTime = moment().subtract(6, 'd').startOf('day');
    this.TodayEndTime = moment().endOf('day');

    //start time and end time for last 7 days data
    this.GetBranches_List();
    this.GetMangers_List();
    this.barChartLabels = this._HelperService.CalculateIntermediateDate(moment(this.TodayStartTime), moment(this.TodayEndTime));
    this.LoadData();


  }

  public GetBranches_Option: Select2Options;
  public GetBranches_Transport: any;
  GetBranches_List() {
    var PlaceHolder = "Select Branch";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.Ptsp.V3.TUCPtspAccount,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
      ]
    }
    this.GetBranches_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetBranches_Option = {
      placeholder: "All Branches",
      ajax: this.GetBranches_Transport,
      multiple: false,
    };
  }
  public GetMangers_Option: Select2Options;
  public GetMangers_Transport: any;
  GetMangers_List() {
    var PlaceHolder = "Select Manager";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.Ptsp.V3.TUCPtspAccount,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
      ]
    }

    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'RoleId', this._HelperService.AppConfig.DataType.Text, '8', '=');
    this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetMangers_Option = {
      placeholder: "All RMs",
      ajax: this.GetMangers_Transport,
      multiple: false,
    };
  }
  GetMangers_ListChange(event: any) {

  }
  LoadData() {

    this.GetAccountOverview(this._HelperService.AppConfig.ActiveReferenceId, this._HelperService.AppConfig.ActiveReferenceKey, this.TodayStartTime, this.TodayEndTime);
    this.GetLastSevenData();
    this.Alert_Terminal();
  }

  DateChanged(event: any, Type: any): void {
    this.TodayStartTime = moment(event.start).startOf("day");
    this.TodayEndTime = moment(event.end).endOf("day");
    //#region refreshTerminals 

    this.TerminalsList_Config = this._DataHelperService.List_Operations(
      this.TerminalsList_Config,
      event,
      Type
    );
    this.TerminalsList_GetData();

    //#endregion
    this.barChartLabels = this._HelperService.CalculateIntermediateDate(moment(event.start).startOf("day"), moment(event.end).endOf("day"));

    this.GetAccountOverview(this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.ActiveOwnerKey, this.TodayStartTime, this.TodayEndTime);

    this.GetLastSevenData();

  }

  UserAnalytics_DateChange(Type) {
    this.Type = Type;
    var SDate;
    if (Type == 1) { // today
      SDate =
      {
        start: moment().startOf('day'),
        end: moment().endOf('day'),
      }
    }
    else if (Type == 2) { // yesterday
      SDate =
      {
        start: moment().subtract(1, 'days').startOf('day'),
        end: moment().subtract(1, 'days').endOf('day'),
      }
    }
    else if (Type == 3) {  // this week
      SDate =
      {
        start: moment().startOf('isoWeek'),
        end: moment().endOf('isoWeek'),
      }
    }
    else if (Type == 4) { // last week
      SDate =
      {
        start: moment().subtract(1, 'weeks').startOf('isoWeek'),
        end: moment().subtract(1, 'weeks').endOf('isoWeek'),
      }
    }
    else if (Type == 5) { // this month
      SDate =
      {
        start: moment().startOf('month'),
        end: moment().endOf('month'),
      }
    }
    else if (Type == 6) { // last month
      SDate =
      {
        start: moment().startOf('month').subtract(1, 'month'),
        end: moment().startOf('month').subtract(1, 'days'),
      }
    }
    else if (Type == 7) {
      SDate =
      {
        start: new Date(2017, 0, 1, 0, 0, 0, 0),
        end: moment().endOf('day'),
      }
    }
    this.StartTime = SDate.start;
    this.EndTime = SDate.end;
    this.LoadData();
  }

  GetAccountOverview(
    UserAccountId,
    UserAccountKey,
    StartTime,
    EndTime
  ) {

    var Data = {
      Task: "getaccountoverview",
      StartTime: StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
      EndTime: EndTime, // moment().add(2, 'days'),
      AccountId: UserAccountId,
      AccountKey: UserAccountKey,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Ptsp.V3.TUCPtspAnalytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverview = _Response.Result as OAccountOverview;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._AccountOverview = _Response.Result as OAccountOverview;

            if (this._AccountOverview.TerminalStatus['Total'] != 0) {
              var _TempVal = this._HelperService.DivideTwoNumbers(100, this._AccountOverview.TerminalStatus['Total']);
              this._AccountOverview["IdleTerminalsPerc"] = this._AccountOverview.TerminalStatus['Idle'] * _TempVal;
              this._AccountOverview["ActiveTerminalsPerc"] = this._AccountOverview.TerminalStatus['Active'] * _TempVal;
              this._AccountOverview["DeadTerminalsPerc"] = this._AccountOverview.TerminalStatus['Dead'] * _TempVal;
              this._AccountOverview["UnusedTerminalsPerc"] = this._AccountOverview.TerminalStatus['Inactive'] * _TempVal;
            } else {
              this._AccountOverview["IdleTerminalsPerc"] = 0;
              this._AccountOverview["ActiveTerminalsPerc"] = 0;
              this._AccountOverview["DeadTerminalsPerc"] = 0;
              this._AccountOverview["UnusedTerminalsPerc"] = 0;
            }
          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public BeforeSevenDayTime = null;
  public TodayTime = null;

  public _LastSevenDaysData: any = {};

  GetLastSevenData() {

    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getterminalactivityhistory',
      StartTime: this.TodayStartTime,
      EndTime: this.TodayEndTime,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Ptsp.V3.TUCPtspAnalytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._LastSevenDaysData = _Response.Result;

          for (let i = 0; i < this._LastSevenDaysData.length; i++) {
            // this.barChartData[0].data[i] = this._LastSevenDaysData[i].Data.Total;
            this.barChartColors[0].backgroundColor.push('#00CCCC')
            this.barChartColors[1].backgroundColor.push('#FFC20A')
            this.barChartColors[2].backgroundColor.push('#F10875')
            this.barChartColors[3].backgroundColor.push('#0168FA')


            this.barChartData[0].data.push(this._LastSevenDaysData[i].Data.Active);
            this.barChartData[1].data.push(this._LastSevenDaysData[i].Data.Idle);
            this.barChartData[2].data.push(this._LastSevenDaysData[i].Data.Dead);
            this.barChartData[3].data.push(this._LastSevenDaysData[i].Data.Inactive);

          }

          this.components.forEach(a => {
            try {
              if (a.chart) a.chart.update();
            } catch (error) {

            }
          });

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  public TerminalsList_Config: OList;
  Alert_Terminal() {
    this.TerminalsList_Config = {
      Id: null,
      Sort: null,

      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchantsTerminals,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
      Title: "Available Terminals",
      StatusType: "default",
      Type: this._HelperService.AppConfig.ListType.All,
      ReferenceId:this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey:this._HelperService.AppConfig.ActiveReferenceKey,
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'LastTransactionDate', this._HelperService.AppConfig.DataType.Text, null, '!='),
      DefaultSortExpression: 'LastTransactionDate  desc',

      TableFields: [
        {
          DisplayName: 'TID',
          SystemName: 'DisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Provider',
          SystemName: 'ProviderDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Merchant',
          SystemName: 'MerchantDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Store',
          SystemName: 'StoreDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Trans',
          SystemName: 'TotalTransaction',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Last Tr',
          SystemName: 'LastTransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: false,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Status',
          SystemName: 'ApplicationStatusId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
      ]
    };
    this.TerminalsList_Config = this._DataHelperService.List_Initialize(
      this.TerminalsList_Config,
    );
    this.TerminalsList_GetData();

  }
  TerminalsList_ToggleOption(event: any, Type: any) {
    this.TerminalsList_Config = this._DataHelperService.List_Operations(
      this.TerminalsList_Config,
      event,
      Type
    );
    if (this.TerminalsList_Config.RefreshData == true) {
      this.TerminalsList_GetData();
    }
  }

  TerminalsList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.TerminalsList_Config
    );
    this.TerminalsList_Config = TConfig;
  }
  TerminalsList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveTerminal,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
      }
    );

    this._HelperService.AppConfig.PTSP.ActiveTerminalReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.PTSP.ActiveTerminalReferenceId = ReferenceData.ReferenceId;

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.PTSPPanel.Terminal.Dashboard,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);
  }


  public _AccountOverview: OAccountOverview =
    {
      ActiveMerchants: 0,
      ActiveMerchantsDiff: 0,
      ActiveTerminals: 0,
      ActiveTerminalsDiff: 0,
      CardRewardPurchaseAmount: 0,
      CardRewardPurchaseAmountDiff: 0,
      CashRewardPurchaseAmount: 0,
      CashRewardPurchaseAmountDiff: 0,
      Merchants: 0,
      PurchaseAmount: 0,
      PurchaseAmountDiff: 0,
      Terminals: 0,
      Transactions: 0,
      TransactionsDiff: 0,
      UnusedTerminals: 0,
      IdleTerminals: 0,
      DeadTerminals: 0,
      TerminalStatus: {},
      TotalTerminals: 0,
      TotalSale: 0,
      TotalTransactions: 0,
      AverageTransactionAmount: 0,
      ActiveTerminalsPerc: 0,
      IdleTerminalsPerc: 0,
      DeadTerminalsPerc: 0,
      UnusedTerminalsPerc: 0,
      Active: 0,
      Total: 0,
      Idle: 0,
      Dead: 0,
      Inactive: 0,
    }


}

export class OAccountOverview {
  public TerminalStatus?: any;
  public TotalTerminals?: number;
  public TotalSale?: number;
  public TotalTransactions?: number;
  public AverageTransactionAmount?: number;
  public ActiveTerminalsPerc?: number;
  public IdleTerminalsPerc?: number;
  public DeadTerminalsPerc?: number;
  public UnusedTerminalsPerc?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public Idle?: number;
  public Total?: number;
  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}

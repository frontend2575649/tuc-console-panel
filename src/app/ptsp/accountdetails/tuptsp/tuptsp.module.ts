import { AgmCoreModule } from '@agm/core';
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Select2Module } from "ng2-select2";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgxPaginationModule } from "ngx-pagination";
import { DynamicRoutesguardGuard } from 'src/app/service/guard/dynamicroutes.guard';
import { MainPipe } from '../../../service/main-pipe.module';
import { TUPtspComponent } from "./tuptsp.component";

const routes: Routes = [
    {
        path: "",
        component: TUPtspComponent,
        canActivate:[DynamicRoutesguardGuard],
        data:{accessName:['viewptsp']},
        children: [
            { path: ":referencekey/:referenceid", data: { permission: "getptsp", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../ptsp/dashboards/tudetailptsp/dashboard.module#TUDashboardModule" },
            { path: "ptspaccount/:referencekey/:referenceid", data: { permission: "getptsp", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../ptsp/dashboards/tudetailptsp/dashboard.module#TUDashboardModule" }, 
            { path: "dashboard/:referencekey/:referenceid", data: { permission: "getptsp", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../ptsp/dashboards/root/dashboard.module#TUDashboardModule" }, 
            { path: "merchants/:referencekey/:referenceid", data: { permission: "getptsp", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../ptsp/accounts/tumerchants/ptsp/tumerchants.module#TUMerchantsModule" }, 
            { path: "terminals/:referencekey/:referenceid", data: { permission: "getptsp", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../ptsp/accounts/tuterminals/ptsp/tuterminals.module#TUTerminalsModule" }, 
            { path: "stores/:referencekey/:referenceid", data: { permission: "getptsp", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../ptsp/accounts/tustores/ptsp/tustores.module#TUStoresModule" }, 
            { path: "activity/:referencekey/:referenceid", data: { permission: "getptsp", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../ptsp/dashboards/tuposactivity/tuposactivity.module#TuposactivityModule" }, 
            { path: 'saleshistory/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.Terminal' }, loadChildren: '../../../ptsp/transactions/ptsp/tusale/tupostransactions.module#TupostransactionsModule' },
            { path: 'commissionhistory/:referencekey/:referenceid', data: { 'permission': 'acquirer', PageName: 'System.Menu.CommissionHistory' }, loadChildren: '../../../ptsp/transactions/tucommisonhistory/tucommisionhistory.module#TuCommisionHistorysModule' },
            { path: 'merchant', data: { 'permission': 'acquirer', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../ptsp/accountdetails/tumerchant/ptsp/tumerchant.module#TUMerchantModule' },
            { path: 'store', data: { 'permission': 'acquirer', PageName: 'System.Menu.Store' }, loadChildren: '../../../ptsp/accountdetails/tustore/tustore.module#TUStoreModule' },
            { path: 'terminal', data: { 'permission': 'acquirer', PageName: 'System.Menu.Terminal' }, loadChildren: '../../../ptsp/accountdetails/tuterminal/ptsp/tuterminal.module#TUTerminalModule' },

        
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUPtspRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUPtspRoutingModule,
        GooglePlaceModule,
        MainPipe,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),

    ],
    declarations: [TUPtspComponent]
})
export class TUPtspModule { }

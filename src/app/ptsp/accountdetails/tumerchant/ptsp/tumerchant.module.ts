import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { TUMerchantComponent } from "./tumerchant.component";
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { MainPipe } from '../../../../service/main-pipe.module';


const routes: Routes = [
    {
        path: "",
        component: TUMerchantComponent,
        children: [
            { path: ":referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../../ptsp/dashboards/merchant/dashboard.module#TUDashboardModule" },
            { path: "dashboard/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../../ptsp/dashboards/merchant/dashboard.module#TUDashboardModule" },
            { path: 'transactions/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../transactions/merchant/tutransactions.module#TutransactionsModule' },
            { path: 'terminals/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../accounts/tuterminals/merchant/tumerchantterminal.module#TumerchantterminalModule' },
            { path: 'products/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../accounts/tuproducts/tuproducts.module#TuproductsModule' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUMerchantRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        GooglePlaceModule,
        TUMerchantRoutingModule,
        MainPipe,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    declarations: [TUMerchantComponent]
})
export class TUMerchantModule { }

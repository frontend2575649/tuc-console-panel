import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { Routes, RouterModule } from '@angular/router';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { AgmCoreModule } from '@agm/core';
import { TupostransactionsComponent } from './tupostransactions.component';
import { DynamicRoutesguardGuard } from 'src/app/service/guard/dynamicroutes.guard';
const routes: Routes = [{ path: "",canActivateChild:[DynamicRoutesguardGuard],data:{accessName:['ptsptransaction']}, component: TupostransactionsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TupostransactionsRoutingModule {}
@NgModule({
  declarations: [TupostransactionsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TupostransactionsRoutingModule,
    Select2Module,
    NgxPaginationModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo',
    }),
    Daterangepicker,
    Ng2FileInputModule,
  ]
})
export class TupostransactionsModule { }

import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Select2OptionData } from 'ng2-select2';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable } from 'rxjs';
import { HelperService } from './helper.service';
import { OList, OListResponse, OResponse, OSelect } from './object.service';
declare var $: any;
declare var moment: any;
@Injectable()
export class DataHelperService {
    constructor(
        public _TranslateService: TranslateService,
        public _HelperService: HelperService) {

    }
    S2_BuildList(S2_Options: OSelect) {
        var Id = [];
        var Text = [];
        var Transport = {
            delay: 200,
            data: (params: any) => {
                return {
                    search: params.term,
                    page: params.page || 1
                };
            },
            transport: (params: any, success: any, failure: any) => {
                var SearchContent = "";
                if (params.data.search != undefined) {
                    SearchContent = params.data.search;
                }
                Id = [];
                Text = [];
                var SearchData = '';
                S2_Options.Fields.forEach(Item => {
                    if (Item.Id == true) {
                        Id.push(Item.SystemName)
                    }
                    if (Item.Text == true) {
                        Text.push(Item.SystemName)
                    }
                    if (Item.SearchCondition != undefined && Item.SearchValue != undefined) {
                        SearchData = this._HelperService.GetSearchConditionStrict(SearchData, Item.SystemName, Item.Type, Item.SearchValue, Item.SearchCondition);
                    }
                    else {
                        SearchData = this._HelperService.GetSearchCondition(SearchData, Item.SystemName, Item.Type, SearchContent);
                    }
                });
                if (S2_Options.SearchCondition != undefined && S2_Options.SearchCondition != null && S2_Options.SearchCondition != '') {
                    if (SearchData != undefined && SearchData != null && SearchData != "") {
                        SearchData = "(" + S2_Options.SearchCondition + ") AND " + "(" + SearchData + ")";
                    }
                    else {
                        SearchData = "(" + S2_Options.SearchCondition + ")";
                    }
                }
                var SortCondition = null;
                if (S2_Options.SortCondition != null) {
                    SortCondition = this._HelperService.GetSortCondition(S2_Options.SortCondition);
                }
                else {
                    SortCondition = this._HelperService.GetSortCondition([Text[0] + ' asc']);
                }
                var S2Response = this.S2_GetResults(params.data.page, SearchData, S2_Options.Location, S2_Options.Task, success, failure, SortCondition, S2_Options.ReferenceId, S2_Options.ReferenceKey, S2_Options.AccountId, S2_Options.AccountKey, S2_Options.SubReferenceId
                    , S2_Options.SubReferenceKey, S2_Options.Type, S2_Options.ProgramId,S2_Options.Limit,S2_Options.RefreshCount);
            },
            processResults: (data: any, params: any) => {
                return this.S2_ProcessData(data, params, Text, Id);
            }
        };
        return Transport;
    }
    S2_ProcessData(data: any, params: any, NameParameters: any[], ValueParameters: any[]) {
        var MoreResults = false;
        var Offset = 0;
        var FData;
        if (data != undefined && data != null) {
            if (params.page == undefined) {
                Offset = data.Limit;
            }
            else {
                Offset = params.page * data.Limit;
            }
            MoreResults = false;
            var FData = $.map(data.Data, function (obj) {
                if (NameParameters.length == 1) {
                    obj.text = obj[NameParameters[0]];
                }
                else {
                    var NameContent = '';
                    for (let index = 0; index < NameParameters.length; index++) {
                        const element = NameParameters[index];
                        if (NameContent == '') {
                            if (obj[element] != undefined) {
                                NameContent += obj[element];
                            }
                        }
                        else {
                            if (obj[element] != undefined) {
                                NameContent += ' - ' + obj[element];
                            }
                        }
                    }
                    obj.text = NameContent;
                }
                if (ValueParameters.length == 1) {
                    obj.id = obj[ValueParameters[0]];
                }
                else {
                    var IdContent = '';
                    for (let index = 0; index < ValueParameters.length; index++) {
                        const element = ValueParameters[index];
                        if (IdContent == '') {
                            IdContent += obj[element];
                        }
                        else {
                            IdContent += ' - ' + obj[element];
                        }
                    }
                    obj.text = NameContent;
                }
                return obj;
            });
            params.page = params.page || 1;
            return {
                results: FData,
                pagination: {
                    more: MoreResults,
                }
            };
        }
        else {
            Offset = 1;
            FData = [];
            return {
                results: FData,
                pagination: {
                    more: 0,
                }
            };
        }
    }
    S2_GetResults(Offset, SearchContent, NetworkApiLocation, NetworkApi, success: any, failure: any, SortCondition?: any, ReferenceId?: any, 
        ReferenceKey?: any, AccountId?: any, AccountKey?: any, SubReferenceId?: any, SubReferenceKey?: any, Type?: any, ProgramId?: number,
        Limit=this._HelperService.AppConfig.DropDownListLimit,RefreshCount=this._HelperService.AppConfig.BooleanFalse) {
        // debugger;
        var PData = {
            Task: NetworkApi,
            Offset: (Offset - 1) * this._HelperService.AppConfig.DropDownListLimit,
            Limit: Limit,
            RefreshCount: RefreshCount,
            SearchCondition: SearchContent,
            SortExpression: SortCondition,
            ReferenceId: ReferenceId,
            ReferenceKey: ReferenceKey,
            AccountId: AccountId,
            AccountKey: AccountKey,
            SubReferenceId: SubReferenceId,
            SubReferenceKey: SubReferenceKey,
            Type: Type,
            ProgramId: ProgramId

        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(NetworkApiLocation, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    var _ResponseData = _Response.Result;
                    success(_ResponseData);
                }
                else {
                    success(_ResponseData);
                }
            },
            _Error => {
                failure();
            });
    }
    List_Initialize(ListOptions: OList) {
        if (ListOptions.Task == undefined) { ListOptions.Task = null; }
        if (ListOptions.ReferenceKey == undefined) { ListOptions.ReferenceKey = null; }
        if (ListOptions.Type == undefined) { ListOptions.Type = null; }
        if (ListOptions.Location == undefined) { ListOptions.Location = null; }
        if (ListOptions.Title == undefined) { ListOptions.Title = 'List'; }
        if (ListOptions.TableFields == undefined) { ListOptions.TableFields = []; }
        if (ListOptions.StatusOptions == undefined) { ListOptions.StatusOptions = []; }
        if (ListOptions.TitleResourceId == undefined) { ListOptions.TitleResourceId = null; }
        if (ListOptions.RefreshData == undefined) { ListOptions.RefreshData = false; }
        if (ListOptions.IsDownload == undefined) { ListOptions.IsDownload = false; }
        if (ListOptions.ActivePage == undefined) { ListOptions.ActivePage = 1; }
        if (ListOptions.PageRecordLimit == undefined) { ListOptions.PageRecordLimit = this._HelperService.AppConfig.ListRecordLimit[0]; }
        if (ListOptions.TotalRecords == undefined) { ListOptions.TotalRecords = 0; }
        if (ListOptions.ShowingStart == undefined) { ListOptions.ShowingStart = 0; }
        if (ListOptions.ShowingEnd == undefined) { ListOptions.ShowingEnd = 0; }
        if (ListOptions.StartTime == undefined) { ListOptions.StartTime = null; }
        if (ListOptions.EndTime == undefined) { ListOptions.EndTime = null; }
        if (ListOptions.ReferenceId == undefined) { ListOptions.ReferenceId = 0; }
        if (ListOptions.SubReferenceId == undefined) { ListOptions.SubReferenceId = 0; }
        if (ListOptions.SearchParameter == undefined) { ListOptions.SearchParameter = null; }
        if (ListOptions.SearchCondition == undefined) { ListOptions.SearchCondition = null; }
        if (ListOptions.SearchBaseCondition == undefined) { ListOptions.SearchBaseCondition = null; }
        if (ListOptions.SearchBaseConditions == undefined) { ListOptions.SearchBaseConditions = []; }
        if (ListOptions.Data == undefined) { ListOptions.Data = []; }
        if (ListOptions.StatusType == undefined) { ListOptions.StatusType = 'default'; }
        if (ListOptions.PlanOptions == undefined) { ListOptions.PlanOptions = 'plan'; }
        if (ListOptions.Status == undefined) { ListOptions.Status = 0; }
        if (ListOptions.VisibleHeaders == undefined) { ListOptions.VisibleHeaders = []; }
        if (ListOptions.Sort == undefined) {
            ListOptions.Sort =
            {
                SortDefaultName: null,
                SortDefaultColumn: 'CreateDate',
                SortName: null,
                SortColumn: null,
                SortOrder: 'desc',
                SortOptions: [],
            }
        }
        ListOptions.Sort =
        {
            SortDefaultName: ListOptions.Sort.SortDefaultName,
            SortDefaultColumn: ListOptions.Sort.SortDefaultColumn,
            SortDefaultOrder: ListOptions.Sort.SortDefaultOrder,
            SortName: ListOptions.Sort.SortName,
            SortColumn: ListOptions.Sort.SortColumn,
            SortOrder: ListOptions.Sort.SortOrder,
            SortOptions: [],
        }
        if (ListOptions.Sort.SortDefaultOrder == undefined || ListOptions.Sort.SortDefaultOrder == null) {
            ListOptions.Sort.SortDefaultOrder = 'desc';
        }

        if (ListOptions.Sort.SortName == undefined || ListOptions.Sort.SortName == null || ListOptions.Sort.SortName == '') {
            ListOptions.Sort.SortName = ListOptions.Sort.SortDefaultName;
        }
        if (ListOptions.Sort.SortColumn == undefined || ListOptions.Sort.SortColumn == null || ListOptions.Sort.SortColumn == '') {
            ListOptions.Sort.SortColumn = ListOptions.Sort.SortDefaultColumn;
        }
        if (ListOptions.Sort.SortOrder == undefined || ListOptions.Sort.SortOrder == null || ListOptions.Sort.SortOrder == '') {
            ListOptions.Sort.SortOrder = ListOptions.Sort.SortDefaultOrder;
        }
        ListOptions.StatusOptions = this._HelperService.AppConfig.StatusList[ListOptions.StatusType];
        ListOptions.PlanOptions = this._HelperService.AppConfig.StatusList[ListOptions.PlanOptions];
        this.S2_Status_Option = {
            multiple: false,
            placeholder: "Filter by status"
        };
        this.S3_Status_Option = {
            multiple: false,
            placeholder: "Select Status"
        };
        this.S2_Plan_Option = {
            multiple: false,
            placeholder: "Filter by status"
        };
        this.S2_SavedFilters_Option = {
            multiple: false,
            placeholder: "Filter by status"
        };

        this.S2_NewSavedFilters_Option = {
            multiple: false,
            placeholder: "default view"
        };

        this.S2_Sort_Option = {
            multiple: false,
            placeholder: "Sort results"
        };
        if (ListOptions.TitleResourceId != undefined && ListOptions.TitleResourceId != null && ListOptions.TitleResourceId != '') {
            this._TranslateService.get(ListOptions.TitleResourceId).subscribe((_Message: string) => {
                ListOptions.Title = _Message;
            });
        }
        ListOptions.TableFields.forEach(element => {
            if (element.ResourceId != undefined && element.ResourceId != null && element.ResourceId != '') {
                this._TranslateService.get(element.ResourceId).subscribe((_Message: string) => {
                    element.DisplayName = _Message;
                });
            }
            ListOptions.TableFields.push(element);
            if (element.Sort == true) {
                var FilterItem =
                {
                    Name: element.DisplayName,
                    SystemName: element.SystemName,
                    SystemActive: false
                }
                ListOptions.Sort.SortOptions.push(FilterItem);
            }
            if (element.Show == true) {
                ListOptions.VisibleHeaders.push(element);
                var Option =
                {
                    id: element.SystemName,
                    text: element.DisplayName,
                }
            }
        });
        return ListOptions;
    }

    List_InitializeRewPerc(ListOptions: OList) {
        if (ListOptions.Task == undefined) { ListOptions.Task = null; }
        if (ListOptions.ReferenceKey == undefined) { ListOptions.ReferenceKey = null; }
        if (ListOptions.Type == undefined) { ListOptions.Type = null; }
        if (ListOptions.Location == undefined) { ListOptions.Location = null; }
        if (ListOptions.Title == undefined) { ListOptions.Title = 'List'; }
        if (ListOptions.TableFields == undefined) { ListOptions.TableFields = []; }
        if (ListOptions.StatusOptions == undefined) { ListOptions.StatusOptions = []; }
        if (ListOptions.TitleResourceId == undefined) { ListOptions.TitleResourceId = null; }
        if (ListOptions.RefreshData == undefined) { ListOptions.RefreshData = false; }
        if (ListOptions.IsDownload == undefined) { ListOptions.IsDownload = false; }
        if (ListOptions.ActivePage == undefined) { ListOptions.ActivePage = 1; }
        if (ListOptions.PageRecordLimit == undefined) { ListOptions.PageRecordLimit = this._HelperService.AppConfig.ListRecordLimit[0]; }
        if (ListOptions.TotalRecords == undefined) { ListOptions.TotalRecords = 0; }
        if (ListOptions.ShowingStart == undefined) { ListOptions.ShowingStart = 0; }
        if (ListOptions.ShowingEnd == undefined) { ListOptions.ShowingEnd = 0; }
        if (ListOptions.StartTime == undefined) { ListOptions.StartTime = null; }
        if (ListOptions.EndTime == undefined) { ListOptions.EndTime = null; }
        if (ListOptions.ReferenceId == undefined) { ListOptions.ReferenceId = 0; }
        if (ListOptions.SubReferenceId == undefined) { ListOptions.SubReferenceId = 0; }
        if (ListOptions.SearchParameter == undefined) { ListOptions.SearchParameter = null; }
        if (ListOptions.SearchCondition == undefined) { ListOptions.SearchCondition = null; }
        if (ListOptions.SearchBaseCondition == undefined) { ListOptions.SearchBaseCondition = null; }
        if (ListOptions.SearchBaseConditions == undefined) { ListOptions.SearchBaseConditions = []; }
        if (ListOptions.Data == undefined) { ListOptions.Data = []; }
        if (ListOptions.StatusType == undefined) { ListOptions.StatusType = 'default'; }
        if (ListOptions.Status == undefined) { ListOptions.Status = 0; }
        if (ListOptions.VisibleHeaders == undefined) { ListOptions.VisibleHeaders = []; }
        if (ListOptions.Sort == undefined) {
            ListOptions.Sort =
            {
                SortDefaultName: null,
                SortDefaultColumn: 'StartDate',
                SortName: null,
                SortColumn: null,
                SortOrder: 'desc',
                SortOptions: [],
            }
        }
        ListOptions.Sort =
        {
            SortDefaultName: ListOptions.Sort.SortDefaultName,
            SortDefaultColumn: ListOptions.Sort.SortDefaultColumn,
            SortDefaultOrder: ListOptions.Sort.SortDefaultOrder,
            SortName: ListOptions.Sort.SortName,
            SortColumn: ListOptions.Sort.SortColumn,
            SortOrder: ListOptions.Sort.SortOrder,
            SortOptions: [],
        }
        if (ListOptions.Sort.SortDefaultOrder == undefined || ListOptions.Sort.SortDefaultOrder == null) {
            ListOptions.Sort.SortDefaultOrder = 'desc';
        }

        if (ListOptions.Sort.SortName == undefined || ListOptions.Sort.SortName == null || ListOptions.Sort.SortName == '') {
            ListOptions.Sort.SortName = ListOptions.Sort.SortDefaultName;
        }
        if (ListOptions.Sort.SortColumn == undefined || ListOptions.Sort.SortColumn == null || ListOptions.Sort.SortColumn == '') {
            ListOptions.Sort.SortColumn = ListOptions.Sort.SortDefaultColumn;
        }
        if (ListOptions.Sort.SortOrder == undefined || ListOptions.Sort.SortOrder == null || ListOptions.Sort.SortOrder == '') {
            ListOptions.Sort.SortOrder = ListOptions.Sort.SortDefaultOrder;
        }
        ListOptions.StatusOptions = this._HelperService.AppConfig.StatusList[ListOptions.StatusType];
        this.S2_Status_Option = {
            multiple: false,
            placeholder: "Filter by status"
        };
        this.S2_Plan_Option = {
            multiple: false,
            placeholder: "Filter by status"
        };
        this.S2_SavedFilters_Option = {
            multiple: false,
            placeholder: "Filter by status"
        };

        this.S2_NewSavedFilters_Option = {
            multiple: false,
            placeholder: "default"
        };

        this.S2_Sort_Option = {
            multiple: false,
            placeholder: "Sort results"
        };
        if (ListOptions.TitleResourceId != undefined && ListOptions.TitleResourceId != null && ListOptions.TitleResourceId != '') {
            this._TranslateService.get(ListOptions.TitleResourceId).subscribe((_Message: string) => {
                ListOptions.Title = _Message;
            });
        }
        ListOptions.TableFields.forEach(element => {
            if (element.ResourceId != undefined && element.ResourceId != null && element.ResourceId != '') {
                this._TranslateService.get(element.ResourceId).subscribe((_Message: string) => {
                    element.DisplayName = _Message;
                });
            }
            ListOptions.TableFields.push(element);
            if (element.Sort == true) {
                var FilterItem =
                {
                    Name: element.DisplayName,
                    SystemName: element.SystemName,
                    SystemActive: false
                }
                ListOptions.Sort.SortOptions.push(FilterItem);
            }
            if (element.Show == true) {
                ListOptions.VisibleHeaders.push(element);
                var Option =
                {
                    id: element.SystemName,
                    text: element.DisplayName,
                }
            }
        });
        return ListOptions;
    }

    List_Initialize_ForStores(ListOptions: OList) {
        if (ListOptions.Task == undefined) { ListOptions.Task = null; }
        if (ListOptions.ReferenceKey == undefined) { ListOptions.ReferenceKey = null; }
        if (ListOptions.Type == undefined) { ListOptions.Type = null; }
        if (ListOptions.Location == undefined) { ListOptions.Location = null; }
        if (ListOptions.Title == undefined) { ListOptions.Title = 'List'; }
        if (ListOptions.TableFields == undefined) { ListOptions.TableFields = []; }
        if (ListOptions.StatusOptions == undefined) { ListOptions.StatusOptions = []; }
        if (ListOptions.TitleResourceId == undefined) { ListOptions.TitleResourceId = null; }
        if (ListOptions.RefreshData == undefined) { ListOptions.RefreshData = false; }
        if (ListOptions.IsDownload == undefined) { ListOptions.IsDownload = false; }
        if (ListOptions.ActivePage == undefined) { ListOptions.ActivePage = 1; }
        if (ListOptions.PageRecordLimit == undefined) { ListOptions.PageRecordLimit = this._HelperService.AppConfig.ListRecordLimit[0]; }
        if (ListOptions.TotalRecords == undefined) { ListOptions.TotalRecords = 0; }
        if (ListOptions.ShowingStart == undefined) { ListOptions.ShowingStart = 0; }
        if (ListOptions.ShowingEnd == undefined) { ListOptions.ShowingEnd = 0; }
        if (ListOptions.StartTime == undefined) { ListOptions.StartTime = null; }
        if (ListOptions.EndTime == undefined) { ListOptions.EndTime = null; }
        if (ListOptions.ReferenceId == undefined) { ListOptions.ReferenceId = 0; }
        if (ListOptions.SubReferenceId == undefined) { ListOptions.SubReferenceId = 0; }
        if (ListOptions.SubReferenceKey == undefined) { ListOptions.SubReferenceKey = null; }
        if (ListOptions.SearchParameter == undefined) { ListOptions.SearchParameter = null; }
        if (ListOptions.SearchCondition == undefined) { ListOptions.SearchCondition = null; }
        if (ListOptions.SearchBaseCondition == undefined) { ListOptions.SearchBaseCondition = null; }
        if (ListOptions.SearchBaseConditions == undefined) { ListOptions.SearchBaseConditions = []; }
        if (ListOptions.Data == undefined) { ListOptions.Data = []; }
        if (ListOptions.StatusType == undefined) { ListOptions.StatusType = 'default'; }
        if (ListOptions.Status == undefined) { ListOptions.Status = 0; }
        if (ListOptions.VisibleHeaders == undefined) { ListOptions.VisibleHeaders = []; }
        if (ListOptions.Sort == undefined) {
            ListOptions.Sort =
            {
                SortDefaultName: null,
                SortDefaultColumn: 'CreateDate',
                SortName: null,
                SortColumn: null,
                SortOrder: 'desc',
                SortOptions: [],
            }
        }
        ListOptions.Sort =
        {
            SortDefaultName: ListOptions.Sort.SortDefaultName,
            SortDefaultColumn: ListOptions.Sort.SortDefaultColumn,
            SortDefaultOrder: ListOptions.Sort.SortDefaultOrder,
            SortName: ListOptions.Sort.SortName,
            SortColumn: ListOptions.Sort.SortColumn,
            SortOrder: ListOptions.Sort.SortOrder,
            SortOptions: [],
        }
        if (ListOptions.Sort.SortDefaultOrder == undefined || ListOptions.Sort.SortDefaultOrder == null) {
            ListOptions.Sort.SortDefaultOrder = 'desc';
        }

        if (ListOptions.Sort.SortName == undefined || ListOptions.Sort.SortName == null || ListOptions.Sort.SortName == '') {
            ListOptions.Sort.SortName = ListOptions.Sort.SortDefaultName;
        }
        if (ListOptions.Sort.SortColumn == undefined || ListOptions.Sort.SortColumn == null || ListOptions.Sort.SortColumn == '') {
            ListOptions.Sort.SortColumn = ListOptions.Sort.SortDefaultColumn;
        }
        if (ListOptions.Sort.SortOrder == undefined || ListOptions.Sort.SortOrder == null || ListOptions.Sort.SortOrder == '') {
            ListOptions.Sort.SortOrder = ListOptions.Sort.SortDefaultOrder;
        }
        ListOptions.StatusOptions = this._HelperService.AppConfig.StatusList[ListOptions.StatusType];
        this.S2_Status_Option = {
            multiple: false,
            placeholder: "Filter by status"
        };
        this.S2_Plan_Option = {
            multiple: false,
            placeholder: "Filter by status"
        };
        this.S2_SavedFilters_Option = {
            multiple: false,
            placeholder: "Filter by status"
        };

        this.S2_NewSavedFilters_Option = {
            multiple: false,
            placeholder: "default"
        };

        this.S2_Sort_Option = {
            multiple: false,
            placeholder: "Sort results"
        };
        if (ListOptions.TitleResourceId != undefined && ListOptions.TitleResourceId != null && ListOptions.TitleResourceId != '') {
            this._TranslateService.get(ListOptions.TitleResourceId).subscribe((_Message: string) => {
                ListOptions.Title = _Message;
            });
        }
        ListOptions.TableFields.forEach(element => {
            if (element.ResourceId != undefined && element.ResourceId != null && element.ResourceId != '') {
                this._TranslateService.get(element.ResourceId).subscribe((_Message: string) => {
                    element.DisplayName = _Message;
                });
            }
            ListOptions.TableFields.push(element);
            if (element.Sort == true) {
                var FilterItem =
                {
                    Name: element.DisplayName,
                    SystemName: element.SystemName,
                }
                ListOptions.Sort.SortOptions.push(FilterItem);
            }
            if (element.Show == true) {
                ListOptions.VisibleHeaders.push(element);
                var Option =
                {
                    id: element.SystemName,
                    text: element.DisplayName,
                }
            }
        });
        return ListOptions;
    }

    List_Operations(ListOptions: OList, event: any, Type: any) {
        var ResetOffset = false;
        ListOptions.RefreshData = false;
        ListOptions.RefreshCount = false;
        if (Type == this._HelperService.AppConfig.ListToggleOption.Limit) {
            ListOptions.PageRecordLimit = event;
            ResetOffset = true;
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Search) {
            if (event != null) {
                ListOptions.SearchParameter = event.target.value;
            }
            ResetOffset = true;
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Date) {
            if (ListOptions.StartDate == undefined && ListOptions.EndDate == undefined) {
                ListOptions.StartTime = event.start;
                ListOptions.EndTime = event.end;
            }
            ResetOffset = true;
            ListOptions.RefreshData = false;
            ListOptions.RefreshCount = false;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.DateExpiry) {
            if (ListOptions.ExpiryStartDate == undefined && ListOptions.ExpiryEndDate == undefined) {
                ListOptions.ExpiryStartTime = event.start;
                ListOptions.ExpiryEndTime = event.end;
            }
            ResetOffset = true;
            ListOptions.RefreshData = false;
            ListOptions.RefreshCount = false;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.DateExpiry) {
            if (ListOptions.StartDate == undefined && ListOptions.EndDate == undefined) {
                ListOptions.StartTime = event.start;
                ListOptions.EndTime = event.end;
            }
            ResetOffset = true;
            ListOptions.RefreshData = false;
            ListOptions.RefreshCount = false;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Status) {
            if ((event.data.length > 0) && event.data[0].code != undefined
                && (event.data[0].code.includes('default') || event.data[0].code.includes('dealcode'))
                || event.data[0].code.includes('paymentstatus') || event.data[0].code.includes('transaction') || event.data[0].code.includes('cashoutstatus') || event.data[0].code.includes('product') || event.data[0].code.includes('campaign') || event.data[0].code.includes('orderstatus')) {
                ListOptions.Status = event.data[0].code;

            } else {
                ListOptions.Status = event.value;
            }

            ListOptions.RefreshData = false;
            ListOptions.RefreshCount = false;
            ResetOffset = false;
        }

        else if (Type == this._HelperService.AppConfig.ListToggleOption.Sort) {
            ListOptions.Sort.SortColumn = event.SystemName;
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
            ResetOffset = true;
            $('#' + ListOptions.Id + "_sdropdown").dropdown('toggle');
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.SortOrder) {
            if (ListOptions.Sort.SortOrder == 'asc') {
                ListOptions.Sort.SortOrder = 'desc';
            }
            else {
                ListOptions.Sort.SortOrder = 'asc';
            }
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
            ResetOffset = true;
            $('#' + ListOptions.Id + "_sdropdown").dropdown('toggle');
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.SortApply) {
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
            ResetOffset = true;
            $('#' + ListOptions.Id + "_sdropdown").dropdown('toggle');
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.SortReset) {
            ListOptions.Sort.SortOrder = 'desc';
            ListOptions.Sort.SortColumn = null;
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
            ResetOffset = true;
            $('#' + ListOptions.Id + "_sdropdown").dropdown('toggle');
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Page) {
            ListOptions.ActivePage = event;
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = false;

        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.MultiSelect) {
            ListOptions.ActivePage = event;
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;

        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Refresh) {
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Refresh) {
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.ResetOffset) {
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
            ResetOffset = true;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Csv) {
            this.List_DownloadCsv(ListOptions);
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.ApplyFilter) {
            ResetOffset = true;
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
            $('#' + ListOptions.Id + "_fdropdown").dropdown('toggle');
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.CancelFilter) {
            $('#' + ListOptions.Id + "_fdropdown").dropdown('toggle');
        }
        if (ResetOffset == true) {
            ListOptions.ActivePage = 1;
            ListOptions.TotalRecords = 0;
            ListOptions.ShowingStart = 0;
            ListOptions.ShowingEnd = 0;
        }
        return ListOptions;
    }
    List_GetSearchCondition(ListOptions: OList, noStatusSearch?: Boolean, searchWithStatusId?: Boolean) {
        var DateSearchFieldName = null;
        ListOptions.SearchCondition = '';
        if (ListOptions.TableFields != undefined) {
            ListOptions.TableFields.forEach(element => {
                if (element.IsDateSearchField == true) {
                    DateSearchFieldName = element.SystemName;
                }
                if (element.Search == true) {
                    ListOptions.SearchCondition = this._HelperService.GetSearchCondition(ListOptions.SearchCondition, element.SystemName, element.DataType, ListOptions.SearchParameter);
                }
            });
        }
        if (!noStatusSearch && ListOptions.Status != 0) {
            if (searchWithStatusId) {
                ListOptions.SearchCondition = this._HelperService.GetSearchConditionStrict(ListOptions.SearchCondition, 'StatusId', 'number', ListOptions.Status, "=");
            } else {
                ListOptions.SearchCondition = this._HelperService.GetSearchConditionStrict(ListOptions.SearchCondition, 'StatusCode', 'text', ListOptions.Status, "=");
            }
        }
        if (DateSearchFieldName != null) {
            ListOptions.SearchCondition = this._HelperService.GetDateCondition(ListOptions.SearchCondition, DateSearchFieldName, ListOptions.StartTime, ListOptions.EndTime);
        }
        if (ListOptions.SearchBaseCondition != undefined && ListOptions.SearchBaseCondition != null && ListOptions.SearchBaseCondition != '') {
            if (ListOptions.SearchCondition != undefined && ListOptions.SearchCondition != null && ListOptions.SearchCondition != '') {
                ListOptions.SearchCondition = '(' + ListOptions.SearchCondition + ') AND (' + ListOptions.SearchBaseCondition + ')';
            }
            else {
                ListOptions.SearchCondition = ListOptions.SearchBaseCondition;
            }
        }
        if (ListOptions.SearchBaseConditions != undefined && ListOptions.SearchBaseConditions.length > 0) {
            ListOptions.SearchBaseConditions.forEach(element => {
                if (ListOptions.SearchCondition != undefined && ListOptions.SearchCondition != null && ListOptions.SearchCondition != '') {
                    ListOptions.SearchCondition = '(' + ListOptions.SearchCondition + ') AND (' + element + ')';
                }
                else {
                    ListOptions.SearchCondition = element;
                }
            });

        }
        return ListOptions;
    }
    List_OperationsForTransCounts(Options: any, event: any, Type: any) {
        if (Type == this._HelperService.AppConfig.ListToggleOption.Search) {
            if (event != null) {
                Options.SearchParameter = event.target.value;
            }
            Options.RefreshData = true;
            Options.RefreshCount = true;
        }

        else if (Type == this._HelperService.AppConfig.ListToggleOption.Date) {
            if (Options.StartTime == undefined && Options.EndDate == undefined) {
                Options.StartTime = event.start;
                Options.EndTime = event.end;
            }

            Options.RefreshData = false;
            Options.RefreshCount = false;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.DateExpiry) {
            if (Options.ExpiryStartTime == undefined && Options.ExpiryEndDate == undefined) {
                Options.ExpiryStartTime = event.start;
                Options.ExpiryEndTime = event.end;
            }

            Options.RefreshData = false;
            Options.RefreshCount = false;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Status) {
            if ((event.data.length > 0) && event.data[0].code != undefined
                && event.data[0].code.includes('default')) {
                Options.Status = event.data[0].code;
            } else {
                Options.Status = event.value;
            }

            Options.RefreshData = false;
            Options.RefreshCount = false;

        }

        else if (Type == this._HelperService.AppConfig.ListToggleOption.MultiSelect) {
            Options.ActivePage = event;
            Options.RefreshData = true;
            Options.RefreshCount = true;

        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Refresh) {
            Options.RefreshData = true;
            Options.RefreshCount = true;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.ResetOffset) {
            Options.RefreshData = true;
            Options.RefreshCount = true;
        }

        else if (Type == this._HelperService.AppConfig.ListToggleOption.ApplyFilter) {

            Options.RefreshData = true;
            Options.RefreshCount = true;
        }

        else if (Type == this._HelperService.AppConfig.ListToggleOption.CancelFilter) {

        }

        return Options;
    }
    List_GetSearchConditionTur(ListOptions: OList, noStatusSearch?: Boolean) {
        var DateSearchFieldName = null;
        ListOptions.SearchCondition = '';
        if (ListOptions.TableFields != undefined) {
            ListOptions.TableFields.forEach(element => {
                if (element.IsDateSearchField == true) {
                    DateSearchFieldName = element.SystemName;
                }
                if (element.Search == true) {
                    ListOptions.SearchCondition = this._HelperService.GetSearchCondition(ListOptions.SearchCondition, element.SystemName, element.DataType, ListOptions.SearchParameter);
                }
            });
        }
        if (!noStatusSearch && ListOptions.Status != 0) {
            ListOptions.SearchCondition = this._HelperService.GetSearchConditionStrict(ListOptions.SearchCondition, 'StatusId', this._HelperService.AppConfig.DataType.Number, ListOptions.Status, "=");
        }
        if (DateSearchFieldName != null) {
            ListOptions.SearchCondition = this._HelperService.GetDateCondition(ListOptions.SearchCondition, DateSearchFieldName, ListOptions.StartTime, ListOptions.EndTime);
        }
        // if (ListOptions.ExpiryStartDate != null && ListOptions.ExpiryEndDate != null) {
        //     ListOptions.SearchCondition = this._HelperService.GetDateCondition(ListOptions.SearchCondition, DateSearchFieldName, ListOptions.ExpiryStartTime, ListOptions.ExpiryEndTime);
        // }
        if (ListOptions.SearchBaseCondition != undefined && ListOptions.SearchBaseCondition != null && ListOptions.SearchBaseCondition != '') {
            if (ListOptions.SearchCondition != undefined && ListOptions.SearchCondition != null && ListOptions.SearchCondition != '') {
                ListOptions.SearchCondition = '(' + ListOptions.SearchCondition + ') AND (' + ListOptions.SearchBaseCondition + ')';
            }
            else {
                ListOptions.SearchCondition = ListOptions.SearchBaseCondition;
            }
        }
        if (ListOptions.SearchBaseConditions != undefined && ListOptions.SearchBaseConditions.length > 0) {
            ListOptions.SearchBaseConditions.forEach(element => {
                if (ListOptions.SearchCondition != undefined && ListOptions.SearchCondition != null && ListOptions.SearchCondition != '') {
                    ListOptions.SearchCondition = '(' + ListOptions.SearchCondition + ') AND (' + element + ')';
                }
                else {
                    ListOptions.SearchCondition = element;
                }
            });

        }
        return ListOptions;
    }
    List_FormatResponse(ListOptions: OList, ResponseData: OListResponse, Show?: any) {
        ListOptions.Data = [];
        ListOptions.ShowingStart = ((ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit) + 1;
        ListOptions.ShowingEnd = (ListOptions.ShowingStart + ListOptions.PageRecordLimit) - 1;
        ListOptions.TotalRecords = ResponseData.TotalRecords;
        ListOptions.TotalCustomer = ResponseData.Total;
        var Result: any[] = ResponseData.Data;
        Result = this._HelperService.HCXGetDateComponentList(Result);
        if (Show != undefined && Show != null) {
            if (Result.length == 0) {
                Show.ListShow = 'none';
                Show.IconShow = 'block';
            } else {
                Show.ListShow = 'block';
                Show.IconShow = 'none';
            }
        }


        for (var index = 0; index < Result.length; index++) {
            var element = Result[index];
            if (Result[index].ProcessingTime != undefined) {
                Result[index].ProcessingTime = (Result[index].ProcessingTime / 1000);
            }

            Result[index].StartTime = this._HelperService.GetTimeS(element.StartDate);
            Result[index].EndTime = this._HelperService.GetTimeS(element.EndDate);
            Result[index].SendDateD = this._HelperService.GetDateS(element.SendDate);
            Result[index].StartDateD = this._HelperService.GetDateS(element.StartDate);
            Result[index].StartDateT = this._HelperService.GetTimeS(element.StartDate);
            Result[index].EndDateD = this._HelperService.GetDateS(element.EndDate);
            Result[index].EndDateT = this._HelperService.GetTimeS(element.EndDate);
            Result[index].SendDateT = this._HelperService.GetTimeS(element.SendDate);
            Result[index].StartDateS = this._HelperService.GetDateS(element.StartDate);
            Result[index].EndDateS = this._HelperService.GetDateS(element.EndDate);
            Result[index].LastUseDateD = this._HelperService.GetDateS(element.LastUseDate);
            Result[index].LastUseDateT = this._HelperService.GetTimeS(element.LastUseDate);
            Result[index].LastUseDate = this._HelperService.GetDateTimeS(element.LastUseDate);
            Result[index].ActivityDate = this._HelperService.GetDateTimeS(element.ActivityDate);
            Result[index].LastTransactionDateDiff = this._HelperService.GetTimeDifferenceS(element.LastTransactionDate, moment());
            Result[index].TransactionDateDiff = this._HelperService.GetTimeDifferenceS(element.TransactionDate, moment());
            Result[index].TransactionDateD = this._HelperService.GetDateS(element.TransactionDate);
            Result[index].TransactionDateT = this._HelperService.GetTimeS(element.TransactionDate);
            Result[index].LastActivityDateD = this._HelperService.GetDateS(element.LastActivityDate);
            Result[index].LastActivityDateT = this._HelperService.GetTimeS(element.LastActivityDate);
            Result[index].TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
            Result[index].VerifyDate = this._HelperService.GetDateTimeS(element.VerifyDate);
            Result[index].InvoiceDate = this._HelperService.GetDateS(element.InvoiceDate);
            Result[index].RequestTime = this._HelperService.GetDateTimeS(element.RequestTime);
            Result[index].ResponseTime = this._HelperService.GetDateTimeS(element.ResponseTime);
            Result[index].CreateDateD = this._HelperService.GetDateS(element.CreateDate);
            Result[index].CreateDateT = this._HelperService.GetTimeS(element.CreateDate);
            Result[index].CreateDate = this._HelperService.GetDateTimeS(element.CreateDate);
            Result[index].ExpiryDateD = this._HelperService.GetDateS(element.ExpiryDate);
            Result[index].ExpiryDateT = this._HelperService.GetTimeS(element.ExpiryDate);
            Result[index].ExpiryDate = this._HelperService.GetDateTimeS(element.ExpiryDate);
            Result[index].IssueDateD = this._HelperService.GetDateS(element.IssueDate);
            Result[index].IssueDateT = this._HelperService.GetTimeS(element.IssueDate);
            Result[index].IssueDate = this._HelperService.GetDateTimeS(element.IssueDate);
            Result[index].LoginDate_D = this._HelperService.GetDateS(element.LoginDate);
            Result[index].LoginDate_T = this._HelperService.GetTimeS(element.LoginDate);
            Result[index].LogoutDate_D = this._HelperService.GetDateS(element.LogoutDate);
            Result[index].LogoutDate_T = this._HelperService.GetTimeS(element.LogoutDate);
            Result[index].ModifyDate_D = this._HelperService.GetDateS(element.ModifyDate);
            Result[index].ModifyDate_T = this._HelperService.GetTimeS(element.ModifyDate);


            Result[index].RequestTimeD = this._HelperService.GetDateS(element.RequestTime);
            Result[index].RequestTimeT = this._HelperService.GetTimeS(element.RequestTime);
            Result[index].ResponseTimeD = this._HelperService.GetDateS(element.ResponseTime);
            Result[index].ResponseTimeT = this._HelperService.GetTimeS(element.ResponseTime);
            Result[index].DeliveryDateD = this._HelperService.GetDateS(element.DeliveryDate);
            Result[index].DeliveryDateT = this._HelperService.GetTimeS(element.DeliveryDate);
            Result[index].DateSetD = this._HelperService.GetDateS(element.DateSet);
            Result[index].DateSetT = this._HelperService.GetTimeS(element.DateSet);





            // 
            // this.Result[index].LastLoginDateT = this._HelperService.GetTimeS(element.LastLoginDate);
            // this.Result[index].LastLoginDateD = this._HelperService.GetDateTimeS(element.LastLoginDate);
            // this.Result[index].LastLoginDate = this._HelperService.GetDateTimeS(element.LastLoginDate);
            //

            Result[index].Date = this._HelperService.GetDateS(element.Date);
            //  Result[index].ModifyDate = this._HelperService.GetDateTimeS(element.ModifyDate);
            //   Result[index].LoginDate = this._HelperService.GetDateTimeS(element.LoginDate);
            if (Result[index].LastLoginDate != "0001-01-01T00:00:00") {
                Result[index].LastLoginDate = this._HelperService.GetDateTimeS(element.LastLoginDate);
            }
            else {
                Result[index].LastLoginDate = null;
            }
            if (Result[index].LastTransactionDate != "0001-01-01T00:00:00") {
                Result[index].LastTransactionDate = this._HelperService.GetDateTimeS(element.LastTransactionDate);
            }
            else {
                Result[index].LastTransactionDate = "";
            }
            // Result[index].LogoutDate = this._HelperService.GetDateTimeS(element.LogoutDate);

            if (element.StatusId != undefined && element.StatusId != null) {
                Result[index].StatusI = this._HelperService.GetStatusIcon(element.StatusId);
                Result[index].StatusBadge = this._HelperService.GetStatusBadge(element.StatusId);
                Result[index].StatusC = this._HelperService.GetStatusColor(element.StatusId);
            } else {
                Result[index].StatusI = this._HelperService.GetStatusIcon(element.StatusCode);
                Result[index].StatusBadge = this._HelperService.GetStatusBadge(element.StatusCode);
                Result[index].StatusC = this._HelperService.GetStatusColor(element.StatusCode);
            }

            if (element.ApplicationStatusId != undefined) {
                Result[index].ApplStatusBadge = this._HelperService.GetStatusBadge(element.ApplicationStatusId);
            }

        }
        ListOptions.Data = Result;
        var ResultI = this._HelperService.HCXGetDateComponentList(Result);
        ListOptions.Data = ResultI;
        return ListOptions;
    }

    Result: any = [];
    List_FormatResponse1(ListOptions: OList, ResponseData: OListResponse, Show?: any) {
        ListOptions.Data = [];
        ListOptions.ShowingStart = ((ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit) + 1;
        ListOptions.ShowingEnd = (ListOptions.ShowingStart + ListOptions.PageRecordLimit) - 1;
        ListOptions.TotalRecords = ResponseData.TotalRecords;
        ListOptions.TotalCustomer = ResponseData.Total;

        this.Result = ResponseData;

        if (Show != undefined && Show != null) {
            if (this.Result.length == 0) {
                Show.ListShow = 'none';
                Show.IconShow = 'block';
            } else {
                Show.ListShow = 'block';
                Show.IconShow = 'none';
            }
        }
        this.Result = this._HelperService.HCXGetDateComponentList(this.Result)
        for (var index = 0; index < this.Result.length; index++) {
            var element = this.Result[index];
            if (this.Result[index].ProcessingTime != undefined) {
                this.Result[index].ProcessingTime = (this.Result[index].ProcessingTime / 1000);
            }

            this.Result[index].StartTime = this._HelperService.GetTimeS(element.StartDate);
            this.Result[index].EndTime = this._HelperService.GetTimeS(element.EndDate);
            this.Result[index].SendDateD = this._HelperService.GetDateS(element.SendDate);
            this.Result[index].StartDateD = this._HelperService.GetDateS(element.StartDate);
            this.Result[index].StartDateT = this._HelperService.GetTimeS(element.StartDate);
            this.Result[index].EndDateD = this._HelperService.GetDateS(element.EndDate);
            this.Result[index].EndDateT = this._HelperService.GetTimeS(element.EndDate);
            this.Result[index].SendDateT = this._HelperService.GetTimeS(element.SendDate);
            this.Result[index].StartDateS = this._HelperService.GetDateS(element.StartDate);
            this.Result[index].EndDateS = this._HelperService.GetDateS(element.EndDate);
            this.Result[index].LastUseDate = this._HelperService.GetDateTimeS(element.LastUseDate);
            this.Result[index].ActivityDate = this._HelperService.GetDateTimeS(element.ActivityDate);
            this.Result[index].LastTransactionDateDiff = this._HelperService.GetTimeDifferenceS(element.LastTransactionDate, moment());
            this.Result[index].TransactionDateDiff = this._HelperService.GetTimeDifferenceS(element.TransactionDate, moment());
            this.Result[index].TransactionDateD = this._HelperService.GetDateS(element.TransactionDate);
            this.Result[index].TransactionDateT = this._HelperService.GetTimeS(element.TransactionDate);
            this.Result[index].LastActivityDateD = this._HelperService.GetDateS(element.LastActivityDate);
            this.Result[index].LastActivityDateT = this._HelperService.GetTimeS(element.LastActivityDate);
            this.Result[index].TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
            this.Result[index].VerifyDate = this._HelperService.GetDateTimeS(element.VerifyDate);
            this.Result[index].InvoiceDate = this._HelperService.GetDateS(element.InvoiceDate);
            this.Result[index].RequestTime = this._HelperService.GetDateTimeS(element.RequestTime);
            this.Result[index].ResponseTime = this._HelperService.GetDateTimeS(element.ResponseTime);
            this.Result[index].CreateDateD = this._HelperService.GetDateS(element.CreateDate);
            this.Result[index].CreateDateT = this._HelperService.GetTimeS(element.CreateDate);
            this.Result[index].CreateDate = this._HelperService.GetDateTimeS(element.CreateDate);
            
            // 
            // this.Result[index].LastLoginDateT = this._HelperService.GetTimeS(element.LastLoginDate);
            // this.Result[index].LastLoginDateD = this._HelperService.GetDateTimeS(element.LastLoginDate);
            // this.Result[index].LastLoginDate = this._HelperService.GetDateTimeS(element.LastLoginDate);
            //
            this.Result[index].Date = this._HelperService.GetDateS(element.Date);
            this.Result[index].ModifyDate = this._HelperService.GetDateTimeS(element.ModifyDate);
            this.Result[index].LoginDate = this._HelperService.GetDateTimeS(element.LoginDate);
            if (this.Result[index].LastLoginDate != "0001-01-01T00:00:00") {
                this.Result[index].LastLoginDate = this._HelperService.GetDateTimeS(element.LastLoginDate);
            }
            else {
                this.Result[index].LastLoginDate = null;
            }
            if (this.Result[index].LastTransactionDate != "0001-01-01T00:00:00") {
                this.Result[index].LastTransactionDate = this._HelperService.GetDateTimeS(element.LastTransactionDate);
            }
            else {
                this.Result[index].LastTransactionDate = "";
            }
            this.Result[index].LogoutDate = this._HelperService.GetDateTimeS(element.LogoutDate);

            if (element.StatusId != undefined && element.StatusId != null) {
                this.Result[index].StatusI = this._HelperService.GetStatusIcon(element.StatusId);
                this.Result[index].StatusBadge = this._HelperService.GetStatusBadge(element.StatusId);
                this.Result[index].StatusC = this._HelperService.GetStatusColor(element.StatusId);
            } else {
                this.Result[index].StatusI = this._HelperService.GetStatusIcon(element.StatusCode);
                this.Result[index].StatusBadge = this._HelperService.GetStatusBadge(element.StatusCode);
                this.Result[index].StatusC = this._HelperService.GetStatusColor(element.StatusCode);
            }

            if (element.ApplicationStatusId != undefined) {
                this.Result[index].ApplStatusBadge = this._HelperService.GetStatusBadge(element.ApplicationStatusId);
            }

        }
        // var ResultI = this._HelperService.HCXGetDateComponentList(this.Result);
        ListOptions.Data = this.Result;
        return ListOptions;
    }

    solddownloadfile:boolean=false;
    List_GetData(ListOptions: OList, Show?: any, noStatusSearch?: Boolean, searchWithStatusId?: boolean): OList {

        if (Show != undefined && Show != null) {
            Show.ListShow = 'none';
            Show.IconShow = 'block';
        }

        ListOptions.SearchCondition = '';
        ListOptions = this.List_GetSearchCondition(ListOptions, noStatusSearch, searchWithStatusId);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;
        // ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;;
        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }

        if(ListOptions.IsfileDownloaded==true){
            this.solddownloadfile=true;
        }
        else{
            this.solddownloadfile=false;

        }
        var pData = {
            Task: ListOptions.Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            CashierReferenceId: ListOptions.CashierReferenceId,
            CashierReferenceKey: ListOptions.CashierReferenceKey,
            AccountId: ListOptions.AccountId,
            AccountKey: ListOptions.AccountKey,
            ListType: ListOptions.ListType,
            IsDownload: this.solddownloadfile,
            Status: ListOptions.Status,
            ProgramId: ListOptions.ProgramId
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(ListOptions.Location, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.DataLoad = false;
                    if (Show != undefined && Show != null) {
                        ListOptions = this.List_FormatResponse(ListOptions, _Response.Result, Show);
                    } else {
                        ListOptions = this.List_FormatResponse(ListOptions, _Response.Result);
                    }
                    if (pData.Task === 'getshipments') {
                        if (_Response.Result.Data && _Response.Result.Data.length > 0 && ListOptions.ListType === 1) {
                            _Response.Result.Data.forEach(element => {
                                if (element.CreateDate) {
                                    const startTime = moment(element.CreateDate);
                                    const endTime = moment();

                                    const hoursDiff = endTime.diff(startTime, 'hours');
                                    element.hoursDiff = `${hoursDiff} hours`;

                                    if (element.hoursDiff < 1) {
                                        const hoursDiff = endTime.diff(startTime, 'minutes');
                                        element.hoursDiff = `${hoursDiff} min`;
                                    }
                                    if (hoursDiff > 24) {
                                        const hoursDiff = endTime.diff(startTime, 'days');
                                        element.hoursDiff = `${hoursDiff} days`;
                                    }
                                }
                            });
                        }
                        if (_Response.Result.Data && _Response.Result.Data.length > 0 && (ListOptions.ListType === 2 || ListOptions.ListType === 3)) {
                            _Response.Result.Data.forEach(element => {
                                if (element.PickUpDate) {
                                    const startTime = moment(element.PickUpDate);
                                    const endTime = moment();

                                    var hrs = moment.utc(endTime.diff(startTime)).format("HH");
                                    var min = moment.utc(endTime.diff(startTime)).format("mm");
                                    var sec = moment.utc(endTime.diff(startTime)).format("ss");
                                    element.hoursDiff = [hrs, min, sec].join(':');
                                }
                            });
                        }
                    }
                    // console.log("ListOptions",ListOptions);
                    
                    return ListOptions;
                }
                else {
                    ListOptions.Data = [];

                    if (Show != undefined && Show != null) {
                        Show.ListShow = 'none';
                        Show.IconShow = 'block';
                    }
                    this._HelperService.NotifyError(_Response.Message);
                    return ListOptions;
                }
            },
            _Error => {
                if (Show != undefined && Show != null) {
                    Show.ListShow = 'none';
                    Show.IconShow = 'block';
                }
                this._HelperService.HandleException(_Error);
                return ListOptions;
            });
        return ListOptions;
    }
//start code for status options
statusValue = null
    public Status_Option: Select2Options;
    showOptions = true;
    public TUTr_Filter_StatusOptions: Array<Select2OptionData>;
    statusOption(options) {
        this.showOptions = false;
        if (options != undefined) {
            var _Data = options as any[];
            if (_Data.length > 0) {
              var finalCat = [];
              this.showOptions = false;
              _Data.forEach(element => {
                var Item = {
                  id: element.id,
                  text: element.text,
                code:element.code
                
                };
                finalCat.push(Item);
              });
              this.Status_Option = {
                placeholder: "Select Status",
                multiple: false,
                allowClear: false,
              };
              this.TUTr_Filter_StatusOptions = finalCat;
              this.showOptions = true;
            }
            else {
              this.Status_Option = {
                placeholder: "Select Status",
                multiple: false,
                allowClear: false,
              };
              setTimeout(() => {
                this.showOptions = true;
              }, 500);
            }
          }
          else {
            this.Status_Option = {
              placeholder: "Select Status",
              multiple: false,
              allowClear: false,
            };
            setTimeout(() => {
              this.showOptions = true;
            }, 500);
          }
    }

//end code for status options
    // for the deal merchant and deal customers
    List_GetDataas(ListOptions: OList, Show?: any): OList {
        if (Show != undefined && Show != null) {
            Show.ListShow = 'none';
            Show.IconShow = 'block';
        }

        ListOptions.SearchCondition = '';
        ListOptions = this.List_GetSearchCondition(ListOptions);
        if (ListOptions.ActivePage >= 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;
        // ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;;
        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }

        var pData = {
            Task: ListOptions.Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            CashierReferenceId: ListOptions.CashierReferenceId,
            CashierReferenceKey: ListOptions.CashierReferenceKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(ListOptions.Location, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.DataLoad = false;
                    if (Show != undefined && Show != null) {
                        ListOptions = this.List_FormatResponse(ListOptions, _Response.Result, Show);
                    } else {
                        ListOptions = this.List_FormatResponse(ListOptions, _Response.Result);
                    }
                    return ListOptions;
                }
                else {
                    ListOptions.Data = [];

                    if (Show != undefined && Show != null) {
                        Show.ListShow = 'none';
                        Show.IconShow = 'block';
                    }

                    this._HelperService.NotifyError(_Response.Message);
                    return ListOptions;
                }
            },
            _Error => {

                if (Show != undefined && Show != null) {
                    Show.ListShow = 'none';
                    Show.IconShow = 'block';
                }

                this._HelperService.HandleException(_Error);
                return ListOptions;
            });
        return ListOptions;
    }
    
    List_GetOverviewRef(ListOptions: OList, Show?: any): OList{
        if (Show != undefined && Show != null) {
            Show.ListShow = 'none';
            Show.IconShow = 'block';
        }

        ListOptions.SearchCondition = '';
        ListOptions = this.List_GetSearchCondition(ListOptions);
        if (ListOptions.ActivePage >= 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;
        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }

        var pData = {
            Task: ListOptions.Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            CashierReferenceId: ListOptions.CashierReferenceId,
            CashierReferenceKey: ListOptions.CashierReferenceKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(ListOptions.Location, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.DataLoad = false;
                    if (Show != undefined && Show != null) {
                        ListOptions = this.List_FormatResponse(ListOptions, _Response.Result, Show);
                    } else {
                        ListOptions = this.List_FormatResponse(ListOptions, _Response.Result);
                    }
                    return ListOptions;
                }
                else {
                    ListOptions.Data = [];

                    if (Show != undefined && Show != null) {
                        Show.ListShow = 'none';
                        Show.IconShow = 'block';
                    }
                    this._HelperService.NotifyError(_Response.Message);
                    return ListOptions;
                }
            },
            _Error => {
                if (Show != undefined && Show != null) {
                    Show.ListShow = 'none';
                    Show.IconShow = 'block';
                }
                this._HelperService.HandleException(_Error);
                return ListOptions;
            });
        return ListOptions;

    }

    List_GetDataconfig(ListOptions: OList, Show?: any): OList {

        if (Show != undefined && Show != null) {
            Show.ListShow = 'none';
            Show.IconShow = 'block';
        }

        ListOptions.SearchCondition = '';
        ListOptions = this.List_GetSearchCondition(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;
        // ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;;
        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }

        var pData = {
            Task: ListOptions.Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            CashierReferenceId: ListOptions.CashierReferenceId,
            CashierReferenceKey: ListOptions.CashierReferenceKey,
            AccountId: ListOptions.AccountId,
            AccountKey: ListOptions.AccountKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(ListOptions.Location, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.DataLoad = false;
                    if (Show != undefined && Show != null) {
                        ListOptions = this.List_FormatResponse1(ListOptions, _Response.Result, Show);
                    } else {
                        ListOptions = this.List_FormatResponse1(ListOptions, _Response.Result);
                    }

                    return ListOptions;
                }
                else {
                    ListOptions.Data = [];

                    if (Show != undefined && Show != null) {
                        Show.ListShow = 'none';
                        Show.IconShow = 'block';
                    }

                    this._HelperService.NotifyError(_Response.Message);
                    return ListOptions;
                }
            },
            _Error => {

                if (Show != undefined && Show != null) {
                    Show.ListShow = 'none';
                    Show.IconShow = 'block';
                }

                this._HelperService.HandleException(_Error);
                return ListOptions;
            });
        return ListOptions;


    }

    List_GetDataWithOutSort(ListOptions: OList, Show?: any, ForOverview?: Boolean, FirstTime?: Boolean, SearchWithStatusID?: Boolean): OList {
        if (Show != undefined && Show != null) {
            Show.ListShow = 'none';
            Show.IconShow = 'block';
        }

        ListOptions.SearchCondition = '';
        ListOptions = this.List_GetSearchCondition(ListOptions,false,SearchWithStatusID);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }


        if (!FirstTime) {
            var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;

            if (ListOptions.Sort.SortDefaultName) {
                ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
                ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
                ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
            }

            if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
                if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                    SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
                }
                else {
                    SortExpression = ListOptions.Sort.SortColumn + ' desc';
                }
            }
        }


        var pData = {
            Task: ListOptions.Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(ListOptions.Location, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {

                    if (ForOverview != undefined && ForOverview == true) {
                        this._HelperService.TerminalsBubbleSort(_Response.Result.Data);
                    }


                    if (Show != undefined && Show != null) {
                        ListOptions = this.List_FormatResponse(ListOptions, _Response.Result, Show);
                    } else {
                        ListOptions = this.List_FormatResponse(ListOptions, _Response.Result);
                    }

                    this._HelperService.SortTerminals(ListOptions.Data);

                    return ListOptions;

                }
                else {
                    ListOptions.Data = [];

                    if (Show != undefined && Show != null) {
                        Show.ListShow = 'none';
                        Show.IconShow = 'block';
                    }

                    this._HelperService.NotifyError(_Response.Message);
                    return ListOptions;
                }
            },
            _Error => {

                if (Show != undefined && Show != null) {
                    Show.ListShow = 'none';
                    Show.IconShow = 'block';
                }

                this._HelperService.HandleException(_Error);
                return ListOptions;
            });
        return ListOptions;
    }
    
    redeemdownloadFile:boolean=false;
    List_GetDataTur(ListOptions: OList, Show?: any, noStatusSearch?: boolean,): OList {
        if (Show != undefined && Show != null) {
            Show.ListShow = 'none';
            Show.IconShow = 'block';
        }

        ListOptions.SearchCondition = '';
        ListOptions = this.List_GetSearchConditionTur(ListOptions, noStatusSearch);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }
        if(ListOptions.IsfileDownloaded==true){
            this.redeemdownloadFile=true;
        }
        else{
            this.redeemdownloadFile=false;

        }

        var pData = {
            Task: ListOptions.Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            AccountId: ListOptions.AccountId,
            AccountKey: ListOptions.AccountKey,
            ListType: ListOptions.ListType,
            IsDownload: this.redeemdownloadFile,
            Status: ListOptions.Status,
            AuthAccountId: ListOptions.AuthAccountId,
            EmailAddress: ListOptions.EmailAddress,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(ListOptions.Location, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {

                    if (Show != undefined && Show != null) {
                        ListOptions = this.List_FormatResponse(ListOptions, _Response.Result, Show);
                    } else {
                        ListOptions = this.List_FormatResponse(ListOptions, _Response.Result);
                    }

                    return ListOptions;
                }
                else {
                    ListOptions.Data = [];

                    if (Show != undefined && Show != null) {
                        Show.ListShow = 'none';
                        Show.IconShow = 'block';
                    }

                    this._HelperService.NotifyError(_Response.Message);
                    return ListOptions;
                }
            },
            _Error => {

                if (Show != undefined && Show != null) {
                    Show.ListShow = 'none';
                    Show.IconShow = 'block';
                }

                this._HelperService.HandleException(_Error);
                return ListOptions;
            });
        return ListOptions;
    }
    List_GetDataRef(ListOptions: OList, Show?: any, noStatusSearch?: boolean,): OList {
        if (Show != undefined && Show != null) {
            Show.ListShow = 'none';
            Show.IconShow = 'block';
        }

        ListOptions.SearchCondition = '';
        ListOptions = this.List_GetSearchConditionTur(ListOptions, noStatusSearch);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }

        var pData = {
            Task: ListOptions.Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            // RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            // Type: ListOptions.Type,
            // ReferenceKey: ListOptions.ReferenceKey,
            // StartDate: ListOptions.StartDate,
            // EndDate: ListOptions.EndDate,
            // ReferenceId: ListOptions.ReferenceId,
            // SubReferenceId: ListOptions.SubReferenceId,
            // SubReferenceKey: ListOptions.SubReferenceKey,
            // AccountId: ListOptions.AccountId,
            // AccountKey: ListOptions.AccountKey,
            ListType: ListOptions.ListType,
            // IsDownload: false,
            // Status: ListOptions.Status,
            AuthAccountId: ListOptions.AuthAccountId,
            EmailAddress: ListOptions.EmailAddress,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(ListOptions.Location, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {

                    if (Show != undefined && Show != null) {
                        ListOptions = this.List_FormatResponse(ListOptions, _Response.Result, Show);
                    } else {
                        ListOptions = this.List_FormatResponse(ListOptions, _Response.Result);
                    }

                    return ListOptions;
                }
                else {
                    ListOptions.Data = [];

                    if (Show != undefined && Show != null) {
                        Show.ListShow = 'none';
                        Show.IconShow = 'block';
                    }

                    this._HelperService.NotifyError(_Response.Message);
                    return ListOptions;
                }
            },
            _Error => {

                if (Show != undefined && Show != null) {
                    Show.ListShow = 'none';
                    Show.IconShow = 'block';
                }

                this._HelperService.HandleException(_Error);
                return ListOptions;
            });
        return ListOptions;
    }
    List_GetDataTerm(ListOptions: OList, Show?: any, ForOverview?: Boolean): OList {

        if (Show != undefined && Show != null) {
            Show.ListShow = 'none';
            Show.IconShow = 'block';
        }

        ListOptions.SearchCondition = '';
        ListOptions = this.List_GetSearchCondition(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;

        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }

        var pData = {
            Task: ListOptions.Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(ListOptions.Location, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {

                    if (ForOverview != undefined && ForOverview == true) {
                        this._HelperService.TerminalsBubbleSort(_Response.Result.Data);
                    }


                    if (Show != undefined && Show != null) {
                        ListOptions = this.List_FormatResponse(ListOptions, _Response.Result, Show);
                    } else {
                        ListOptions = this.List_FormatResponse(ListOptions, _Response.Result);
                    }

                    this._HelperService.SortTerminals(ListOptions.Data);

                    return ListOptions;

                }
                else {
                    ListOptions.Data = [];

                    if (Show != undefined && Show != null) {
                        Show.ListShow = 'none';
                        Show.IconShow = 'block';
                    }

                    this._HelperService.NotifyError(_Response.Message);
                    return ListOptions;
                }
            },
            _Error => {

                if (Show != undefined && Show != null) {
                    Show.ListShow = 'none';
                    Show.IconShow = 'block';
                }

                this._HelperService.HandleException(_Error);
                return ListOptions;
            });
        return ListOptions;
    }
    List_DownloadCsv(ListOptions: OList) {
        var pData = {
            Task: ListOptions.Task,
            AccountId :ListOptions.AccountId,
            AccountKey : ListOptions.AccountKey,
            TotalRecords: ListOptions.TotalRecords,
            Offset: 0,
            Limit: ListOptions.TotalRecords,
            RefreshCount: false,
            SearchCondition: ListOptions.SearchCondition,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            ListType: ListOptions.ListType,
            IsDownload: ListOptions.IsDownload,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(ListOptions.Location, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (ListOptions.IsDownload) {
                        this._HelperService.NotifySuccess('Your download will be available shortly in downloads section');
                    }
                    else {
                        var _ResponseData = _Response.Result as OListResponse;
                        var Result = _ResponseData.Data;

                        ListOptions = this.List_FormatResponse(ListOptions, _Response.Result);
                        var CsvData = [];
                        var CsvLabels = [];
                        var CsvSystemNames = [];
                        ListOptions.VisibleHeaders.forEach(element => {
                            if (element.Show == true) {
                                CsvLabels.push(element.DisplayName);
                                CsvSystemNames.push(element.SystemName);
                            }
                        });
                        for (var index = 0; index < Result.length; index++) {
                            var Item = Result[index];
                            var Content: any = {};
                            CsvSystemNames.forEach(element => {
                                var item = Item[element];
                                if (item != undefined && item != null) {
                                    Content[element] = Item[element];
                                }
                                else {
                                    Content[element] = "n/a";
                                }
                            });
                            CsvData.push(Content);
                        }
                        var CsvOptions = {
                            showLabels: true,
                            useBom: false,
                            showTitle: true,
                            title: ListOptions.Title,
                            headers: CsvLabels,
                        };
                        this._HelperService.DownloadCsv(CsvData, ListOptions.Title, CsvOptions);
                    }
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }


    public S2_Status_Option: Select2Options;
    public S3_Status_Option: Select2Options;
    public S2_Plan_Option: Select2Options
    public S2_Sort_Option: Select2Options;
    public S2_Status_Transport: any;
    S2_Status_List(Value) {
        var PlaceHolder = 'Sort by status';
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            ReferenceId: 0,
            ReferenceKey: null,
            Fields: [
                {
                    SystemName: 'ReferenceId',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'Name',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }

            ],
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'ParentCode', this._HelperService.AppConfig.DataType.Text, Value, '=');
        this.S2_Status_Transport = this.S2_BuildList(_Select) as any;
        this.S2_Status_Option = {
            placeholder: PlaceHolder,
            ajax: this.S2_Status_Transport,
            multiple: false,
            allowClear: true,
        };
    }

    public S2_SavedFilters_Option: Select2Options;
    public S2_NewSavedFilters_Option: Select2Options;

    GetOverviews(ListOptions: any, Task: string): any {
        this._HelperService.IsFormProcessing = true;

        ListOptions.SearchCondition = '';
        ListOptions = this.List_GetSearchConditionTur(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }


        var pData = {
            Task: Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            AccountId: ListOptions.AccountId,
            AccountKey: ListOptions.AccountKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MTransaction, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    var Data = _Response.Result as any;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);

            });


    }



    //#region Location Manager - Start
    _LocManager =
        {
            IsAddressSet: false,
            Latitude: 0,
            Longitude: 0,

            Address: null,
            MapAddress: null,

            CountryId: 0,
            CountryCode: null,
            CountryName: null,

            StateId: 0,
            StateCode: null,
            StateName: null,

            CityId: 0,
            CityCode: null,
            CityName: null,

            CityAreaId: 0,
            CityAreaCode: null,
            CityAreaName: null,

            PostalCode: null,

            ShowCity: false,
            ShowState: false,
        }
    LocManager_OpenManager() {
        this.LocManager_OpenUpdateManager_Clear();
        this._HelperService.OpenModal('ModalLocationManager');
    }
    LocManager_OpenUpdateManager_Clear() {
        this._LocManager =
        {
            IsAddressSet: false,
            Latitude: 0,
            Longitude: 0,

            Address: null,
            MapAddress: null,

            CountryId: 0,
            CountryCode: null,
            CountryName: null,

            StateId: 0,
            StateName: null,
            StateCode: null,

            CityId: 0,
            CityCode: null,
            CityName: null,

            CityAreaId: 0,
            CityAreaName: null,
            CityAreaCode: null,

            PostalCode: null,

            ShowCity: false,
            ShowState: false,
        }
    }
    LocManager_OpenUpdateManager() {

        this._HelperService.OpenModal('ModalLocationManager');
    }
    public LocManager_AddressChange(address: Address) {
        this._LocManager.Latitude = address.geometry.location.lat();
        this._LocManager.Longitude = address.geometry.location.lng();
        this._LocManager.MapAddress = address.formatted_address;
        this._LocManager.Address = address.formatted_address;
        var tAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);;
        if (tAddress.postal_code != undefined && tAddress.postal_code != null && tAddress.postal_code != "") {
            this._LocManager.PostalCode = tAddress.postal_code;
        }
        if (tAddress.country != undefined && tAddress.country != null && tAddress.country != "") {
            this._LocManager.CountryName = tAddress.country;
        }
        if (tAddress.administrative_area_level_1 != undefined && tAddress.administrative_area_level_1 != null && tAddress.administrative_area_level_1 != "") {
            this._LocManager.StateName = tAddress.administrative_area_level_1;
        }
        if (tAddress.locality != undefined && tAddress.locality != null && tAddress.locality != "") {
            this._LocManager.CityName = tAddress.locality;
        }
        if (tAddress.administrative_area_level_2 != undefined && tAddress.administrative_area_level_2 != null && tAddress.administrative_area_level_2 != "") {
            this._LocManager.CityAreaName = tAddress.administrative_area_level_2;
        }
        if (tAddress.administrative_area_level_2 != undefined && tAddress.administrative_area_level_2 != null && tAddress.administrative_area_level_2 != "") {
            this._LocManager.CityAreaName = tAddress.administrative_area_level_2;
        }

        if (this._LocManager.CountryName != this._HelperService.UserCountry.CountryName) {
            this._HelperService.NotifyError('Currently we’re not serving in this area, please add locality within ' + this._HelperService.UserCountrycode);
            this.LocManager_OpenUpdateManager_Clear();
        }
        else {
            this._LocManager.CountryId = this._HelperService.UserCountry.CountryId;
            this._LocManager.CountryCode = this._HelperService.UserCountry.CountryKey;
            this._LocManager.CountryName = this._HelperService.UserCountry.CountryName;
            this.Loc_Manager_GetStates();

        }
    }
    public _LocManager_S2States_Data: Array<Select2OptionData>;
    Loc_Manager_GetStates() {
        this._LocManager.ShowState = false;
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Core.getstates,
            ReferenceKey: this._LocManager.CountryCode,
            ReferenceId: this._LocManager.CountryId,
            Offset: 0,
            Limit: 1000,
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.State, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        var _Data = _Response.Result.Data as any[];
                        if (_Data.length > 0) {
                            var finalCat = [];
                            _Data.forEach(element => {
                                var Item = {
                                    id: element.ReferenceId,
                                    key: element.ReferenceKey,
                                    text: element.Name,
                                    additional: element,
                                };
                                if (this._LocManager.StateName != undefined || this._LocManager.StateName != null || this._LocManager.StateName != '') {
                                    if (element.Name == this._LocManager.StateName) {
                                        this._LocManager.StateId = Item.id;
                                        this._LocManager.StateCode = Item.key;
                                        this._LocManager.StateName = Item.text;
                                        this._LocManager.ShowCity = false;
                                        this.Loc_Manager_City_Load();
                                        setTimeout(() => {
                                            this._LocManager.ShowCity = true;
                                        }, 500);
                                    }
                                }
                                finalCat.push(Item);
                            });
                            this._LocManager_S2States_Data = finalCat;
                            this._LocManager.ShowState = true;
                            if (this._LocManager.CityName != undefined && this._LocManager.CityName != null && this._LocManager.CityName != "") {
                                this.Loc_Manager_GetStateCity(this._LocManager.CityName);
                            }
                        }
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                this._HelperService.ToggleField = false;

            });
    }
    Loc_Manager_StateSelected(Items) {
        if (Items != undefined && Items.value != undefined && Items.data.length > 0) {
            this._LocManager.StateId = Items.data[0].id;
            this._LocManager.StateCode = Items.data[0].key;
            this._LocManager.StateName = Items.data[0].text;
            this._LocManager.CityId = null;
            this._LocManager.CityCode = null;
            this._LocManager.CityName = null;

            this._LocManager.Address = null;
            this._LocManager.MapAddress = null;
            this._LocManager.Latitude = 0;
            this._LocManager.Longitude = 0;
            this._LocManager.ShowCity = false;
            this.Loc_Manager_City_Load();
            setTimeout(() => {
                this._LocManager.ShowCity = true;
            }, 500);
        }
    }
    Loc_Manager_GetStateCity(CityName) {
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Core.getstates,
            ReferenceKey: this._LocManager.CountryCode,
            ReferenceId: this._LocManager.CountryId,
            SearchCondition: this._HelperService.GetSearchConditionStrict('', 'Name', 'text', CityName, '='),
            Offset: 0,
            Limit: 1,
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.State, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        var _Result = _Response.Result.Data;
                        if (_Result != undefined && _Result != null && _Result.length > 0) {
                            var Item = _Result[0];
                            this._LocManager.CityId = Item.ReferenceId;
                            this._LocManager.CityCode = Item.ReferenceKey;
                            this._LocManager.CityName = Item.Name;
                            this._LocManager.ShowCity = false;
                            this.Loc_Manager_City_Load();
                            setTimeout(() => {
                                this._LocManager.ShowCity = true;
                            }, 500);
                        }
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                this._HelperService.ToggleField = false;

            });
    }

    public Loc_Manager_City_Option: Select2Options;
    Loc_Manager_City_Load() {
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.getcities,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.City,
            ReferenceId: this._LocManager.StateId,
            ReferenceKey: this._LocManager.StateCode,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
            ],
        };
        var _Transport = this.S2_BuildList(_Select) as any;
        if (this._LocManager.CityName != undefined || this._LocManager.CityName != null && this._LocManager.CityName != '') {
            this.Loc_Manager_City_Option = {
                placeholder: this._LocManager.CityName,
                ajax: _Transport,
                multiple: false,
                allowClear: false,
            };
        }
        else {
            this.Loc_Manager_City_Option = {
                placeholder: "Select City",
                ajax: _Transport,
                multiple: false,
                allowClear: false,
            };
        }

    }
    Loc_Manager_City_Change(Items: any) {
        if (Items != undefined && Items.value != undefined && Items.data.length > 0) {
            this._LocManager.CityId = Items.data[0].ReferenceId;
            this._LocManager.CityCode = Items.data[0].ReferenceKey;
            this._LocManager.CityName = Items.data[0].Name;
        }
    }
    Loc_Manager_SetAddress() {
        if (this._LocManager.Address == undefined || this._LocManager.Address == null || this._LocManager.Address == "") {
            this._HelperService.NotifyError("Please enter address")
        }
        else if (this._LocManager.CountryId == undefined || this._LocManager.CountryId == null || this._LocManager.CountryId == 0) {
            this._HelperService.NotifyError("Please select country")
        }
        else if (this._LocManager.StateId == undefined || this._LocManager.StateId == null || this._LocManager.StateId == 0) {
            this._HelperService.NotifyError("Please select state")
        }
        else if (this._LocManager.CityId == undefined || this._LocManager.CityId == null || this._LocManager.CityId == 0) {
            this._HelperService.NotifyError("Please select city")
        }
        else {
            this._LocManager.IsAddressSet = true;
            this._HelperService.CloseModal('ModalLocationManager');
        }
    }
    Loc_Manager_SetAddress_Cancel() {
        this.LocManager_OpenUpdateManager_Clear();
        this._HelperService.CloseModal('ModalLocationManager');
    }
    //#endregion Location Manager -  End
}


export class OSearchFilter {
    public Title: string;
    public FildSystemName: string;
}



import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HelperService } from '../helper.service';
import { OAccessUserAccount } from '../object.service';
import { TranslateService } from "@ngx-translate/core";

@Injectable({
  providedIn: 'root'
})
export class MerchantguardGuard implements CanActivate, CanActivateChild {
  constructor(public _HelperService: HelperService, public _Router: Router,public _TranslateService: TranslateService) {
    this._TranslateService
      .get("Common.ReferenceId")
      .subscribe((_Message: string) => {
        this._HelperService.AppConfig.CommonResource.ReferenceId = _Message;
      });
    this._TranslateService
      .get("Common.InvalidImage")
      .subscribe((_Message: string) => {
        this._HelperService.AppConfig.CommonResource.ReferenceId = _Message;
      });

    this._TranslateService
      .get("Common.ReferenceCode")
      .subscribe((_Message: string) => {
        this._HelperService.AppConfig.CommonResource.ReferenceCode = _Message;
      });

    this._TranslateService
      .get("Common.ReferenceKey")
      .subscribe((_Message: string) => {
        this._HelperService.AppConfig.CommonResource.ReferenceKey = _Message;
      });

    this._TranslateService.get("Common.Name").subscribe((_Message: string) => {
      this._HelperService.AppConfig.CommonResource.Name = _Message;
    });

    this._TranslateService
      .get("Common.Select")
      .subscribe((_Message: string) => {
        this._HelperService.AppConfig.CommonResource.Select = _Message;
      });

    this._TranslateService
      .get("Common.Cancel")
      .subscribe((_Message: string) => {
        this._HelperService.AppConfig.CommonResource.Cancel = _Message;
      });
    this._TranslateService
      .get("Common.Continue")
      .subscribe((_Message: string) => {
        this._HelperService.AppConfig.CommonResource.Continue = _Message;
      });

    this._TranslateService
      .get("Common.Status")
      .subscribe((_Message: string) => {
        this._HelperService.AppConfig.CommonResource.Status = _Message;
      });

    this._TranslateService
      .get("Common.CreateDate")
      .subscribe((_Message: string) => {
        this._HelperService.AppConfig.CommonResource.CreateDate = _Message;
      });

    this._TranslateService
      .get("Common.CreatedBy")
      .subscribe((_Message: string) => {
        this._HelperService.AppConfig.CommonResource.CreatedBy = _Message;
      });
    this._TranslateService
      .get("Common.ModifyDate")
      .subscribe((_Message: string) => {
        this._HelperService.AppConfig.CommonResource.ModifyDate = _Message;
      });
    this._TranslateService
      .get("Common.ModifyBy")
      .subscribe((_Message: string) => {
        this._HelperService.AppConfig.CommonResource.ModifyBy = _Message;
      });
    this._TranslateService
      .get("Common.DeleteTitle")
      .subscribe((_Message: string) => {
        this._HelperService.AppConfig.CommonResource.DeleteTitle = _Message;
      });
    this._TranslateService
      .get("Common.DeleteHelp")
      .subscribe((_Message: string) => {
        this._HelperService.AppConfig.CommonResource.DeleteHelp = _Message;
      });
  }
  public UserAccount: OAccessUserAccount;
  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | import("@angular/router").UrlTree | Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
    // switch (state.url) {
    //   case "/console/dashboard": {
    //     if (true) {
    //       return true;
    //     } else {
    //       return false;
    //     }
    //   }
    //     break;

    //   default:
    //     break;
    // }

    this._HelperService.SaveStorage("temploc", {
      StorageRoute: state.url
    });
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.FullContainer = false;
    var AccountTypeCode = childRoute.data.accounttypecode;
    if (AccountTypeCode != undefined) {
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = AccountTypeCode;
    } else {
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = null;
    }
    var ActiveReferenceListType = childRoute.data.listtype;
    if (ActiveReferenceListType != undefined) {
      this._HelperService.AppConfig.ActiveReferenceListType = ActiveReferenceListType;
    } else {
      this._HelperService.AppConfig.ActiveReferenceListType = null;
    }
    return true;
    if (this._HelperService.UserAccount != null) {
      var PageName = childRoute.data.PageName;


      if (PageName != undefined) {
        this._HelperService._TranslateService.get(PageName).subscribe((_PageName: string) => {
          this._HelperService.AppConfig.ActivePageName = _PageName;
        });
      }
      return true;
    } else {
      this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
      return false;
    }
    return true;

    // return true;
    // this._HelperService.SalesPerformances = true;
    // return true;
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      this._HelperService.SaveStorage("temploc", {
        StorageRoute: state.url
      });
      this._HelperService.AppConfig.ShowHeader = true;      
      var AccountTypeCode = next.data.accounttypecode;
      if (AccountTypeCode != undefined) {
        this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = AccountTypeCode;
      } else {
        this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = null;
      }
      var ActiveReferenceListType = next.data.listtype;
      if (ActiveReferenceListType != undefined) {
        this._HelperService.AppConfig.ActiveReferenceListType = ActiveReferenceListType;
      } else {
        this._HelperService.AppConfig.ActiveReferenceListType = null;
      }
  
      if (this._HelperService.UserAccount != null) {
        var PageName = next.data.PageName;
  
  
        if (PageName != undefined) {
          this._TranslateService.get(PageName).subscribe((_PageName: string) => {
            this._HelperService.AppConfig.ActivePageName = _PageName;
          });
        }     
      } 

    switch (state.url) {
      case "/console/dashboard": {
        if (true) {
          return true;

        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);

        }
      }

        break;
      case "/console/merchants": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewmerchants')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;


      case "/console/stores": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'storesview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;


      case "/console/terminals": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'terminalsview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;

      case "/console/cashiers": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewcashiers')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;


      case "/console/acquirers": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewacquirers')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;

      case "/console/refer": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewrefer')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;


      case "/console/ptsps": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewptsps')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/gatewayproviders": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewpssps')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/partners": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewpartners')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;

      case "/console/customers/allcustomers": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewcustomers')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/customers/merchantcustomers": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewcustomers')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/customers/tuccustomers": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewcustomers')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/customers/tucappdownloadcustomers": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewcustomers')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/saleshistory": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'salestransactionsview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/conpendingtransaction": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'susptransactionsview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/rewardhistory": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'rewardtransactionsview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;

      case "/console/pendingrewardhistory": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'pendingrewardtransactionsview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;

      case "/console/redeemhistory": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'redeemtransactionsview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;

      case "/console/rewardclaim": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'rewardclaimtransactionsview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/analyticoverview": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'analyticsoverview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/customeranalysis": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewcustomeranalytics')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/merchantanalysis": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewmerchantanalytics')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/growthreveneue": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewgrowthanalytics')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/merchantwallets": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewmerchantwalletpayments')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/customerwallets": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewcustomerwalletpayments')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

      case "/console/customerwallethistory": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewcustomerwalletpayments')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

      case "/console/merchantwallethistory": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewcustomerwalletpayments')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/merchantcashouts": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewmerchantcashouts')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/cashouts": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewcustomercashouts')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/supercash": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewsupercashrequests')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/refunds": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewrefundrequests')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/refbonus": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewrefbonusrequests')) {
          return true;
        } 
        else {
          // this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return true;
        }
      }

        break;
        case "/console/referralb": {

          if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewrefbonusrequests')) {
            return true;
          } 
          else {
            // this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
            return true;
          }
        }
  
          break;
      case "/console/plans": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewaccountsubscriptions')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/invoices": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewinvoices')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/referal": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'allcampaignview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/payments": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewvaspayments')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/vasanalytics": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewvasanalytics')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/vas/airtime": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewvassettings')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/vas/electricity": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewvaselectricity')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/vas/tollgate": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewvastollgate')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/vas/tvrecharge": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewvastvrecharge')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/deals": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'dealsview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/flashdeals": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'flasdealsview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;

      case "/console/dealpromotiondeals": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'promotiondelview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/dealsoverview": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'dealoverviewview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/soldhistory": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'dealssoldhistoryview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/externalurl": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'dealsexternalurlview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }


        break;
      case "/console/dealredeemhistory": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'dealsredeemhistory')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/apppromotion": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'apppromotionview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;

      case "/console/downloads": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewdownloads')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }
        break;
      case "/console/merchantcategories": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'businesscategoryview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }
        break;
      case "/console/faqcategories": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'faqcategoryview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }
        break;
      case "/console/appslider": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'customerappsliderview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }
        break;
      case "/console/verificationrequest": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'customermobileverificationrequetview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }
        break;
      case "/console/usersession": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'customerloginsessionview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }
        break;
      case "/console/requesthistory": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'apirequestmanagerview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }
        //newly added config V2 path

        break;
      case "/console/smsoverview": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'smsoverview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;

      case "/console/smscampaigns": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'campaignsview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/merchantsms": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'smsmerchant')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/credithistory": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'smscredithistory')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;

      case "/console/generalsettinghome": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'generalsettings')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/administrationhome": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'systemadministration')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;

      case "/console/newapis": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'appandkeymanagerconfigs')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }



        break;
      case "/console/apps": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'appandkeymanagerview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }
        break;
      case "/console/responsecode": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'systemresponsemessageview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }
        break;
      case "/console/systemlog": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'systemlogmanager')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }
        break;
      case "/console/configurations": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'configurationmanagerview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }
        break;
      case "/console/city": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'citymanagerview')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }
        break;


      case "/console/adminfeatures": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'adminnfeatures')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }
        break;
      case "/console/logfeature": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'changelogfeature')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }
        break;


      case "/console/pushnotification": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'customerapppushnotification')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }
        break;

      case "/console/smsnotification": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'smsnotificationlog')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }
        break;

      case "/console/settings/adminusers": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewusers')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;
      case "/console/settings/addroles": {
        return true;
        // if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'changelogfeature')) {
        //   return true;
        // } else {
        //   this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
        //   return false;
        // }
      }
        break;
      case "/console/settings/adminroles": {

        if (this._HelperService.UserAccount.Features.find(x => x.SystemName == 'viewroles')) {
          return true;
        } else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
          return false;
        }
      }

        break;

      case "/console/dashboard": {
        return true;
      }

        break;

      default:
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotAccess]);
        return false;
        break;
    }
    this._HelperService.Poster_Crop_Clear();
    this._HelperService.Icon_Crop_Clear();
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.FullContainer = false;
    if (this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.Account) != null) {
      return true;
    } else {
      this._Router.navigate([this._HelperService.AppConfig.Pages.System.Login]);
      return false;
    }
    return true;

    // this._HelperService.SalesPerformances = true;
    // return true;
  }




  // canActivate(
  //   next: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
  //   var Dashboard = this._HelperService.SystemName.filter(function(Dashboard) {
  //     return  Dashboard = 'dashboard';
  //   });
  //   if (Dashboard) {
  //     return true
  //   }
  //   else {
  //     return false
  //   }
  //   this._HelperService.SalesPerformances = true;
  //   return true;
  // }
}
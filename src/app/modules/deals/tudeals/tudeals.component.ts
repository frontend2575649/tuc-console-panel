import { ChangeDetectorRef, Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;
declare var moment: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../service/service";
import { Observable, Subscription } from 'rxjs';
import { ChangeContext } from 'ng5-slider';
declare var moment: any;

@Component({
  selector: "tu-tudeals",
  templateUrl: "./tudeals.component.html",
})
export class TUDealsComponent implements OnInit, OnDestroy {
  typeList = 3;
  FlashTimings: any = {
    Start: new Date(),
    End: new Date()
  }
  DealTimings: any = {
    Start: new Date(),
    End: new Date()
  }

  Form_AddUser: FormGroup;

  public showFlashDeals: boolean = true;
  public ResetFilterControls: boolean = true;
  UpdateStatusArray: any = ['deal.draft', 'deal.approvalpending', 'deal.approved', 'deal.published', 'deal.paused', 'deal.expired',];
  public _ObjectSubscription: Subscription = null;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
  }


  ngOnInit() {

    this._HelperService.StopClickPropogation();
    Feather.replace();
    this.ListType = 3;
    this.TodayStartTime = this._HelperService.AppConfig.DefaultStartTimeAll;
    this.TodayEndTime = this._HelperService.AppConfig.DefaultEndTimeToday;
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];
      this.DealsList_Setup();
      this.DealsList_Filter_Owners_Load();
      this.InitColConfig();
      this.GetSalesOverview();
      this.Form_AddUser_Load();
    });

    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.DealsList_GetData();
    // });
    // this._HelperService.StopClickPropogation();
     this.DealerList_ListTypeChange(this.typeList);
  }

  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {
    }
  }


  public _GetoverviewSummary: any = {};
  public TodayStartTime = null;
  public TodayEndTime = null;
  private pData = {
    Task: 'getdealsoverview',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    // ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
    // ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
    AccountId: null,
    AccountKey: null,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  
  RestrictApiCall:boolean =false; //Helps to avoid multiple api calls of getdeals 
  GetSalesOverview() {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);
    this.pData.AccountId = this._HelperService.AppConfig.ActiveMerchantReferenceId
    this.pData.AccountKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetoverviewSummary = _Response.Result as any;
          // this.Piedata[0] = this._GetoverviewSummary.Total;
          this._ChangeDetectorRef.detectChanges();
          return;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  //#region columnConfig

  TempColumnConfig: any = [
    { Name: "Status", Value: true, },
    { Name: "City", Value: true, },
    { Name: "Contact", Value: true, },
    { Name: "Stores", Value: true, },
    { Name: "Reward", Value: true, },
    { Name: "POS", Value: true, },
    { Name: "ActivePOS", Value: true, },
    { Name: "RM", Value: true, },
    { Name: "Added", Value: true, },
  ];

  ColumnConfig: any = [
    { Name: "Status", Value: true, },
    { Name: "City", Value: true, },
    { Name: "Contact", Value: true, },
    { Name: "Stores", Value: true, },
    { Name: "Reward", Value: true, },
    { Name: "POS", Value: true, },
    { Name: "ActivePOS", Value: true, },
    { Name: "RM", Value: true, },
    { Name: "Added", Value: true, },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  AddNewDeal() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.MerchantDeals.MDealAdd,
      this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      this._HelperService.AppConfig.ActiveMerchantReferenceId
    ]);
  }

  Deal_AvailableRangeMinAmount: number = this._HelperService.AppConfig.DealMinimumLimit;
  Deal_AvailableRangeMaxAmount: number = this._HelperService.AppConfig.DealMaximumLimit;
  Deal_SoldRangeMinAmount: number = this._HelperService.AppConfig.DealPurchaseMinimumLimit;
  Deal_SoldRangeMaxAmount: number = this._HelperService.AppConfig.DealPurchaseMaximumLimit;

  SetSearchRanges(): void {
    this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalAvailable', this.DealsList_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalAvailable', this.Deal_AvailableRangeMinAmount, this.Deal_AvailableRangeMaxAmount);
    if (this.Deal_AvailableRangeMinAmount == this._HelperService.AppConfig.DealMinimumLimit && this.Deal_AvailableRangeMaxAmount == this._HelperService.AppConfig.DealMaximumLimit) {
      this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.DealsList_Config.SearchBaseConditions);
    }
    else {
      this.DealsList_Config.SearchBaseConditions.push(SearchCase);
    }
    this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalPurchase', this.DealsList_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalPurchase', this.Deal_SoldRangeMinAmount, this.Deal_SoldRangeMaxAmount);
    if (this.Deal_SoldRangeMinAmount == this._HelperService.AppConfig.DealPurchaseMinimumLimit && this.Deal_SoldRangeMaxAmount == this._HelperService.AppConfig.DealPurchaseMaximumLimit) {
      this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.DealsList_Config.SearchBaseConditions);
    }
    else {
      this.DealsList_Config.SearchBaseConditions.push(SearchCase);
    }

  }

  public ListType: number;
  public DealsList_Config: OList;

  DealsList_Setup() {
    this.DealsList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.Getdeals,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals,
      Title: "Available Stores",
      StatusType: "Deal",
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      DefaultSortExpression: "CreateDate desc",
      SearchBaseCondition: '',
      TableFields: [
        {
          DisplayName: " Merchant Name",
          SystemName: "AccountDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: " Title",
          SystemName: "Title",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Stores",
          SystemName: "Locations",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        // {
        //   DisplayName: "Is Flash",
        //   SystemName: "IsFlashDeal",
        //   DataType: this._HelperService.AppConfig.DataType.Number,
        //   Class: "text-right",
        //   Show: true,
        //   Search: false,
        //   Sort: true,
        //   ResourceId: null,
        //   NavigateField: "ReferenceKey"
        // },
        {
          DisplayName: "Description",
          SystemName: "Description",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Budget",
          SystemName: "Budget",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Deals",
          SystemName: "TotalAvailable",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Deals Sold",
          SystemName: "TotalPurchase",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
          //   .PanelAcquirer.Merchant.Dashboard,
        }, {
          DisplayName: "Category Name",
          SystemName: "CategoryName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: 'Start Date',
          SystemName: "StartDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: 'End Date',
          SystemName: "EndDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.DealsList_Config.SearchBaseCondition= this._HelperService.GetSearchConditionStrict('', 'AccountId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '=='),
    this.DealsList_Config.ListType = this.ListType;

    if (this.DealsList_Config.ListType == 1) //  approvalpending
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.approvalpending', "=");
      this.SelectStatusApprovalpending = true;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }
    else if (this.DealsList_Config.ListType == 2) //  approved
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'AccountId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '==');
      this.DealsList_Config.Type = "upcoming";
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = true;
      this.SelectStatusExpired = false;
    }
    else if (this.DealsList_Config.ListType == 3) // published
    {
      this.DealsList_Config.SearchBaseCondition= this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, 'TotalAvailable', this._HelperService.AppConfig.DataType.Number,'0', '>'),
      this.DealsList_Config.Type = "running";
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = true;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }
    else if (this.DealsList_Config.ListType == 4) // paused
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.paused', "=");
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = true;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }
    else if (this.DealsList_Config.ListType == 5) // draft
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.draft', "=");
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = true;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }
    else if (this.DealsList_Config.ListType == 6) // expired
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.expired', "=");
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = true;
    }
    else {
      this.DealsList_Config.DefaultSortExpression = 'CreateDate desc';
      this.SelectStatusApprovalpending = false;
      this.SelectStatusDraft = false;
      this.SelectStatusRunning = false;
      this.SelectStatusPaused = false;
      this.SelectStatusUpcoming = false;
      this.SelectStatusExpired = false;
    }
    this.DealsList_Config = this._DataHelperService.List_Initialize(
      this.DealsList_Config
    );
    // this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "DealerKey", 'text', this._HelperService.AppConfig.ActiveOwnerKey, "=");
    // this.DealsList_GetData();
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Deals,
      this.DealsList_Config
    );
    this.DealsList_GetData();
  }
  public validasale: boolean = true;

  DealsList_ToggleOption(event: any, Type: any) {
    if (Type == "date") {
      this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
      this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
    }
    if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
      event.data = {
        DealMin: this.Deal_AvailableRangeMinAmount,
        DealMax: this.Deal_AvailableRangeMaxAmount,
        DealPurchaseMin: this.Deal_SoldRangeMinAmount,
        DealPurchaseMax: this.Deal_SoldRangeMaxAmount
      }
    }
    if (event != null) {
      for (let index = 0; index < this.DealsList_Config.Sort.SortOptions.length; index++) {
        const element = this.DealsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(event, Type, this.DealsList_Config);
    this.DealsList_Config = this._DataHelperService.List_Operations(this.DealsList_Config, event, Type);

    if (
      (this.DealsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.DealsList_GetData();
    }

  }

  timeout = null;
  DealsList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.DealsList_Config.Sort.SortOptions.length; index++) {
          const element = this.DealsList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(event, Type, this.DealsList_Config);
      this.DealsList_Config = this._DataHelperService.List_Operations(this.DealsList_Config, event, Type);

      if (
        (this.DealsList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.DealsList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);
  }

  DealsList_GetData() {
    if(this.ListType === 2){
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'AccountId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '==');
    }
    var TConfig = this._DataHelperService.List_GetData(this.DealsList_Config);
    this.DealsList_Config = TConfig;
  }

  DealsList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveDeal,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Deal,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
    this._HelperService.AppConfig.ActiveAccountKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveAccountId = ReferenceData.ReferenceId;

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.MerchantDeals.MDeal,
      ReferenceData.ReferenceKey, ReferenceData.ReferenceId,
      ReferenceData.AccountId, ReferenceData.AccountKey,
    ]);
  }

  //#endregion
  SelectStatusApprovalpending: boolean = false
  SelectStatusDraft: boolean = false
  SelectStatusRunning: boolean = true
  SelectStatusPaused: boolean = false
  SelectStatusUpcoming: boolean = false
  SelectStatusExpired: boolean = false


  DealerList_ListTypeChange(Type) {
    this.RestrictApiCall =true;
    this.ListType = Type;
    this.DealsList_Setup();
    this.GetSalesOverview();
    this.flashOptionCurrent = 'false';
  }
  //#region OwnerFilter

  public DealsList_Filter_Owners_Option: Select2Options;
  public DealsList_Filter_Owners_Selected = null;
  DealsList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };

    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.DealsList_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }

  DealsList_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.DealsList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );
    this.OwnerEventProcessing(event);
  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.DealsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.DealsList_Filter_Owners_Selected,
        "="
      );
      this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.DealsList_Config.SearchBaseConditions
      );
      this.DealsList_Filter_Owners_Selected = null;
    } else if (event.value != this.DealsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.DealsList_Filter_Owners_Selected,
        "="
      );
      this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.DealsList_Config.SearchBaseConditions
      );
      this.DealsList_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.DealsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.DealsList_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.DealsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  //#endregion

  SetSalesRanges(): void {
    this.Deal_AvailableRangeMinAmount = this._HelperService.AppConfig.DealMinimumLimit;
    this.Deal_AvailableRangeMaxAmount = this._HelperService.AppConfig.DealMaximumLimit;
    this.Deal_SoldRangeMinAmount = this._HelperService.AppConfig.DealPurchaseMinimumLimit;
    this.Deal_SoldRangeMaxAmount = this._HelperService.AppConfig.DealPurchaseMaximumLimit;
  }


  SetOtherFilters(): void {
    this.DealsList_Config.SearchBaseConditions = [];

    this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'AccountId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '==');
    this.DealsList_Config.ListType = this.ListType;
    // this.DealsList_Config.SearchBaseCondition = "";

    if (this.DealsList_Config.ListType == 1) //  approvalpending
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.approvalpending', "=");
    }
    else if (this.DealsList_Config.ListType == 2) //  approved
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.approved', "=");
    }
    else if (this.DealsList_Config.ListType == 3) // published
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.published', "=");
    }
    else if (this.DealsList_Config.ListType == 4) // paused
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.paused', "=");
    }
    else if (this.DealsList_Config.ListType == 5) // draft
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.draft', "=");
    }
    else if (this.DealsList_Config.ListType == 6) // expired
    {
      this.DealsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.DealsList_Config.SearchBaseCondition, "StatusCode", 'number', 'deal.expired', "=");
    }
    else {
      this.DealsList_Config.DefaultSortExpression = 'CreateDate desc';
    }

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.DealsList_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.DealsList_Config);
    this.SetOtherFilters();
    this.SetSalesRanges();
    if( event.value != '-1' || this.RestrictApiCall === false){
      if(event.data.length >0){
        this.DealsList_GetData();
      }
    }
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.DealsList_Config);
    if (Type == 'Time') {
      this._HelperService.AppConfig.DateRangeOptions.startDate = new Date(2017, 0, 1, 0, 0, 0, 0);
      this._HelperService.AppConfig.DateRangeOptions.endDate = moment().endOf("day");
    }
    this.SetOtherFilters();
    this.SetSalesRanges();
    this.DealsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        maxLength: "4",
        minLength: "4",
      },
      inputValidator: function (value) {
        if (value === '' || value.length < 4) {
          return 'Enter filter name length greater than 4!'
        }
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();
        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Deals
        );
        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Deals
        );
        this._FilterHelperService.SetMerchantConfig(this.DealsList_Config);
        this.DealsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    if (this.Deal_AvailableRangeMinAmount == null || this.Deal_AvailableRangeMaxAmount == null || this.Deal_SoldRangeMinAmount == null || this.Deal_SoldRangeMaxAmount == null) {
      this._HelperService.NotifyError("Please Enter a number greater than or equal to 0")
    } else if ((this.Deal_AvailableRangeMinAmount > this.Deal_AvailableRangeMaxAmount) || (this.Deal_SoldRangeMaxAmount < this.Deal_SoldRangeMinAmount)) {
      this._HelperService.NotifyError("Enter valid Deal value")
    } else {

      this.SetSearchRanges();
      this._HelperService.MakeFilterSnapPermanent();
      this.DealsList_GetData();

      if (ButtonType == 'Sort') {
        $("#DealsList_sdropdown").dropdown('toggle');
      } else if (ButtonType == 'Other') {
        $("#DealsList_fdropdown").dropdown('toggle');
      }

      this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }
  }

  ResetFilters(event: any, Type: any): void {
    this.validasale = true;
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.DealsList_Config);
    this.SetOtherFilters();
    this.SetSalesRanges();
    this.DealsList_GetData();
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    this.DealsList_Filter_Owners_Load();
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  CloseRowModal(index: number): void {
    $("#SubAccountsList_rdropdown_" + index).dropdown('toggle');
  }

  flashOptionCurrent: string = 'false';
  FlashOptionSelected: any;
  FlashOptions: any = [
    {
      id: 0,
      text: 'All',
      code: 'all'
    },
    {
      id: 1,
      text: 'Flash',
      code: 'flash'
    }
  ];

  FlashList_ToggleOption(): void {
    var ev: any = {
      target: 'IsFlashDeal',
      value: this.flashOptionCurrent
    };
    this.DealList_Filter_Flash_Change(ev);
  }

  DealList_Filter_Flash_Change(event: any) {
    if (event.value == this.FlashOptionSelected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'IsFlashDeal', this._HelperService.AppConfig.DataType.Text, this.FlashOptionSelected, '=');
      this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.DealsList_Config.SearchBaseConditions);
      this.FlashOptionSelected = 0;
    }
    else if (event.value != this.FlashOptionSelected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'IsFlashDeal', this._HelperService.AppConfig.DataType.Text, this.FlashOptionSelected, '=');
      this.DealsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.DealsList_Config.SearchBaseConditions);
      this.FlashOptionSelected = event.value;
      this.DealsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'IsFlashDeal', this._HelperService.AppConfig.DataType.Text, this.FlashOptionSelected, '='));
    }
    this.DealsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  public _UserAccount: any =
    {
      MerchantDisplayName: null,
      SecondaryEmailAddress: null,
      BankDisplayName: null,
      BankKey: null,
      OwnerName: null,
      SubOwnerAddress: null,
      SubOwnerLatitude: null,
      SubOwnerDisplayName: null,
      SubOwnerKey: null,
      SubOwnerLongitude: null,
      AccessPin: null,
      LastLoginDateS: null,
      AppKey: null,
      AppName: null,
      AppVersionKey: null,
      CreateDate: null,
      CreateDateS: null,
      CreatedByDisplayName: null,
      CreatedByIconUrl: null,
      CreatedByKey: null,
      Description: null,
      IconUrl: null,
      ModifyByDisplayName: null,
      ModifyByIconUrl: null,
      ModifyByKey: null,
      ModifyDate: null,
      ModifyDateS: null,
      PosterUrl: null,
      ReferenceKey: null,
      StatusCode: null,
      StatusI: null,
      StatusId: null,
      StatusName: null,
      AccountCode: null,
      AccountOperationTypeCode: null,
      AccountOperationTypeName: null,
      AccountTypeCode: null,
      AccountTypeName: null,
      Address: null,
      AppVersionName: null,
      ApplicationStatusCode: null,
      ApplicationStatusName: null,
      AverageValue: null,
      CityAreaKey: null,
      CityAreaName: null,
      CityKey: null,
      CityName: null,
      ContactNumber: null,
      CountValue: null,
      CountryKey: null,
      CountryName: null,
      DateOfBirth: null,
      DisplayName: null,
      EmailAddress: null,
      EmailVerificationStatus: null,
      EmailVerificationStatusDate: null,
      FirstName: null,
      GenderCode: null,
      GenderName: null,
      LastLoginDate: null,
      LastName: null,
      Latitude: null,
      Longitude: null,
      MobileNumber: null,
      Name: null,
      NumberVerificationStatus: null,
      NumberVerificationStatusDate: null,
      OwnerDisplayName: null,
      OwnerKey: null,
      Password: null,
      Reference: null,
      ReferralCode: null,
      ReferralUrl: null,
      RegionAreaKey: null,
      RegionAreaName: null,
      RegionKey: null,
      RegionName: null,
      RegistrationSourceCode: null,
      RegistrationSourceName: null,
      RequestKey: null,
      RoleKey: null,
      RoleName: null,
      SecondaryPassword: null,
      SystemPassword: null,
      UserName: null,
      WebsiteUrl: null,
      StateKey: null,
      StateName: null
    }

  CouponCount: number = null;
  SelectedDeal: any = {};
  SelectedDealStartDateS: any = "";

  GetAccountDetails(ReferenceData: any, modal: string) {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetDeal,
      ReferenceId: ReferenceData.ReferenceId, ReferenceKey: ReferenceData.ReferenceKey,
      AccountId: ReferenceData.AccountId, AccountKey: ReferenceData.AccountKey
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._UserAccount = _Response.Result;
          this.CouponCount = this._UserAccount.MaximumUnitSale;
          this.SelectedDeal = {};
          this.SelectedDeal.ReferenceKey = this._UserAccount.ReferenceKey;
          this.SelectedDeal.ReferenceId = this._UserAccount.ReferenceId;
          this.SelectedDeal.EndDate = moment(this._UserAccount.EndDate);
          this.SelectedDeal.StartDate = moment(this._UserAccount.StartDate);
          this.SelectedDealStartDateS = this._HelperService.GetDateS(this._UserAccount.StartDate);
          this.SelectedDeal.Schedule = [];

          //#region Remove Redeem Schedules 
          if (this._UserAccount.Schedule) {
            for (let index = 0; index < this._UserAccount.Schedule.length; index++) {
              const element = this._UserAccount.Schedule[index];
              if (element.Type != 'dealshedule') {
                this.SelectedDeal.Schedule.push(element);
              }
            }
          }
          //#endregion
          if (modal == 'schedule') {
            this._HelperService.OpenModal('EditSchedule');
          } else {
            this._HelperService.OpenModal('AddCoupons');
          }
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
    //    this._ChangeDetectorRef.detectChanges();
  }

  public _Address: any = {};
  public _ContactPerson: any = {};
  timeRemaining = "--";
  isTimeLessThanDay: boolean = false;
  timerConfig: any = { leftTime: 30 };

  //#region Run Time 
  IsRunAllTime: boolean = true;
  RunAllTimeToogle(): void {
    this.IsRunAllTime = !(this.IsRunAllTime);
    this.SelectedDeal.Schedule = [];
  }

  ShowEditSchedule(ReferenceData: any): void {}

  ScheduleEndDateRangeChange(value) {
    this.SelectedDeal.EndDate = value.end;
  }

  ReFormat_RequestBody(): void {
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'updatedeal',
      'ReferenceKey': this.SelectedDeal.ReferenceKey,
      'ReferenceId': this.SelectedDeal.ReferenceId,
      "TypeCode": "deal",
      "StartDate": this.SelectedDeal.StartDate,
      "EndDate": this.SelectedDeal.EndDate,
      "Schedule": this.SelectedDeal.Schedule
    };
    //#region Set Schedule 

    for (let index = 0; index < 7; index++) {

      if (this.IsRunAllTime) {
        formRequest.Schedule.push({
          DayOfWeek: index,
          StartHour: '00:00',
          EndHour: '23:59',
          Type: 'dealshedule'
        });
      } else {
        formRequest.Schedule.push({
          DayOfWeek: index,
          StartHour: moment(this.DealTimings.Start).format('hh:mm'),
          EndHour: moment(this.DealTimings.End).format('hh:mm'),
          Type: 'dealshedule'
        });

      }
    }
    //#endregion
    return formRequest;
  }

  UpdateSchedule(): void {

    var Req = this.ReFormat_RequestBody();
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, Req);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess('Deal Schedule updated successfully. It will take upto 5 minutes to update changes.');

          this.ClearScheduleUpdate();
          this._HelperService.CloseModal('EditSchedule');
          this.DealsList_GetData();
        }
        else {
          this.SelectedDeal.Schedule = [];
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.SelectedDeal.Schedule = [];
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  ShowEditStock(ReferenceData: any): void {
    this.SelectedDeal.ReferenceKey = ReferenceData.ReferenceKey;
    this.SelectedDeal.ReferenceId = ReferenceData.ReferenceId;
    this._HelperService.OpenModal('AddCoupons');
  }

  EditStock(): void {

    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'updatedeal',
      'ReferenceKey': this.SelectedDeal.ReferenceKey,
      'ReferenceId': this.SelectedDeal.ReferenceId,
      "TypeCode": "deal",
      "MaximumUnitSale": this.CouponCount
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, formRequest);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess('Deal Coupon Count updated successfully. It will take upto 5 minutes to update changes.');
          this.CouponCount = null;
          this._HelperService.CloseModal('AddCoupons');
          this.DealsList_GetData();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  ClearCouponUpdate(): void {
    this.CouponCount = null;
  }

  DeleteDeal(ReferenceData): void {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteDeal,
      text: this._HelperService.AppConfig.CommonResource.DeleteDealHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PData =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.deletedeal,
          ReferenceId: ReferenceData.ReferenceId, ReferenceKey: ReferenceData.ReferenceKey,
          AccountId: ReferenceData.AccountId, AccountKey: ReferenceData.AccountKey
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, PData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Status Updated successfully. It will take upto 5 minutes to update changes.");
              this.DealsList_Setup();
              this._HelperService.IsFormProcessing = false;
              this._HelperService.CloseModal('exampleModal')
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });

      }
    });
  }

  //deal approve and deny code

  Approve_RequestBody(ApproveReferencedata): void {
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'approvedeal',
      //'ReferenceKey': this.SelectedDeal.ReferenceKey,
      // 'ReferenceId': this.SelectedDeal.ReferenceId,
      'ReferenceId': ApproveReferencedata.ReferenceId,
      'ReferenceKey': ApproveReferencedata.ReferenceKey,

    };
    return formRequest;
  }

  ApproveReferencedata: any;
  Deal_Approve_popup(dealapprove:any) {
    this.ApproveReferencedata = dealapprove;
    this._HelperService.OpenModal('Deal_approve1');
  }

  Deal_Approve() {
    var Req = this.Approve_RequestBody(this.ApproveReferencedata);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, Req);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.ClearScheduleUpdate();
          this._HelperService.CloseModal('Deal_approve1');
          this.DealsList_GetData();
        }
        else {
          this.SelectedDeal.Schedule = [];
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.SelectedDeal.Schedule = [];
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  DenyReferencedata: any
  Deal_deny(Deal_deny) {
    this.DenyReferencedata = Deal_deny;
    this._HelperService.OpenModal('Deal_deny');
  }

  Deny_RequestBody(_FormValue): void {
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'rejectdeal',
      //'ReferenceKey': this.SelectedDeal.ReferenceKey,
      // 'ReferenceId': this.SelectedDeal.ReferenceId,
      'Comment': _FormValue.comment,
      'ReferenceId': this.DenyReferencedata.ReferenceId,
      'ReferenceKey': this.DenyReferencedata.ReferenceKey,
    };
    return formRequest;
  }

  deal_deny_confirm() {
    var _FormValue = this.Form_AddUser.value;
    var Req = this.Deny_RequestBody(_FormValue);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, Req);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.Form_AddUser_Clear();
          //this.ClearScheduleUpdate();
          this._HelperService.CloseModal('Deal_deny');
          this.DealsList_GetData();
        }
        else {
          this.SelectedDeal.Schedule = [];
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.SelectedDeal.Schedule = [];
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  Form_AddUser_Load() {
    this.Form_AddUser = this._FormBuilder.group({
      comment: [null, Validators.required],
    });
  }

  Form_AddUser_Clear() {
    this.Form_AddUser_Load();
  }

  SelectedDealEndDateS: any = "";

  ClearScheduleUpdate(): void {
    this.SelectedDeal = {};
    this.SelectedDealStartDateS = "";
    this.SelectedDealEndDateS = "";
  }

  Update_RowSelected(ReferenceData: any): void {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.EditDeal,
      ReferenceData.ReferenceKey, ReferenceData.ReferenceId,
      ReferenceData.AccountId, ReferenceData.AccountKey,
    ]);
  }

  isUpcommingState(startDate: string): boolean {
    const startDateMoment = moment(startDate).valueOf();
    const today = moment(new Date()).valueOf();
    return (startDateMoment > today) ? true : false;
  }

}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AgmCoreModule } from '@agm/core';
import { AddDealsComponent } from './tuadddeals.component';
// import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ImageCropperModule } from 'ngx-image-cropper';
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { TimepickerModule } from 'ngx-bootstrap';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 3,
};
const routes: Routes = [
  { path: '', component: AddDealsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUAddDealsRoutingModule { }
@NgModule({
  declarations: [AddDealsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    GooglePlaceModule,
    ImageCropperModule,
    CKEditorModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
    }),
    TUAddDealsRoutingModule,
    InputFileModule.forRoot(config),
    TimepickerModule.forRoot()
    // LeafletModule
  ]
})
export class AddDealsModule { }

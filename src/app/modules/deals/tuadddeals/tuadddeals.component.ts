import { Component, OnInit, ViewChild, ChangeDetectorRef, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from "../../../service/service";;
import swal from 'sweetalert2';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
declare var $: any;
import * as Feather from 'feather-icons';
declare var google: any;
declare var moment: any;
import * as cloneDeep from 'lodash/cloneDeep';
import { InputFileComponent, InputFile } from 'ngx-input-file';
import { ThrowStmt } from '@angular/compiler';
import { template } from '@angular/core/src/render3';

import { Select2OptionData } from 'ng2-select2';
@Component({
    selector: 'app-tuadddeals',
    templateUrl: './tuadddeals.component.html',
    styleUrls: ['./tuadddeals.component.css'],
})
export class AddDealsComponent implements OnInit, AfterContentInit {

    SelectedImage:any;
    jpeg:any;
    public ckConfig =
        {
            toolbar: ['Bold', 'NumberedList', 'BulletedList'],
            height: 300
        }
    public Editor = ClassicEditor;
    _ImageManager =
        {
            TCroppedImage: null,
            ActiveImage: null,
            ActiveImageName: null,
            ActiveImageSize: null,
            Option: {
                MaintainAspectRatio: "false",
                MinimumWidth: 600,
                MinimumHeight: 500,
                MaximumWidth: 600,
                MaximumHeight: 500,
                ResizeToWidth: 600,
                ResizeToHeight: 550,
                Format: "jpeg",
            }
        }
    public Hours = [];
    public Days = [];
    _DealConfig =
        {
            DefaultStartDate: null,
            DefaultEndDate: null,
            SelectedDealCodeHours: 12,
            SelectedDealCodeDays: 30,
            SelectedDealCodeEndDate: 0,
            DealCodeValidityTypeCode: 'dealenddate',
            StartDate: null,
            EndDate: null,
            DealImages: [],
            SelectedTitle: 3,
            Images: [],
            StartDateConfig: {
            },
            EndDateConfig: {
            },
            DealCodeEndDateConfig: {
            }
        }
        dealType: any = '';
        isStorePickup = true;
        isDeliverToAddress = false;
        Type_Data = ['box', "envelope", "soft-packaging"];
        Form_AddDeal: FormGroup;
        Form_AddPackage: FormGroup;
        getDealType
        dealTypeVal = ""
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {
    }



    ngOnInit(): void {

        this.getDealType = this._HelperService.GetStorage("hca").UserAccount.UserAccountTypes

        if (this.getDealType) {
            this.getDealType.forEach(element => {
                if (element.TypeCode == 'accounttype.thankucashdeals') {
                    this.dealTypeVal = element.Value
                }

            });
            if (this.getDealType.length == 1 && this.getDealType[0].TypeCode == 'accounttype.thankucashdeals') {
                this.dealType = this.getDealType[0].Value == '0' ? 'dealtype.product' : '';
            }
        }

        for (let index = 1; index < 24; index++) {
            this.Hours.push(index);
        }
        for (let index = 1; index < 366; index++) {
            this.Days.push(index);
        }
        this._DealConfig.DefaultStartDate = moment();
        this._DealConfig.DefaultEndDate = moment().add(1, 'days').endOf("day");
        this._DealConfig.StartDate = this._DealConfig.DefaultStartDate;
        this._DealConfig.EndDate = this._DealConfig.DefaultEndDate;
        this._DealConfig.StartDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            parentEl: '#adddeal_startdate',
            startDate: this._HelperService.DateInUTC(moment()),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            parentEl: '#adddeal_enddate',
            startDate: this._HelperService.DateInUTC(moment()),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        this._DealConfig.DealCodeEndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            parentEl:"#dealexpireCode_date",
            showDropdowns: true,
            startDate: this._DealConfig.EndDate,
            minDate: this._DealConfig.EndDate,
        };


        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
            this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];
            this.GetStores_List();
        });
        this.Form_AddUser_Load();
        this.Form_AddDeal_Load();
        this.GetDealCategories_List();
    }

    ngAfterContentInit() {
        $(".js-example-data-array").select2({
            placeholder: "Select a state",
            tags: [],
            data: {},
            ajax: {
                url: "https://jsonplaceholder.typicode.com/posts",
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function (params) {
                    // console.log("params", params)
                    var queryParameters = {
                        term: params.term
                    }
                    return queryParameters;
                },
                processResults: function (data) {
                    // console.log("params", data)
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.title,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });
    }


    Form_AddDeal_Load() {

        this.Form_AddDeal = this._FormBuilder.group({
            Title: [null, Validators.compose([Validators.required,])],
            Description: [null, Validators.compose([Validators.required, Validators.minLength(30), Validators.maxLength(60)])],
            Type: [null, Validators.compose([Validators.required,])],
            Height: [null, Validators.compose([Validators.required,])],
            Weight: [null, Validators.compose([Validators.required,])],
            Length: [null, Validators.compose([Validators.required,])],
            Width: [null, Validators.compose([Validators.required,])],
        });
    }

    //#region  Form Manager
    Form_AddUser_Images = [];
    Form_AddUser: FormGroup;
    Form_AddUser_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_AddUser = this._FormBuilder.group({
            SubCategoryKey: [null, Validators.required],
            TitleTypeId: ["3", Validators.required], //transient
            TitleContent: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            Description: [null, Validators.compose([Validators.required, Validators.minLength(80), Validators.maxLength(5048)])],
            Terms: [null],
            MaximumUnitSale: [null, Validators.compose([Validators.required, Validators.min(1)])],
            MaximumUnitSalePerPerson: [null, Validators.compose([Validators.required, Validators.min(0)])],
            ActualPrice: [null, Validators.compose([Validators.required, Validators.min(1)])],
            SellingPrice: [null, Validators.compose([Validators.required, Validators.min(1)])],
            StartDate: this._DealConfig.DefaultStartDate.format('DD-MM-YYYY hh:mm a'),
            EndDate: this._DealConfig.DefaultEndDate.format('DD-MM-YYYY hh:mm a'),
            CodeUsageTypeCode: "dealenddate", //transient
            TImage: null,
            CodeValidityEndDate: this._DealConfig.DefaultEndDate.format('DD-MM-YYYY hh:mm a'),
        });
    }

    isSaveinDraft:boolean=false;
    isPublishDeal:boolean=false;
    Form_AddUser_Process(_FormValue: any, IsDraft: boolean) {
        if (this.dealType == "") {
            this._HelperService.NotifyError("Select Deal type");
        }  else if (this.isDeliverToAddress && this.Form_AddDeal.value.Weight == undefined && this.Form_AddDeal.value.Weight < 0.1) {
            this._HelperService.NotifyError("Please enter weight");
        }
        else if (this.isDeliverToAddress && this.Form_AddDeal.value.Description == undefined && this.Form_AddDeal.value.Description == "") {
            this._HelperService.NotifyError("Please enter package description");
        }
        else if (!this.isStorePickup && !this.isDeliverToAddress) {
            this._HelperService.NotifyError("Select at least one Delivery Option");
        }

        else if(this._SelectedSubCategory == undefined || this._SelectedSubCategory == null || this._SelectedSubCategory.ReferenceId == undefined || this._SelectedSubCategory.ReferenceId == null || this._SelectedSubCategory.ReferenceId == 0) {
            this._HelperService.NotifyError("Select deal category");
        }
        else if (_FormValue.ActualPrice == undefined || _FormValue.ActualPrice == null || _FormValue.ActualPrice == undefined || _FormValue.ActualPrice == 0) {
            this._HelperService.NotifyError("Enter original price");
        }
        else if (_FormValue.SellingPrice == undefined || _FormValue.SellingPrice == null || _FormValue.SellingPrice == undefined || _FormValue.SellingPrice == 0) {
            this._HelperService.NotifyError("Enter selling price");
        }
        else if (_FormValue.SellingPrice <= 0) {
            this._HelperService.NotifyError("Actual price must be greater than 0");
        }
        else if (_FormValue.ActualPrice <= 0) {
            this._HelperService.NotifyError("Selling price must be greater than or equal 0");
        }
        else if (_FormValue.MaximumUnitSalePerPerson > _FormValue.MaximumUnitSale) {
            this._HelperService.NotifyError("Deals per person should be less than total deals available");
        }
        else if (parseFloat(_FormValue.SellingPrice) > parseFloat(_FormValue.ActualPrice)) {
            this._HelperService.NotifyError("Selling price must be less than or equal to actual price");
        }
        else if (this._DealConfig.DealImages == undefined || this._DealConfig.DealImages == null || this._DealConfig.DealImages.length == 0) {
            this._HelperService.NotifyError("Atleast 1 image is required for adding deal");
        }
        else if (this._DealConfig.StartDate == undefined || this._DealConfig.StartDate == null) {
            this._HelperService.NotifyError("Deal start date required");
        }
        else if (this._DealConfig.EndDate == undefined || this._DealConfig.EndDate == null) {
            this._HelperService.NotifyError("Deal end date required");
        }
        else {
            var Title = this._Titles.Title3;
            if (this._DealConfig.SelectedTitle == 1) {
                Title = this._Titles.Title1;
            }
            else if (this._DealConfig.SelectedTitle == 2) {
                Title = this._Titles.Title2;
            }
            else {
                Title = this._Titles.Title3;
            }

            // if (this._SelectedSubCategory == undefined || this._SelectedSubCategory == null || this._SelectedSubCategory.ReferenceId == undefined || this._SelectedSubCategory.ReferenceId == null || this._SelectedSubCategory.ReferenceId == 0) {
            //     this._HelperService.NotifyError("Select deal category");
            // }
            // else if (_FormValue.ActualPrice == undefined || _FormValue.ActualPrice == null || _FormValue.ActualPrice == undefined || _FormValue.ActualPrice == 0) {
            //     this._HelperService.NotifyError("Enter original price");
            // }
            // else if (_FormValue.SellingPrice == undefined || _FormValue.SellingPrice == null || _FormValue.SellingPrice == undefined || _FormValue.SellingPrice == 0) {
            //     this._HelperService.NotifyError("Enter selling price");
            // }
            // else if (_FormValue.SellingPrice <= 0) {
            //     this._HelperService.NotifyError("Actual price must be greater than 0");
            // }
            // else if (_FormValue.ActualPrice <= 0) {
            //     this._HelperService.NotifyError("Selling price must be greater than or equal 0");
            // }
            // else if (_FormValue.MaximumUnitSalePerPerson > _FormValue.MaximumUnitSale) {
            //     this._HelperService.NotifyError("Deals per person should be less than total deals available");
            // }
            // else if (_FormValue.SellingPrice > _FormValue.ActualPrice) {
            //     this._HelperService.NotifyError("Selling price must be less than or equal to actual price");
            // }
            // else if (this._DealConfig.DealImages == undefined || this._DealConfig.DealImages == null || this._DealConfig.DealImages.length == 0) {
            //     this._HelperService.NotifyError("Atleast 1 image is required for adding deal");
            // }
            // else if (this._DealConfig.StartDate == undefined || this._DealConfig.StartDate == null) {
            //     this._HelperService.NotifyError("Deal start date required");
            // }
            // else if (this._DealConfig.EndDate == undefined || this._DealConfig.EndDate == null) {
            //     this._HelperService.NotifyError("Deal end date required");
            // }
            // else {
            //     var Title = this._Titles.Title3;
            //     if (this._DealConfig.SelectedTitle == 1) {
            //         Title = this._Titles.Title1;
            //     }
            //     else if (this._DealConfig.SelectedTitle == 2) {
            //         Title = this._Titles.Title2;
            //     }
            //     else {
            //         Title = this._Titles.Title3;
            //     }
            var _PostData:any =
            {
                Task: "savedeal",
                AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
                AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
                Title: Title,
                TitleContent: _FormValue.TitleContent,
                TitleTypeId: _FormValue.TitleTypeId,
                Description: _FormValue.Description,
                Terms: _FormValue.Terms,

                StartDate: moment(this._DealConfig.StartDate).format('YYYY-MM-DD HH:mm'),
                EndDate: moment(this._DealConfig.EndDate).format('YYYY-MM-DD HH:mm'),
                CodeUsageTypeCode: _FormValue.CodeUsageTypeCode,
                CodeValidityDays: this._DealConfig.SelectedDealCodeDays,
                CodeValidityHours: this._DealConfig.SelectedDealCodeHours,
                CodeValidityEndDate: null,
                ActualPrice: _FormValue.ActualPrice,
                SellingPrice: _FormValue.SellingPrice,
                MaximumUnitSale: _FormValue.MaximumUnitSale,
                MaximumUnitSalePerPerson: _FormValue.MaximumUnitSalePerPerson,
                SubCategoryKey: this._SelectedSubCategory.ReferenceKey,
                StatusCode: 'deal.draft',
                SendNotification: false,
                Images: [],
                Locations: [],
                Shedule: [],
                DealTypeCode: this.dealType,
            };

            
            if (this.dealType === 'dealtype.product') {
                if (this.isStorePickup && this.isDeliverToAddress) {
                    _PostData.DeliveryTypeCode = 'deliverytype.instoreanddelivery'
                }
                if (this.isDeliverToAddress && !this.isStorePickup) {
                    _PostData.DeliveryTypeCode = 'deliverytype.delivery'
                }
                if (this.isStorePickup && !this.isDeliverToAddress) {
                    _PostData.DeliveryTypeCode = 'deliverytype.instore'
                }
            }

            if (this.dealType === 'dealtype.product' && this.isDeliverToAddress) {
                _PostData.Packaging = {};
                _PostData.Packaging.Name = this.Form_AddDeal.value.Description;
                _PostData.Packaging.ParcelDescription = this.Form_AddDeal.value.Description;
                _PostData.Packaging.Weight = this.Form_AddDeal.value.Weight;
            }

            if (this._DealConfig.SelectedDealCodeEndDate != null) {
                _PostData.CodeValidityEndDate = moment(this._DealConfig.SelectedDealCodeEndDate).format('YYYY-MM-DD HH:mm')
            }
            this._DealConfig.DealImages.forEach(element => {
                // element.OriginalContent = null;
                element.ImageContent.OriginalContent = null;
                _PostData.Images.push(element);
            });
            if (IsDraft) {
                _PostData.StatusCode = 'deal.draft';
            } else {
                _PostData.StatusCode = 'deal.approvalpending';
            }
            this._HelperService.IsFormProcessing = true;
            if (IsDraft){
                this.isSaveinDraft=true;
                this.isPublishDeal=false;
            }
            else
            {
                this.isPublishDeal=true;
                this.isSaveinDraft=false;
            }
            
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, _PostData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    this.isSaveinDraft=false;
                    this.isPublishDeal=false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess('Deal created successfully. It will take upto 5 minutes to update changes.');
                        this.Form_AddUser.reset();
                        this._DealConfig =
                        {
                            DefaultStartDate: null,
                            DefaultEndDate: null,
                            SelectedDealCodeHours: 12,
                            SelectedDealCodeDays: 30,
                            SelectedDealCodeEndDate: 0,
                            DealCodeValidityTypeCode: 'dealenddate',
                            StartDate: null,
                            EndDate: null,
                            DealImages: [],
                            SelectedTitle: 3,
                            Images: [],
                            StartDateConfig: {
                            },
                            EndDateConfig: {
                            },
                            DealCodeEndDateConfig: {
                            }
                        }
                        this._ImageManager =
                        {
                            TCroppedImage: null,
                            ActiveImage: null,
                            ActiveImageName: null,
                            ActiveImageSize: null,
                            Option: {
                                MaintainAspectRatio: "false",
                                MinimumWidth: 800,
                                MinimumHeight: 500,
                                MaximumWidth: 800,
                                MaximumHeight: 500,
                                ResizeToWidth: 800,
                                ResizeToHeight: 500,
                                Format: "jpg",
                            }
                        }
                        this.ngOnInit();
                        this.Form_AddUser_Close();
                    }
                    else {
                        // this._HelperService.IsFormProcessing = false;
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this.isSaveinDraft=false;
                    this.isPublishDeal=false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    Form_AddUser_Close() {
        let refkey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;
        let activemaerchantid = this._HelperService.AppConfig.ActiveMerchantReferenceId

        // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deals]);
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.MerchantDeals.MDeals + "/" + refkey + "/" + activemaerchantid]);
    }
    //#endregion
    //#region Amount Distribution
    _AmountDistribution =
        {
            ActualPrice: null,
            SellingPrice: null,
            SellingPricePercentage: null,
            SellingPriceDifference: null,
            TUCPercentage: null,
            TUCAmount: null,
            OrignalTUCPercentage: null,
            MerchantPercentage: null,
            MerchantAmount: null,
        };
    // ProcessAmounts() {
    //     // debugger;
    //     var tAmount = this._AmountDistribution;
    //     if (tAmount.ActualPrice != undefined && tAmount.ActualPrice != null && isNaN(tAmount.ActualPrice) == false && tAmount.ActualPrice >= 0) {
    //         if (tAmount.SellingPrice != undefined && tAmount.SellingPrice != null && isNaN(tAmount.SellingPrice) == false && tAmount.SellingPrice >= 0) {
    //             if (tAmount.ActualPrice >= tAmount.SellingPrice) {
    //                 tAmount.SellingPriceDifference = this._HelperService.GetFixedDecimalNumber(tAmount.ActualPrice - tAmount.SellingPrice);
    //                 tAmount.SellingPricePercentage = this._HelperService.GetPercentageFromNumber(tAmount.SellingPriceDifference, tAmount.ActualPrice);
    //                 tAmount.TUCAmount = this._HelperService.GetAmountFromPercentage(tAmount.SellingPrice, tAmount.TUCPercentage);
    //                 tAmount.MerchantAmount = this._HelperService.GetFixedDecimalNumber(tAmount.SellingPrice - tAmount.TUCAmount);
    //                 tAmount.MerchantPercentage = this._HelperService.GetPercentageFromNumber(tAmount.MerchantAmount, tAmount.SellingPrice);
    //             }
    //             else {
    //                 if (this._AmountDistribution.SellingPrice >= this._AmountDistribution.ActualPrice) {
    //                     setTimeout(() => {
    //                         this._AmountDistribution =
    //                         {
    //                             ActualPrice: this._AmountDistribution.ActualPrice,
    //                             SellingPrice: null,
    //                             SellingPricePercentage: null,
    //                             SellingPriceDifference: null,
    //                             TUCPercentage: this._AmountDistribution.TUCPercentage,
    //                             TUCAmount: null,
    //                             MerchantPercentage: null,
    //                             MerchantAmount: null,
    //                         };
    //                         tAmount.SellingPriceDifference = this._HelperService.GetFixedDecimalNumber(tAmount.ActualPrice - 0);
    //                         tAmount.SellingPricePercentage = this._HelperService.GetPercentageFromNumber(tAmount.SellingPriceDifference, tAmount.ActualPrice);
    //                         tAmount.TUCAmount = this._HelperService.GetAmountFromPercentage(0, tAmount.TUCPercentage);
    //                         tAmount.MerchantAmount = this._HelperService.GetFixedDecimalNumber(0 - tAmount.TUCAmount);
    //                         tAmount.MerchantPercentage = this._HelperService.GetPercentageFromNumber(tAmount.MerchantAmount, 0);
    //                     }, 200);
    //                 }
    //             }
    //         }
    //     }
    //     this.ProcessTitle();
    // }


    ProcessAmounts() {
        var tAmount = this._AmountDistribution;
        var SellingPrice = parseFloat(this._AmountDistribution.SellingPrice);
        var ActualPrice = parseFloat(this._AmountDistribution.ActualPrice);
        if (ActualPrice != undefined && ActualPrice != null && isNaN(ActualPrice) == false && ActualPrice >= 0) {
            if (SellingPrice != undefined && SellingPrice != null && isNaN(SellingPrice) == false && SellingPrice >= 0) {
                if (ActualPrice >= SellingPrice) {
                    tAmount.SellingPriceDifference = this._HelperService.GetFixedDecimalNumber(ActualPrice - SellingPrice);
                    tAmount.SellingPricePercentage = this._HelperService.GetPercentageFromNumber(tAmount.SellingPriceDifference, ActualPrice);
                    tAmount.TUCAmount = this._HelperService.GetAmountFromPercentage(SellingPrice, tAmount.TUCPercentage);
                    tAmount.MerchantAmount = this._HelperService.GetFixedDecimalNumber(SellingPrice - tAmount.TUCAmount);
                    tAmount.MerchantPercentage = this._HelperService.GetPercentageFromNumber(tAmount.MerchantAmount, SellingPrice);
                }
                else {
                    if (parseFloat(this._AmountDistribution.SellingPrice) >= parseFloat(this._AmountDistribution.ActualPrice)) {
                        setTimeout(() => {
                            this._AmountDistribution =
                            {
                                ActualPrice: this._AmountDistribution.ActualPrice,
                                SellingPrice: null,
                                SellingPricePercentage: null,
                                SellingPriceDifference: null,
                                OrignalTUCPercentage: this._AmountDistribution.OrignalTUCPercentage,
                                TUCPercentage: this._AmountDistribution.TUCPercentage,
                                TUCAmount: null,
                                MerchantPercentage: null,
                                MerchantAmount: null,
                            };
                            tAmount.SellingPriceDifference = this._HelperService.GetFixedDecimalNumber(ActualPrice - 0);
                            tAmount.SellingPricePercentage = this._HelperService.GetPercentageFromNumber(tAmount.SellingPriceDifference, ActualPrice);
                            tAmount.TUCAmount = this._HelperService.GetAmountFromPercentage(0, tAmount.TUCPercentage);
                            tAmount.MerchantAmount = this._HelperService.GetFixedDecimalNumber(0 - tAmount.TUCAmount);
                            tAmount.MerchantPercentage = this._HelperService.GetPercentageFromNumber(tAmount.MerchantAmount, 0);
                        }, 200);
                    }
                }
            }
        }
        this.ProcessTitle();
    }
    //#endregion

    //#region Title Manager
    _Titles =
        {
            OriginalTitle: "",
            Title1: this._HelperService.UserCountry.CurrencyNotation + " X deal title for " + this._HelperService.UserCountry.CurrencyNotation + " Y",
            Title2: this._HelperService.UserCountry.CurrencyNotation + " Y off on deal title",
            Title3: "x% off on deal title"
        }
    ProcessTitle() {
        // if (this._AmountDistribution.ActualPrice == undefined) {
        //     this._AmountDistribution.ActualPrice = 0;
        // }
        // if (this._AmountDistribution.SellingPrice == undefined) {
        //     this._AmountDistribution.SellingPrice = 0;
        // }
        // if (this._AmountDistribution.SellingPricePercentage == undefined) {
        //     this._AmountDistribution.SellingPricePercentage = 0;
        // }
        // if (this._AmountDistribution.SellingPriceDifference == undefined) {
        //     this._AmountDistribution.SellingPriceDifference = 0;
        // }
        // if (this._AmountDistribution.TUCPercentage == undefined) {
        //     this._AmountDistribution.TUCPercentage = 0;
        // }
        // if (this._AmountDistribution.TUCAmount == undefined) {
        //     this._AmountDistribution.TUCAmount = 0;
        // }
        // if (this._AmountDistribution.MerchantPercentage == undefined) {
        //     this._AmountDistribution.MerchantPercentage = 0;
        // }
        // if (this._AmountDistribution.MerchantAmount == undefined) {
        //     this._AmountDistribution.MerchantAmount = 0;
        // }

        this._Titles.Title1 = this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.ActualPrice) + " " + this._Titles.OriginalTitle + " for " + this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.SellingPrice);
        this._Titles.Title2 = this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.SellingPriceDifference) + " off on " + this._Titles.OriginalTitle;
        this._Titles.Title3 = Math.round(this._AmountDistribution.SellingPricePercentage) + "% off on " + this._Titles.OriginalTitle;


        // if (this._Titles.OriginalTitle != undefined && this._Titles.OriginalTitle != null && this._Titles.OriginalTitle != "") {
        //     this._Titles.Title1 = this._HelperService.UserCountry.CurrencyCode + " " + this._AmountDistribution.ActualPrice + " " + this._Titles.OriginalTitle + " for " + this._HelperService.UserCountry.CurrencyCode + " " + this._AmountDistribution.SellingPrice;
        //     this._Titles.Title2 = this._HelperService.UserCountry.CurrencyCode + " " + this._AmountDistribution.SellingPriceDifference + " off on " + this._Titles.OriginalTitle;
        //     this._Titles.Title3 = this._AmountDistribution.SellingPricePercentage + "% off on " + this._Titles.OriginalTitle;
        // }
        // else {
        //     // this._Titles.Title1 = this._HelperService.UserCountry.CurrencyCode + " X deal title for " + this._HelperService.UserCountry.CurrencyCode + "Y";
        //     // this._Titles.Title2 = this._HelperService.UserCountry.CurrencyCode + " Y off on deal title";
        //     // this._Titles.Title3 = "x% off on deal title";
        // }
    }
    //#endregion
    //#region Deal Shedule Manager 
    ScheduleStartDateRangeChange(value) {
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: value.start,
            minDate: value.start,
        };
        this._DealConfig.StartDate = value.start;
        this._DealConfig.EndDate = value.start;
        this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                StartDate: value.start.format('DD-MM-YYYY hh:mm a'),
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    ScheduleEndDateRangeChange(value) {
        this._DealConfig.DealCodeEndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            parentEl:'#dealexpireCode_date',
            startDate: value.start,
            minDate: value.start,
        };
        this._DealConfig.EndDate = value.start;
        this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    ScheduleCodeEndDateRangeChange(value) {
        this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    Icon_Crop_Loaded() {
        this._HelperService.OpenModal('_Icon_Cropper_Modal_Deal');
    }
    //#endregion

    //#region Image Manager
    onImageAccept(value) {
        setTimeout(() => {
            this.Form_AddUser.patchValue(
                {
                    TImage: null,
                }
            );
        }, 300);
        this._ImageManager.ActiveImage = value;
        this._ImageManager.ActiveImageName = value.file.name;
        this._ImageManager.ActiveImageName = value.file.size;
    }
    onImageRejected(value) {
    }
    Icon_B64Cropped(base64: string) {
        this._ImageManager.TCroppedImage = base64;
    }
    Icon_B64CroppedDone() {
        var ImageDetails = this._HelperService.GetImageDetails(this._ImageManager.TCroppedImage);
        var ImageItem =
        {
            OriginalContent: this._ImageManager.TCroppedImage,
            Name: this._ImageManager.ActiveImageName,
            Size: this._ImageManager.ActiveImageSize,
            Extension: ImageDetails.Extension,
            Content: ImageDetails.Content
        };
        if (this._DealConfig.DealImages.length == 0) {
            this._DealConfig.DealImages.push(
                {
                    ImageContent: ImageItem,
                    IsDefault: 1,
                }
            );
        }
        else {
            this._DealConfig.DealImages.push(
                {
                    ImageContent: ImageItem,
                    IsDefault: 0,
                }
            );
        }
        this._ImageManager.TCroppedImage = null;
        this._ImageManager.ActiveImage = null;
        this._ImageManager.ActiveImageName = null;
        this._ImageManager.ActiveImageSize = null;
    }
    Icon_Crop_Clear() {
        this._ImageManager.TCroppedImage = null;
        this._ImageManager.ActiveImage = null;
        this._ImageManager.ActiveImageName = null;
        this._ImageManager.ActiveImageSize = null;
        this._HelperService.CloseModal('_Icon_Cropper_Modal_Deal');
    }
    RemoveImage(Item) {
        this._DealConfig.DealImages = this._DealConfig.DealImages.filter(x => x != Item);
    }
    //#endregion

    //#region   List Helpers
    public _S2Categories_Data: Array<Select2OptionData>;
    public options = { placeholder: { id: '', text: 'Select Category' } }
    public selectedCategory :any ="Selected Category" 
    GetDealCategories_List() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.getallcategories,
            Offset: 0,
            Limit: 1000,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System2, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    var _Data = _Response.Result.Data as any[];
                    if (_Data.length > 0) {
                        var parentCategories = [];
                        _Data.forEach(element => {
                            var itemIndex = parentCategories.findIndex(x => x.ParentCategoryId == element.ParentCategoryId);
                            if (itemIndex == -1) {
                                var categories = _Data.filter(x => x.ParentCategoryId == element.ParentCategoryId);
                                parentCategories.push(
                                    {
                                        ParentCategoryId: element.ParentCategoryId,
                                        ParentCategoryKey: element.ParentCategoryKey,
                                        ParentCategoryName: element.ParentCategoryName,
                                        Categories: categories,
                                    }
                                );
                            }
                        });
                        var finalCat = [];
                        finalCat.push(
                            {
                                id: "-1",
                                text: "Select category",
                                disabled:true,
                            }
                        )

                        parentCategories.forEach(element => {
                            var Item = {
                                id: element.ParentCategoryId,
                                text: element.ParentCategoryName,
                                children: [],
                                additional: element,
                            };
                            element.Categories.forEach(CategoryItem => {
                                var cItem = {
                                    id: CategoryItem.ReferenceId,
                                    text: CategoryItem.Name + " (" + CategoryItem.Fees + "%)",
                                    additional: CategoryItem,
                                    disabled:false,
                                };
                                Item.children.push(cItem);
                            });
                            finalCat.push(Item);
                        });
                        this._S2Categories_Data = finalCat;
                        this.selectedCategory = this._S2Categories_Data[0];
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    _SelectedSubCategory =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Fee: 5,
            Name: null,
            IconUrl: null,

            ParentCategoryId: 0,
            ParentCategoryKey: 0,
            ParentCategoryName: 0
        };
    GetDealCategories_Selected(event: any) {
        if (event.value != "0" && event.data.length > 0) {
            this._SelectedSubCategory.ReferenceId = event.data[0].additional.ReferenceId;
            this._SelectedSubCategory.ReferenceKey = event.data[0].additional.ReferenceKey;
            this._SelectedSubCategory.Name = event.data[0].additional.Name;
            this._SelectedSubCategory.IconUrl = event.data[0].additional.IconUrl;
            this._SelectedSubCategory.Fee = event.data[0].additional.Fees;
            this._AmountDistribution.TUCPercentage = event.data[0].additional.Fees;
            this._SelectedSubCategory.ParentCategoryId = event.data[0].additional.ParentCategoryId;
            this._SelectedSubCategory.ParentCategoryKey = event.data[0].additional.ParentCategoryKey;
            this._SelectedSubCategory.ParentCategoryName = event.data[0].additional.ParentCategoryName;
            this.Form_AddUser.patchValue(
                {
                    SubCategoryKey: this._SelectedSubCategory.ParentCategoryKey
                }
            );
            this.ProcessAmounts();
        }
        // else if (event.value == "0") {
        //     this._HelperService.NotifyError("select catogeory is not applicable");
        // }


    }

    // public GetMerchants_Option: Select2Options;
    // public GetMerchants_Transport: any;
    // public SelectedMerchant: any = {};
    GetStores_List() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
            Offset: 0,
            Limit: 1000,
            SearchCondition: this._HelperService.GetSearchCondition("", "MerchantReferenceId", "number", this._HelperService.AppConfig.ActiveMerchantReferenceId),
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    //#endregion
}

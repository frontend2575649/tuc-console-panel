import { ChangeDetectorRef, Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BaseChartDirective, Label } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OList, OResponse, OSelect } from '../../../service/service';
declare var moment: any;
declare var $: any;
import * as pluginEmptyOverlay from "chartjs-plugin-empty-overlay";
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import swal from 'sweetalert2';
import * as Feather from "feather-icons";

@Component({
  selector: 'tudeal',
  templateUrl: './tudeal.component.html',
  styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUDealComponent implements OnInit {
  showDailyChart: boolean = true;
  Piechartlable = ['Available Deals', 'Sold Deals', 'Redeemed Deals'];
  public TodayDate: any;
  Piedata = ['0', '0', '0']
  public dougnut: any = {
    type: 'doughnut',
    data: {
      labels: ["Red", "Orange", "Green"],
      datasets: [{
        label: '# of Votes',
        data: [33, 33, 33],
        backgroundColor: [
          'rgba(231, 76, 60, 1)',
          'rgba(255, 164, 46, 1)',
          'rgba(46, 204, 113, 1)'
        ],
        borderColor: [
          'rgba(255, 255, 255 ,1)',
          'rgba(255, 255, 255 ,1)',
          'rgba(255, 255, 255 ,1)'
        ],
        borderWidth: 5
      }]

    },
    options: {
      rotation: 1 * Math.PI,
      circumference: 1 * Math.PI,
      legend: {
        display: false
      },
      tooltip: {
        enabled: false
      },
      cutoutPercentage: 95
    }
  }
  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
  }

  ngOnInit() {
    this.Form_AddUser_Load();
    this.TodayStartTime = this._HelperService.AppConfig.DefaultStartTimeAll;
    this.TodayEndTime = this._HelperService.AppConfig.DefaultEndTimeToday;
    Feather.replace();

    setTimeout(() => {
      this._HelperService.ValidateData();

    }, 500);

    this._HelperService.FullContainer = false;
    this._HelperService.FullContainer = false;
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this.pData.ReferenceKey = params["referencekey"];
      // this.pReviewData.ReferenceKey = params["referencekey"];
      this.pDealPurchaseData.ReferenceKey = params["referencekey"];

      this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
      this.pData.ReferenceId = params["referenceid"];
      // this.pReviewData.ReferenceId = params["referenceid"];
      this.pDealPurchaseData.ReferenceId = params["referenceid"];


      this._HelperService.AppConfig.ActiveAccountKey = params["accountkey"];
      this.pData.AccountKey = params["accountkey"];
      // this.pReviewData.AccountKey = params["accountkey"];
      this.pDealPurchaseData.AccountKey = params["accountkey"];


      this._HelperService.AppConfig.ActiveAccountId = params["accountid"];
      this.pData.AccountId = params["accountid"];
      // this.pReviewData.AccountId = params["accountid"];
      this.pDealPurchaseData.AccountId = params["accountid"];

      if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
        this._Router.navigate([
          this._HelperService.AppConfig.Pages.System.NotFound
        ]);
      } else {
        this._HelperService.ResetDateRange();
        this.GetAccountDetails();
        this.PurchaseHistory_Setup();
        this.GetSalesOverview();
        this.ReviewHistory_Setup();
      }
    });
  }


  public _UserAccount: any =
    {
      MerchantDisplayName: null,
      SecondaryEmailAddress: null,
      BankDisplayName: null,
      BankKey: null,
      OwnerName: null,
      SubOwnerAddress: null,
      SubOwnerLatitude: null,
      SubOwnerDisplayName: null,
      SubOwnerKey: null,
      SubOwnerLongitude: null,
      AccessPin: null,
      LastLoginDateS: null,
      AppKey: null,
      AppName: null,
      AppVersionKey: null,
      CreateDate: null,
      CreateDateS: null,
      CreatedByDisplayName: null,
      CreatedByIconUrl: null,
      CreatedByKey: null,
      Description: null,
      IconUrl: null,
      ModifyByDisplayName: null,
      ModifyByIconUrl: null,
      ModifyByKey: null,
      ModifyDate: null,
      ModifyDateS: null,
      PosterUrl: null,
      ReferenceKey: null,
      StatusCode: null,
      StatusI: null,
      StatusId: null,
      StatusName: null,
      AccountCode: null,
      AccountOperationTypeCode: null,
      AccountOperationTypeName: null,
      AccountTypeCode: null,
      AccountTypeName: null,
      Address: null,
      AppVersionName: null,
      ApplicationStatusCode: null,
      ApplicationStatusName: null,
      AverageValue: null,
      CityAreaKey: null,
      CityAreaName: null,
      CityKey: null,
      CityName: null,
      ContactNumber: null,
      CountValue: null,
      CountryKey: null,
      CountryName: null,
      DateOfBirth: null,
      DisplayName: null,
      EmailAddress: null,
      EmailVerificationStatus: null,
      EmailVerificationStatusDate: null,
      FirstName: null,
      GenderCode: null,
      GenderName: null,
      LastLoginDate: null,
      LastName: null,
      Latitude: null,
      Longitude: null,
      MobileNumber: null,
      Name: null,
      NumberVerificationStatus: null,
      NumberVerificationStatusDate: null,
      OwnerDisplayName: null,
      OwnerKey: null,
      Password: null,
      Reference: null,
      ReferralCode: null,
      ReferralUrl: null,
      RegionAreaKey: null,
      RegionAreaName: null,
      RegionKey: null,
      RegionName: null,
      RegistrationSourceCode: null,
      RegistrationSourceName: null,
      RequestKey: null,
      RoleKey: null,
      RoleName: null,
      SecondaryPassword: null,
      SystemPassword: null,
      UserName: null,
      WebsiteUrl: null,

      StateKey: null,
      StateName: null

    }
  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
    //    this._ChangeDetectorRef.detectChanges();
  }
  public _Address: any = {};
  public _ContactPerson: any = {};
  timeRemaining = "--";
  isTimeLessThanDay: boolean = false;
  timerConfig: any = { leftTime: 30 };

  VarAccordion: boolean = false;
  VarAccordion1: boolean = false;
  VarAccordion1F() {
    this.VarAccordion1 = !this.VarAccordion1;

  }

  VarAccordionF() {
    this.VarAccordion = !this.VarAccordion;

  }
  ShowAllLocations(): void {
    this._HelperService.OpenModal("AllLocation");
  }
  GetAccountDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetDeal,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveAccountId,
      AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
      // Reference: this._HelperService.GetSearchConditionStrict(
      //   "",
      //   "ReferenceKey",
      //   this._HelperService.AppConfig.DataType.Text,
      //   this._HelperService.AppConfig.ActiveReferenceKey,
      //   "="
      // ),
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.toogleIsFormProcessing(false);
          this._UserAccount = _Response.Result;
          this._Address = this._UserAccount.Address;
          this._ContactPerson = this._UserAccount.ContactPerson;
          let stillUtc = moment.utc(this._UserAccount.EndDate).toDate();
          let local = moment(stillUtc).local().format('YYYY-MM-DD HH:mm:ss');
          var difference = moment(local).diff(moment(), 'hours');

          //#region RelocateMarker 

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }

          if (difference < 0) {
            this.timeRemaining = "Deal No Longer Available";
            this.isTimeLessThanDay = false;
          } else if (difference >= 0 && difference <= 24) {
            this.isTimeLessThanDay = true;
            this.timerConfig.leftTime= moment(local).diff(moment(), 'seconds');
          } else if (difference > 24 && difference >= 48) {
            this.timeRemaining = Math.round(difference / 24) + " Day Remaining";
            this.isTimeLessThanDay = false;
          } else {
            this.timeRemaining = Math.round(difference / 24) + " Days Remaining";
            this.isTimeLessThanDay = false;
          }
          // this.FormC_EditUser_Latitude = this._UserAccount.Latitude;
          // this.FormC_EditUser_Longitude = this._UserAccount.Longitude;

          // this._HelperService._ReLocate();

          //#endregion

          //#region DatesAndStatusInit 
          this._UserAccount.CodeValidityStartDate = this._HelperService.GetDateS(
            this._UserAccount.CodeValidityStartDate
          );
          this._UserAccount.CodeValidityEndDate = this._HelperService.GetDateS(
            this._UserAccount.CodeValidityEndDate
          );
          this._UserAccount.StartDateS = this._HelperService.GetDateS(
            this._UserAccount.StartDate
          );
          this._UserAccount.EndDateS = this._HelperService.GetDateS(
            this._UserAccount.EndDate
          );
          this._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(
            this._UserAccount.CreateDate
          );
          this._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(
            this._UserAccount.ModifyDate
          );
          this._UserAccount.StatusI = this._HelperService.GetStatusIcon(
            this._UserAccount.StatusCode
          );
          this._UserAccount.StatusB = this._HelperService.GetStatusBadge(
            this._UserAccount.StatusCode
          );
          this._UserAccount.StatusC = this._HelperService.GetStatusColor(
            this._UserAccount.StatusCode
          );
          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }



  public PurchaseHistory_Config: OList;
  PurchaseHistory_Setup() {
    this.PurchaseHistory_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetPurchaseHistory,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals,
      Title: "Purchase History",
      StatusType: "default",
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveAccountId,
      AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: " Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Contact No",
          SystemName: "ContactNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Terminals",
          SystemName: "Terminals",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Cashiers",
          SystemName: "Cashiers",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
          //   .PanelAcquirer.Merchant.Dashboard,
        }, {
          DisplayName: "Manager",
          SystemName: "ManagerName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.PurchaseHistory_Config = this._DataHelperService.List_Initialize(
      this.PurchaseHistory_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Stores,
      this.PurchaseHistory_Config
    );

    this.PurchaseHistory_GetData();
  }
  PurchaseHistory_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.PurchaseHistory_Config.Sort.SortOptions.length; index++) {
        const element = this.PurchaseHistory_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.PurchaseHistory_Config
    );

    this.PurchaseHistory_Config = this._DataHelperService.List_Operations(
      this.PurchaseHistory_Config,
      event,
      Type
    );

    if (
      (this.PurchaseHistory_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.PurchaseHistory_GetData();
    }

  }
  timeout = null;
  PurchaseHistory_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.PurchaseHistory_Config.Sort.SortOptions.length; index++) {
          const element = this.PurchaseHistory_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.PurchaseHistory_Config
      );

      this.PurchaseHistory_Config = this._DataHelperService.List_Operations(
        this.PurchaseHistory_Config,
        event,
        Type
      );

      if (
        (this.PurchaseHistory_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.PurchaseHistory_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);
  }
  PurchaseHistory_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.PurchaseHistory_Config
    );
    this.PurchaseHistory_Config = TConfig;
  }
  PurchaseHistory_RowSelected(ReferenceData) {
    // this._HelperService.SaveStorage(
    //   this._HelperService.AppConfig.Storage.ActiveDeal,
    //   {
    //     ReferenceKey: ReferenceData.ReferenceKey,
    //     ReferenceId: ReferenceData.ReferenceId,
    //     DisplayName: ReferenceData.DisplayName,
    //     AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
    //   }
    // );

    // this._HelperService.AppConfig.ActiveReferenceKey =
    //   ReferenceData.ReferenceKey;
    // this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
    // this._HelperService.AppConfig.ActiveAccountKey =
    //   ReferenceData.ReferenceKey;
    // this._HelperService.AppConfig.ActiveAccountId = ReferenceData.ReferenceId;

    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deal,
    //   ReferenceData.ReferenceKey,
    //   ReferenceData.ReferenceId,
    //   ReferenceData.AccountId,
    //   ReferenceData.AccountKey,

    // ]);


  }

  public _GetoverviewSummary: any = {};
  public TodayStartTime = null;
  public TodayEndTime = null;
  private pData = {
    Task: 'getdealsoverview',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
    ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
    AccountId: this._HelperService.AppConfig.ActiveAccountId,
    AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetSalesOverview() {
    this.showDailyChart = false;
    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetoverviewSummary = _Response.Result as any;
          // this.Piedata[0] = this._GetoverviewSummary.Total;
          this.showDailyChart = true;
          this._ChangeDetectorRef.detectChanges();
          this.GetDealPurchaseOverview();

          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  public _GetPurchaseoverviewSummary: any = {};
  TotalCountDeal: any = null;
  private pDealPurchaseData = {
    Task: 'getdealspurchaseoverview',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
    ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
    AccountId: this._HelperService.AppConfig.ActiveAccountId,
    AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetDealPurchaseOverview() {
    this.showDailyChart = false;
    this._ChangeDetectorRef.detectChanges();

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, this.pDealPurchaseData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetPurchaseoverviewSummary = _Response.Result as any;
          this.Piedata[1] = this._GetPurchaseoverviewSummary.Total;
          this.Piedata[2] = this._GetPurchaseoverviewSummary.Used;
          this.TotalCountDeal = this._GetPurchaseoverviewSummary.MaxLimit - this._GetPurchaseoverviewSummary.Total
          this.Piedata[0] = this.TotalCountDeal;

          this.showDailyChart = true;
          this._ChangeDetectorRef.detectChanges();

          return;

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  public ReviewHistory_Config: OList;
  ReviewHistory_Setup() {
    this.ReviewHistory_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.getdealreviews,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals,
      Title: "Purchase History",
      StatusType: "default",
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveAccountId,
      AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: " Name",
          SystemName: "AccountDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Contact No",
          SystemName: "AccountMobileNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Rating",
          SystemName: "Rating",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Review",
          SystemName: "Review",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "IsWorking",
          SystemName: "IsWorking",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
          //   .PanelAcquirer.Merchant.Dashboard,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.ReviewHistory_Config = this._DataHelperService.List_Initialize(
      this.ReviewHistory_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Stores,
      this.ReviewHistory_Config
    );

    this.ReviewHistory_GetData();
  }
  ReviewHistory_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.ReviewHistory_Config.Sort.SortOptions.length; index++) {
        const element = this.ReviewHistory_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.ReviewHistory_Config
    );

    this.ReviewHistory_Config = this._DataHelperService.List_Operations(
      this.ReviewHistory_Config,
      event,
      Type
    );

    if (
      (this.ReviewHistory_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.ReviewHistory_GetData();
    }

  }
  ReviewHistory_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.ReviewHistory_Config.Sort.SortOptions.length; index++) {
          const element = this.ReviewHistory_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.ReviewHistory_Config
      );

      this.ReviewHistory_Config = this._DataHelperService.List_Operations(
        this.ReviewHistory_Config,
        event,
        Type
      );

      if (
        (this.ReviewHistory_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.ReviewHistory_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);
  }
  ReviewHistory_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.ReviewHistory_Config
    );
    this.ReviewHistory_Config = TConfig;
  }
  // ReviewHistory_RowSelected(ReferenceData) {
  //    this._HelperService.SaveStorage(
  //      this._HelperService.AppConfig.Storage.ActiveDeal,
  //      {
  //        ReferenceKey: ReferenceData.ReferenceKey,
  //        ReferenceId: ReferenceData.ReferenceId,
  //        DisplayName: ReferenceData.DisplayName,
  //        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
  //      }
  //    );

  //    this._HelperService.AppConfig.ActiveReferenceKey =
  //      ReferenceData.ReferenceKey;
  //    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
  //    this._HelperService.AppConfig.ActiveAccountKey =
  //      ReferenceData.ReferenceKey;
  //    this._HelperService.AppConfig.ActiveAccountId = ReferenceData.ReferenceId;


  //    this._Router.navigate([
  //      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deal,
  //      ReferenceData.ReferenceKey,
  //      ReferenceData.ReferenceId,
  //      ReferenceData.AccountId,
  //      ReferenceData.AccountKey,

  //    ]);



  //  }
  UpdateStatusArray: any = ['deal.draft', 'deal.approvalpending', 'deal.approved', 'deal.published', 'deal.paused', 'deal.expired',];
  StatusUpdate(ReferenceData, i) {
    swal({
      title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
      text: this._HelperService.AppConfig.CommonResource.ContinueUpdateHelp,
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: true,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      // input: 'password',
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      // inputAttributes: {
      //   autocapitalize: 'off',
      //   autocorrect: 'off',
      //   maxLength: "4",
      //   minLength: "4"
      // },

      // inputValidator: function (value) {
      //   if (value === '' || value.length < 4) {
      //     return 'Enter your 4 digit pin!'
      //   }
      // },

    }).then((result) => {
      if (result.value) {
        //   if (result.value.length < 4) {
        //     this._HelperService.NotifyError('Pin length must be 4 digits');
        //     return;
        // }

        this._HelperService.IsFormProcessing = true;
        var P1Data =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.updatedealstatus,
          ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
          ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
          AccountId: this._HelperService.AppConfig.ActiveAccountId,
          AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
          StatusCode: this.UpdateStatusArray[i],
          // AuthPin: result.value,
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, P1Data);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Status Updated successfully. It will take upto 5 minutes to update changes.");
              this.GetAccountDetails();
              this._HelperService.IsFormProcessing = false;
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });

      }
    });



  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.ReviewHistory_GetData();

    if (ButtonType == 'Sort') {
      $("#DealsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#DealsList_fdropdown").dropdown('toggle');
    }
    this._HelperService.StopClickPropogation();
    // this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //deal approve and deny code

  Approve_RequestBody(ApproveReferencedata): void {
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'approvedeal',
      //'ReferenceKey': this.SelectedDeal.ReferenceKey,
      // 'ReferenceId': this.SelectedDeal.ReferenceId,

      'ReferenceId': this._UserAccount.ReferenceId,
      'ReferenceKey': this._UserAccount.ReferenceKey,

    };
    //#region Set Schedule 

    //#endregion

    return formRequest;

  }

  ApproveReferencedata: any;
  Deal_Approve_popup() {

    this._HelperService.OpenModal('Deal_approve1');
  }

  Deal_Approve() {
    var Req = this.Approve_RequestBody(this.ApproveReferencedata);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, Req);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);

          this.ClearScheduleUpdate();
          this._HelperService.CloseModal('Deal_approve1');
          this.GetAccountDetails();
        }
        else {
          this.SelectedDeal.Schedule = [];
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.SelectedDeal.Schedule = [];
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });

  }

  DenyReferencedata: any
  Deal_deny() {
    // this.DenyReferencedata=Deal_deny;
    this._HelperService.OpenModal('Deal_deny');
  }


  Deny_RequestBody(_FormValue): void {
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'rejectdeal',
      //'ReferenceKey': this.SelectedDeal.ReferenceKey,
      // 'ReferenceId': this.SelectedDeal.ReferenceId,
      'Comment': _FormValue.comment,
      'ReferenceId': this._UserAccount.ReferenceId,
      'ReferenceKey': this._UserAccount.ReferenceKey,

    };
    //#region Set Schedule 

    //#endregion

    return formRequest;

  }

  deal_deny_confirm() {
    var _FormValue = this.Form_AddUser.value;
    var Req = this.Deny_RequestBody(_FormValue);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, Req);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.Form_AddUser_Clear();
          //this.ClearScheduleUpdate();
          this._HelperService.CloseModal('Deal_deny');
          this.GetAccountDetails();
        }
        else {
          this.SelectedDeal.Schedule = [];
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.SelectedDeal.Schedule = [];
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  Form_AddUser: FormGroup;
  Form_AddUser_Load() {


    this.Form_AddUser = this._FormBuilder.group({

      comment: [null, Validators.required],

    });
  }

  Form_AddUser_Clear() {
    this.Form_AddUser_Load();
  }

  SelectedDeal: any = {};
  SelectedDealStartDateS: any = "";
  SelectedDealEndDateS: any = "";

  ClearScheduleUpdate(): void {
    this.SelectedDeal = {};
    this.SelectedDealStartDateS = "";
    this.SelectedDealEndDateS = "";
  }

  //duplicate deal
  DuplicateDeal(): void {
    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.DuplicateDeal,
    //   this._UserAccount.ReferenceKey,
    //   this._UserAccount.ReferenceId,
    //   this._UserAccount.AccountId,
    //   this._UserAccount.AccountKey,
    // ]);
    swal({
      title: "Duplicate Deal ?",
      text: "Click on continue button to duplicate deal",
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;

        var P1Data =
        {
          Task: this._HelperService.AppConfig.Api.ThankUCash.DuplicateDeal,
          ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
          ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
          AccountId: this._HelperService.AppConfig.ActiveAccountId,
          AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Deals, P1Data);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess(_Response.Message);
              this._Router.navigate([
                this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.EditDeal,
                _Response.Result.ReferenceKey,
                _Response.Result.ReferenceId,
                this._HelperService.AppConfig.ActiveAccountId,
                this._HelperService.AppConfig.ActiveAccountKey,
              ]);

              //this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.MerchantDeals.MDeals
              // this._HelperService.NotifySuccess("Notification Sent successfully");
              // this._HelperService.NotifEventEmit.emit(0);
              // this._HelperService.IsFormProcessing = false;
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          }
          ,
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;
          });
      }
    });
  }
  //end of duplicate deal


  tabSwitched(arg1: any): void { }
  ResetFilters(arg1: any, arg2: any): void { }
  OpenEditColModal(): void { }
  showMore = false;

}


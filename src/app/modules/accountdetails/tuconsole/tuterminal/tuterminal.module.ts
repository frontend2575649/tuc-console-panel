import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { TUTerminalComponent} from "./tuterminal.component";
import { DynamicRoutesguardGuard } from "src/app/service/guard/dynamicroutes.guard";

const routes: Routes = [
    {
        path: "",
        component:TUTerminalComponent ,
        canActivate:[DynamicRoutesguardGuard],
        data:{accessName:['terminalview']},
        children: [
            // { path: "/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../dashboards/terminal/dashboard.module#TUDashboardModule" },
            // { path: "dashboard/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../dashboards/terminal/dashboard.module#TUDashboardModule" },
            { path: '/:referencekey/:referenceid/:merchantkey/:merchantid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Terminal' }, loadChildren: '../../../../modules/transactions/tusale/terminal/merchant/tusale.module#TUSaleModule' },          
            { path: 'sales/saleshistory/:referencekey/:referenceid/:merchantkey/:merchantid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Terminal' }, loadChildren: '../../../../modules/transactions/tusale/terminal/merchant/tusale.module#TUSaleModule' },
            // { path: 'terminals/:referencekey/:referenceid', data: { 'permission': 'terminals', PageName: 'System.Menu.Terminal' }, loadChildren: '../../../../modules/accounts/tuterminals/store/tuterminals.module#TUTerminalsModule' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUTerminalRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUTerminalRoutingModule,
        GooglePlaceModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    declarations: [TUTerminalComponent]
})
export class TUTerminalModule { }

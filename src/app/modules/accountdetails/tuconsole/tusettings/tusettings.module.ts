import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { ImageCropperModule } from 'ngx-image-cropper';
import { TUSettingsComponent } from "./tusettings.component";
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { MainPipe } from '../../../../service/main-pipe.module';
import { InputFileConfig, InputFileModule } from 'ngx-input-file';

const config: InputFileConfig = {
    fileAccept: '*',
    fileLimit: 1,
};

const routes: Routes = [
    {
        path: "",
        component: TUSettingsComponent,
        children: [
            { path: '', data: { permission: "getmerchantdetails", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: '../../../accounts/tuconsole/stores/tustores.module#TUStoresModule' },
            { path: 'cashiers', data: { 'permission': 'cashiers', PageName: 'System.Menu.Settings' }, loadChildren: '../../../accounts/tuconsole/cashiers/tucashiers.module#TUCashiersModule' },
            { path: 'stores', data: { 'permission': 'stores', PageName: 'System.Menu.Settings' }, loadChildren: '../../../accounts/tuconsole/stores/tustores.module#TUStoresModule' },
            { path: 'terminals', data: { 'permission': 'cashiers', PageName: 'System.Menu.Settings' }, loadChildren: '../../../accounts/tuconsole/tuterminals/merchant/tuterminals.module#TUTerminalsModule' },
            { path: 'rewardpercentage', data: { 'permission': 'cashiers', PageName: 'System.Menu.Settings' }, loadChildren: '../../../accounts/tuconsole/rewardpercentage/turewardpercentage.module#TURewardPercentageModule' },
            { path: 'subaccounts', data: { 'permission': 'cashiers', PageName: 'System.Menu.Settings' }, loadChildren: '../../../accounts/tuconsole/subaccounts/tusubaccounts.module#TUSubAccountsModule' },
            { path: 'topup', data: { 'permission': 'topup', PageName: 'System.Menu.Settings' }, loadChildren: '../../../accounts/tuconsole/topup/topup.module#TUTopUpModule' },
            { path: 'wallate', data: { 'permission': 'topup', PageName: 'System.Menu.Settings' }, loadChildren: '../../../accounts/tuconsole/wallate/wallate.module#TUWallateModule' }
            // { path: 'cashout', data: { 'permission': 'cashout', PageName: 'System.Menu.Settings' }, loadChildren: '../../../accounts/tuconsole/topup/topup.module#TUTopUpModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUSettingRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUSettingRoutingModule,
        GooglePlaceModule,
        MainPipe,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        ImageCropperModule,
        InputFileModule.forRoot(config),

    ],
    declarations: [TUSettingsComponent]
})
export class TUSettingsModule { }

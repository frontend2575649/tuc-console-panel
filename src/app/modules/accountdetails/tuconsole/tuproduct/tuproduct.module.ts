import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { ImageCropperModule } from "ngx-image-cropper";
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { TUProductComponent } from "./tuproduct.component";
import { InputFileConfig, InputFileModule } from 'ngx-input-file';

const config: InputFileConfig = {
    fileAccept: '*',
    fileLimit: 1,
};

const routes: Routes = [
    {
        path: "",
        component: TUProductComponent,
        children: [
            { path: '', data: { 'permission': 'salehistory', PageName: 'System.Menu.Store' }, loadChildren: '../../../../panel/console/tucmall/tuvariants/tuvariants.module#TUVariantsModule' },
            // { path: 'inventory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Product' }, loadChildren: '../../../../panel/console/tucmall/tuinventory/tuinventory.module#TUInventoryModule' },
            // { path: 'dashboard/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Product' }, loadChildren: '../../../../panel/console/tucmall/tuproductoverview/dashboard.module#TUDashboardModule' },
            { path: 'variants/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Product' }, loadChildren: '../../../../panel/console/tucmall/tuvariants/tuvariants.module#TUVariantsModule' },
           // { path: 'ambassador/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Product' }, loadChildren: '../../../../panel/console/tucmall/tuambassador/tuambassador.module#TUAmbassadorModule' },

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUProductRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUProductRoutingModule,
        GooglePlaceModule,
        ImageCropperModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        InputFileModule.forRoot(config),
    ],
    declarations: [TUProductComponent]
})
export class TUProductModule { }

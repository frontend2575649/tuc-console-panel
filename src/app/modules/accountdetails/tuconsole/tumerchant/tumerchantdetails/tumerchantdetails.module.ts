import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { TUMerchantDetailsComponent } from "./tumerchantdetails.component";
import { Angular4PaystackModule } from 'angular4-paystack'
import { DynamicRoutesguardGuard } from "src/app/service/guard/dynamicroutes.guard";
import { ImageCropperModule } from 'ngx-image-cropper';
import { InputFileConfig, InputFileModule } from 'ngx-input-file';

const routes: Routes = [
    {
        path: "",
        component: TUMerchantDetailsComponent,
        canActivate:[DynamicRoutesguardGuard],
        data:{accessName:['viewmerchant']},
        children: [
            { path: "", data: { permission: "customer", menuoperations: "ManageMerchant", accounttypecode: "customer" }, loadChildren: "../../../../accounts/tuconsole/topup/topup.module#TUTopUpModule" },
            { path: "moredetail/:referencekey/:referenceid", data: { permission: "customer", menuoperations: "ManageMerchant", PageName: 'System.Menu.Customer', accounttypecode: "customer" }, loadChildren: "../../../../accounts/tuconsole/topup/topup.module#TUTopUpModule" },
       //{ path: "moredetail/:referencekey/:referenceid", data: { permission: "customer", menuoperations: "ManageMerchant", PageName: 'System.Menu.Customer', accounttypecode: "customer" }, loadChildren: "../../../../accounts/tuconsole/wallate/wallate.module#TUWallateModule" },
            // { path: 'sales/salehistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Customer' }, loadChildren: '../../../../modules/transactions/tusale/customer/merchant/tusale.module#TUSaleModule' },
            // { path: 'sales/rewardhistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Customer' }, loadChildren: '../../../../modules/transactions/tusale/customer/rewardhistory/tusale.module#TUSaleModule' },
            // { path: 'sales/redeemhistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Customer' }, loadChildren: '../../../../modules/transactions/tusale/customer/redeemhistory/tusale.module#TUSaleModule' },
            // { path: 'sales/rewardclaimhistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Customer' }, loadChildren: '../../../../modules/transactions/tusale/customer/turewardclaim/turewardclaim.module#TURewardsClaimModule' },

            // { path: 'terminals/:referencekey/:referenceid', data: { 'permission': 'terminals', PageName: 'System.Menu.Store' }, loadChildren: '../../../../modules/accounts/tuterminals/store/tuterminals.module#TUTerminalsModule' },

            { path: 'moredetail/rewardpercentage/:referencekey/:referenceid', data: { 'permission': 'cashiers', PageName: 'System.Menu.Settings' }, loadChildren: '../../../../accounts/tuconsole/rewardpercentage/turewardpercentage.module#TURewardPercentageModule' },
            { path: 'moredetail/subaccounts/:referencekey/:referenceid', data: { 'permission': 'cashiers', PageName: 'System.Menu.Settings' }, loadChildren: '../../../../accounts/tuconsole/subaccounts/tusubaccounts.module#TUSubAccountsModule' },
            { path: 'moredetail/topup/:referencekey/:referenceid', data: { 'permission': 'topup', PageName: 'System.Menu.Settings' }, loadChildren: '../../../../accounts/tuconsole/topup/topup.module#TUTopUpModule' },
            { path: 'moredetail/wallate/:referencekey/:referenceid', data: { 'permission': 'topup', PageName: 'System.Menu.Settings' }, loadChildren: '../../../../accounts/tuconsole/wallate/wallate.module#TUWallateModule' },
            { path: 'moredetail/cashout/:referencekey/:referenceid', data: { 'permission': 'topup', PageName: 'System.Menu.Settings' }, loadChildren: '../../../../accounts/tuconsole/cashout/cashout.module#TUCashoutModule' },
            { path: 'configurations/:referencekey/:referenceid', data: { 'permission': 'dashboard', PageName: 'System.Menu.Configuration' }, loadChildren: '../../../../../modules/accounts/tuconsole/merchants/tuconfigurations/tuconfigurations.module#TUConfigurationManagerModule' },
            { path: 'bnplconfigurations/:referencekey/:referenceid', data: { 'permission': 'dashboard', PageName: 'BNPL Configurations' }, loadChildren: '../../../../../modules/accounts/tuconsole/merchants/bnplconfigurations/bnplconfigurations.module#BnplconfigurationsModule' },
        ]
    }
];
const config: InputFileConfig = {
    fileAccept: '*',
    fileLimit: 1,
  };
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUCustomerRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        ImageCropperModule,
        TUCustomerRoutingModule,
        GooglePlaceModule,
        Angular4PaystackModule,
        ImageCropperModule,
        InputFileModule.forRoot(config),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    declarations: [TUMerchantDetailsComponent]
})
export class TUMerchantDetailsModule { }

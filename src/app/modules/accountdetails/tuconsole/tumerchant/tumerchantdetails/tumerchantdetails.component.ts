import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewRef,ViewChild } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { DataHelperService, FilterHelperService, HelperService, OResponse, OSelect, OUserDetails } from "../../../../../service/service";
import { Observable, Subscription } from 'rxjs';
declare let $: any;
declare var d3: any;
import swal from 'sweetalert2';
import * as Feather from 'feather-icons';
import { InputFile, InputFileComponent } from 'ngx-input-file';
@Component({
 selector: "tu-merchantdetails",
 templateUrl: "./tumerchantdetails.component.html"

})
export class TUMerchantDetailsComponent implements OnInit,OnDestroy {
  VarAccordion: boolean = false;
  VarAccordion1: boolean = false;
  CurrentImagesCount: number = 0;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
  }
  @ViewChild("inputfile_test")
  private InputFileComponent: InputFileComponent;
  public _ObjectSubscription1: Subscription = null;
  //   public _Roles = [
  //     {
  //         'id': 0,
  //         'text': 'Select Role',
  //         'apival': 'Select Role'
  //     },
  //     {
  //         'id': 10,
  //         'text': 'Admin',
  //         'apival': 'merchantadmin'
  //     },
  //     {
  //       'id': 11,
  //       'text': 'Store Manager',
  //       'apival': 'merchantstoremanager'
  //   },

  // ];

  selectedRole = null;
  public _Roles = [
    {
      'id': 10,
      'text': 'Admin',
      'apival': 'merchantadmin'
    },
    {
      'id': 11,
      'text': 'Store Manager',
      'apival': 'merchantstoremanager'
    },

  ];


  ngOnInit() {

    this._HelperService.ValidateData();
    Feather.replace();   
    // this._ActivatedRoute.params.subscribe((params: Params) => {

    //   this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
    //   this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];

    // });
    // this._ObjectSubscription1 = this._HelperService.ObjectCreated.subscribe(value => {
    //   location.reload();

    // });
    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveMerchant);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveMerchantReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
      this.GetBalance();
      this.GetMerchantDetails();
      this.Form_AddSubAccount_Load();
      this.Get_UserAccountDetails();
      this.TUTr_Filter_Stores_Load();
    }
    else {
      this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound])
      this._HelperService.DeleteStorage('templocmerchant');

    }
    this.InitImagePicker(this.InputFileComponent);


  }
  private InitImagePicker(InputFileComponent: InputFileComponent) {
    if (InputFileComponent != undefined) {
      this.CurrentImagesCount = 0;
      this._HelperService._InputFileComponent = InputFileComponent;
      InputFileComponent.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
  }
  removeImage(): void {
    this.CurrentImagesCount = 0;
    this._HelperService.Icon_Crop_Clear();
  }
resetImage(){
  
this._HelperService.Icon_Crop_Clear();
this.InputFileComponent.files.pop();
this._HelperService._Icon_Cropper_Data.Content=null;
// let formValue=this.Form_Add.value;
// formValue.IconContent.Content=null;
}
ResetImagePlaceholder:boolean = true
ResetImage(): void {
  this.ResetImagePlaceholder = false;
  this._ChangeDetectorRef.detectChanges();
  this.ResetImagePlaceholder = true;
  this._ChangeDetectorRef.detectChanges();
}
  ngOnDestroy(): void {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    setTimeout(() => {
      if (this._ChangeDetectorRef && !(this._ChangeDetectorRef as ViewRef).destroyed) {
        this._ChangeDetectorRef.detectChanges();
      }
    });
    try {
      this._ObjectSubscription1.unsubscribe();
    } catch (error) {

    }
  }


  public GetRoles_Option: Select2Options;
  public GetRoles_Transport: any;
  getroles_list() {
    this.GetRoles_Option = {
      placeholder: 'Select Role',
      ajax: this.GetRoles_Transport,
      multiple: false,
    };
  }
  GetRoles_ListChange(event: any) {
    // console.log(event.value)
    if (event.value == 0) {
      this._HelperService.NotifyError('Select Role');
    }
    this.Form_AddSubAccount.patchValue(
      {
        RoleKey: event.data[0].apival,
        RoleId: event.value

      }
    );
    // this.ToggleStoreSelect = false;
    // this.ToggleStoreSelect = false;
    // this.TUTr_Filter_Stores_Load();
    // setTimeout(() => {
    //   this.ToggleStoreSelect = true;
    //   this.TUTr_Filter_Stores_Load();
    // }, 500);

    // if (this.Form_AddSubAccount.controls['RoleKey'].value == 'manager') {
    //   this.TUTr_Filter_Stores_Load();
    //   setTimeout(() => {
    //     this.ToggleStoreSelect = true;
    //     this.TUTr_Filter_Stores_Load();
    //   }, 500);
    // }
    // else
     {
      this.Form_AddSubAccount.patchValue({
        // StoreReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
        // StoreReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId
        StoreReferenceKey: 0,
        StoreReferenceId: 0

      })
    }



  }

  Form_UpdateAddressUser: FormGroup;

  public Get_UserAccountDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccount,
      Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveMerchantReferenceKey, '='),
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService._UserAccount = _Response.Result as OUserDetails;
          this._HelperService._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(this._HelperService._UserAccount.CreateDate);
          this._HelperService._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(this._HelperService._UserAccount.ModifyDate);
          this._HelperService._UserAccount.StatusI = this._HelperService.GetStatusIcon(this._HelperService._UserAccount.StatusCode);
          this._HelperService.IsFormProcessing = false;
          //#endregion
        }
        else {
          this._HelperService.IsFormProcessing = false;

          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {

        this._HelperService.HandleException(_Error);
      });
  }


  VarAccordion1F() {
    this.VarAccordion1 = !this.VarAccordion1;

  }

  VarAccordionF() {
    this.VarAccordion = !this.VarAccordion;

  }
  VarAccordion1Business1: boolean = false;

  VarAccordion1Business() {
    this.VarAccordion1Business1 = !this.VarAccordion1Business1;

  }


  public _MerchantDetails: any =
    {
      "ReferenceId": null,
      "ReferenceKey": null,
      "DisplayName": null,
      "EmailAddress": null,
      "IconUrl": null,
      "AccountCode": null,
      "WebsiteUrl": null,
      "Address": {
        "Address": null,
        "Latitude": null,
        "Longitude": null
      },
      "ContactPerson": {
        "MobileNumber": null,
        "EmailAddress": null
      },
      // 
      "Rm": {
        "Name": null,
        "MobileNumber": null,
        "EmailAddress": null,
      },
      // 
      "Categories": [
        {
          "ReferenceId": null,
          "ReferenceKey": null,
          "Name": null,
          "IconUrl": null
        }
      ],
      "Stores": null,
      "Terminals": null,
      "Cashiers": null,
      "CreateDate": null,
      "StatusCode": null,
      "StatusName": null

    }



  CashierRoute() {

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.Cashiers,
      this._MerchantDetails.ReferenceKey,
      this._MerchantDetails.ReferenceId

    ]);
  }

  StoreRoute() {

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Merchant.Stores,
      this._MerchantDetails.ReferenceKey,
      this._MerchantDetails.ReferenceId


    ]);

  }

  TerminalRoute() {

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Merchant.Terminals,
      this._MerchantDetails.ReferenceKey,
      this._MerchantDetails.ReferenceId

    ]);
  }

  Reward() {

    this._Router.navigate([
      // this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Merchant.Terminals,
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Loyality.RewardHistory,
      this._MerchantDetails.ReferenceKey,
      this._MerchantDetails.ReferenceId

    ]);
  }


  BlockAccount() {
    swal({
      title: this._HelperService.AppConfig.CommonResource.BlockMerchantTitle,
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
        ' <label> Do you really want to block this merchant?  </label>' +
        '<input type="text"  placeholder="Enter Comment"  maxLength= "128" id="swal-input1" class="swal2-input">' +
        '<input  style="-webkit-text-security: disc;" type="text" placeholder="Enter Pin"     maxLength= "4" id="swal-input2" class="swal2-input">',
      focusConfirm: false,
      preConfirm: () => {
        return [
          document.getElementById('swal-input1')['value'],
          document.getElementById('swal-input2')['value']
        ]
      },
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      // inputAttributes: {
      //   autocapitalize: 'off',
      //   autocorrect: 'off',
      //   maxLength: "4",
      //   minLength: "4"
      // },

    }).then((result) => {
      if (result.value) {
        if (result.value[1].length < 4) {
          this._HelperService.NotifyError('Enter your 4 digit pin');
          return;
        }

        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updateaccountstatus",
          AccountId: this._MerchantDetails.ReferenceId,
          AccountKey: this._MerchantDetails.ReferenceKey,
          StatusCode: "default.blocked",
          AuthPin: result.value[1],
          Comment: result.value[0],
          AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant

        };

        let _OResponse: Observable<OResponse>;

        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, PostData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Merchant Blocked Successfully");
              this.GetMerchantDetails();
            } else {
              this._HelperService.NotifySuccess(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });


  }
  UnblockBlockAccount() {
    swal({
      title: this._HelperService.AppConfig.CommonResource.UnBlockMerchantTitle,
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
        ' <label> Do you really want to unblock this merchant?  </label>' +
        '<input type="text" placeholder="Enter Comment" maxLength = "128"  id="swal-input1" class="swal2-input">' +
        '<input style="-webkit-text-security: disc;" type="text" placeholder="Enter Pin" maxLength = "4" id="swal-input2" class="swal2-input">',


      focusConfirm: false,
      preConfirm: () => {
        return [
          document.getElementById('swal-input1')['value'],
          document.getElementById('swal-input2')['value']
        ]
      },
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      //   inputAttributes: {
      //     autocapitalize: 'off',
      //     autocorrect: 'off',
      //     maxLength: "4",
      //     minLength: "4"
      //   },
      //   inputValidator:function(value) {
      //     if(value === '' || value.length < 4) { 
      //       return 'Enter your 4 digit pin!'
      //     }
      // },
    }).then((result) => {
      if (result.value) {
        if (result.value[1].length < 4) {
          this._HelperService.NotifyError('Enter your 4 digit pin');
          return;
        }
        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updateaccountstatus",
          AccountId: this._MerchantDetails.ReferenceId,
          AccountKey: this._MerchantDetails.ReferenceKey,
          StatusCode: "default.active",
          AuthPin: result.value[1],
          Comment: result.value[0],
          AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant

        };

        let _OResponse: Observable<OResponse>;

        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, PostData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Merchant UnBlocked Successfully");
              this.GetMerchantDetails();
            } else {
              this._HelperService.NotifySuccess(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });


  }


  initGauge() {
    var $container = d3.select('#linear-gauge');
    var width = parseFloat($container.style("width"));
    var height = parseFloat($container.style("height"));
    window.onresize = resize;
    function resize() {
      $container = d3.select('#linear-gauge');
      if ($container && $container != null) {
        width = parseFloat($container.style("width"));
        height = parseFloat($container.style("height"));
        chart_w = width;
        resultPos = chart_w * result;
        text_margins = { top: chart_y_pos + gauge_h + 0, right: 10, bottom: 0, left: 10 };
        d3.select("line")
          .attr("x1", resultPos)
          .attr("y1", chart_y_pos)
          .attr("x2", resultPos)
          .attr("y2", gauge_h + chart_y_pos);

        d3.select("circle")
          .attr("cx", resultPos)
          .attr("cy", (gauge_h + chart_y_pos) / 2);

        d3.select(".rightLabel")
          .attr("x", chart_w)
          .text("width: " + width);

        d3.select(".rightPrcnt")
          .attr("x", chart_w);
      }

    }

    //#region Tick mark 

    var LF = 30;

    var gauge_h = 25;

    var chart_w = width;
    var chart_y_pos = 0;

    var result = this._AccountBalance.Ratio;	// in a scale [0 1]
    var resultPos = chart_w * result;

    var text_margins = { top: chart_y_pos + gauge_h + 35, right: 10, bottom: 0, left: 10 };

    //#endregion

    //#region chartsize 

    var svg = d3.select('#linear-gauge').append("svg")
      .attr("width", '100%')
      .attr("height", '100%');

    var gradient = svg.append("svg:defs")
      .append("svg:linearGradient")
      .attr("id", "gradient")
      .attr("x1", "0%")
      .attr("y1", "0%")
      .attr("x2", "100%")
      .attr("y2", "0%")
      .attr("spreadMethod", "pad");

    gradient.append("svg:stop")
      .attr("offset", "0%")
      .attr("stop-color", "#c00")
      .attr("stop-opacity", 1);

    gradient.append("svg:stop")
      .attr("offset", "50%")
      .attr("stop-color", "yellow")
      .attr("stop-opacity", 1);

    gradient.append("svg:stop")
      .attr("offset", "100%")
      .attr("stop-color", "#0c0")
      .attr("stop-opacity", 1);

    svg.append("g")
      .append("rect")
      .attr("x", 0)
      .attr("y", chart_y_pos)
      .attr("width", "100%")
      .attr("height", gauge_h)
      .style("fill", "url(#gradient)");

    //#endregion

    /****************************************
    * Text, titles
    *****************************************/

    //#region left percentage indicator 

    svg.append("g")
      .append("text")
      .attr("x", 0)
      .attr("y", text_margins.top)
      .style("font-size", "14px")
      .text(Math.round(this._AccountBalance.Ratio * 100) + '%  balance is remaining from last topup amount' + '  ' + Math.round(this._AccountBalance.LastTransactionAmount ? this._AccountBalance.LastTransactionAmount : 0.0));
    // svg.append("g")
    //   .append("text")
    //   .attr("x", 0)
    //   .attr("y", text_margins.top + LF)
    //   .text("Alarm");

    //#endregion

    //#region right percentage indicator 

    // svg.append("g")
    //   .append("text")
    //   .classed("rightPrcnt", true)
    //   .attr("x", chart_w)
    //   .attr("y", text_margins.top)
    //   .attr("text-anchor", "end")
    //   .text("100%");

    // svg.append("g")
    //   .append("text")
    //   .classed("rightLabel", true)
    //   .attr("x", chart_w)
    //   .attr("y", text_margins.top + LF)
    //   .attr("text-anchor", "end")
    //   .text("width: " + chart_w);

    //#endregion

    //#region Result 

    var tickMark = svg.append("g");

    tickMark.append("line")
      .attr("x1", resultPos)
      .attr("y1", chart_y_pos)
      .attr("x2", resultPos)
      .attr("y2", gauge_h + chart_y_pos)
      .attr("stroke-width", 3)
      .attr("stroke", "black");


    // tickMark.append("circle")
    //   .attr("cx", resultPos)
    //   .attr("cy", (gauge_h + chart_y_pos) / 2)
    //   .attr("r", 7);

    //#endregion

  }

  _ShowGauge: boolean = true;
  //#region Balance Data 
  public _TransactionReference = null;
  public _AccountBalanceCreditAmount = 0;
  public PaymentReference = null;

  generateRandomNumber() {
    var TrRef = Math.floor(100000 + Math.random() * (999999 + 1 - 100000));
    return TrRef;
  }
  OpenPaymentOptions() {
    this._AccountBalanceCreditAmount = 0;
    // var Ref = this.generateRandomNumber();
    this.TransactionId = this.generateRandomNumber()

    // this._TransactionReference = this._HelperService.AppConfig.ActiveMerchantReferenceId + '_' + this._HelperService.UserAccount.ReferralCode + '_' + Ref;
    this._HelperService.OpenModal('Form_TopUp_Content');
  }
  paymentDone(ref: any) {
    if (ref != undefined && ref != null && ref != '') {
      if (ref.status == 'success') {
        this.CreditAmount();
      }
      else {
        this._HelperService.NotifyError('Payment failed');
      }
    }
    else {
      this._HelperService.NotifyError('Payment failed');
    }
    // this.title = 'Payment successfull';
  }

  public TransactionId
  public CreditAmount() {
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "topupaccount",
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      Amount: this._AccountBalanceCreditAmount,
      PaymentReference: this.PaymentReference,
      TransactionId: this.TransactionId
    };
    if (this._AccountBalanceCreditAmount <= 0) {
      this._HelperService.NotifyError(" Amount must be greater than 0");
      this._HelperService.IsFormProcessing = false;
    }
    else {
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscription, PostData);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.IsFormProcessing = false;
            this._AccountBalanceCreditAmount = 0;
            this.PaymentReference = null;
            this._TransactionReference = null;
            this._HelperService.NotifySuccess("Account credited");
            this._HelperService.CloseModal('Form_TopUp_Content');
            this._HelperService.ObjectCreated.next(true);
            this.GetBalance();
          } else {
            this._HelperService.NotifyError(_Response.Message);
            this._HelperService.IsFormProcessing = false;
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );
    }

  }
  paymentCancel() {
  }
  public ToggleStoreSelect: boolean = false;
  public TUTr_Filter_Store_Option: Select2Options;
  TUTr_Filter_Stores_Load() {

    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
        // {
        //   SystemName: "AccountTypeCode",
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   SearchCondition: "=",
        //   SearchValue: this._HelperService.AppConfig.AccountType.Store
        // }
      ]
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'MerchantReferenceId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Store_Option = {
      placeholder: 'Select Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Stores_Change(event: any) {

    this.Form_AddSubAccount.patchValue(
      {
        StoreReferenceId: event.data[0].ReferenceId,
        StoreReferenceKey: event.data[0].ReferenceKey,
        // StoreName: event.data[0].DisplayName,

      }


    );

  }



  public _AccountBalance: any = {
    Credit: 0.0,
    Debit: 0.0,
    Balance: 0.0,
    LastTransactionAmount: 0.0,
    LastTopupBalance: 0.0,
    LastTransactionDate: null,
    Ratio: 0,
    IsLoaded: false,
  };

  public GetBalance() {
    this._ShowGauge = false;
    this._AccountBalance.IsLoaded = false;
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: 'getaccountbalance',
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      Source: this._HelperService.AppConfig.TransactionSource.Merchant
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscription, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountBalance = _Response.Result;
          this._HelperService.IsFormProcessing = false;
          if (this._AccountBalance.Balance && this._AccountBalance.LastTopupBalance && this._AccountBalance.Balance > 0) {
            this._AccountBalance.Ratio = this._AccountBalance.Balance / this._AccountBalance.LastTopupBalance;
          }
          else if(this._AccountBalance.Balance && this._AccountBalance.LastTopupBalance == 0){
            this._AccountBalance.Ratio = this._AccountBalance.Balance / this._AccountBalance.LastTransactionAmount;
          }
          else {
            this._AccountBalance.Ratio = 0;
          }
          this._AccountBalance.IsLoaded = true;

          setTimeout(() => {
            this._ShowGauge = true;
            setTimeout(() => {
              try {
                this.initGauge();
              } catch (error) {
              }
            }, 200);
          }, 500);

        } else {
          this._AccountBalance.IsLoaded = true;
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region MerchantDetails 

  public _UserAccount: OUserDetails = {
    ContactNumber: null,
    SecondaryEmailAddress: null,
    ReferenceId: null,
    BankDisplayName: null,
    BankKey: null,
    SubOwnerAddress: null,
    SubOwnerLatitude: null,
    SubOwnerDisplayName: null,
    SubOwnerKey: null,
    SubOwnerLongitude: null,
    AccessPin: null,
    LastLoginDateS: null,
    AppKey: null,
    AppName: null,
    AppVersionKey: null,
    CreateDate: null,
    CreateDateS: null,
    CreatedByDisplayName: null,
    CreatedByIconUrl: null,
    CreatedByKey: null,
    Description: null,
    IconUrl: null,
    ModifyByDisplayName: null,
    ModifyByIconUrl: null,
    ModifyByKey: null,
    ModifyDate: null,
    ModifyDateS: null,
    PosterUrl: null,
    ReferenceKey: null,
    StatusCode: null,
    StatusI: null,
    StatusId: null,
    StatusName: null,
    AccountCode: null,
    AccountOperationTypeCode: null,
    AccountOperationTypeName: null,
    AccountTypeCode: null,
    AccountTypeName: null,
    Address: null,
    AppVersionName: null,
    ApplicationStatusCode: null,
    ApplicationStatusName: null,
    AverageValue: null,
    CityAreaKey: null,
    CityAreaName: null,
    CityKey: null,
    CityName: null,
    CountValue: null,
    CountryKey: null,
    CountryName: null,
    DateOfBirth: null,
    DisplayName: null,
    EmailAddress: null,
    EmailVerificationStatus: null,
    EmailVerificationStatusDate: null,
    FirstName: null,
    GenderCode: null,
    GenderName: null,
    LastLoginDate: null,
    LastName: null,
    Latitude: null,
    Longitude: null,
    MobileNumber: null,
    Name: null,
    NumberVerificationStatus: null,
    NumberVerificationStatusDate: null,
    OwnerDisplayName: null,
    OwnerKey: null,
    Password: null,
    Reference: null,
    ReferralCode: null,
    ReferralUrl: null,
    RegionAreaKey: null,
    RegionAreaName: null,
    RegionKey: null,
    RegionName: null,
    RegistrationSourceCode: null,
    RegistrationSourceName: null,
    RequestKey: null,
    RoleKey: null,
    RoleName: null,
    SecondaryPassword: null,
    SystemPassword: null,
    UserName: null,
    WebsiteUrl: null
  };
  public _Address: any = {};
  public _ContactPerson: any = {};
  public _Overview: any = {};
  GetMerchantDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchant,
      // Task: this._HelperService.AppConfig.Api.Core.GetUserAccount,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      //Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveMerchantReferenceKey, '='),
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._MerchantDetails = _Response.Result;
          this._Address = this._MerchantDetails.AddressComponent;
          this._ContactPerson = this._MerchantDetails.ContactPerson;
          this._MerchantDetails['ContactPerson'] = {};
          //#region RelocateMarker 

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          // this.FormC_EditUser_Latitude = this._MerchantDetails.Latitude;
          // this.FormC_EditUser_Longitude = this._MerchantDetails.Longitude;
          // this.FormC_EditUser_Latitude = _Response.Result.Latitude;
          // this.FormC_EditUser_Longitude = _Response.Result.Longitude;

          //#endregion

          //#region DatesAndStatusInit 

          this._MerchantDetails.StartDateS = this._HelperService.GetDateS(
            this._MerchantDetails.StartDate
          );
          this._MerchantDetails.EndDateS = this._HelperService.GetDateS(
            this._MerchantDetails.EndDate
          );
          this._MerchantDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._MerchantDetails.CreateDate
          );
          this._MerchantDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._MerchantDetails.ModifyDate
          );
          this._MerchantDetails.StatusI = this._HelperService.GetStatusIcon(
            this._MerchantDetails.StatusCode
          );
          this._MerchantDetails.StatusB = this._HelperService.GetStatusBadge(
            this._MerchantDetails.StatusCode
          );
          this._MerchantDetails.StatusC = this._HelperService.GetStatusColor(
            this._MerchantDetails.StatusCode
          );
          this._MerchantDetails.CreateDateD = this._HelperService.GetDateS(this._MerchantDetails.CreateDate);
          this._MerchantDetails.CreateDateT = this._HelperService.GetTimeS(this._MerchantDetails.CreateDate);


          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }



  EditMerchant(): void {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Profile, this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      this._HelperService.AppConfig.ActiveMerchantReferenceId
    ]);
    this._HelperService.CloseAllModal();

  }

  //#endregion

  //#region Add SubAccount 

  Form_AddSubAccount: FormGroup;
  Form_AddSubAccount_Show() {
    this._HelperService.OpenModal("Form_AddSubAccount_Content");
    // this.Form_Add_Clear();
      // this._HelperService.OpenModal('Form_Add_Content');
      setTimeout(() => {
        this.InitImagePicker(this.InputFileComponent);
      }, 100);
  }
  Form_AddSubAccount_Close() {
    this._HelperService.CloseModal("Form_AddSubAccount_Content");
    this._HelperService.ObjectCreated.next(true);
          this.Form_AddSubAccount_Clear();
          this.getroles_list();
          this.ToggleStoreSelect = false;
  }
  Form_AddSubAccount_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;
    

    this.Form_AddSubAccount = this._FormBuilder.group({
      OperationType: "new",
      StoreReferenceKey: [null],
      StoreReferenceId: [null],
      RoleKey: [null, Validators.required],
      RoleId: [null, Validators.required],
      Task: this._HelperService.AppConfig.Api.ThankUCash.SaveSubAccount,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      StatusCode: this._HelperService.AppConfig.Status.Active,
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      GenderCode: this._HelperService.AppConfig.Gender.Male,
    });
  }
  Form_AddSubAccount_Clear() {
    this.Form_AddSubAccount.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddSubAccount_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  isRoleSelect:boolean=true;
  CloseModal(ModalId) {
    $("#" + ModalId).modal("hide");
  }
  Form_AddSubAccount_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;

    var Request = this.CreateSubAccountRequest(_FormValue);
   

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.Account,
      Request
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.FlashSwalSuccess("New SubAccount has been added successfully",
            "Done! You have successfully created new SubAccount");
          this._HelperService.ObjectCreated.next(true);
          this.Form_AddSubAccount_Clear();
          this.Form_AddSubAccount_Close();
          this.ToggleStoreSelect = false;
          this.isRoleSelect=false;
          setTimeout(()=>{
            this.isRoleSelect=true;
          },500);
          this.ResetImage();
          this._HelperService._FileSelect_Icon_Reset();
         
          
          this._HelperService.Icon_Crop_Clear();
          
        
          if (_FormValue.OperationType == "close") {
            this.Form_AddSubAccount_Close();
            
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  CreateSubAccountRequest(_FormValue: any): void {

    // _FormValue.Address = {
    //   Latitude: this.Form_AddStore_Latitude,
    //   Longitude: this.Form_AddStore_Longitude,
    //   Address: this.Form_AddStore_Address,
    //   CityName: this._CurrentAddress.sublocality_level_2,
    //   StateName: this._CurrentAddress.sublocality_level_1,
    //   CountryName: this._CurrentAddress.country
    // }

    // _FormValue.Latitude = undefined;
    // _FormValue.Longitude = undefined;
    // _FormValue.MapAddress = undefined;

    var IconContent: any = undefined;

    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      IconContent = this._HelperService._Icon_Cropper_Data;
    }

    _FormValue.IconContent = IconContent;

    return _FormValue;
  }
}
import { AgmCoreModule } from '@agm/core';
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Select2Module } from "ng2-select2";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgxPaginationModule } from "ngx-pagination";
import { MainPipe } from '../../../../service/main-pipe.module';
import { TUMerchantComponent } from "./tumerchant.component";
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { ImageCropperModule } from 'ngx-image-cropper';
import { HCXAddressManagerModule } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';
import { DynamicRoutesguardGuard } from 'src/app/service/guard/dynamicroutes.guard';

const config: InputFileConfig = {
    fileAccept: '*',
    fileLimit: 1,
};

const routes: Routes = [
    {
        path: "",
        component: TUMerchantComponent,
        canActivate:[DynamicRoutesguardGuard],
        data:{accessName:['viewmerchant']},
        children: [
            { path: "",data:{permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../tumerchant/tumerchantdetails/tumerchantdetails.module#TUMerchantDetailsModule" },
            { path: "details", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../tumerchant/tumerchantdetails/tumerchantdetails.module#TUMerchantDetailsModule" },

            { path: 'configurations/:referencekey/:referenceid', data: { 'permission': 'dashboard', PageName: 'System.Menu.Configuration' }, loadChildren: '../../../../modules/accounts/tuconsole/merchants/tuconfigurations/tuconfigurations.module#TUConfigurationManagerModule' },
            { path: "dashboard/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.Dashboard" }, loadChildren: "../../../../modules/dashboards/merchant/dashboard.module#TUDashboardModule" },
            { path: "downloads/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.Dashboard" }, loadChildren: "../../../../modules/dashboards/merchant/downloads.module#TUDownloadsModule" },
            // { path: "dashboard/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../dashboards/merchant/acquirer/dashboard.module#TUDashboardModule" },
            { path: 'salesummary/:referencekey/:referenceid', data: { 'permission': 'salesummary', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../modules/dashboards/mybuisness/tusalesummary/tusalesummary.module#TUSaleSummaryModule' },
            { path: 'salestrend/:referencekey/:referenceid', data: { 'permission': 'saletrends', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../modules/dashboards/mybuisness/tusalestrends/tusalestrends.module#TuSalesTrendModule' },
            { path: 'saleshistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../modules/transactions/tusale/merchant/tusale.module#TUSaleModule' },
            { path: 'pendingtransaction/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../modules/transactions/tusale/Pendingtransaction/merchant/pendingtransaction/pendingtransaction.module#TUPendingTransactionModule' },

            { path: "rewardhistory/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.RewardHistory" }, loadChildren: "../../../../modules/transactions/tusale/rewardhistory/merchant/tusale.module#TUSaleModule" },
            { path: "pendingrewardhistory/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.RewardHistory" }, loadChildren: "../../../../modules/transactions/tusale/pendingrewardhistory/merchant/tusale.module#TUSaleModule" },

            { path: "redeemhistory/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.RedeemHistory" }, loadChildren: "../../../../modules/transactions/tusale/redeemhistory/merchant/tusale.module#TUSaleModule" },
            { path: "rewardclaimhistory/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.Menu.RewardClaim" }, loadChildren: "../../../../modules/transactions/tusale/turewardclaim/merchant/turewardclaim.module#TURewardsClaimModule" },

            { path: 'overview/:referencekey/:referenceid', data: { 'permission': 'redeemhistory', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../modules/dashboards/loyality/overview/dashboard.module#TUDashboardModule' },
            { path: 'visits/:referencekey/:referenceid', data: { 'permission': 'rewardhistory', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../modules/dashboards/loyality/visits/dashboard.module#TUDashboardModule' },
            { path: 'customers/:referencekey/:referenceid', data: { 'permission': 'rewardhistory', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../modules/dashboards/loyality/customers/tucustomers.module#TUCustomersModule' },

            { path: 'cashiers/:referencekey/:referenceid',canActivateChild:[DynamicRoutesguardGuard], data: { 'permission': 'cashiers', PageName: 'System.Menu.Settings',accessName:['maccashiers'] }, loadChildren: '../../../../modules/accounts/tuconsole/cashiers/tucashiers.module#TUCashiersModule' },
            { path: 'stores/:referencekey/:referenceid', data: { 'permission': 'stores', PageName: 'System.Menu.Settings' }, loadChildren: '../../../../modules/accounts/tuconsole/stores/tustores.module#TUStoresModule' },
            { path: 'terminals/:referencekey/:referenceid', data: { 'permission': 'cashiers', PageName: 'System.Menu.Settings' }, loadChildren: '../../../../modules/accounts/tuconsole/tuterminals/merchant/tuterminals.module#TUTerminalsModule' },
            { path: 'subaccounts/:referencekey/:referenceid', data: { 'permission': 'subaccounts', PageName: 'System.Menu.Settings' }, loadChildren: '../../../../modules/accounts/tuconsole/subaccounts/tusubaccounts.module#TUSubAccountsModule' },

            { path: "bank/:referencekey/:referenceid", data: { permission: "account", PageName: "System.Menu.BankReconcialiation" }, loadChildren: "../../../../modules/accounts/tuconsole/bankreconciliation/bank.module#TUBankModule" },
            { path: "dailyreconciliation/:referencekey/:referenceid", data: { permission: "account", PageName: "System.Menu.BankReconcialiation" }, loadChildren: "../../../../modules/accounts/tuconsole/administration/dailyreconciliation/dailyreconciliation.module#TUDailyReconciliationModule" },

            { path: "campaigns/:referencekey/:referenceid", data: { 'permission': "dashboard", PageName: "System.Menu.Campaigns" }, loadChildren: "../../../../modules/dashboards/loyality/campaigns/tucampaigns.module#TUCampaignsModule" },
            { path: 'products/:referencekey/:referenceid', data: { 'permission': 'terminals', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../panel/console/tucmall/tuproducts/tuproducts.module#TUProductsModule' },
            { path: 'ambassadors/:referencekey/:referenceid', data: { 'permission': 'terminals', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../panel/console/tucmall/tuambassadors/tuambassadors.module#TUAmbassadorsModule' },
            { path: 'orders/:referencekey/:referenceid', data: { 'permission': 'stores', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../panel/console/tucmall/tuorders/tuorders.module#TUOrdersModule' },
            { path: 'tucorders/:referencekey/:referenceid', data: { 'permission': 'stores', PageName: 'System.Menu.Merchant' }, loadChildren: '../../../../panel/console/tucmall/tucorders/tucorders.module#TUCOrderssModule' },
            { path: "customergroup/:referencekey/:referenceid", data: { permission: "dashboard", PageName: "System.SMS.CustomerGroup" }, loadChildren: "../../../../panel/smscampaign/tuallcustomersgroup/tuallcustomersgroup.module#TUCustomerGroupModule" },
            { path: "customergroup/allcustomers/:referencekey/:referenceid/:groupkey/:groupid/:title", data: { permission: "dashboard", PageName: "System.SMS.Customers" }, loadChildren: "../../../../panel/smscampaign/allcutomers/allcutomers.module#TUAllCustomersModule" },
            { path: "mdsmscampaigns/:referencekey/:referenceid", data: { permission: "customer", menuoperations: "ManageMerchant", PageName: 'System.Menu.Merchant', accounttypecode: "customer" }, loadChildren: "../../../../panel/smscampaign/Merchant/tuallsmscampaign/tuallsmscampaign.module#TUAllSMSCampaignModule" },

            //Deal Route
            { path: "mdeals/:referencekey/:referenceid", data: { permission: "customer", menuoperations: "ManageMerchant", PageName: 'System.Menu.Merchant', accounttypecode: "customer" }, loadChildren: "../../../../modules/deals/tudeals/tudeals.module#TUDealsModule" },
            { path: "mdeal/:referencekey/:referenceid/:accountid/:accountkey", data: { permission: "customer", menuoperations: "ManageMerchant", PageName: 'System.Menu.Merchant', accounttypecode: "customer" }, loadChildren: "../../../../modules/deals/tudeal/tudeal.module#TUDealModule" },
            { path: "mdealoverview/:referencekey/:referenceid", data: { permission: "customer", menuoperations: "ManageMerchant", PageName: 'System.Menu.Merchant', accounttypecode: "customer" }, loadChildren: "../../../../modules/deals/tudealspurchaseoverview/tuoverview.module#TuOverviewTrendModule" },
            { path: "mdealadd/:referencekey/:referenceid", data: { permission: "customer", menuoperations: "ManageMerchant", PageName: 'System.Menu.Merchant', accounttypecode: "customer" }, loadChildren: "../../../../modules/deals/tuadddeals/tuadddeals.module#AddDealsModule" },

            // GC
            { path: "giftcards/:referencekey/:referenceid", data: { permission: "customer", menuoperations: "ManageMerchant", PageName: 'System.Menu.Merchant', accounttypecode: "customer" }, loadChildren: "../../../../merchant/giftcard/overview/tugiftcard.module#TuGiftCardModule" },
            { path: 'gcrewardhistory/:referencekey/:referenceid', data: { 'permission': 'giftcard', PageName: 'System.Menu.GiftCard' }, loadChildren: '../../../../merchant/giftcard/Reward/tusale.module#TUSaleModule' },
            { path: 'gcredeemhistory/:referencekey/:referenceid', data: { 'permission': 'giftcard', PageName: 'System.Menu.GiftCard' }, loadChildren: '../../../../merchant/giftcard/Redeemed/tusale.module#TUSaleModule' },
            { path: 'gcbalancehistory/:referencekey/:referenceid', data: { 'permission': 'giftcard', PageName: 'System.Menu.GiftCard' }, loadChildren: '../../../../merchant/giftcard/Balance/tusale.module#TUSaleModule' },

            // { path: "giftcardsales/:referencekey/:referenceid", data: { permission: "customer", menuoperations: "ManageMerchant", PageName: 'System.Menu.Merchant', accounttypecode: "customer" }, loadChildren: "../../../../merchant/giftcard/home/home.module#TUHomeModule" },
            //   { path: "", data: { permission: "dashboard", PageName: "System.Menu.Home" }, loadChildren: "../../modules/transactions/tusale/giftcard/home/home.module#TUHomeModule" },
            // { path: "giftcardshistory/:referencekey/:referenceid", data: { permission: "customer", menuoperations: "ManageMerchant", PageName: 'System.Menu.Merchant', accounttypecode: "customer" }, loadChildren: "../../../../panel/merchant/giftcard/home/tugiftcard.module#TuGiftCardModule" },

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUMerchantRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUMerchantRoutingModule,
        GooglePlaceModule,
        MainPipe,
        HCXAddressManagerModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        ImageCropperModule,
        InputFileModule.forRoot(config)
    ],
    declarations: [TUMerchantComponent]
})
export class TUMerchantModule { }

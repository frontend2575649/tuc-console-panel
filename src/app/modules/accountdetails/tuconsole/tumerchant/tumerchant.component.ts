import { Placeholder } from "@angular/compiler/src/i18n/i18n_ast";
import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, OnDestroy, AfterContentChecked, ViewRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { InputFile, InputFileComponent } from 'ngx-input-file';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, OResponse, OUserDetails, OSelect } from "../../../../service/service";
declare let $: any;
declare var moment: any;
import { HCoreXAddress, HCXAddressConfig, locationType } from '../../../../component/hcxaddressmanager/hcxaddressmanager.component';
import { Select2OptionData } from "ng2-select2";
import { T } from "@angular/cdk/keycodes";
import { TurootcategoryComponent } from "src/app/panel/administration/turootcategory/turootcategory.component";

const HelpImageType: any = {
  Receipt: 'receipt',
  Terminal: 'terminal',
  Serial: 'serial'
}

@Component({
  selector: "tu-merchant",
  templateUrl: "./tumerchant.component.html",
  
})
export class TUMerchantComponent implements OnInit, OnDestroy,AfterContentChecked {
  public SaveAccountRequest: any;
  ShowManager = false;
  public StoreManager_Data: Array<Select2OptionData>;
  public S2StoreManager_Option: Select2Options;
  public mapheight = '200px'

  public _Address: HCoreXAddress = {};
  public _AddressConfig: HCXAddressConfig =
    {
      locationType: locationType.form
    }
  AddressChange(Address) {
    this._Address = Address;
  }

  defaultToDarkFilter = ["grayscale: 100%", "invert: 100%"];
  public ShowCategorySelector: boolean = true;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this._HelperService.ResetDateRange();
  }
  ngAfterContentChecked(): void {
    this._ChangeDetectorRef.detectChanges();
  }
  ngOnDestroy(): void {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    setTimeout(() => {
      if (this._ChangeDetectorRef && !(this._ChangeDetectorRef as ViewRef).destroyed) {
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }


  @ViewChild("terminal")
  private InputFileComponent_Term: InputFileComponent;

  @ViewChild("cashier")
  private InputFileComponent_Cashier: InputFileComponent;

  CurrentImagesCount: number = 0;

  //#region mapCorrection 

  ExpandedView: any = false;

  ToogleExpandedView(): void {
    this.ExpandedView = !this.ExpandedView;
    if (this.ExpandedView) {
    }
  }

  //#endregion

  //#region DetailShowHIde 

  HideStoreDetail() {
    // var element = document.getElementById("StoresDetails");
    // element.classList.add("Hm-HideDiv");
    // element.classList.remove("Hm-ShowStoreDetail");
  }

  ShowStoreDetail() {
    // var element = document.getElementById("StoresDetails");
    // element.classList.add("Hm-ShowStoreDetail");
    // element.classList.remove("Hm-HideDiv");
  }

  resetImage(){
    this.CurrentImagesCount = 0;
this._HelperService.Icon_Crop_Clear();
this.InputFileComponent_Term.files.pop();
}
isSelectStore:boolean=true;
isSelectTerminal:boolean=true;
isSelectBank:boolean=true;
resetSelectOption(){
this.isSelectStore=false;
this.isSelectTerminal=false;
this.isSelectBank=false;
setTimeout(()=>{
  this.isSelectStore=true;
this.isSelectTerminal=true;
this.isSelectBank=true;
},500);

}
  //#endregion

  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  timeout = null;
  ngOnInit() {

    this.timeout = setTimeout(() => {
      this._HelperService.ValidateData();
    }, 1000);


    //#region GetStorageData 

    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveMerchant);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveMerchantReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;

      this.GetMerchantDetails();
      this.GetStateCategories();
    }

    //#endregion

    //#region UIInitialize 

    Feather.replace();
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.ContainerHeight = window.innerHeight;

    this.InitBackDropClickEvent();

    this.HideStoreDetail();

    //#endregion

    //#region InitEditForm 

    this.FormA_EditUser_Load();
    this.FormB_EditUser_Load();
    this.FormC_EditUser_Load();

    //#endregion

    // this.GetMerchantDetails();

    this.InitBackDropClickEvent();
    this.HideStoreDetail();
    this.GetMerchantDetails();
    this.Form_AddUser_Load();
    this.TUTr_Filter_Stores_Load();
    this.TUTr_Filter_Providers_Load();
    this.TUTr_Filter_Banks_Load();
    this.Form_AddStore_Load();
    this.Form_AddCashier_Load();
    this.GetBusinessCategories();
    this.TUTr_Filter_Cashiers_Load();
    this.GetStoreManagerList()

  }
  public SelectedStoreManager = ''
  GetStoreManagerList() {
    this.ShowManager = false;
    var PData =
    {
        Task: 'getstoremanagers',
        AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
        AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, PData);
    _OResponse.subscribe(
        _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
                if (_Response.Result.Data != undefined) {
                    var _Data = _Response.Result.Data as any[];
                    if (_Data.length > 0) {
                        var finalCat = [];
                        this.ShowManager = false;
                        _Data.forEach(element => {
                            var Item = {
                                id: element.ReferenceId,
                                key: element.ReferenceKey,
                                text: element.DisplayName,
                                additional: element,
                            };
                            finalCat.push(Item);
                        });
                        this.S2StoreManager_Option = {
                          placeholder: "Select Store Manager",
                          multiple: false,
                          allowClear: false,
                      };
                        this.StoreManager_Data = finalCat;
                        this.ShowManager = true;
                      
                    }
                    else {
                        this.S2StoreManager_Option = {
                            placeholder: "Select Store Manager",
                            multiple: false,
                            allowClear: false,
                        };
                        setTimeout(() => {
                            this.ShowManager = true;
                        }, 500);
                    }
                }
                else {
                    this.S2StoreManager_Option = {
                        placeholder: "Select Store Manager",
                        multiple: false,
                        allowClear: false,
                    };
                    setTimeout(() => {
                        this.ShowManager = true;
                    }, 500);
                }
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
            this._HelperService.ToggleField = false;

        });
  }

  StoreManagerSelected(Items) {
    this.Form_AddStore.controls["StoreManagerId"].patchValue(Items.data[0].additional.ReferenceId)
    this.Form_AddStore.controls["MEmailAddress"].patchValue(Items.data[0].additional.EmailAddress)
    this.Form_AddStore.controls["MobileNumber"].patchValue(Items.data[0].additional.ContactNumber)
}

  CloseRowModal(): void {
    $("#SubAccountsList_rdropdown").dropdown('toggle');
  }
  CloseRowModal1(): void {
    $("#SubAccountsList_rdropdown1").dropdown('toggle');
  }
  CloseRowModal2(): void {
    $("#SubAccountsList_rdropdown2").dropdown('toggle');
  }
  CloseRowModal3(): void {
    $("#SubAccountsList_rdropdown3").dropdown('toggle');
  }

  onImgError(event){
    event.target.src = 'https://cdn.pixabay.com/photo/2016/10/02/03/15/bill-1708867__480.png'
   //Do other stuff with the event.target
   }
  _ShowGeneralPreview() {
    console.log(this._HelperService._Icon_Cropper_Image);
    this._HelperService.CloseModal("Form_AddUser_Content");
    this._HelperService.OpenModal('_PreviewGeneral');
    this.resetSelectOption();
  }
  _ShowCashier() {
    this._HelperService.CloseModal("Form_AddCashier_Content");
    this._HelperService.OpenModal('_PreviewCashier');

  }

  _ShowGeneralStore() {
    //   if (this.statename == null) {
    //     this._HelperService.NotifyError("Select State");
    //   }
    //   else {
    this._HelperService.CloseModal("Form_AddStore_Content");
    this._HelperService.OpenModal('_PreviewStore');
  }

  // }
  //#region dropdowns 
  public BusinessCategories = [];
  public S2BusinessCategories = [];

  GetBusinessCategories() {

    this._HelperService.ToggleField = true;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getcategories,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      SortExpression: 'Name asc',
      Offset: 0,
      Limit: 1000,
    }
    PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
      PData.SearchCondition,
      "TypeCode",
      this._HelperService.AppConfig.DataType.Text,
      this._HelperService.AppConfig.HelperTypes.MerchantCategories,
      "="
    );
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Op, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            this.BusinessCategories = _Response.Result.Data;

            this.ShowCategorySelector = false;
            this._ChangeDetectorRef.detectChanges();
            for (let index = 0; index < this.BusinessCategories.length; index++) {
              const element = this.BusinessCategories[index];
              this.S2BusinessCategories.push(
                {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name
                }
              );
            }
            this.ShowCategorySelector = true;
            this._ChangeDetectorRef.detectChanges();

            this._HelperService.ToggleField = false;

          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });
  }
  public SelectedBusinessCategories = [];
  CategoriesSelected(Items) {
    if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
      this.SelectedBusinessCategories = Items.value;
    }
    else {
      this.SelectedBusinessCategories = [];
    }


  }
  SaveMerchantBusinessCategory(item) {
    if (item != '0') {
      var Setup =
      {
        Task: this._HelperService.AppConfig.Api.Core.SaveUserParameter,
        TypeCode: this._HelperService.AppConfig.HelperTypes.MerchantCategories,
        // UserAccountKey: this._UserAccount.ReferenceKey,
        CommonKey: item,
        StatusCode: 'default.active'
      };
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, Setup);
      _OResponse.subscribe(
        _Response => {
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess('Business category assigned to merchant');
            // this.BusinessCategories = [];
            // this.GetMerchantBusinessCategories();
          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }
  }

  public TerminalId: string = null;

  public ToggleStoreSelect: boolean = false;
  public TUTr_Filter_Store_Option: Select2Options;
  TUTr_Filter_Stores_Load() {

    this.Form_AddUser.patchValue(
      {
        StoreReferenceId: null,
        StoreReferenceKey: null,
      }


    );
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Store
        }
      ]
    };

    var OwnerKey = this._HelperService.AppConfig.ActiveMerchantReferenceId;
    if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
      OwnerKey = this._HelperService.AppConfig.ActiveMerchantReferenceId;
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, OwnerKey, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Store_Option = {
      placeholder: 'Select Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Stores_Change(event: any) {
    this.Form_AddCashier.patchValue(
      {
        StoreId: event.data[0].ReferenceId,
        StoreKey: event.data[0].ReferenceKey,
        StoreName: event.data[0].DisplayName

      }
    );
    this.Form_AddUser.patchValue(
      {
        StoreReferenceId: event.data[0].ReferenceId,
        StoreReferenceKey: event.data[0].ReferenceKey,
        StoreName: event.data[0].DisplayName

      }


    );


  }
  public TUTr_Filter_Bank_Option: Select2Options;
  TUTr_Filter_Banks_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
        }]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Bank_Option = {
      placeholder: 'Select Bank (Acquirer)',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Banks_Change(event: any) {
    this.Form_AddUser.patchValue(
      {
        AcquirerReferenceId: event.data[0].ReferenceId,
        AcquirerReferenceKey: event.data[0].ReferenceKey,
        BankName: event.data[0].DisplayName

      });
  }
  public TUTr_Filter_Provider_Option: Select2Options;
  TUTr_Filter_Providers_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
        }]
    };

    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Provider_Option = {
      placeholder: 'Select  Provider (PTSP)',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Providers_Change(event: any) {
    this.Form_AddUser.patchValue(
      {
        ProviderReferenceId: event.data[0].ReferenceId,
        ProviderReferenceKey: event.data[0].ReferenceKey,
        PtspName: event.data[0].DisplayName
      });
  }
  public TUTr_Filter_Cashier_Option: Select2Options;
  TUTr_Filter_Cashiers_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetCashiers,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //   SystemName: "AccountTypeCode",
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   SearchCondition: "=",
        //   SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
        // }
      ]
    };


    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Cashier_Option = {
      placeholder: 'Select  Cashier',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Cashiers_Change(event: any) {
    this.Form_AddUser.patchValue(
      {
        CashierReferenceId: event.data[0].ReferenceId,
        CashierReferenceKey: event.data[0].ReferenceKey,
      });
  }

  //#endregion

  //#region MerchantDetails 

  public _UserAccount: OUserDetails = {
    ContactNumber: null,
    SecondaryEmailAddress: null,
    ReferenceId: null,
    BankDisplayName: null,
    BankKey: null,
    SubOwnerAddress: null,
    SubOwnerLatitude: null,
    SubOwnerDisplayName: null,
    SubOwnerKey: null,
    SubOwnerLongitude: null,
    AccessPin: null,
    LastLoginDateS: null,
    AppKey: null,
    AppName: null,
    AppVersionKey: null,
    CreateDate: null,
    CreateDateS: null,
    CreatedByDisplayName: null,
    CreatedByIconUrl: null,
    CreatedByKey: null,
    Description: null,
    IconUrl: null,
    ModifyByDisplayName: null,
    ModifyByIconUrl: null,
    ModifyByKey: null,
    ModifyDate: null,
    ModifyDateS: null,
    PosterUrl: null,
    ReferenceKey: null,
    StatusCode: null,
    StatusI: null,
    StatusId: null,
    StatusName: null,
    AccountCode: null,
    AccountOperationTypeCode: null,
    AccountOperationTypeName: null,
    AccountTypeCode: null,
    AccountTypeName: null,
    Address: null,
    AppVersionName: null,
    ApplicationStatusCode: null,
    ApplicationStatusName: null,
    AverageValue: null,
    CityAreaKey: null,
    CityAreaName: null,
    CityKey: null,
    CityName: null,
    CountValue: null,
    CountryKey: null,
    CountryName: null,
    DateOfBirth: null,
    DisplayName: null,
    EmailAddress: null,
    EmailVerificationStatus: null,
    EmailVerificationStatusDate: null,
    FirstName: null,
    GenderCode: null,
    GenderName: null,
    LastLoginDate: null,
    LastName: null,
    Latitude: null,
    Longitude: null,
    MobileNumber: null,
    Name: null,
    NumberVerificationStatus: null,
    NumberVerificationStatusDate: null,
    OwnerDisplayName: null,
    OwnerKey: null,
    Password: null,
    Reference: null,
    ReferralCode: null,
    ReferralUrl: null,
    RegionAreaKey: null,
    RegionAreaName: null,
    RegionKey: null,
    RegionName: null,
    RegistrationSourceCode: null,
    RegistrationSourceName: null,
    RequestKey: null,
    RoleKey: null,
    RoleName: null,
    SecondaryPassword: null,
    SystemPassword: null,
    UserName: null,
    WebsiteUrl: null
  };

  public _MerchantDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      Latitude: null,
      Longitude: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,

    }

  GetMerchantDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchant,
      // Task: this._HelperService.AppConfig.Api.Core.GetUserAccount,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      //Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveMerchantReferenceKey, '='),
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._MerchantDetails = _Response.Result;

          this._MerchantDetails['ContactPerson'] = {};
          //#region RelocateMarker 

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          // this.FormC_EditUser_Latitude = this._MerchantDetails.Latitude;
          // this.FormC_EditUser_Longitude = this._MerchantDetails.Longitude;
          // this.FormC_EditUser_Latitude = _Response.Result.Latitude;
          // this.FormC_EditUser_Longitude = _Response.Result.Longitude;

          //#endregion

          //#region DatesAndStatusInit 

          this._MerchantDetails.StartDateS = this._HelperService.GetDateS(
            this._MerchantDetails.StartDate
          );
          this._MerchantDetails.EndDateS = this._HelperService.GetDateS(
            this._MerchantDetails.EndDate
          );
          this._MerchantDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._MerchantDetails.CreateDate
          );
          this._MerchantDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._MerchantDetails.ModifyDate
          );
          this._MerchantDetails.StatusI = this._HelperService.GetStatusIcon(
            this._MerchantDetails.StatusCode
          );
          this._MerchantDetails.StatusB = this._HelperService.GetStatusBadge(
            this._MerchantDetails.StatusCode
          );
          this._MerchantDetails.StatusC = this._HelperService.GetStatusColor(
            this._MerchantDetails.StatusCode
          );


          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region EditUser

  //#region EditUserFormA 

  FormA_EditUser: FormGroup;

  FormA_EditUser_Show() {
    this._HelperService.OpenModal("FormA_EditUser_Content");
  }
  FormA_EditUser_Close() {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  FormA_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormA_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      ReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
      OwnerName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
      Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      RewardPercentage: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])]
    });
  }

  FormA_EditUser_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.System,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          this.Forms_EditUser_Close();
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region EditUserFormB 

  FormB_EditUser: FormGroup;
  FormB_EditUser_Address: string = null;
  FormB_EditUser_Latitude: number = 0;
  FormB_EditUser_Longitude: number = 0;


  FormB_EditUser_Show() {
    this._HelperService.OpenModal("FormB_EditUser_Content");
  }

  FormB_EditUser_Close() {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  FormB_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormB_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      ReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,

      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      // LastName: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      SecondaryEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
    });
  }

  FormB_EditUser_Process(_FormValue: any) {
    // _FormValue.DisplayName = _FormValue.FirstName;
    // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
    this.toogleIsFormProcessing(true);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.System,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          if (_FormValue.OperationType == "edit") {
            this.Forms_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region EditUserFormC 

  FormC_EditUser: FormGroup;
  FormC_EditUser_Address: string = null;
  FormC_EditUser_Latitude: number = 0;
  FormC_EditUser_Longitude: number = 0;


  @ViewChild('placesStore') placesStore: GooglePlaceDirective;

  FormC_EditUser_PlaceMarkerClick(event) {
    this.FormC_EditUser_Latitude = event.coords.lat;
    this.FormC_EditUser_Longitude = event.coords.lng;

  }
  public FormC_EditUser_AddressChange(address: Address) {
    this.FormC_EditUser_Latitude = address.geometry.location.lat();
    this.FormC_EditUser_Longitude = address.geometry.location.lng();
    this.FormC_EditUser_Address = address.formatted_address;
  }

  FormC_EditUser_Show() {
    this._HelperService.OpenModal("FormC_EditUser_Content");
  }

  FormC_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormC_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      ReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
      Latitude: 0,
      Longitude: 0,
      Address: [null, Validators.compose([Validators.required, Validators.maxLength(256), Validators.minLength(2)])],
      Configuration: [],
    });
  }

  FormC_EditUser_Process(_FormValue: any) {
    // _FormValue.DisplayName = _FormValue.FirstName;
    // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
    _FormValue.Longitude = this.FormC_EditUser_Longitude
    _FormValue.Latitude = this.FormC_EditUser_Latitude

    this.toogleIsFormProcessing(true);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.System,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          if (_FormValue.OperationType == "edit") {
            this.Forms_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion
  Forms_EditUser_Close() {
    this.GetMerchantDetails();
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  //#endregion

  //#region BackdropDismiss 

  @ViewChild("offCanvas") divView: ElementRef;

  clicked() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

  //#endregion

  //#region Add Terminal 

  Form_AddUser: FormGroup;
  Form_AddUser_Show() {
    this._HelperService.Icon_Crop_Clear();
    this.InitImagePicker(this.InputFileComponent_Term);
    this._HelperService.OpenModal("Form_AddUser_Content");
  }
  Form_AddUser_Close() {
    // this._HelperService.CloseModal("Form_AddUser_Content");
    this._HelperService.CloseModal("_PreviewGeneral");
    this._HelperService.CloseAllModal();
    this.Form_AddUser_Clear();
    this.Form_AddUser_Close();
    this.resetSelectOption();
    this.resetImage();


  }
  Form_AddUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.saveterminal,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      MerchantReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      MerchantReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      StoreReferenceId: [null, Validators.required],
      StoreReferenceKey: [null, Validators.required],
      ProviderReferenceId: [null, Validators.required],
      ProviderReferenceKey: [null, Validators.required],
      AcquirerReferenceId: [null, Validators.required],
      AcquirerReferenceKey: [null, Validators.required],
      // CashierReferenceId: [null],
      // CashierReferenceKey: [null],
      StatusCode: this._HelperService.AppConfig.Status.Active,
      TerminalId: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25)])],
      SerialNumber: null,
      PtspName: null,
      BankName: null,
      StoreName: null,
      MerchantName: null,


    });
  }
  Form_AddUser_Clear() {
    this.Form_AddUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this._HelperService.Icon_Crop_Clear();

    this.Form_AddUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddUser_Process(_FormValue?: any) {
    var _FormValue = this.Form_AddUser.value;
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.Account,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.FlashSwalSuccess("New pos terminal has been added successfully",
            "You have successfully created new POS terminal");
          this._HelperService.ObjectCreated.next(true);
          this.Form_AddUser_Clear();
          this.CurrentImagesCount = 0;
this._HelperService.Icon_Crop_Clear();
this.InputFileComponent_Term.files.pop();
          this.Form_AddUser_Close();
          if (_FormValue.OperationType == "close") {
            this.Form_AddUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region Add Cashier 

  Form_AddCashier: FormGroup;
  Form_AddCashier_Show() {
    this._HelperService.Icon_Crop_Clear();
    this.InitImagePicker(this.InputFileComponent_Cashier);
    this._HelperService.OpenModal("Form_AddCashier_Content");
  }
  Form_AddCashier_Close() {
    // this._HelperService.CloseModal("Form_AddCashier_Content");
    this._HelperService.CloseModal("_PreviewCashier");
    this._HelperService.CloseAllModal();


  }
  Form_AddCashier_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddCashier = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.SaveCashier,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      StoreId: [null, Validators.required],
      StoreKey: [null, Validators.required],
      StatusCode: this._HelperService.AppConfig.Status.Active,
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      EmployeeCode: [null],
      GenderCode: this._HelperService.AppConfig.Gender.Male,
      StoreName: [null],

    });
  }
  Form_AddCashier_Clear() {
    this.Form_AddCashier.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this._HelperService.Icon_Crop_Clear();
    this.Form_AddCashier_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddCashier_Process(_FormValue: any) {
    var _FormValue = this.Form_AddCashier.value;

    this._HelperService.IsFormProcessing = true;

    var Request = this.CreateCashierRequest(_FormValue);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.Account,
      Request
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.FlashSwalSuccess("New cashier has been added successfully",
            "Done! You have successfully created new Cashier");

          this._HelperService.ObjectCreated.next(true);

          this.Form_AddCashier_Clear();
          this.Form_AddCashier_Close();
          this.TUTr_Filter_Cashiers_Load();
          if (_FormValue.OperationType == "close") {
            this.Form_AddCashier_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region Add Store 

  _CurrentAddress: any = {};
  Form_AddStore_Address: string = null;
  Form_AddStore_Latitude: number = 0;
  Form_AddStore_Longitude: number = 0;
  @ViewChild('places') places: GooglePlaceDirective;
  Form_AddStore_PlaceMarkerClick(event) {
    this.Form_AddStore_Latitude = event.coords.lat;
    this.Form_AddStore_Longitude = event.coords.lng;
  }
  public Form_AddStore_AddressChange(address: Address) {

    this.Form_AddStore_Latitude = address.geometry.location.lat();
    this.Form_AddStore_Longitude = address.geometry.location.lng();
    this.Form_AddStore_Address = address.formatted_address;

    this.Form_AddStore.controls['Latitude'].setValue(this.Form_AddStore_Latitude);
    this.Form_AddStore.controls['Longitude'].setValue(this.Form_AddStore_Longitude);
    this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
    if (this._CurrentAddress.country != this._HelperService.UserCountrycode) {
      this._HelperService.NotifyError('Currently we’re not serving in this area, please add locality within ' + this._HelperService.UserCountrycode);
      this.reset();

    }
    else {
      this.Form_AddStore.controls['Address'].setValue(address.formatted_address);
      this.Form_AddStore.controls['MapAddress'].setValue(address.formatted_address);
    }

  }

  reset() {
    this.Form_AddStore.controls['Address'].reset()
    this.Form_AddStore.controls['MapAddress'].reset()
    this.Form_AddStore.controls['CityName'].reset()
    this.Form_AddStore.controls['StateName'].reset()
    this.Form_AddStore.controls['CountryName'].reset()

  }

  Form_AddStore: FormGroup;
  Form_AddStore_Show() {
    this._HelperService.OpenModal("Form_AddStore_Content");
  }
  Form_AddStore_Close() {
    // this._HelperService.CloseModal("Form_AddStore_Content");
    this._HelperService.CloseModal('_PreviewStore');
    this._HelperService.CloseAllModal();
    // location.reload();
    
   


  }
  Form_AddStore_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddStore = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.savestore,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
      Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      StoreManagerId: [null],
      // FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      // LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      MobileNumber: [null],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      MEmailAddress: [null],
      Latitude: 0,
      Longitude: 0,
      StatusCode: this._HelperService.AppConfig.Status.Active,
      CountryName: this._HelperService.UserCountrycode,
    });
  }
  Form_AddStore_Clear() {
    this.Form_AddStore.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this._HelperService.Icon_Crop_Clear();

    this.Form_AddStore_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  
  Form_AddStore_Process(_FormValue?: any) {
    if (!this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "") {
      this._HelperService.NotifyError('Please select business location');
    }
    else {
      var _FormValue = this.Form_AddStore.value;
      if (this.SelectedBusinessCategories != undefined && this.SelectedBusinessCategories.length > 0) {
        _FormValue.Categories = []
        this.SelectedBusinessCategories.forEach(element => {
          _FormValue.Categories.push(
            {
              ReferenceId: element
            }
          )
        });
      }
      // this._HelperService.IsFormProcessing = true;
      this._HelperService.IsFormProcessing = true;
      this.SaveAccountRequest = this.ReFormat_RequestBody();

      let _OResponse: Observable<OResponse>;

      var Request = this.CreateStoreRequest(_FormValue);
      _OResponse = this._HelperService.PostData(
        this._HelperService.AppConfig.NetworkLocation.V3.Account,
        Request
      );
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            
            this._HelperService.FlashSwalSuccess("Store Added successfully", "Done! New Store Has Been Added");
            this._HelperService.ObjectCreated.next(true);
            this.Form_AddStore_Clear();
            this.Form_AddStore_Close();
          
            if (_FormValue.OperationType == "close") {
              this.Form_AddStore_Close();
            }
            location.reload();
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );

    }
  }

  //#endregion



  public citykey: any; public cityid: any; cityname: any;
  public cityareaid: any;
  public cityareacode: any;
  public cityareaname: any;
  // public countryname: any;
  // public countryid: any;
  // public countrycode: any;

  CreateStoreRequest(_FormValue: any): void {
    _FormValue.ContactPerson = {
      // FirstName: _FormValue.FirstName,
      // LastName: _FormValue.LastName,
      MobileNumber: _FormValue.MobileNumber,
      EmailAddress: _FormValue.MEmailAddress
    }
    _FormValue.Address = this._Address.Address;
    _FormValue.AddressComponent = this._Address;
    _FormValue.FirstName = undefined;
    _FormValue.LastName = undefined;
    _FormValue.MobileNumber = undefined;
    _FormValue.Latitude = undefined;
    _FormValue.Longitude = undefined;
    _FormValue.MapAddress = undefined;

    return _FormValue;
  }

  CreateCashierRequest(_FormValue: any): void {

    var IconContent: any = undefined;

    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      IconContent = this._HelperService._Icon_Cropper_Data;
    }

    _FormValue.IconContent = IconContent;

    return _FormValue;
  }


  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
    //    this._ChangeDetectorRef.detectChanges();
  }

  FormA_EditUser_Block() {

  }
  FormC_EditUser_Block() {

  }

  FormB_EditUser_Block() {

  }

  private InitImagePicker(InputFileComponent: InputFileComponent) {
    if (InputFileComponent != undefined) {
      this.CurrentImagesCount = 0;
      this._HelperService._InputFileComponent = InputFileComponent;
      InputFileComponent.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
  }
  OpenModel() {
    this._HelperService.CloseModal('Form_AddHelp_Content');

    this._HelperService.OpenModal('Form_AddUser_Content');


  }

  HelpImageUrl: string;
  ShowHelpImage(HelpImage: string): void {
    this._HelperService.CloseModal('Form_AddUser_Content');

    switch (HelpImage) {
      case HelpImageType.Receipt: {
        this.HelpImageUrl = '../../../../../assets/img/receipt.png';
        this._HelperService.OpenModal("Form_AddHelp_Content");
      }

        break;

      case HelpImageType.Terminal: {
        this.HelpImageUrl = '../../../../../assets/img/terminalid.png';
        this._HelperService.OpenModal('Form_AddHelp_Content');
      }
        break;

      case HelpImageType.Serial: {
        this.HelpImageUrl = '../../../../../assets/img/serial.png';
        this._HelperService.OpenModal('Form_AddHelp_Content');
      }

        break;

      default:
        break;
    }
  }

  //state
  public StateCategories = [];
  public S2StateCategories = [];
  public stateerrro: boolean = false;
  public ShowstateSelector: boolean = true;
  public ShowcitySelector: boolean = true;
  GetStateCategories() {
    if (this.SelectedCityCategories != undefined && this.SelectedCityCategories.length > 0) {
      this.S2CityCategories = [];
    }
    this._HelperService.ToggleField = true;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getstates,
      ReferenceKey: this._HelperService.UserCountrykey,
      ReferenceId: this._HelperService.UserCountryId,
      //SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      // SortExpression: 'Name asc',
      Offset: 0,
      Limit: 1000,
    }
    // PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
    //     PData.SearchCondition,
    //     "TypeCode",
    //     this._HelperService.AppConfig.DataType.Text,
    //     this._HelperService.AppConfig.HelperTypes.MerchantCategories,
    //     "="
    // );
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.State, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            this.StateCategories = _Response.Result.Data;

            this.ShowstateSelector = false;
            this._ChangeDetectorRef.detectChanges();
            this.S2StateCategories.push(
              {
                id: 0,
                key: "0",
                text: "Select State"
              }
            );


            for (let index = 0; index < this.StateCategories.length; index++) {

              const element = this.StateCategories[index];
              this.S2StateCategories.push(
                {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name
                }
              );
            }
            this.ShowstateSelector = true;
            this._ChangeDetectorRef.detectChanges();

            this._HelperService.ToggleField = false;

          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });
  }
  public SelectedStateCategories = [];
  public selectedstate: boolean = false;
  statekey: any; stateid: any; statename: any;
  StateSelected(Items) {
    if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
      this.SelectedStateCategories = Items.value;
      this.statekey = Items.data[0].key;
      this.stateid = Items.data[0].id;
      this.statename = Items.data[0].text;
      this.selectedstate = true;
      this.GetCityCategories()
      // this.Form_AddStore.controls['StateCode'].patchValue(this.statekey);
      // this.Form_AddStore.controls['StateId'].patchValue(this.stateid);
      // this.Form_AddStore.controls['StateName'].patchValue(this.statename);
    }
    else {
      this.SelectedStateCategories = [];
    }
  }


  //City
  public CityCategories = [];
  public S2CityCategories = [];

  GetCityCategories() {
    this._HelperService.ToggleField = true;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getcities,
      ReferenceKey: this.statekey,
      ReferenceId: this.stateid,
      //SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      // SortExpression: 'Name asc',
      Offset: 0,
      Limit: 1000,
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.City, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            this.CityCategories = _Response.Result.Data;
            this.ShowcitySelector = false;
            this.S2CityCategories = [];
            this._ChangeDetectorRef.detectChanges();
            for (let index = 0; index < this.CityCategories.length; index++) {
              const element = this.CityCategories[index];
              this.S2CityCategories.push(
                {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name
                }
              );
            }
            this.ShowcitySelector = true;
            this._ChangeDetectorRef.detectChanges();

            this._HelperService.ToggleField = false;

          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });



  }
  public SelectedCityCategories = [];
  // public citykey: any; public cityid: any; cityname: any;
  CitySelected(Items) {
    if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
      this.SelectedCityCategories = Items.value;
      this.citykey = Items.data[0].key;
      this.cityid = Items.data[0].id
      this.cityname = Items.data[0].text
      // this.Form_AddStore.controls['CityCode'].patchValue(this.citykey);
      // this.Form_AddStore.controls['CityId'].patchValue(this.cityid);
      // this.Form_AddStore.controls['CityName'].patchValue(this.cityname);
    }
    else {
      this.SelectedCityCategories = [];
    }
  }


  ReFormat_RequestBody(): void {
    var formValue: any = this.Form_AddStore.value;
    var formRequest: any = {
      OperationType: 'new',
      Task: formValue.Task,
      DisplayName: formValue.DisplayName,
      Name: formValue.Name,
      ContactNumber: formValue.ContactNumber,
      EmailAddress: formValue.EmailAddress,
      RewardPercentage: formValue.RewardPercentage,
      WebsiteUrl: formValue.WebsiteUrl,
      ReferralCode: formValue.ReferralCode,
      Description: formValue.Description,
      UserName: formValue.UserName,
      Password: formValue.Password,
      StatusCode: formValue.StatusCode,
      ContactPerson: {
        FirstName: formValue.FirstName,
        LastName: formValue.LastName,
        MobileNumber: formValue.MobileNumber,
        EmailAddress: formValue.EmailAddress
      },
      IconContent: formValue.IconContent
    };
    return formRequest;

  }
}
export class OAccountOverview {
  public Merchants: number;
  public Stores: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable, Subscription } from "rxjs";
import { DataHelperService, HelperService, OResponse, OSelect, OUserDetails, FilterHelperService } from "../../../../service/service";
import { InputFileComponent, InputFile } from 'ngx-input-file';
declare let $: any;

@Component({
  selector: "tu-tucambassador",
  templateUrl: "./tucambassador.component.html"
})
export class TUCAmbassadorComponent implements OnInit, OnDestroy {
  public _CashierAddress: any = {};
  ToggleStoreSelect: number;

  @ViewChild("inputfile")
  private InputFileComponent: InputFileComponent;

  CurrentImagesCount: number;

  //#region subscriptions 

  subscription: Subscription;
  ReloadSubscription: Subscription;

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.ReloadSubscription.unsubscribe();
  }

  //#endregion

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {

  }

  //#region MapCorrection 

  slideOpen: any = false;
  changeSlide(): void {
    this.slideOpen = !this.slideOpen;
    if (this.slideOpen) {
    }
  }

  //#endregion

  //#region ToogleDetailView 

  HideStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-ShowStoreDetail");
  }

  ShowStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-ShowStoreDetail");
    element.classList.remove("Hm-HideDiv");
  }

  //#endregion



  ngOnInit() {
    //#region UIInit 
    Feather.replace();

    this._HelperService.ContainerHeight = window.innerHeight;
    this._HelperService.AppConfig.ShowHeader = true;
    //#endregion
    //#region Subscriptions 
    this.subscription = this._HelperService.isprocessingtoogle
      .subscribe((item) => {
        this._ChangeDetectorRef.detectChanges();
      });
    this.ReloadSubscription = this._HelperService.ReloadEventEmit.subscribe((number) => {

    });
    //#endregion
    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveProduct);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }
    //#region DropdownInit 
    this.Get_UserProductDetails();
    this.GetProductCategories_List();
    this.CurrentImagesCount = 0;
    this._HelperService.Icon_Crop_Clear();
    this._HelperService._InputFileComponent = this.InputFileComponent;
    this.InputFileComponent.onChange = (files: Array<InputFile>): void => {
      if (files.length >= this.CurrentImagesCount) {
        this._HelperService._SetFirstImageOrNone(this.InputFileComponent.files);
      }
      this.CurrentImagesCount = files.length;
    };

  }



  //#region CashierDetails 

  public _ProductDetail: any = {};
  public Get_UserProductDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: "getninjaregistration",
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._ProductDetail = _Response.Result as OProductDetail;
          this._ProductDetail.CreateDate = this._HelperService.GetDateS(this._ProductDetail.CreateDate);
          this._ProductDetail.CreateDateS = this._HelperService.GetDateTimeS(this._ProductDetail.CreateDate);
          this._ProductDetail.ModifyDateS = this._HelperService.GetDateTimeS(this._ProductDetail.ModifyDate);
          this._ProductDetail.StatusI = this._HelperService.GetStatusIcon(this._ProductDetail.StatusCode);
          this._ProductDetail.StatusB = this._HelperService.GetStatusBadge(this._ProductDetail.StatusCode);
          this._ProductDetail.StatusC = this._HelperService.GetStatusColor(this._ProductDetail.StatusCode);
          // this.GetProductCategories_Option.placeholder = this._ProductDetail.CategoryName;
          if (this._ProductDetail.CreatedByDisplayName != undefined) {
            if (this._ProductDetail.CreatedByDisplayName.length > 8) {
              this._ProductDetail.CreatedByDisplayNameShort = this._ProductDetail.CreatedByDisplayName.substring(0, 8) + '..';
            }
            else {
              this._ProductDetail.CreatedByDisplayNameShort = this._ProductDetail.CreatedByDisplayName;
            }
          }
          if (this._ProductDetail.ModifyByDisplayName != undefined) {
            if (this._ProductDetail.ModifyByDisplayName.length > 8) { this._ProductDetail.ModifyByDisplayNameShort = this._ProductDetail.ModifyByDisplayName.substring(0, 8) + '..' } else {
              this._ProductDetail.ModifyByDisplayNameShort = this._ProductDetail.ModifyByDisplayName;
            };
          }

          // this.Form_Product.controls['AllowMultiple'].setValue((this._ProductDetail.AllowMultiple == 1) ? true : false);
          this._HelperService.IsFormProcessing = false;
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion
  OpenEditFile() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Product.EditProducts
      ,
    ]);
  }
  OpenUpdateProduct() {
    this._HelperService.OpenModal('ModalUpdateProduct');
  }
  UpdateProduct() {
    if (this._ProductDetail.Name == undefined || this._ProductDetail.Name == null || this._ProductDetail.Name == "") {
      this._HelperService.NotifyError("Enter product name");
    }
    else if (this._ProductDetail.Description == undefined || this._ProductDetail.Description == null || this._ProductDetail.Description == "") {
      this._HelperService.NotifyError("Enter product information/ description");
    }
    else {

      this._HelperService.IsFormProcessing = true;
      var pData = {
        Task: "updateproduct",
        ReferenceId: this._ProductDetail.ReferenceId,
        ReferenceKey: this._ProductDetail.ReferenceKey,
        Name: this._ProductDetail.Name,
        Description: this._ProductDetail.Description,
        IconContent: null,
        CategoryKey: this.CategoryKey
      };
      if (this._HelperService._Icon_Cropper_Data.Content != null) {
        pData.IconContent = this._HelperService._Icon_Cropper_Data;
      }
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, pData);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.CloseModal('ModalUpdateProduct');
            this.Get_UserProductDetails();
            this._HelperService.NotifySuccess("Product details updated");
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.HandleException(_Error);
        }
      );
    }
  }

  UpdateProductStatus(StatusCode) {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: "updateproduct",
      ReferenceId: this._ProductDetail.ReferenceId,
      ReferenceKey: this._ProductDetail.ReferenceKey,
      StatusCode: StatusCode
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.HCProduct, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.Get_UserProductDetails();
          this._HelperService.NotifySuccess("Product details updated");
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


  public GetProductCategories_Option: Select2Options;
  GetProductCategories_List() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Product.getcategories,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.HCProduct,
      SearchCondition: '',
      SortCondition: ['Name asc'],
      Fields: [
        {
          SystemName: 'ReferenceKey',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: 'Name',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        {
          SystemName: 'StatusCode',
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: '=',
          SearchValue: this._HelperService.AppConfig.Status.Active,
        }
      ],
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'ReferenceId', this._HelperService.AppConfig.DataType.Number, '0', '>');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;

    this.GetProductCategories_Option = {
      placeholder: PlaceHolder,
      ajax: _Transport,
      multiple: false,
    };
  }
  public CategoryKey: string;
  public CategoryId: number;

  GetProductCategories_ListChange(event: any) {
    this.CategoryKey = event.data[0].ReferenceKey;
    this.CategoryId = event.data[0].ReferenceId
  }

}

export class OProductDetail {
  public ReferenceId: number;
  public ReferenceKey: string;
  public IconUrl: string;
  public AccountId: number;
  public AccountKey: string;
  public AccountDisplayName: string;
  public Sku: string;
  public Name: string;
  public SystemName: string;
  public Description: string;
  public MinumumAmount: number;
  public MaximumAmount: number;
  public MinimumQuantity: number;
  public MaximumQuantity: number;
  public ActualPrice: number;
  public SellingPrice: number;
  public AllowMultiple: number;
  public ReferenceNumber: string;
  public RewardPercentage: number;
  public MaximumRewardAmount: number;
  public TotalStock: number;
  public CreateDate: string;
  public CreatedByKey: string;
  public CreatedByDisplayName: string;
  public CreatedByDisplayNameShort?: string;
  public StatusId: number;
  public StatusCode: string;
  public StatusName: string;

  public ModifyDate: Date;
  public ModifyByKey: string;
  public ModifyByDisplayName: string;
  public ModifyByDisplayNameShort?: string;

  public StatusI: string;
  public StatusB?: string;
  public StatusC?: string;
  public CreateDateS: string;
  public ModifyDateS: string;
}

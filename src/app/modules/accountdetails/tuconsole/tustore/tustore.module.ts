import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { TUStoreComponent } from "./tustore.component";
import { HCXAddressManagerModule } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';
import { DynamicRoutesguardGuard } from "src/app/service/guard/dynamicroutes.guard";


const routes: Routes = [
    {
        path: "",
        component: TUStoreComponent,
        canActivate:[DynamicRoutesguardGuard],
        data:{accessName:['storeview']},
        children: [
            // { path: "/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../dashboards/store/dashboard.module#TUDashboardModule" },
            { path: "/:referencekey/:referenceid", data: { permission: "salehistory", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../../modules/transactions/tusale/store/tusale.module#TUSaleModule" },
            // { path: "dashboard/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../dashboards/store/dashboard.module#TUDashboardModule" },
            { path: "dashboard/:referencekey/:referenceid", data: { permission: "salehistory", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../../modules/transactions/tusale/store/tusale.module#TUSaleModule" },
            { path: '/:referencekey/:referenceid/:merchantkey/:merchantid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Store' }, loadChildren: '../../../../modules/transactions/tusale/store/tusale.module#TUSaleModule' },
            { path: 'sales/saleshistory/:referencekey/:referenceid/:merchantkey/:merchantid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Store' }, loadChildren: '../../../../modules/transactions/tusale/store/tusale.module#TUSaleModule' },
            { path: 'terminals/:referencekey/:referenceid', data: { 'permission': 'terminals', PageName: 'System.Menu.Store' }, loadChildren: '../../../../modules/accounts/tuconsole/tuterminals/store/tuterminals.module#TUTerminalsModule' },
            { path: 'cashiers/:referencekey/:referenceid', data: { 'permission': 'terminals', PageName: 'System.Menu.Store' }, loadChildren: '../../../../modules/accounts/tuconsole/cashiers/Store/tucashiers.module#TUCashiersModule' },

        ]

    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUStoreRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUStoreRoutingModule,
        GooglePlaceModule,
        HCXAddressManagerModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    declarations: [TUStoreComponent]
})
export class TUStoreModule { }

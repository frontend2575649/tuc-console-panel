import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable, Subscription } from "rxjs";
import { DataHelperService, HelperService, OResponse, OSelect, OUserDetails, FilterHelperService } from "../../../../service/service";
import { HCoreXAddress, HCXAddressConfig, locationType } from '../../../../component/hcxaddressmanager/hcxaddressmanager.component';

declare let $: any;
import swal from 'sweetalert2';
import { Select2OptionData } from "ng2-select2";

@Component({
  selector: "tu-store",
  templateUrl: "./tustore.component.html"
})
export class TUStoreComponent implements OnInit, OnDestroy {
  isLoaded: boolean = true;
  public _isAddressLoaded = false;
  public _Address: HCoreXAddress = {};
  public _AddressConfig: HCXAddressConfig =
    {
      locationType: locationType.form,
    }
  AddressChange(Address) {
    this._Address = Address;
  }

  public ShowCategorySelector: boolean = true;
  public _StoreContactPerson: any = {};

  ShowManager = false;
  public StoreManager_Data: Array<Select2OptionData>;
  public S2StoreManager_Option: Select2Options;
  //#region subscriptions 

  subscription: Subscription;
  ReloadSubscription: Subscription;
  public SelectedStoreManager: any


  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.ReloadSubscription.unsubscribe();
    setTimeout(() => {
      if (this._ChangeDetectorRef && !(this._ChangeDetectorRef as ViewRef).destroyed) {
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  //#endregion

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {

  }

  //#region ToogleDetailView 

  HideStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-ShowStoreDetail");
  }

  ShowStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-ShowStoreDetail");
    element.classList.remove("Hm-HideDiv");
  }

  //#endregion

  BackDropInit(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  ngOnInit() {
    //#region UIInit 
    setTimeout(() => {
      this._HelperService.ValidateData();
    }, 500);
    Feather.replace();
    this._HelperService.ContainerHeight = window.innerHeight;
    this._HelperService.AppConfig.ShowHeader = true;
    this.BackDropInit();
    //#endregion
    //#region Subscriptions 
    this.subscription = this._HelperService.isprocessingtoogle
      .subscribe((item) => {
        this._ChangeDetectorRef.detectChanges();
      });

    this.ReloadSubscription = this._HelperService.ReloadEventEmit.subscribe((number) => {

    });

    //#endregion

    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveStore);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveStoreReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveStoreReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = StorageDetails.MerchantKey;
      this._HelperService.AppConfig.ActiveMerchantReferenceId = StorageDetails.MerchantId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }

    var MerchantDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveMerchant);
    if (MerchantDetails != null) {
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = MerchantDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveMerchantReferenceId = MerchantDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = MerchantDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = MerchantDetails.AccountTypeCode;
    }
    // this._HelperService.Get_UserAccountDetails(false);

    //#region DropdownInit 

    this.GetSoresDetails();


    //#endregion

    this.FormA_EditUser_Load();
    this.FormB_EditUser_Load();
    this.FormD_EditUser_Load();
    this.GetBusinessCategories();
    this.GetStoreManagerList();
    this.Form_UpdateCredentialUser_Load();

  }

  //#region OffCanvasNBackdrop 
  @ViewChild("offCanvas") divView: ElementRef;

  ShowOffCanvas() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  RemoveOffCanvas() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }
  //#endregion

  //#region StoreDetails 

  public _StoreDetails: any =
    {
      ManagerName: null,
      BranchName: null,
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      Address: null,
      AddressComponent:
      {
        Latitude: 0,
        Longitude: 0,
      },
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }
  GetSoresDetails() {

    this._HelperService.IsFormProcessing = true;
    this.isLoaded = false;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStore,
      AccountKey: this._HelperService.AppConfig.ActiveStoreReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveStoreReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this.isLoaded = true;

          this._StoreDetails = _Response.Result;
          this._StoreDetails.UpdateCategories = [];
          for (let index = 0; index < this._StoreDetails.Categories.length; index++) {
            const element = this._StoreDetails.Categories[index];
            this._StoreDetails.UpdateCategories.push({
              Name: element.Name,
              ReferenceKey: element.ReferenceKey,
              ReferenceId: element.ReferenceId
            });
          }
          this._Address = this._StoreDetails.AddressComponent;
          this._isAddressLoaded = true;
          this._StoreDetails.CreateDate = this._HelperService.GetDateS(this._StoreDetails.CreateDate);
          this._StoreDetails.ModifyDateS = this._HelperService.GetDateS(this._StoreDetails.ModifyDate);
          this._StoreDetails.StatusI = this._HelperService.GetStatusIcon(this._StoreDetails.StatusCode);
          this._StoreDetails.StatusB = this._HelperService.GetStatusBadge(this._StoreDetails.StatusCode);
          this._StoreDetails.StatusC = this._HelperService.GetStatusColor(this._StoreDetails.StatusCode);
          this._StoreContactPerson = this._StoreDetails.ContactPerson;
          if (this._StoreDetails.StoreManagerId) {
            this.SelectedStoreManager = this._StoreDetails.StoreManagerId
          } else {
            this.SelectedStoreManager = null
          }

          if (this._StoreDetails.ContactPerson != undefined && this._StoreDetails.ContactPerson.MobileNumber != undefined && this._StoreDetails.ContactPerson.MobileNumber != null) {
            if (this._StoreDetails.ContactPerson.MobileNumber.startsWith("234") || this._StoreDetails.ContactPerson.MobileNumber.startsWith("233") || this._StoreDetails.ContactPerson.MobileNumber.startsWith("254")) {
              this._StoreDetails.ContactPerson.MobileNumber = this._StoreDetails.ContactPerson.MobileNumber.substring(3, this._StoreDetails.ContactPerson.length);
            }
          }
          if (this._StoreDetails != undefined && this._StoreDetails.ContactNumber != undefined && this._StoreDetails.ContactNumber != null) {
            if (this._StoreDetails.ContactNumber.startsWith("234") || this._StoreDetails.ContactNumber.startsWith("233") || this._StoreDetails.ContactNumber.startsWith("254")) {
              this._StoreDetails.ContactNumber = this._StoreDetails.ContactNumber.substring(3, this._StoreDetails.length);
            }
          }
          this._ChangeDetectorRef.detectChanges();
          //#region ResponseInit 
          //#endregion
          //#endregion
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  GetStoreManagerList() {
    this.ShowManager = false;
    var PData =
    {
      Task: 'getstoremanagers',
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            var _Data = _Response.Result.Data as any[];
            if (_Data.length > 0) {
              var finalCat = [];
              this.ShowManager = false;
              _Data.forEach(element => {
                var Item = {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.DisplayName,
                  additional: element,
                };
                finalCat.push(Item);
              });
              this.S2StoreManager_Option = {
                placeholder: "Select Store Manager",
                multiple: false,
                allowClear: false,
              };
              this.StoreManager_Data = finalCat;
              this.ShowManager = true;

            }
            else {
              this.S2StoreManager_Option = {
                placeholder: "Select Store Manager",
                multiple: false,
                allowClear: false,
              };
              setTimeout(() => {
                this.ShowManager = true;
              }, 500);
            }
          }
          else {
            this.S2StoreManager_Option = {
              placeholder: "Select Store Manager",
              multiple: false,
              allowClear: false,
            };
            setTimeout(() => {
              this.ShowManager = true;
            }, 500);
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });
  }

  StoreManagerSelected(Items) {
    this.FormB_EditUser.controls["StoreManagerId"].patchValue(Items.data[0].additional.ReferenceId)
    this.FormB_EditUser.controls["EmailAddress"].patchValue(Items.data[0].additional.EmailAddress)
    this.FormB_EditUser.controls["MobileNumber"].patchValue(Items.data[0].additional.ContactNumber)
  }

  AccoutSettings() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Settings
    ]);
  }

  //#endregion

  //#region EditStore

  //#region EditUserFormA 

  FormA_EditUser: FormGroup;

  FormA_EditUser_Show() {
    this._HelperService.OpenModal("FormA_EditUser_Content");
  }
  FormA_EditUser_Close() {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  FormA_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;
    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormA_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      AccountKey: this._HelperService.AppConfig.ActiveStoreReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveStoreReferenceId,
      Task: this._HelperService.AppConfig.Api.ThankUCash.updatestore,
      DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
      Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$'), Validators.minLength(2)])]
    });
  }

  FormA_EditUser_Process(_FormValue: any) {
    // this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    var request = this.CreateRequestJson();
    if (request.Address.CityId < 1) {
      this._HelperService.NotifyError("Please locate address of store");
    }
    else {
      swal({
        position: "center",
        title: "Are you sure you want to update details ?",
        text: "Click on Save to update details",
        animation: false,
        customClass: this._HelperService.AppConfig.Alert_Animation,
        showCancelButton: true,
        confirmButtonColor: this._HelperService.AppConfig.Color_Green,
        cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
        confirmButtonText: "Save",
        cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      }).then((result) => {
        if (result.value) {
          this._HelperService.IsFormProcessing = true;
          _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, request);
          _OResponse.subscribe(
            _Response => {
              this.toogleIsFormProcessing(false);
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.NotifySuccess("Store details updated successfully");
                this.GetSoresDetails();
                this.Forms_EditUser_Close();
              } else {
                this._HelperService.NotifyError(_Response.Message);
              }
            },
            _Error => {
              this.toogleIsFormProcessing(false);
              this._HelperService.HandleException(_Error);
            }
          );
        }
        else {
          this.toogleIsFormProcessing(false);
        }
      })
    }
  }

  //#endregion

  //#region EditUserFormB 

  FormB_EditUser: FormGroup;
  FormB_EditUser_Address: string = null;
  FormB_EditUser_Latitude: number = 0;
  FormB_EditUser_Longitude: number = 0;


  FormB_EditUser_Show() {
    this._HelperService.OpenModal("FormB_EditUser_Content");
  }

  FormB_EditUser_Close() {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  FormB_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormB_EditUser = this._FormBuilder.group({
      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      StoreManagerId: [null, Validators.required],
      // FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128),Validators.pattern("^[a-zA-Z _-]*$") ])],
      // LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128), Validators.pattern("^[a-zA-Z _-]*$")])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.pattern(this._HelperService.AppConfig.EmailPattern), Validators.minLength(2)])],
    });
  }

  FormB_EditUser_Process(_FormValue: any) {
    var request = this.CreateRequestJson();
    this.toogleIsFormProcessing(true);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.Account,
      request
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          if (_FormValue.OperationType == "edit") {
            this.Forms_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region EditUserFormC 

  FormC_EditUser_Process(_FormValue: any) {
    if (this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "") {
      this._HelperService.NotifyError("Please select address");
    }
    else {
      // _FormValue.DisplayName = _FormValue.FirstName;
      // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
      var request = this.CreateRequestJson();
      _FormValue.Location = this._Address;
      this.toogleIsFormProcessing(true);
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(
        this._HelperService.AppConfig.NetworkLocation.V3.Account,
        request
      );
      _OResponse.subscribe(
        _Response => {
          this.toogleIsFormProcessing(false);
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess("Account updated successfully");
            if (_FormValue.OperationType == "edit") {
              this.Forms_EditUser_Close();
            }
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this.toogleIsFormProcessing(false);
          this._HelperService.HandleException(_Error);
        }
      );
    }
  }

  //#endregion

  //#endregion
  CreateRequestJson(): any {
    var AccountDetail = this.FormA_EditUser.value;
    AccountDetail.Categories = [];
    if (this.SelectedBusinessCategories != undefined && this.SelectedBusinessCategories.length > 0) {
      this.SelectedBusinessCategories.forEach(element => {
        if (!this.DoesCatAlreadyExist(element)) {
          AccountDetail.Categories.push(
            {
              ReferenceId: element,
            }
          )
        }
      });
    }
    AccountDetail.Address = this._Address.Address;
    AccountDetail.AddressComponent = this._Address;
    AccountDetail.Username = this.FormD_EditUser.value.Username;
    AccountDetail.Password = this.FormD_EditUser.value.Password;
    AccountDetail.Categories = AccountDetail.Categories.concat(this._StoreDetails.UpdateCategories);
    // AccountDetail.ContactPerson = this.FormB_EditUser.value;
    AccountDetail.StoreManagerId = this.FormB_EditUser.controls['StoreManagerId'].value
    AccountDetail.EmailAddress = this._HelperService.lowerCaseConvert(AccountDetail.EmailAddress),
      AccountDetail.AccountKey = this._StoreDetails.ReferenceKey;
    AccountDetail.AccountId = this._StoreDetails.ReferenceId;
    return AccountDetail;
  }

  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
  }

  Forms_EditUser_Close() {
    // this.GetMerchantDetails();
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }

  Form_EditUser_Block() {

  }

  public BusinessCategories = [];
  public S2BusinessCategories = [];

  GetBusinessCategories() {
    this._HelperService.ToggleField = true;

    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getcategories,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      SortExpression: 'Name asc',
      Offset: 0,
      Limit: 1000,
    }
    PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
      PData.SearchCondition,
      "TypeCode",
      this._HelperService.AppConfig.DataType.Text,
      this._HelperService.AppConfig.HelperTypes.MerchantCategories,
      "="
    );
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Op, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            this.BusinessCategories = _Response.Result.Data;

            this.ShowCategorySelector = false;
            this._ChangeDetectorRef.detectChanges();
            for (let index = 0; index < this.BusinessCategories.length; index++) {
              const element = this.BusinessCategories[index];
              this.S2BusinessCategories.push(
                {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name
                }
              );
            }
            this.ShowCategorySelector = true;
            this._ChangeDetectorRef.detectChanges();


            this._HelperService.ToggleField = false;
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;
      });
  }
  public SelectedBusinessCategories = [];
  CategoriesSelected(Items) {

    if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
      this.SelectedBusinessCategories = Items.value;
    }
    else {
      this.SelectedBusinessCategories = [];
    }

  }
  SaveMerchantBusinessCategory(item) {
    if (item != '0') {
      var Setup =
      {
        Task: this._HelperService.AppConfig.Api.Core.SaveUserParameter,
        TypeCode: this._HelperService.AppConfig.HelperTypes.MerchantCategories,
        // UserAccountKey: this._UserAccount.ReferenceKey,
        CommonKey: item,
        StatusCode: 'default.active'
      };
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, Setup);
      _OResponse.subscribe(
        _Response => {
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess('Business category assigned to merchant');
            // this.BusinessCategories = [];
            // this.GetMerchantBusinessCategories();
          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }
  }

  removeCategory(cat: any): void {
    this._StoreDetails.UpdateCategories.splice(cat, 1);
  }

  DoesCatAlreadyExist(Id: number): boolean {
    for (let index = 0; index < this._StoreDetails.UpdateCategories.length; index++) {
      const element = this._StoreDetails.UpdateCategories[index];
      if (element.ReferenceId == Id) {
        return true;
      }
    }
    return false;
  }

  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

  BlockStore() {
    this._HelperService.OpenModal("BlockPos");
  }
  public Block(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "disablestore",
      AccountId: this._StoreDetails.ReferenceId,
      AccountKey: this._StoreDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess("Store Disabled Successfully ");
          this._HelperService.CloseModal("BlockPos");
          this.GetSoresDetails();

        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public UnBlock(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "enablestore",
      AccountId: this._StoreDetails.ReferenceId,
      AccountKey: this._StoreDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess("Store Enabled Successfully ");
          this._HelperService.CloseModal("BlockPos");
          this.GetSoresDetails();

        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  FormD_EditUser: FormGroup;
  FormD_EditUser_Address: string = null;
  FormD_EditUser_Latitude: number = 0;
  FormD_EditUser_Longitude: number = 0;


  FormD_EditUser_Show() {
    this._HelperService.OpenModal("FormB_EditUser_Content");
  }

  FormD_EditUser_Close() {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  FormD_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormD_EditUser = this._FormBuilder.group({
      // OperationType: 'new',
      // Task: this._HelperService.AppConfig.Api.ThankUCash.updatestore,

      Username: [null, Validators.required],
      Password: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(50),
          Validators.pattern(
            this._HelperService.AppConfig.ValidatorRegex.Password
          )
        ])
      ],

    });
  }

  FormD_EditUser_Process(_FormValue: any) {

    var request = this.CreateRequestJson();

    this.toogleIsFormProcessing(true);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.Account,
      request
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          if (_FormValue.OperationType == "edit") {
            this.Forms_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }


  //#region Expand/Collapse Details 

  ExpandedView: any = false;

  ToogleExpandedView(): void {
    this.ExpandedView = !this.ExpandedView;
  }

  //#endregion


  //#region Credential
  Form_UpdateCredentialUser: FormGroup;
  Form_UpdateCredentialUser_Show() {
  }
  Form_UpdateCredentialUser_Close() {
    this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreUsers]);
  }
  Form_UpdateCredentialUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_UpdateCredentialUser = this._FormBuilder.group({
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      // AccountTypeCode: this._HelperService.AppConfig.AccountType.Admin,
      // AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Online,
      // RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      OwnerKey: null,
      RoleKey: null,
      UserName: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(256)])],
      Description: null,
      ReferralCode: null,
      ReferralUrl: null,

      CountValue: 0,
      AverageValue: 0,

      ApplicationStatusCode: null,

      EmailVerificationStatus: 0,
      EmailVerificationStatusDate: null,

      NumberVerificationStatus: 0,
      NumberVerificationStatusDate: null,

      CountryKey: null,
      RegionKey: null,
      RegionAreaKey: null,
      CityKey: null,
      CityAreaKey: null,

      IconContent: this._HelperService._FileSelect_Icon_Data,
      PosterContent: this._HelperService._FileSelect_Poster_Data,

      Owners: [],
      Configuration: [],
    });
  }
  Form_UpdateCredentialUser_Clear() {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_UpdateCredentialUser_Load();
  }
  Form_UpdateCredentialUser_Process(_FormValue: any) {
    swal({
      position: 'top',
      title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
      // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Primary,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        // _FormValue.AuthPin = result.value;
        _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this.GetSoresDetails();
              this._HelperService.NotifySuccess('Account details updated');
            }
            else {
              this._HelperService.NotifyError('Unable to udpate account details');
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          });
      }
    });

  }
  //#endregion


  public ShowPasswordDiv: boolean = false;
  ShowPassword() {
    this.ShowPasswordDiv = !this.ShowPasswordDiv;
  }
  public Password = null;
  ResetPassword() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: 'resetmerchantpassword',
      AccountKey: this._HelperService.AppConfig.ActiveStoreReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveStoreReferenceId
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.Password = _Response.Result;
          this.GetSoresDetails();
          this._HelperService.NotifySuccess("Password Reset Successfully");
          this.ShowPasswordDiv = true
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {

        this._HelperService.HandleException(_Error);
      });
  }
}

export class OAccountOverview {
  public Merchants: number;
  public Stores: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}

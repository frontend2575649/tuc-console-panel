import { AfterContentChecked, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable, Subscription } from "rxjs";
import { DataHelperService, HelperService, OResponse, OSelect, OUserDetails, FilterHelperService } from "../../../../service/service";
import { InputFileComponent, InputFile } from 'ngx-input-file';
declare let $: any;

@Component({
  selector: "tu-cashier",
  templateUrl: "./tucashier.component.html"
})
export class TUCashierComponent implements OnInit,AfterContentChecked, OnDestroy {
  isLoaded: boolean = true;
  ShowImagePicker: boolean = true;

  public _CashierAddress: any = {};
  ToggleStoreSelect: number;
  showStorePicker: boolean;

  @ViewChild("inputfile")
  private InputFileComponent: InputFileComponent;

  CurrentImagesCount: number;

  //#region subscriptions 

  subscription: Subscription;
  ReloadSubscription: Subscription;

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.ReloadSubscription.unsubscribe();
    // this._CashierDataSubscription.unsubscribe();
    setTimeout(() => {
      if (this._ChangeDetectorRef && !(this._ChangeDetectorRef as ViewRef).destroyed) {
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  //#endregion

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {

  }
  ngAfterContentChecked(): void {
    this._ChangeDetectorRef.detectChanges();
  }

  //#region MapCorrection 

  slideOpen: any = false;
  changeSlide(): void {
    this.slideOpen = !this.slideOpen;
    if (this.slideOpen) {
    }
  }

  //#endregion

  //#region ToogleDetailView 

  HideStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-ShowStoreDetail");
  }

  ShowStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-ShowStoreDetail");
    element.classList.remove("Hm-HideDiv");
  }

  //#endregion

  BackDropInit(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  public _CashierDataSubscription: Subscription = null;
  ngOnInit() {
    //#region UIInit 
    setTimeout(() => {
      this._HelperService.ValidateData();
    }, 500);
    Feather.replace();
    this._HelperService.ContainerHeight = window.innerHeight;
    this._HelperService.AppConfig.ShowHeader = true;
    this.BackDropInit();
    //#endregion
    //#region Subscriptions 
    this.subscription = this._HelperService.isprocessingtoogle
      .subscribe((item) => {
        this._ChangeDetectorRef.detectChanges();
      });
    this.ReloadSubscription = this._HelperService.ReloadEventEmit.subscribe((number) => {

    });
    //#endregion
    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveCashier);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveCashierReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveCashierReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = StorageDetails.MerchantKey;
      this._HelperService.AppConfig.ActiveMerchantReferenceId = StorageDetails.MerchantId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
      this.GetCashiersDetails();

    }
    //#region DropdownInit 
    //#endregion
    this.Form_EditUser_Load();
    this.TUTr_Filter_Stores_Load();
    let activeCashier = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveCashier);
    if (activeCashier && activeCashier !== null) {
      this._HelperService.AppConfig.ActiveCashierReferenceKey = activeCashier.ReferenceKey;
      this._HelperService.AppConfig.ActiveCashierReferenceId = activeCashier.ReferenceId;
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = activeCashier.MerchantKey;
      this._HelperService.AppConfig.ActiveMerchantReferenceId = activeCashier.MerchantId;
      this.GetCashiersDetails();

    } else {
      this._Router.navigate([
        this._HelperService.AppConfig.Pages.System.NotFound
      ]);
    }

    // this._CashierDataSubscription = this._HelperService.CashierDetails.subscribe(params => {
    //   console.log("params......",params)

    //   if (this._HelperService.AppConfig.ActiveCashierReferenceKey != undefined && this._HelperService.AppConfig.ActiveCashierReferenceKey != null) {
    //     this.GetCashiersDetails();
    //   }
    //   else {
    //     this._Router.navigate([
    //       this._HelperService.AppConfig.Pages.System.NotFound
    //     ]);
    //   }
    // });



  }



  private InitImagePicker(previewurl?: string) {
    if (this.InputFileComponent != undefined) {
      this.CurrentImagesCount = 0;
      if (previewurl) {
        this.InputFileComponent.files[0] = {};
        this.InputFileComponent.files[0].preview = previewurl;
      }
      this._HelperService._InputFileComponent = this.InputFileComponent;
      this.InputFileComponent.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(this.InputFileComponent.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
  }




  StoreRoute(ReferenceData) {

    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveStore,
      {
        ReferenceKey: ReferenceData.StoreReferenceKey,
        ReferenceId: ReferenceData.StoreReferenceId,
        MerchantKey: ReferenceData.MerchantReferenceKey,
        MerchantId: ReferenceData.MerchantReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
      }
    );

    this._HelperService.AppConfig.ActiveStoreReferenceKey =
      ReferenceData.SubParentKey;
    this._HelperService.AppConfig.ActiveStoreReferenceId = ReferenceData.SubParentId;



    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Store
        .SalesHistory,
      ReferenceData.StoreReferenceKey,
      ReferenceData.StoreReferenceId,
      ReferenceData.MerchantReferenceKey,
      ReferenceData.MerchantReferenceId
    ]);

  }

  //#region EditUser 

  Form_EditUser: FormGroup;

  //#region EditUser_Address 

  _CurrentAddress: any = {};

  Form_EditUser_Address: string = null;
  Form_EditUser_Latitude: number = 0;
  Form_EditUser_Longitude: number = 0;

  @ViewChild('placesStore') placesStore: GooglePlaceDirective;

  Form_EditUser_PlaceMarkerClick(event) {
    this.Form_EditUser_Latitude = event.coords.lat;
    this.Form_EditUser_Longitude = event.coords.lng;
  }
  public Form_EditUser_AddressChange(address: Address) {
    this.Form_EditUser_Latitude = address.geometry.location.lat();
    this.Form_EditUser_Longitude = address.geometry.location.lng();
    this.Form_EditUser_Address = address.formatted_address;
    this.Form_EditUser.controls['Latitude'].setValue(this.Form_EditUser_Latitude);
    this.Form_EditUser.controls['Longitude'].setValue(this.Form_EditUser_Longitude);
    this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
  }

  //#endregion

  Form_EditUser_Show() {
    this._HelperService.OpenModal("Form_EditUser_Content");
  }
  Form_EditUser_Close() {
    // this._Router.navigate([
    //     this._HelperService.AppConfig.Pages.System.AdminUsers
    // ]);
    this._HelperService.OpenModal("Form_EditUser_Content");
  }
  Form_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;
    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;
    this.Form_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.ThankUCash.UpdateCashier,
      AccountKey: this._HelperService.AppConfig.ActiveCashierReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveCashierReferenceId,
      StoreId: [null, Validators.required],
      StoreKey: [null, Validators.required],
      StatusCode: this._HelperService.AppConfig.Status.Active,
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      EmployeeCode: null,
      GenderCode: this._HelperService.AppConfig.Gender.Male,
    });
  }
  Form_EditUser_Clear() {
    //this.Form_EditUser.reset();
    //this.Form_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_EditUser_Process(_FormValue: any) {
    // _FormValue.DisplayName = _FormValue.FirstName;
    // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
    _FormValue.Latitude = this.Form_EditUser_Latitude;
    _FormValue.Longitude = this.Form_EditUser_Longitude;
    _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveCashierReferenceKey;

    var IconContent: any = undefined;
    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      IconContent = this._HelperService._Icon_Cropper_Data;
    }

    _FormValue.IconContent = IconContent;

    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.Account,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account Updated successfully");
          this.GetCashiersDetails();
          this._HelperService.Icon_Crop_Clear();
          this.RemoveOffCanvas();
          this.Form_EditUser_Clear();
          this._HelperService.CloseModal('off-canvas')


        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region OffCanvasNBackdrop 
  @ViewChild("offCanvas") divView: ElementRef;

  ShowOffCanvas() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  RemoveOffCanvas() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }
  //#endregion


  //#region CashierDetails 

  public _CashierDetails: any =
    {
      ManagerName: null,
      BranchName: null,
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }
  GetCashiersDetails() {

    this._HelperService.IsFormProcessing = true;
    this.isLoaded = false;

    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetCashier,
      // AccountId: this._HelperService.UserAccount.AccountId,
      // AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountKey: this._HelperService.AppConfig.ActiveCashierReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveCashierReferenceId,


    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this.isLoaded = true;
          this._CashierDetails = _Response.Result;
          this._CashierDetails.CreateDate = this._HelperService.GetDateS(this._CashierDetails.CreateDate);
          this._CashierDetails.ModifyDate = this._HelperService.GetDateS(this._CashierDetails.ModifyDate);
          this._CashierDetails.StatusI = this._HelperService.GetStatusIcon(this._CashierDetails.StatusCode);
          this._CashierDetails.StatusB = this._HelperService.GetStatusBadge(this._CashierDetails.StatusCode);
          this._CashierDetails.StatusC = this._HelperService.GetStatusColor(this._CashierDetails.StatusCode);
          this._CashierAddress = this._CashierDetails.Address;

          this.showStorePicker = false;
          this._ChangeDetectorRef.detectChanges();

          this.TUTr_Filter_Store_Option.placeholder = this._CashierDetails.StoreDisplayName;
          this.TUTr_Filter_Stores_Load();

          this.showStorePicker = true;
          this._ChangeDetectorRef.detectChanges();
          //#region ResponseInit 
          this._CashierDetails.EndDateS = this._HelperService.GetDateS(
            this._CashierDetails.EndDate
          );
          this._CashierDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._CashierDetails.CreateDate
          );
          this._CashierDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._CashierDetails.ModifyDate
          );
          this._CashierDetails.StatusI = this._HelperService.GetStatusIcon(
            this._CashierDetails.StatusCode
          );
          this._CashierDetails.StatusB = this._HelperService.GetStatusBadge(
            this._CashierDetails.StatusCode
          );
          this._CashierDetails.StatusC = this._HelperService.GetStatusColor(
            this._CashierDetails.StatusCode
          );

          //#endregion

          //#region InitLocationParams 
          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {

            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;

            this.Form_EditUser_Latitude = _Response.Result.Latitude;
            this.Form_EditUser_Longitude = _Response.Result.Longitude;

            this.Form_EditUser.controls['Latitude'].setValue(_Response.Result.Latitude);
            this.Form_EditUser.controls['Longitude'].setValue(_Response.Result.Longitude);

          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }

          this.Form_EditUser.controls['StoreId'].setValue(_Response.Result.StoreReferenceId);
          this.Form_EditUser.controls['StoreKey'].setValue(_Response.Result.StoreReferenceKey);

          this.InitImagePicker(this._CashierDetails.IconUrl);

          //#endregion

        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion
  Form_EditUser_Block() {

  }
  
  public TUTr_Filter_Store_Option: Select2Options;
  // TUTr_Filter_Stores_Load() {
  //   var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
  //   var _Select: OSelect = {
  //     Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
  //     Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
  //     SearchCondition: "",
  //     SortCondition: [],
  //     Fields: [
  //       {
  //         SystemName: "ReferenceKey",
  //         Type: this._HelperService.AppConfig.DataType.Text,
  //         Id: true,
  //         Text: false,
  //       },
  //       {
  //         SystemName: "DisplayName",
  //         Type: this._HelperService.AppConfig.DataType.Text,
  //         Id: false,
  //         Text: true
  //       },
  //       {
  //         SystemName: "AccountTypeCode",
  //         Type: this._HelperService.AppConfig.DataType.Text,
  //         SearchCondition: "=",
  //         SearchValue: this._HelperService.AppConfig.AccountType.Store
  //       }
  //     ]
  //   };

  //   var OwnerKey = this._HelperService.UserAccount.AccountId;
  //   if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
  //     OwnerKey = this._HelperService.UserAccount.AccountId;
  //   }
   
  // _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, OwnerKey, '=');
  //  var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
  //   this.TUTr_Filter_Store_Option = {
  //     placeholder: this._CashierDetails.StoreDisplayName,
  //     ajax: _Transport,
  //     multiple: false,
  //     allowClear: true,
  //   };
  // }
  TUTr_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    this.Form_EditUser.patchValue(
      {
        StoreReferenceId: null,
        StoreReferenceKey: null,
      }


    );
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Store
        }
      ]
    };

    var OwnerKey = this._HelperService.AppConfig.ActiveMerchantReferenceId;
    if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
      OwnerKey = this._HelperService.AppConfig.ActiveMerchantReferenceId;
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, OwnerKey, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Store_Option = {
      placeholder: this._CashierDetails.StoreDisplayName,
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Stores_Change(event: any) {
    this.Form_EditUser.patchValue(
      {
        StoreId: event.data[0].ReferenceId,
        StoreKey: event.data[0].ReferenceKey,
        StoreName: event.data[0].DisplayName
      }


    );

  }

  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }
  BlockCashier() {
    this._HelperService.OpenModal("BlockCashier");
    this.RemoveOffCanvas();
  }

  public Block(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "disablecashier",
      AccountId: this._CashierDetails.ReferenceId,
      AccountKey: this._CashierDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess("Cashier Disabled Successfully ");
          this._HelperService.CloseModal("BlockCashier");
          this.GetCashiersDetails();
          this.ShowOffCanvas();

        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public UnBlock(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "enablecashier",
      AccountId: this._CashierDetails.ReferenceId,
      AccountKey: this._CashierDetails.ReferenceKey,
      StatusCode: "default.active",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess("Cashier Enabled Successfully ");
          this._HelperService.CloseModal("BlockCashier");
          this.GetCashiersDetails();
          this.ShowOffCanvas();


        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


  removeImage(): void {
    this.CurrentImagesCount = 0;
    this._HelperService.Icon_Crop_Clear();
    var Req = this.Approve_RequestBody();
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Profile, Req);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Image Removed Successfully");

        }
        else {

          this._HelperService.NotifyError("Image Not Found");
        }
      },
      _Error => {

        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  Approve_RequestBody(): void {
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'removeprofileimage',
      //'ReferenceKey': this.SelectedDeal.ReferenceKey,
      // 'ReferenceId': this.SelectedDeal.ReferenceId,
      "ReferenceKey": this._CashierDetails.ReferenceKey,
      'ReferenceId': this._CashierDetails.ReferenceId,
      // 'IconUrl':'https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/o/2021/12/51da6698fc8044b1a2d4ed78aee6f2f8.png'


    };
    //#region Set Schedule 

    //#endregion

    return formRequest;

  }
}

export class OAccountOverview {
  public Merchants: number;
  public Stores: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}

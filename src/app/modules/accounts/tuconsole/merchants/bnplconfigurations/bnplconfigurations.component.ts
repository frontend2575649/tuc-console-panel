import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OUserDetails } from '../../../../../service/service';
import swal from 'sweetalert2';
import * as moment from 'moment';

@Component({
  selector: 'app-bnplconfigurations',
  templateUrl: './bnplconfigurations.component.html'
})
export class BnplconfigurationsComponent implements OnInit {

  public AccountActivationCheckList =
    {
      IsBusinessInformationComplete: false,
      IsConfigurationComplete: false,
      IsStoreComplete: false,
      IsBusinessCategoriesSet: false,
    }

  isShow = false;
  isFirst =false;


  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
  ) { }



  MerchantDetails: any = {};

  ngOnInit() {
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = this._HelperService.AppConfig.AccountType.Merchant;
    this._HelperService.ContainerHeight = window.innerHeight;
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
      if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.UserOnboarding.PendingMerchant]);
      } else {
        this._HelperService.Get_UserAccountDetails(true);
        this.Get_UserAccountDetails(true);
      }
    });
  }

  public Get_UserAccountDetails(ShowHeader: boolean) {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccount,
      Reference: this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this._HelperService.AppConfig.ActiveReferenceKey,
        "="
      )
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._UserAccount = _Response.Result as OUserDetails;
          this._UserAccount.DateOfBirth = this._HelperService.GetDateS(this._UserAccount.DateOfBirth);
          this._UserAccount.LastLoginDateS = this._HelperService.GetDateTimeS(this._UserAccount.LastLoginDate);
          this._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(this._UserAccount.CreateDate);
          this._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(this._UserAccount.ModifyDate);
          this._UserAccount.StatusI = this._HelperService.GetStatusIcon(this._UserAccount.StatusCode);
          this._HelperService.IsFormProcessing = false;
          this.GetMerchantConfigurations();
          if (this._UserAccount.SubAccounts > 0) {
            this.AccountActivationCheckList.IsStoreComplete = true;
          }
          this.AccountActivationCheckList.IsBusinessInformationComplete = true;
          if (
            this._UserAccount.DisplayName == null
            || this._UserAccount.DisplayName == ''
            || this._UserAccount.Name == null
            || this._UserAccount.Name == ''

            || this._UserAccount.ContactNumber == null
            || this._UserAccount.ContactNumber == ''

            || this._UserAccount.EmailAddress == null
            || this._UserAccount.EmailAddress == ''

            || this._UserAccount.AccountOperationTypeCode == null
            || this._UserAccount.AccountOperationTypeCode == ''

            || this._UserAccount.IconUrl == null
            || this._UserAccount.IconUrl == ''
            || this._UserAccount.IconUrl.endsWith('defaulticon.png')
            || this._UserAccount.Description == null
            || this._UserAccount.Description == ''

            || this._UserAccount.Address == null
            || this._UserAccount.Address == ''

            || this._UserAccount.FirstName == null
            || this._UserAccount.FirstName == ''

            || this._UserAccount.LastName == null
            || this._UserAccount.LastName == ''

            || this._UserAccount.MobileNumber == null
            || this._UserAccount.MobileNumber == ''

            || this._UserAccount.EmailAddress == null
            || this._UserAccount.EmailAddress == ''

            || this._UserAccount.SecondaryEmailAddress == null
            || this._UserAccount.SecondaryEmailAddress == ''
          ) {
            this.AccountActivationCheckList.IsBusinessInformationComplete = false;
          }
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public _Configurations = {
    RewardPercentage: null,
    RewardMaximumI_ConfigurationsnvoiceAmount: null,
    RewardDeductionTypeCode: null,
    RewardDeductionType: null,
    MaximumRewardDeductionAmount: null,

    ThankUPlus: null,
    BNPL_Config: null,
    BNPLSettelementSchedule: null,
    BNPLSettelementTransferCriteria: null,
    MerchantSettelement: null,
    ThankUPlusCriteriaType: null,
    ThankUPlusCriteriaTypeCode: null,
    ThankUPlusCriteriaValue: null,
    ThankUPlusMinimumTransferAmount: null,

    SettlmentShedule: null,
    SettlmentSheduleCode: null,
    MinimumSettlementAmount: null,
    enablethankucash: null,
    merchantappvisiblity: null,
    merchantreward: null,
    merchantredeem: null,
    rewardsmstucplus: null,
    rewardsms: null,
    redeemsms: null,
    tucuserrewardonly: null,
    bynowpaylater: null,
    cagiftpoints: null,
    cagiftcards: null,
    securitymaximumtransactionperdaycap: 3,
    securitymaximuminvoiceamountperdaycap: 50000,
    userrewardpercentage: null,
    IsMerchantSettelment: null
  };
  GetMerchantConfigurations() {
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetUserParameters,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      SortExpression: 'Name asc',
      Offset: 0,
      Limit: 1000,
    }
    PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
      PData.SearchCondition,
      "TypeCode",
      this._HelperService.AppConfig.DataType.Text,
      this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
      "="
    );
    PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
      PData.SearchCondition,
      "UserAccountKey",
      this._HelperService.AppConfig.DataType.Text,
      this._UserAccount.ReferenceKey,
      "="
    );
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            var ResposeData = _Response.Result.Data;
            const bnpldata = ResposeData.find(el => el.CommonSystemName == "bynowpaylater")
            if (bnpldata) {
              this.isFirst = false;
              this._Configurations.BNPL_Config = bnpldata.Value;
              this.isShow = this._Configurations.BNPL_Config == 1 ? true : false;
            } else {
              this.isFirst = true;
            }
            if (this._Configurations.BNPL_Config == 1) {
              this.GetBNPLMerchantConfigurations();
            }
          }
          this.AccountActivationCheckList.IsConfigurationComplete = true;
          if (this._Configurations.BNPL_Config == null
          ) {
            this.AccountActivationCheckList.IsConfigurationComplete = false;
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  updateSettlement() {
    swal({
      position: 'top',
      title: 'Stop/Resume Settlement ?',
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Blue,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: this._HelperService.AppConfig.Api.Core.managesettelment,
          MerchantId: this._UserAccount.ReferenceId,
          StatusCode: this._Configurations.IsMerchantSettelment == 1 ? "merchantsettelment.processing" : "merchantsettelment.cancelled",
          IsMerchantSettelment: this._Configurations.IsMerchantSettelment
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.AccountPlan.BNPLMerchant, PostData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              // this._HelperService.NotifySuccess("Settlemnt Status updated successfully");
              this._HelperService.NotifySuccess(_Response.Message);
            } else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });
  }
  UpdateMerchantStatus() {
    swal({
      position: 'top',
      title: 'Activate merchant ?',
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Blue,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "activateaccount",
          UserAccountKey: this._UserAccount.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              // this._HelperService.NotifySuccess("Merchant activated");
              this._HelperService.NotifySuccess(_Response.Message);
              this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.UserOnboarding.PendingMerchant]);

            } else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });
  }

  //Merchant BNPL Configuration Added by dBNPLEnable_Disableev_omkar
  BNPLEnable_Disable() {
    swal({
      title: 'Do you want to Enable/Disable this Merchant for BNPL?',
      text: 'After settings, it takes upto 1 hours to reflect changes, Do you want to continue?',
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Blue,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      input: 'password',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;


        //Save account configuration for merchant
        var PostData = {
          Task: "saveaccountconfiguration",
          AuthPin: result.value,
          TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
          UserAccountKey: this._UserAccount.ReferenceKey,
          ConfigurationKey: "bynowpaylater",
          Value: this._Configurations.BNPL_Config,
          StatusCode: this._HelperService.AppConfig.Status.Active,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              // this._HelperService.NotifySuccess("BNPL Configuration Updated Successfully");
              this._HelperService.NotifySuccess(_Response.Message);
              this.GetMerchantConfigurations();

              if (this._Configurations.BNPL_Config == 1) {
                this.GetBNPLMerchantConfigurations();
              }
            } else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
        //End save account configuration for merchant

        //Save Merchant for BNPL

        if (this.isFirst) {
          var PData = {
            Task: this._HelperService.AppConfig.Api.Core.savemerchant,
            AccountId: this._UserAccount.ReferenceId,
            AccountKey: this._UserAccount.ReferenceKey,
            SettelmentTypeCode: this._HelperService.AppConfig.HelperTypes.SettelementTypeCode,
            TransactionSourceCode: "transaction.source.bnpl",
            SettelmentDays: 0,
            IsMerchantSettelment: 1,
            StartDate: moment().format(),
            StatusCode: this._HelperService.AppConfig.Status.Active
          };
          let OResponse: Observable<OResponse>;
          OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.AccountPlan.BNPLMerchant, PData);
          OResponse.subscribe(
            _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                // this._HelperService.NotifySuccess("BNPL Configuration Updated Successfully");
                this._HelperService.NotifySuccess(_Response.Message);
                this.GetMerchantConfigurations();
                if (this._Configurations.BNPL_Config == 1) {
                  this.GetBNPLMerchantConfigurations();
                }
              } else {
                this._HelperService.NotifyError(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
            }
          );
        }
        //End save BNPL Merchant
      }
    });
  }

  DaysCount: number = 0;
  DayCount: boolean = false;
  checkcount() {
    if (this.DaysCount > 7 || this.DaysCount == null) {
      this.DayCount = true;
    }
    else {
      this.DayCount = false;
    }
  }
  //Update BNPL merchant
  Update_Merchant_Config() {
    this._HelperService.IsFormProcessing == true;
    var PData = {
      Task: this._HelperService.AppConfig.Api.Core.updatemerchant,
      AccountId: this._UserAccount.ReferenceId,
      AccountKey: this._UserAccount.ReferenceKey,
      SettelmentDays: this.DaysCount,
      SettelmentTypeCode: this._HelperService.AppConfig.HelperTypes.SettelementTypeCode,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.AccountPlan.BNPLMerchant, PData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          // this._HelperService.NotifySuccess("BNPL Settelement Schedule Updated Successfully");
          this._HelperService.NotifySuccess(_Response.Message);
          this.GetMerchantConfigurations();
          if (this._Configurations.BNPL_Config == 1) {
            this.GetBNPLMerchantConfigurations();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  //End Update BNPL Merchant

  //Merchant Settelement transfer criteria
  Set_Transfer_Criteria() {
    this._HelperService.IsFormProcessing == true;
    var PData = {
      Task: this._HelperService.AppConfig.Api.Core.updatesettelementcriteria,
      AccountId: this._UserAccount.ReferenceId,
      AccountKey: this._UserAccount.ReferenceKey,
      TransactionSourceCode: this._Configurations.BNPLSettelementTransferCriteria,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.AccountPlan.BNPLMerchant, PData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          // this._HelperService.NotifySuccess("BNPL Settelement Criteria Updated Successfully");
          this._HelperService.NotifySuccess(_Response.Message);
          this.GetMerchantConfigurations();
          if (this._Configurations.BNPL_Config == 1) {
            this.GetBNPLMerchantConfigurations();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  //End Merchant Settelement transfer criteria

  //Get BNPL Merchant Configuration
  GetBNPLMerchantConfigurations() {
    var PData = {
      Task: this._HelperService.AppConfig.Api.Core.getmerchant,
      AccountId: this._UserAccount.ReferenceId,
      AccountKey: this._UserAccount.ReferenceKey,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.AccountPlan.BNPLMerchant, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result != undefined) {
            this.MerchantDetails = _Response.Result;
            if (this.MerchantDetails.SettelmentDays !== null) {
              this._Configurations.BNPLSettelementSchedule = this.MerchantDetails.SettelmentDays / 24;
              this.DaysCount = this.MerchantDetails.SettelmentDays / 24;
            }
            if (this.MerchantDetails.TransactionSourceCode !== null) {
              this._Configurations.BNPLSettelementTransferCriteria = this.MerchantDetails.TransactionSourceCode;
            }
            this._Configurations.IsMerchantSettelment = this.MerchantDetails.IsMerchantSettelment
          }
          else {
            this._HelperService.NotifySuccess(_Response.Message);
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  //End Get BNPL Merchant Configuration
  //End BNPL Configuration

  public _UserAccount: OUserDetails = {
    ContactNumber: null,
    SecondaryEmailAddress: null,
    ReferenceId: null,
    BankDisplayName: null,
    BankKey: null,
    SubOwnerAddress: null,
    SubOwnerLatitude: null,
    SubOwnerDisplayName: null,
    SubOwnerKey: null,
    SubOwnerLongitude: null,
    AccessPin: null,
    LastLoginDateS: null,
    AppKey: null,
    AppName: null,
    AppVersionKey: null,
    CreateDate: null,
    CreateDateS: null,
    CreatedByDisplayName: null,
    CreatedByIconUrl: null,
    CreatedByKey: null,
    Description: null,
    IconUrl: null,
    ModifyByDisplayName: null,
    ModifyByIconUrl: null,
    ModifyByKey: null,
    ModifyDate: null,
    ModifyDateS: null,
    PosterUrl: null,
    ReferenceKey: null,
    StatusCode: null,
    StatusI: null,
    StatusId: null,
    StatusName: null,
    AccountCode: null,
    AccountOperationTypeCode: null,
    AccountOperationTypeName: null,
    AccountTypeCode: null,
    AccountTypeName: null,
    Address: null,
    AppVersionName: null,
    ApplicationStatusCode: null,
    ApplicationStatusName: null,
    AverageValue: null,
    CityAreaKey: null,
    CityAreaName: null,
    CityKey: null,
    CityName: null,
    CountValue: null,
    CountryKey: null,
    CountryName: null,
    DateOfBirth: null,
    DisplayName: null,
    EmailAddress: null,
    EmailVerificationStatus: null,
    EmailVerificationStatusDate: null,
    FirstName: null,
    GenderCode: null,
    GenderName: null,
    LastLoginDate: null,
    LastName: null,
    Latitude: null,
    Longitude: null,
    MobileNumber: null,
    Name: null,
    NumberVerificationStatus: null,
    NumberVerificationStatusDate: null,
    OwnerDisplayName: null,
    OwnerKey: null,
    Password: null,
    Reference: null,
    ReferralCode: null,
    ReferralUrl: null,
    RegionAreaKey: null,
    RegionAreaName: null,
    RegionKey: null,
    RegionName: null,
    RegistrationSourceCode: null,
    RegistrationSourceName: null,
    RequestKey: null,
    RoleKey: null,
    RoleName: null,
    SecondaryPassword: null,
    SystemPassword: null,
    UserName: null,
    WebsiteUrl: null
  };



}

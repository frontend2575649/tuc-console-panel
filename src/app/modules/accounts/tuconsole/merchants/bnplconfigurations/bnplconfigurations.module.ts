import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BnplconfigurationsComponent } from './bnplconfigurations.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  { path: '', component: BnplconfigurationsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BnplconfigurationsRoutingModule { }

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      BnplconfigurationsRoutingModule,
  ],
  declarations: [BnplconfigurationsComponent]
})
export class BnplconfigurationsModule { }

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import swal from 'sweetalert2';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OUserDetails } from '../../../../../service/service';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
    selector: 'tu-configurationmanager',
    templateUrl: './tuconfigurations.component.html',
})
export class TUConfigurationManagerComponent implements OnInit {


    public ActivationProgress = 0;
    public ActivationTotalCount = 20;
    public ActivationCompletionCount = 0;
    public AccountActivationCheckList =
        {
            IsBusinessInformationComplete: false,
            IsConfigurationComplete: false,
            IsStoreComplete: false,
            IsBusinessCategoriesSet: false,
        }

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {
        this._HelperService.showAddNewPosBtn = false;
        this._HelperService.showAddNewStoreBtn = false;
        this._HelperService.showAddNewCashierBtn = false;
        this._HelperService.showAddNewSubAccBtn = false;
    }
    ngOnInit() {
        this._HelperService.AppConfig.ShowHeader = true;
        this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = this._HelperService.AppConfig.AccountType.Merchant;
        this._HelperService.ContainerHeight = window.innerHeight;
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.UserOnboarding.PendingMerchant]);
            } else {
                this._HelperService.Get_UserAccountDetails(true);
                this.Get_UserAccountDetails(true);

            }
        });
    }

    public _AccountBalanceCreditAmount = 10000;
    public _AccountBalanceCreditReference = "";

    public _AccountBalance =
        {
            Credit: 0,
            Debit: 0,
            Balance: 0
        }
    public GetBalance() {
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: 'getuseraccountbalance',
            UserAccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
            Source: this._HelperService.AppConfig.TransactionSource.Merchant
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._AccountBalance = _Response.Result;
                    this._AccountBalance.Balance = _Response.Result.Balance / 100;
                    this._AccountBalance.Credit = _Response.Result.Credit / 100;
                    this._AccountBalance.Debit = _Response.Result.Debit / 100;
                } else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    public CreditAmount() {
        if (this._AccountBalanceCreditAmount < 1) {
            this._HelperService.NotifyError('Amount must be greater than 0');
        }
        else if (this._AccountBalanceCreditAmount > 1000000) {
            this._HelperService.NotifyError('Amount must be less than 10,00,000');
        }
        else if (this._AccountBalanceCreditReference == undefined || this._AccountBalanceCreditReference == null || this._AccountBalanceCreditReference == "") {
            this._HelperService.NotifyError('Enter payment reference number');
        }
        else {
            swal({
                title: 'Credit  N' + this._AccountBalanceCreditAmount + ' to account?',
                text: 'Enter security pin and click on continue to credit amount',
                position: this._HelperService.AppConfig.Alert_Position,
                animation: this._HelperService.AppConfig.Alert_AllowAnimation,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
                allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
                showCancelButton: true,
                input: 'password',
                inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
                inputAttributes: {
                    autocapitalize: 'off',
                    autocorrect: 'off',
                    maxLength: "4",
                    minLength: "4"
                },
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "creditmerchantwallet",
                        AccountId: this._HelperService.AppConfig.ActiveReferenceId,
                        AuthPin: result.value,
                        InvoiceAmount: this._AccountBalanceCreditAmount,
                        ReferenceNumber: this._AccountBalanceCreditReference,
                    };
                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.ThankU, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {

                                this._AccountBalanceCreditAmount = 0;
                                this._AccountBalanceCreditReference = "";
                                this._HelperService.NotifySuccess("Account credited");
                                this.GetMerchantConfigurations();
                                this.GetBalance();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });
        }
    }
    public Get_UserAccountDetails(ShowHeader: boolean) {
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccount,
            Reference: this._HelperService.GetSearchConditionStrict(
                "",
                "ReferenceKey",
                this._HelperService.AppConfig.DataType.Text,
                this._HelperService.AppConfig.ActiveReferenceKey,
                "="
            )
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._UserAccount = _Response.Result as OUserDetails;
                    this._UserAccount.DateOfBirth = this._HelperService.GetDateS(this._UserAccount.DateOfBirth);
                    this._UserAccount.LastLoginDateS = this._HelperService.GetDateTimeS(this._UserAccount.LastLoginDate);
                    this._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(this._UserAccount.CreateDate);
                    this._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(this._UserAccount.ModifyDate);
                    this._UserAccount.StatusI = this._HelperService.GetStatusIcon(this._UserAccount.StatusCode);
                    this._HelperService.IsFormProcessing = false;
                    this.GetMerchantConfigurations();
                    if (this._UserAccount.SubAccounts > 0) {
                        this.AccountActivationCheckList.IsStoreComplete = true;
                    }
                    this.AccountActivationCheckList.IsBusinessInformationComplete = true;
                    if (
                        this._UserAccount.DisplayName == null
                        || this._UserAccount.DisplayName == ''
                        || this._UserAccount.Name == null
                        || this._UserAccount.Name == ''

                        || this._UserAccount.ContactNumber == null
                        || this._UserAccount.ContactNumber == ''

                        || this._UserAccount.EmailAddress == null
                        || this._UserAccount.EmailAddress == ''

                        || this._UserAccount.AccountOperationTypeCode == null
                        || this._UserAccount.AccountOperationTypeCode == ''

                        || this._UserAccount.IconUrl == null
                        || this._UserAccount.IconUrl == ''
                        || this._UserAccount.IconUrl.endsWith('defaulticon.png')
                        || this._UserAccount.Description == null
                        || this._UserAccount.Description == ''

                        || this._UserAccount.Address == null
                        || this._UserAccount.Address == ''

                        || this._UserAccount.FirstName == null
                        || this._UserAccount.FirstName == ''

                        || this._UserAccount.LastName == null
                        || this._UserAccount.LastName == ''

                        || this._UserAccount.MobileNumber == null
                        || this._UserAccount.MobileNumber == ''

                        || this._UserAccount.EmailAddress == null
                        || this._UserAccount.EmailAddress == ''

                        || this._UserAccount.SecondaryEmailAddress == null
                        || this._UserAccount.SecondaryEmailAddress == ''
                    ) {
                        this.AccountActivationCheckList.IsBusinessInformationComplete = false;
                    }
                } else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    public _Configurations = {
        RewardPercentage: null,
        RewardMaximumInvoiceAmount: null,
        RewardDeductionTypeCode: null,
        RewardDeductionType: null,
        MaximumRewardDeductionAmount: null,

        ThankUPlus: null,
        ThankUPlusCriteriaType: null,
        ThankUPlusCriteriaTypeCode: null,
        ThankUPlusCriteriaValue: null,
        ThankUPlusMinimumTransferAmount: null,

        SettlmentShedule: null,
        SettlmentSheduleCode: null,
        MinimumSettlementAmount: null,
        enablethankucash: null,
        merchantappvisiblity: null,
        merchantreward: null,
        merchantredeem: null,
        rewardsmstucplus: null,
        rewardsms: null,
        redeemsms: null,
        tucuserrewardonly: null,
        cagiftpoints: null,
        cagiftcards: null,
        securitymaximumtransactionperdaycap: 3,
        securitymaximuminvoiceamountperdaycap: 50000,
        userrewardpercentage: null,
    };
    GetMerchantConfigurations() {
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetUserParameters,
            SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
            SortExpression: 'Name asc',
            Offset: 0,
            Limit: 1000,
        }
        PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
            PData.SearchCondition,
            "TypeCode",
            this._HelperService.AppConfig.DataType.Text,
            this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
            "="
        );
        PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
            PData.SearchCondition,
            "UserAccountKey",
            this._HelperService.AppConfig.DataType.Text,
            this._UserAccount.ReferenceKey,
            "="
        );
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        var ResposeData = _Response.Result.Data;
                        ResposeData.forEach(element => {
                            if (element.CommonSystemName == "rewardpercentage") {
                                this._Configurations.RewardPercentage = element.Value;
                            }
                            if (element.CommonSystemName == "rewardmaxinvoiceamount") {
                                this._Configurations.RewardMaximumInvoiceAmount = element.Value;
                            }
                            if (element.CommonSystemName == "rewarddeductiontype") {
                                this._Configurations.RewardDeductionType = element.HelperName;
                                this._Configurations.RewardDeductionTypeCode = element.HelperCode;
                                if (
                                    this._Configurations.RewardDeductionTypeCode == 'rewarddeductiontype.prepay' ||
                                    this._Configurations.RewardDeductionTypeCode == 'rewarddeductiontype.postpay' ||
                                    this._Configurations.RewardDeductionTypeCode == 'rewarddeductiontype.prepayandpostpay'
                                ) {
                                    this.GetBalance();

                                }
                            }
                            if (element.CommonSystemName == "maximumrewarddeductionamount") {
                                this._Configurations.MaximumRewardDeductionAmount = element.Value;
                            }
                            if (element.CommonSystemName == "cagiftpoints") {
                                this._Configurations.cagiftpoints = element.Value;
                            }
                            if (element.CommonSystemName == "cagiftcards") {
                                this._Configurations.cagiftcards = element.Value;
                            }
                            if (element.CommonSystemName == "userrewardpercentage") {
                                this._Configurations.userrewardpercentage = element.Value;
                            }

                            if (element.CommonSystemName == "settlementsheduletype") {
                                this._Configurations.SettlmentShedule = element.HelperName;
                                this._Configurations.SettlmentSheduleCode = element.HelperCode;
                            }
                            if (element.CommonSystemName == "minimumsettlementamount") {
                                this._Configurations.MinimumSettlementAmount = element.Value;
                            }

                            if (element.CommonSystemName == "thankucashplus") {
                                this._Configurations.ThankUPlus = element.Value;
                            }
                            if (element.CommonSystemName == "enablethankucash") {
                                this._Configurations.enablethankucash = element.Value;
                            }

                            if (element.CommonSystemName == "merchantappvisiblity") {
                                this._Configurations.merchantappvisiblity = element.Value;
                            }
                            if (element.CommonSystemName == "merchantreward") {
                                this._Configurations.merchantreward = element.Value;
                            }
                            if (element.CommonSystemName == "merchantredeem") {
                                this._Configurations.merchantredeem = element.Value;
                            }
                            if (element.CommonSystemName == "rewardsmstucplus") {
                                this._Configurations.rewardsmstucplus = element.Value;
                            }
                            if (element.CommonSystemName == "rewardsms") {
                                this._Configurations.rewardsms = element.Value;
                            }
                            if (element.CommonSystemName == "redeemsms") {
                                this._Configurations.redeemsms = element.Value;
                            }
                            if (element.CommonSystemName == "thankucashplusrewardcriteria") {
                                this._Configurations.ThankUPlusCriteriaValue = element.Value;
                                this._Configurations.ThankUPlusCriteriaType = element.HelperName;
                                this._Configurations.ThankUPlusCriteriaTypeCode = element.HelperCode;
                            }
                            if (element.CommonSystemName == "thankucashplusmintransferamount") {
                                this._Configurations.ThankUPlusMinimumTransferAmount = element.Value;
                            }

                            if (element.CommonSystemName == "securitymaximumtransactionperdaycap") {
                                this._Configurations.securitymaximumtransactionperdaycap = element.Value;
                            }
                            if (element.CommonSystemName == "securitymaximuminvoiceamountperdaycap") {
                                this._Configurations.securitymaximuminvoiceamountperdaycap = element.Value;
                            }
                            if (element.CommonSystemName == "tucuserrewardonly") {
                                this._Configurations.tucuserrewardonly = element.Value;
                            }
                        });
                    }
                    this.AccountActivationCheckList.IsConfigurationComplete = true;
                    if (
                        this._Configurations.RewardPercentage == null
                        || this._Configurations.RewardMaximumInvoiceAmount == null
                        || this._Configurations.RewardDeductionTypeCode == null
                        || this._Configurations.SettlmentShedule == null
                        || this._Configurations.MinimumSettlementAmount == null
                        || this._Configurations.RewardDeductionType == null
                        || this._Configurations.MaximumRewardDeductionAmount == null
                        || this._Configurations.ThankUPlus == null
                    ) {
                        this.AccountActivationCheckList.IsConfigurationComplete = false;
                    }
                    if (this._Configurations.ThankUPlus == 1) {
                        if (
                            this._Configurations.ThankUPlusCriteriaType == null
                            || this._Configurations.ThankUPlusCriteriaTypeCode == null
                            || this._Configurations.ThankUPlusCriteriaValue == null
                            || this._Configurations.ThankUPlusMinimumTransferAmount == null
                        ) {
                            this.AccountActivationCheckList.IsConfigurationComplete = false;
                        }
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }
    RewardPercentage_Update() {
        if (
            this._Configurations.RewardPercentage == undefined ||
            this._Configurations.RewardPercentage == null ||
            this._Configurations.RewardPercentage == ""
        ) {
            this._HelperService.NotifyError(
                "Please enter reward percentage"
            );
        } else if (isNaN(this._Configurations.RewardPercentage) == true) {
            this._HelperService.NotifyError(
                "Please enter valid reward percentage"
            );
        } else if (parseFloat(this._Configurations.RewardPercentage) < 0) {
            this._HelperService.NotifyError(
                "Reward percentage must be between 0 to 100"
            );
        } else if (parseFloat(this._Configurations.RewardPercentage) > 100) {
            this._HelperService.NotifyError(
                "Reward percentage must be between 0 to 100"
            );
        } else {
            swal({
                title: 'Update reward percentage to ' + this._Configurations.RewardPercentage + '% ?',
                text: 'After updating reward percentage, it takes upto 1 hours to reflect changes, Do you want to continue?',
                position: this._HelperService.AppConfig.Alert_Position,
                animation: this._HelperService.AppConfig.Alert_AllowAnimation,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
                allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
                showCancelButton: true,
                input: 'password',
                inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
                inputAttributes: {
                    autocapitalize: 'off',
                    autocorrect: 'off',
                    maxLength: "4",
                    minLength: "4"
                },
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveaccountconfiguration",
                        AuthPin: result.value,
                        TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                        UserAccountKey: this._UserAccount.ReferenceKey,
                        ConfigurationKey: 'rewardpercentage',
                        StatusCode: this._HelperService.AppConfig.Status.Active,
                        Value: this._Configurations.RewardPercentage,
                    };
                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                this._HelperService.CloseModal("Modal_RewardPercentage");
                                this.GetMerchantConfigurations();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });


        }
    }
    UserRewardPercentage_Update() {
        if (
            this._Configurations.userrewardpercentage == undefined ||
            this._Configurations.userrewardpercentage == null ||
            this._Configurations.userrewardpercentage == ""
        ) {
            this._HelperService.NotifyError(
                "Please enter user reward percentage"
            );
        } else if (isNaN(this._Configurations.userrewardpercentage) == true) {
            this._HelperService.NotifyError(
                "Please enter valid user reward percentage"
            );
        } else if (parseFloat(this._Configurations.userrewardpercentage) < 0) {
            this._HelperService.NotifyError(
                "User reward percentage must be between 0 to 100"
            );
        } else if (parseFloat(this._Configurations.userrewardpercentage) > 100) {
            this._HelperService.NotifyError(
                "User reward percentage must be between 0 to 100"
            );
        } else {
            swal({
                title: 'Update user reward percentage to ' + this._Configurations.userrewardpercentage + '% ?',
                text: 'After updating reward percentage, it takes upto 1 hours to reflect changes, Do you want to continue?',
                position: this._HelperService.AppConfig.Alert_Position,
                animation: this._HelperService.AppConfig.Alert_AllowAnimation,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
                allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
                showCancelButton: true,
                input: 'password',
                inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
                inputAttributes: {
                    autocapitalize: 'off',
                    autocorrect: 'off',
                    maxLength: "4",
                    minLength: "4"
                },
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveaccountconfiguration",
                        AuthPin: result.value,
                        TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                        UserAccountKey: this._UserAccount.ReferenceKey,
                        ConfigurationKey: 'userrewardpercentage',
                        StatusCode: this._HelperService.AppConfig.Status.Active,
                        Value: this._Configurations.userrewardpercentage,
                    };
                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                this._HelperService.CloseModal("Modal_RewardPercentage");
                                this.GetMerchantConfigurations();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });


        }
    }
    MaximumRewardInvoiceAmount_Update() {
        if (
            this._Configurations.RewardMaximumInvoiceAmount == undefined ||
            this._Configurations.RewardMaximumInvoiceAmount == null ||
            this._Configurations.RewardMaximumInvoiceAmount == ""
        ) {
            this._HelperService.NotifyError(
                "Please enter maximum reward invoice amount"
            );
        } else if (isNaN(this._Configurations.RewardMaximumInvoiceAmount) == true) {
            this._HelperService.NotifyError(
                "Please enter valid maximum reward invoice amount"
            );
        } else if (parseFloat(this._Configurations.RewardMaximumInvoiceAmount) < 0) {
            this._HelperService.NotifyError(
                "Maximum reward invoice amount  must be between 0 to 10000000"
            );
        } else if (parseFloat(this._Configurations.RewardMaximumInvoiceAmount) > 10000000) {
            this._HelperService.NotifyError(
                "Maximum reward invoice amount  must be between 0 to 10000000"
            );
        } else {
            swal({
                title: 'Update Maximum Rewards Invoice Amount to ' + this._Configurations.RewardMaximumInvoiceAmount + '% ?',
                text: 'After updating amount , it takes upto 1 hours to reflect changes, Do you want to continue?',
                position: this._HelperService.AppConfig.Alert_Position,
                animation: this._HelperService.AppConfig.Alert_AllowAnimation,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
                allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
                showCancelButton: true,
                input: 'password',
                inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
                inputAttributes: {
                    autocapitalize: 'off',
                    autocorrect: 'off',
                    maxLength: "4",
                    minLength: "4"
                },
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveaccountconfiguration",
                        AuthPin: result.value,
                        TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                        UserAccountKey: this._UserAccount.ReferenceKey,
                        ConfigurationKey: 'rewardmaxinvoiceamount',
                        StatusCode: this._HelperService.AppConfig.Status.Active,
                        Value: this._Configurations.RewardMaximumInvoiceAmount,
                    };

                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                this._HelperService.CloseModal("Modal_RewardsMaximumInvoiceLimit");
                                this.GetMerchantConfigurations();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });


        }
    }
    MinimumSettlementAmount_Update() {
        if (
            this._Configurations.MinimumSettlementAmount == undefined ||
            this._Configurations.MinimumSettlementAmount == null ||
            this._Configurations.MinimumSettlementAmount == ""
        ) {
            this._HelperService.NotifyError(
                "Please enter minimum settlement amount"
            );
        } else if (isNaN(this._Configurations.MinimumSettlementAmount) == true) {
            this._HelperService.NotifyError(
                "Please enter valid minimum settlement amount"
            );
        } else if (parseFloat(this._Configurations.MinimumSettlementAmount) < 0) {
            this._HelperService.NotifyError(
                "Minimum settlement amount  must be between 0 to 10000000"
            );
        } else if (parseFloat(this._Configurations.MinimumSettlementAmount) > 10000000) {
            this._HelperService.NotifyError(
                "Minimum settlement amount  must be between 0 to 10000000"
            );
        } else {
            swal({
                title: 'Update minimum settlement amount to ' + this._Configurations.MinimumSettlementAmount + '?',
                text: 'After updating amount, it takes upto 1 hours to reflect changes, Do you want to continue?',
                position: this._HelperService.AppConfig.Alert_Position,
                animation: this._HelperService.AppConfig.Alert_AllowAnimation,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
                allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
                showCancelButton: true,
                input: 'password',
                inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
                inputAttributes: {
                    autocapitalize: 'off',
                    autocorrect: 'off',
                    maxLength: "4",
                    minLength: "4"
                },
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveaccountconfiguration",
                        AuthPin: result.value,
                        TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                        UserAccountKey: this._UserAccount.ReferenceKey,
                        ConfigurationKey: 'minimumsettlementamount',
                        StatusCode: this._HelperService.AppConfig.Status.Active,
                        Value: this._Configurations.MinimumSettlementAmount,
                    };

                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                this._HelperService.CloseModal("Modal_MinimumSettlementAmount");
                                this.GetMerchantConfigurations();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });


        }
    }
    RewardWalletMaxDeductionAmt_Update() {
        if (
            this._Configurations.MaximumRewardDeductionAmount == undefined ||
            this._Configurations.MaximumRewardDeductionAmount == null ||
            this._Configurations.MaximumRewardDeductionAmount == ""
        ) {
            this._HelperService.NotifyError(
                "Please enter rewards wallet maximum amount deduction"
            );
        } else if (isNaN(this._Configurations.MaximumRewardDeductionAmount) == true) {
            this._HelperService.NotifyError(
                "Please enter valid rewards wallet maximum amount deduction"
            );
        } else if (parseFloat(this._Configurations.MaximumRewardDeductionAmount) < 0) {
            this._HelperService.NotifyError(
                "Rewards wallet maximum amount deduction   must be between 0 to 10000000"
            );
        } else if (parseFloat(this._Configurations.MaximumRewardDeductionAmount) > 10000000) {
            this._HelperService.NotifyError(
                "Rewards wallet maximum amount deduction   must be between 0 to 10000000"
            );
        } else {
            swal({
                title: 'Update maximum negative balance to ' + this._Configurations.MaximumRewardDeductionAmount + '?',
                text: 'After updating amount, it takes upto 1 hours to reflect changes, Do you want to continue?',
                position: this._HelperService.AppConfig.Alert_Position,
                animation: this._HelperService.AppConfig.Alert_AllowAnimation,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
                allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
                showCancelButton: true,
                input: 'password',
                inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
                inputAttributes: {
                    autocapitalize: 'off',
                    autocorrect: 'off',
                    maxLength: "4",
                    minLength: "4"
                },
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveaccountconfiguration",
                        AuthPin: result.value,
                        TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                        UserAccountKey: this._UserAccount.ReferenceKey,
                        ConfigurationKey: 'maximumrewarddeductionamount',
                        StatusCode: this._HelperService.AppConfig.Status.Active,
                        Value: this._Configurations.MaximumRewardDeductionAmount,
                    };

                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                this._HelperService.CloseModal("Modal_RewardsWalletMaximumAmountDeduction");
                                this.GetMerchantConfigurations();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });


        }
    }
    SettlementShedule_Update() {
        swal({
            title: 'Update settlement shedule ?',
            text: 'After updating shedule, it takes upto 1 hours to reflect changes, Do you want to continue?',
            position: this._HelperService.AppConfig.Alert_Position,
            animation: this._HelperService.AppConfig.Alert_AllowAnimation,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            showCancelButton: true,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var PostData = {
                    Task: "saveaccountconfiguration",
                    AuthPin: result.value,
                    TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                    UserAccountKey: this._UserAccount.ReferenceKey,
                    ConfigurationKey: 'settlementsheduletype',
                    StatusCode: this._HelperService.AppConfig.Status.Active,
                    Value: this._Configurations.SettlmentShedule,
                    HelperCode: this._Configurations.SettlmentSheduleCode,
                };

                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Configuration Updated");
                            this._HelperService.CloseModal("Modal_SettlementShedule");
                            this.GetMerchantConfigurations();
                        } else {
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }
    PaymentCollectionMethod_Update() {
        swal({
            title: 'Update reward wallet type ?',
            text: 'After updating settings, it takes upto 1 hours to reflect changes, Do you want to continue?',
            position: this._HelperService.AppConfig.Alert_Position,
            animation: this._HelperService.AppConfig.Alert_AllowAnimation,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            showCancelButton: true,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var PostData = {
                    Task: "saveaccountconfiguration",
                    TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                    UserAccountKey: this._UserAccount.ReferenceKey,
                    ConfigurationKey: 'rewarddeductiontype',
                    Value: this._Configurations.RewardDeductionType,
                    HelperCode: this._Configurations.RewardDeductionTypeCode,
                    StatusCode: this._HelperService.AppConfig.Status.Active,
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Configuration Updated");
                            this._HelperService.CloseModal("Modal_SettlementShedule");
                            this.GetMerchantConfigurations();
                        } else {
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }
    UpdateMerchantStatus() {
        swal({
            position: 'top',
            title: 'Activate merchant ?',
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var PostData = {
                    Task: "activateaccount",
                    UserAccountKey: this._UserAccount.ReferenceKey,
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Merchant activated");
                            this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.UserOnboarding.PendingMerchant]);

                        } else {
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }
    ThankUCashPlusStatus_Update() {
        swal({
            title: 'Update ThankUCash Plus Status?',
            text: 'After settings, it takes upto 1 hours to reflect changes, Do you want to continue?',
            position: this._HelperService.AppConfig.Alert_Position,
            animation: this._HelperService.AppConfig.Alert_AllowAnimation,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            showCancelButton: true,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var PostData = {
                    Task: "saveaccountconfiguration",
                    AuthPin: result.value,
                    TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                    UserAccountKey: this._UserAccount.ReferenceKey,
                    ConfigurationKey: "thankucashplus",
                    Value: this._Configurations.ThankUPlus,
                    StatusCode: this._HelperService.AppConfig.Status.Active,
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Configuration Updated");
                            this._HelperService.CloseModal("Modal_ThankUChashPlusStatus");
                            this.GetMerchantConfigurations();
                        } else {
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }
    TUCPlusRewardCriteria_Update() {
        if (
            this._Configurations.ThankUPlusCriteriaValue == undefined ||
            this._Configurations.ThankUPlusCriteriaValue == null ||
            this._Configurations.ThankUPlusCriteriaValue == ""
        ) {
            this._HelperService.NotifyError(
                "Please enter invoice amount"
            );
        } else if (isNaN(this._Configurations.ThankUPlusCriteriaValue) == true) {
            this._HelperService.NotifyError(
                "Please enter valid invoice amount"
            );
        } else if (parseFloat(this._Configurations.ThankUPlusCriteriaValue) < 0) {
            this._HelperService.NotifyError(
                "Invoice amount  must be between 0 to 10000000"
            );
        } else if (parseFloat(this._Configurations.ThankUPlusCriteriaValue) > 10000000) {
            this._HelperService.NotifyError(
                "Invoice amount  must be between 0 to 10000000"
            );
        } else {
            swal({
                title: 'Update reward criteria ?',
                text: 'After settings, it takes upto 1 hours to reflect changes, Do you want to continue?',
                position: this._HelperService.AppConfig.Alert_Position,
                animation: this._HelperService.AppConfig.Alert_AllowAnimation,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
                allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
                showCancelButton: true,
                input: 'password',
                inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
                inputAttributes: {
                    autocapitalize: 'off',
                    autocorrect: 'off',
                    maxLength: "4",
                    minLength: "4"
                },
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveaccountconfiguration",
                        AuthPin: result.value,
                        TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                        UserAccountKey: this._UserAccount.ReferenceKey,
                        ConfigurationKey: "thankucashplusrewardcriteria",
                        HelperCode: this._Configurations.ThankUPlusCriteriaTypeCode,
                        Value: this._Configurations.ThankUPlusCriteriaValue,
                        StatusCode: this._HelperService.AppConfig.Status.Active,
                    };
                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                this._HelperService.CloseModal("Modal_TUCPlusMinTransferAmount");
                                this.GetMerchantConfigurations();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });


        }
    }
    TUCPlusMinTransferAmount_Update() {
        if (
            this._Configurations.ThankUPlusMinimumTransferAmount == undefined ||
            this._Configurations.ThankUPlusMinimumTransferAmount == null ||
            this._Configurations.ThankUPlusMinimumTransferAmount == ""
        ) {
            this._HelperService.NotifyError(
                "Please enter minimum transfer amount"
            );
        } else if (isNaN(this._Configurations.ThankUPlusMinimumTransferAmount) == true) {
            this._HelperService.NotifyError(
                "Please enter valid minimum transfer amount"
            );
        } else if (parseFloat(this._Configurations.ThankUPlusMinimumTransferAmount) < 0) {
            this._HelperService.NotifyError(
                "Minimum transfer amount  must be between 0 to 10000000"
            );
        } else if (parseFloat(this._Configurations.ThankUPlusMinimumTransferAmount) > 10000000) {
            this._HelperService.NotifyError(
                "Minimum transfer amount  must be between 0 to 10000000"
            );
        } else {
            swal({
                title: 'Update minimum transfer amount to ' + this._Configurations.ThankUPlusMinimumTransferAmount + '?',
                text: 'After settings, it takes upto 1 hours to reflect changes, Do you want to continue?',
                position: this._HelperService.AppConfig.Alert_Position,
                animation: this._HelperService.AppConfig.Alert_AllowAnimation,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
                allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
                showCancelButton: true,
                input: 'password',
                inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
                inputAttributes: {
                    autocapitalize: 'off',
                    autocorrect: 'off',
                    maxLength: "4",
                    minLength: "4"
                },
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveaccountconfiguration",
                        AuthPin: result.value,
                        TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                        UserAccountKey: this._UserAccount.ReferenceKey,
                        ConfigurationKey: 'thankucashplusmintransferamount',
                        StatusCode: this._HelperService.AppConfig.Status.Active,
                        Value: this._Configurations.ThankUPlusMinimumTransferAmount,
                    };

                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                this._HelperService.CloseModal("Modal_TUCPlusMinTransferAmount");
                                this.GetMerchantConfigurations();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });


        }
    }
    TotalTransactionPerDay_Update() {
        if (
            this._Configurations.securitymaximumtransactionperdaycap == undefined ||
            this._Configurations.securitymaximumtransactionperdaycap == null ||
            this._Configurations.securitymaximumtransactionperdaycap == 0
        ) {
            this._HelperService.NotifyError("Please enter allowed total allowed transactions per day per customer ");
        } else if (isNaN(this._Configurations.securitymaximumtransactionperdaycap) == true) {
            this._HelperService.NotifyError(
                "Please enter valid total allowed transactions per day per customer"
            );
        } else if (this._Configurations.securitymaximumtransactionperdaycap < 1) {
            this._HelperService.NotifyError(
                "Total allowed transactions per day per customer must be  greater than 0 "
            );
        } else {
            swal({
                title: 'Update total allowed transactions per day per customer to' + this._Configurations.securitymaximumtransactionperdaycap + ' ?',
                text: 'After updating settings, it takes upto 1 hours to reflect changes, Do you want to continue?',
                position: this._HelperService.AppConfig.Alert_Position,
                animation: this._HelperService.AppConfig.Alert_AllowAnimation,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
                allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
                showCancelButton: true,
                input: 'password',
                inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
                inputAttributes: {
                    autocapitalize: 'off',
                    autocorrect: 'off',
                    maxLength: "4",
                    minLength: "4"
                },
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveaccountconfiguration",
                        AuthPin: result.value,
                        TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                        UserAccountKey: this._UserAccount.ReferenceKey,
                        ConfigurationKey: 'securitymaximumtransactionperdaycap',
                        StatusCode: this._HelperService.AppConfig.Status.Active,
                        Value: this._Configurations.securitymaximumtransactionperdaycap,
                    };
                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                this._HelperService.CloseModal("Modal_RewardPercentage");
                                this.GetMerchantConfigurations();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });


        }
    }
    MaximumInvoiceAmountPerDay_Update() {
        if (
            this._Configurations.securitymaximuminvoiceamountperdaycap == undefined || this._Configurations.securitymaximuminvoiceamountperdaycap == null) {
            this._HelperService.NotifyError("Please enter total  allowed invoice amount per day per customer ");
        } else if (isNaN(this._Configurations.securitymaximuminvoiceamountperdaycap) == true) {
            this._HelperService.NotifyError("Please enter valid total  allowed invoice amount per day per customer");
        } else if (this._Configurations.securitymaximuminvoiceamountperdaycap < 0) {
            this._HelperService.NotifyError("Total  allowed invoice amount per day per customer must be greater than  0");
        } else {
            swal({
                title: 'Update  Total  allowed invoice amount per day per customer to N' + this._Configurations.RewardMaximumInvoiceAmount + ' ?',
                text: 'After updating changes , it takes upto 1 hours to reflect changes, Do you want to continue?',
                position: this._HelperService.AppConfig.Alert_Position,
                animation: this._HelperService.AppConfig.Alert_AllowAnimation,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
                allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
                showCancelButton: true,
                input: 'password',
                inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
                inputAttributes: {
                    autocapitalize: 'off',
                    autocorrect: 'off',
                    maxLength: "4",
                    minLength: "4"
                },
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveaccountconfiguration",
                        AuthPin: result.value,
                        TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                        UserAccountKey: this._UserAccount.ReferenceKey,
                        ConfigurationKey: 'securitymaximuminvoiceamountperdaycap',
                        StatusCode: this._HelperService.AppConfig.Status.Active,
                        Value: this._Configurations.securitymaximuminvoiceamountperdaycap,
                    };
                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                this.GetMerchantConfigurations();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });


        }
    }
    AppVisiblityStatus_Update() {
        swal({
            title: 'Update Merchant App Visiblity Status?',
            text: 'After settings, it takes upto 1 hours to reflect changes, Do you want to continue?',
            position: this._HelperService.AppConfig.Alert_Position,
            animation: this._HelperService.AppConfig.Alert_AllowAnimation,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            showCancelButton: true,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var PostData = {
                    Task: "saveaccountconfiguration",
                    AuthPin: result.value,
                    TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                    UserAccountKey: this._UserAccount.ReferenceKey,
                    ConfigurationKey: "merchantappvisiblity",
                    Value: this._Configurations.merchantappvisiblity,
                    StatusCode: this._HelperService.AppConfig.Status.Active,
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Configuration Updated");
                            this.GetMerchantConfigurations();
                        } else {
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }
    MerchantTUCStatus_Update() {
        swal({
            title: 'Update Merchant ThankUCash Status?',
            text: 'After settings, it takes upto 1 hours to reflect changes, Do you want to continue?',
            position: this._HelperService.AppConfig.Alert_Position,
            animation: this._HelperService.AppConfig.Alert_AllowAnimation,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            showCancelButton: true,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var PostData = {
                    Task: "saveaccountconfiguration",
                    AuthPin: result.value,
                    TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                    UserAccountKey: this._UserAccount.ReferenceKey,
                    ConfigurationKey: "enablethankucash",
                    Value: this._Configurations.enablethankucash,
                    StatusCode: this._HelperService.AppConfig.Status.Active,
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Configuration Updated");
                            this.GetMerchantConfigurations();
                        } else {
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }
    MerchantGiftCardStatus_Update() {
        swal({
            title: 'Update Merchant Gift Card Status?',
            text: 'After settings, it takes upto 1 hours to reflect changes, Do you want to continue?',
            position: this._HelperService.AppConfig.Alert_Position,
            animation: this._HelperService.AppConfig.Alert_AllowAnimation,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            showCancelButton: true,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var PostData = {
                    Task: "saveaccountconfiguration",
                    AuthPin: result.value,
                    TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                    UserAccountKey: this._UserAccount.ReferenceKey,
                    ConfigurationKey: "cagiftcards",
                    Value: this._Configurations.cagiftcards,
                    StatusCode: this._HelperService.AppConfig.Status.Active,
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Configuration Updated");
                            this.GetMerchantConfigurations();
                        } else {
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }
    MerchantGiftPointStatus_Update() {
        swal({
            title: 'Update Merchant Gift Point Status?',
            text: 'After settings, it takes upto 1 hours to reflect changes, Do you want to continue?',
            position: this._HelperService.AppConfig.Alert_Position,
            animation: this._HelperService.AppConfig.Alert_AllowAnimation,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            showCancelButton: true,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var PostData = {
                    Task: "saveaccountconfiguration",
                    AuthPin: result.value,
                    TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                    UserAccountKey: this._UserAccount.ReferenceKey,
                    ConfigurationKey: "cagiftpoints",
                    Value: this._Configurations.cagiftpoints,
                    StatusCode: this._HelperService.AppConfig.Status.Active,
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Configuration Updated");
                            this.GetMerchantConfigurations();
                        } else {
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }
    MerchantRewardsStatus_Update() {
        swal({
            title: 'Update Merchant Rewards Status?',
            text: 'After settings, it takes upto 1 hours to reflect changes, Do you want to continue?',
            position: this._HelperService.AppConfig.Alert_Position,
            animation: this._HelperService.AppConfig.Alert_AllowAnimation,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            showCancelButton: true,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var PostData = {
                    Task: "saveaccountconfiguration",
                    AuthPin: result.value,
                    TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                    UserAccountKey: this._UserAccount.ReferenceKey,
                    ConfigurationKey: "merchantreward",
                    Value: this._Configurations.merchantreward,
                    StatusCode: this._HelperService.AppConfig.Status.Active,
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Configuration Updated");
                            this.GetMerchantConfigurations();
                        } else {
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }
    MerchantRedeemStatus_Update() {
        swal({
            title: 'Update Merchant Redeem Status?',
            text: 'After settings, it takes upto 1 hours to reflect changes, Do you want to continue?',
            position: this._HelperService.AppConfig.Alert_Position,
            animation: this._HelperService.AppConfig.Alert_AllowAnimation,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            showCancelButton: true,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var PostData = {
                    Task: "saveaccountconfiguration",
                    AuthPin: result.value,
                    TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                    UserAccountKey: this._UserAccount.ReferenceKey,
                    ConfigurationKey: "merchantredeem",
                    Value: this._Configurations.merchantredeem,
                    StatusCode: this._HelperService.AppConfig.Status.Active,
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Configuration Updated");
                            this.GetMerchantConfigurations();
                        } else {
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }
    MerchantTucRewardToCustomerStatus_Update() {
        swal({
            title: 'Update Reward To ThankUCash Customers Only Status?',
            text: 'After settings, it takes upto 1 hours to reflect changes, Do you want to continue?',
            position: this._HelperService.AppConfig.Alert_Position,
            animation: this._HelperService.AppConfig.Alert_AllowAnimation,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            showCancelButton: true,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var PostData = {
                    Task: "saveaccountconfiguration",
                    AuthPin: result.value,
                    TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                    UserAccountKey: this._UserAccount.ReferenceKey,
                    ConfigurationKey: "tucuserrewardonly",
                    Value: this._Configurations.tucuserrewardonly,
                    StatusCode: this._HelperService.AppConfig.Status.Active,
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Configuration Updated");
                            this.GetMerchantConfigurations();
                        } else {
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }

    public _UserAccount: OUserDetails = {
        ContactNumber: null,
        SecondaryEmailAddress: null,
        ReferenceId: null,
        BankDisplayName: null,
        BankKey: null,
        SubOwnerAddress: null,
        SubOwnerLatitude: null,
        SubOwnerDisplayName: null,
        SubOwnerKey: null,
        SubOwnerLongitude: null,
        AccessPin: null,
        LastLoginDateS: null,
        AppKey: null,
        AppName: null,
        AppVersionKey: null,
        CreateDate: null,
        CreateDateS: null,
        CreatedByDisplayName: null,
        CreatedByIconUrl: null,
        CreatedByKey: null,
        Description: null,
        IconUrl: null,
        ModifyByDisplayName: null,
        ModifyByIconUrl: null,
        ModifyByKey: null,
        ModifyDate: null,
        ModifyDateS: null,
        PosterUrl: null,
        ReferenceKey: null,
        StatusCode: null,
        StatusI: null,
        StatusId: null,
        StatusName: null,
        AccountCode: null,
        AccountOperationTypeCode: null,
        AccountOperationTypeName: null,
        AccountTypeCode: null,
        AccountTypeName: null,
        Address: null,
        AppVersionName: null,
        ApplicationStatusCode: null,
        ApplicationStatusName: null,
        AverageValue: null,
        CityAreaKey: null,
        CityAreaName: null,
        CityKey: null,
        CityName: null,
        CountValue: null,
        CountryKey: null,
        CountryName: null,
        DateOfBirth: null,
        DisplayName: null,
        EmailAddress: null,
        EmailVerificationStatus: null,
        EmailVerificationStatusDate: null,
        FirstName: null,
        GenderCode: null,
        GenderName: null,
        LastLoginDate: null,
        LastName: null,
        Latitude: null,
        Longitude: null,
        MobileNumber: null,
        Name: null,
        NumberVerificationStatus: null,
        NumberVerificationStatusDate: null,
        OwnerDisplayName: null,
        OwnerKey: null,
        Password: null,
        Reference: null,
        ReferralCode: null,
        ReferralUrl: null,
        RegionAreaKey: null,
        RegionAreaName: null,
        RegionKey: null,
        RegionName: null,
        RegistrationSourceCode: null,
        RegistrationSourceName: null,
        RequestKey: null,
        RoleKey: null,
        RoleName: null,
        SecondaryPassword: null,
        SystemPassword: null,
        UserName: null,
        WebsiteUrl: null
    };





}
import { ChangeDetectorRef, Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
import * as cloneDeep from 'lodash/cloneDeep';
declare var $: any;
declare var moment:any;
import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: "tu-tumerchants",
  templateUrl: "./tumerchants.component.html",
})
export class TUMerchantsComponent implements OnInit, OnDestroy {
  public ListType: number;

  DateRangeOptions: any;
  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = true;
    this._HelperService.showAddNewSubAccBtn = false;

    this.DateRangeOptions = cloneDeep(this._HelperService.AppConfig.DateRangeOptions);
    this.DateRangeOptions.opens = "left";

  }

  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();
    this._HelperService.ValidateData();
    // this.ListType = 1;
    this.MerchantsList_Setup();
    this.MerchantsList_Filter_Owners_Load();
    this.CategoriesList_Filter_Owners_Load();
    // this.MerchantsList_Filter_Stores_Load();
    this.InitColConfig();
    // this._HelperService.StopClickPropogation();
    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.MerchantsList_GetData();
    // });
  }


  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }
  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region merchantslist

  public MerchantsList_Config: OList;
  MerchantsList_Setup() {
    this.MerchantsList_Config = {
      Id: null,
      Sort: null,
      Type: "all",
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      Title: "Available Merchants",
      StatusType: "default",
      DefaultSortExpression: "CreateDate desc",
     // Status: this._HelperService.AppConfig.StatusList.defaultaccountitem,
      StatusName: 'Active',
      IsDownload: true,
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: "Merchant Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Contact No",
          SystemName: "ContactNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Store",
          SystemName: "StoreDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Total POS",
          SystemName: "TotalTerminals",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Active POS",
          SystemName: "ActiveTerminals",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        }, {
          DisplayName: "R%",
          SystemName: "RewardPercentage",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Stores",
          SystemName: "Stores",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Balance",
          SystemName: "MainBalance",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Added On",
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
          NavigateField: "ReferenceKey",
        },
      ],
  
    };

    this.MerchantsList_Config.ListType = this.ListType;
    this.MerchantsList_Config.SearchBaseCondition = "";

    if (this.MerchantsList_Config.ListType == 1) // Active
    {
        this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.MerchantsList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.active', "=");
    }
    else if (this.MerchantsList_Config.ListType == 2) // Suspend
    {
        this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.MerchantsList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.suspended', "=");
    }
    else if (this.MerchantsList_Config.ListType == 3) // Blocked
    {
        this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.MerchantsList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.blocked', "=");
    }
    
    else if (this.MerchantsList_Config.ListType == 4) {  // InActive
        this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.MerchantsList_Config.SearchBaseCondition, "StatusCode", 'number', 'default.inactive', "=");
    }
    else if (this.MerchantsList_Config.ListType == 5) {  // All
        this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrictFromArray(this.MerchantsList_Config.SearchBaseCondition, "StatusCode", 'number', [], "=");
    }
    else {
        this.MerchantsList_Config.DefaultSortExpression = 'CreateDate desc';
    }

    this.MerchantsList_Config = this._DataHelperService.List_Initialize(
      this.MerchantsList_Config
    );
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Cashier,
      this.MerchantsList_Config
    );
    this.MerchantsList_GetData();
  }
  MerchantsList_ToggleOption(event: any, Type: any) {
    if (Type == "date") {
      this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
      this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
  }
    if (event != null) {
      for (let index = 0; index < this.MerchantsList_Config.Sort.SortOptions.length; index++) {
        const element = this.MerchantsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.MerchantsList_Config
    );

    this.MerchantsList_Config = this._DataHelperService.List_Operations(
      this.MerchantsList_Config,
      event,
      Type
    );

    if (
      (this.MerchantsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.MerchantsList_GetData();
    }

  }
  MerchantsList_GetData() {
    this.GetOverviews(this.MerchantsList_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.GetMerchantsOverview);

    var TConfig = this._DataHelperService.List_GetData(
      this.MerchantsList_Config
    );
    this.MerchantsList_Config = TConfig;

    this._DataHelperService.statusOption(this.MerchantsList_Config.StatusOptions);
  }
  MerchantsList_RowSelected(ReferenceData) {
    //#region Save Current Merchant To Storage 

    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveMerchant,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveMerchantReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveMerchantReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.RewardPercentage,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);

    //#endregion
  }

  timeout = null;

  MerchantsList_ToggleOptionSearch(event: any, Type: any) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.MerchantsList_Config.Sort.SortOptions.length; index++) {
          const element = this.MerchantsList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.MerchantsList_Config


      );

      this.MerchantsList_Config = this._DataHelperService.List_Operations(
        this.MerchantsList_Config,
        event,
        Type
      );

      if (
        (this.MerchantsList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.MerchantsList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);

  }





  MerchantsList_ListTypeChange(Type) {
    this.ListType = Type;
    this.MerchantsList_Setup();
}
  //#endregion

  public MerchantsList_Filter_Owners_Option: Select2Options;
  public MerchantsList_Filter_Owners_Selected = 0;
  MerchantsList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetAccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      SearchCondition: "",
      SortCondition: ["DisplayName asc" ],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ]
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
      [
        this._HelperService.AppConfig.AccountType.Merchant,
        this._HelperService.AppConfig.AccountType.Acquirer,
        this._HelperService.AppConfig.AccountType.PGAccount,
        this._HelperService.AppConfig.AccountType.PosAccount
      ]
      , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.MerchantsList_Filter_Owners_Option = {
      placeholder: 'Sort by Referrer',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  MerchantsList_Filter_Owners_Change(event: any) {
    if (event.value == this.MerchantsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.MerchantsList_Filter_Owners_Selected, '=');
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.MerchantsList_Config.SearchBaseConditions);
      this.MerchantsList_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.MerchantsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.MerchantsList_Filter_Owners_Selected, '=');
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.MerchantsList_Config.SearchBaseConditions);
      this.MerchantsList_Filter_Owners_Selected = event.value;
      this.MerchantsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.MerchantsList_Filter_Owners_Selected, '='));
    }

    this.MerchantsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.MerchantsList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );
  }
  
  
  SetOtherFilters(): void {
    this.MerchantsList_Config.SearchBaseConditions = [];
    // this.MerchantsList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    // if (CurrentIndex != -1) {
    //   this.MerchantsList_Filter_Stores_Selected = null;
    //   this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    // }
  }


  
  
  public CategoriesList_Filter_Owners_Option: Select2Options;
  public CategoriesList_Filter_Owners_Selected = 0;
  CategoriesList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetCoreCommons,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ]
    };

    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'TypeCode', this._HelperService.AppConfig.DataType.Text,
      [
        this._HelperService.AppConfig.HelperTypes.MerchantCategories,
      ]
      , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.CategoriesList_Filter_Owners_Option = {
      placeholder: 'Filter By Categories',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  CategoriesList_Filter_Owners_Change(event: any) {
    if (event.value == this.CategoriesList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CategoryKey', this._HelperService.AppConfig.DataType.Text, this.CategoriesList_Filter_Owners_Selected, '=');
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.MerchantsList_Config.SearchBaseConditions);
      this.CategoriesList_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.CategoriesList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CategoryKey', this._HelperService.AppConfig.DataType.Text, this.CategoriesList_Filter_Owners_Selected, '=');
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.MerchantsList_Config.SearchBaseConditions);
      this.CategoriesList_Filter_Owners_Selected = event.value;
      this.MerchantsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CategoryKey', this._HelperService.AppConfig.DataType.Text, this.CategoriesList_Filter_Owners_Selected, '='));
    }
    
    this.MerchantsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {

    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.MerchantsList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
    if (Type == 'Time') {
      this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
      this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
  }
    this.SetOtherFilters();

    this.MerchantsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      // inputAttributes: {
      //   autocapitalize: "off",
      //   autocorrect: "off",
      //   maxLength: "4",
      //   minLength: "4",
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Cashier,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Cashier
        );
        this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
        this.MerchantsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.MerchantsList_GetData();

    if (ButtonType == 'Sort') {
      $("#MerchantsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#MerchantsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
    this.SetOtherFilters();

    this.MerchantsList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    // this.MerchantsList_Filter_Stores_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  public OverviewData: any = {
    Total: 0,
    Active: 0,
    Blocked: 0,

  };


  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);
    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: SortExpression,
      Type: ListOptions.Type,
      ReferenceKey: ListOptions.ReferenceKey,
      StartDate: ListOptions.StartDate,
      EndDate: ListOptions.EndDate,
      ReferenceId: ListOptions.ReferenceId,
      SubReferenceId: ListOptions.SubReferenceId,
      SubReferenceKey: ListOptions.SubReferenceKey,
      AccountId: ListOptions.AccountId,
      AccountKey: ListOptions.AccountKey,
      //OwnerId: ListOptions.OwnerId,
      //OwnerKey: ListOptions.OwnerKey,
       
      ListType: ListOptions.ListType,
      IsDownload: false,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.OverviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });


  }

  GotoAddMerchant() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.UserOnboarding.Merchant
    ]);
  }


  merchantOnBoarding() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.OnBoardMerchant.merchantOnboardingBusiness
    ]);
  }
}

import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import { Observable, Subscription } from 'rxjs';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse } from "../../../../service/service";
import swal from 'sweetalert2';
declare var moment: any;

declare var $: any;


@Component({
  selector: "tu-wallate",
  templateUrl: "./wallate.component.html",
})
export class TUWallateComponent implements OnInit {
  public ResetFilterControls: boolean = true;
  CurrentPlan: any = {
    Name: null,
    TypeName: null,
    SellingPrice: null,
    Policy: null,


  };
  FreePlan: any = {
    Name: null,
    TypeName: null,
    SellingPrice: null,
    Policy: null,


  };
  BasicPlan: any = {
    Name: null,
    TypeName: null,
    SellingPrice: null,
    Policy: null,
  };
  PremiumPlan: any = {
    Name: null,
    TypeName: null,
    SellingPrice: null,
    Policy: null,
  };
//start  Inovicehistrory static data for test
  invoiceHistory=[
    {
      StartDate:"11-06-2021",
      Service:"Basic Subscription",
      Amount:"700"
    },
    {
      StartDate:"15-06-2021",
      Service:"Basic Subscription",
      Amount:"700"
    },
    {
      StartDate:"19-06-2021",
      Service:"Basic Subscription",
      Amount:"700"
    },
    {
      StartDate:"20-06-2021",
      Service:"Basic Subscription",
      Amount:"700"
    },
  ]
//end Inovicehistrory static data for test
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
  }
  public _ObjectSubscription: Subscription = null;
  ngOnInit() {
    Feather.replace();


    // this.GetSubscription();


    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.GetBalance();
    // });

    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];
      this.TopUpHistoryList_Setup();
      this.GetAccountSubscription();
      this.GetBalance();

    });
  }

  ngOnDestroy(): void {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
    this._HelperService.showAddNewCashierBtn = false;

    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }
  }


  public choosenPlan: string;
  public SubscriptionId: number;
  public SubscriptionKey: string;
  selectPlan(plan: string) {
    this.choosenPlan = plan;
    if (plan == 'basic') {
      this.SubscriptionId = this.BasicPlan.ReferenceId
      this.SubscriptionKey = this.BasicPlan.ReferenceKey
    }
    if (plan == 'premium') {
      this.SubscriptionId = this.PremiumPlan.ReferenceId
      this.SubscriptionKey = this.PremiumPlan.ReferenceKey
    }
    this._HelperService.CloseModal('Plans');
  }

  closePlanDetail() {
    this._HelperService.OpenModal('Plans');
  }
  closePremiumPlanDetail() {
    this._HelperService.OpenModal('Plans');
  }

  public _TransactionReference = null;
  public _AccountBalanceCreditAmount = 0;
  public PaymentReference = null;

  generateRandomNumber() {
    var TrRef = Math.floor(100000 + Math.random() * (999999 + 1 - 100000));
    return TrRef;
  }
  OpenPaymentOptions() {
    this.TransactionId = this.generateRandomNumber();
    // var Ref = this.generateRandomNumber();
    // this._TransactionReference = this._HelperService.AppConfig.ActiveMerchantReferenceId + '_' + this._HelperService.UserAccount.ReferralCode + '_' + Ref;
    this._HelperService.OpenModal('Form_AddUser_Content2');
  }
  paymentDone(ref: any) {
    this.TransactionId = ref.trans
    if (ref != undefined && ref != null && ref != '') {
      if (ref.status == 'success') {
        this.CreditAmount();
      }
      else {
        this._HelperService.NotifyError('Payment failed');
      }
    }
    else {
      this._HelperService.NotifyError('Payment failed');
    }
    // this.title = 'Payment successfull';
  }

  public TransactionId
  public CreditAmount() {
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "topupaccount",
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      Amount: this._AccountBalanceCreditAmount,
      PaymentReference: this.PaymentReference,
      TransactionId: this.TransactionId
    };
    if (this._AccountBalanceCreditAmount == 0) {
      this._HelperService.NotifyError(" Amount must be greater than 0");
      this._HelperService.IsFormProcessing = false;

    }
    else {
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscription, PostData);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._AccountBalanceCreditAmount = 0;
            this._HelperService.IsFormProcessing = false;

            this.PaymentReference = null;
            this._TransactionReference = null;
            this._HelperService.NotifySuccess("Account credited");
            // this._HelperService.CloseModal('Form_AddUser_Content2');
            // this.GetBalance();
            setTimeout(()=>{
              location.reload();
            },1000);
            

          } else {
            this._HelperService.NotifyError("Payment Reference Required");
            this._HelperService.IsFormProcessing = false;

          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );
    }
  }
  paymentCancel() {
  }



  //Subscription Payment

  public _SubscriptionTransactionReference = null;
  SubscriptionDone(ref: any, SubscriptionId, SubscriptionKey) {
    this.TransactionId = ref.trans
    if (ref != undefined && ref != null && ref != '') {
      if (ref.status == 'success') {
        this.Subscription();
      }
      else {
        this._HelperService.NotifyError('Payment failed');
      }
    }
    else {
      this._HelperService.NotifyError('Payment failed');
    }
  }

  ShowPaymentRef(): void {
    this.TransactionId = this.generateRandomNumber();

    var Ref = this.generateRandomNumber();
    this._TransactionReference = this._HelperService.AppConfig.ActiveMerchantReferenceId + '_' + this._HelperService.UserAccount.ReferralCode + '_' + Ref;

    // this.PaymentReference = this.generateRandomNumber();
    this._HelperService.OpenModal('Form_PaymentRef_Content');
  }

  ClosePaymentRef(): void {
    this.selectPlan(this.choosenPlan);

    if (this.choosenPlan == 'basic') {
      this._HelperService.OpenModal('BasicPlans');
    }
    if (this.choosenPlan == 'premium') {
      this._HelperService.OpenModal('PremiumPlans');
    }

  }

  public Subscription() {
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "updatesubscription",
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      SubscriptionId: this.SubscriptionId,
      SubscriptionKey: this.SubscriptionKey,
      PaymentReference: null,
    };
    let _OResponse: Observable<OResponse>;
    if (this._SubscriptionTransactionReference) {
      PostData.PaymentReference = this._HelperService.AppConfig.ActiveMerchantReferenceId + '_' + this._HelperService.UserAccount.ReferralCode + '_' + this._SubscriptionTransactionReference;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscriptions, PostData);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._AccountBalanceCreditAmount = 0;
            this._SubscriptionTransactionReference = null;
            this._TransactionReference = null;
            this._HelperService.NotifySuccess(_Response.Message);
            this._HelperService.CloseModal('Form_PaymentRef_Content');
            this.GetAccountSubscription();

          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );
    }
    else {

      this._HelperService.NotifyError('Please enter payment reference number');

    }

  }
  //end Subscription

  SubscriptionpaymentCancel() {

  }

  FreePlanActivate() {
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "updatesubscription",
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      SubscriptionId: 1,
      SubscriptionKey: 'thankucashmerchantfree',
      PaymentReference: this._SubscriptionTransactionReference,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscriptions, PostData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this._HelperService.CloseModal('FreePlans');
          this.GetAccountSubscription();

        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }


  public _AccountBalance =
    {
      Credit: 0,
      Debit: 0,
      Balance: 0
    }
  public GetBalance() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: 'getaccountbalance',
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      Source: this._HelperService.AppConfig.TransactionSource.Merchant
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscription, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;

          this._AccountBalance = _Response.Result;
          // this._AccountBalance.Balance = _Response.Result.Balance / 100;
          // this._AccountBalance.Credit = _Response.Result.Credit / 100;
          // this._AccountBalance.Debit = _Response.Result.Debit / 100;
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  OpenTopUpHistory() {
    this._HelperService.OpenModal('TopupHistory')
    this.TopUpHistoryList_Setup();
  }
  public TopUpHistoryList_Config: OList;

  // public TopUpHistoryList_Config: OList = {
  //   Id: null,
  //   Task: this._HelperService.AppConfig.Api.ThankUCash.GetTopupHistory,
  //   Location: this._HelperService.AppConfig.NetworkLocation.V3.Subscription,
  //   Title: "Available Stores",
  //   StatusType: "default",
  //   AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
  //   AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
  //   Sort:
  //   {
  //     SortDefaultName: null,
  //     SortDefaultColumn: 'TransactionDate',
  //     SortName: null,
  //     SortColumn: null,
  //     SortOrder: 'desc',
  //     SortOptions: [],
  //   },

  //   TableFields: [
  //     {
  //       DisplayName: "Reference Number",
  //       SystemName: "PaymentReference",
  //       DataType: this._HelperService.AppConfig.DataType.Text,
  //       Class: "",
  //       Show: true,
  //       Search: true,
  //       Sort: false,
  //       ResourceId: null,
  //       NavigateField: "ReferenceKey"
  //     },


  //     {
  //       DisplayName: "Amount",
  //       SystemName: "Amount",
  //       DataType: this._HelperService.AppConfig.DataType.Decimal,
  //       Class: "text-right",
  //       Show: true,
  //       Search: false,
  //       Sort: true,
  //       ResourceId: null,
  //       NavigateField: "ReferenceKey"
  //     },

  //     {
  //       DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
  //       SystemName: "TransactionDate",
  //       DataType: this._HelperService.AppConfig.DataType.Date,
  //       Class: "td-date text-right",
  //       Show: true,
  //       Search: false,
  //       Sort: true,
  //       ResourceId: null,
  //       NavigateField: "ReferenceKey",
  //       IsDateSearchField: true,

  //     },
  //   ],

  //   Data: [{}]
  // };
  TopUpHistoryList_Setup() {
    this.TopUpHistoryList_Config = {
      Id: null,
      // Task: this._HelperService.AppConfig.Api.ThankUCash.GetTopupHistory,
      // Location: this._HelperService.AppConfig.NetworkLocation.V3.Subscription,
      Task: this._HelperService.AppConfig.Api.Core.GetPointPurchaseHistory,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.M,
      Title: "Available Stores",
      StatusType: "default",
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'TransactionDate',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },
      TableFields: [
        {
          DisplayName: '#',
          SystemName: 'ReferenceId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Show: true,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: "Reference Number",
          SystemName: "PaymentReference",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },

        {
          DisplayName: 'Amount',
          SystemName: 'Amount',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Show: true,
          Search: false,
          Sort: true,
        },

        {
          DisplayName: 'Date',
          SystemName: 'TransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Show: true,
          Search: false,
          Sort: true,
          IsDateSearchField: true,
        },
        {
          DisplayName: 'Created By',
          SystemName: 'CreatedByDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: true,
          Sort: true,
        },

        {
          DisplayName: 'Payment Source',
          SystemName: 'PaymentMethodName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: true,
          Sort: true,
        },

      ],
      // Data: [{}]
    };
    this.TopUpHistoryList_Config = this._DataHelperService.List_Initialize(
      this.TopUpHistoryList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.console_cashout,
      this.TopUpHistoryList_Config
    );

    this.TopUpHistoryList_GetData();
  }
  TopUpHistoryList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.TopUpHistoryList_Config.Sort.SortOptions.length; index++) {
        const element = this.TopUpHistoryList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.TopUpHistoryList_Config


    );

    this.TopUpHistoryList_Config = this._DataHelperService.List_Operations(
      this.TopUpHistoryList_Config,
      event,
      Type
    );

    if (
      (this.TopUpHistoryList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TopUpHistoryList_GetData();
    }

  }
   timeout = null;
  TopUpHistoryList_ToggleOptionSearch(event: any, Type: any) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.TopUpHistoryList_Config.Sort.SortOptions.length; index++) {
          const element = this.TopUpHistoryList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(event, Type, this.TopUpHistoryList_Config);

      this.TopUpHistoryList_Config = this._DataHelperService.List_Operations(this.TopUpHistoryList_Config, event, Type);

      if (
        (this.TopUpHistoryList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.TopUpHistoryList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);
  }
  TopUpHistoryList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(this.TopUpHistoryList_Config)
    this.TopUpHistoryList_Config = TConfig;

  }




  TopUpHistoryList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveStore,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
      }
    );

    this._HelperService.AppConfig.ActiveMerchantReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveMerchantReferenceId = ReferenceData.ReferenceId;




  }
  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantSalesConfig(this.TopUpHistoryList_Config);

    //#region setOtherFilters
    // this.SetOtherFilters();
    // this.SetSalesRanges();
    //#endregion

    this.TopUpHistoryList_GetData();
}


Save_NewFilter() {
  swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
          '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
          '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
          '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
          return {
              filter: document.getElementById('swal-input1')['value'],
              private: document.getElementById('swal-input2')['checked']
          }
      },
   
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
  }).then((result) => {
      if (result.value) {

          if (result.value.filter.length < 5) {
              this._HelperService.NotifyError('Enter filter name length greater than 4');
              return;
          }

          this._HelperService._RefreshUI = false;
          this._ChangeDetectorRef.detectChanges();

          this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

          var AccessType: number = result.value.private ? 0 : 1;
          this._HelperService.Save_NewFilter(
              this._HelperService.AppConfig.FilterTypeOption.console_cashout,
              AccessType
          );

          this._HelperService._RefreshUI = true;
          this._ChangeDetectorRef.detectChanges();
      }
  });
}
Delete_Filter() {

  swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

  }).then((result) => {
      if (result.value) {
          this._HelperService._RefreshUI = false;
          this._ChangeDetectorRef.detectChanges();

          this._HelperService.Delete_Filter(
              this._HelperService.AppConfig.FilterTypeOption.console_cashout
          );
          this._FilterHelperService.SetMerchantSalesConfig(this.TopUpHistoryList_Config);
          this.TopUpHistoryList_GetData();

          this._HelperService._RefreshUI = true;
          this._ChangeDetectorRef.detectChanges();
      }
  });
}
  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.TopUpHistoryList_GetData();

    if (ButtonType == 'Sort') {
      $("#TopUpHistoryList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#TopUpHistoryList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }
  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_MerchantSales(Type, index);
    this._FilterHelperService.SetMerchantSalesConfig(this.TopUpHistoryList_Config);

    if (Type == 'Time') {
        var daterangetoday = {
            start: this._HelperService.DateInUTC(new Date(2017, 0, 1, 0, 0, 0, 0)),
            end: this._HelperService.DateInUTC(moment().endOf("day"))
        };
        this.TopUpHistoryList_Config = this._DataHelperService.List_Operations(this.TopUpHistoryList_Config, daterangetoday, this._HelperService.AppConfig.ListToggleOption.Date);
    }

    //#region setOtherFilters
  
    //#endregion

    this.TopUpHistoryList_GetData();
}

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.TopUpHistoryList_Config);

    this.TopUpHistoryList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  //#endregion
  ViewPlans() {
    this.GetSubscription();
    var Ref = this.generateRandomNumber();
    // this._SubscriptionTransactionReference = this._HelperService.AppConfig.ActiveMerchantReferenceId + '_' + this._HelperService.UserAccount.ReferralCode + '_' + Ref;
  }

  public _AccountSubscription = {

    Data: []
  }



  public GetSubscription() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: 'getsubscriptions',
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      Source: this._HelperService.AppConfig.TransactionSource.Merchant
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscriptions, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this._AccountSubscription = _Response.Result;
          this.GetAccountSubscription();

          for (let index = 0; index < this._AccountSubscription.Data.length; index++) {
            const element: any = this._AccountSubscription.Data[index];
            if (element.ReferenceId == 1) {
              this.FreePlan = element
            }
            else if (element.ReferenceId == 2) {
              this.BasicPlan = element
              // document.getElementById("BasicPlanPolicy").innerHTML = element.Policy

            }
            else {
              this.PremiumPlan = element

            }
            for (let index = 0; index < element.Features.length; index++) {
              const features = element.Features[index];

            }


          }
          // this._AccountBalance.Balance = _Response.Result.Balance / 100;
          // this._AccountBalance.Credit = _Response.Result.Credit / 100;
          // this._AccountBalance.Debit = _Response.Result.Debit / 100;
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public _GetAccountSubscription = {

    Data: []
  }

  public GetAccountSubscription() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: 'getaccountsubscriptions',
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      Offset: 0,
      Limit: 10,
      // Source: this._HelperService.AppConfig.TransactionSource.Merchant
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscriptions, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this._GetAccountSubscription = _Response.Result;

          for (let index = 0; index < this._GetAccountSubscription.Data.length; index++) {
            const element: any = this._GetAccountSubscription.Data[index];


            if (this._GetAccountSubscription.Data[0]) {
              this.CurrentPlan = this._GetAccountSubscription.Data[0]


            }


            // for (let index = 0; index < this._GetAccountSubscription.Data.length; index++) {
            //   const element = this._GetAccountSubscription.Data[index];


            // }


            for (let index = 0; index < element.Features.length; index++) {
              const features = element.Features[index];

            }


          }
          // this._AccountBalance.Balance = _Response.Result.Balance / 100;
          // this._AccountBalance.Credit = _Response.Result.Credit / 100;
          // this._AccountBalance.Debit = _Response.Result.Debit / 100;
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


}

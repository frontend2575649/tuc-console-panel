import { ChangeDetectorRef, Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;
declare var moment: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
  OStorageContent,
} from "../../../../service/service";
import { Observable, Subscription } from 'rxjs';
import { InputFileComponent, InputFile } from 'ngx-input-file';
import { ImageCroppedEvent } from "ngx-image-cropper";

@Component({
  selector: "tu-tusubaccounts",
  templateUrl: "./tusubaccounts.component.html",
})
export class TUSubAccountsComponent implements OnInit, OnDestroy {

  imageChangedEvent: any = '';
  croppedImage: any = '';
  croppedImage1: any = '';
  ngOnDestroy(): void {
    try {
      this._HelperService.showAddNewSubAccBtn = false;
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }

  }
  public ResetFilterControls: boolean = true;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = true;


  }

  public _ObjectSubscription: Subscription = null;

  ngOnInit() {
    this.Form_UpdateSubAccount_Load();
    this.SubAccountsList_Filter_Stores_Load();
    this.InitColConfig();
    this.TUTr_Filter_Stores_Load();
    this.getroles_list();
    this._HelperService.StopClickPropogation();
    Feather.replace();

    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];

      this.SubAccountsList_Setup();
    });
    // this._HelperService.StopClickPropogation();
    //this.TestNavigation();
    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.SubAccountsList_GetData();
    // });
  }
  jpeg: "jpeg";
  IconContent: OStorageContent = {
    Name: null,
    Content: null,
    Extension: null,
    TypeCode: null,
    Height: 400,
    Width: 800
  };

  fileChangeEvent(event: any) {
    this.imageChangedEvent = event;
    this._HelperService.OpenModal("imageCropperModule");
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    this.croppedImage1 = this.croppedImage.replace("data:image/png;base64,", "")
    this.IconContent.Content = this.croppedImage1;
    this.IconContent.Extension = event.file.type;

  }

  Icon_Crop_Clear() {
    this.croppedImage1 = '';
    this.croppedImage = '';
    this.IconContent.Content = null;
    this.IconContent.Extension = null;
    this.ImageIconUrl= this._SubAccountDetails.IconUrl;
    let image = document.getElementById('imageInput');
    var $el = $('#imageInput');
                $el.wrap('<form>').closest(
                  'form').get(0).reset();
                $el.unwrap();
    this._HelperService.CloseModal("imageCropperModule");
  

  }

  croppedImaged() {
    this.ImageIconUrl=this.croppedImage;
    this._HelperService.CloseModal("imageCropperModule");
  }

  croperReady() {

  }
  loadImageFailed() {

  }
  
  public _Roles = [
    // {
    //   'id': 0,
    //   'text': 'Select Role',
    //   'apival': 'Select Role'
    // },
    {
      'id': 10,
      'text': 'Admin',
      'apival': 'merchantadmin'
    },
    {
      'id': 11,
      'text': 'Store Manager',
      'apival': 'merchantstoremanager'
    }

  ];
  public GetRoles_Option: Select2Options;
  public GetRoles_Transport: any;
  getroles_list() {
    this.GetRoles_Option = {
      placeholder: 'List',
      ajax: this.GetRoles_Transport,
      multiple: false,
    };
  }
  GetRoles_ListChange(event: any) {
    this.Form_UpdateSubAccount.patchValue(
      {
        RoleKey: event.data[0].apival,
        RoleId: event.value

      }
    );
    this.ToggleStoreSelect = false;

    if (this.Form_UpdateSubAccount.controls['RoleKey'].value == 'manager') {
      this.TUTr_Filter_Stores_Load();
      setTimeout(() => {
        this.ToggleStoreSelect = true;
        this.TUTr_Filter_Stores_Load();
      }, 500);
    }
    else {
      this.Form_UpdateSubAccount.patchValue({
        StoreReferenceKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
        StoreReferenceId: this._HelperService.AppConfig.ActiveMerchantReferenceId

      })
    }



  }


  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region subaccountslist

  public SubAccountsList_Config: OList;
  SubAccountsList_Setup() {
    this.SubAccountsList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetSubAccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      Title: "Available SubAccounts",
      StatusType: "default",
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: "Sub Account Name",
          SystemName: "Name",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "User Name",
          SystemName: "UserName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null
        },
        {
          DisplayName: "Contact No",
          SystemName: "ContactNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "LastActivity On",
          SystemName: "LastActivityDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: "Added On",
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.SubAccountsList_Config = this._DataHelperService.List_Initialize(
      this.SubAccountsList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.SubAccount,
      this.SubAccountsList_Config
    );

    this.SubAccountsList_GetData();
  }
  SubAccountsList_ToggleOption(event: any, Type: any) {
    if (Type == "date") {
      this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
      this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
  }

    if (event != null) {
      for (let index = 0; index < this.SubAccountsList_Config.Sort.SortOptions.length; index++) {
        const element = this.SubAccountsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.SubAccountsList_Config


    );

    this.SubAccountsList_Config = this._DataHelperService.List_Operations(
      this.SubAccountsList_Config,
      event,
      Type
    );

    if (
      (this.SubAccountsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.SubAccountsList_GetData();
    }

  }

  timeout = null;
  SubAccountsList_ToggleOptionSearch(event: any, Type: any) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.SubAccountsList_Config.Sort.SortOptions.length; index++) {
          const element = this.SubAccountsList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.SubAccountsList_Config


      );

      this.SubAccountsList_Config = this._DataHelperService.List_Operations(
        this.SubAccountsList_Config,
        event,
        Type
      );

      if (
        (this.SubAccountsList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.SubAccountsList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);
  }

  SubAccountsList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.SubAccountsList_Config
    );
    this.SubAccountsList_Config = TConfig;
  }
  SubAccountsList_RowSelected(ReferenceData) {

    this.GetSubAccountsDetails(ReferenceData, true);
  }

  //#endregion

  //#region StoreFilter

  public SubAccountsList_Filter_Stores_Option: Select2Options;
  public SubAccountsList_Filter_Stores_Selected = null;
  SubAccountsList_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.SubAccountsList_Filter_Stores_Option = {
      placeholder: "Select Store",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  SubAccountsList_Filter_Stores_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.SubAccountsList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.SubAccountsList_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.SubAccountsList_Filter_Stores_Selected,
        "="
      );
      this.SubAccountsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.SubAccountsList_Config.SearchBaseConditions
      );
      this.SubAccountsList_Filter_Stores_Selected = null;
    } else if (event.value != this.SubAccountsList_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.SubAccountsList_Filter_Stores_Selected,
        "="
      );
      this.SubAccountsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.SubAccountsList_Config.SearchBaseConditions
      );
      this.SubAccountsList_Filter_Stores_Selected = event.data[0].ReferenceKey;
      this.SubAccountsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "StoreReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.SubAccountsList_Filter_Stores_Selected,
          "="
        )
      );
    }

    this.SubAccountsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion
  public ToggleStoreSelect: boolean = false;
  public TUTr_Filter_Store_Option: Select2Options;
  TUTr_Filter_Stores_Load() {

    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
        // {
        //   SystemName: "AccountTypeCode",
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   SearchCondition: "=",
        //   SearchValue: this._HelperService.AppConfig.AccountType.Store
        // }
      ]
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'MerchantReferenceId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveMerchantReferenceId, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Store_Option = {
      // placeholder: 'Select Store',
      placeholder: this._SubAccountDetails.RoleName,
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Stores_Change(event: any) {

    this.Form_UpdateSubAccount.patchValue(
      {
        StoreReferenceId: event.data[0].ReferenceId,
        StoreReferenceKey: event.data[0].ReferenceKey,
        StoreName: event.data[0].DisplayName,

      }


    );

  }

  SetOtherFilters(): void {
    this.SubAccountsList_Config.SearchBaseConditions = [];
    this.SubAccountsList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.SubAccountsList_Filter_Stores_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.SubAccountsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.SubAccountsList_GetData();
  }



  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.SubAccountsList_Config);
    if (Type == 'Time') {
      this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
      this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
  }
    this.SetOtherFilters();

    this.SubAccountsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.SubAccount
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.SubAccount
        );
        this._FilterHelperService.SetMerchantConfig(this.SubAccountsList_Config);
        this.SubAccountsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.SubAccountsList_GetData();

    if (ButtonType == 'Sort') {
      $("#SubAccountsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#SubAccountsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }


  CloseRowModal(index: number): void {
    $("#SubAccountsList_rdropdown_" + index).dropdown('toggle');
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.SubAccountsList_Config);
    this.SetOtherFilters();

    this.SubAccountsList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.SubAccountsList_Filter_Stores_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  TestNavigation(): void {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.SubAccountDetails.SalesHistory,
      "ReferenceData.ReferenceKey",
      "ReferenceData.ReferenceId",
    ]);
  }

  //#region Update SubAccount 

  Form_UpdateSubAccount: FormGroup;
  Form_UpdateSubAccount_Show() {
    this._HelperService.OpenModal("Form_UpdateSubAccount_Content");


  }
  Form_UpdateSubAccount_Close() {
    this._HelperService.CloseModal("Form_UpdateSubAccount_Content");
  }
  Form_UpdateSubAccount_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_UpdateSubAccount = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.UpdateSubAccount,
      StoreReferenceId: [null],
      StoreReferenceKey: [null],
      RoleId: null,
      RoleKey: null,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
      StatusCode: this._HelperService.AppConfig.Status.Active,
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      GenderCode: this._HelperService.AppConfig.Gender.Male,
    });
  }
  Form_UpdateSubAccount_Clear() {
    this.Form_UpdateSubAccount.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_UpdateSubAccount_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }

  //   removeCategory(cat: any): void {
  //     this._SubAccountDetails.UpdateCategories.splice(cat, 1);
  //     this.reset();

  // }


  Form_UpdateSubAccount_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;

    _FormValue.AccountKey = this._SubAccountDetails.ReferenceKey;
    _FormValue.AccountId = this._SubAccountDetails.ReferenceId;
    if (this.IconContent.Content != null) {
      _FormValue.IconContent = this.IconContent;
    }


    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.Account,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.FlashSwalSuccess("SubAccount has been updated successfully",
            "Done! You have successfully updated SubAccount");
          // this.Form_UpdateSubAccount_Clear();
          this.Form_UpdateSubAccount_Close();
          this.Icon_Crop_Clear();
          if (_FormValue.OperationType == "close") {
            this.Form_UpdateSubAccount_Close();
          }
          this.SubAccountsList_GetData();
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region SubAccountDetails 

  public _SubAccountDetails: any =
    {
      ManagerName: null,
      BranchName: null,
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }


  RoleName: boolean = true;
  ImageIconUrl: any;
  GetSubAccountsDetails(SubAccountsDetails: any, showModal: boolean) {

    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetSubAccount,
      // AccountId: this._HelperService.UserAccount.AccountId,
      // AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountKey: SubAccountsDetails.ReferenceKey,
      AccountId: SubAccountsDetails.ReferenceId,

    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._SubAccountDetails = _Response.Result;
         
          this.ImageIconUrl = this._SubAccountDetails.IconUrl;
          this._SubAccountDetails.CreateDate = this._HelperService.GetDateS(this._SubAccountDetails.CreateDate);
          this._SubAccountDetails.ModifyDate = this._HelperService.GetDateS(this._SubAccountDetails.ModifyDate);
          this._SubAccountDetails.StatusI = this._HelperService.GetStatusIcon(this._SubAccountDetails.StatusCode);
          this._SubAccountDetails.StatusB = this._HelperService.GetStatusBadge(this._SubAccountDetails.StatusCode);
          this._SubAccountDetails.StatusC = this._HelperService.GetStatusColor(this._SubAccountDetails.StatusCode);
          // this._SubAccountAddress = this._SubAccountDetails.Address;
          this.TUTr_Filter_Store_Option.placeholder = this._SubAccountDetails.RoleName;
          this.Form_UpdateSubAccount.controls['RoleId'].setValue(this._SubAccountDetails.RoleId);
          this.Form_UpdateSubAccount.controls['RoleId'].setValue(this._SubAccountDetails.RoleId);

          this.GetRoles_Option.placeholder = this._SubAccountDetails.RoleName;

          this.Form_UpdateSubAccount.controls['RoleKey'].setValue(this._SubAccountDetails.RoleKey);
          this.Form_UpdateSubAccount.controls['StoreReferenceId'].setValue(this._SubAccountDetails.StoreReferenceId);
          this.Form_UpdateSubAccount.controls['StoreReferenceKey'].setValue(this._SubAccountDetails.StoreReferenceKey);
          this.TUTr_Filter_Store_Option.placeholder = this._SubAccountDetails.StoreDislayName;
          this.RoleName = false;
          this._ChangeDetectorRef.detectChanges();
          this.GetRoles_Option.placeholder = this._SubAccountDetails.RoleName;
          this.RoleName = true;
          this._ChangeDetectorRef.detectChanges();

          this.SubAccountsList_GetData();
          //#region ResponseInit 


          this._SubAccountDetails.EndDateS = this._HelperService.GetDateS(
            this._SubAccountDetails.EndDate
          );
          this._SubAccountDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._SubAccountDetails.CreateDate
          );
          this._SubAccountDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._SubAccountDetails.ModifyDate
          );
          this._SubAccountDetails.StatusI = this._HelperService.GetStatusIcon(
            this._SubAccountDetails.StatusCode
          );
          this._SubAccountDetails.StatusB = this._HelperService.GetStatusBadge(
            this._SubAccountDetails.StatusCode
          );
          this._SubAccountDetails.StatusC = this._HelperService.GetStatusColor(
            this._SubAccountDetails.StatusCode
          );

          //#endregion

          //#region InitLocationParams 
          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {

            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;

            // this.Form_EditUser_Latitude = _Response.Result.Latitude;
            // this.Form_EditUser_Longitude = _Response.Result.Longitude;

            // this.Form_EditUser.controls['Latitude'].setValue(_Response.Result.Latitude);
            // this.Form_EditUser.controls['Longitude'].setValue(_Response.Result.Longitude);

          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }

          // this.Form_EditUser.controls['StoreId'].setValue(_Response.Result.StoreReferenceId);
          // this.Form_EditUser.controls['StoreKey'].setValue(_Response.Result.StoreReferenceKey);
          //#endregion

          if (showModal) {
            this._HelperService.OpenModal('Form_UpdateSubAccount_Content');
          }

        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region Block - UnBlock Code 

  Title: string;
  BlockSubAccount(TItem) {
    if (TItem.StatusCode == 'default.active') {
      this.Title = "Block";
    } else {
      this.Title = "Un-Block";
    }

    this._SubAccountDetails = TItem;
    this._HelperService.OpenModal("BlockPos");

  }

  public Block(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "disablesubaccount",
      AccountId: this._SubAccountDetails.ReferenceId,
      AccountKey: this._SubAccountDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess("Subaccount blocked successfully ");
          this._HelperService.CloseModal("BlockPos");
          this.SubAccountsList_GetData();

        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public UnBlock(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "enablesubaccount",
      AccountId: this._SubAccountDetails.ReferenceId,
      AccountKey: this._SubAccountDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess("SubAccount Enabled Successfully ");
          this._HelperService.CloseModal("BlockPos");
          this.SubAccountsList_GetData();
        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

}

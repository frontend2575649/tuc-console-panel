import { ChangeDetectorRef, Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ChartDataSets } from 'chart.js';
import * as Feather from 'feather-icons';
import * as cloneDeep from 'lodash/cloneDeep';
import { BaseChartDirective, Color } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OResponse, OSalesTrend, OSalesTrendData, OSalesTrendDataHourly, OSelect } from '../../../../service/service';
declare var moment: any;
declare var $: any;

@Component({
  selector: "tusalesummary",
  templateUrl: "./tusalesummary.component.html",
  styles: [
    `
      agm-map {
        height: 300px;
      }
    `
  ]
})
export class TUSaleSummaryComponent implements OnInit {

  lastdaytext = "LAST DAY";
  lastweektext = "LAST WEEK";
  lastweekCustom = "LAST WEEK";
  lastmonthtext = "LAST MONTH";

  Types: any = {
    year: 'year',
    month: 'month',
    week: 'week',
    day: 'day',
    hour: 'hour'
  }

  public showGraph: boolean = true;
  public lineChartData: ChartDataSets[] = [
    { data: [87, 110, 22, 80, 77, 100, 150], type: 'bar', label: 'Series B', hidden: false },
    { data: [54, 110, 98, 44, 23, 64, 120], type: 'line', label: 'Series A', hidden: false },
  ];
  public lineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    },
    {
      backgroundColor: 'rgba(1, 104, 250, 00)',
    },
    {
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ]
  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
  }

  //#endregion

  ngOnInit() {
    this._HelperService.FullContainer = false;
    Feather.replace();

    this.initializeDatePicker('weeklydate', this._HelperService.AppConfig.DatePickerTypes.week, 'WeekSalesTrend_dropdown');

    //start time and end time for overview
    this.TodayStartTime = moment().startOf('day');
    this.TodayEndTime = moment().endOf('day');
    this.TUTr_Filter_Stores_Load();
    this.barChartLabels = this._HelperService.CalculateIntermediateDate(moment(this.TodayStartTime), moment(this.TodayEndTime));
    this.InitializeDates();

    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];

      this.pDataSales.AccountId = this._HelperService.AppConfig.ActiveMerchantReferenceId;
      this.pDataSales.AccountKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;

      this.pData.AccountId = this._HelperService.AppConfig.ActiveMerchantReferenceId;
      this.pData.AccountKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;

      this.LoadData();

    });

  }

  LoadData() {
    this.GetSalesOverview();
    this.GetWeekelyData();
    this._WeeklySalesReportGetActualData();
    this._DailySalesReportGetActualData();
    this._WeeklyBarSalesReportGetActualData();
  }

  InitializeDates(): void {
    //#region Daily Dates 

    this._Daily.ActualStartDate = moment().startOf('day');
    this._Daily.ActualEndDate = moment().endOf('day');

    //#endregion
    //#region Weekly Dates 
    this._Weekly.ActualStartDate = moment().startOf('week').startOf('day');
    this._Weekly.ActualEndDate = moment().endOf('day');

    this._Weekly.CompareStartDate = moment().subtract(1, 'week').startOf('week').startOf('day');
    this._Weekly.CompareEndDate = moment().subtract(1, 'week').endOf('week').endOf('day');
    //#endregion
  }

  //#region DateChangeHandler
  DateChanged(event: any, Type: any): void {

    var ev: any = cloneDeep(event);
    //#region Daily 

    this._Daily.ActualStartDate = moment(ev.start).startOf("day");
    this._Daily.ActualEndDate = moment(ev.end).endOf("day");

    this._Daily.CompareStartDate = moment(ev.start).subtract(1, 'day').startOf("day");
    this._Daily.CompareEndDate = moment(ev.end).subtract(1, 'day').endOf("day");

    //#endregion
    //#region Week 

    this._Weekly.ActualStartDate = moment(ev.start).startOf('week').startOf('day');
    this._Weekly.ActualEndDate = moment(ev.end).endOf('week').endOf('day');

    this._Weekly.CompareStartDate = moment(ev.start).subtract(1, 'week').startOf('week').startOf('day');
    this._Weekly.CompareEndDate = moment(ev.end).subtract(1, 'week').endOf('week').endOf('day');

    //#endregion

    //#region WeekBar 

    this._WeeklyBar.ActualStartDate = moment(ev.start).startOf('week').startOf('day');
    this._WeeklyBar.ActualEndDate = moment(ev.end).endOf('week').endOf('day');

    this._WeeklyBar.CompareStartDate = moment(ev.start).subtract(1, 'week').startOf('week').startOf('day');
    this._WeeklyBar.CompareEndDate = moment(ev.end).subtract(1, 'day').endOf('week').endOf('day');

    //#endregion

    this.TodayStartTime = moment(ev.start).startOf('day');
    this.TodayEndTime = moment(ev.end).endOf('day');

    this.GetSalesOverview();

    this._WeeklySalesReportGetActualData();
    this._DailySalesReportGetActualData();

    this._WeeklyBarSalesReportGetActualData();
  }

  //#endregion

  //#region Filters 

  //#region Store 
  public TUTr_Filter_Store_Selected: any;
  public ToggleStoreSelect: boolean = false;
  public TUTr_Filter_Store_Option: Select2Options;
  TUTr_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Store
        }
      ]
    };

    var OwnerKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;
    if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
      OwnerKey = this._HelperService.AppConfig.ActiveMerchantReferenceId;
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, OwnerKey, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Store_Option = {
      placeholder: 'Select Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Stores_Change(event: any) {
    if (event.value == this.TUTr_Filter_Store_Selected) {
      this.pData.StoreReferenceId = 0;
      this.pData.StoreReferenceKey = null;

      this.pDataSales.StoreReferenceId = 0;
      this.pDataSales.StoreReferenceKey = null;

      this.TUTr_Filter_Store_Selected = 0;
    }
    else if (event.value != this.TUTr_Filter_Store_Selected) {
      this.pData.StoreReferenceId = event.data[0].ReferenceId;
      this.pData.StoreReferenceKey = event.data[0].ReferenceKey;

      this.pDataSales.StoreReferenceId = event.data[0].ReferenceId;
      this.pDataSales.StoreReferenceKey = event.data[0].ReferenceKey;

      this.TUTr_Filter_Store_Selected = event.value;
    }
    this.LoadData();
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 500);
  }
  //#endregion
  //#endregion

  //#region AccountOverview 

  public TodayStartTime = null;
  public TodayEndTime = null;
  public _WeeklySales: any = {};

  public _AccountOverview: OAccountOverview =
    {
      ActiveMerchants: 0,
      ActiveMerchantsDiff: 0,
      ActiveTerminals: 0,
      ActiveTerminalsDiff: 0,
      CardRewardPurchaseAmount: 0,
      CardRewardPurchaseAmountDiff: 0,
      CashRewardPurchaseAmount: 0,
      CashRewardPurchaseAmountDiff: 0,
      Merchants: 0,
      PurchaseAmount: 0,
      PurchaseAmountDiff: 0,
      Terminals: 0,
      Transactions: 0,
      TransactionsDiff: 0,
      UnusedTerminals: 0,
      IdleTerminals: 0,
      DeadTerminals: 0,
      TerminalStatus: {},
      TotalTerminals: 0,
      TotalSale: 0,
      TotalTransactions: 0,
      AverageTransactionAmount: 0,
      ActiveTerminalsPerc: 0,
      IdleTerminalsPerc: 0,
      DeadTerminalsPerc: 0,
      UnusedTerminalsPerc: 0,
      Active: 0,
      Total: 0,
      Idle: 0,
      Dead: 0,
      Inactive: 0,
      CardInvoiceAmount: 0,
      CardTransaction: 0,
      CashInvoiceAmount: 0,
      CashTransaction: 0,
      InvoiceAmount: 0

    }

  public _SalesSummary: any = {
    CardInvoiceAmount: 0,
    CardTransaction: 0,
    CashInvoiceAmount: 0,
    CashTransaction: 0,
    InvoiceAmount: 0,
    Transactions: 0
  }

  public _SalesSummaryReset(): void {
    this._SalesSummary = {
      CardInvoiceAmount: 0,
      CardTransaction: 0,
      CashInvoiceAmount: 0,
      CashTransaction: 0,
      InvoiceAmount: 0,
      Transactions: 0
    }
  }
  //#endregion
  //#region BarChartConfig 
  public Options: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    layout: {
      padding: {
        top: 16,
      },
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return Math.round(value);
          } else {
            return Math.round(value);
          }
        }
      }
    }
  }
  public barChartLabels = [];
  public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  // public barChartColors = [{ backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [], label: 'Mon' },
    { data: [], label: 'Tue' },
    { data: [], label: 'Wed' },
    { data: [], label: 'Thu' },
    { data: [], label: 'Fri' },
    { data: [], label: 'Sat' },
    { data: [], label: 'Sun' },
  ];

  //#endregion

  //#region last7day 

  public _LastSevenDaysData: any = {};

  GetWeekelyData() {
    this.barChartData = [
      { data: [], label: 'Remote' },
      { data: [], label: 'Remote' },
      { data: [], label: 'Visit' },
      { data: [], label: 'Visit' },
    ];

    this.barChartColors = [
      { backgroundColor: [] },
      { backgroundColor: [] },
      { backgroundColor: [] },
      { backgroundColor: [] }
    ];

    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getsaleshistory',
      StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
      EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._LastSevenDaysData = _Response.Result;
          for (let i = 0; i < this._LastSevenDaysData.length; i++) {
            if (this._LastSevenDaysData[i].Data) {
              this.barChartData[0].data.push(this._LastSevenDaysData[i].Data.Active);
            }
          }
          if (this.components) {
            this.components.forEach(a => {
              try {
                if (a.chart) a.chart.update();
              } catch (error) {
              }
            });
          }

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  private pData = {
    Task: 'getsalesoverview',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
    AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    AmountDistribution: true
  };

  GetSalesOverview() {

    this._SalesSummaryReset();
    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this._SalesSummary = _Response.Result as any;
          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  //#endregion
  //#region Terminals 

  //#endregion

  Toogle_Dataset_Visibility(dataset_label: string): void {
    for (let index = 0; index < this.components["last"].datasets.length; index++) {
      const element = this.components["last"].datasets[index];
      if (element.label == dataset_label) {
        this.showGraph = false;
        this._ChangeDetectorRef.detectChanges();

        element.hidden = !element.hidden;

        this.showGraph = true;
        this._ChangeDetectorRef.detectChanges();
        break;
      }
    }

  }

  //#region Daily Sales Report 

  public DailylineChartData: ChartDataSets[] = [
    { data: [], label: 'Today' }
  ];
  public DailylineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];


  public DailylineChartLabels = ['00:00', 'O1:00', '02:00', 'O3:00', '04:00', 'O5:00', '06:00', 'O7:00', '08:00', '09:00', '10:00', '11:00',
    '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
  showDailyChart = true;

  public _Daily: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,
    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,
    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }

  public _DailySalesReportGetActualData(): void {

    this.DailylineChartData[0].data = [];
    this.GetSalesReportDaily(this._Daily.ActualStartDate, this._Daily.ActualEndDate, this._Daily.ActualData, this.Types.hour, 'actual');

  }
  //#endregion

  //#region Weekly Sales Report 

  public WeeklylineChartData: ChartDataSets[] = [
    { data: [], label: 'Current Week', type: 'line' },
    { data: [], borderDash: [10, 5], label: 'Compared Week', type: 'line' },
    { data: [], label: 'Current Week', type: 'bar' },
    { data: [], label: 'Compared Week', type: 'bar' }
  ];
  public WeeklylineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ];
  public WeeklylineChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
  showWeeklyChart = true;

  public _Weekly: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }

  WeelyDateChanged(event: any, Type: any): void {

    var ev: any = cloneDeep(event);
    this._Weekly.CompareStartDate = moment(ev.start).startOf("week").startOf('day');
    this._Weekly.CompareEndDate = moment(ev.end).endOf("week").endOf('day');

    this.lastweektext = this._HelperService.GetDateSByFormat(this._Weekly.CompareStartDate, 'DD MMM YY')
      + "-" + this._HelperService.GetDateSByFormat(this._Weekly.CompareEndDate, 'DD MMM YY');
    this.WeeklylineChartData[1].label = this._HelperService.GetDateSByFormat(this._Weekly.CompareStartDate, 'DD MMM YY');
    this.lastweekCustom = this._HelperService.GetDateSByFormat(this._Weekly.CompareStartDate, 'DD MMM YYYY')
    this.lastweekCustom = 'Custom Week';

    this.hideWeeklyPicker = true;
    this._WeeklySalesReportGetActualData();
  }

  public _WeeklySalesReportReset(): void {


  }

  public _WeeklySalesReportGetActualData(): void {

    this.WeeklylineChartData[0].data = [];
    this.WeeklylineChartData[1].data = [];
    this.WeeklylineChartData[2].data = [];
    this.WeeklylineChartData[3].data = [];

    this.GetSalesReportWeekly(this._Weekly.ActualStartDate, this._Weekly.ActualEndDate, this._Weekly.ActualData, this.Types.day, 'actual');
    this.GetSalesReportWeekly(this._Weekly.CompareStartDate, this._Weekly.CompareEndDate, this._Weekly.CompareData, this.Types.day, 'compare');

  }
  //#endregion

  //#region Weekly1 Sales Report 

  public WeeklyBarChartData: ChartDataSets[] = [
    { data: [], label: 'Week', type: 'bar' }
  ];
  public Weekly1BarChartColors: Color[] =
    [
      {
        borderColor: '#0168fa',
        backgroundColor: [],
      }
    ]
    ;
  public WeeklyBarChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
  showWeeklyBarChart = true;

  public _WeeklyBar: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }


  public _WeeklyBarSalesReportGetActualData(): void {

    this.WeeklyBarChartData[0].data = [];
    this.GetSalesReportWeekly(this._Weekly.ActualStartDate, this._Weekly.ActualEndDate, this._Weekly.ActualData, 'week', 'actual');

  }
  //#endregion

  //#region Sales History General Method 

  private pDataSales = {
    Task: 'getsaleshistory',
    StartDate: null,
    EndDate: null,
    AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
    AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };


  GetSalesReport(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pDataSales.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pDataSales.EndDate = this._HelperService.DateInUTC(EndDateTime);
    this.pDataSales.Type = Type;

    if (Type == this.Types.day) {
      this.pDataSales.Type = 'month';
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.pDataSales);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OSalesTrendData[];

          var TempArray = [];
          var TempColorArray = ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'];
          var SalesAmount = 0;

          var Heigest = Data[0];
          var HeigestIndex = 0;
          var Lowest = Data[0];
          var LowestIndex = 0;

          for (let index = 0; index < Data.length; index++) {
            const element: OSalesTrendData = Data[index];
            TempArray.push(element.TotalInvoiceAmount);

            if (Heigest.TotalInvoiceAmount < element.TotalInvoiceAmount) {
              Heigest = element;
              HeigestIndex = index;
            }

            if (Lowest.TotalInvoiceAmount > element.TotalInvoiceAmount) {
              Lowest = element;
              LowestIndex = index;
            }

            SalesAmount = SalesAmount + element.TotalInvoiceAmount;
          }
          if (Type == this._HelperService.AppConfig.GraphTypes.hour) {
            this.showDailyChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.DailylineChartData[0].data = TempArray;
              this._Daily.ActualSalesAmount = SalesAmount;
            } else if (LineType == 'compare') {
              this.DailylineChartData[1].data = TempArray;
              this._Daily.CompareSalesAmount = SalesAmount;
            }

            this.showDailyChart = true;
            this._ChangeDetectorRef.detectChanges();
          } else if (Type == this._HelperService.AppConfig.GraphTypes.day) {
            this.showWeeklyChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.WeeklylineChartData[0].data = TempArray;
              this._Weekly.ActualSalesAmount = SalesAmount;
              this.WeeklylineChartData[2].data = TempArray;
            } else if (LineType == 'compare') {
              this.WeeklylineChartData[1].data = TempArray;
              this.WeeklylineChartData[3].data = TempArray;
              this._Weekly.CompareSalesAmount = SalesAmount;
            }

            this.showWeeklyChart = true;
            this._ChangeDetectorRef.detectChanges();
          } else if (Type == 'week') {

            this._WeeklyBar.HeigestSales = Heigest;
            this._WeeklyBar.LowestSales = Lowest;

            this.showWeeklyBarChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.WeeklyBarChartData[0].data = TempArray;
              TempColorArray[HeigestIndex] = '#10B759';
              TempColorArray[LowestIndex] = '#F10875';
              this._WeeklyBar.ActualSalesAmount = SalesAmount;
            } else if (LineType == 'compare') {
              this.WeeklyBarChartData[1].data = TempArray;
              this._WeeklyBar.CompareSalesAmount = SalesAmount;
            }

            this.Weekly1BarChartColors[0].backgroundColor = TempColorArray;

            this.showWeeklyBarChart = true;
            this._ChangeDetectorRef.detectChanges();
          }
          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  GetSalesReportDaily(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pDataSales.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pDataSales.EndDate = this._HelperService.DateInUTC(EndDateTime);


    this.pDataSales.Type = this.Types.hour;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.pDataSales);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OSalesTrendData[];

          var TempArray = [];
          var SalesAmount = 0;
          var Heigest: OSalesTrendDataHourly = {
            Hour: null,
            HourAmPm: '0:00 AM',
            HourAmPmNext: '1:00 AM',
            TotalTransaction: 0.0,
            TotalInvoiceAmount: 0.0
          };

          var Lowest: OSalesTrendDataHourly = {
            Hour: null,
            HourAmPm: '0:00 AM',
            HourAmPmNext: '1:00 AM',
            TotalTransaction: 0.0,
            TotalInvoiceAmount: 0.0
          };


          var DataHourly = _Response.Result as OSalesTrendDataHourly[];
          for (let index = 0; index < 24; index++) {
            var RData: OSalesTrendDataHourly = DataHourly.find(x => x.Hour == index);
            if (RData != undefined && RData != null) {

              TempArray.push(RData.TotalCustomer);

              const element: OSalesTrendDataHourly = RData;
              if (Heigest.TotalInvoiceAmount <= element.TotalInvoiceAmount) {
                Heigest = element;

                //#region current 

                var dd = " AM";
                var h = index;
                if (h >= 12) {
                  h = index - 12;
                  dd = " PM";
                }
                if (h == 0) {
                  h = 12;
                }
                var Hour = h + ":00" + dd;
                Heigest.HourAmPm = Hour;

                //#endregion

                //#region next 

                var dd = " AM";
                var h = index + 1;
                if (h >= 12) {
                  h = index - 12;
                  dd = " PM";
                }
                if (h == 0) {
                  h = 12;
                }
                var Hour2 = h + ":00" + dd;
                Heigest.HourAmPmNext = Hour2;

                //#endregion
              }

              if (Lowest.TotalInvoiceAmount >= element.TotalInvoiceAmount) {
                Lowest = element;

                //#region current 

                var dd = " AM";
                var h = index;
                if (h >= 12) {
                  h = index - 12;
                  dd = " PM";
                }
                if (h == 0) {
                  h = 12;
                }
                var Hour = h + ':00' + dd;
                Lowest.HourAmPm = Hour;

                //#endregion


                //#region next 

                var dd = " AM";
                var h = index + 1;
                if (h >= 12) {
                  h = index - 12;
                  dd = " PM";
                }
                if (h == 0) {
                  h = 12;
                }
                var Hour2 = h + ':00' + dd;
                Lowest.HourAmPmNext = Hour2;

                //#endregion
              }
              SalesAmount = SalesAmount + element.TotalInvoiceAmount;
            }
            else {
              TempArray.push(0);
            }

          }



          if (Type == this._HelperService.AppConfig.GraphTypes.hour) {


            this.showDailyChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.DailylineChartData[0].data = TempArray;
              this._Daily.ActualSalesAmount = SalesAmount;
            } else if (LineType == 'compare') {
              this.DailylineChartData[1].data = TempArray;
              this._Daily.CompareSalesAmount = SalesAmount;
            }

            this.showDailyChart = true;
            this._ChangeDetectorRef.detectChanges();
          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  GetSalesReportWeekly(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pDataSales.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pDataSales.EndDate = this._HelperService.DateInUTC(EndDateTime);

    this.pDataSales.Type = Type;

    if (Type == this.Types.day) {
      this.pDataSales.Type = 'week';
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.pDataSales);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OSalesTrendData[];

          var TempArray = [];
          var SalesAmount = 0;

          var TempColorArray = ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'];

          var Heigest = Data[0];
          var HeigestIndex = 0;
          var Lowest = Data[0];
          var LowestIndex = 0;

          var DataDaily = _Response.Result as OSalesTrendData[];

          for (let index = 0; index < 7; index++) {

            var weekday = '';
            switch (index) {
              case 0: weekday = 'Sunday';
                break;
              case 1: weekday = 'Monday';

                break;
              case 2: weekday = 'Tuesday';

                break;
              case 3: weekday = 'Wednesday';

                break;
              case 4: weekday = 'Thursday';

                break;
              case 5: weekday = 'Friday';

                break;
              case 6: weekday = 'Saturday';
                break;

              default:
                break;
            }

            var RData: OSalesTrendData = DataDaily.find(x => x['WeekDay'] == weekday);
            if (RData != undefined && RData != null) {

              TempArray.push(RData.TotalInvoiceAmount);

              const element: OSalesTrendData = RData;

              SalesAmount = SalesAmount + element.TotalInvoiceAmount;


              if (Heigest.TotalInvoiceAmount < element.TotalInvoiceAmount) {
                Heigest = element;
                HeigestIndex = index;
              }

              if (Lowest.TotalInvoiceAmount > element.TotalInvoiceAmount && element.TotalInvoiceAmount != 0) {
                Lowest = element;
                LowestIndex = index;
              }

              SalesAmount = SalesAmount + element.TotalInvoiceAmount;
            }
            else {
              TempArray.push(0);
            }

          }

          if (Type == this._HelperService.AppConfig.GraphTypes.day) {
            this.showWeeklyChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.WeeklylineChartData[0].data = TempArray;
              this.WeeklylineChartData[2].data = TempArray;
              this._Weekly.ActualSalesAmount = SalesAmount;
            } else if (LineType == 'compare') {
              this.WeeklylineChartData[1].data = TempArray;
              this.WeeklylineChartData[3].data = TempArray;
              this._Weekly.CompareSalesAmount = SalesAmount;
            }

            this.showWeeklyChart = true;
            this._ChangeDetectorRef.detectChanges();
          } else if (Type == this._HelperService.AppConfig.GraphTypes.week) {
            this._WeeklyBar.HeigestSales = Heigest;
            this._WeeklyBar.LowestSales = Lowest;

            this.showWeeklyBarChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.WeeklyBarChartData[0].data = TempArray;
              TempColorArray[HeigestIndex] = '#10B759';
              TempColorArray[LowestIndex] = '#F10875';
              this._WeeklyBar.ActualSalesAmount = SalesAmount;
            } else if (LineType == 'compare') {
              this.WeeklyBarChartData[1].data = TempArray;
              this._WeeklyBar.CompareSalesAmount = SalesAmount;
            }

            this.Weekly1BarChartColors[0].backgroundColor = TempColorArray;

            this.showWeeklyBarChart = true;
            this._ChangeDetectorRef.detectChanges();
          }
          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }


  //#endregion


  CloseRowModal(index: number): void {
    switch (index) {
      // case 0: {
      //   var ev: any = {
      //     start: moment().subtract(1, 'week').startOf('day'),
      //     end: moment().subtract(1, 'week').endOf('day')
      //   };
      //   this.DailyDateChanged(ev, '');
      //   $("#TodaySalesTrend_dropdown").dropdown('toggle');
      // }

      //   break;
      case 1: {
        // var ev: any = {
        //   start: moment().subtract(1, 'month').startOf('week').startOf('day'),
        //   end: moment().subtract(1, 'month').endOf('week').endOf('day')
        // };
        var ev: any = {
          start: moment(this._Weekly.ActualStartDate).subtract(1, 'month').startOf('week').startOf('day'),
          end: moment(this._Weekly.ActualStartDate).subtract(1, 'month').endOf('week').endOf('day')
        };
        this.WeelyDateChanged(ev, '');
        $("#WeekSalesTrend_dropdown").dropdown('toggle');
      }

        break;
      // case 2: {
      //   var ev: any = {
      //     start: moment().subtract(1, 'year').startOf('month').startOf('day'),
      //     end: moment().subtract(1, 'year').endOf('month').endOf('day')
      //   };
      //   this.MonthlyDateChanged(ev, '');
      //   $("#MonthSalesTrend_dropdown").dropdown('toggle');
      // }

      //   break;

      default:
        break;
    }
  }

  initializeDatePicker(pickerId: string, type: string, dropdownId: string): void {
    var i = '#' + pickerId;
    var picker = "#" + dropdownId;


    if (type == this._HelperService.AppConfig.DatePickerTypes.month) {
      $(i).datepicker(
        {
          viewMode: "months",
          minViewMode: "months"
        }
      );
    } else {
      $(i).datepicker();
    }

    $(i).on('changeDate', () => {
      switch (type) {
        // case this._HelperService.AppConfig.DatePickerTypes.hour:
        //   {
        //     this.DailyDateChanged({
        //       start: $(i).datepicker("getDate"),
        //       end: $(i).datepicker("getDate")
        //     }, '');
        //     $(picker).dropdown('toggle');
        //   }
        //   break;

        case this._HelperService.AppConfig.DatePickerTypes.week:
          {
            this.WeelyDateChanged({
              start: $(i).datepicker("getDate"),
              end: $(i).datepicker("getDate")
            }, '');
            $(picker).dropdown('toggle');
          }
          break;

        // case this._HelperService.AppConfig.DatePickerTypes.month:
        //   {
        //     this.MonthlyDateChanged({
        //       start: $(i).datepicker("getDate"),
        //       end: $(i).datepicker("getDate")
        //     }, '');
        //     $(picker).dropdown('toggle');
        //   }
        //   break;

        default:
          break;
      }
    });
  }

  // hideDailyPicker: boolean = true;
  hideWeeklyPicker: boolean = true;
  // hideMonthlyPicker: boolean = true;

  ShowHideCalendar(type: string) {
    switch (type) {
      // case this._HelperService.AppConfig.DatePickerTypes.hour: {
      //   this.hideDailyPicker = !this.hideDailyPicker;
      // }

      //   break;
      case this._HelperService.AppConfig.DatePickerTypes.week: {
        this.hideWeeklyPicker = !this.hideWeeklyPicker;
      }

        break;
      // case this._HelperService.AppConfig.DatePickerTypes.month: {
      //   this.hideMonthlyPicker = !this.hideMonthlyPicker;
      // }

      //   break;

      default:
        break;
    }
  }
}
export class OAccountOverview {
  public TerminalStatus?: any;
  public TotalTerminals?: number;
  public TotalSale?: number;
  public AverageTransactionAmount?: number;
  public TotalTransactions?: number;
  public ActiveTerminalsPerc?: number;
  public IdleTerminalsPerc?: number;
  public DeadTerminalsPerc?: number;
  public UnusedTerminalsPerc?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public Idle?: number;
  public Total?: number;

  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;

  public CardInvoiceAmount: number;
  public CardTransaction: number;
  public CashInvoiceAmount: number;
  public CashTransaction: number;
  public InvoiceAmount: number;
}
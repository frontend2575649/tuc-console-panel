import { ChangeDetectorRef, Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import { InputFile, InputFileComponent } from 'ngx-input-file';
import { Observable } from 'rxjs';
import swal from "sweetalert2";
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from "../../../../service/service";
declare var $: any;


@Component({
  selector: "tu-tucampaigns",
  templateUrl: "./tucampaigns.component.html",
})
export class TuCampaignsComponent implements OnInit {
  public ResetFilterControls: boolean = true;
  @ViewChild("campaign")
  private InputFileComponent_campaign: InputFileComponent;

  CurrentImagesCount: number = 0;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;

  }

  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();

    this._HelperService.Icon_Crop_Clear();

    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];

      this.CampaignsList_Setup();
      this.CampaignsList_Filter_Owners_Load();
      this.InitColConfig();
      this.TUTr_Filter_Stores_Load();
      this.Form_AddUser_Load();
      this.InitImagePicker(this.InputFileComponent_campaign);

    });

    // this._HelperService.StopClickPropogation();
  }
  private InitImagePicker(InputFileComponent: InputFileComponent) {
    if (InputFileComponent != undefined) {
      this.CurrentImagesCount = 0;
      this._HelperService._InputFileComponent = InputFileComponent;
      InputFileComponent.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
  }
  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region merchantlist

  public CampaignsList_Config: OList;
  CampaignsList_Setup() {
    this.CampaignsList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      Title: "Available Campaigns",
      StatusType: "default",
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: "Merchant Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Contact No",
          SystemName: "ContactNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Terminals",
          SystemName: "Terminals",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Cashiers",
          SystemName: "Cashiers",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
          //   .PanelAcquirer.Merchant.Dashboard,
        }, {
          DisplayName: "Manager",
          SystemName: "ManagerName",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "text-right",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.CampaignsList_Config = this._DataHelperService.List_Initialize(
      this.CampaignsList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Campaign,
      this.CampaignsList_Config
    );

    this.CampaignsList_GetData();
  }
  CampaignsList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.CampaignsList_Config.Sort.SortOptions.length; index++) {
        const element = this.CampaignsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.CampaignsList_Config


    );

    this.CampaignsList_Config = this._DataHelperService.List_Operations(
      this.CampaignsList_Config,
      event,
      Type
    );

    if (
      (this.CampaignsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.CampaignsList_GetData();
    }

  }

  timeout = null;
  CampaignsList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.CampaignsList_Config.Sort.SortOptions.length; index++) {
          const element = this.CampaignsList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.CampaignsList_Config


      );

      this.CampaignsList_Config = this._DataHelperService.List_Operations(
        this.CampaignsList_Config,
        event,
        Type
      );

      if (
        (this.CampaignsList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.CampaignsList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);

  }

  CampaignsList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.CampaignsList_Config
    );
    this.CampaignsList_Config = TConfig;
  }
  CampaignsList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveCampaign,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Campaign,
      }
    );

    this._HelperService.AppConfig.ActiveMerchantReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveMerchantReferenceId = ReferenceData.ReferenceId;

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Campaign
        .Dashboard,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);


  }

  //#endregion

  //#region OwnerFilter

  public CampaignsList_Filter_Owners_Option: Select2Options;
  public CampaignsList_Filter_Owners_Selected = null;
  CampaignsList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant,
    //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     this._HelperService.AppConfig.AccountType.PosAccount
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.CampaignsList_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  CampaignsList_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.CampaignsList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.CampaignsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.CampaignsList_Filter_Owners_Selected,
        "="
      );
      this.CampaignsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.CampaignsList_Config.SearchBaseConditions
      );
      this.CampaignsList_Filter_Owners_Selected = null;
    } else if (event.value != this.CampaignsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.CampaignsList_Filter_Owners_Selected,
        "="
      );
      this.CampaignsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.CampaignsList_Config.SearchBaseConditions
      );
      this.CampaignsList_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.CampaignsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.CampaignsList_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.CampaignsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.CampaignsList_Config.SearchBaseConditions = [];
    this.CampaignsList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.CampaignsList_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.CampaignsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.CampaignsList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.CampaignsList_Config);

    this.SetOtherFilters();

    this.CampaignsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Campaign
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Campaign
        );
        this._FilterHelperService.SetMerchantConfig(this.CampaignsList_Config);
        this.CampaignsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.CampaignsList_GetData();

    if (ButtonType == 'Sort') {
      $("# CampaignsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("# CampaignsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.CampaignsList_Config);
    this.SetOtherFilters();

    this.CampaignsList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.CampaignsList_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  public ToggleStoreSelect: boolean = false;
  public TUTr_Filter_Store_Option: Select2Options;
  TUTr_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Store
        }
      ]
    };

    var OwnerKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;
    if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
      OwnerKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number,
      this._HelperService.AppConfig.ActiveMerchantReferenceId, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Store_Option = {
      placeholder: 'Select Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Stores_Change(event: any) {

  }


  Form_AddUser: FormGroup;
  Form_AddUser_Show() {
    // this._HelperService.Icon_Crop_Clear();
    this._HelperService.OpenModal("Form_AddUser_Content");
  }
  Form_AddUser_Close() {
    this._HelperService.CloseModal("Form_AddUser_Content");
  }
  Form_AddUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.SaveCampaign,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,

      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
      Description: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      RewardPercentage: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])],
      CampaignType: [null, Validators.required]

    });
  }
  Form_AddUser_Clear() {
    this.Form_AddUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddUser_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    var IconContent: any = undefined;

    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      IconContent = this._HelperService._Icon_Cropper_Data;
    }

    _FormValue.IconContent = IconContent;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.FlashSwalSuccess("New cashier has been added successfully",
            "Done! You have successfully created new Cashier");
          this.Form_AddUser_Clear();
          this.Form_AddUser_Close();
          if (_FormValue.OperationType == "close") {
            this.Form_AddUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }



}

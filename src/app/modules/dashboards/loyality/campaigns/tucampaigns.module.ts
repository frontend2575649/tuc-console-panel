import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { MainPipe } from '../../../../service/main-pipe.module'

import { TuCampaignsComponent } from "./tucampaigns.component";
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { ImageCropperModule } from 'ngx-image-cropper';
const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 1,
};
const routes: Routes = [{ path: "", component: TuCampaignsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUCampaignsRoutingModule {}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TUCampaignsRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    MainPipe,
    ImageCropperModule,
    InputFileModule.forRoot(config),
    InputFileModule

  ],
  declarations: [TuCampaignsComponent]
})
export class TUCampaignsModule {}

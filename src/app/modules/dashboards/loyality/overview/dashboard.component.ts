import { ChangeDetectorRef, Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ChartDataSets } from 'chart.js';
import * as Feather from "feather-icons";
import * as cloneDeep from 'lodash/cloneDeep';
import { BaseChartDirective, Color, Label } from 'ng2-charts';
import { Observable, Subscription } from 'rxjs';
import { DataHelperService, HelperService, OLoyalityHistory, OLoyalityHistoryData, OResponse, OSalesTrend, OSalesTrendData, OSelect } from '../../../../service/service';
declare var moment: any;


@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styles: ['']
})
export class TUDashboardComponent implements OnInit {
  Types: any = {
    year: 'year',
    month: 'month',
    week: 'week',
    day: 'day',
    hour: 'hour'
  }

  public showGraph: boolean = true;

  public lineChartData: ChartDataSets[] = [
    { data: [87, 10, 101, 110], type: 'line', label: 'Series A', hidden: false },
    { data: [122, 10, 101, 200], type: 'line', label: 'Series A' },
    { data: [87, 10, 101, 110], type: 'bar', label: 'Series A', hidden: false },
    { data: [87, 10, 101, 110], type: 'line', label: 'Series A', hidden: false },
  ];
  public lineSalesChartColors: Color[] = [
    {
      borderColor: '#10b759',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#EE9800',
      backgroundColor: 'rgba(255, 255, 255, 0)',
    }



  ]

  public lineVisitsChartColors: Color[] = [

    {
      backgroundColor: 'rgba(0, 204, 204, 1)',
    },
    {
      backgroundColor: 'rgba(241, 8, 117, 1)',
    }

  ]

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef

  ) {
    this._HelperService.ShowDateRange = true;
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;
  }
  // ngOnDestroy(): void {
  //     this._DateSubscription.unsubscribe();
  // }

  //#region DougnutConfigs 

  //#endregion

  public _DateSubscription: Subscription = null;

  ngOnInit() {
    Feather.replace();
    this.TUTr_Filter_Stores_Load();

    this.InitializeDates();

    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveMerchantReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];

      this.pData.AccountId = this._HelperService.AppConfig.ActiveMerchantReferenceId;
      this.pData.AccountKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;

      this.pLoyalityData.AccountId = this._HelperService.AppConfig.ActiveMerchantReferenceId;
      this.pLoyalityData.AccountKey = this._HelperService.AppConfig.ActiveMerchantReferenceKey;

      this.LoadData();
    });

  }


  LoadData() {
    this.GetLoyaltyOverviewLite();
    this._MonthlySalesReportGetActualData();
    this.GetLoyalityReport(moment().startOf('day'), moment().endOf('day'),
      this.LoyalityData);
  }

  RefreshData() {
    this.GetLoyaltyOverviewLite();
    this._MonthlySalesReportGetActualData();
    this.GetLoyalityReport(moment(this.CurrentDate.start).startOf('day'), moment(this.CurrentDate.end).endOf('day'), this.LoyalityData);
  }

  InitializeDates(): void {
    //#region Monthly Dates 

    this._Monthly.ActualStartDate = moment().startOf('day');
    this._Monthly.ActualEndDate = moment().endOf('day');

    // this._Monthly.CompareStartDate = moment().subtract(1, 'months').startOf('day');
    // this._Monthly.CompareEndDate = moment().subtract(1, 'months').endOf('day');



    //#endregion

  }

  CurrentDate: any;
  //#region DateChangeHandler
  DateChanged(event: any, Type: any): void {

    this.CurrentDate = cloneDeep(event);
    //#region Monthly 
    this._Monthly.ActualStartDate = moment(this.CurrentDate.start).startOf('day');
    this._Monthly.ActualEndDate = moment(this.CurrentDate.end).endOf('day');
    // this._Monthly.CompareStartDate = moment(ev.start).subtract(1, 'months').startOf('month').startOf('day');
    // this._Monthly.CompareEndDate = moment(ev.end).subtract(1, 'months').endOf('month').endOf('day');
    //#endregion
    this.GetLoyaltyOverviewLite();
    this._MonthlySalesReportGetActualData();
    this.GetLoyalityReport(moment(this.CurrentDate.start).startOf('day'), moment(this.CurrentDate.end).endOf('day'), this.LoyalityData);

  }
  //#endregion
  public BarChartOptions: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      },
    },
    emptyOverlay: {
      fillStyle: 'rgba(255,0,0,0.4)',
      fontColor: 'rgba(255,255,255,1.0)',
      fontStrokeWidth: 0,
      enabled: true
    }
  }
  public barChartLabels: Label[] = [];
  public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#F10875', '#F10875', '#F10875', '#F10875', '#F10875', '#F10875', '#F10875'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Active', hidden: false },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Idle', hidden: false },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Dead', hidden: false },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Inactive', hidden: false }
  ];

  TodayStartTime = null;
  TodayEndTime = null;
  GetLoyaltyOverviewLite() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getloyaltyoverview',
      StartDate: this._HelperService.DateInUTC(this._Monthly.ActualStartDate),
      EndDate: this._HelperService.DateInUTC(this._Monthly.ActualEndDate),
      AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverview = _Response.Result as OAccountOverview;
          this.components.forEach(a => {
            try {
              if (a.chart) a.chart.update();
            } catch (error) {

            }
          });

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public MonthlybarChartLabels = [];

  public MonthlylineChartData: ChartDataSets[] = [
    { data: [22, 44, 44, 77], label: 'Series A' },
    { data: [33, 55, 77, 44], borderDash: [10, 5], label: 'Series B' },
    { data: [33, 55, 77, 99], type: 'bar', label: 'Series C' },
    { data: [33, 55, 77, 88], type: 'bar', label: 'Series D' }

  ];

  public MonthlylineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    },
    {
      backgroundColor: 'red',
    },
    {
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ]

  //#region Monthly Sales Report 

  showMonthlyChart = true;

  public _Monthly: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }

  MonthlyDateChanged(event: any, Type: any): void {
    var ev: any = cloneDeep(event);
    this._Monthly.CompareStartDate = moment(ev.start).startOf("day");
    this._Monthly.CompareEndDate = moment(ev.end).endOf("day");

    //this.MonthlybarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Monthly.ActualStartDate), moment(this._Monthly.ActualEndDate).endOf('month').endOf('day'));

    this._MonthlySalesReportGetActualData();
  }

  public _MonthlySalesReportReset(): void {

  }

  public _MonthlySalesReportGetActualData(): void {

    this.MonthlylineChartData[0].data = [];
    this.MonthlylineChartData[1].data = [];
    this.MonthlylineChartData[2].data = [];
    this.MonthlylineChartData[3].data = [];


    this._Monthly.ActualData = this.GetSalesReport(this._Monthly.ActualStartDate, this._Monthly.ActualEndDate, this._Monthly.ActualData, this.Types.month, 'actual');
    // this._Monthly.CompareData = this.GetSalesReport(this._Monthly.CompareStartDate, this._Monthly.CompareEndDate, this._Monthly.CompareData, this.Types.month, 'compare');

  }
  //#endregion
  private pData = {
    Task: 'getsaleshistory',
    StartDate: null,
    EndDate: null,
    AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
    AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };

  GetSalesReport(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);
    this.pData.Type = Type;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OSalesTrendData[];

          var TempArray = [];
          var SalesAmount = 0;

          for (let index = 0; index < Data.length; index++) {
            const element: OSalesTrendData = Data[index];
            TempArray.push(element.TotalInvoiceAmount);

            SalesAmount = SalesAmount + element.TotalInvoiceAmount;
          }

          if (Type == this._HelperService.AppConfig.GraphTypes.month) {
            this.showMonthlyChart = false;
            this._ChangeDetectorRef.detectChanges();

            if (LineType == 'actual') {
              this.MonthlylineChartData[0].data = TempArray;
              this._Monthly.ActualSalesAmount = SalesAmount;
            } else if (LineType == 'compare') {
              this.MonthlylineChartData[1].data = TempArray;
              this._Monthly.CompareSalesAmount = SalesAmount;
            }

            this.showMonthlyChart = true;
            this._ChangeDetectorRef.detectChanges();
          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }


  public _AccountOverview: OAccountOverview =
    {

      RewardAmount: 0,
      RedeemAmount: 0,
      NewCustomers: 0,
      RepeatingCustomers: 0,
      TransactionInvoiceAmount: 0,
      VisitsByRepeatingCustomers: 0,
      Transaction: 0,
      RedeemTransaction: 0,
      RedeemInvoiceAmount: 0,
      TucPlusRewardAmountCommission: 0,
      TotalTucPlusRewardClaimTransactionAmount: 0,
      TucPlusRewardAmountUser: 0,
      TotalTucPlusRewardTransactionAmount: 0,
      TucPlusRewardClaimAmount: 0,
      TucRewardInvoiceAmount: 0,
      RepeatingCustomerInvoiceAmount: 0,
      NewCustomerInvoiceAmount: 0,

      RewardCommissionAmount: 0,
      RewardInvoiceAmount: 0,
      RewardTransaction: 0,
      TucPlusRewardAmount: 0,
      TucPlusRewardClaimInvoiceAmount: 0,
      TucPlusRewardClaimTransaction: 0,
      TucPlusRewardCommissionAmount: 0,
      TucPlusRewardInvoiceAmount: 0,
      TucPlusRewardTransaction: 0,
      TucRewardTransaction: 0,

    }
  Toogle_Dataset_Visibility(dataset_label: string): void {
    for (let index = 0; index < this.SalesChartData.length; index++) {
      const element = this.SalesChartData[index];

      if (element.label == dataset_label) {

        //#region Reload_UI 

        this.showSalesLoalityChart = false;
        this._ChangeDetectorRef.detectChanges();

        element.hidden = !element.hidden;

        this.showSalesLoalityChart = true;
        this._ChangeDetectorRef.detectChanges();

        //#endregion
        break;
      }
    }

    for (let index = 0; index < this.VisitsChartData.length; index++) {
      const element = this.VisitsChartData[index];

      if (element.label == dataset_label) {

        //#region Reload_UI 

        this.showVisitsLoalityChart = false;
        this._ChangeDetectorRef.detectChanges();

        element.hidden = !element.hidden;

        this.showVisitsLoalityChart = true;
        this._ChangeDetectorRef.detectChanges();

        //#endregion
        break;
      }
    }

  }

  public ToggleStoreSelect: boolean = false;
  public TUTr_Filter_Store_Selected: any;
  public TUTr_Filter_Store_Option: Select2Options;
  TUTr_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //   SystemName: "AccountTypeCode",
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   SearchCondition: "=",
        //   SearchValue: this._HelperService.AppConfig.AccountType.Store
        // }
      ]
    };

    var OwnerId = this._HelperService.AppConfig.ActiveMerchantReferenceId;
    if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
      OwnerId = this._HelperService.AppConfig.ActiveMerchantReferenceId;
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'MerchantReferenceId', this._HelperService.AppConfig.DataType.Number, OwnerId, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Store_Option = {
      placeholder: 'Select Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Stores_Change(event: any) {
    if (event.value == this.TUTr_Filter_Store_Selected) {
      this.pData.StoreReferenceId = 0;
      this.pData.StoreReferenceKey = null;

      this.pLoyalityData.StoreReferenceId = 0;
      this.pLoyalityData.StoreReferenceKey = null;

      this.TUTr_Filter_Store_Selected = 0;
    }
    else if (event.value != this.TUTr_Filter_Store_Selected) {
      this.pData.StoreReferenceId = event.data[0].ReferenceId;
      this.pData.StoreReferenceKey = event.data[0].ReferenceKey;

      this.pLoyalityData.StoreReferenceId = event.data[0].ReferenceId;
      this.pLoyalityData.StoreReferenceKey = event.data[0].ReferenceKey;


      this.TUTr_Filter_Store_Selected = event.value;
    }
    this.LoadData();
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 500);
  }

  DateTypeSelected(event): void {
  }

  //#region loyality History 

  //#region Sales Chart Data 
  public SalesChartData: ChartDataSets[] = [

    { data: [], label: 'Returning Sales', hidden: false },
    { data: [], borderDash: [10, 5], label: 'New Sales', hidden: false }

  ];
  public SalesChartColors: Color[] = [
    {
      borderColor: '#10b759',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#EE9800',
      backgroundColor: 'rgba(255, 255, 255, 0)',
    }
  ];
  public SalesChartLabels = [];
  //#endregion

  //#region Visits Chart Data 
  public VisitsChartData: ChartDataSets[] = [

    { data: [], type: 'bar', label: 'Returning Visits', hidden: false },
    { data: [], type: 'bar', label: 'New Visits', hidden: false }

  ];
  public VisitsChartColors: Color[] = [
    {
      backgroundColor: 'rgba(0, 204, 204, 1)',
    },
    {
      backgroundColor: 'rgba(0, 204, 204, .10)',
    },
  ];
  public VisitsChartLabels = [];
  //#endregion

  public _OLoyalityHistory: OLoyalityHistory = {
    NewCustomerInvoiceAmount_Sum: 0,
    NewCustomer_Sum: 0,
    RepeatingCustomerSaleAmount_Sum: 0,
    RepeatingCustomer_Sum: 0,
    TotalCustomer_Sum: 0,
    TotalInvoiceAmount_Sum: 0,
    RepeatingCustomerInvoiceAmount: 0
  };

  private pLoyalityData = {
    Task: 'getloyaltyvisithistory',
    StartDate: null,
    EndDate: null,
    AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
    AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };

  LoyalityData: OLoyalityHistoryData[];

  showVisitsLoalityChart: boolean = true;
  showSalesLoalityChart: boolean = true;

  GetLoyalityReport(StartDateTime, EndDateTime, Data: OLoyalityHistoryData[]) {

    this._OLoyalityHistory = {
      NewCustomerInvoiceAmount_Sum: 0,
      NewCustomer_Sum: 0,
      RepeatingCustomerSaleAmount_Sum: 0,
      RepeatingCustomer_Sum: 0,
      TotalCustomer_Sum: 0,
      TotalInvoiceAmount_Sum: 0,
      RepeatingCustomerInvoiceAmount: 0
    };

    this._HelperService.IsFormProcessing = true;

    this.pLoyalityData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pLoyalityData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.pLoyalityData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OLoyalityHistoryData[];

          var ReturningSales = [];
          var NewSales = [];

          var ReturningVisits = [];
          var NewVisits = [];

          var ChartLabels: any[] = [];



          if (Data.length > 0 && this.TitleIsHour(Data[0].Title)) {

            for (let index = 0; index < 24; index++) {

              //#region current 

              var dd = " AM";
              var h = index;
              if (h >= 12) {
                h = index - 12;
                dd = " PM";
              }
              if (h == 0) {
                h = 12;
              }
              var Hour = h + ":00" + dd;
              ChartLabels.push(Hour);

              //#endregion

              var RData: any = Data.find(x => x.Title == Hour);
              if (RData != undefined && RData != null) {
                const element: any = RData;

                //#region Calculate Totals 

                this._OLoyalityHistory.NewCustomer_Sum = this._OLoyalityHistory.NewCustomer_Sum + element.NewCustomer;
                this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum = this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum + element.NewCustomerInvoiceAmount;
                this._OLoyalityHistory.RepeatingCustomer_Sum = this._OLoyalityHistory.RepeatingCustomer_Sum + element.RepeatingCustomer;
                this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum = this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum + element.RepeatingCustomerInvoiceAmount;
                this._OLoyalityHistory.TotalCustomer_Sum = this._OLoyalityHistory.TotalCustomer_Sum + element.NewCustomer + element.RepeatingCustomer;
                this._OLoyalityHistory.TotalInvoiceAmount_Sum = this._OLoyalityHistory.TotalInvoiceAmount_Sum + element.NewCustomerInvoiceAmount + element.RepeatingCustomerInvoiceAmount;

                //#endregion

                ReturningSales.push(element.RepeatingCustomerInvoiceAmount);
                NewSales.push(element.NewCustomerInvoiceAmount);
                ReturningVisits.push(element.VisitsByRepeatingCustomers);
                NewVisits.push(element.NewCustomer);

              }
              else {
                ReturningSales.push(0.0);
                NewSales.push(0.0);
                ReturningVisits.push(0.0);
                NewVisits.push(0.0);

              }
            }

          } else {
            this.CalculateDateRangeType(cloneDeep(StartDateTime), cloneDeep(EndDateTime));

            if (this.SelectedDateRangeType == this._HelperService.AppConfig.GraphTypes.week) {

              for (let index = 0; index < this.Intermediate.length; index++) {
                const element = this.Intermediate[index];
                ChartLabels.push(element);
                var RData: any = Data.find(x => moment(x.Title, 'DD-MM-YYYY').format('DD MMM') == element);
                if (RData != undefined && RData != null) {
                  const element: any = RData;

                  //#region Calculate Totals 

                  this._OLoyalityHistory.NewCustomer_Sum = this._OLoyalityHistory.NewCustomer_Sum + element.NewCustomer;
                  this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum = this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum + element.NewCustomerInvoiceAmount;
                  this._OLoyalityHistory.RepeatingCustomer_Sum = this._OLoyalityHistory.RepeatingCustomer_Sum + element.RepeatingCustomer;
                  this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum = this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum + element.RepeatingCustomerInvoiceAmount;
                  this._OLoyalityHistory.TotalCustomer_Sum = this._OLoyalityHistory.TotalCustomer_Sum + element.NewCustomer + element.RepeatingCustomer;
                  this._OLoyalityHistory.TotalInvoiceAmount_Sum = this._OLoyalityHistory.TotalInvoiceAmount_Sum + element.NewCustomerInvoiceAmount + element.RepeatingCustomerInvoiceAmount;

                  //#endregion

                  ReturningSales.push(element.RepeatingCustomerInvoiceAmount);
                  NewSales.push(element.NewCustomerInvoiceAmount);
                  ReturningVisits.push(element.VisitsByRepeatingCustomers);
                  NewVisits.push(element.NewCustomer);

                }
                else {
                  ReturningSales.push(0.0);
                  NewSales.push(0.0);
                  ReturningVisits.push(0.0);
                  NewVisits.push(0.0);

                }
              }

            } else if (this.SelectedDateRangeType == this._HelperService.AppConfig.GraphTypes.month) {

              for (let index = 0; index < this.Intermediate.length; index++) {
                const element = this.Intermediate[index];
                ChartLabels.push(element);
                var RData: any = Data.find(x => moment(x.Title, 'DD-MM-YYYY').format('DD MMM') == element);
                if (RData != undefined && RData != null) {
                  const element: any = RData;

                  //#region Calculate Totals 

                  this._OLoyalityHistory.NewCustomer_Sum = this._OLoyalityHistory.NewCustomer_Sum + element.NewCustomer;
                  this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum = this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum + element.NewCustomerInvoiceAmount;
                  this._OLoyalityHistory.RepeatingCustomer_Sum = this._OLoyalityHistory.RepeatingCustomer_Sum + element.RepeatingCustomer;
                  this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum = this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum + element.RepeatingCustomerInvoiceAmount;
                  this._OLoyalityHistory.TotalCustomer_Sum = this._OLoyalityHistory.TotalCustomer_Sum + element.NewCustomer + element.RepeatingCustomer;
                  this._OLoyalityHistory.TotalInvoiceAmount_Sum = this._OLoyalityHistory.TotalInvoiceAmount_Sum + element.NewCustomerInvoiceAmount + element.RepeatingCustomerInvoiceAmount;

                  //#endregion

                  ReturningSales.push(element.RepeatingCustomerInvoiceAmount);
                  NewSales.push(element.NewCustomerInvoiceAmount);
                  ReturningVisits.push(element.VisitsByRepeatingCustomers);
                  NewVisits.push(element.NewCustomer);

                }
                else {
                  ReturningSales.push(0.0);
                  NewSales.push(0.0);
                  ReturningVisits.push(0.0);
                  NewVisits.push(0.0);

                }
              }

            } else if (this.SelectedDateRangeType == this._HelperService.AppConfig.GraphTypes.year) {

              for (let index = 0; index < this.Intermediate.length; index++) {
                const element = this.Intermediate[index];
              }

            }


          }

          this.MonthlybarChartLabels = ChartLabels;

          this.showVisitsLoalityChart = false;
          this.showSalesLoalityChart = false;
          this._ChangeDetectorRef.detectChanges();

          this.SalesChartData[0].data = NewSales;
          this.SalesChartData[1].data = ReturningSales;

          this.VisitsChartData[0].data = ReturningVisits;
          this.VisitsChartData[1].data = NewVisits;

          this.showVisitsLoalityChart = true;
          this.showSalesLoalityChart = true;
          this._ChangeDetectorRef.detectChanges();

          this._HelperService.IsFormProcessing = false;

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  TitleIsHour(title: string): boolean {
    return (title.includes('AM') || title.includes('PM'));
  }

  SelectedDateRangeType: string = this._HelperService.AppConfig.GraphTypes.week;
  Intermediate: any[];
  CalculateDateRangeType(start, end): void {

    this.Intermediate = this._HelperService.CalculateIntermediateDate(start, end);

    if (this.Intermediate.length <= 7) {
      this.SelectedDateRangeType = this._HelperService.AppConfig.GraphTypes.week;
    } else if (this.Intermediate.length > 7 && this.Intermediate.length < 32) {
      this.SelectedDateRangeType = this._HelperService.AppConfig.GraphTypes.month;
    } else if (this.Intermediate.length > 32) {
      this.SelectedDateRangeType = this._HelperService.AppConfig.GraphTypes.year;
    }

  }

  //#endregion

}

export class OAccountOverview {
  public NewCustomerInvoiceAmount: any;
  public NewCustomers: any;
  public RedeemAmount: any;
  public RedeemInvoiceAmount: any;
  public RedeemTransaction: any;
  public RepeatingCustomerInvoiceAmount: any;
  public RepeatingCustomers: any;
  public RewardAmount: any;
  public RewardCommissionAmount: any;
  public RewardInvoiceAmount: any;
  public RewardTransaction: any;
  public Transaction: any;
  public TransactionInvoiceAmount: any;
  public TucPlusRewardAmount: any;
  public TucPlusRewardClaimAmount: any;
  public TucPlusRewardClaimInvoiceAmount: any;
  public TucPlusRewardClaimTransaction: any;
  public TucPlusRewardCommissionAmount: any;
  public TucPlusRewardInvoiceAmount: any;
  public TucPlusRewardTransaction: any;
  public TucRewardTransaction: any;
  public VisitsByRepeatingCustomers: any;


  public TucPlusRewardAmountCommission: any;
  public TotalTucPlusRewardClaimTransactionAmount: any;
  public TucPlusRewardAmountUser: any;
  public TotalTucPlusRewardTransactionAmount: any;
  public TucRewardInvoiceAmount: any;

}
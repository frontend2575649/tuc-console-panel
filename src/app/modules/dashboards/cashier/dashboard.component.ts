import { ChangeDetectorRef, Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BaseChartDirective } from 'ng2-charts';
import { Observable, Subscription } from 'rxjs';
import { DataHelperService, HelperService, OResponse } from '../../../service/service';
declare var moment: any;

@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.component.html',
    styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUDashboardComponent implements OnInit {

    @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef

    ) {
        this._HelperService.ShowDateRange = true;
    }
    // ngOnDestroy(): void {
    //     this._DateSubscription.unsubscribe();
    // }

    //#region DougnutConfigs 
    public pieChartOptions = {
        maintainAspectRatio: false,
        responsive: true,
        legend: {
            display: false,
        },
        animation: {
            animateScale: true,
            animateRotate: true
        },

    };
    public doughnutcard = [{ backgroundColor: ['#FFC20A', '#10b759'] }];
    public datapieCard = {
        labels: ['Reward Transaction', 'Non-Reward Transaction'],
        labelColor: 'white',
        datasets: [{
            data: [0, 0],
            labelColor: '#10b759',
        }],
    };
    public doughnuttype = [{ backgroundColor: ['#00cccc', '#f10075'] }];
    public datapieType = {
        labels: ['Cash', 'Card'],
        datasets: [{
            data: [0, 0],
            backgroundColor: ['#66a4fb', '#4cebb5']
        }]
    };
    //#endregion
    public _DateSubscription: Subscription = null;
    ngOnInit() {
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.CashierDetails.next(params);
            this._HelperService.AppConfig.ActiveCashierReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveCashierReferenceId = params["referenceid"];
            this._HelperService.AppConfig.ActiveMerchantReferenceKey = params["merchantkey"];
            this._HelperService.AppConfig.ActiveMerchantReferenceId = params["merchantid"];
            if (this._HelperService.AppConfig.ActiveCashierReferenceKey == null) {
                this._Router.navigate([
                    this._HelperService.AppConfig.Pages.System.NotFound
                ]);
            } else {
                this._HelperService.ResetDateRange();
                this.GetAccountOverviewLite();
                this.GetPosUsedList();
            }
        });

        this._DateSubscription = this._HelperService.RangeAltered.subscribe(value => {
            this.GetAccountOverviewLite();
            this.GetPosUsedList()
        });
    }
    //#region AccountOverview 
    public _AccountOverview: OAccountOverview =
        {
            Customers: 0,
            Transactions: 0,
            InvoiceAmount: 0.0,
            CardTransaction: 0,
            CardInvoiceAmount: 0.0,
            CashTransaction: 0,
            CashInvoiceAmount: 0.0,
            CardTransPerTerm: 0.0,
            CardTransactionsAmount: 0.0,
            CashTransPerTerm: 0.0,
            OtherCardTotal: 0.0,
            OwnerCardTotal: 0.0

        }
    public _LoyalityOverview: OLoyalityOverview =
        {
            NewCustomers: 0,
            RepeatingCustomers: 0,
            VisitsByRepeatingCustomers: 0,
            NewCustomerInvoiceAmount: 0.0,
            RepeatingCustomerInvoiceAmount: 0.0,
            Transaction: 0,
            TransactionInvoiceAmount: 0.0,
            RewardTransaction: 0,
            RewardAmount: 0.0,
            TucRewardTransaction: 0,
            TucPlusRewardTransaction: 0,
            TucPlusRewardClaimTransaction: 0,
            RedeemTransaction: 0,

            NonRewardTransaction: 0.0,

            RewardTransactionPerc: 0.0,
            NonRewardTransactionPerc: 0.0

        }
    public _TerminalList: OTerminalList[] =
        [

        ]

    GetAccountOverviewLite() {

        this._HelperService.IsFormProcessing = true;
        var Data = {
            Task: 'getsalesoverview',
            StartDate: this._HelperService.DateInUTC(this._HelperService.DateRangeStart), // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndDate: this._HelperService.DateInUTC(this._HelperService.DateRangeEnd), // moment().add(2, 'days'),
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            CashierReferenceId: this._HelperService.AppConfig.ActiveCashierReferenceId,
            CashierReferenceKey: this._HelperService.AppConfig.ActiveCashierReferenceKey,
            AmountDistribution: true

        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
        _OResponse.subscribe(
            _Response => {

                var OwnerCardTotal: any = {
                    "Name": "OwnerAllCards",
                    "Transactions": 0,
                    "TransactionsPerc": 0.0,
                    "Amount": 0.0
                }

                var OtherCardTotal: any = {
                    "Name": "OtherAllCards",
                    "Transactions": 0,
                    "TransactionsPerc": 0.0,
                    "Amount": 0.0
                }

                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._AccountOverview = _Response.Result as OAccountOverview;

                    // Open To See Dummy Data

                    // this._AccountOverview = {
                    //     Customers: 0,
                    //     Transactions: 500,
                    //     InvoiceAmount: 7020.0,
                    //     CardTransaction: 200.0,
                    //     CardInvoiceAmount: 5010.0,
                    //     CashTransaction: 300.0,
                    //     CashInvoiceAmount: 2010.0,

                    //     CardTransPerTerm: 0.0,
                    //     CardTransactionsAmount: 0.0,
                    //     CashTransPerTerm: 0.0,
                    //     OtherCardTotal: 0.0,
                    //     OwnerCardTotal: 0.0

                    // };

                    this.datapieType.datasets[0].data[1] = this._AccountOverview.CardTransaction;
                    this.datapieType.datasets[0].data[0] = this._AccountOverview.CashTransaction;

                    //#region PercentCal 
                    var TotalTransactions = this._AccountOverview.CardTransaction + this._AccountOverview.CashTransaction;
                    if (TotalTransactions > 0) {
                        this._AccountOverview.CashTransPerTerm = (this._AccountOverview.CashTransaction / TotalTransactions) * 100;
                        this._AccountOverview.CardTransPerTerm = (this._AccountOverview.CardTransaction / TotalTransactions) * 100;
                    }
                    //#endregion

                    //#region OwnerAndOtherCal 

                    //#region OwnerAndOtherTotals 



                    if (this._AccountOverview['CardTypeSale']) {
                        for (let index = 0; index < this._AccountOverview['CardTypeSale'].length; index++) {
                            const element = this._AccountOverview['CardTypeSale'][index];
                            var _TempVal = this._HelperService.DivideTwoNumbers(100, this._AccountOverview['TotalTransactions']);
                            element['TransactionPer'] = element['Transactions'] * _TempVal;
                        }
                        for (let index = 0; index < this._AccountOverview['OwnerCardTypeSale'].length; index++) {
                            const element = this._AccountOverview['OwnerCardTypeSale'][index];
                            OwnerCardTotal.Transactions += element.Transactions;
                            OwnerCardTotal.Amount += element.Amount;
                        }
                    }

                    OtherCardTotal.Transactions = this._AccountOverview['OwnerCardTransactions'] - OwnerCardTotal.Transactions;
                    OtherCardTotal.Amount = this._AccountOverview['OwnerCardTransactionsAmount'] - OwnerCardTotal.Amount;

                    //#endregion

                    //#region OwnerAndOtherPerc 

                    OwnerCardTotal.TransactionsPerc = (this._HelperService.DivideTwoNumbers(OwnerCardTotal.Transactions, this._AccountOverview['OwnerCardTransactions']) * 100);
                    OtherCardTotal.TransactionsPerc = (this._HelperService.DivideTwoNumbers(OtherCardTotal.Transactions, this._AccountOverview['OwnerCardTransactions']) * 100);

                    //#endregion

                    //#endregion


                    this._AccountOverview['OwnerCardTotal'] = OwnerCardTotal;
                    this._AccountOverview['OtherCardTotal'] = OtherCardTotal;


                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }

                //#region OtherCardComputation 

                var otherCards: any = {
                    Name: "Other",
                    Transactions: 0,
                    Amount: 0.0,
                    TransactionPer: 0
                }

                if (this._AccountOverview['CardTypeSale']) {
                    for (let index = 0; index < this._AccountOverview['CardTypeSale'].length; index++) {
                        let element = this._AccountOverview['CardTypeSale'][index];
                        otherCards.Transactions += element['Transactions'];
                        otherCards.Amount += element['Amount'];
                    }

                    otherCards.Transactions = this._AccountOverview["TotalTransactions"] - otherCards['Transactions'];
                    otherCards.Amount = this._AccountOverview["TotalSale"] - otherCards['Amount'];
                    otherCards.TransactionPer = (this._HelperService.DivideTwoNumbers(otherCards.Transactions, this._AccountOverview['TotalTransactions']) * 100)
                    this._AccountOverview['OtherCards'] = otherCards;
                }

                //#endregion

                this.components.forEach(a => {
                    try {
                        if (a.chart) a.chart.update();
                    } catch (error) {
                        // 
                    }
                });

                //To calculate non reward percentage we 1st need All transactions count. Therefore called here.
                this.GetLoyalityOverviewLite();

            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }
    GetLoyalityOverviewLite() {

        this._HelperService.IsFormProcessing = true;
        var Data = {
            Task: 'getloyaltyoverview',
            StartDate: this._HelperService.DateInUTC(this._HelperService.DateRangeStart), // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndDate: this._HelperService.DateInUTC(this._HelperService.DateRangeEnd), // moment().add(2, 'days'),
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            CashierReferenceId: this._HelperService.AppConfig.ActiveCashierReferenceId,
            CashierReferenceKey: this._HelperService.AppConfig.ActiveCashierReferenceKey,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
        _OResponse.subscribe(
            _Response => {



                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._LoyalityOverview = _Response.Result as OLoyalityOverview;
                    this._HelperService.IsFormProcessing = false;
                    // Open To See Dummy Data

                    // this._LoyalityOverview = {
                    //     NewCustomers: 0,
                    //     RepeatingCustomers: 0,
                    //     VisitsByRepeatingCustomers: 0,
                    //     NewCustomerInvoiceAmount: 0.0,
                    //     RepeatingCustomerInvoiceAmount: 0.0,
                    //     Transaction: 520.0,
                    //     TransactionInvoiceAmount: 0.0,

                    //     RewardTransaction: 210.0,
                    //     RewardAmount: 0.0,
                    //     NonRewardTransaction: 0.0,

                    //     TucRewardTransaction: 510.0,
                    //     TucPlusRewardTransaction: 0,
                    //     TucPlusRewardClaimTransaction: 0,

                    //     RedeemTransaction: 0,

                    //     RewardTransactionPerc: 210.0,
                    //     NonRewardTransactionPerc: 0.0,
                    // }

                    this._LoyalityOverview.NonRewardTransaction = this._AccountOverview.Transactions - this._LoyalityOverview.RewardTransaction;

                    this.datapieCard.datasets[0].data[1] = this._LoyalityOverview.RewardTransaction;
                    this.datapieCard.datasets[0].data[0] = this._LoyalityOverview.NonRewardTransaction;

                    if (this._LoyalityOverview.Transaction > 0) {
                        this._LoyalityOverview.RewardTransactionPerc = (this._LoyalityOverview.RewardTransaction / this._LoyalityOverview.Transaction) * 100;
                        this._LoyalityOverview.NonRewardTransactionPerc = (this._LoyalityOverview.NonRewardTransaction / this._LoyalityOverview.Transaction) * 100;
                    }

                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
                //#endregion

                this.components.forEach(a => {
                    try {
                        if (a.chart) a.chart.update();
                    } catch (error) {
                        //
                    }
                });

            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }
    GetPosUsedList() {

        this._HelperService.IsFormProcessing = true;
        var Data = {
            Task: 'getcashierposhistory',
            StartDate: this._HelperService.DateInUTC(this._HelperService.DateRangeStart), // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndDate: this._HelperService.DateInUTC(this._HelperService.DateRangeEnd), // moment().add(2, 'days'),
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            CashierReferenceId: this._HelperService.AppConfig.ActiveCashierReferenceId,
            CashierReferenceKey: this._HelperService.AppConfig.ActiveCashierReferenceKey,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
        _OResponse.subscribe(
            _Response => {



                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._TerminalList = _Response.Result;

                    // Open To See Dummy Data

                    // this._LoyalityOverview = {
                    //     NewCustomers: 0,
                    //     RepeatingCustomers: 0,
                    //     VisitsByRepeatingCustomers: 0,
                    //     NewCustomerInvoiceAmount: 0.0,
                    //     RepeatingCustomerInvoiceAmount: 0.0,
                    //     Transaction: 520.0,
                    //     TransactionInvoiceAmount: 0.0,

                    //     RewardTransaction: 210.0,
                    //     RewardAmount: 0.0,
                    //     NonRewardTransaction: 0.0,

                    //     TucRewardTransaction: 510.0,
                    //     TucPlusRewardTransaction: 0,
                    //     TucPlusRewardClaimTransaction: 0,

                    //     RedeemTransaction: 0,

                    //     RewardTransactionPerc: 210.0,
                    //     NonRewardTransactionPerc: 0.0,
                    // }

                    this._LoyalityOverview.NonRewardTransaction = this._AccountOverview.Transactions - this._LoyalityOverview.RewardTransaction;

                    this.datapieCard.datasets[0].data[1] = this._LoyalityOverview.RewardTransaction;
                    this.datapieCard.datasets[0].data[0] = this._LoyalityOverview.NonRewardTransaction;

                    if (this._LoyalityOverview.Transaction > 0) {
                        this._LoyalityOverview.RewardTransactionPerc = (this._LoyalityOverview.RewardTransaction / this._LoyalityOverview.Transaction) * 100;
                        this._LoyalityOverview.NonRewardTransactionPerc = (this._LoyalityOverview.NonRewardTransaction / this._LoyalityOverview.Transaction) * 100;
                    }

                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
                //#endregion

                this.components.forEach(a => {
                    try {
                        if (a.chart) a.chart.update();
                    } catch (error) {
                        //
                    }
                });

            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    onChartClick(event: any) { }
    //#endregion

    TerminalsList_ToggleOption(val: any, val2: any): void {

    }


    RefreshData() {
        this.GetPosUsedList();

    }

}

export class OTerminalList {
    public TerminalId: any;
    public TotalTransaction: any;
    public TotalInvoiceAmount: any;

}

export class OAccountOverview {
    public Customers: any;
    public Transactions: any;
    public InvoiceAmount: any;
    public CardTransaction: any;
    public CardInvoiceAmount: any;
    public CashTransaction: any;
    public CashInvoiceAmount: any;
    public CashTransPerTerm: any;
    public CardTransPerTerm: any;
    public CardTransactionsAmount: any;
    public OwnerCardTotal: any;
    public OtherCardTotal: any;
}
export class OLoyalityOverview {
    public NewCustomers: any;
    public RepeatingCustomers: any;
    public VisitsByRepeatingCustomers: any;
    public NewCustomerInvoiceAmount: any;
    public RepeatingCustomerInvoiceAmount: any;
    public TransactionInvoiceAmount: any;
    public RewardTransaction: any;
    public NonRewardTransaction: any;
    public TucRewardTransaction: any;
    public TucPlusRewardTransaction: any;
    public TucPlusRewardClaimTransaction: any;
    public RedeemTransaction: any;
    public Transaction: any;
    public RewardTransactionPerc: any;
    public NonRewardTransactionPerc: any;
    public RewardAmount: any;
}
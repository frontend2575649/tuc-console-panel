import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Observable, of, Subscription } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon } from '../../../../service/service';
import swal from 'sweetalert2';
import * as Feather from 'feather-icons';
declare var moment: any;
import { DaterangePickerComponent } from 'ng2-daterangepicker';
export interface LstData {
    ReferenceId:number;
    ReferenceKey:string;
    Name: string;
    isEdit: boolean;
    isAdd: boolean;
    isView: boolean;
    isDelete:boolean;
    Child:LstData[];
  }
@Component({
    selector: 'tu-tuaddroles',
    templateUrl: './tuaddroles.component.html',
    styleUrls: ['./tuaddroles.component.css']
})
export class TURolesComponent implements OnInit {
    public _ObjectSubscription: Subscription = null;
    arrayobj:any=[];
    
    public FeatureList_Config: OList;
    datalst:LstData[];
 
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        private fb:FormBuilder,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {
        this.FormAddRole_Load();
    }
    public StartDate = null;

    ngOnInit() {
        Feather.replace();   
        this.GetDepartments_List();
        this.GetFeatureList();
    }
    createData() {
        return this._FormBuilder.group({
          isEdit: [],
          isView: [],
          isAdd: [],
          Name: [],
          ReferenceId: [],
          isDelete:[],
          details: this.fb.array([])
        });
      }


    public CollapseId = null;
    timeout = null;
    public subCollapseId = null;
    VarAccordion: boolean = false;
    VarSubAccordion: boolean = false;

    FilterStore(referencedata, i: number) {
      this.VarAccordion = !this.VarAccordion;

      this.CollapseId = this._HelperService.GetRandomNumber();
      // this.AmountDistribution = referencedata.AmountDistribution
      this.CollapseId = this._HelperService.GetRandomNumber();
  }
  ParentSelectUnSelectView(FormValue:any,Id:number) {
    FormValue.forEach(element => {
      if(element.ReferenceId==Id)
      {
        let isView=!element.isView;
        element.isView=isView;
        element.details.forEach(subelement =>{
          subelement.isView=isView;
          subelement.details.forEach(sub2element =>{
            sub2element.isView=isView;
            sub2element.details.forEach(sub3element =>{
              sub3element.isView=isView;
            })
          })
        })
      }
    });
      let lst= FormValue;
      this.FormAddRole.get('Features').setValue(lst);
  }
  ParentSelectUnSelectCreate(FormValue:any,Id:number) {
      FormValue.forEach(element => {
        if(element.ReferenceId==Id)
        {
           let isAdd=!element.isAdd;
          element.isAdd=isAdd;
          element.details.forEach(subelement =>{
            subelement.isAdd=isAdd;
            subelement.details.forEach(sub2element =>{
              sub2element.isAdd=isAdd;
              sub2element.details.forEach(sub3element =>{
                sub3element.isAdd=isAdd;
              })
            })
          })
        }
      }); 
        let lst= FormValue;
        this.FormAddRole.get('Features').setValue(lst);
  }
  ParentSelectUnSelectEdit(FormValue:any,Id:number) {
    FormValue.forEach(element => {
      if(element.ReferenceId==Id)
      {
        let isEdit=!element.isEdit;
        element.isEdit=isEdit;
        element.details.forEach(subelement =>{
          subelement.isEdit=isEdit;
          subelement.details.forEach(sub2element =>{
            sub2element.isEdit=isEdit;
            sub2element.details.forEach(sub3element =>{
              sub3element.isEdit=isEdit;
            })
          })
        })
      }
    }); let lst= FormValue;
  this.FormAddRole.get('Features').setValue(lst);
  }
  ParentSelectUnSelectDelete(FormValue:any,Id:number) {
    FormValue.forEach(element => {
      if(element.ReferenceId==Id)
      {
        let isDelete=!element.isDelete;
        element.isDelete=isDelete;
        element.details.forEach(subelement =>{
          subelement.isDelete=isDelete;
          subelement.details.forEach(sub2element =>{
            sub2element.isDelete=isDelete;
            sub2element.details.forEach(sub3element =>{
              sub3element.isDelete=isDelete;
            })
          })
        })
      }
      });let lst= FormValue;
  this.FormAddRole.get('Features').setValue(lst);
  }
  
  SelectUnSelectView(FormValue:any,Id:number) {
    FormValue.forEach(element => {     
      element.details.forEach(subelement =>{
        if(subelement.ReferenceId==Id)
        {           
          let isView=!subelement.isView;
        subelement.isView=isView;
        subelement.details.forEach(sub2element =>{
        sub2element.isView=isView;   
        sub2element.details.forEach(sub3element =>{
          sub3element.isView=isView;
        })         
        })
      }
      })        
  });
  let lst= FormValue;
  this.FormAddRole.get('Features').setValue(lst);
  }
  SelectUnSelectCreate(FormValue:any,Id:number) {
    FormValue.forEach(element => {     
      element.details.forEach(subelement =>{
        if(subelement.ReferenceId==Id)
        {        
        let isAdd=!subelement.isAdd;   
        subelement.isAdd=isAdd;
        subelement.details.forEach(sub2element =>{
        sub2element.isAdd=isAdd;
        sub2element.details.forEach(sub3element =>{
          sub3element.isAdd=isAdd;
        })               
        })
      }
      })        
      });   
      let lst= FormValue;
  this.FormAddRole.get('Features').setValue(lst);
  }
  SelectUnSelectEdit(FormValue:any,Id:number) {
    FormValue.forEach(element => {     
      element.details.forEach(subelement =>{
      if(subelement.ReferenceId==Id)
      {           
      let isEdit=!subelement.isEdit;
      subelement.isEdit=isEdit;
      subelement.details.forEach(sub2element =>{
      sub2element.isEdit=isEdit;    
      sub2element.details.forEach(sub3element =>{
        sub3element.isEdit=isEdit;
      })         
      })
      }
      })        
      });let lst= FormValue;
  this.FormAddRole.get('Features').setValue(lst);
  }
  SelectUnSelectDelete(FormValue:any,Id:number) {
    FormValue.forEach(element => {     
      element.details.forEach(subelement =>{
      if(subelement.ReferenceId==Id)
      {    
      let isDelete=!subelement.isDelete;
      subelement.isDelete=isDelete;
      subelement.details.forEach(sub2element =>{
      sub2element.isDelete=isDelete; 
      sub2element.details.forEach(sub3element =>{
        sub3element.isDelete=isDelete;
      })           
      })
      }
      })        
      });
      let lst= FormValue;
  this.FormAddRole.get('Features').setValue(lst);
  }


  ChildSelectUnSelectView(FormValue:any,Id:number) {
    FormValue.forEach(element => {     
      element.details.forEach(subelement =>{        
        subelement.details.forEach(sub2element =>{
        if(sub2element.ReferenceId==Id){
        let isView=!sub2element.isView;
        sub2element.isView=isView;   
        sub2element.details.forEach(sub3element =>{
          sub3element.isView=isView;  
        })}      
        })      
      })        
  });
  let lst= FormValue;
  this.FormAddRole.get('Features').setValue(lst);
  }
  ChildSelectUnSelectCreate(FormValue:any,Id:number) {
    FormValue.forEach(element => {     
      element.details.forEach(subelement =>{        
        subelement.details.forEach(sub2element =>{
        if(sub2element.ReferenceId==Id){
        let isAdd=!sub2element.isAdd;
        sub2element.isAdd=isAdd;   
        sub2element.details.forEach(sub3element =>{
          sub3element.isAdd=isAdd;  
        })}      
        })      
      })          
      });   
      let lst= FormValue;
  this.FormAddRole.get('Features').setValue(lst);
  }
  ChildSelectUnSelectEdit(FormValue:any,Id:number) {
    FormValue.forEach(element => {     
      element.details.forEach(subelement =>{        
        subelement.details.forEach(sub2element =>{
        if(sub2element.ReferenceId==Id){
        let isEdit=!sub2element.isEdit;
        sub2element.isEdit=isEdit;   
        sub2element.details.forEach(sub3element =>{
          sub3element.isEdit=isEdit;  
        })}      
        })      
      })          
      });
      let lst= FormValue;
  this.FormAddRole.get('Features').setValue(lst);
  }
  ChildSelectUnSelectDelete(FormValue:any,Id:number) {
    FormValue.forEach(element => {     
      element.details.forEach(subelement =>{        
        subelement.details.forEach(sub2element =>{
        if(sub2element.ReferenceId==Id){
        let isDelete=!sub2element.isDelete;
        sub2element.isDelete=isDelete;   
        sub2element.details.forEach(sub3element =>{
          sub3element.isDelete=isDelete;  
        })}      
        })      
      })        
      });
      let lst= FormValue;
  this.FormAddRole.get('Features').setValue(lst);
  }
  

  
  
    FilterSubArray(referencedata, i: number) {
        this.VarSubAccordion = !this.VarSubAccordion;

        this.subCollapseId = this._HelperService.GetRandomNumber();
        // this.AmountDistribution = referencedata.AmountDistribution
        this.subCollapseId = this._HelperService.GetRandomNumber();
    }
    public GetDepartments_Option: Select2Options;
    public GetDepartments_Transport: any;
    GetDepartments_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetDepartments,
            Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.System,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'Name',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                }

            ],
        }
        // _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
        this.GetDepartments_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetDepartments_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetDepartments_Transport,
            multiple: false,
        };
    }
    GetDepartments_ListChange(event: any) {
        this.FormAddRole.patchValue(
            {
                DepartmentKey: event.data[0].ReferenceKey
            }
        );
    }

    FormAddRole: FormGroup;
    FormAddRole_Show() {
        this.FormAddRole_Clear();
        this._HelperService.OpenModal('FormAddRole_Content');
    }
    FormAddRole_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AdminRoles]);
    }
    FormAddRole_Load() {
        this.FormAddRole = this._FormBuilder.group({
            AllowDuplicateName: false,
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SaveRole,           
            Name: [null, Validators.required],
            Sequence: 0,
            DepartmentKey: [null, Validators.required],
            StatusCode: 'default.inactive',
            Features: this._FormBuilder.array([]),
            
        });
    
    }
    FormAddRole_Clear() {
        this.FormAddRole.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.FormAddRole_Load();
    }
    FormAddRole_Process(_FormValue: any) {
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;     
        //Compare Parent Array
        // let result= _FormValue.Features.filter(o1 => !this.datalst.some(o2 => o1.isAdd == o2.isAdd &&
        //     o1.isView == o2.isView && o1.isEdit == o2.isEdit && o1.isDelete == o2.isDelete )); 
        //     this.arrayobj=[];
        //     for(let index=0;index<_FormValue.Features.length;index++){
        //         //Compare Child Array
        //      let array=this.datalst[index].Child;
        //      if(array.length>0){
        //      let innerresult= _FormValue.Features[index].details.filter(o1 => !array.some(o2 => o1.isAdd == o2.isAdd &&
        //                 o1.isView == o2.isView && o1.isEdit == o2.isEdit && o1.isDelete == o2.isDelete )); 
        //                 innerresult.forEach(element => {
        //                     result.push(element);
        //                 });                     
        //      }
        //     }
        let result= []; 
        for(let index=0;index<_FormValue.Features.length;index++){
            //Compare Child Array
         let array=this.datalst[index];
         let parentarray=_FormValue.Features[index];
         if(parentarray.isAdd!=array.isAdd || parentarray.isEdit!=array.isEdit || parentarray.isView!=array.isView || parentarray.isDelete!=array.isDelete){
          result.push(_FormValue.Features[index]);
         }
         if(array.Child.length>0){
          for(let childindex=0;childindex<parentarray.details.length;childindex++)
          {
            let array2=array.Child[childindex];
            let childarray=parentarray.details[childindex];
            if(childarray.isAdd!=array2.isAdd || childarray.isEdit!=array2.isEdit 
              || childarray.isView!=array2.isView || childarray.isDelete!=array2.isDelete){
              result.push(parentarray.details[childindex]);
             }
             if(array2.Child.length>0){
                for(let subchildindex=0;subchildindex<childarray.details.length;subchildindex++)
                {
                  let subarray=array2.Child[subchildindex];
                  let subchildarray=childarray.details[subchildindex];
                  if(subchildarray.isAdd!=subarray.isAdd || subchildarray.isEdit!=subarray.isEdit 
                    || subchildarray.isView!=subarray.isView || subchildarray.isDelete!=subarray.isDelete){
                    result.push(childarray.details[subchildindex]);
                   } 
                   if(subarray.Child.length>0){
                    for(let scindex=0;scindex<subchildarray.details.length;scindex++)
                    {
                      let subcarray=subarray.Child[scindex];
                      let scarray=subchildarray.details[scindex];
                      if(scarray.isAdd!=subcarray.isAdd || scarray.isEdit!=subcarray.isEdit 
                        || scarray.isView!=subcarray.isView || scarray.isDelete!=subcarray.isDelete){
                        result.push(subchildarray.details[scindex]);
                       }                  
                    } 
    
                 }                 
                } 

             }
          }                   
         }
        }
        console.log(result);
        _FormValue.Features=result;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, _FormValue);
       
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                    this.FormAddRole_Clear();
                    this.FormAddRole_Close();
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

      GetFeatureList() {
        this._HelperService.IsFormProcessing = true;
        var pData = {
          Task: this._HelperService.AppConfig.Api.Core.GetFeatureList,      
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, pData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {  
                this._HelperService.IsFormProcessing = false;        
              this.datalst = _Response.Result.Data;   
              // console.log(this.datalst)    
              this.datalst.forEach(item => {
                const fb = this.createData();
                fb.controls.isAdd.setValue(item.isAdd);
                fb.controls.isEdit.setValue(item.isEdit);
                fb.controls.isView.setValue(item.isView);
                fb.controls.isDelete.setValue(item.isDelete);
                fb.controls.Name.setValue(item.Name);
                fb.controls.ReferenceId.setValue(item.ReferenceId);
                item.Child.forEach((detail, i) => {
                  const cfb = this.createData();
                  cfb.get('isEdit').setValue(detail.isEdit);
                  cfb.get('isDelete').setValue(detail.isDelete);
                  cfb.get('isView').setValue(detail.isView);
                  cfb.get('isAdd').setValue(detail.isAdd);
                  cfb.get('Name').setValue(detail.Name);
                  cfb.get('ReferenceId').setValue(detail.ReferenceId);   
                  detail.Child.forEach((subdetail, i) => {
                    const scfb = this.createData();
                    scfb.get('isEdit').setValue(subdetail.isEdit);
                    scfb.get('isDelete').setValue(subdetail.isDelete);
                    scfb.get('isView').setValue(subdetail.isView);
                    scfb.get('isAdd').setValue(subdetail.isAdd);
                    scfb.get('Name').setValue(subdetail.Name);
                    scfb.get('ReferenceId').setValue(subdetail.ReferenceId); 
                    subdetail.Child.forEach((ssdetails,i) =>{
                      const sscfb = this.createData();
                      sscfb.get('isEdit').setValue(ssdetails.isEdit);
                      sscfb.get('isDelete').setValue(ssdetails.isDelete);
                      sscfb.get('isView').setValue(ssdetails.isView);
                      sscfb.get('isAdd').setValue(ssdetails.isAdd);
                      sscfb.get('Name').setValue(ssdetails.Name);
                      sscfb.get('ReferenceId').setValue(ssdetails.ReferenceId); 
                      (scfb.controls.details as FormArray).push(sscfb);
                    });                           
                    (cfb.controls.details as FormArray).push(scfb);
                  });       
                  (fb.controls.details as FormArray).push(cfb);
                });
                (this.FormAddRole.get('Features') as FormArray).push(fb);
              });      
              this._ChangeDetectorRef.detectChanges();        
            }
            else {            
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.HandleException(_Error);
          }
        );
      }

}
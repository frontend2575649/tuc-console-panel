import { ChangeDetectorRef, Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
import { OList, OSelect, OResponse, HelperService, DataHelperService, FilterHelperService } from 'src/app/service/service';
import { Observable, Subscription } from 'rxjs';
declare var $: any;
declare var moment:any;
import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: "tu-adminroles",
  templateUrl: "./adminroles.component.html",
})
export class AdminRolesComponent implements OnInit {
  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;
  public isUpdate:boolean=false;
  public isDelete:boolean=false;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewUserBtn = false;
    this._HelperService.showAddNewRoleBtn = true;


  }
  ngOnInit() {
    this._HelperService.showAddNewUserBtn = false;
    this._HelperService.showAddNewRoleBtn = true;
    this._HelperService.StopClickPropogation();
    this._HelperService.ValidateData();
    Feather.replace();
    this.isUpdate=this._HelperService.SystemName.includes("updaterole");
    this.isDelete=this._HelperService.SystemName.includes("deleterole");
    this.MerchantsList_Setup();
    this.MerchantsList_Filter_Stores_Load();
    this.InitColConfig();
    this.GetDepartments_List();
    this.FormEditRole_Load();
    this.FormEditRoleFeature_Load();
    this.GetFeatures_List();
    // this.RequestHistory_Setup();
    // this._HelperService.StopClickPropogation();
    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.MerchantsList_GetData();
    // });
  }


  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }
  }




  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region merchantslist

  public MerchantsList_Config: OList;
  MerchantsList_Setup() {
    this.MerchantsList_Config =
    {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.Core.GetRoles,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.System,
      Title: 'Admin User Roles',
      StatusType: 'default',
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.HelperTypes.Role, "="),
      TableFields: [
        {
          DisplayName: 'Role',
          SystemName: 'Name',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Access',
          SystemName: 'SubItemsCount',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreatedBy,
          SystemName: 'CreatedByDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        // {
        //   DisplayName: 'AccountTypeCode',
        //   SystemName: 'AccountTypeCode',
        //   DataType: this._HelperService.AppConfig.DataType.Text,
        //   Show: true,
        //   Search: true,
        //   Sort: false,
        //   ResourceId: null,
        // },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.ModifyDate,
          SystemName: 'ModifyDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.ModifyBy,
          SystemName: 'ModifyByDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
      ]
    }
    this.MerchantsList_Config = this._DataHelperService.List_Initialize(
      this.MerchantsList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.AdminRoles,
      this.MerchantsList_Config
    );

    this.MerchantsList_GetData();
  }
  MerchantsList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.MerchantsList_Config.Sort.SortOptions.length; index++) {
        const element = this.MerchantsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.MerchantsList_Config


    );

    this.MerchantsList_Config = this._DataHelperService.List_Operations(
      this.MerchantsList_Config,
      event,
      Type
    );

    if (
      (this.MerchantsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.MerchantsList_GetData();
    }

  }
  timeout = null;
  MerchantsList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.MerchantsList_Config.Sort.SortOptions.length; index++) {
          const element = this.MerchantsList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.MerchantsList_Config


      );

      this.MerchantsList_Config = this._DataHelperService.List_Operations(
        this.MerchantsList_Config,
        event,
        Type
      );

      if (
        (this.MerchantsList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.MerchantsList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);


  }
  MerchantsList_GetData() {
    //this.GetOverviews(this.MerchantsList_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.GetCashiersOverview);

    var TConfig = this._DataHelperService.List_GetData(
      this.MerchantsList_Config
    );
    this.MerchantsList_Config = TConfig;
  }
  MerchantsList_RowSelected(ReferenceData) {
    //#region Save Current Merchant To Storage 

    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveMerchant,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveMerchantReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveMerchantReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 

    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.RewardPercentage,
    //   ReferenceData.ReferenceKey,
    //   ReferenceData.ReferenceId,
    // ]);

    //#endregion
  }

  //#endregion

  //#region StoreFilter

  public MerchantsList_Filter_Stores_Option: Select2Options;
  public MerchantsList_Filter_Stores_Selected = null;
  MerchantsList_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetDepartments,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.System,

      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.MerchantsList_Filter_Stores_Option = {
      placeholder: "Filter by department",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  MerchantsList_Filter_Stores_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.MerchantsList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.MerchantsList_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "DepartmentKey",
        this._HelperService.AppConfig.DataType.Text,
        this.MerchantsList_Filter_Stores_Selected,
        "="
      );
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.MerchantsList_Config.SearchBaseConditions
      );
      this.MerchantsList_Filter_Stores_Selected = null;
    } else if (event.value != this.MerchantsList_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "DepartmentKey",
        this._HelperService.AppConfig.DataType.Text,
        this.MerchantsList_Filter_Stores_Selected,
        "="
      );
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.MerchantsList_Config.SearchBaseConditions
      );
      this.MerchantsList_Filter_Stores_Selected = event.data[0].ReferenceKey;
      this.MerchantsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "DepartmentKey",
          this._HelperService.AppConfig.DataType.Text,
          this.MerchantsList_Filter_Stores_Selected,
          "="
        )
      );
    }

    this.MerchantsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.MerchantsList_Config.SearchBaseConditions = [];
    this.MerchantsList_Config.SearchBaseCondition = null;
    // this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.HelperTypes.Role, "=");

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.MerchantsList_Filter_Stores_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.MerchantsList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
    if (Type == 'Time') {
      this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
      this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
  }
    this.SetOtherFilters();

    this.MerchantsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.AdminRoles
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.AdminRoles
        );
        this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
        this.MerchantsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.MerchantsList_GetData();

    if (ButtonType == 'Sort') {
      $("#MerchantsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#MerchantsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
    this.SetOtherFilters();

    this.MerchantsList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.MerchantsList_Filter_Stores_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  public OverviewData: any = {
    Total: 0,
    Active: 0,
    Blocked: 0,

  };


  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetSearchConditionTur(ListOptions);
    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: SortExpression,
      Type: ListOptions.Type,
      ReferenceKey: ListOptions.ReferenceKey,
      StartDate: ListOptions.StartDate,
      EndDate: ListOptions.EndDate,
      ReferenceId: ListOptions.ReferenceId,
      SubReferenceId: ListOptions.SubReferenceId,
      SubReferenceKey: ListOptions.SubReferenceKey,
      AccountId: ListOptions.AccountId,
      AccountKey: ListOptions.AccountKey,
      ListType: ListOptions.ListType,
      IsDownload: false,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.OverviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });


  }

  GotoAddMerchant() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.UserOnboarding.Merchant
    ]);
  }
  public GetDepartments_Option: Select2Options;
  public GetDepartments_Transport: any;
  GetDepartments_List() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetDepartments,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.System,
      SearchCondition: '',
      SortCondition: ['Name asc'],
      Fields: [
        {
          SystemName: 'ReferenceKey',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: 'Name',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        }

      ],
    }
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
    this.GetDepartments_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetDepartments_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetDepartments_Transport,
      multiple: false,
    };
  }
  GetDepartments_ListChange(event: any) {
    this.FormEditRole.controls['DepartmentKey'].setValue(event.data[0].ReferenceKey);
  }


  public GetFeatures_Option: Select2Options;
  public GetFeatures_Transport: any;
  public SelectedBFeatures = {};

  GetFeatures_List() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetFeatures,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.System,
      SearchCondition: '',
      Type: 'object',
      SortCondition: ['CreateDate asc'],
      Fields: [
        {
          SystemName: 'ReferenceKey',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: 'Name',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        }
        // {
        //   SystemName: 'StatusCode',
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   SearchCondition: '=',
        //   SearchValue: this._HelperService.AppConfig.Status.Active,
        // }
      ],
    }
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'StatusCode', this._HelperService.AppConfig.DataType.Text, null, '=');
    this.GetFeatures_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetFeatures_Option = {
      placeholder: 'Select Features',
      ajax: this.GetFeatures_Transport,
      multiple: false,
    };
  }
  GetFeatures_ListChange(event: any) {

    if (event != undefined && event.value != undefined && event.value.length > 0) {
      // this.SelectedBFeatures = event.data[0];
      this.FormEditRoleFeature.controls['ReferenceId'].setValue(event.data[0].ReferenceId);
      this.FormEditRoleFeature.controls['ReferenceKey'].setValue(event.data[0].ReferenceKey);
    }
    else {
      // this.SelectedBFeatures = [];
    }
  }




  FormEditRole: FormGroup;
  DepartmentName: boolean = true;
  RoleName: boolean = true;

  public Rolekey: any;
  public RoleId: any;
  public RoleData: any = {
    Name: '',
    DepartmentKey: ''
  }

  FormEditRole_Show(ReferenceData) {
    // this.FormEditRole_Clear();
    // this._HelperService.OpenModal('Edit_Role_Content');

    // this.DepartmentName = false;
    // this._ChangeDetectorRef.detectChanges();
    // this.RoleData = cloneDeep(ReferenceData);
    // this.RoleName = this.RoleData.Name;

    // this.FormEditRole.controls['Name'].setValue(this.RoleData.Name)
    // this.GetDepartments_Option.placeholder = this.RoleData.DepartmentName;
    // this.FormEditRole.controls['DepartmentKey'].setValue(this.RoleData.DepartmentKey);


    // this.Rolekey = ReferenceData.ReferenceKey;
    // this.RoleId = ReferenceData.ReferenceId;

    // this.FormEditRole_Load();
    // this.DepartmentName = true;
    // this._ChangeDetectorRef.detectChanges();
    // this.GetDepartments_List();

    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.EditRole,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.Name
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.EditRoles,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);



  }
  FormEditRole_Close() {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
    this._HelperService.CloseModal('Edit_Role_Content');
  }
  FormEditRole_Load() {
    this.FormEditRole = this._FormBuilder.group({
      AllowDuplicateName: false,
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.Updaterole,
      // TypeCode: this._HelperService.AppConfig.HelperTypes.Role,
      ReferenceKey: this.Rolekey,
      ReferenceId: this.RoleId,
      Name: [null, Validators.required],
      Sequence: 0,
      DepartmentKey: [this.RoleData.DepartmentKey, Validators.required],
      StatusCode: 'default.inactive',
    });
  }
  FormEditRole_Clear() {
    this.FormEditRole.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.FormEditRole_Load();
  }
  FormEditRole_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;

    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, _FormValue);

    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.FormEditRole_Clear();
          this.FormEditRole_Close();
          this.MerchantsList_GetData();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  FormEditRoleFeatureFeature_Close() {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
    this._HelperService.CloseModal('Add_Role_Content');
  }
  FormEditRoleFeature: FormGroup
  LaodData: boolean = false;
  FormEditRoleFeature_Show(ReferenceData: any) {

    this.RoleData = cloneDeep(ReferenceData);
    this.Rolekey = ReferenceData.ReferenceKey;
    this.RoleId = ReferenceData.ReferenceId;
    this.FormEditRole_Clear();
    this._HelperService.OpenModal('Edit_Rolefeature_Content');
    this.Rolekey = ReferenceData.ReferenceKey;
    this.RoleId = ReferenceData.ReferenceId;
    this.FormEditRoleFeature_Load();
    this.RequestHistory_Setup();
    this.LaodData = true;

  }
  FormEditRoleFeature_Close() {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
    this._HelperService.CloseModal('Edit_Rolefeature_Content');
  }
  FormEditRoleFeature_Load() {
    this.FormEditRoleFeature = this._FormBuilder.group({
      AllowDuplicateName: false,
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.SaveRoleFeature,
      RoleId: this.RoleId,
      RoleKey: this.Rolekey,
      ReferenceKey: [null, Validators.required],
      ReferenceId: [null, Validators.required],
      // TypeCode: this._HelperService.AppConfig.HelperTypes.Role,
    });
  }
  FormEditRoleFeature_Clear() {
    this.FormEditRoleFeature.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.FormEditRoleFeature_Load();
  }
  FormEditRoleFeature_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, _FormValue);

    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.FormEditRoleFeature_Clear();
          this.FormEditRoleFeature_Close();
          this.MerchantsList_Setup;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  public RequestHistory_Config: OList;
  RequestHistory_Setup() {
    1
    this.RequestHistory_Config = {
      Id: null,
      Type: "all",
      Sort: null,
      Task: this._HelperService.AppConfig.Api.Core.GetRolesFeatures,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.System,
      ReferenceKey: this.Rolekey,
      ReferenceId: this.RoleId,
      Title: 'Features',
      StatusType: 'default',
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.HelperTypes.Feature, "="),
      TableFields: [
        {
          DisplayName: 'Name',
          SystemName: 'Name',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'System Name',
          SystemName: 'SystemName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
        },

      ]
    };
    this.RequestHistory_Config = this._DataHelperService.List_Initialize(
      this.RequestHistory_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Cashier,
      this.RequestHistory_Config
    );

    this.RequestHistory_GetData();
  }
  RequestHistory_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.RequestHistory_Config.Sort.SortOptions.length; index++) {
        const element = this.RequestHistory_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }
    if (Type == "date") {
      this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
      this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
  }
    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.RequestHistory_Config
    );

    this.RequestHistory_Config = this._DataHelperService.List_Operations(
      this.RequestHistory_Config,
      event,
      Type
    );

    if (
      (this.RequestHistory_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.RequestHistory_GetData();
    }

  }
  RequestHistory_GetData() {

    var TConfig = this._DataHelperService.List_GetDataWithOutSort(
      this.RequestHistory_Config
    );
    this.RequestHistory_Config = TConfig;
  }
  RequestHistory_RowSelected(ReferenceData) {
  }



  public DeleteRole(ReferenceData): void {
    this.Rolekey = ReferenceData.ReferenceKey;
    this.RoleId = ReferenceData.ReferenceId;


    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "deleterole",
      ReferenceId: this.RoleId,
      ReferenceKey: this.Rolekey,
    };
    let _OResponse: Observable<OResponse>;

    swal({
      //title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      title: "Delete Role?",
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      input: 'password',
      inputClass: 'swalText',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, pData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.NotifySuccess("Role Deleted Successfully ");
              this.MerchantsList_Setup();

            } else {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });




  }


  public FeatureId: number;
  public FeatureKey: string;

  public DeleteRoleFeature(ReferenceData): void {
    this.FeatureKey = ReferenceData.ReferenceKey;
    this.FeatureId = ReferenceData.ReferenceId;

    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "deleterolefeature",
      ReferenceId: this.FeatureId,
      ReferenceKey: this.FeatureKey,
      RoleId: this.RoleId,
      RoleKey: this.Rolekey,

    };
    let _OResponse: Observable<OResponse>;
    this._HelperService.CloseModal('Edit_Rolefeature_Content');

    swal({
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      input: 'password',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {
        this._HelperService.OpenModal('Edit_Rolefeature_Content');
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, pData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.NotifySuccess("Role Deleted Successfully ");
              this._HelperService.CloseModal('Edit_Rolefeature_Content');

              this.MerchantsList_Setup();

            } else {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });




  }


}
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { ImageCropperModule } from 'ngx-image-cropper';
import { TUAdminUserComponent } from "./adminuser.component";
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { MainPipe } from '../../../../service/main-pipe.module'
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { MerchantguardGuard } from "src/app/service/guard/merchantguard.guard";
import { DynamicRoutesguardGuard } from "src/app/service/guard/dynamicroutes.guard";

const config: InputFileConfig = {
    fileAccept: '*',
    fileLimit: 1,
};

const routes: Routes = [
    {
        path: "",
        component: TUAdminUserComponent,
        canActivate:[DynamicRoutesguardGuard],
        data:{accessName:['viewuser']},
        children: [
            { path: '', data: { permission: "getmerchantdetails", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: '../../tusettings/loginhistory/loginhistory.module#LoginHistorysModule' },
            { path: 'loginhistory/:referencekey/:referenceid', data: { 'permission': 'users', PageName: 'System.Menu.Settings' }, loadChildren: '../../tusettings/loginhistory/loginhistory.module#LoginHistorysModule' },
            // { path: '/adminuser/editprofile/:referencekey/:referenceid', data: { 'permission': 'users', PageName: 'System.Menu.Settings' }, loadChildren: '../../tusettings/adminuser/editprofile/editprofile.module#TUEditProfileModule' },
            { path: 'passwordsecurity', data: { 'permission': 'users', PageName: 'System.Menu.Settings' }, loadChildren: '../../tusettings/adminuser/passwordsecurity/passwordsecurity.module#TUPasswordSecurityUserModule' },
        
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUAdminUserRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUAdminUserRoutingModule,
        GooglePlaceModule,
        MainPipe,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        ImageCropperModule,
        InputFileModule.forRoot(config),

    ],
    declarations: [TUAdminUserComponent]
})
export class TUAdminUserModule { }

import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, ViewChildren, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, OResponse, OUserDetails, OSelect } from "../../../../../service/service";
import { InputFileComponent, InputFile } from 'ngx-input-file';
import { QueryList } from '@angular/core/src/render3';
declare let $: any;
declare var moment: any;

const HelpImageType: any = {
  Receipt: 'receipt',
  Terminal: 'terminal',
  Serial: 'serial'
}

@Component({
  selector: "editprofile",
  templateUrl: "./editprofile.component.html"
})
export class TUEditProfileComponent implements OnInit, OnDestroy {
  isLoaded: boolean = true;
  ngOnDestroy(): void {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService.Icon_Crop_Clear();

  }


  @ViewChild("inputfile")
  private InputFileComponent: InputFileComponent;
  private InitImagePicker(InputFileComponent: InputFileComponent) {
    if (InputFileComponent != undefined) {
      this.CurrentImagesCount = 0;
      this._HelperService._InputFileComponent = InputFileComponent;
      InputFileComponent.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
  }

  

  public ShowCategorySelector: boolean = true;
  public MerchantAddress: any = {};
  public MerchantContactPerson: any = {};
  CurrentFileInput: string = "stores";
  FileInputOptions: any = {
    stores: "stores",
    cashiers: "cashiers"
  }
  
  
  removeImage(): void {
    this.CurrentImagesCount = 0;
    this._HelperService.Icon_Crop_Clear();
   
  }

  public DateOfBirthPickerOptions: any;
  public _MerchantDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      Latitude: null,
      Longitude: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,

    }

  defaultToDarkFilter = ["grayscale: 100%", "invert: 100%"];
  Form_AddRolevalue;
  CurrentImagesCount: number = 0;
  OwnerDisplayName: boolean = true;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this.CurrentFileInput = this.FileInputOptions.stores;
    this._HelperService.ResetDateRange();

    this.DateOfBirthPickerOptions = {
      singleDatePicker: true,
      timePicker: false,
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      showDropdowns: true,
      startDate: this._HelperService.DateInUTC(moment().subtract(18, 'year').startOf("day")),
      endDate: this._HelperService.DateInUTC(moment().subtract(18, 'year').endOf("day")),
      maxDate: moment().subtract(18, 'year').startOf('day')
    };

  }


  //#endregion
  DOB_ToggleOption(event: any) {

    this.Form_EditUser.controls['DateOfBirth'].setValue(event.start);

  }

  ngOnInit() {

    this.GetMerchantDetails();
    this.Form_EditUser_Load();
    this.GetRoles_List();
    this.GetReporting_List();
    this.InitImagePicker(this.InputFileComponent);

  }
  public _AdminDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      Latitude: null,
      Longitude: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,

    }

  GetMerchantDetails() {
    this._HelperService.IsFormProcessing = true;
    this.isLoaded = false;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetAccount,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      // Reference: this._HelperService.GetSearchConditionStrict(
      //   "",
      //   "ReferenceKey",
      //   this._HelperService.AppConfig.DataType.Text,
      //   this._HelperService.AppConfig.ActiveReferenceKey,
      //   "="
      // )
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.toogleIsFormProcessing(false);
          this.isLoaded = true;

          this._AdminDetails = _Response.Result;
          this._AdminDetails.CreateDateS = this._HelperService.GetDateTimeS(this._AdminDetails.CreateDate);
          this._AdminDetails.ModifyDateS = this._HelperService.GetDateTimeS(this._AdminDetails.ModifyDate);

          this._AdminDetails.StatusI = this._HelperService.GetStatusIcon(this._AdminDetails.StatusCode);
          this._AdminDetails.StatusB = this._HelperService.GetStatusBadge(this._AdminDetails.StatusCode);
          this._AdminDetails.StatusC = this._HelperService.GetStatusColor(this._AdminDetails.StatusCode);
          this.OwnerDisplayName = false;
          this._ChangeDetectorRef.detectChanges();

          this.GetReporting_Option.placeholder = this._AdminDetails.OwnerDisplayName;
          this.Form_EditUser.controls['OwnerKey'].setValue(_Response.Result.OwnerKey);

          this.GetRoles_Option.placeholder = this._AdminDetails.RoleName;
          this.Form_EditUser.controls['RoleKey'].setValue(_Response.Result.RoleKey);

          this.MerchantAddress = this._AdminDetails.Address
          this.MerchantContactPerson = this._AdminDetails.ContactPerson
          this.OwnerDisplayName = true;
          this._ChangeDetectorRef.detectChanges();
          this.GetReporting_List();


          //#region RelocateMarker 

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }

          this._AdminDetails.StartDateS = this._HelperService.GetDateS(
            this._AdminDetails.StartDate
          );
          this._AdminDetails.EndDateS = this._HelperService.GetDateS(
            this._AdminDetails.EndDate
          );
          this._AdminDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._AdminDetails.CreateDate
          );
          this._AdminDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._AdminDetails.ModifyDate
          );
          this._AdminDetails.StatusI = this._HelperService.GetStatusIcon(
            this._AdminDetails.StatusCode
          );
          this._AdminDetails.StatusB = this._HelperService.GetStatusBadge(
            this._AdminDetails.StatusCode
          );
          this._AdminDetails.StatusC = this._HelperService.GetStatusColor(
            this._AdminDetails.StatusCode
          );


          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  public GetRoles_Option: Select2Options;
  public GetRoles_Transport: any;
  GetRoles_List() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetCoreParameters,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: '',
      SortCondition: ['Name asc'],
      Fields: [
        {
          SystemName: 'ReferenceKey',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: 'Name',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        {
          SystemName: 'StatusCode',
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: '=',
          SearchValue: this._HelperService.AppConfig.Status.Active,
        }
      ],
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
    this.GetRoles_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetRoles_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetRoles_Transport,
      multiple: false,
    };
  }
  GetRoles_ListChange(event: any) {
    this.Form_EditUser.patchValue(
      {
        RoleKey: event.data[0].ReferenceKey
      }
    );
  }

  public GetReporting_Option: Select2Options;
  public GetReporting_Transport: any;
  GetReporting_List() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetAccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: "AccountTypeCode",
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: "=",
        //     SearchValue: this._HelperService.AppConfig.AccountType.Merchant
        // }
      ]
    }
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
    this.GetReporting_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetReporting_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetReporting_Transport,
      multiple: false,
    };
  }
  GetReporting_ListChange(event: any) {
    this.Form_EditUser.patchValue(
      {
        OwnerKey: event.data[0].ReferenceKey
      }
    );
  }

  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
    //    this._ChangeDetectorRef.detectChanges();
  }
  //#region Edit Terminal 

  Form_EditUser: FormGroup;
  Form_EditUser_Show() {
    this._HelperService.Icon_Crop_Clear();
    this._HelperService.OpenModal("Account_Edit_Content");
  }
  Form_EditUser_Close() {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService.CloseModal("Account_Edit_Content");
  }
  Form_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_EditUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      OwnerKey: [null, Validators.required],
      RoleKey: [null, Validators.required],
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      Code: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(5)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      GenderCode: this._HelperService.AppConfig.Gender.Male,
      DateOfBirth: null,
      Address: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      StatusCode: this._HelperService.AppConfig.Status.Inactive,
      IconContent: this._HelperService._FileSelect_Icon_Data,
      PosterContent: this._HelperService._FileSelect_Poster_Data,
    });
  }
  Form_EditUser_Clear() {
    this.Form_EditUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_EditUser_Process(_FormValue: any) {
    // _FormValue.DisplayName = _FormValue.FirstName;
    // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
    }

    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.Console.V3.System,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account created successfully");
          // this.Form_EditUser_Clear();
          this._HelperService.CloseModal('Account_Edit_Content');
          this.GetMerchantDetails();

          if (_FormValue.OperationType == "close") {
            this.Form_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }





}


import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, ViewChildren, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, OResponse, OUserDetails, OSelect } from "../../../../service/service";
import { InputFileComponent, InputFile } from 'ngx-input-file';
import { QueryList } from '@angular/core/src/render3';
declare let $: any;
declare var moment: any;
import swal from 'sweetalert2';

const HelpImageType: any = {
  Receipt: 'receipt',
  Terminal: 'terminal',
  Serial: 'serial'
}

@Component({
  selector: "tu-adminuser",
  templateUrl: "./adminuser.component.html"
})
export class TUAdminUserComponent implements OnInit, OnDestroy {
  isLoaded: boolean = true;
  public isUpdate:boolean=false;
  public isDelete:boolean=false;
  ngOnDestroy(): void {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService.Icon_Crop_Clear();

  }

  removeImage(): void {

    this.CurrentImagesCount = 0;
    this._HelperService.Icon_Crop_Clear();
  }
  public ShowCategorySelector: boolean = true;
  public MerchantAddress: any = {};
  public MerchantContactPerson: any = {};
  CurrentFileInput: string = "stores";
  FileInputOptions: any = {
    stores: "stores",
    cashiers: "cashiers"


  }

  public _MerchantDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      Latitude: null,
      Longitude: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,

    }

  defaultToDarkFilter = ["grayscale: 100%", "invert: 100%"];
  Form_AddRolevalue;

  @ViewChild("terminal")
  private InputFileComponent_Term: InputFileComponent;

  @ViewChild("cashier")
  private InputFileComponent_Cashier: InputFileComponent;
 
  CurrentImagesCount: number = 0;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this.CurrentFileInput = this.FileInputOptions.stores;
    this._HelperService.ResetDateRange();
    this.DateOfBirthPickerOptions = {
      singleDatePicker: true,
      timePicker: false,
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      showDropdowns: true,
      parentEl: '#update_admin_dateofbirth',
      startDate: this._HelperService.DateInUTC(moment().subtract(18, 'year').startOf("day")),
      endDate: this._HelperService.DateInUTC(moment().subtract(18, 'year').endOf("day")),
      maxDate: moment().subtract(18, 'year').startOf('day')
    };
  }

  //#region DetailShowHIde 

  HideStoreDetail() {
    var element = document.getElementById("StoresDetails");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-ShowStoreDetail");
  }

  ShowStoreDetail() {
    var element = document.getElementById("StoresDetails");
    element.classList.add("Hm-ShowStoreDetail");
    element.classList.remove("Hm-HideDiv");
  }
  resetImage(){
    this._HelperService.Icon_Crop_Clear();
    this.InputFileComponent_Term.files.pop();
    this.InputFileComponent_Cashier.files.pop();
  }
  

  //#endregion

  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  ngOnInit() {

    //#region GetStorageData 
    this._HelperService.ValidateData();
    this.isUpdate=this._HelperService.SystemName.includes("updateuser");
    this.isDelete=this._HelperService.SystemName.includes("deleteuser");
    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveMerchant);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }

    //#endregion

    //#region UIInitialize 

    Feather.replace();
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.ContainerHeight = window.innerHeight;

    this.InitBackDropClickEvent();
    // this.HideStoreDetail();
    this.GetMerchantDetails();
    this.Form_EditUser_Load();

    this.GetBusinessCategories();
    this.Form_AddSubAccount_Load();
    this.Form_AddUser_Load();
    this.GetRoles_List();
    this.GetReporting_List();
    this.FormAddRole_Load();
    // this.InitImagePicker(this.InputFileComponent);

    //    this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)

  }
  private InitImagePicker(previewurl?: string) {
    if (this.InputFileComponent != undefined) {
        this.CurrentImagesCount = 0;
        if (previewurl) {
            this.InputFileComponent.files[0] = {};
            this.InputFileComponent.files[0].preview = previewurl;
        }
        this._HelperService._InputFileComponent = this.InputFileComponent;
        this.InputFileComponent.onChange = (files: Array<InputFile>): void => {
            if (files.length >= this.CurrentImagesCount) {
                this._HelperService._SetFirstImageOrNone(this.InputFileComponent.files);
            }
            this.CurrentImagesCount = files.length;
        };
    }
}


  BlockAccount() {
    swal({
      title: this._HelperService.AppConfig.CommonResource.BlockUserTitle,
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
      '<input type="text"  placeholder="Enter Comment"  id="swal-input1" class="swal2-input">' +
      '<input style="-webkit-text-security: disc;" type="text" placeholder="Enter Pin"  id="swal-input2" class="swal2-input">',
        
        
      focusConfirm: false,
      inputClass:"swalText",
      preConfirm: () => {
        return [
          document.getElementById('swal-input1')['value'],
          document.getElementById('swal-input2')['value']
        ]
      },
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updateaccountstatus",
          AccountId: this._AdminDetails.ReferenceId,
          AccountKey: this._AdminDetails.ReferenceKey,
          StatusCode: "default.suspended",
          AuthPin: result.value[1],
          Comment:result.value[0],
          AccountTypeCode:this._HelperService.AppConfig.AccountType.Admin

        };

        let _OResponse: Observable<OResponse>;

        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, PostData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Admin InActivated Successfully");
              this.GetMerchantDetails();
            } else {
              this._HelperService.NotifySuccess(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });


  }
  UnblockBlockAccount() {
    swal({
      title: this._HelperService.AppConfig.CommonResource.ActiveUserTitle,
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
      '<input type="text" placeholder="Enter Comment"  id="swal-input1" class="swal2-input">' +
      '<input type="password" placeholder="Enter Password" id="swal-input2" class="swal2-input">',
        
        
      focusConfirm: false,
      preConfirm: () => {
        return [
          document.getElementById('swal-input1')['value'],
          document.getElementById('swal-input2')['value']
        ]
      },
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updateaccountstatus",
          AccountId: this._AdminDetails.ReferenceId,
          AccountKey: this._AdminDetails.ReferenceKey,
          StatusCode: "default.active",
          AuthPin: result.value[1],
          Comment:result.value[0],
          AccountTypeCode:this._HelperService.AppConfig.AccountType.Admin

        };

        let _OResponse: Observable<OResponse>;

        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, PostData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Admin Activated Successfully");
              this.GetMerchantDetails();
            } else {
              this._HelperService.NotifySuccess(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });


  }



  MerchantsList_RowSelected() {


    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Loginhistory,
      this._AdminDetails.ReferenceKey,
      this._AdminDetails.ReferenceId,
    ]);

    //#endregion
  }

  AddBulkTerminal() {
    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.BulkTerminal,
    // ]);
  }
  public DateOfBirthPickerOptions: any;
  DOB_ToggleOption(event: any) {

    // this.Form_EditUser.controls['DateOfBirth'].setValue(event.start);
    this.Form_EditUser.controls['DateOfBirth'].setValue(this._HelperService.DateInUTC(event.start));
  }
  //#region MerchantDetails 
  // public _UserAccount: OUserDetails = {
  //   ContactNumber: null,
  //   SecondaryEmailAddress: null,
  //   ReferenceId: null,
  //   BankDisplayName: null,
  //   BankKey: null,
  //   SubOwnerAddress: null,
  //   SubOwnerLatitude: null,
  //   SubOwnerDisplayName: null,
  //   SubOwnerKey: null,
  //   SubOwnerLongitude: null,
  //   AccessPin: null,
  //   LastLoginDateS: null,
  //   AppKey: null,
  //   AppName: null,
  //   AppVersionKey: null,
  //   CreateDate: null,
  //   CreateDateS: null,
  //   CreatedByDisplayName: null,
  //   CreatedByIconUrl: null,
  //   CreatedByKey: null,
  //   Description: null,
  //   IconUrl: null,
  //   ModifyByDisplayName: null,
  //   ModifyByIconUrl: null,
  //   ModifyByKey: null,
  //   ModifyDate: null,
  //   ModifyDateS: null,
  //   PosterUrl: null,
  //   ReferenceKey: null,
  //   StatusCode: null,
  //   StatusI: null,
  //   StatusId: null,
  //   StatusName: null,
  //   AccountCode: null,
  //   AccountOperationTypeCode: null,
  //   AccountOperationTypeName: null,
  //   AccountTypeCode: null,
  //   AccountTypeName: null,
  //   Address: null,
  //   AppVersionName: null,
  //   ApplicationStatusCode: null,
  //   ApplicationStatusName: null,
  //   AverageValue: null,
  //   CityAreaKey: null,
  //   CityAreaName: null,
  //   CityKey: null,
  //   CityName: null,
  //   CountValue: null,
  //   CountryKey: null,
  //   CountryName: null,
  //   DateOfBirth: null,
  //   DisplayName: null,
  //   EmailAddress: null,
  //   EmailVerificationStatus: null,
  //   EmailVerificationStatusDate: null,
  //   FirstName: null,
  //   GenderCode: null,
  //   GenderName: null,
  //   LastLoginDate: null,
  //   LastName: null,
  //   Latitude: null,
  //   Longitude: null,
  //   MobileNumber: null,
  //   Name: null,
  //   NumberVerificationStatus: null,
  //   NumberVerificationStatusDate: null,
  //   OwnerDisplayName: null,
  //   OwnerKey: null,
  //   Password: null,
  //   Reference: null,
  //   ReferralCode: null,
  //   ReferralUrl: null,
  //   RegionAreaKey: null,
  //   RegionAreaName: null,
  //   RegionKey: null,
  //   RegionName: null,
  //   RegistrationSourceCode: null,
  //   RegistrationSourceName: null,
  //   RequestKey: null,
  //   RoleKey: null,
  //   RoleName: null,
  //   SecondaryPassword: null,
  //   SystemPassword: null,
  //   UserName: null,
  //   WebsiteUrl: null
  // };

  public _AdminDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      Latitude: null,
      Longitude: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
      LastActivityDate:null

    }
  @ViewChild("inputfile")
  private InputFileComponent: InputFileComponent;
  // private InitImagePicker(InputFileComponent: InputFileComponent) {
  //   if (InputFileComponent != undefined) {
  //     this.CurrentImagesCount = 0;
  //     this._HelperService._InputFileComponent = InputFileComponent;
  //     InputFileComponent.onChange = (files: Array<InputFile>): void => {
  //       if (files.length >= this.CurrentImagesCount) {
  //         this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
  //       }
  //       this.CurrentImagesCount = files.length;
  //     };
  //   }
  // }
  OwnerDisplayName : boolean = true;
  reportingManager:any;
  assignedRole:any;
  
  GetMerchantDetails() {
    this._HelperService.IsFormProcessing = true;
    this.isLoaded = false;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetAccount,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      // Reference: this._HelperService.GetSearchConditionStrict(
      //   "",
      //   "ReferenceKey",
      //   this._HelperService.AppConfig.DataType.Text,
      //   this._HelperService.AppConfig.ActiveReferenceKey,
      //   "="
      // )
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.toogleIsFormProcessing(false);
          this.isLoaded = true;

          this._AdminDetails = _Response.Result;
          this._AdminDetails.CreateDateS = this._HelperService.GetDateTimeS(this._AdminDetails.CreateDate);
          this._AdminDetails.LastActivityDate = this._HelperService.GetDateTimeS(this._AdminDetails.LastActivityDate);

          this._AdminDetails.ModifyDateS = this._HelperService.GetDateTimeS(this._AdminDetails.ModifyDate);
          this._AdminDetails.DateOfBirthS = this._HelperService.GetDateS(this._AdminDetails.DateOfBirth);
          this._AdminDetails.StatusI = this._HelperService.GetStatusIcon(this._AdminDetails.StatusCode);
          this._AdminDetails.StatusB = this._HelperService.GetStatusBadge(this._AdminDetails.StatusCode);
          this._AdminDetails.StatusC = this._HelperService.GetStatusColor(this._AdminDetails.StatusCode);
          // this.OwnerDisplayName = false;
          this.InitImagePicker(this._AdminDetails.IconUrl);

          // this._ChangeDetectorRef.detectChanges();

          this.GetReporting_Option.placeholder = this._AdminDetails.OwnerDisplayName;
         this.reportingManager=this._AdminDetails.OwnerDisplayName;
          this.Form_EditUser.controls['OwnerKey'].setValue(_Response.Result.OwnerKey);

          this.GetRoles_Option.placeholder = this._AdminDetails.RoleName;
          this.assignedRole=this._AdminDetails.RoleName;
          this.Form_EditUser.controls['RoleKey'].setValue(_Response.Result.RoleKey);

          this.MerchantAddress = this._AdminDetails.Address
          this.MerchantContactPerson = this._AdminDetails.ContactPerson
          // this.OwnerDisplayName = true;
          
          this.GetReporting_List();
          
          this.DateOfBirthPickerOptions.startDate = moment(this._AdminDetails.DateOfBirth);
          this.DateOfBirthPickerOptions.endDate = moment(this._AdminDetails.DateOfBirth);
          //#region RelocateMarker 

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          // this.FormC_EditUser_Latitude = this._AdminDetails.Latitude;
          // this.FormC_EditUser_Longitude = this._AdminDetails.Longitude;
          this.FormC_EditUser_Latitude = _Response.Result.Latitude;
          this.FormC_EditUser_Longitude = _Response.Result.Longitude;
          // this._HelperService._ReLocate();

          //#endregion

          //#region DatesAndStatusInit 

          this._AdminDetails.StartDateS = this._HelperService.GetDateS(
            this._AdminDetails.StartDate
          );
          this._AdminDetails.EndDateS = this._HelperService.GetDateS(
            this._AdminDetails.EndDate
          );
          this._AdminDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._AdminDetails.CreateDate
          );
          this._AdminDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._AdminDetails.ModifyDate
          );
          this._AdminDetails.StatusI = this._HelperService.GetStatusIcon(
            this._AdminDetails.StatusCode
          );
          this._AdminDetails.StatusB = this._HelperService.GetStatusBadge(
            this._AdminDetails.StatusCode
          );
          this._AdminDetails.StatusC = this._HelperService.GetStatusColor(
            this._AdminDetails.StatusCode
          );
          this._AdminDetails.ModifyDateTimes = this._HelperService.GetDateTimeS(this._AdminDetails.ModifyDate);

          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region EditUser

  //#region EditUserFormA 

  FormA_EditUser: FormGroup;

  FormA_EditUser_Show() {
    this._HelperService.OpenModal("FormA_EditUser_Content");
  }
  FormA_EditUser_Close() {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  FormA_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormA_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      FirstName: null,
      LastName: null,
      MobileNumber: null,
      EmailAddress: null,
      Gender: null,
      Status: null,
      OwnerName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      IconContent: this._HelperService._FileSelect_Icon_Data,

    });
  }

  FormA_EditUser_Process(_FormValue: any) {


    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.System,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          this.Forms_EditUser_Close();
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region EditUserFormB 

  FormB_EditUser: FormGroup;
  FormB_EditUser_Address: string = null;
  FormB_EditUser_Latitude: number = 0;
  FormB_EditUser_Longitude: number = 0;


  FormB_EditUser_Show() {
    this._HelperService.OpenModal("FormB_EditUser_Content");
  }

  FormB_EditUser_Close() {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  FormB_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormB_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,

      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      // LastName: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      SecondaryEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
    });
  }

  FormB_EditUser_Process(_FormValue: any) {
    // _FormValue.DisplayName = _FormValue.FirstName;
    // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
    this.toogleIsFormProcessing(true);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.System,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          if (_FormValue.OperationType == "edit") {
            this.Forms_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region EditUserFormC 

  FormC_EditUser: FormGroup;
  FormC_EditUser_Address: string = null;
  FormC_EditUser_Latitude: number = 0;
  FormC_EditUser_Longitude: number = 0;


  @ViewChild('placesStore') placesStore: GooglePlaceDirective;

  FormC_EditUser_PlaceMarkerClick(event) {
    this.FormC_EditUser_Latitude = event.coords.lat;
    this.FormC_EditUser_Longitude = event.coords.lng;

  }
  public FormC_EditUser_AddressChange(address: Address) {
    this.FormC_EditUser_Latitude = address.geometry.location.lat();
    this.FormC_EditUser_Longitude = address.geometry.location.lng();
    this.FormC_EditUser_Address = address.formatted_address;
  }

  FormC_EditUser_Show() {
    this._HelperService.OpenModal("FormC_EditUser_Content");
  }

  FormC_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormC_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
      Latitude: 0,
      Longitude: 0,
      Address: [null, Validators.compose([Validators.required, Validators.maxLength(256), Validators.minLength(2)])],
      Configuration: [],
    });
  }

  FormC_EditUser_Process(_FormValue: any) {
    // _FormValue.DisplayName = _FormValue.FirstName;
    // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
    _FormValue.Longitude = this.FormC_EditUser_Longitude
    _FormValue.Latitude = this.FormC_EditUser_Latitude

    this.toogleIsFormProcessing(true);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.System,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          if (_FormValue.OperationType == "edit") {
            this.Forms_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion
  Forms_EditUser_Close() {
    this.GetMerchantDetails();
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  //#endregion

  //#region BackdropDismiss 

  @ViewChild("offCanvas") divView: ElementRef;

  clicked() {

    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Profile
    // ]);

  }


  //#endregion

  public GetReporting_Option: Select2Options;
  public GetReporting_Transport: any;
  GetReporting_List() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetAccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: "AccountTypeCode",
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: "=",
        //     SearchValue: this._HelperService.AppConfig.AccountType.Merchant
        // }
      ]
    }
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
    this.GetReporting_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetReporting_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetReporting_Transport,
      multiple: false,
    };
  }
  GetReporting_ListChange(event: any) {
    if(event){
      this.reportingManager=event.data[0].Name;
    }
    this.Form_AddUser.patchValue(
      {
        OwnerKey: event.data[0].ReferenceKey
      }
    );
    this.Form_EditUser.patchValue(
      {
        OwnerKey: event.data[0].ReferenceKey
      }
    );
  }

  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
    //    this._ChangeDetectorRef.detectChanges();
  }
  //#region Edit Terminal 

  Form_EditUser: FormGroup;
  Form_EditUser_Show() {
    this._HelperService.Icon_Crop_Clear();
    // this.InitImagePicker(this.InputFileComponent_Term);
    this.InitImagePicker(this._MerchantDetails.IconUrl);

    this._HelperService.OpenModal("Account_Edit_Content");
  }
  Form_EditUser_Close() {
    this._HelperService._FileSelect_Icon_Reset();
    this.InputFileComponent_Term.files.pop();
    this._HelperService.CloseModal("Account_Edit_Content");
  }
  Form_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_EditUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.Core.UpdateAccount,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      OwnerKey: [null, Validators.required],
      RoleKey: [null, Validators.required],
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      Code: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(5)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      GenderCode: this._HelperService.AppConfig.Gender.Male,
      DateOfBirth: null,
      Address: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      // StatusCode: this._HelperService.AppConfig.Status.Inactive,
      IconContent: this._HelperService._FileSelect_Icon_Data,
      PosterContent: this._HelperService._FileSelect_Poster_Data,
      StatusCode:[null]
    });
  }
  Form_EditUser_Clear() {
    this.Form_EditUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_EditUser_Process(_FormValue: any) {
   
    // console.log("_FormValue",_FormValue);
    // _FormValue.DisplayName = _FormValue.FirstName;
    // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
    }

    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account Updated successfully");
          this._HelperService.IsFormProcessing = false;
          // this.Form_EditUser_Clear();
          this._HelperService.CloseModal('Account_Edit_Content');
          // this.Form_EditUser_Clear();
          this.GetMerchantDetails();

          if (_FormValue.OperationType == "close") {
            this.Form_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  //#endregion

  //#region Add Cashier 

  Form_AddCashier: FormGroup;
  Form_AddCashier_Show() {
    this._HelperService.Icon_Crop_Clear();
    // this.InitImagePicker(this.InputFileComponent_Cashier);
    this._HelperService.OpenModal("Form_AddCashier_Content");
  }
  Form_AddCashier_Close() {
    this._HelperService._FileSelect_Icon_Reset();
    this.InputFileComponent_Cashier.files.pop();
    this._HelperService.CloseModal("Form_AddCashier_Content");
  }
  Form_AddCashier_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddCashier = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.SaveCashier,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
      StoreId: [null, Validators.required],
      StoreKey: [null, Validators.required],
      StatusCode: this._HelperService.AppConfig.Status.Active,
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      EmployeeCode: [null],
      GenderCode: this._HelperService.AppConfig.Gender.Male,
    });
  }
  Form_AddCashier_Clear() {
    this.Form_AddCashier.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddCashier_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddCashier_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;

  }

  //#endregion


  //#region Add SubAccount 

  Form_AddSubAccount: FormGroup;
  Form_AddSubAccount_Show() {
    this._HelperService.OpenModal("Form_AddSubAccount_Content");
  }
  Form_AddSubAccount_Close() {
    this._HelperService.CloseModal("Form_AddSubAccount_Content");
  }
  Form_AddSubAccount_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddSubAccount = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.SaveSubAccount,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
      StatusCode: this._HelperService.AppConfig.Status.Active,
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      GenderCode: this._HelperService.AppConfig.Gender.Male,
    });
  }
  Form_AddSubAccount_Clear() {
    this.Form_AddSubAccount.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddSubAccount_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddSubAccount_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    var Request = this.CreateSubAccountRequest(_FormValue);
    let _OResponse: Observable<OResponse>;


  }
  //#endregion
  public TerminalId: string = null;

  public TUTr_Filter_Bank_Option: Select2Options;
  TUTr_Filter_Banks_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
        }]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Bank_Option = {
      placeholder: 'Select Bank (Acquirer)',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Banks_Change(event: any) {
    this.Form_EditUser.patchValue(
      {
        AcquirerReferenceId: event.data[0].ReferenceId,
        AcquirerReferenceKey: event.data[0].ReferenceKey,
      });
  }
  public TUTr_Filter_Provider_Option: Select2Options;
  TUTr_Filter_Providers_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
        }]
    };


    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Provider_Option = {
      placeholder: 'Select  Provider (PTSP)',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Providers_Change(event: any) {
    this.Form_EditUser.patchValue(
      {
        ProviderReferenceId: event.data[0].ReferenceId,
        ProviderReferenceKey: event.data[0].ReferenceKey,
      });
  }
  public TUTr_Filter_Cashier_Option: Select2Options;
  TUTr_Filter_Cashiers_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetCashiers,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
        // {
        //   SystemName: "AccountTypeCode",
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   SearchCondition: "=",
        //   SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
        // }
      ]
    };


    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Cashier_Option = {
      placeholder: 'Select  Cashier',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Cashiers_Change(event: any) {
    this.Form_EditUser.patchValue(
      {
        CashierReferenceId: event.data[0].ReferenceId,
        CashierReferenceKey: event.data[0].ReferenceKey,
      });
  }



  HelpImageUrl: string;
  ShowHelpImage(HelpImage: string): void {
    switch (HelpImage) {
      case HelpImageType.Receipt: {
        this.HelpImageUrl = '../../../../../assets/img/receipt.png';
        this._HelperService.OpenModal("Form_AddHelp_Content");
      }

        break;

      case HelpImageType.Terminal: {
        this.HelpImageUrl = '../../../../../assets/img/terminalid.png';
        this._HelperService.OpenModal('Form_AddHelp_Content');
      }

        break;

      case HelpImageType.Serial: {
        this.HelpImageUrl = '../../../../../assets/img/serial.png';
        this._HelperService.OpenModal('Form_AddHelp_Content');
      }

        break;

      default:
        break;
    }
  }

  //#region Add Store 

  _CurrentAddress: any = {};
  Form_AddRole_Address: string = null;
  Form_AddRole_Latitude: number = 0;
  Form_AddRole_Longitude: number = 0;
  @ViewChild('places') places: GooglePlaceDirective;
  Form_AddRole_PlaceMarkerClick(event) {
    this.Form_AddRole_Latitude = event.coords.lat;
    this.Form_AddRole_Longitude = event.coords.lng;
  }
  public Form_AddRole_AddressChange(address: Address) {

    this.Form_AddRole_Latitude = address.geometry.location.lat();
    this.Form_AddRole_Longitude = address.geometry.location.lng();
    this.Form_AddRole_Address = address.formatted_address;

    this.Form_AddRole.controls['Latitude'].setValue(this.Form_AddRole_Latitude);
    this.Form_AddRole.controls['Longitude'].setValue(this.Form_AddRole_Longitude);
    this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);

  }

  Form_AddRole: FormGroup;
  Form_AddRole_Show() {
    this._HelperService.OpenModal("Add_Role_Content");
  }
  Form_AddRole_Close() {
    this._HelperService.CloseModal("Form_AddRole_Content");
  }
  Form_AddRole_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddRole = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.savestore,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
      Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      MapAddress: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      Latitude: [null, Validators.compose([Validators.required])],
      Longitude: [null, Validators.compose([Validators.required])],
      StatusCode: this._HelperService.AppConfig.Status.Active,
    });
  }
  Form_AddRole_Clear() {
    this.Form_AddRole.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddRole_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddRole_Process(_FormValue: any) {

    if (this.SelectedBusinessCategories != undefined && this.SelectedBusinessCategories.length > 0) {
      _FormValue.Categories = []
      this.SelectedBusinessCategories.forEach(element => {
        _FormValue.Categories.push(
          {
            // TypeCode: this._HelperService.AppConfig.HelperTypes.MerchantCategories,
            ReferenceKey: element.key,
            ReferenceId: element.id
          }
        )
      });
    }

    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;

  }
  //#endregion
  CreateStoreRequest(_FormValue: any): void {
    _FormValue.ContactPerson = {
      FirstName: _FormValue.FirstName,
      LastName: _FormValue.LastName,
      MobileNumber: _FormValue.MobileNumber,
      EmailAddress: _FormValue.EmailAddress
    }

    _FormValue.Address = {
      Latitude: this.Form_AddRole_Latitude,
      Longitude: this.Form_AddRole_Longitude,
      Address: this.Form_AddRole_Address,
      CityName: this._CurrentAddress.sublocality_level_2,
      StateName: this._CurrentAddress.sublocality_level_1,
      CountryName: this._CurrentAddress.country
    }

    _FormValue.FirstName = undefined;
    _FormValue.LastName = undefined;
    _FormValue.MobileNumber = undefined;
    // _FormValue.EmailAddress = undefined;

    _FormValue.Latitude = undefined;
    _FormValue.Longitude = undefined;
    _FormValue.MapAddress = undefined;

    return _FormValue;
  }

  CreateCashierRequest(_FormValue: any): void {

    // _FormValue.Address = {
    //   Latitude: this.Form_AddRole_Latitude,
    //   Longitude: this.Form_AddRole_Longitude,
    //   Address: this.Form_AddRole_Address,
    //   CityName: this._CurrentAddress.sublocality_level_2,
    //   StateName: this._CurrentAddress.sublocality_level_1,
    //   CountryName: this._CurrentAddress.country
    // }

    // _FormValue.Latitude = undefined;
    // _FormValue.Longitude = undefined;
    // _FormValue.MapAddress = undefined;

    var IconContent: any = undefined;

    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      IconContent = this._HelperService._Icon_Cropper_Data;
    }

    _FormValue.IconContent = IconContent;

    return _FormValue;
  }

  CreateSubAccountRequest(_FormValue: any): void {

    // _FormValue.Address = {
    //   Latitude: this.Form_AddRole_Latitude,
    //   Longitude: this.Form_AddRole_Longitude,
    //   Address: this.Form_AddRole_Address,
    //   CityName: this._CurrentAddress.sublocality_level_2,
    //   StateName: this._CurrentAddress.sublocality_level_1,
    //   CountryName: this._CurrentAddress.country
    // }

    // _FormValue.Latitude = undefined;
    // _FormValue.Longitude = undefined;
    // _FormValue.MapAddress = undefined;

    var IconContent: any = undefined;

    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      IconContent = this._HelperService._Icon_Cropper_Data;
    }

    _FormValue.IconContent = IconContent;

    return _FormValue;
  }

  public BusinessCategories = [];
  public S2BusinessCategories = [];

  GetBusinessCategories() {

    this._HelperService.ToggleField = true;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getcategories,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      SortExpression: 'Name asc',
      Offset: 0,
      Limit: 1000,
    }
    PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
      PData.SearchCondition,
      "TypeCode",
      this._HelperService.AppConfig.DataType.Text,
      this._HelperService.AppConfig.HelperTypes.MerchantCategories,
      "="
    );
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Op, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            this.BusinessCategories = _Response.Result.Data;

            this.ShowCategorySelector = false;
            this._ChangeDetectorRef.detectChanges();
            for (let index = 0; index < this.BusinessCategories.length; index++) {
              const element = this.BusinessCategories[index];
              this.S2BusinessCategories.push(
                {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name
                }
              );
            }
            this.ShowCategorySelector = true;
            this._ChangeDetectorRef.detectChanges();

            this._HelperService.ToggleField = false;

          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });
  }
  public SelectedBusinessCategories = [];
  CategoriesSelected(Items) {

    if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
      this.SelectedBusinessCategories = Items.data;
    }
    else {
      this.SelectedBusinessCategories = [];
    }


  }
  SaveMerchantBusinessCategory(item) {
    if (item != '0') {
      var Setup =
      {
        Task: this._HelperService.AppConfig.Api.Core.SaveUserParameter,
        TypeCode: this._HelperService.AppConfig.HelperTypes.MerchantCategories,
        // UserAccountKey: this._UserAccount.ReferenceKey,
        CommonKey: item,
        StatusCode: 'default.active'
      };
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, Setup);
      _OResponse.subscribe(
        _Response => {
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess('Business category assigned to merchant');
            // this.BusinessCategories = [];
            // this.GetMerchantBusinessCategories();
          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }
  }

  ExpandedView: any = false;

  ToogleExpandedView(): void {
    this.ExpandedView = !this.ExpandedView;
  }


  //#region Add new admin 
  public GetRoles_Option: Select2Options;
  public GetRoles_Transport: any;
  GetRoles_List() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetRoles,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.System,
      SearchCondition: '',
      SortCondition: ['Name asc'],
      Fields: [
        {
          SystemName: 'ReferenceKey',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: 'Name',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        {
          SystemName: 'StatusCode',
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: '=',
          SearchValue: this._HelperService.AppConfig.Status.Active,
        }
      ],
    }
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
    this.GetRoles_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetRoles_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetRoles_Transport,
      multiple: false,
    };
  }
  GetRoles_ListChange(event: any) {
    if(event){
      this.assignedRole=event.data[0].Name;
    }
    this.Form_AddUser.patchValue(
      {
        RoleKey: event.value
      }
    );
    this.Form_EditUser.patchValue(
      {
        RoleKey: event.data[0].ReferenceKey
      }
    );
  }

  Form_AddUser: FormGroup;
  Form_AddUser_Show() {
    this._HelperService.OpenModal("Add_User_Content");
  }
  Form_AddUser_Close() {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Administration.AdminUsers]);
  }
  Form_AddUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddUser = this._FormBuilder.group({
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,

      AccountTypeCode: this._HelperService.AppConfig.AccountType.Admin,
      AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Online,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      OwnerKey: null,
      RoleKey: [null, Validators.required],

      // UserName: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(256)])],
      // Password: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.Password)])],
      // AccessPin: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)])],
      SecondaryPassword: null,

      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      Code: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(5)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      GenderCode: this._HelperService.AppConfig.Gender.Male,
      DateOfBirth: null,
      // Description: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      // Address: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],

      ReferralCode: null,
      ReferralUrl: null,

      CountValue: 0,
      AverageValue: 0,

      ApplicationStatusCode: null,

      EmailVerificationStatus: 0,
      EmailVerificationStatusDate: null,

      NumberVerificationStatus: 0,
      NumberVerificationStatusDate: null,

      CountryKey: null,
      RegionKey: null,
      RegionAreaKey: null,
      CityKey: null,
      CityAreaKey: null,

      StatusCode: this._HelperService.AppConfig.Status.Inactive,

      IconContent: this._HelperService._FileSelect_Icon_Data,
      PosterContent: this._HelperService._FileSelect_Poster_Data,

      Owners: [],
      Configuration: [],
    });
  }
  Form_AddUser_Clear() {
    // this.Form_AddUser.reset();
    // this._HelperService._FileSelect_Icon_Reset();
    // this._HelperService._FileSelect_Poster_Reset();
    // this.Form_AddUser_Load();
    // this._HelperService.GetRandomNumber();
    // this._HelperService.GeneratePassoword();
  }
  Form_AddUser_Process(_FormValue: any) {

    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
    }


    _FormValue.DisplayName = _FormValue.FirstName;
    _FormValue.Name = _FormValue.FirstName + ' ' + _FormValue.LastName;
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.Form_AddUser_Clear();
          this._HelperService.CloseModal('Add_User_Content');
          if (_FormValue.OperationType == 'edit') {
            // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Administration.AdminUser.Dashboard, _Response.Result.ReferenceKey]);
          }
          else if (_FormValue.OperationType == 'close') {
            this.Form_AddUser_Close();
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  //#endregion


  //#region role add 

  FormAddRole: FormGroup;
  FormAddRole_Show() {
    this.FormAddRole_Clear();
    this._HelperService.OpenModal('FormAddRole_Content');
  }
  FormAddRole_Close() {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
    this._HelperService.CloseModal('Add_Role_Content');
  }
  FormAddRole_Load() {
    this.FormAddRole = this._FormBuilder.group({
      AllowDuplicateName: false,
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.SaveCoreParameter,
      TypeCode: this._HelperService.AppConfig.HelperTypes.Role,
      Name: [null, Validators.required],
      Sequence: 0,
      StatusCode: 'default.inactive',
    });
  }
  FormAddRole_Clear() {
    this.FormAddRole.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.FormAddRole_Load();
  }
  FormAddRole_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.FormAddRole_Clear();
          this.FormAddRole_Close();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  //#endregion

}

export class OAccountOverview {
  public Merchants: number;
  public Stores: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}

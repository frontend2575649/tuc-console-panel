import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, ViewChildren, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, OResponse, OUserDetails, OSelect } from "../../../../../service/service";
import { InputFileComponent, InputFile } from 'ngx-input-file';
import { QueryList } from '@angular/core/src/render3';
declare let $: any;
declare var moment: any;
import swal from 'sweetalert2';

const HelpImageType: any = {
  Receipt: 'receipt',
  Terminal: 'terminal',
  Serial: 'serial'
}

@Component({
  selector: "tu-passwordsecurity",
  templateUrl: "./passwordsecurity.component.html"
})
export class TUPasswordSecurityComponent implements OnInit, OnDestroy {
  isLoaded: boolean = true;
  _Form_Login_Processing = false;

  ngOnDestroy(): void {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService.Icon_Crop_Clear();

  }

  removeImage(): void {
    this.CurrentImagesCount = 0;
    this._HelperService.Icon_Crop_Clear();
  }
  public ShowCategorySelector: boolean = true;
  public MerchantAddress: any = {};
  public MerchantContactPerson: any = {};
  CurrentFileInput: string = "stores";
  FileInputOptions: any = {
    stores: "stores",
    cashiers: "cashiers"
  }

  public _MerchantDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      Latitude: null,
      Longitude: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,

    }

  defaultToDarkFilter = ["grayscale: 100%", "invert: 100%"];
  Form_AddRolevalue;

  @ViewChild("terminal")
  private InputFileComponent_Term: InputFileComponent;

  @ViewChild("cashier")
  private InputFileComponent_Cashier: InputFileComponent;

  CurrentImagesCount: number = 0;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this.CurrentFileInput = this.FileInputOptions.stores;
    this._HelperService.ResetDateRange();

  }



  ngOnInit() {

    //#region GetStorageData 

    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveMerchant);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }
    //#region UIInitialize 

    Feather.replace();
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.ContainerHeight = window.innerHeight;
    this.Form_EditUserName_Load();
    this.GetMerchantDetails();

  }

  public _AdminDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      Latitude: null,
      Longitude: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
      LastActivityDate: null

    }
  GetMerchantDetails() {
    this._HelperService.IsFormProcessing = true;

    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetAccount,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      // Reference: this._HelperService.GetSearchConditionStrict(
      //   "",
      //   "ReferenceKey",
      //   this._HelperService.AppConfig.DataType.Text,
      //   this._HelperService.AppConfig.ActiveReferenceKey,
      //   "="
      // )
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this._AdminDetails = _Response.Result;
          this._AdminDetails.CreateDateS = this._HelperService.GetDateTimeS(this._AdminDetails.CreateDate);
          this._AdminDetails.LastActivityDate = this._HelperService.GetDateTimeS(this._AdminDetails.LastActivityDate);

          this._AdminDetails.ModifyDateS = this._HelperService.GetDateTimeS(this._AdminDetails.ModifyDate);
          this._AdminDetails.DateOfBirthS = this._HelperService.GetDateS(this._AdminDetails.DateOfBirth);
          this._AdminDetails.StatusI = this._HelperService.GetStatusIcon(this._AdminDetails.StatusCode);
          this._AdminDetails.StatusB = this._HelperService.GetStatusBadge(this._AdminDetails.StatusCode);
          this._AdminDetails.StatusC = this._HelperService.GetStatusColor(this._AdminDetails.StatusCode);
          //#region RelocateMarker 

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }

          // this._HelperService._ReLocate();

          //#endregion

          //#region DatesAndStatusInit 

          this._AdminDetails.StartDateS = this._HelperService.GetDateS(
            this._AdminDetails.StartDate
          );
          this._AdminDetails.EndDateS = this._HelperService.GetDateS(
            this._AdminDetails.EndDate
          );
          this._AdminDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._AdminDetails.CreateDate
          );
          this._AdminDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._AdminDetails.ModifyDate
          );
          this._AdminDetails.StatusI = this._HelperService.GetStatusIcon(
            this._AdminDetails.StatusCode
          );
          this._AdminDetails.StatusB = this._HelperService.GetStatusBadge(
            this._AdminDetails.StatusCode
          );
          this._AdminDetails.StatusC = this._HelperService.GetStatusColor(
            this._AdminDetails.StatusCode
          );


          //#endregion

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  public ShowPasswordDiv: boolean = false;
  public ShowPinDiv: boolean = false;

  // ShowPassword() {
  //     this.ShowPasswordDiv = !this.ShowPasswordDiv;
  // }


  public Password = null;
  ResetPassword() {
    swal({
      title: 'Are You Sure You Want To Reset Your Password',
      text: 'Click Continue To Reset Your Password',
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      // input: 'password',
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      // inputAttributes: {
      //     autocapitalize: 'off',
      //     autocorrect: 'off',
      //     maxLength: "4",
      //     minLength: "4"
      // },
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;
        var pData = {
          Task: 'resetpassword',
          AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
          AccountId: this._HelperService.AppConfig.ActiveReferenceId,
          // AuthPin: result.value
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this.Password = _Response.Result;
              this.GetMerchantDetails();
              this._HelperService.NotifySuccess("Password Reset Successfully");
              this.ShowPasswordDiv = true;



            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {

            this._HelperService.HandleException(_Error);
          });
      }
    });
  }

  public Pin = null;
  ResetPin() {
    swal({
      title: 'Are You Sure You Want To Reset Your Pin',
      text: 'Click Continue To Reset Your Pin',
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      // input: 'password',
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      // inputAttributes: {
      //     autocapitalize: 'off',
      //     autocorrect: 'off',
      //     maxLength: "4",
      //     minLength: "4"
      // },
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;
        var pData = {
          Task: 'resetpin',
          AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
          AccountId: this._HelperService.AppConfig.ActiveReferenceId,

          // AuthPin: result.value
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this.Pin = _Response.Result;
              this.GetMerchantDetails();
              this._HelperService.NotifySuccess("Pin Reset Successfully");
              this.ShowPinDiv = true;



            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {

            this._HelperService.HandleException(_Error);
          });
      }
    });
  }


  Form_EditUserName: FormGroup;
  Form_EditUserName_Load() {
    this.Form_EditUserName = this._FormBuilder.group({
      OperationType: 'new',
      // ReferenceKey: this._HelperService.UserAccount.AccountKey,
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserName,
      UserName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
    });
  }
  Form_EditUserName_Clear() {
    this.Form_EditUserName.reset();
  }
  Form_EditUserName_Process(_FormValue: any) {
    swal({
      position: 'top',
      title: 'Update UserName',
      text: 'Are You Sure You Want To Update  Your UserName',
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        _FormValue.AccountKey = this._HelperService.AppConfig.ActiveReferenceKey;
        _FormValue.AccountId = this._HelperService.AppConfig.ActiveReferenceId;
        // _FormValue.AuthPin = result.value;
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin, _FormValue);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess('UserName updated');
              this.Form_EditUserName_Clear();
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          });


      }
    });

  }



}


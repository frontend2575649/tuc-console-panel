import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { ImageCropperModule } from 'ngx-image-cropper';
import { TUUSerProfileComponent } from "./tuuserprofile.component";
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { MainPipe } from '../../../../service/main-pipe.module'
import { InputFileConfig, InputFileModule } from 'ngx-input-file';

const config: InputFileConfig = {
    fileAccept: '*',
    fileLimit: 1,
};

const routes: Routes = [
    {
        path: "",
        component: TUUSerProfileComponent,
        children: [
            { path: '', data: { permission: "profile", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: '../userprofile/editprofile/editprofile.module#TUEditProfileModule' },
            { path: 'editprofile', data: { 'permission': 'useprofilers', PageName: 'System.Menu.Profile' }, loadChildren: '../userprofile/editprofile/editprofile.module#TUEditProfileModule' },
            { path: 'loginhistory', data: { 'permission': 'profile', PageName: 'System.Menu.Profile' }, loadChildren: '../../../acountsettings/tusettings/userprofile/loginhistory/loginhistory.module#LoginHistorysModule' },
            { path: 'credentials', data: { 'permission': 'credentials', PageName: 'System.Menu.Settings' }, loadChildren: '../credentials/credentials.module#CredentialsModule' },

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUUSerProfileRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUUSerProfileRoutingModule,
        GooglePlaceModule,
        MainPipe,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        ImageCropperModule,
        InputFileModule.forRoot(config),

    ],
    declarations: [TUUSerProfileComponent]
})
export class TUUserProfileModule { }

import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { ImageCropperModule } from 'ngx-image-cropper';
import { TUSettingsComponent } from "./tusettings.component";
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { MainPipe } from '../../../service/main-pipe.module'
import { InputFileConfig, InputFileModule } from 'ngx-input-file';


const config: InputFileConfig = {
    fileAccept: '*',
    fileLimit: 1,
};

const routes: Routes = [
    {
        path: "",
        component: TUSettingsComponent,
        children: [
            { path: '', data: { permission: "getmerchantdetails", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: '../tusettings/adminroles/adminroles.module#AdminRolesModule' },
            { path: 'adminroles', data: { 'permission': 'users', PageName: 'System.Menu.Settings' }, loadChildren: '../tusettings/adminroles/adminroles.module#AdminRolesModule' },
            { path: 'adminusers', data: { 'permission': 'roles', PageName: 'System.Menu.Settings' }, loadChildren: '../tusettings/adminusers/adminusers.module#AdminUsersModule' },
            { path: 'credentials', data: { 'permission': 'credentials', PageName: 'System.Menu.Settings' }, loadChildren: '../tusettings/credentials/credentials.module#CredentialsModule' },
        
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class TUSettingRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUSettingRoutingModule,
        GooglePlaceModule,
        MainPipe,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        ImageCropperModule,
        InputFileModule.forRoot(config),

    ],
    declarations: [TUSettingsComponent]
})
export class TUSettingsModule { }

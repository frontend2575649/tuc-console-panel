import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TueditrolesComponent } from './tueditroles.component';

describe('TueditrolesComponent', () => {
  let component: TueditrolesComponent;
  let fixture: ComponentFixture<TueditrolesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TueditrolesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TueditrolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

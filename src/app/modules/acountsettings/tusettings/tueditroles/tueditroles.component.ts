import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { DataHelperService } from 'src/app/service/datahelper.service';
import { HelperService } from 'src/app/service/helper.service';
import { OList, OResponse, OSelect } from 'src/app/service/object.service';
import * as cloneDeep from 'lodash/cloneDeep';
import * as Feather from 'feather-icons';
export interface LstData {
  ReferenceId:number;
  ReferenceKey:string;
  Name: string;
  isEdit: boolean;
  isAdd: boolean;
  isView: boolean;
  isDelete:boolean;
  Child:LstData[];
}
@Component({
  selector: 'app-tueditroles',
  templateUrl: './tueditroles.component.html',
  styleUrls: ['./tueditroles.component.css']
})
export class TueditrolesComponent implements OnInit {
  public _ObjectSubscription: Subscription = null;
    arrayobj:any=[];
    
    public FeatureList_Config: OList;
    datalst:LstData[];
  FormAddRole: any;
 
  constructor(  public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    private fb:FormBuilder,
    public _ChangeDetectorRef: ChangeDetectorRef) {
      this.FormEditRole_Load();
     }

  ngOnInit() {
    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
    });
    Feather.replace();   
   //
    this.GetRoleDetails();
    this.GetDepartments_List();
  }
  createData() {
    return this._FormBuilder.group({
      isEdit: [],
      isView: [],
      isAdd: [],
      Name: [],
      ReferenceId: [],
      isDelete:[],
      details: this.fb.array([])
    });
  }

public CollapseId = null;
timeout = null;
public subCollapseId = null;
    VarAccordion: boolean = false;
    VarSubAccordion: boolean = false;

    FilterStore(referencedata, i: number) {
        this.VarAccordion = !this.VarAccordion;

        this.CollapseId = this._HelperService.GetRandomNumber();
        // this.AmountDistribution = referencedata.AmountDistribution
        this.CollapseId = this._HelperService.GetRandomNumber();
    }
    FilterSubArray(referencedata, i: number) {
        this.VarSubAccordion = !this.VarSubAccordion;

        this.subCollapseId = this._HelperService.GetRandomNumber();
        // this.AmountDistribution = referencedata.AmountDistribution
        this.subCollapseId = this._HelperService.GetRandomNumber();
    }
FormEditRole: FormGroup;
DepartmentName: boolean = true;
RoleName: boolean = true;

public Rolekey: any;
public RoleId: any;
public RoleData: any = {
  Name: '',
  DepartmentKey: '',
  DepartmentName:'',
  StatusCode:''
}

ParentSelectUnSelectView(FormValue:any,Id:number) {
  FormValue.forEach(element => {
    if(element.ReferenceId==Id)
    {
      let isView=!element.isView;
      element.isView=isView;
      element.details.forEach(subelement =>{
        subelement.isView=isView;
        subelement.details.forEach(sub2element =>{
          sub2element.isView=isView;
          sub2element.details.forEach(sub3element =>{
            sub3element.isView=isView;
          })
        })
      })
    }
  });
    let lst= FormValue;
    this.FormEditRole.get('Features').setValue(lst);
}
ParentSelectUnSelectCreate(FormValue:any,Id:number) {
    FormValue.forEach(element => {
      if(element.ReferenceId==Id)
      {
         let isAdd=!element.isAdd;
        element.isAdd=isAdd;
        element.details.forEach(subelement =>{
          subelement.isAdd=isAdd;
          subelement.details.forEach(sub2element =>{
            sub2element.isAdd=isAdd;
            sub2element.details.forEach(sub3element =>{
              sub3element.isAdd=isAdd;
            })
          })
        })
      }
    }); 
      let lst= FormValue;
      this.FormEditRole.get('Features').setValue(lst);
}
ParentSelectUnSelectEdit(FormValue:any,Id:number) {
  FormValue.forEach(element => {
    if(element.ReferenceId==Id)
    {
      let isEdit=!element.isEdit;
      element.isEdit=isEdit;
      element.details.forEach(subelement =>{
        subelement.isEdit=isEdit;
        subelement.details.forEach(sub2element =>{
          sub2element.isEdit=isEdit;
          sub2element.details.forEach(sub3element =>{
            sub3element.isEdit=isEdit;
          })
        })
      })
    }
  }); let lst= FormValue;
this.FormEditRole.get('Features').setValue(lst);
}
ParentSelectUnSelectDelete(FormValue:any,Id:number) {
  FormValue.forEach(element => {
    if(element.ReferenceId==Id)
    {
      let isDelete=!element.isDelete;
      element.isDelete=isDelete;
      element.details.forEach(subelement =>{
        subelement.isDelete=isDelete;
        subelement.details.forEach(sub2element =>{
          sub2element.isDelete=isDelete;
          sub2element.details.forEach(sub3element =>{
            sub3element.isDelete=isDelete;
          })
        })
      })
    }
    });let lst= FormValue;
this.FormEditRole.get('Features').setValue(lst);
}

SelectUnSelectView(FormValue:any,Id:number) {
  FormValue.forEach(element => {     
    element.details.forEach(subelement =>{
      if(subelement.ReferenceId==Id)
      {           
        let isView=!subelement.isView;
      subelement.isView=isView;
      subelement.details.forEach(sub2element =>{
      sub2element.isView=isView;   
      sub2element.details.forEach(sub3element =>{
        sub3element.isView=isView;
      })         
      })
    }
    })        
});
let lst= FormValue;
this.FormEditRole.get('Features').setValue(lst);
}
SelectUnSelectCreate(FormValue:any,Id:number) {
  FormValue.forEach(element => {     
    element.details.forEach(subelement =>{
      if(subelement.ReferenceId==Id)
      {        
      let isAdd=!subelement.isAdd;   
      subelement.isAdd=isAdd;
      subelement.details.forEach(sub2element =>{
      sub2element.isAdd=isAdd;
      sub2element.details.forEach(sub3element =>{
        sub3element.isAdd=isAdd;
      })               
      })
    }
    })        
    });   
    let lst= FormValue;
this.FormEditRole.get('Features').setValue(lst);
}
SelectUnSelectEdit(FormValue:any,Id:number) {
  FormValue.forEach(element => {     
    element.details.forEach(subelement =>{
    if(subelement.ReferenceId==Id)
    {           
    let isEdit=!subelement.isEdit;
    subelement.isEdit=isEdit;
    subelement.details.forEach(sub2element =>{
    sub2element.isEdit=isEdit;    
    sub2element.details.forEach(sub3element =>{
      sub3element.isEdit=isEdit;
    })         
    })
    }
    })        
    });let lst= FormValue;
this.FormEditRole.get('Features').setValue(lst);
}
SelectUnSelectDelete(FormValue:any,Id:number) {
  FormValue.forEach(element => {     
    element.details.forEach(subelement =>{
    if(subelement.ReferenceId==Id)
    {    
    let isDelete=!subelement.isDelete;
    subelement.isDelete=isDelete;
    subelement.details.forEach(sub2element =>{
    sub2element.isDelete=isDelete; 
    sub2element.details.forEach(sub3element =>{
      sub3element.isDelete=isDelete;
    })           
    })
    }
    })        
    });
    let lst= FormValue;
this.FormEditRole.get('Features').setValue(lst);
}


ChildSelectUnSelectView(FormValue:any,Id:number) {
  FormValue.forEach(element => {     
    element.details.forEach(subelement =>{        
      subelement.details.forEach(sub2element =>{
      if(sub2element.ReferenceId==Id){
      let isView=!subelement.isView;
      sub2element.isView=isView;   
      sub2element.details.forEach(sub3element =>{
        sub3element.isView=isView;  
      })}      
      })      
    })        
});
let lst= FormValue;
this.FormEditRole.get('Features').setValue(lst);
}
ChildSelectUnSelectCreate(FormValue:any,Id:number) {
  FormValue.forEach(element => {     
    element.details.forEach(subelement =>{        
      subelement.details.forEach(sub2element =>{
      if(sub2element.ReferenceId==Id){
      let isAdd=!subelement.isAdd;
      sub2element.isAdd=isAdd;   
      sub2element.details.forEach(sub3element =>{
        sub3element.isAdd=isAdd;  
      })}      
      })      
    })          
    });   
    let lst= FormValue;
this.FormEditRole.get('Features').setValue(lst);
}
ChildSelectUnSelectEdit(FormValue:any,Id:number) {
  FormValue.forEach(element => {     
    element.details.forEach(subelement =>{        
      subelement.details.forEach(sub2element =>{
      if(sub2element.ReferenceId==Id){
      let isEdit=!subelement.isEdit;
      sub2element.isEdit=isEdit;   
      sub2element.details.forEach(sub3element =>{
        sub3element.isEdit=isEdit;  
      })}      
      })      
    })          
    });
    let lst= FormValue;
this.FormEditRole.get('Features').setValue(lst);
}
ChildSelectUnSelectDelete(FormValue:any,Id:number) {
  FormValue.forEach(element => {     
    element.details.forEach(subelement =>{        
      subelement.details.forEach(sub2element =>{
      if(sub2element.ReferenceId==Id){
      let isDelete=!subelement.isDelete;
      sub2element.isDelete=isDelete;   
      sub2element.details.forEach(sub3element =>{
        sub3element.isDelete=isDelete;  
      })}      
      })      
    })        
    });
    let lst= FormValue;
this.FormEditRole.get('Features').setValue(lst);
}
GetRoleDetails() {
  this._HelperService.IsFormProcessing = true;
  var pData = {
    Task: this._HelperService.AppConfig.Api.Core.GetRoleDetails, 
    ReferenceId:this._HelperService.AppConfig.ActiveReferenceId,
    ReferenceKey:this._HelperService.AppConfig.ActiveReferenceKey
  }
  let _OResponse: Observable<OResponse>;
  _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, pData);
  _OResponse.subscribe(
    _Response => {
      if (_Response.Status == this._HelperService.StatusSuccess) {  
        this._HelperService.IsFormProcessing = false;     

        this.datalst = _Response.Result.featureLists;   
            
        this.RoleName = _Response.Result.Name;
        this.RoleData.DepartmentKey=_Response.Result.DepartmentKey;
        this.RoleData.Name=_Response.Result.Name;
        this.RoleData.DepartmentName=_Response.Result.DepartmentName;
        this.RoleData.StatusCode=_Response.Result.StatusCode;
        this.FormEditRole.controls['Name'].setValue(this.RoleData.Name)
     //   this.GetDepartments_Option.placeholder =this.RoleData.DepartmentName;
        this.FormEditRole.controls['DepartmentKey'].setValue(this.RoleData.DepartmentKey);
       // this.FormEditRole.controls['StatusCode'].setValue(this.RoleData.StatusCode);
        this.datalst.forEach(item => {
          const fb = this.createData();
          fb.controls.isAdd.setValue(item.isAdd);
          fb.controls.isEdit.setValue(item.isEdit);
          fb.controls.isView.setValue(item.isView);
          fb.controls.isDelete.setValue(item.isDelete);
          fb.controls.Name.setValue(item.Name);
          fb.controls.ReferenceId.setValue(item.ReferenceId);
          item.Child.forEach((detail, i) => {
            const cfb = this.createData();
            cfb.get('isEdit').setValue(detail.isEdit);
            cfb.get('isDelete').setValue(detail.isDelete);
            cfb.get('isView').setValue(detail.isView);
            cfb.get('isAdd').setValue(detail.isAdd);
            cfb.get('Name').setValue(detail.Name);
            cfb.get('ReferenceId').setValue(detail.ReferenceId);   
            detail.Child.forEach((subdetail, i) => {
              const scfb = this.createData();
              scfb.get('isEdit').setValue(subdetail.isEdit);
              scfb.get('isDelete').setValue(subdetail.isDelete);
              scfb.get('isView').setValue(subdetail.isView);
              scfb.get('isAdd').setValue(subdetail.isAdd);
              scfb.get('Name').setValue(subdetail.Name);
              scfb.get('ReferenceId').setValue(subdetail.ReferenceId);    
              subdetail.Child.forEach((ssdetails,i) =>{
                const sscfb = this.createData();
                sscfb.get('isEdit').setValue(ssdetails.isEdit);
                sscfb.get('isDelete').setValue(ssdetails.isDelete);
                sscfb.get('isView').setValue(ssdetails.isView);
                sscfb.get('isAdd').setValue(ssdetails.isAdd);
                sscfb.get('Name').setValue(ssdetails.Name);
                sscfb.get('ReferenceId').setValue(ssdetails.ReferenceId); 
                (scfb.controls.details as FormArray).push(sscfb);
              });                            
              (cfb.controls.details as FormArray).push(scfb);
            });       
            (fb.controls.details as FormArray).push(cfb);
          });                    
          (this.FormEditRole.get('Features') as FormArray).push(fb);
        });  

        this.Rolekey = _Response.Result.ReferenceKey;
        this.RoleId = _Response.Result.ReferenceId;

        this._HelperService.AppConfig.ActiveReferenceId=this.RoleId;
        this._HelperService.AppConfig.ActiveReferenceKey=this.Rolekey;

        this.DepartmentName = true;
      
        this.GetDepartments_List();
       // this.FormEditRole_Load();
        this._ChangeDetectorRef.detectChanges();        
      }
      else {            
        this._HelperService.NotifyError(_Response.Message);
      }
    },
    _Error => {
      this._HelperService.HandleException(_Error);
    }
  );
}

  FormEditRole_Close() {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AdminRoles]);
  }
  FormEditRole_Load() {
    this.FormEditRole = this._FormBuilder.group({
      AllowDuplicateName: false,
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.Updaterole,
      // TypeCode: this._HelperService.AppConfig.HelperTypes.Role,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      Name: [null, Validators.required],
      Sequence: 0,
      DepartmentKey: [this.RoleData.DepartmentKey, Validators.required],
      StatusCode: 'default.inactive',
      Features: this._FormBuilder.array([]),
    });
  }
  FormEditRole_Clear() {
    this.FormEditRole.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.FormEditRole_Load();
  }
  FormEditRole_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    let result= []; 
      for(let index=0;index<_FormValue.Features.length;index++){
          //Compare Child Array
       let array=this.datalst[index];
       let parentarray=_FormValue.Features[index];
       if(parentarray.isAdd!=array.isAdd || parentarray.isEdit!=array.isEdit || parentarray.isView!=array.isView || parentarray.isDelete!=array.isDelete){
        result.push(_FormValue.Features[index]);
       }
       if(array.Child.length>0){
        for(let childindex=0;childindex<parentarray.details.length;childindex++)
        {
          let array2=array.Child[childindex];
          let childarray=parentarray.details[childindex];
          if(childarray.isAdd!=array2.isAdd || childarray.isEdit!=array2.isEdit 
            || childarray.isView!=array2.isView || childarray.isDelete!=array2.isDelete){
            result.push(parentarray.details[childindex]);
           }
           if(array2.Child.length>0){
            for(let subchildindex=0;subchildindex<childarray.details.length;subchildindex++)
            {
              let subarray=array2.Child[subchildindex];
              let subchildarray=childarray.details[subchildindex];
              if(subchildarray.isAdd!=subarray.isAdd || subchildarray.isEdit!=subarray.isEdit 
                || subchildarray.isView!=subarray.isView || subchildarray.isDelete!=subarray.isDelete){
                result.push(childarray.details[subchildindex]);
               }
               if(subarray.Child.length>0){
                for(let scindex=0;scindex<subchildarray.details.length;scindex++)
                {
                  let subcarray=subarray.Child[scindex];
                  let scarray=subchildarray.details[scindex];
                  if(scarray.isAdd!=subcarray.isAdd || scarray.isEdit!=subcarray.isEdit 
                    || scarray.isView!=subcarray.isView || scarray.isDelete!=subcarray.isDelete){
                    result.push(subchildarray.details[scindex]);
                   }                  
                } 

             }                   
            } 

         }
        }                   
       }
      }
    _FormValue.ReferenceId=this.RoleId;
    _FormValue.ReferenceKey=this.Rolekey;
    _FormValue.Features=result;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.System, _FormValue);

    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.FormEditRole_Clear();
          this.FormEditRole_Close();
        //  this.MerchantsList_GetData();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  public GetDepartments_Option: Select2Options;
  public GetDepartments_Transport: any;
  GetDepartments_List() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetDepartments,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.System,
      SearchCondition: '',
      SortCondition: ['Name asc'],
      Fields: [
        {
          SystemName: 'ReferenceKey',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: 'Name',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        }

      ],
    }
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
    this.GetDepartments_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    if(this.RoleData.DepartmentKey!=null){
      this.GetDepartments_Option = {
        placeholder: "Management",
        ajax: this.GetDepartments_Transport,
        multiple: false,
      };
    }
    else{
    this.GetDepartments_Option = {
      placeholder: "Management",
      ajax: this.GetDepartments_Transport,
      multiple: false,
    };
  }
  }
  GetDepartments_ListChange(event: any) {
    this.FormEditRole.controls['DepartmentKey'].setValue(event.data[0].ReferenceKey);
  }

}

import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Observable } from 'rxjs';
import { HelperService, OResponse } from "../../../../service/service";
import { TranslateService } from '@ngx-translate/core';
import swal from 'sweetalert2';
declare var $: any;


@Component({
    selector: "tu-credentials",
    templateUrl: "./credentials.component.html",
})
export class CredentialsComponent implements OnInit {
    _Form_Login_Processing = false;
    _Show_Form_ResetPassword = false;
    _Form_Login: FormGroup;
    _Form_Otp: FormGroup;

    URLData: any = {
        ActiveReferenceKey: null,
        ActiveReferenceId: null,
        Code: null
    };

    constructor(
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _TranslateService: TranslateService,
        public _Router: Router,
        public _HelperService: HelperService,
    ) {

        this._HelperService.showAddNewUserBtn = false;
        this._HelperService.showAddNewRoleBtn = false;

        this.Form_EditPassword_Load();

        this._ActivatedRoute.params.subscribe((params: Params) => {

            this.URLData.ActiveReferenceKey = params['referencekey'];
            this.URLData.ActiveReferenceId = params['referenceid'];
            this.URLData.Code = params['code'];

        });
    }
    ngOnInit() {
        this.Form_EditUserName_Load();
        this.GetMerchantDetails();
        this.Form_EditPin_Load();

    }


    OShowPassword: boolean = true;
    OToogleShowHidePassword(): void {
        this.OShowPassword = !this.OShowPassword;
    }

    NShowPassword: boolean = true;
    NToogleShowHidePassword(): void {
        this.NShowPassword = !this.NShowPassword;
    }

    public _AdminDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      Latitude: null,
      Longitude: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
      LastActivityDate:null

    }
    GetMerchantDetails() {
        this._HelperService.IsFormProcessing = true;
      
        var pData = {
          Task: this._HelperService.AppConfig.Api.Core.GetAccount,
          ReferenceId: this._HelperService.UserAccount.AccountId,
          ReferenceKey: this._HelperService.UserAccount.AccountKey,
          // Reference: this._HelperService.GetSearchConditionStrict(
          //   "",
          //   "ReferenceKey",
          //   this._HelperService.AppConfig.DataType.Text,
          //   this._HelperService.AppConfig.ActiveReferenceKey,
          //   "="
          // )
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin, pData);
        _OResponse.subscribe(
          _Response => {
            if (_Response.Status == this._HelperService.StatusSuccess) {
        
              this._AdminDetails = _Response.Result;
              this._AdminDetails.CreateDateS = this._HelperService.GetDateTimeS(this._AdminDetails.CreateDate);
              this._AdminDetails.LastActivityDate = this._HelperService.GetDateTimeS(this._AdminDetails.LastActivityDate);
    
              this._AdminDetails.ModifyDateS = this._HelperService.GetDateTimeS(this._AdminDetails.ModifyDate);
              this._AdminDetails.DateOfBirthS = this._HelperService.GetDateS(this._AdminDetails.DateOfBirth);
              this._AdminDetails.StatusI = this._HelperService.GetStatusIcon(this._AdminDetails.StatusCode);
              this._AdminDetails.StatusB = this._HelperService.GetStatusBadge(this._AdminDetails.StatusCode);
              this._AdminDetails.StatusC = this._HelperService.GetStatusColor(this._AdminDetails.StatusCode);
              //#region RelocateMarker 
    
              if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
                this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
                this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
              } else {
                this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
                this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
              }
            
              // this._HelperService._ReLocate();
    
              //#endregion
    
              //#region DatesAndStatusInit 
    
              this._AdminDetails.StartDateS = this._HelperService.GetDateS(
                this._AdminDetails.StartDate
              );
              this._AdminDetails.EndDateS = this._HelperService.GetDateS(
                this._AdminDetails.EndDate
              );
              this._AdminDetails.CreateDateS = this._HelperService.GetDateTimeS(
                this._AdminDetails.CreateDate
              );
              this._AdminDetails.ModifyDateS = this._HelperService.GetDateTimeS(
                this._AdminDetails.ModifyDate
              );
              this._AdminDetails.StatusI = this._HelperService.GetStatusIcon(
                this._AdminDetails.StatusCode
              );
              this._AdminDetails.StatusB = this._HelperService.GetStatusBadge(
                this._AdminDetails.StatusCode
              );
              this._AdminDetails.StatusC = this._HelperService.GetStatusColor(
                this._AdminDetails.StatusCode
              );
    
    
              //#endregion
    
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.HandleException(_Error);
          }
        );
      }

    Form_EditPassword: FormGroup;
    Form_EditPassword_Load() {
        this.Form_EditPassword = this._FormBuilder.group({
            OperationType: 'new',
            // ReferenceKey: this._HelperService.UserAccount.AccountKey,
            Task: this._HelperService.AppConfig.Api.Core.UpdatePassword,
            OldPassword: [null, Validators.required],
            NewPassword: [null, Validators.compose([Validators.required , Validators.pattern(
                this._HelperService.AppConfig.ValidatorRegex.Password
            ),
            Validators.minLength(8)
        ])],
        });
    }
    Form_EditPassword_Clear() {
        this.Form_EditPassword.reset();
        this.Form_EditPassword_Load();
    }
    Form_EditPassword_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: 'Update password',
            // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                _FormValue.ReferenceKey = this._HelperService.UserAccount.AccountKey;
                // _FormValue.AuthPin = result.value;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                if (_FormValue.OldPassword != _FormValue.NewPassword) {
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Profile, _FormValue);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess('Password updated');
                                this.Form_EditPassword_Clear();
                            }
                            else {
                                this._HelperService.NotifyError('Enter valid old password');
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        });
                }
                else {
                    this._HelperService.NotifyError('Old Password and New Password should not be same');
                }

            }
        });

    }

    Form_EditUserName: FormGroup;
    Form_EditUserName_Load() {
        this.Form_EditUserName = this._FormBuilder.group({
            OperationType: 'new',
            // ReferenceKey: this._HelperService.UserAccount.AccountKey,
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserName,
            UserName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
        });
    }
    Form_EditUserName_Clear() {
        this.Form_EditPassword.reset();
    }
    Form_EditUserName_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: 'Update UserName',
            text:'Are You Sure You Want To Update  Your UserName',
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                _FormValue.AccountKey = this._HelperService.UserAccount.AccountKey;
                _FormValue.AccountId = this._HelperService.UserAccount.AccountId;
                // _FormValue.AuthPin = result.value;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin, _FormValue);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess('UserName updated');
                                this.Form_EditUserName_Clear();
                            }
                            else {
                                this._HelperService.NotifyError(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        });
              

            }
        });

    }

    Form_EditPin: FormGroup;
    Form_EditPin_Load() {
        this.Form_EditPin = this._FormBuilder.group({
            OperationType: 'new',
            // ReferenceKey: this._HelperService.UserAccount.AccountKey,
            Task: this._HelperService.AppConfig.Api.Core.UpdatePin,
            OldPin: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)])],
            NewPin: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)])],
        });
    }
    Form_EditPin_Clear() {
        this.Form_EditPin.reset();
        this.Form_EditPin_Load();
    }
    Form_EditPin_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: 'Update Pin',
            text: 'Are You Sure You Want To Update  Your Pin?',
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                _FormValue.AccountKey = this._HelperService.UserAccount.AccountKey;
                _FormValue.AccountId = this._HelperService.UserAccount.AccountId;

                // _FormValue.AuthPin = result.value;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                if (_FormValue.OldPin != _FormValue.NewPin) {
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin, _FormValue);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess('Pin updated');
                                this.Form_EditPin_Clear();
                            }
                            else {
                                this._HelperService.NotifyError('Enter valid Old Pin');
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        });
                }
                else {
                    this._HelperService.NotifyError('Old Pin and New Pin should not be same');
                }

            }
        });

    }
    

}

import { ChangeDetectorRef, Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
import { OList, OSelect, OResponse, HelperService, DataHelperService, FilterHelperService } from 'src/app/service/service';
import { Observable, Subscription } from 'rxjs';
declare var $: any;
declare var moment:any;

@Component({
  selector: "tu-adminusers",
  templateUrl: "./adminusers.component.html",
 
})
export class AdminUsersComponent implements OnInit, OnDestroy {
  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewUserBtn = true;
    this._HelperService.showAddNewRoleBtn = false;
  }

  ReloadSubscription: Subscription;

  ngOnInit() {
   
    this._HelperService.showAddNewUserBtn = true;
    this._HelperService.showAddNewRoleBtn = false;
    this._HelperService.StopClickPropogation();
    Feather.replace();

    this.MerchantsList_Setup();
    this.MerchantsList_Filter_Roles_Load();
    this.InitColConfig();

    this.ReloadSubscription = this._HelperService.ReloadEventEmit.subscribe((number) => {
      this.MerchantsList_GetData();
    });
    this.Reporting_Filter_Load();
    // this._HelperService.StopClickPropogation();
    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.MerchantsList_GetData();
    // });
  }

 
 
  
  ngOnDestroy(): void {

    
    try {
      this._ObjectSubscription.unsubscribe();
      this.ReloadSubscription.unsubscribe();
    } catch (error) {

    }
  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  RouteAdmin(ReferenceData) {
    //#region Save Current Merchant To Storage 

    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveMerchant,
      {
        ReferenceKey: ReferenceData.OwnerKey,
        ReferenceId: ReferenceData.OwnerId,
        DisplayName: ReferenceData.OwnerDisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceData.OwnerKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.OwnerId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Loginhistory,
      ReferenceData.OwnerKey,
      ReferenceData.OwnerId,
    ]);

    //#endregion
  }

  //#endregion

  //#region merchantslist

  public MerchantsList_Config: OList;
  MerchantsList_Setup() {
    this.MerchantsList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.Core.GetAccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin,
      Title: 'User Accounts',
      StatusType: 'default',
      TableFields: [
        {
          DisplayName: 'Name',
          SystemName: 'Name',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Reporting',
          SystemName: 'OwnerDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Mobile Number',
          SystemName: 'MobileNumber',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'UserName',
          SystemName: 'EmailAddress',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Role',
          SystemName: 'RoleName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Last Login',
          SystemName: 'LastActivityDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true
        }
      ]
    };

    // this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.MerchantsList_Config.SearchBaseCondition, "AccountTypeCode", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.AccountType.Admin, "=");
    this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.MerchantsList_Config.SearchBaseCondition, "ReferenceKey", this._HelperService.AppConfig.DataType.Text, this._HelperService.UserAccount.AccountKey, "!=");
    this.MerchantsList_Config = this._DataHelperService.List_Initialize(
      this.MerchantsList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.AdminUsers,
      this.MerchantsList_Config
    );

    this.MerchantsList_GetData();
  }
  MerchantsList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.MerchantsList_Config.Sort.SortOptions.length; index++) {
        const element = this.MerchantsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }
    if (Type == "date") {
      this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
      this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
  }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.MerchantsList_Config


    );

    this.MerchantsList_Config = this._DataHelperService.List_Operations(
      this.MerchantsList_Config,
      event,
      Type
    );

    if (
      (this.MerchantsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.MerchantsList_GetData();
    }

  }

  timeout = null;
  MerchantsList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.MerchantsList_Config.Sort.SortOptions.length; index++) {
          const element = this.MerchantsList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.MerchantsList_Config


      );

      this.MerchantsList_Config = this._DataHelperService.List_Operations(
        this.MerchantsList_Config,
        event,
        Type
      );

      if (
        (this.MerchantsList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.MerchantsList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);


  }
  MerchantsList_GetData() {
    //this.GetOverviews(this.MerchantsList_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.GetCashiersOverview);

    var TConfig = this._DataHelperService.List_GetData(
      this.MerchantsList_Config
    );

    this.MerchantsList_Config = TConfig;
    // console.log("this.MerchantsList_Config",this.MerchantsList_Config);
  }
  MerchantsList_RowSelected(ReferenceData) {
    //#region Save Current Merchant To Storage 

    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveMerchant,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      }
    );

    //#endregion

    //#region Set Active Reference Key To Current Merchant 

    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    //#endregion

    //#region navigate 

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Loginhistory,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);

    //#endregion
  }

  //#endregion

  //#region RoleFilter

  public MerchantsList_Filter_Roles_Option: Select2Options;
  public MerchantsList_Filter_Roles_Selected = null;
  MerchantsList_Filter_Roles_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetCoreParameters,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: '',
      SortCondition: ['Name asc'],
      Fields: [
        {
          SystemName: 'ReferenceId',
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: 'Name',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        {
          SystemName: 'StatusCode',
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: '=',
          SearchValue: this._HelperService.AppConfig.Status.Active,
        }
      ],
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
    var GetRoles_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.MerchantsList_Filter_Roles_Option = {
      placeholder: PlaceHolder,
      ajax: GetRoles_Transport,
      multiple: false,
    };
  }
  MerchantsList_Filter_Roles_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.MerchantsList_Config,
      this._HelperService.AppConfig.OtherFilters.SubAdmin.Role
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.MerchantsList_Filter_Roles_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "RoleKey",
        this._HelperService.AppConfig.DataType.Number,
        this.MerchantsList_Filter_Roles_Selected,
        "="
      );
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.MerchantsList_Config.SearchBaseConditions
      );
      this.MerchantsList_Filter_Roles_Selected = null;
    } else if (event.value != this.MerchantsList_Filter_Roles_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "RoleKey",
        this._HelperService.AppConfig.DataType.Number,
        this.MerchantsList_Filter_Roles_Selected,
        "="
      );
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.MerchantsList_Config.SearchBaseConditions
      );
      this.MerchantsList_Filter_Roles_Selected = event.data[0].ReferenceKey;
      this.MerchantsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "RoleKey",
          this._HelperService.AppConfig.DataType.Number,
          this.MerchantsList_Filter_Roles_Selected,
          "="
        )
      );
    }

    this.MerchantsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion



  //Reporting

  public Reporting_Filter_Option: Select2Options;
  public Reporting_Filter_Selected = null;
  Reporting_Filter_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetAccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Admin,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: 'ReferenceId',
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: 'Name',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        }
      ],
    }
    var GetRoles_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.Reporting_Filter_Option = {
      placeholder: PlaceHolder,
      ajax: GetRoles_Transport,
      multiple: false,
    };
  }
  Reporting_Filter_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.MerchantsList_Config,
      this._HelperService.AppConfig.OtherFilters.SubAdmin.Role
    );

    this.ReportingOwnerEventProcessing(event);

  }

  ReportingOwnerEventProcessing(event: any): void {
    if (event.value == this.Reporting_Filter_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "OwnerKey",
        this._HelperService.AppConfig.DataType.Number,
        this.Reporting_Filter_Selected,
        "="
      );
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.MerchantsList_Config.SearchBaseConditions
      );
      this.Reporting_Filter_Selected = null;
    } else if (event.value != this.Reporting_Filter_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "OwnerKey",
        this._HelperService.AppConfig.DataType.Number,
        this.Reporting_Filter_Selected,
        "="
      );
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.MerchantsList_Config.SearchBaseConditions
      );
      this.Reporting_Filter_Selected = event.data[0].ReferenceKey;
      this.MerchantsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "OwnerKey",
          this._HelperService.AppConfig.DataType.Number,
          this.Reporting_Filter_Selected,
          "="
        )
      );
    }

    this.MerchantsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //end

  SetOtherFilters(): void {

    this.MerchantsList_Config.SearchBaseConditions = [];
    this.MerchantsList_Config.SearchBaseCondition = null;
    // this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.MerchantsList_Config.SearchBaseCondition, "AccountTypeCode", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.AccountType.Admin, "=");
    this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.MerchantsList_Config.SearchBaseCondition, "ReferenceKey", this._HelperService.AppConfig.DataType.Text, this._HelperService.UserAccount.AccountKey, "!=");
    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.MerchantsList_Filter_Roles_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);

    //#region setOtherFilters
    //this.SetOtherFilters();
    //#endregion

    this.MerchantsList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {

    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
    if (Type == 'Time') {
      this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
      this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
  }
    this.SetOtherFilters();

    this.MerchantsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.AdminUsers
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.AdminUsers
        );
        this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
        this.MerchantsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.MerchantsList_GetData();

    if (ButtonType == 'Sort') {
      $("#MerchantsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#MerchantsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
    this.SetOtherFilters();

    this.MerchantsList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.MerchantsList_Filter_Roles_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  public OverviewData: any = {
    Total: 0,
    Active: 0,
    Blocked: 0,

  };


  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetSearchConditionTur(ListOptions);
    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: SortExpression,
      Type: ListOptions.Type,
      ReferenceKey: ListOptions.ReferenceKey,
      StartDate: ListOptions.StartDate,
      EndDate: ListOptions.EndDate,
      ReferenceId: ListOptions.ReferenceId,
      SubReferenceId: ListOptions.SubReferenceId,
      SubReferenceKey: ListOptions.SubReferenceKey,
      AccountId: ListOptions.AccountId,
      AccountKey: ListOptions.AccountKey,
      ListType: ListOptions.ListType,
      IsDownload: false,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.OverviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });


  }

  GotoAddMerchant() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.UserOnboarding.Merchant
    ]);
  }

}
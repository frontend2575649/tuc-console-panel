import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable, Subscription } from "rxjs";
import { DataHelperService, HelperService, OResponse, OSelect, OUserDetails, FilterHelperService } from '../../../service/service';
import { InputFileComponent, InputFile } from 'ngx-input-file';
declare let $: any;

@Component({
  selector: "tu-home",
  templateUrl: "./home.component.html"
})
export class TUHomeComponent implements OnInit {
  isLoaded: boolean = true;

  public _CashierAddress: any = {};
  ToggleStoreSelect: number;
  showStorePicker: boolean;

  @ViewChild("inputfile")
  private InputFileComponent: InputFileComponent;

  CurrentImagesCount: number;




  //#endregion

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {

  }


  ngOnInit() {

  }


}


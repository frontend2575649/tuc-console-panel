import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { ImageCropperModule } from "ngx-image-cropper";
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { TUHomeComponent } from "./home.component";
import { InputFileConfig, InputFileModule } from 'ngx-input-file';

const config: InputFileConfig = {
    fileAccept: '*',
    fileLimit: 1,
};

const routes: Routes = [
    {
        path: "",
        component: TUHomeComponent,
        children: [
            //
            { path: '', data: { 'permission': 'salehistory', PageName: 'System.Menu.GiftCard' }, loadChildren: "../../../merchant/giftcard/Reward/tusale.module#TUSaleModule" },
            { path: 'rewardhistory/:referencekey/:referenceid', data: { 'permission': 'giftcard', PageName: 'System.Menu.GiftCard' }, loadChildren: '../../../merchant/giftcard/Reward/tusale.module#TUSaleModule' },
            { path: 'redeemhistory/:referencekey/:referenceid', data: { 'permission': 'giftcard', PageName: 'System.Menu.GiftCard' }, loadChildren: '../../../merchant/giftcard/Redeemed/tusale.module#TUSaleModule' },
            { path: 'balancehistory/:referencekey/:referenceid', data: { 'permission': 'giftcard', PageName: 'System.Menu.GiftCard' }, loadChildren: '../../../merchant/giftcard/Balance/tusale.module#TUSaleModule' },

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUHomeRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUHomeRoutingModule,
        GooglePlaceModule,
        ImageCropperModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        InputFileModule.forRoot(config),
    ],
    declarations: [TUHomeComponent]
})
export class TUHomeModule { }

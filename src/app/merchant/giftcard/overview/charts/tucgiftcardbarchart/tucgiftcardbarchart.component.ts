import { Component, OnInit, ViewChild } from "@angular/core";
import { ChartConfiguration } from "chart.js";
import { BaseChartDirective } from "ng2-charts";
import { HelperService } from "src/app/service/helper.service";

@Component({
    selector: "tugiftcardbarchart",
    templateUrl: "./tucgiftcardbarchart.component.html",
    styles: [
        `
      agm-map {
        height: 300px;
      }
    `
    ]
})
export class TuGiftBarChartComponent implements OnInit {

    @ViewChild(BaseChartDirective) chart: BaseChartDirective | undefined;
    
    constructor(
        public _HelperService: HelperService,
    ){

    }

    ngOnInit(): void {
    }
  
    public barChartData: any = {
      labels: [ '2006', '2007', '2008', '2009', '2010', '2011', '2012' ],
      datasets: [
        { data: [ 65, 59, 80, 81, 56, 55, 40 ], label: 'Series A' },
        { data: [ 28, 48, 40, 19, 86, 27, 90 ], label: 'Series B' }
      ]
    };
  
}
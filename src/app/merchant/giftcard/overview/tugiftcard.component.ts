import { Component, OnInit, ViewChildren, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseChartDirective, Color } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OList, OResponse, OSelect, OSalesTrend, OSalesTrendData, OSalesTrendDataHourly } from '../../../service/service';
import * as Feather from 'feather-icons';
import { ChartDataSets } from 'chart.js';
declare var moment: any;
import * as cloneDeep from 'lodash/cloneDeep';
declare var $: any;
import swal from 'sweetalert2';
import { GiftCardCount, GiftCardCounts, GiftCardOverview } from './tugiftcard';

@Component({
    selector: "tugiftcard",
    templateUrl: "./tugiftcard.component.html",
    styles: [
        `
      agm-map {
        height: 300px;
      }
    `
    ]
})
export class TuCardComponent implements OnInit {

    lastdaytext = "LAST DAY";
    lastweektext = "LAST WEEK";
    lastweekCustom = "LAST WEEK";
    lastmonthtext = "LAST MONTH";
    lastyeartext = "LAST YEAR";
    Rewardlastyeartext = "LAST YEAR";

    Types: any = {
        year: 'year',
        month: 'month',
        week: 'week',
        day: 'day',
        hour: 'hour'
    }

    public lineChartData: ChartDataSets[] = [
        { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
        { data: [87, 44, 22, 55, 77, 333, 222], borderDash: [10, 5], label: 'Series B' },

    ];
    public LineChartYearlyLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
    public lineChartYearlyData: ChartDataSets[] = [
        { data: [65, 59, 80, 81, 56, 55, 40, 80, 81, 56, 55, 40] },
        { data: [87, 44, 22, 55, 77, 333, 222, 80, 181, 256, 155, 140], borderDash: [10, 5] },

    ];
    public LineWeelyChartLabels = ['MON', 'TUE', 'WED', 'THUS', 'FRI', 'SAT', 'SUN'];
    public lineChartWeeklyData: ChartDataSets[] = [
        { data: [65, 59, 80, 81, 56, 55, 40] },
        { data: [87, 44, 22, 55, 77, 333, 222], borderDash: [10, 5] },

    ];
    public lineChartColors: Color[] = [
        {
            borderColor: '#0168fa',
            backgroundColor: 'rgba(1, 104, 250, 0.10)',
        },
        {
            borderColor: '#FFC20A',
            backgroundColor: 'rgba(255, 194, 10, 00)',
        }
    ]

    public isMerchantGC: boolean;
    public giftCardCounts: GiftCardCount[] = GiftCardCounts;
    giftCardOverviewModel: GiftCardOverview = new GiftCardOverview();


    @ViewChildren(BaseChartDirective) components: BaseChartDirective[];
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {
        this.isMerchantGC = !this._Router.url.includes('/console/giftcards');
        if (this.isMerchantGC) {
            this.pData = {
                Task: 'getgiftcardssaleshistory',
                StartDate: null,
                EndDate: null,
                AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
                AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
                StoreReferenceId: 0,
                StoreReferenceKey: null,
                Type: null
            };
        } else {
            this.pData = {
                Task: 'gethomegiftcardssaleshistory',
                StartDate: null,
                EndDate: null,
                Type: null
            };
        }
    }

    //#endregion
    ngOnInit() {

        this.initializeDatePicker('dailydate', this._HelperService.AppConfig.DatePickerTypes.hour, 'TodaySalesTrend_dropdown');
        this.initializeDatePicker('weeklydate', this._HelperService.AppConfig.DatePickerTypes.week, 'WeekSalesTrend_dropdown');
        this.initializeDatePicker('monthlydate', this._HelperService.AppConfig.DatePickerTypes.month, 'MonthSalesTrend_dropdown');
        this.initializeDatePicker('yearlydate', this._HelperService.AppConfig.DatePickerTypes.year, 'YearSalesTrend_dropdown');
        this.initializeDatePicker('yearlydate1', this._HelperService.AppConfig.DatePickerTypes.Rewardyear, 'YearSalesTrend_dropdown1');
        this._HelperService.FullContainer = false;
        Feather.replace();
        //start time and end time for overview
        this.TUTr_Filter_Stores_Load();
        this.InitializeDates();
        this.LoadData();
        this.Form_AddUser_Load();
        this.GetGiftPointBalance();
        // this.GetGiftPointBalance();
        // document.getElementById("salesReward").hidden = true;
    }

    LoadData() {
        this._WeeklySalesReportGetActualData();
        this._DailySalesReportGetActualData();
        this._MonthlySalesReportGetActualData();
        this._YearlySalesReportGetActualData();
        this._RewardYearlySalesReportGetActualData();
        // this.GetGiftPointBalance();

        if (this.isMerchantGC) {
            this.GetGiftCardOverview();
        } else {
            this.GetGiftCardHomeOverview(this._HelperService.DateInUTC(this._Yearly.ActualStartDate), this._HelperService.DateInUTC(this._Yearly.ActualEndDate));
        }


    }

    InitializeDates(): void {

        //#region Daily Dates 

        this._Daily.ActualStartDate = moment().startOf('day');
        this._Daily.ActualEndDate = moment().endOf('day');

        this._Daily.CompareStartDate = moment().subtract(1, 'day').startOf('day');
        this._Daily.CompareEndDate = moment().subtract(1, 'day').endOf('day');

        //#endregion

        //#region Weekly Dates 

        this._Weekly.ActualStartDate = moment().startOf('week').startOf('day');
        this._Weekly.ActualEndDate = moment().endOf('day');

        this._Weekly.CompareStartDate = moment().subtract(1, 'week').startOf('week').startOf('day');
        this._Weekly.CompareEndDate = moment().subtract(1, 'week').endOf('week').endOf('day');

        //#endregion

        //#region Monthly Dates 

        this._Monthly.ActualStartDate = moment().startOf('month').startOf('day');
        this._Monthly.ActualEndDate = moment().endOf('month').endOf('day');

        this._Monthly.CompareStartDate = moment().subtract(1, 'months').startOf('month').startOf('day');
        this._Monthly.CompareEndDate = moment().subtract(1, 'months').endOf('month').endOf('day');

        this.MonthlybarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Monthly.ActualStartDate), moment(this._Monthly.ActualEndDate));

        //#endregion

        //#region Yearly Dates 

        this._Yearly.ActualStartDate = moment().startOf('year').startOf('day');
        this._Yearly.ActualEndDate = moment().endOf('year').endOf('day');

        this._Yearly.CompareStartDate = moment().subtract(1, 'year').startOf('year').startOf('day');
        this._Yearly.CompareEndDate = moment().subtract(1, 'year').endOf('year').endOf('day');

        this._RewardYearly.ActualStartDate = moment().startOf('year').startOf('day');
        this._RewardYearly.ActualEndDate = moment().endOf('year').endOf('day');

        this._RewardYearly.CompareStartDate = moment().subtract(1, 'year').startOf('year').startOf('day');
        this._RewardYearly.CompareEndDate = moment().subtract(1, 'year').endOf('year').endOf('day');

        //#endregion

    }

    public DateSelected = moment().startOf("day");
    //#region DateChangeHandler
    DateChanged(event: any, Type: any): void {
        var ev: any = cloneDeep(event);

        this.GetGiftCardHomeOverview(moment(ev.start), moment(ev.end))

        this.DateSelected = moment(ev.start).startOf("day");

        //#region Daily

        this._Daily.ActualStartDate = moment(ev.start).startOf("day");
        this._Daily.ActualEndDate = moment(ev.end).endOf("day");

        this._Daily.CompareStartDate = moment(ev.start).subtract(1, 'day').startOf("day");
        this._Daily.CompareEndDate = moment(ev.end).subtract(1, 'day').endOf("day");

        //#endregion
        //#region Week 

        this._Weekly.ActualStartDate = moment(ev.start).startOf('week').startOf('day');
        this._Weekly.ActualEndDate = moment(ev.end).endOf('week').endOf('day');

        this._Weekly.CompareStartDate = moment(ev.start).subtract(1, 'week').startOf('week').startOf('day');
        this._Weekly.CompareEndDate = moment(ev.end).subtract(1, 'week').endOf('week').endOf('day');

        //#endregion
        //#region Monthly 

        this._Monthly.ActualStartDate = moment(ev.start).startOf('month').startOf('day');
        this._Monthly.ActualEndDate = moment(event.end).endOf('month').endOf('day');

        this._Monthly.CompareStartDate = moment(ev.start).subtract(1, 'months').startOf('month').startOf('day');
        this._Monthly.CompareEndDate = moment(ev.end).subtract(1, 'months').endOf('month').endOf('day');

        this.MonthlybarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Monthly.ActualStartDate), moment(this._Monthly.ActualEndDate).endOf('month').endOf('day'));
        //#endregion
        //#region Yearly 

        this._Yearly.ActualStartDate = moment(ev.start).startOf('year').startOf('day');
        this._Yearly.ActualEndDate = moment(event.end).endOf('year').endOf('day');

        this._Yearly.CompareStartDate = moment(ev.start).subtract(1, 'year').startOf('year').startOf('day');
        this._Yearly.CompareEndDate = moment(ev.end).subtract(1, 'year').endOf('year').endOf('day');

        this._RewardYearly.ActualStartDate = moment(ev.start).startOf('year').startOf('day');
        this._RewardYearly.ActualEndDate = moment(event.end).endOf('year').endOf('day');

        this._RewardYearly.CompareStartDate = moment(ev.start).subtract(1, 'year').startOf('year').startOf('day');
        this._RewardYearly.CompareEndDate = moment(ev.end).subtract(1, 'year').endOf('year').endOf('day');

        //#endregion

        this._DailySalesReportGetActualData();
        this._WeeklySalesReportGetActualData();
        this._MonthlySalesReportGetActualData();
        this._YearlySalesReportGetActualData();

        this.RefreshDateLabels();

    }
    //#endregion

    //#region BarChartConfig 
    public Options: any = {
        cornerRadius: 20,
        responsive: true,
        legend: {
            display: false,
            position: 'right',
        },
        ticks: {
            autoSkip: false
        },
        scales: {
            xAxes: [
                {
                    gridLines: {
                        stacked: true,
                        display: false
                    },
                    ticks: {
                        autoSkip: false,
                        fontSize: 11
                    }
                }
            ],
            yAxes: [
                {

                    gridLines: {
                        stacked: true,
                        display: true
                    },
                    ticks: {
                        beginAtZero: true,
                        fontSize: 11
                    }
                }
            ]
        },
        annotation: {
            annotations: [{
                type: 'line',
                mode: 'horizontal',
                scaleID: 'y-axis-0',
                // value: 20,
                borderColor: 'rgb(75, 192, 192)',
                borderWidth: 4,
                label: {
                    enabled: false,
                    content: 'Test label'
                }
            }]
        },
        plugins: {
            datalabels: {
                backgroundColor: "#ffffff47",
                color: "#798086",
                borderRadius: "2",
                borderWidth: "1",
                borderColor: "transparent",
                anchor: "end",
                align: "end",
                padding: 2,
                font: {
                    size: 10,
                    weight: 500
                },
                formatter: (value, ctx) => {
                    const label = ctx.chart.data.labels[ctx.dataIndex];
                    if (label != undefined) {
                        return value;
                    } else {
                        return value;
                    }
                }
            }
        }
    }
    public barChartLabels = [];
    // public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
    public barChartColors = [{ backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }];
    public barChartType = 'bar';
    public barChartLegend = true;
    public barChartData = [
        { data: [200], label: 'Remote' },
        { data: [22], label: 'Remote' },
        { data: [22], label: 'Visit' },
        { data: [22], label: 'Visit' },
    ];

    //#endregion

    //#region MonthlyBarChartConfig 
    public MonthlyOptions: any = {
        cornerRadius: 20,
        responsive: true,
        legend: {
            display: false,
            position: 'right',
        },
        ticks: {
            autoSkip: false
        },
        scales: {
            xAxes: [
                {
                    gridLines: {
                        stacked: true,
                        display: false
                    },
                    ticks: {
                        autoSkip: false,
                        fontSize: 11
                    }
                }
            ],
            yAxes: [
                {

                    gridLines: {
                        stacked: true,
                        display: true
                    },
                    ticks: {
                        beginAtZero: true,
                        fontSize: 11
                    }
                }
            ]
        },
        annotation: {
            annotations: [{
                type: 'line',
                mode: 'horizontal',
                scaleID: 'y-axis-0',
                // value: 20,
                borderColor: 'rgb(75, 192, 192)',
                borderWidth: 4,
                label: {
                    enabled: false,
                    content: 'Test label'
                }
            }]
        },
        plugins: {
            datalabels: {
                backgroundColor: "#ffffff47",
                color: "#798086",
                borderRadius: "2",
                borderWidth: "1",
                borderColor: "transparent",
                anchor: "end",
                align: "end",
                padding: 2,
                font: {
                    size: 10,
                    weight: 500
                },
                formatter: (value, ctx) => {
                    const label = ctx.chart.data.labels[ctx.dataIndex];
                    if (label != undefined) {
                        return value;
                    } else {
                        return value;
                    }
                }
            }
        }
    }
    public MonthlybarChartLabels = [];
    // public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
    public MonthlybarChartColors = [{ backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }];
    public MonthlybarChartType = 'bar';
    public MonthlybarChartLegend = true;
    public MonthlybarChartData = [
        { data: [200], label: 'Remote' },
        { data: [22], label: 'Remote' },
        { data: [22], label: 'Visit' },
        { data: [22], label: 'Visit' },
    ];


    public MonthlylineChartData: ChartDataSets[] = [
        { data: [], label: 'Current Month' },
        { data: [], borderDash: [10, 5], label: 'Compared Month' },
    ];

    public MonthlylineChartColors: Color[] = [
        {
            borderColor: '#0168fa',
            backgroundColor: 'rgba(1, 104, 250, 0.10)',
        },
        {
            borderColor: '#FFC20A',
            backgroundColor: 'rgba(255, 194, 10, 00)',
        }
    ]

    //#endregion

    //#region Daily Sales Report 

    public DailylineChartData: ChartDataSets[] = [
        { data: [], label: 'Today' },
        { data: [], borderDash: [10, 5], label: 'Compared Day' },
    ];
    public DailylineChartColors: Color[] = [
        {
            borderColor: '#0168fa',
            backgroundColor: 'rgba(1, 104, 250, 0.10)',
        },
        {
            borderColor: '#FFC20A',
            backgroundColor: 'rgba(255, 194, 10, 00)',
        }
    ];
    public DailylineChartLabels = ['00:00', 'O1:00', '02:00', 'O3:00', '04:00', 'O5:00', '06:00', 'O7:00', '08:00', '09:00', '10:00', '11:00',
        '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
    showDailyChart = true;

    public _Daily: OSalesTrend = {
        ActualStartDate: moment(),
        ActualEndDate: moment(),
        ActualData: null,
        CompareStartDate: moment(),
        CompareEndDate: moment(),
        CompareData: null,
        ActualSalesAmount: 0,
        CompareSalesAmount: 0,
        SalesAmountDifference: 0
    }


    private RefreshDateLabels() {
        this.lastyeartext = this._HelperService.GetDateSByFormat(this._Yearly.CompareStartDate, 'YYYY');
        this.lastyeartext = this._HelperService.GetDateSByFormat(this._RewardYearly.CompareStartDate, 'YYYY');
        this.Rewardlastyeartext = this._HelperService.GetDateSByFormat(this._Yearly.CompareStartDate, 'YYYY');
        this.Rewardlastyeartext = this._HelperService.GetDateSByFormat(this._RewardYearly.CompareStartDate, 'YYYY');
        this.Rewardlastyeartext = moment(this._RewardYearly.CompareStartDate).format('YYYY');

        this.lastdaytext = this._HelperService.GetDateS(this._Daily.CompareStartDate);

        this.lastweektext = this._HelperService.GetDateSByFormat(this._Weekly.CompareStartDate, 'DD MMM YY')
            + "-" + this._HelperService.GetDateSByFormat(this._Weekly.CompareEndDate, 'DD MMM YY');

        this.lastmonthtext = this._HelperService.GetDateSByFormat(this._Monthly.CompareStartDate, 'MMM YYYY');
    }

    DailyDateChanged(event: any, Type: any): void {
        var ev: any = cloneDeep(event);
        this._Daily.CompareStartDate = moment(ev.start).startOf("day");
        this._Daily.CompareEndDate = moment(ev.end).endOf("day");

        this.lastdaytext = this._HelperService.GetDateS(this._Daily.CompareStartDate);
        this.DailylineChartData[1].label = this.lastdaytext;

        this.hideDailyPicker = true;
        this._DailySalesReportGetActualData();
    }

    public _DailySalesReportReset(): void {


    }

    public _DailySalesReportGetActualData(): void {

        this.DailylineChartData[0].data = [];
        this.DailylineChartData[1].data = [];
        this.GetSalesReportDaily(this._Daily.ActualStartDate, this._Daily.ActualEndDate, this._Daily.ActualData, this.Types.hour, 'actual');
        this.GetSalesReportDaily(this._Daily.CompareStartDate, this._Daily.CompareEndDate, this._Daily.CompareData, this.Types.hour, 'compare');

    }
    //#endregion

    //#region Weekly Sales Report 

    public WeeklylineChartData: ChartDataSets[] = [
        { data: [], label: 'Current Week' },
        { data: [], borderDash: [10, 5], label: 'Week Compared' },
    ];
    public WeeklylineChartColors: Color[] = [
        {
            borderColor: '#0168fa',
            backgroundColor: 'rgba(1, 104, 250, 0.10)',
        },
        {
            borderColor: '#FFC20A',
            backgroundColor: 'rgba(255, 194, 10, 00)',
        }
    ];
    public WeeklylineChartLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
    showWeeklyChart = true;

    public _Weekly: OSalesTrend = {
        ActualStartDate: moment(),
        ActualEndDate: moment(),
        ActualData: null,

        CompareStartDate: moment(),
        CompareEndDate: moment(),
        CompareData: null,

        ActualSalesAmount: 0,
        CompareSalesAmount: 0,
        SalesAmountDifference: 0
    }

    WeelyDateChanged(event: any, Type: any): void {

        var ev: any = cloneDeep(event);
        this._Weekly.CompareStartDate = moment(ev.start).startOf("week").startOf('day');
        this._Weekly.CompareEndDate = moment(ev.end).endOf("week").endOf('day');

        this.lastweektext = this._HelperService.GetDateSByFormat(this._Weekly.CompareStartDate, 'DD MMM YY')
            + "-" + this._HelperService.GetDateSByFormat(this._Weekly.CompareEndDate, 'DD MMM YY');
        this.WeeklylineChartData[1].label = this._HelperService.GetDateSByFormat(this._Weekly.CompareStartDate, 'DD MMM YY');
        this.lastweekCustom = this._HelperService.GetDateSByFormat(this._Weekly.CompareStartDate, 'DD MMM YYYY')
        this.lastweekCustom = 'Custom Week';

        this.hideWeeklyPicker = true;
        this._WeeklySalesReportGetActualData();
    }

    public _WeeklySalesReportReset(): void {


    }

    public _WeeklySalesReportGetActualData(): void {

        this.WeeklylineChartData[0].data = [];
        this.WeeklylineChartData[1].data = [];

        this.GetSalesReportWeekly(this._Weekly.ActualStartDate, this._Weekly.ActualEndDate, this._Weekly.ActualData, this.Types.week, 'actual');
        this.GetSalesReportWeekly(this._Weekly.CompareStartDate, this._Weekly.CompareEndDate, this._Weekly.CompareData, this.Types.week, 'compare');

    }
    //#endregion

    //#region Monthly Sales Report 

    showMonthlyChart = true;

    public _Monthly: OSalesTrend = {
        ActualStartDate: moment(),
        ActualEndDate: moment(),
        ActualData: null,

        CompareStartDate: moment(),
        CompareEndDate: moment(),
        CompareData: null,

        ActualSalesAmount: 0,
        CompareSalesAmount: 0,
        SalesAmountDifference: 0
    }

    MonthlyDateChanged(event: any, Type: any): void {
        var ev: any = cloneDeep(event);

        this._Monthly.CompareStartDate = moment(ev.start).startOf("month").startOf("day");
        this._Monthly.CompareEndDate = moment(ev.end).endOf("month").endOf("day");

        this.lastmonthtext = this._HelperService.GetDateSByFormat(this._Monthly.CompareStartDate, 'MMM YYYY');
        this.MonthlylineChartData[1].label = this._HelperService.GetDateSByFormat(this._Monthly.CompareStartDate, 'MMM YY');

        this.MonthlybarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Monthly.ActualStartDate), moment(this._Monthly.ActualEndDate).endOf('month').endOf('day'));


        this.hideMonthlyPicker = true;
        this._MonthlySalesReportGetActualData();
    }

    public _MonthlySalesReportReset(): void {


    }

    public _MonthlySalesReportGetActualData(): void {

        this.MonthlylineChartData[0].data = [];
        this.MonthlylineChartData[1].data = [];

        this._Monthly.ActualData = this.GetSalesReport(this._Monthly.ActualStartDate, this._Monthly.ActualEndDate, this._Monthly.ActualData, this.Types.month, 'actual');
        this._Monthly.CompareData = this.GetSalesReport(this._Monthly.CompareStartDate, this._Monthly.CompareEndDate, this._Monthly.CompareData, this.Types.month, 'compare');

    }
    //#endregion

    //#region Yearly Sales Report 

    public YearlylineChartData: ChartDataSets[] = [
        { data: [], label: 'Current Year' },
        { data: [], borderDash: [10, 5], label: 'Compared Year' },
    ];
    public YearlylineChartColors: Color[] = [
        {
            borderColor: '#0168fa',
            backgroundColor: 'rgba(1, 104, 250, 0.10)',
        },
        {
            borderColor: '#FFC20A',
            backgroundColor: 'rgba(255, 194, 10, 00)',
        }
    ];
    public YearlylineChartLabels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'];
    showYearlyChart = true;

    public _RewardYearly: OSalesTrend = {
        ActualStartDate: moment(),
        ActualEndDate: moment(),
        ActualData: null,

        CompareStartDate: moment(),
        CompareEndDate: moment(),
        CompareData: null,

        ActualSalesAmount: 0,
        CompareSalesAmount: 0,
        SalesAmountDifference: 0
    }
    public _Yearly: OSalesTrend = {
        ActualStartDate: moment(),
        ActualEndDate: moment(),
        ActualData: null,

        CompareStartDate: moment(),
        CompareEndDate: moment(),
        CompareData: null,

        ActualSalesAmount: 0,
        CompareSalesAmount: 0,
        SalesAmountDifference: 0
    }

    YearlyDateChanged(event: any, Type: any): void {

        var ev: any = cloneDeep(event);
        this._Yearly.CompareStartDate = moment(ev.start).startOf("year").startOf('day');
        this._Yearly.CompareEndDate = moment(ev.end).endOf("year").endOf('day');

        this.lastyeartext = this._HelperService.GetDateSByFormat(this._Yearly.CompareStartDate, 'YYYY');

        this.YearlylineChartData[1].label = this._HelperService.GetDateSByFormat(this._Yearly.CompareStartDate, 'YYYY');

        this.hideYearlyPicker = true;
        this._YearlySalesReportGetActualData();

    }
    RewardYearlyDateChanged(event: any, Type: any): void {

        var ev: any = cloneDeep(event);

        this._RewardYearly.CompareStartDate = moment(ev.start).startOf("year").startOf('day');
        this._RewardYearly.CompareEndDate = moment(ev.end).endOf("year").endOf('day');
        // this.Rewardlastyeartext = this._HelperService.GetDateSByFormat(this._RewardYearly.CompareStartDate, 'YYYY');
        this.Rewardlastyeartext = moment(this._RewardYearly.CompareStartDate).format('YYYY');
        this.YearlylineChartData[1].label = this._HelperService.GetDateSByFormat(this._RewardYearly.CompareStartDate, 'YYYY');
        this.hideYearlyPicker = true;
        this._RewardYearlySalesReportGetActualData();
    }


    public _YearlySalesReportReset(): void {


    }

    public _YearlySalesReportGetActualData(): void {
        this.YearlylineChartData[0].data = [];
        this.YearlylineChartData[1].data = [];
        this._Yearly.ActualData = this.GetSalesReport(this._Yearly.ActualStartDate, this._Yearly.ActualEndDate, this._Yearly.ActualData, this.Types.year, 'actual');
        this._Yearly.CompareData = this.GetSalesReport(this._Yearly.CompareStartDate, this._Yearly.CompareEndDate, this._Yearly.CompareData, this.Types.year, 'compare');

    }

    public _RewardYearlySalesReportGetActualData(): void {
        this.YearlylineChartData[0].data = [];
        this.YearlylineChartData[1].data = [];
        this._RewardYearly.ActualData = this.GetSalesReport(this._RewardYearly.ActualStartDate, this._RewardYearly.ActualEndDate, this._RewardYearly.ActualData, this.Types.year, 'actual');
        this._RewardYearly.CompareData = this.GetSalesReport(this._RewardYearly.CompareStartDate, this._RewardYearly.CompareEndDate, this._RewardYearly.CompareData, this.Types.year, 'compare');
    }

    //#endregion

    //#region Sales History General Method 

    private pData: any = {};


    GetSalesReport(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {


        this._HelperService.IsFormProcessing = true;

        this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
        this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);
        this.pData.Type = Type;

        if (Type == this.Types.month) {
            this.pData.Type = this.Types.day;
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.GC, this.pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    Data = _Response.Result as OSalesTrendData[];

                    var TempArray = [];
                    var SalesAmount = 0;

                    if (Type == this._HelperService.AppConfig.GraphTypes.month) {
                        var MonthAllDays: any[] = this._HelperService.CalculateIntermediateDate(cloneDeep(StartDateTime),
                            cloneDeep(EndDateTime));
                        for (let index = 0; index < MonthAllDays.length; index++) {
                            const element = MonthAllDays[index];
                            var RData: OSalesTrendData = Data.find(x => moment(x.Date, 'DD-MM-YYYY').format('DD MMM') == element);
                            if (RData) {
                                TempArray[index] = RData.TotalInvoiceAmount;
                                SalesAmount = SalesAmount + RData.TotalInvoiceAmount;
                            } else {
                                TempArray[index] = 0;
                            }
                        }
                    } else {
                        for (let index = 0; index < 12; index++) {
                            var RData: OSalesTrendData = Data.find(x => x['Month'] == index + 1);
                            if (RData) {
                                TempArray[index] = RData.TotalInvoiceAmount;
                                SalesAmount = SalesAmount + RData.TotalInvoiceAmount;
                            } else {
                                TempArray[index] = 0;
                            }
                        }

                        // for (let index = 0; index < Data.length; index++) {
                        //   const element: OSalesTrendData = Data[index];
                        //   TempArray.push(element.TotalInvoiceAmount);

                        //   SalesAmount = SalesAmount + element.TotalInvoiceAmount;
                        // }
                    }

                    if (Type == this._HelperService.AppConfig.GraphTypes.month) {
                        this.showMonthlyChart = false;
                        this._ChangeDetectorRef.detectChanges();

                        if (LineType == 'actual') {
                            this.MonthlylineChartData[0].data = TempArray;
                            this._Monthly.ActualSalesAmount = SalesAmount;
                        } else if (LineType == 'compare') {
                            this.MonthlylineChartData[1].data = TempArray;
                            this._Monthly.CompareSalesAmount = SalesAmount;
                        }

                        this.showMonthlyChart = true;
                        this._ChangeDetectorRef.detectChanges();
                    } else if (Type == this._HelperService.AppConfig.GraphTypes.hour) {
                        this.showDailyChart = false;
                        this._ChangeDetectorRef.detectChanges();

                        if (LineType == 'actual') {
                            this.DailylineChartData[0].data = TempArray;
                            this._Daily.ActualSalesAmount = SalesAmount;
                        } else if (LineType == 'compare') {
                            this.DailylineChartData[1].data = TempArray;
                            this._Daily.CompareSalesAmount = SalesAmount;
                        }

                        this.showDailyChart = true;
                        this._ChangeDetectorRef.detectChanges();
                    } else if (Type == this._HelperService.AppConfig.GraphTypes.week) {
                        this.showWeeklyChart = false;
                        this._ChangeDetectorRef.detectChanges();

                        if (LineType == 'actual') {
                            this.WeeklylineChartData[0].data = TempArray;
                            this._Weekly.ActualSalesAmount = SalesAmount;
                        } else if (LineType == 'compare') {
                            this.WeeklylineChartData[1].data = TempArray;
                            this._Weekly.CompareSalesAmount = SalesAmount;
                        }

                        this.showWeeklyChart = true;
                        this._ChangeDetectorRef.detectChanges();
                    } else if (Type == this._HelperService.AppConfig.GraphTypes.year) {
                        this.showYearlyChart = false;
                        this._ChangeDetectorRef.detectChanges();

                        if (LineType == 'actual') {
                            this.YearlylineChartData[0].data = TempArray;
                            this._Yearly.ActualSalesAmount = SalesAmount;
                            this._RewardYearly.ActualSalesAmount = SalesAmount;

                        } else if (LineType == 'compare') {
                            this.YearlylineChartData[1].data = TempArray;
                            this._Yearly.CompareSalesAmount = SalesAmount;
                            this._RewardYearly.CompareSalesAmount = SalesAmount;

                        }

                        this.showYearlyChart = true;
                        this._ChangeDetectorRef.detectChanges();
                    }

                    return Data;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                    return Data;
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                return Data;
            });
    }

    GetSalesReportDaily(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {

        this._HelperService.IsFormProcessing = true;

        this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
        this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);


        this.pData.Type = this.Types.hour;
        // console.log(this.pData);


        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.GC, this.pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {

                    Data = _Response.Result as OSalesTrendData[];

                    var TempArray = [];
                    var SalesAmount = 0;
                    var Heigest: OSalesTrendDataHourly = {
                        Hour: null,
                        HourAmPm: '0:00 AM',
                        HourAmPmNext: '1:00 AM',
                        TotalTransaction: 0.0,
                        TotalInvoiceAmount: 0.0
                    };

                    var Lowest: OSalesTrendDataHourly = {
                        Hour: null,
                        HourAmPm: '0:00 AM',
                        HourAmPmNext: '1:00 AM',
                        TotalTransaction: 0.0,
                        TotalInvoiceAmount: 0.0
                    };

                    var DataHourly = _Response.Result as OSalesTrendDataHourly[];
                    for (let index = 0; index < 24; index++) {
                        var RData: OSalesTrendDataHourly = DataHourly.find(x => x.Hour == index);
                        if (RData != undefined && RData != null) {

                            TempArray.push(RData.TotalInvoiceAmount);

                            const element: OSalesTrendDataHourly = RData;
                            if (Heigest.TotalInvoiceAmount <= element.TotalInvoiceAmount) {
                                Heigest = element;

                                //#region current 

                                var dd = " AM";
                                var h = index;
                                if (h >= 12) {
                                    h = index - 12;
                                    dd = " PM";
                                }
                                if (h == 0) {
                                    h = 12;
                                }
                                var Hour = h + ":00" + dd;
                                Heigest.HourAmPm = Hour;

                                //#endregion

                                //#region next 

                                var dd = " AM";
                                var h = index + 1;
                                if (h >= 12) {
                                    h = index - 12;
                                    dd = " PM";
                                }
                                if (h == 0) {
                                    h = 12;
                                }
                                var Hour2 = h + ":00" + dd;
                                Heigest.HourAmPmNext = Hour2;

                                //#endregion
                            }

                            if (Lowest.TotalInvoiceAmount >= element.TotalInvoiceAmount) {
                                Lowest = element;

                                //#region current 

                                var dd = " AM";
                                var h = index;
                                if (h >= 12) {
                                    h = index - 12;
                                    dd = " PM";
                                }
                                if (h == 0) {
                                    h = 12;
                                }
                                var Hour = h + ':00' + dd;
                                Lowest.HourAmPm = Hour;

                                //#endregion


                                //#region next 

                                var dd = " AM";
                                var h = index + 1;
                                if (h >= 12) {
                                    h = index - 12;
                                    dd = " PM";
                                }
                                if (h == 0) {
                                    h = 12;
                                }
                                var Hour2 = h + ':00' + dd;
                                Lowest.HourAmPmNext = Hour2;

                                //#endregion
                            }
                            SalesAmount = SalesAmount + element.TotalInvoiceAmount;
                        }
                        else {
                            TempArray.push(0);
                        }

                    }



                    if (Type == this._HelperService.AppConfig.GraphTypes.hour) {

                        this.showDailyChart = false;
                        this._ChangeDetectorRef.detectChanges();

                        if (LineType == 'actual') {
                            this.DailylineChartData[0].data = TempArray;
                            this._Daily.ActualSalesAmount = SalesAmount;
                        } else if (LineType == 'compare') {
                            this.DailylineChartData[1].data = TempArray;
                            this._Daily.CompareSalesAmount = SalesAmount;
                        }

                        this.showDailyChart = true;
                        this._ChangeDetectorRef.detectChanges();
                    }

                    return Data;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                    return Data;
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                return Data;
            });
    }

    GetSalesReportWeekly(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {

        this._HelperService.IsFormProcessing = true;

        this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
        this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);

        this.pData.Type = Type;

        if (Type == this.Types.day) {
            this.pData.Type = 'week';
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.GC, this.pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    Data = _Response.Result as OSalesTrendData[];

                    var TempArray = [];
                    var SalesAmount = 0;

                    var TempColorArray = ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'];

                    var Heigest = Data[0];
                    var HeigestIndex = 0;
                    var Lowest = Data[0];
                    var LowestIndex = 0;

                    var DataDaily = _Response.Result as OSalesTrendData[];

                    for (let index = 0; index < 7; index++) {

                        var weekday = '';
                        switch (index) {
                            case 0: weekday = 'Sunday';
                                break;
                            case 1: weekday = 'Monday';

                                break;
                            case 2: weekday = 'Tuesday';

                                break;
                            case 3: weekday = 'Wednesday';

                                break;
                            case 4: weekday = 'Thursday';

                                break;
                            case 5: weekday = 'Friday';

                                break;
                            case 6: weekday = 'Saturday';
                                break;

                            default:
                                break;
                        }

                        var RData: OSalesTrendData = DataDaily.find(x => x['WeekDay'] == weekday);
                        if (RData != undefined && RData != null) {

                            TempArray.push(RData.TotalInvoiceAmount);

                            const element: OSalesTrendData = RData;

                            SalesAmount = SalesAmount + element.TotalInvoiceAmount;


                            if (Heigest.TotalInvoiceAmount < element.TotalInvoiceAmount) {
                                Heigest = element;
                                HeigestIndex = index;
                            }

                            if (Lowest.TotalInvoiceAmount > element.TotalInvoiceAmount) {
                                Lowest = element;
                                LowestIndex = index;
                            }

                            SalesAmount = SalesAmount + element.TotalInvoiceAmount;
                        }
                        else {
                            TempArray.push(0);
                        }

                    }

                    if (Type == this._HelperService.AppConfig.GraphTypes.week) {
                        this.showWeeklyChart = false;
                        this._ChangeDetectorRef.detectChanges();

                        if (LineType == 'actual') {
                            this.WeeklylineChartData[0].data = TempArray;
                            this._Weekly.ActualSalesAmount = SalesAmount;
                        } else if (LineType == 'compare') {
                            this.WeeklylineChartData[1].data = TempArray;
                            this._Weekly.CompareSalesAmount = SalesAmount;
                        }

                        this.showWeeklyChart = true;
                        this._ChangeDetectorRef.detectChanges();
                    }


                    return Data;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                    return Data;
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                return Data;
            });
    }

    //#endregion

    //#region Store Filter
    public TUTr_Filter_Store_Selected: any;
    public ToggleStoreSelect: boolean = false;
    public TUTr_Filter_Store_Option: Select2Options;
    TUTr_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceKey",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Store
                }
            ]
        };

        var OwnerKey = this._HelperService.UserAccount.AccountId;
        if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
            OwnerKey = this._HelperService.AppConfig.ActiveMerchantReferenceId;
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, OwnerKey, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Store_Option = {
            placeholder: 'Select Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_Stores_Change(event: any) {
        if (event.value == this.TUTr_Filter_Store_Selected) {
            // this.pData.StoreReferenceId = 0;
            // this.pData.StoreReferenceKey = null;
            this.TUTr_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Store_Selected) {
            // this.pData.StoreReferenceId = event.data[0].ReferenceId;
            // this.pData.StoreReferenceKey = event.data[0].ReferenceKey;

            this.TUTr_Filter_Store_Selected = event.value;
        }

        this.LoadData();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }
    //#endregion


    computePerc(num: OSalesTrend): any {
        if (num.CompareSalesAmount == 0) {
            return '100 %';
        }
        if (num.ActualSalesAmount > num.CompareSalesAmount) {
            return Math.round(((num.ActualSalesAmount - num.CompareSalesAmount) / num.CompareSalesAmount) * 100) + ' %';
        } else if (num.ActualSalesAmount < num.CompareSalesAmount) {
            return Math.round(((num.CompareSalesAmount - num.ActualSalesAmount) / num.CompareSalesAmount) * 100) + ' %';
        } else {
            return '0 %';
        }
    }

    computeFlag(num: OSalesTrend): number {
        if (num.ActualSalesAmount > num.CompareSalesAmount) {
            return 1;
        } else if (num.ActualSalesAmount < num.CompareSalesAmount) {
            return -1;
        } else {
            return 0;
        }
    }

    getAbsolute(num: number): any {
        // return Math.abs(num);
        return num;
    }

    CloseRowModal(index: number): void {

        var Daate = cloneDeep(this.DateSelected);

        switch (index) {
            case 0: {
                var ev: any = {
                    start: moment(Daate).subtract(1, 'week').startOf('day'),
                    end: moment(Daate).subtract(1, 'week').endOf('day')
                };
                this.DailyDateChanged(ev, '');
                $("#TodaySalesTrend_dropdown").dropdown('toggle');
            }

                break;
            case 1: {
                var ev: any = {
                    start: moment(Daate).subtract(1, 'month').startOf('week').startOf('day'),
                    end: moment(Daate).subtract(1, 'month').endOf('week').endOf('day')
                };
                this.WeelyDateChanged(ev, '');
                $("#WeekSalesTrend_dropdown").dropdown('toggle');
            }

                break;
            case 2: {
                var ev: any = {
                    start: moment(Daate).subtract(1, 'year').startOf('month').startOf('day'),
                    end: moment(Daate).subtract(1, 'year').endOf('month').endOf('day')
                };
                this.MonthlyDateChanged(ev, '');
                $("#MonthSalesTrend_dropdown").dropdown('toggle');

            }

                break;
            case 3: {
                var ev: any = {
                    start: moment(Daate).subtract(1, 'year').startOf('year').startOf('day'),
                    end: moment(Daate).subtract(1, 'year').endOf('year').endOf('day')
                };
                this.YearlyDateChanged(ev, '');
                $("#YearSalesTrend_dropdown").dropdown('toggle');
                this.Rewardlastyeartext = "LAST YEAR";
            }

                break;
            case 4: {
                var ev: any = {
                    start: moment(Daate).subtract(1, 'year').startOf('year').startOf('day'),
                    end: moment(Daate).subtract(1, 'year').endOf('year').endOf('day')
                };
                this.RewardYearlyDateChanged(ev, '');
                $("#YearSalesTrend_dropdown1").dropdown('toggle');
            }

                break;


            default:
                break;
        }

    }

    initializeDatePicker(pickerId: string, type: string, dropdownId: string): void {
        var i = '#' + pickerId;
        var picker = "#" + dropdownId;


        if (type == this._HelperService.AppConfig.DatePickerTypes.month) {
            $(i).datepicker(
                {
                    viewMode: "months",
                    minViewMode: "months"
                }
            );
        }
        else if (type == this._HelperService.AppConfig.DatePickerTypes.year) {
            $(i).datepicker(
                {
                    viewMode: "years",
                    minViewMode: "years"
                }
            );
        }

        else if (type == this._HelperService.AppConfig.DatePickerTypes.Rewardyear) {
            $(i).datepicker(
                {
                    viewMode: "years",
                    minViewMode: "years",
                    endDate: '-1y'
                }
            );
        }


        else {
            $(i).datepicker();
        }

        $(i).on('changeDate', () => {
            switch (type) {
                case this._HelperService.AppConfig.DatePickerTypes.hour:
                    {
                        this.DailyDateChanged({
                            start: $(i).datepicker("getDate"),
                            end: $(i).datepicker("getDate")
                        }, '');
                        $(picker).dropdown('toggle');
                    }
                    break;

                case this._HelperService.AppConfig.DatePickerTypes.week:
                    {
                        this.WeelyDateChanged({
                            start: $(i).datepicker("getDate"),
                            end: $(i).datepicker("getDate")
                        }, '');
                        $(picker).dropdown('toggle');
                    }
                    break;

                case this._HelperService.AppConfig.DatePickerTypes.month:
                    {
                        this.MonthlyDateChanged({
                            start: $(i).datepicker("getDate"),
                            end: $(i).datepicker("getDate")
                        }, '');
                        $(picker).dropdown('toggle');
                    }
                    break;
                case this._HelperService.AppConfig.DatePickerTypes.year:
                    {
                        this.YearlyDateChanged({
                            start: $(i).datepicker("getDate"),
                            end: $(i).datepicker("getDate")
                        }, '');
                        $(picker).dropdown('toggle');
                    }
                    break;
                case this._HelperService.AppConfig.DatePickerTypes.Rewardyear:
                    {
                        this.RewardYearlyDateChanged({
                            start: $(i).datepicker("getDate"),
                            end: $(i).datepicker("getDate")
                        }, '');
                        $(picker).dropdown('toggle');
                    }
                    break;

                default:
                    break;
            }
        });
    }

    hideDailyPicker: boolean = true;
    hideWeeklyPicker: boolean = true;
    hideMonthlyPicker: boolean = true;
    hideYearlyPicker: boolean = true;

    ShowHideCalendar(type: string) {
        switch (type) {
            case this._HelperService.AppConfig.DatePickerTypes.hour: {
                this.hideDailyPicker = !this.hideDailyPicker;
            }

                break;
            case this._HelperService.AppConfig.DatePickerTypes.week: {
                this.hideWeeklyPicker = !this.hideWeeklyPicker;
            }

                break;
            case this._HelperService.AppConfig.DatePickerTypes.month: {
                this.hideMonthlyPicker = !this.hideMonthlyPicker;
            }

                break;
            case this._HelperService.AppConfig.DatePickerTypes.year: {
                this.hideYearlyPicker = !this.hideYearlyPicker;
            }

                break;

            default:
                break;
        }
    }
    public _TransactionReference = null;
    public _AccountBalanceCreditAmount = 0;
    generateRandomNumber() {
        var TrRef = Math.floor(100000 + Math.random() * (999999 + 1 - 100000));
        return TrRef;
    }
    OpenPaymentOptions() {
        this._AccountBalanceCreditAmount = 0;
        var Ref = this.generateRandomNumber();
        this._TransactionReference = 'GC' + '_' + this._HelperService.AppConfig.ActiveMerchantReferenceKey + '_' + Ref;
        this._HelperService.OpenModal('Form_AddUser_ContentGC');
    }
    paymentDone(ref: any) {
        this.CreditAmount();
        // this.TransactionId = ref.trans
        // if (ref != undefined && ref != null && ref != '') {
        //     if (ref.status == 'success') {
        //         this.CreditAmount();
        //     }
        //     else {
        //         this._HelperService.NotifyError('Payment failed');
        //     }
        // }
        // else {
        //     this._HelperService.NotifyError('Payment failed');
        // }
        // this.title = 'Payment successfull';
    }
    public TransactionId
    public CreditAmount() {
        this._HelperService.IsFormProcessing = true;
        var PostData = {
            Task: "creditaccount",
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            Amount: this._AccountBalanceCreditAmount,
            PaymentReference: this._TransactionReference,
            TransactionReference: this.TransactionId,
            SourceCode: 'transaction.source.giftcards',
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.PlOps, PostData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._AccountBalanceCreditAmount = 0;
                    this._TransactionReference = null;
                    this._HelperService.CloseModal("Form_AddUser_ContentGC");
                    this._HelperService.NotifySuccess("Account credited");
                    this.GetGiftPointBalance();


                } else {
                    this._HelperService.NotifySuccess(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            }
        );
    }
    paymentCancel() {
    }

    Form_AddUser: FormGroup;
    Form_AddUser_Show() {
        this._HelperService.OpenModal("GiftAway");
    }
    Form_AddUser_Close() {
        // this._Router.navigate([
        //     this._HelperService.AppConfig.Pages.System.AdminUsers
        // ]);
        this._HelperService.CloseModal("GiftAway");
    }
    Form_AddUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_AddUser = this._FormBuilder.group({
            OperationType: "new",
            Task: this._HelperService.AppConfig.Api.ThankUCash.createquickgiftcard,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            StatusCode: this._HelperService.AppConfig.Status.Active,
            ConfigurationKey: this._HelperService.AppConfig.ConfigurationKey,
            GiftMobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            GiftAmount: [null, Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(6)])],
            Comment: [null, Validators.required],
        });
    }
    Form_AddUser_Clear() {
        this.Form_AddUser.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_AddUser_Process(_FormValue: any) {
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        if (_FormValue.GiftAmount < 1) {
            this._HelperService.NotifyError('Amount must be greater than 0');
        }
        else if (_FormValue.GiftMobileNumber == undefined || _FormValue.GiftMobileNumber == null || _FormValue.GiftMobileNumber == "") {
            this._HelperService.NotifyError('Enter mobile number to giveout gift points');
        }
        else {
            _FormValue.Amount = _FormValue.GiftAmount;
            _FormValue.Customer =
            {
                MobileNumber: _FormValue.GiftMobileNumber,
                Name: _FormValue.Name,
            }
            swal({
                title: "Giveout " + _FormValue.GiftAmount + " to  " + _FormValue.GiftMobileNumber + " ?",
                text: "Click on continue to giveout amount",
                position: 'center',
                animation: this._HelperService.AppConfig.Alert_AllowAnimation,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
                allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
                showCancelButton: true,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
            }).then((result) => {
                if (result.value) {
                    _OResponse = this._HelperService.PostData(
                        this._HelperService.AppConfig.NetworkLocation.V3.GC,
                        _FormValue
                    );
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.FlashSwalSuccess("Gift Card Generated", "Gift card generated for customer. Customer can redeem gift card by using mobile number");
                                this.Form_AddUser_Clear();
                                this.Form_AddUser_Close();
                                if (_FormValue.OperationType == "close") {
                                    this.Form_AddUser_Close();
                                }
                            } else {
                                this._HelperService.NotifyError(_Response.Message);
                                this.Form_AddUser_Clear();

                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });
        }
    }
    public _Balance =
        {
            Credit: 0,
            Debit: 0,
            Balance: 0
        }
    GetGiftPointBalance() {
        var pData = {
            Task: 'getbalance',
            // AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            // AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            // // StoreReferenceId: 0,
            // StoreReferenceKey: null,
            SourceCode: 'transaction.source.giftcards',
            // StartDate: null,
            // EndDate: null,
            StartDate: this._HelperService.DateInUTC(this._Monthly.ActualStartDate),
            EndDate: this._HelperService.DateInUTC(this._Monthly.ActualEndDate),
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.GC, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._Balance = _Response.Result;
                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );

    }
giftCardRedirect(){
    // this._HelperService.OpenModal("GiftAway");
}
    // giftCardRedirect(){
    //     if(this._HelperService.AppConfig.Host == "console.thankucash.dev"){
    //        window.open("https://giftcard.thankucash.dev/");
    //     }
    //     else if(this._HelperService.AppConfig.Host == "console.thankucash.tech"){
    //         window.open('https://giftcard.thankucash.tech/');
    //     }
    //     else if(this._HelperService.AppConfig.Host == "console.thankucash.co"){
    //        window.open('https://giftcard.thankucash.co/');
    //     }
    //     else if(this._HelperService.AppConfig.Host == "console.thankucash.com"){
    //        window.open('https://giftcard.thankucash.com/');
    //     }
    //     else{
    //         window.open('https://giftcard.thankucash.dev/');
    //     }
       
    // }

    public _Overview = {
        Total: 0,
        TotalAmount: 0,
        RedeemTotal: 0,
        RedeemAmount: 0,
        TotalCustomer: 0,
        TotalReceivers:0,
        TotalGiftCardReceivers:0,
        Used:0,
    }

    GetGiftCardOverview() {
        var pData: any = {
            Task: 'getgiftcardoverview',
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            // StoreReferenceId: 0,
            // StoreReferenceKey: null,
            SourceCode: 'transaction.source.giftcards',
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.GC, this.giftCardOverviewModel);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (typeof (_Response.Result) == 'object') {
                        console.log(_Response.Result);
                        this._Overview =_Response.Result;
                        for (const key in _Response.Result) {
                            if (Object.prototype.hasOwnProperty.call(_Response.Result, key)) {
                                let card = this.giftCardCounts.find(x => x.id == key);
                                card.value = _Response.Result[key];
                            }
                        }
                    }
                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );

    }

    GetGiftCardHomeOverview(start_date, end_date) {
        var pData = {
            ...this.giftCardOverviewModel,
        }


        pData.StartDate = start_date; this._HelperService.DateInUTC(this._Yearly.ActualStartDate);
        pData.EndDate = end_date; this._HelperService.DateInUTC(this._Yearly.ActualEndDate);


        // if (!this.isMerchantGC) {
        // }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.GC, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (typeof (_Response.Result) == 'object') {
                        for (const key in _Response.Result) {
                            if (Object.prototype.hasOwnProperty.call(_Response.Result, key)) {
                                let card = this.giftCardCounts.find(x => x.id == key);
                                card.value = _Response.Result[key];
                            }
                        }
                    }
                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );

    }


}



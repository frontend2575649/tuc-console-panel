import * as moment from "moment-timezone";

export interface GiftCardCount {
    title: string;
    desc: string;
    value: number | string;
    type: string;
    id: string;
}

export const GiftCardCounts: GiftCardCount[] = [
    {
        title: 'Total sale amount',
        desc: 'Total sale amount of gift card',
        value: 0,
        type: 'amount',
        id: 'TotalAmount'
    },
    {
        title: 'Commission Amount',
        desc: 'Total Commission amount earned from gift card',
        value: 0,
        type: 'amount',
        id: 'CommissionAmount'
    },
    {
        title: 'Gift Card Redeemed',
        desc: 'Total amount of gift cards redeemed',
        value: 0,
        type: 'amount',
        id: 'RedeemAmount'
    },
    {
        title: 'Unredeemed Gift Cards Amount',
        desc: 'This is total number of gift card amount not redeemed',
        value: 0,
        type: 'amount',
        id: 'UnredeemcardAmount'
    },
    {
        title: 'Gift Cards Received',
        desc: 'Total Customer that received gift card',
        value: 0,
        type: 'number',
        id: 'TotalReceivers'
    },
    {
        title: 'Gift Cards Senders',
        desc: 'Total Customer that sends gift card',
        value: 0,
        type: 'number',
        id: 'TotalSenders'
    },
    {
        title: 'Gift Card Expired',
        desc: 'Total number of expired gift cards',
        value: 0,
        type: 'number',
        id: 'GiftCardExpire'
    },
    {
        title: 'Unredeemed Gift Cards',
        desc: 'total number of gift card not redeemed',
        value: 0,
        type: 'number',
        id: 'Unredeemcard'
    }
]

export class GiftCardOverview {
    Task: string = 'gethomegiftcardoverview';
    SourceCode: string = 'transaction.source.giftcards';
    StartDate: string = moment(new Date()).format('YYYY-MM-DD 00:00:00');
    EndDate: string = moment(new Date()).format('YYYY-MM-DD 00:00:00');
}

import { AgmCoreModule } from '@agm/core';
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Select2Module } from "ng2-select2";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgxPaginationModule } from "ngx-pagination";
import { DynamicRoutesguardGuard } from 'src/app/service/guard/dynamicroutes.guard';
import { MainPipe } from '../../../service/main-pipe.module';
import { TUPsspComponent } from "./tupssp.component";



const routes: Routes = [
    {
        path: "",
        component: TUPsspComponent,
        canActivate:[DynamicRoutesguardGuard],
        data:{accessName:['viewpssp']},
        children: [
            { path: ":referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../pssp/dashboards/tudetailpssp/dashboard.module#TUDashboardModule" },
            // { path: "psspaccount", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../pssp/dashboards/tudetailpssp/dashboard.module#TUDashboardModule" },
            { path: "psspaccount/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../pssp/dashboards/tudetailpssp/dashboard.module#TUDashboardModule" },
            { path: "rewardshistory/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../pssp/transactions/tusale/rewardhistory/pssp/tusale.module#TUSaleModule" },
            { path: "redeemedhistory/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../pssp/transactions/tusale/redeemhistory/pssp/tusale.module#TUSaleModule" }, 
            { path: "claimhistory/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../pssp/transactions/tusale/turewardclaim/pssp/turewardclaim.module#TURewardsClaimModule" }, 
            { path: "saleshistory/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../pssp/transactions/tusale/pssp/tusale.module#TUSaleModule" },  
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUPsspRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUPsspRoutingModule,
        GooglePlaceModule,
        MainPipe,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),

    ],
    declarations: [TUPsspComponent]
})
export class TUPsspModule { }

import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from "../../../service/service";
import { HCoreXAddress } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';
import swal from 'sweetalert2';
declare var $: any;
import * as Feather from 'feather-icons';
import { InputFile, InputFileComponent } from 'ngx-input-file';
declare var google: any;

@Component({
    selector: 'app-tuaddpssp',
    templateUrl: './tuaddpssp.component.html',

})
export class AddPsspComponent implements OnInit {
    public SaveAccountRequest: any;
    CurrentImagesCount: number = 0;

    @ViewChild("inputfile")
    private InputFileComponent: InputFileComponent;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {
    }


    ngOnInit() {
        this.Form_AddUser_Load();
        this.GetRoles_List();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
        });
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
        this.GetStateCategories();
        this.InitImagePicker(this.InputFileComponent);

    }
    removeImage(): void {
        this.CurrentImagesCount = 0;
        this._HelperService.Icon_Crop_Clear();
    }


    public _Address: HCoreXAddress = {};
    AddressChange(Address) {
        this._Address = Address;
    }
    private InitImagePicker(InputFileComponent: InputFileComponent) {
        if (InputFileComponent != undefined) {
            this.CurrentImagesCount = 0;
            this._HelperService._InputFileComponent = InputFileComponent;
            InputFileComponent.onChange = (files: Array<InputFile>): void => {
                if (files.length >= this.CurrentImagesCount) {
                    this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
                }
                this.CurrentImagesCount = files.length;
            };
        }
    }
    public GetRoles_Option: Select2Options;
    public GetRoles_Transport: any;
    GetRoles_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreParameters,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'Name',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
        this.GetRoles_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetRoles_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetRoles_Transport,
            multiple: false,
        };
    }
    GetRoles_ListChange(event: any) {
        this.Form_AddUser.patchValue(
            {
                RoleKey: event.value
            }
        );
    }
    _CurrentAddress: any = {};
    Form_AddUser: FormGroup;
    Form_AddUser_Address: string = null;
    Form_AddUser_Latitude: number = 0;
    Form_AddUser_Longitude: number = 0;
    @ViewChild('places') places: GooglePlaceDirective;
    Form_AddUser_PlaceMarkerClick(event) {
        this.Form_AddUser_Latitude = event.coords.lat;
        this.Form_AddUser_Longitude = event.coords.lng;
    }
    public Form_AddUser_AddressChange(address: Address) {
        this.Form_AddUser_Latitude = address.geometry.location.lat();
        this.Form_AddUser_Longitude = address.geometry.location.lng();
        this.Form_AddUser_Address = address.formatted_address;
        this.Form_AddUser.controls['Latitude'].setValue(this.Form_AddUser_Latitude);
        this.Form_AddUser.controls['Longitude'].setValue(this.Form_AddUser_Longitude);
        this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
        // this.Form_AddUser.controls['Address'].setValue( address.formatted_address);
        if (this._CurrentAddress.country != this._HelperService.UserCountrycode) {
            this._HelperService.NotifyError('Currently we’re not serving in this area, please add locality within ' + this._HelperService.UserCountrycode);
            this.reset();

        }
        else {
            this.Form_AddUser.controls['Address'].setValue(address.formatted_address);
            //this.Form_AddUser.controls['MapAddress'].setValue(address.formatted_address);
        }
    }


    reset() {
        this.Form_AddUser.controls['Address'].reset()
        this.Form_AddUser.controls['CityName'].reset()
        this.Form_AddUser.controls['StateName'].reset()
        this.Form_AddUser.controls['CountryName'].reset()

    }

    Form_AddUser_Show() {
    }
    Form_AddUser_Close() {
        this._Router.navigate(['console' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AccountSettings.PSSP]);
    }
    Form_AddUser_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_AddUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SavePssp,
            UserName: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(256)])],
            Password: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.Password)])],
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            SecondaryEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            WebsiteUrl: [null, Validators.compose([Validators.minLength(2), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.WebsiteUrl)])],
            Description: null,
            // Address: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
            StatusCode: this._HelperService.AppConfig.Status.Inactive,
            IconContent: null,
            ReferralCode: null,
            // CountryName: null,
            // Latitude: 0,
            // Longitude: 0,
        });
    }
    Form_AddUser_Clear() {
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 300);
        this.Form_AddUser_Latitude = 0;
        this.Form_AddUser_Longitude = 0;
        this.Form_AddUser.reset();
        this._HelperService.Icon_Crop_Clear();
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_AddUser_Process(_FormValue: any) {
        if (this._HelperService._Icon_Cropper_Data.Content != null) {
            _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
        }

        else if (!this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "") {
            this._HelperService.NotifyError('Please select business location');
        }

        else{

        this._HelperService.IsFormProcessing = true;
        _FormValue.Latitude = this.Form_AddUser_Latitude;
        _FormValue.Longitude = this.Form_AddUser_Longitude;
        this.SaveAccountRequest = this.ReFormat_RequestBody();
        //Location Manager - Start
        this.SaveAccountRequest.Address = this._Address.Address;
        //Location Manager - End  
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, this.SaveAccountRequest);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess('PSSP account created successfully.');
                    this.Form_AddUser_Clear();
                    if (_FormValue.OperationType == 'edit') {
                    }
                    else if (_FormValue.OperationType == 'close') {
                        this.Form_AddUser_Close();
                    }
                    this.Form_AddUser_Close();

                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }}

    ReFormat_RequestBody(): void {
        var formValue: any = this.Form_AddUser.value;
        var formRequest: any = {

            OperationType: 'new',
            Task: formValue.Task,

            DisplayName: formValue.DisplayName,
            Name: formValue.Name,
            ContactNumber: formValue.ContactNumber,
            EmailAddress: formValue.EmailAddress,
            RewardPercentage: formValue.RewardPercentage,
            WebsiteUrl: formValue.WebsiteUrl,
            ReferralCode: formValue.ReferralCode,
            Description: formValue.Description,
            UserName: formValue.UserName,
            Password: formValue.Password,
            StatusCode: formValue.StatusCode,
            // 
            AddressComponent:this._Address,
            // 
            Address: {
                StreetAddress: formValue.Address,
                Address: formValue.Address,
                Latitude: formValue.Latitude,
                Longitude: formValue.Longitude,
               
                MapAddress: formValue.Address,

                CityId: this.cityid,
                CityCode: this.citykey,
                CityName: this.cityname,
               
                StateId: this.stateid,
                StateCode: this.statekey,
                StateName: this.statename,
                
                CountryName: this._HelperService.UserCountrycode,
            },
            ContactPerson: {
                FirstName: formValue.FirstName,
                LastName: formValue.LastName,
                MobileNumber: formValue.MobileNumber,
               // EmailAddress: formValue.EmailAddress
               EmailAddress: formValue.SecondaryEmailAddress
            },
            IconContent: formValue.IconContent


        };

        return formRequest;

    }


    //state
    public StateCategories = [];
    public S2StateCategories = [];
    public ShowstateSelector: boolean = true;
    public ShowcitySelector: boolean = true;
    GetStateCategories() {

        this._HelperService.ToggleField = true;
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Core.getstates,
            ReferenceKey: this._HelperService.UserCountrykey,
            ReferenceId: this._HelperService.UserCountryId,
            //SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
            // SortExpression: 'Name asc',
            Offset: 0,
            Limit: 1000,
        }
       
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.State, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        this.StateCategories = _Response.Result.Data;

                        this.ShowstateSelector = false;
                        this._ChangeDetectorRef.detectChanges();
                        this.S2StateCategories.push(
                            {
                                id: 0,
                                key: "0",
                                text: "Select State"
                            }
                        );
                        for (let index = 0; index < this.StateCategories.length; index++) {
                            const element = this.StateCategories[index];
                            this.S2StateCategories.push(
                                {
                                    id: element.ReferenceId,
                                    key: element.ReferenceKey,
                                    text: element.Name
                                }
                            );
                        }
                        this.ShowstateSelector = true;
                        this._ChangeDetectorRef.detectChanges();

                        this._HelperService.ToggleField = false;

                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                this._HelperService.ToggleField = false;

            });
    }
    public SelectedStateCategories = [];
    public selectedstate: any;
    statekey: any; stateid: any; statename: any;
    StateSelected(Items) {
        if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
            this.SelectedStateCategories = Items.value;
            this.statekey = Items.data[0].key;
            this.stateid = Items.data[0].id;
            this.statename = Items.data[0].text;
            this.selectedstate = true;
            this.GetCityCategories()
        }
        else {
            this.SelectedStateCategories = [];
        }
    }


    //City
    public CityCategories = [];
    public S2CityCategories = [];

    GetCityCategories() {


        this._HelperService.ToggleField = true;
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Core.getcities,
            ReferenceKey: this.statekey,
            ReferenceId: this.stateid,
           
            Offset: 0,
            Limit: 1000,
        }
       
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.City, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        this.CityCategories = _Response.Result.Data;
                        this.ShowcitySelector = false;
                        this._ChangeDetectorRef.detectChanges();
                        this.S2CityCategories = [];
                        for (let index = 0; index < this.CityCategories.length; index++) {
                            const element = this.CityCategories[index];
                            this.S2CityCategories.push(
                                {
                                    id: element.ReferenceId,
                                    key: element.ReferenceKey,
                                    text: element.Name
                                }
                            );
                        }
                        this.ShowcitySelector = true;
                        this._ChangeDetectorRef.detectChanges();

                        this._HelperService.ToggleField = false;

                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
                this._HelperService.ToggleField = false;

            });


    }
    public SelectedCityCategories = [];
    public citykey: any; public cityid: any; cityname: any;
    CitySelected(Items) {
        if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
            this.SelectedCityCategories = Items.value;
            this.citykey = Items.data[0].key;
            this.cityid = Items.data[0].id
            this.cityname = Items.data[0].text
        }
        else {
            this.SelectedCityCategories = [];
        }
    }
    clearImg(){
        this._HelperService.Icon_Crop_Clear();
        this.InputFileComponent.files.pop();
    }
}

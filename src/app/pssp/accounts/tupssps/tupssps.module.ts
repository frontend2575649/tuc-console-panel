import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { MainPipe } from '../../../service/main-pipe.module'

import { TUPsspsComponent } from "./tupssps.component";
import { MerchantguardGuard } from "src/app/service/guard/merchantguard.guard";
const routes: Routes = [{ path: "",canActivate:[MerchantguardGuard], component: TUPsspsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUPsspsRoutingModule {}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TUPsspsRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    MainPipe
  ],
  declarations: [TUPsspsComponent]
})
export class TUPsspsModule {}

import { ChangeDetectorRef, Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BaseChartDirective, Label } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OList, OResponse, OSelect } from '../../../service/service';
declare var moment: any;
declare var $: any;
import * as pluginEmptyOverlay from "chartjs-plugin-empty-overlay";
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import swal from 'sweetalert2';
import * as Feather from 'feather-icons';
import { HCoreXAddress } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUDashboardComponent implements OnInit {

  public _Address: HCoreXAddress = {};
  AddressChange(Address) {
    this._Address = Address;
  }
  public TodayDate: any;

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    Feather.replace();

    this._HelperService.ValidateData();
    this._HelperService.FullContainer = false;
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];

      if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
        this._Router.navigate([
          this._HelperService.AppConfig.Pages.System.NotFound
        ]);
      } else {
        this._HelperService.ResetDateRange();
        this.GetMerchantDetails();
        this.GetStateCategories();
        this.Form_UpdateUser_Load();
      }
    });

  }


  public _UserAccount: any =
    {
      MerchantDisplayName: null,
      SecondaryEmailAddress: null,
      BankDisplayName: null,
      BankKey: null,
      OwnerName: null,
      SubOwnerAddress: null,
      SubOwnerLatitude: null,
      SubOwnerDisplayName: null,
      SubOwnerKey: null,
      SubOwnerLongitude: null,
      AccessPin: null,
      LastLoginDateS: null,
      AppKey: null,
      AppName: null,
      AppVersionKey: null,
      CreateDate: null,
      CreateDateS: null,
      CreatedByDisplayName: null,
      CreatedByIconUrl: null,
      CreatedByKey: null,
      Description: null,
      IconUrl: null,
      ModifyByDisplayName: null,
      ModifyByIconUrl: null,
      ModifyByKey: null,
      ModifyDate: null,
      ModifyDateS: null,
      PosterUrl: null,
      ReferenceKey: null,
      StatusCode: null,
      StatusI: null,
      StatusId: null,
      StatusName: null,
      AccountCode: null,
      AccountOperationTypeCode: null,
      AccountOperationTypeName: null,
      AccountTypeCode: null,
      AccountTypeName: null,
      Address: null,
       AddressComponent:
      {
        Latitude: 0,
        Longitude: 0,
      },
      AppVersionName: null,
      ApplicationStatusCode: null,
      ApplicationStatusName: null,
      AverageValue: null,
      CityAreaKey: null,
      CityAreaName: null,
      CityKey: null,
      CityName: null,
      //  commented Contact no  end
      ContactNumber: null,
      // commented Contact no  end
      CountValue: null,
      CountryKey: null,
      CountryName: null,
      DateOfBirth: null,
      DisplayName: null,
      EmailAddress: null,
      EmailVerificationStatus: null,
      EmailVerificationStatusDate: null,
      FirstName: null,
      GenderCode: null,
      GenderName: null,
      LastLoginDate: null,
      LastName: null,
      Latitude: null,
      Longitude: null,
      // commented mobile no  start
      // MobileNumber: null,
      // commented mobile no  start
      Name: null,
      NumberVerificationStatus: null,
      NumberVerificationStatusDate: null,
      OwnerDisplayName: null,
      OwnerKey: null,
      Password: null,
      Reference: null,
      ReferralCode: null,
      ReferralUrl: null,
      RegionAreaKey: null,
      RegionAreaName: null,
      RegionKey: null,
      RegionName: null,
      RegistrationSourceCode: null,
      RegistrationSourceName: null,
      RequestKey: null,
      RoleKey: null,
      RoleName: null,
      SecondaryPassword: null,
      SystemPassword: null,
      UserName: null,
      WebsiteUrl: null,

      StateKey: null,
      StateName: null

    }
  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
    //    this._ChangeDetectorRef.detectChanges();
  }
  // public _Address: any = {};
  public _ContactPerson: any = {};
  public updateNumber: any;
  public _Overview: any = {};
  GetMerchantDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetPssp,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,
      Accountkey: this._HelperService.AppConfig.ActiveReferenceKey,
      
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.toogleIsFormProcessing(false);
          this._UserAccount = _Response.Result;
          this._Address = this._UserAccount.Address;

          this._Address = this._UserAccount.AddressComponent;

          this._ContactPerson = this._UserAccount.ContactPerson;

          if (this._ContactPerson != undefined && this._ContactPerson.MobileNumber != undefined && this._ContactPerson.MobileNumber != null) {
            if ((this._UserAccount.ContactPerson.MobileNumber.startsWith("234"))||(this._UserAccount.ContactPerson.MobileNumber.startsWith("254"))||(this._UserAccount.ContactPerson.MobileNumber.startsWith("255")||(this._UserAccount.ContactPerson.MobileNumber.startsWith("233")))) {
              this._UserAccount.ContactPerson.MobileNumber = this._UserAccount.ContactPerson.MobileNumber.substring(3, this._UserAccount.ContactPerson.length);
            }
          }
          this.updateNumber = this._UserAccount.ContactPerson.MobileNumber.substring(3);
          this._Overview = this._UserAccount.Overview;
          this._HelperService._Icon_Cropper_Image = this._UserAccount.IconUrl;

          //#region RelocateMarker 

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          // this.FormC_EditUser_Latitude = this._UserAccount.Latitude;
          // this.FormC_EditUser_Longitude = this._UserAccount.Longitude;

          // this._HelperService._ReLocate();

          //#endregion

          //#region DatesAndStatusInit 

          this._UserAccount.StartDateS = this._HelperService.GetDateS(
            this._UserAccount.StartDate
          );
          this._UserAccount.EndDateS = this._HelperService.GetDateS(
            this._UserAccount.EndDate
          );
          this._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(
            this._UserAccount.CreateDate
          );
          this._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(
            this._UserAccount.ModifyDate
          );
          this._UserAccount.StatusI = this._HelperService.GetStatusIcon(
            this._UserAccount.StatusCode
          );
          this._UserAccount.StatusB = this._HelperService.GetStatusBadge(
            this._UserAccount.StatusCode
          );
          this._UserAccount.StatusC = this._HelperService.GetStatusColor(
            this._UserAccount.StatusCode
          );


          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  BlockAccount() {
    swal({
      title: this._HelperService.AppConfig.CommonResource.BlockPsspTitle,
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
        ' <label> Do you really want to block this pssp?  </label>' +
        '<input type="text"  placeholder="Enter Comment"  id="swal-input1" class="swal2-input">' +
        '<input style="-webkit-text-security: disc;" type="text" placeholder="Enter Pin"  id="swal-input2" class="swal2-input">',


      focusConfirm: false,
      preConfirm: () => {
        return [
          document.getElementById('swal-input1')['value'],
          document.getElementById('swal-input2')['value']
        ]
      },
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updateaccountstatus",
          AccountId: this._UserAccount.ReferenceId,
          AccountKey: this._UserAccount.ReferenceKey,
          StatusCode: "default.blocked",
          AuthPin: result.value[1],
          Comment: result.value[0],
          AccountTypeCode: this._HelperService.AppConfig.AccountType.PGAccount

        };

        let _OResponse: Observable<OResponse>;

        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, PostData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("PSSP Blocked Successfully");
              this.GetMerchantDetails();
            } else {
              this._HelperService.NotifySuccess(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });


  }
  UnblockBlockAccount() {
    swal({
      title: this._HelperService.AppConfig.CommonResource.UnBlockPsspTitle,
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
        ' <label> Do you really want to unblock this pssp?  </label>' +
        '<input type="text" placeholder="Enter Comment"  id="swal-input1" class="swal2-input">' +
        '<input style="-webkit-text-security: disc;" type="text" placeholder="Enter Pin" id="swal-input2" class="swal2-input">',


      focusConfirm: false,
      preConfirm: () => {
        return [
          document.getElementById('swal-input1')['value'],
          document.getElementById('swal-input2')['value']
        ]
      },
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updateaccountstatus",
          AccountId: this._UserAccount.ReferenceId,
          AccountKey: this._UserAccount.ReferenceKey,
          StatusCode: "default.active",
          AuthPin: result.value[1],
          Comment: result.value[0],
          AccountTypeCode: this._HelperService.AppConfig.AccountType.PGAccount

        };

        let _OResponse: Observable<OResponse>;

        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts, PostData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("PSSP UnBlocked Successfully");
              this.GetMerchantDetails();
            } else {
              this._HelperService.NotifySuccess(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });


  }






  public GetRoles_Option: Select2Options;
  public GetRoles_Transport: any;
  GetRoles_List() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetCoreParameters,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: '',
      SortCondition: ['Name asc'],
      Fields: [
        {
          SystemName: 'ReferenceKey',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: 'Name',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        {
          SystemName: 'StatusCode',
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: '=',
          SearchValue: this._HelperService.AppConfig.Status.Active,
        }
      ],
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
    this.GetRoles_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetRoles_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetRoles_Transport,
      multiple: false,
    };
  }
  GetRoles_ListChange(event: any) {
    this.Form_UpdateUser.patchValue(
      {
        RoleKey: event.value
      }
    );
  }

  Form_UpdateUser: FormGroup;
  Form_UpdateUser_Address: string = null;
  Form_UpdateUser_Latitude: number = 0;
  Form_UpdateUser_Longitude: number = 0;
  @ViewChild('places') places: GooglePlaceDirective;
  Form_UpdateUser_PlaceMarkerClick(event) {
    this.Form_UpdateUser_Latitude = event.coords.lat;
    this.Form_UpdateUser_Longitude = event.coords.lng;
  }
  public Form_UpdateUser_AddressChange(address: Address) {
    this.Form_UpdateUser_Latitude = address.geometry.location.lat();
    this.Form_UpdateUser_Longitude = address.geometry.location.lng();
    this.Form_UpdateUser_Address = address.formatted_address;
  }
  Form_UpdateUser_Show() {
  }
  Form_UpdateUser_Close() {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AccountSettings.PSSP]);
  }
  Form_UpdateUser_Load() {
    this._HelperService._Icon_Cropper_Data.Width = 128;
    this._HelperService._Icon_Cropper_Data.Height = 128;
    this.Form_UpdateUser = this._FormBuilder.group({
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      UserName: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(256)])],
      Password: [null, Validators.compose([Validators.minLength(8), Validators.maxLength(20), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.Password)])],
      DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
      Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      // commented mobile no  start
      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      // commented mobile no  end
      // commented Contact no  start
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      // commented Contact no  end

      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      SecondaryEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      WebsiteUrl: [null, Validators.compose([Validators.minLength(2), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.WebsiteUrl)])],
      Description: null,
      // Address: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      Address: [null,],
      // AddressComponent: [null,],
      // StatusCode: this._HelperService.AppConfig.Status.Inactive,
      IconContent: this._HelperService._Icon_Cropper_Data,
      CountryName: this._HelperService.UserCountrycode,
      CityId: null,
      CityCode: null,
      CityName: null,
      // CityAreaId: null,
      // CityAreaCode: null,
      // CityAreaName: null,
      StateId: null,
      StateCode: null,
      StateName: null,
    });
  }
  Form_UpdateUser_Clear() {
    this._HelperService.ToggleField = true;
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 300);
    this.Form_UpdateUser_Latitude = 0;
    this.Form_UpdateUser_Longitude = 0;
    this.Form_UpdateUser.reset();
    this._HelperService.Icon_Crop_Clear();
    this.Form_UpdateUser_Load();
  }
  Form_UpdateUser_Process(_FormValue: any) {

    if (this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "") {
      this._HelperService.NotifyError("Please select address");
    }
    else {
      swal({
        position: 'top',
        title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
        text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
        input: 'password',
        inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
        inputAttributes: {
          autocapitalize: 'off',
          autocorrect: 'off',
          maxLength: "4",
          minLength: "4"
        },
        animation: false,
        customClass: this._HelperService.AppConfig.Alert_Animation,
        showCancelButton: true,
        confirmButtonColor: this._HelperService.AppConfig.Color_Red,
        cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
        confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
        cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      }).then((result) => {
        if (result.value) {
          _FormValue.AuthPin = result.value;
          _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveReferenceKey;
          _FormValue.StatusCode = this._UserAccount.StatusCode;
          // 
          _FormValue.Location = this._Address;
          // 
          this._HelperService.IsFormProcessing = true;
          let _OResponse: Observable<OResponse>;
          _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
          _OResponse.subscribe(
            _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.NotifySuccess('Account details updated successfully');
                this._HelperService.Get_UserAccountDetails(true);
                this._HelperService.Icon_Crop_Clear();
                this.GetMerchantDetails();
              }
              else {
                this._HelperService.NotifyError(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
            });
        }
      });


    }
  }


  public ShowstateSelector: boolean = true;
  public ShowcitySelector: boolean = true;
  //state
  public StateCategories = [];
  public S2StateCategories = [];

  GetStateCategories() {

    this._HelperService.ToggleField = true;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getstates,
      ReferenceKey: this._HelperService.UserCountrykey,
      ReferenceId: this._HelperService.UserCountryId,
      //SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      // SortExpression: 'Name asc',
      Offset: 0,
      Limit: 1000,
    }
    // PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
    //     PData.SearchCondition,
    //     "TypeCode",
    //     this._HelperService.AppConfig.DataType.Text,
    //     this._HelperService.AppConfig.HelperTypes.MerchantCategories,
    //     "="
    // );
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.State, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            this.StateCategories = _Response.Result.Data;

            this.ShowstateSelector = false;
            this._ChangeDetectorRef.detectChanges();
            this.S2StateCategories.push(
              {
                id: 0,
                key: "0",
                text: "Select State"
              }
            );
            for (let index = 0; index < this.StateCategories.length; index++) {
              const element = this.StateCategories[index];
              this.S2StateCategories.push(
                {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name
                }
              );
            }
            this.ShowstateSelector = true;
            this._ChangeDetectorRef.detectChanges();

            this._HelperService.ToggleField = false;

          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });
  }
  public SelectedStateCategories = [];
  public selectedstate: any;
  statekey: any; stateid: any; statename: any;
  StateSelected(Items) {
    if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
      this.SelectedStateCategories = Items.value;
      this.statekey = Items.data[0].key;
      this.stateid = Items.data[0].id;
      this.statename = Items.data[0].text;
      this.selectedstate = true;
      this.GetCityCategories()

      this.Form_UpdateUser.controls['StateCode'].patchValue(this.statekey);
      this.Form_UpdateUser.controls['StateId'].patchValue(this.stateid);
      this.Form_UpdateUser.controls['StateName'].patchValue(this.statename);
    }
    else {
      this.SelectedStateCategories = [];
    }
  }


  //City
  public CityCategories = [];
  public S2CityCategories = [];

  GetCityCategories() {


    this._HelperService.ToggleField = true;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getcities,
      ReferenceKey: this.statekey,
      ReferenceId: this.stateid,
      //SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      // SortExpression: 'Name asc',
      Offset: 0,
      Limit: 1000,
    }
    // PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
    //     PData.SearchCondition,
    //     "TypeCode",
    //     this._HelperService.AppConfig.DataType.Text,
    //     this._HelperService.AppConfig.HelperTypes.MerchantCategories,
    //     "="
    // );
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Console.V3.City, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            this.CityCategories = _Response.Result.Data;
            this.ShowcitySelector = false;
            this._ChangeDetectorRef.detectChanges();
            this.S2CityCategories = [];
            for (let index = 0; index < this.CityCategories.length; index++) {
              const element = this.CityCategories[index];
              this.S2CityCategories.push(
                {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name
                }
              );
            }
            this.ShowcitySelector = true;
            this._ChangeDetectorRef.detectChanges();

            this._HelperService.ToggleField = false;

          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });


  }
  public SelectedCityCategories = [];
  public citykey: any; public cityid: any; cityname: any;
  CitySelected(Items) {
    if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
      this.SelectedCityCategories = Items.value;
      this.citykey = Items.data[0].key;
      this.cityid = Items.data[0].id
      this.cityname = Items.data[0].text
      this.Form_UpdateUser.controls['CityCode'].patchValue(this.citykey);
      this.Form_UpdateUser.controls['CityId'].patchValue(this.cityid);
      this.Form_UpdateUser.controls['CityName'].patchValue(this.cityname);
    }
    else {
      this.SelectedCityCategories = [];
    }
  }
}

